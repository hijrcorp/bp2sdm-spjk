package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Distrik {
	public static final String ID = "id_distrik";
	public static final String KODE_PROPINSI = "kode_propinsi_distrik";
	public static final String NAMA = "nama_distrik";

	private String idDistrik;
	private String kodePropinsiDistrik;
	private String namaDistrik;
	

	
	public Distrik() {

	}

	public Distrik(String idDistrik) {
		this.idDistrik = idDistrik;
	}

	
	public String getIdDistrik() {
		return idDistrik;
	}
	public void setIdDistrik(String idDistrik) {
		this.idDistrik = idDistrik;
	}
	public String getKodePropinsiDistrik() {
		return kodePropinsiDistrik;
	}
	public void setKodePropinsiDistrik(String kodePropinsiDistrik) {
		this.kodePropinsiDistrik = kodePropinsiDistrik;
	}
	public String getNamaDistrik() {
		return namaDistrik;
	}
	public void setNamaDistrik(String namaDistrik) {
		this.namaDistrik = namaDistrik;
	}

	
	
//	public Distrik() {
//
//	}
//
//	public Distrik(String id) {
//		this.id = id;
//	}
//
//	@JsonProperty(ID)
//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	@JsonProperty(KODE_PROPINSI)
//	public String getKodePropinsi() {
//		return kodePropinsi;
//	}
//
//	public void setKodePropinsi(String kodePropinsi) {
//		this.kodePropinsi = kodePropinsi;
//	}
//
//	@JsonProperty(NAMA)
//	public String getNama() {
//		return nama;
//	}
//
//	public void setNama(String nama) {
//		this.nama = nama;
//	}


}
