package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.MappingFungsi;

public interface MappingFungsiMapper {
	@Insert("INSERT INTO tbl_mapping_fungsi (id_mapping_fungsi, tujuan_utama_mapping_fungsi, id_kelompok_jabatan_mapping_fungsi, id_source_mapping_fungsi, id_account_added_mapping_fungsi, timestamp_added_mapping_fungsi, id_account_modified_mapping_fungsi, timestamp_modified_mapping_fungsi) VALUES (#{id:VARCHAR}, #{tujuanUtama:VARCHAR}, #{idKelompokJabatan:VARCHAR}, #{idSource:VARCHAR}, #{idAccountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{idAccountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(MappingFungsi mappingFungsi);

	@Update("UPDATE tbl_mapping_fungsi SET id_mapping_fungsi=#{id:VARCHAR}, tujuan_utama_mapping_fungsi=#{tujuanUtama:VARCHAR}, id_kelompok_jabatan_mapping_fungsi=#{idKelompokJabatan:VARCHAR}, id_source_mapping_fungsi=#{idSource:VARCHAR}, id_account_added_mapping_fungsi=#{idAccountAdded:VARCHAR}, timestamp_added_mapping_fungsi=#{timestampAdded:TIMESTAMP}, id_account_modified_mapping_fungsi=#{idAccountModified:VARCHAR}, timestamp_modified_mapping_fungsi=#{timestampModified:TIMESTAMP} WHERE id_mapping_fungsi=#{id}")
	void update(MappingFungsi mappingFungsi);

	@Delete("DELETE FROM tbl_mapping_fungsi WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_mapping_fungsi WHERE id_mapping_fungsi=#{id}")
	void delete(MappingFungsi mappingFungsi);

	List<MappingFungsi> getList(QueryParameter param);

	MappingFungsi getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
