package id.go.menlhk.bp2sdm.spjk.domain;


import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpegJabatan {

	/********************************** - Begin Generate - ************************************/

	public static final String KODE = "kode";
	public static final String JABATAN = "jabatan";
	public static final String ESELON = "eselon";
	public static final String KELOMPOK_JABATAN = "kelompok_jabatan";
	
	private String kode;
	private String jabatan;
	private String eselon;
	private String kelompokJabatan;
	
	
	public SimpegJabatan() {
	
	}
//	public SimpegJabatan(String id) {
//		this.idJabatan = id;
//	}
	
	@JsonProperty(KODE)
	public String getKode() {
		return kode;
	}
	
	public void setKode(String kode) {
		this.kode = kode;
	}
	
	@JsonProperty(JABATAN)
	public String getJabatan() {
		return jabatan;
	}
	
	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
	
	@JsonProperty(ESELON)
	public String getEselon() {
		return eselon;
	}
	
	public void setEselon(String eselon) {
		this.eselon = eselon;
	}
	
	@JsonProperty(KELOMPOK_JABATAN)
	public String getKelompokJabatan() {
		return kelompokJabatan;
	}
	
	public void setKelompokJabatan(String kelompokJabatan) {
		this.kelompokJabatan = kelompokJabatan;
	}
	
	
	/********************************** - End Generate - ************************************/
	public static final String ID_JABATAN = "id_jabatan_mapping_jabatan_simpeg";
	private String idJabatanMappingJabatanSimpeg;


	public String getIdJabatanMappingJabatanSimpeg() {
		return idJabatanMappingJabatanSimpeg;
	}

	public void setIdJabatanMappingJabatanSimpeg(String idJabatanMappingJabatanSimpeg) {
		this.idJabatanMappingJabatanSimpeg = idJabatanMappingJabatanSimpeg;
	}
	
}
