package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiTeknis;

@Mapper
public interface UnitKompetensiTeknisMapper {
	@Insert("INSERT INTO spjk_unit_kompetensi_teknis (id_unit_kompetensi_teknis, id_kelompok_jabatan_unit_kompetensi_teknis, kode_unit_kompetensi_teknis, judul_unit_kompetensi_teknis, id_source_unit_kompetensi_teknis, id_account_added_unit_kompetensi_teknis, timestamp_added_unit_kompetensi_teknis, id_account_modified_unit_kompetensi_teknis, timestamp_modified_unit_kompetensi_teknis) VALUES (#{id:VARCHAR}, #{idKelompokJabatan:VARCHAR}, #{kode:VARCHAR}, #{judul:VARCHAR}, #{idSource:VARCHAR}, #{idAccountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{idAccountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(UnitKompetensiTeknis unitKompetensiTeknis);

	@Update("UPDATE spjk_unit_kompetensi_teknis SET id_unit_kompetensi_teknis=#{id:VARCHAR}, id_kelompok_jabatan_unit_kompetensi_teknis=#{idKelompokJabatan:VARCHAR}, kode_unit_kompetensi_teknis=#{kode:VARCHAR}, judul_unit_kompetensi_teknis=#{judul:VARCHAR}, id_source_unit_kompetensi_teknis=#{idSource:VARCHAR}, id_account_added_unit_kompetensi_teknis=#{idAccountAdded:VARCHAR}, timestamp_added_unit_kompetensi_teknis=#{timestampAdded:TIMESTAMP}, id_account_modified_unit_kompetensi_teknis=#{idAccountModified:VARCHAR}, timestamp_modified_unit_kompetensi_teknis=#{timestampModified:TIMESTAMP} WHERE id_unit_kompetensi_teknis=#{id}")
	void update(UnitKompetensiTeknis unitKompetensiTeknis);

	@Delete("DELETE FROM spjk_unit_kompetensi_teknis WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_unit_kompetensi_teknis WHERE id_unit_kompetensi_teknis=#{id}")
	void delete(UnitKompetensiTeknis unitKompetensiTeknis);

	List<UnitKompetensiTeknis> getList(QueryParameter param);

	List<UnitKompetensiTeknis> getListWithPertanyaan(QueryParameter param);

	UnitKompetensiTeknis getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}