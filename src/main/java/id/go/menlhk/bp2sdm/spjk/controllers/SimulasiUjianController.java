package id.go.menlhk.bp2sdm.spjk.controllers;

import static org.mockito.Matchers.intThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Config;
import id.go.menlhk.bp2sdm.spjk.domain.MappingJabatanUnitAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.Pertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.PertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.Sesi;
import id.go.menlhk.bp2sdm.spjk.domain.User;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.UserResponden;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.bc.PernyataanJawaban;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.common.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.ConfigMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingJabatanUnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserRespondenMapper;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@RestController
@RequestMapping("/simulasi-survey")
public class SimulasiUjianController extends BaseController {


	@Autowired
	private PertanyaanMapper pertanyaanMapper;
	
	@Autowired
	private PertanyaanJawabanMapper pertanyaanJawabanMapper;

	@Autowired
	private UserPertanyaanMapper userPertanyaanMapper;

	@Autowired
	private UserPertanyaanJawabanMapper userPertanyaanJawabanMapper;

	@Autowired
	private UserRespondenMapper userRespondenMapper;

	@Autowired
	private ConfigMapper configMapper;

	@Autowired
	private SesiMapper sesiMapper;
	
	public String idResponden;
    

	@Autowired
	private MappingJabatanUnitAuditiMapper mappingJabatanUnitAuditiMapper;
	
	@RequestMapping(value = "/check-responden/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(@PathVariable String id, HttpServletRequest request, HttpSession session) throws Exception {
		ResponseWrapper resp = new id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper();
		UserResponden data = userRespondenMapper.checkRespondenDataGeneral(id);
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	//
	@RequestMapping(value = "/update-status-soal", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> updateStatusSoal(
			@RequestParam("id") Optional<String> id,
			@RequestParam("mark") Optional<String> mark,
			@RequestParam("jenis") Optional<String> jenis,
			@RequestParam("durasi") Optional<Integer> durasi, HttpSession session,
			HttpServletRequest request, Model model) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		if(!isTokenValid(request)) { 
			resp.setCode(HttpStatus.UNAUTHORIZED.value());
			resp.setMessage("Sesi Login anda telah habis/Anda telah melakukan login pada device yang lain.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		UserPertanyaan data = userPertanyaanMapper.getEntityNormal(id.get());
		if(data==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+data.getIdResponden()+"'");
		param.setClause(param.getClause()+" AND "+UserPertanyaan.STATUS+" ='"+1+"'");
		List<UserPertanyaan> dataNow = userPertanyaanMapper.getList(param);
		if(data.getJenisKompetensi().equals(dataNow.get(0).getJenisKompetensi())) {
			data = dataNow.get(0);
		}else {
			resp.setCode(HttpStatus.NOT_ACCEPTABLE.value());
			resp.setMessage("Seharusnya Kompetensi anda adalah:<strong>"+dataNow.get(0).getJenisKompetensi()+"</strong> bukan <strong>"+data.getJenisKompetensi()+"</strong> , Silahkan refresh halaman anda untuk melanjutkan, atau klik <a href='javascript:void(0)' onclick='window.location.reload()'>disini</a>.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		Integer nosoal = 0;
		if(mark.isPresent()) nosoal = Integer.valueOf(mark.get());
		if(data.getNo() > nosoal) {
			resp.setCode(HttpStatus.NOT_ACCEPTABLE.value());
			resp.setMessage("Nomor Soal tidak sesuai, anda seharusnya berada di nomor:<strong>"+data.getNo()+"</strong>, Silahkan refresh halaman anda untuk melanjutkan, atau klik <a href='javascript:void(0)' onclick='window.location.reload()'>disini</a>.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		if(mark.isPresent()){
			data.setStatus(null);
			userPertanyaanMapper.updateStatus(data);
			
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+data.getIdResponden()+"'");
			param.setClause(param.getClause()+" AND "+UserPertanyaan.NO+" ='"+(data.getNo()+1)+"'");
			param.setClause(param.getClause()+" AND "+UserPertanyaan.JENIS_KOMPETENSI+" ='"+data.getJenisKompetensi()+"'");
			if(userPertanyaanMapper.getCount(param) > 1) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Jika pesan ini muncul, laporakan ke admin.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
			UserPertanyaan userPertanyaan = userPertanyaanMapper.getList(param).get(0);
			userPertanyaan.setStatus("1");
			userPertanyaanMapper.updateStatus(userPertanyaan);
			
			UserResponden ur = userRespondenMapper.checkRespondenDataGeneral(data.getIdResponden());
			if(ur.getStatus().equals("")) {
				ur.setStatus("PROSES");
				userRespondenMapper.update(ur);
			}
		}
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/update-durasi-soal", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> updateDurasiSoal(
			@RequestParam("id") Optional<String> id,
			@RequestParam("mark") Optional<String> mark,
			@RequestParam("jenis") Optional<String> jenis,
			@RequestParam("durasi") Optional<Integer> durasi, HttpSession session,
			HttpServletRequest request, Model model) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		UserPertanyaan data = userPertanyaanMapper.getEntityNormal(id.get());
		data.setDurasiBerjalan(data.getDurasi()-durasi.get());
		userPertanyaanMapper.updateDurasi(data);
		
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	@RequestMapping(value = "/update-status-responden", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> updateStatusResponden(
			@RequestParam("id") Optional<String> id, HttpSession session,
			HttpServletRequest request, Model model) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();

		if(!isTokenValid(request)) { 
			resp.setCode(HttpStatus.UNAUTHORIZED.value());
			resp.setMessage("Sesi Login anda telah habis/Anda telah melakukan login pada device yang lain.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		UserResponden data = userRespondenMapper.getEntity(id.get());
		data.setStatus("PROSES");
		userRespondenMapper.update(data);
		
		resp.setData(data);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	//
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> getIndex(HttpServletRequest request,
			@RequestParam("mark") Optional<String> mark,
			@RequestParam("durasi") Optional<Integer> durasi,
			@RequestParam("header_responden") Optional<String> headerResponden,
			@RequestParam("filter_status") Optional<String> filter_status,HttpSession session,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+ " AND "+ UserResponden.ID_ACCOUNT + " ='" + extractAccountLogin(request, AccountLoginInfo.ACCOUNT) + "'");
		param.setClause(param.getClause()+ " AND "+ UserResponden.ID_SESI + " ='" + sesiMapper.getEntityActive().getId() + "'");
		param.setSecondClause(param.getSecondClause()+ " AND "+ UserPertanyaan.ID_RESPONDEN + " ='" + headerResponden.get() + "'");
		List<UserResponden> listUserResponden = userRespondenMapper.getList(param);
		UserResponden userResponden = new UserResponden(); 
		if(listUserResponden.size()>0) {
			userResponden = listUserResponden.get(0);
		}else {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("User tidak ditemukan..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND "+ UserPertanyaan.ID_RESPONDEN + " ='" + userResponden.getId() + "'");
		param.setClause(param.getClause() + " AND "+ UserPertanyaan.STATUS + " ='1'");	
		List<UserPertanyaan> listUserPertanyaan = userPertanyaanMapper.getList(param);
		String jenisKomp = "";
		if(listUserPertanyaan. size()>0) {
			jenisKomp = listUserPertanyaan.get(0).getJenisKompetensi();
		}else {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Pertanyaan tidak ditemukan..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		param.setClause(param.getClause() + " AND "+UserPertanyaan.JENIS_KOMPETENSI+" ='"+jenisKomp+"'");
		param.setOrder("no_user_pertanyaan");
		
		if(userPertanyaanMapper.getCount(param)>1) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Pertanyaan Have Duplicated..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		QueryParameter param3 = new QueryParameter();
		param3.setClause(param3.getClause() + " AND "+ UserPertanyaan.ID_RESPONDEN + " ='" + userResponden.getId() + "'");
		param3.setClause(param3.getClause() + " AND "+UserPertanyaan.JENIS_KOMPETENSI+" ='"+jenisKomp+"'");

		List<UserPertanyaan> data = new ArrayList<UserPertanyaan>();
		Map<String, Object> map = new HashMap<>();
		if(jenisKomp.equals("MANAJERIAL") || jenisKomp.equals("SOSIOKULTURAL")) {
			data = userPertanyaanMapper.getListNonTeknis(param);
			for(UserPertanyaan up : data) {
				up.setIsi(null);
				for(UserPertanyaanJawaban upj : up.getUserPertanyaanJawaban()) {
					upj.setBobot(null);
					upj.setOrder(null);
				}
			}
			
			List<UserPertanyaan> dataref = userPertanyaanMapper.getListNonTeknis(param3);
			for(UserPertanyaan up : dataref) {
				up.setIsi(null);
				up.setUserPertanyaanJawaban(null);
			}
			map.put("dataref", dataref);
		}else {
			data = userPertanyaanMapper.getList(param);
			for(UserPertanyaan up : data) {
				up.setKunciJawaban(1);
				up.setOrder("1"); 
				for(UserPertanyaanJawaban upj : up.getUserPertanyaanJawaban()) {
					upj.setBobot(null);
				}
			}
			List<UserPertanyaan> dataref = userPertanyaanMapper.getList(param3);
			for(UserPertanyaan up : dataref) {
				up.setKunciJawaban(1);
				up.setUserPertanyaanJawaban(null);
			}
			map.put("dataref", dataref);
		}
		map.put("data", data);
		long total=userPertanyaanMapper.getCount(param3);
		int pageNow=data.get(0).getNo();
		
		resp.setMessage("Pertanyaan "+pageNow+"<strong class='text-success'> dari </strong>"+total);
		resp.setCount(total-pageNow);
		resp.setNextPageNumber(pageNow+1);
		resp.setData(map);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	/*@Scheduled(cron = "0 28 21 46 * ?")
	@RequestMapping(value = "/tes", method = RequestMethod.GET, produces = "application/json")
	public void scheduleTaskUsingCronExpression() {
	 
	    long now = System.currentTimeMillis() / 1000;
	    System.out.println(
	      "schedule tasks using cron jobs - " + now);
	}*/
	
	//gagal methodnya kurang pas, tapi lumayan baru tau fungsi ini bisa dipake bwt scheduling 
    /*@Scheduled(fixedRate = 10*1000)
    public void fixedRateSch() {
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

       Date now = new Date();
       String strDate = sdf.format(now);
       System.out.println("Fixed Rate scheduler:: " + strDate);
       System.out.println("idResponden:: "+ idResponden);
       //if(idResponden!=null && idUser!=null && idSesi!=null) {
       if(idResponden!=null) {
    	   System.out.println("TRY DO SOMETHING");
	       UserResponden userResponden = userRespondenMapper.getEntity(idResponden);
	       int durasi=(userResponden.getDurasiBerjalan()!=null?userResponden.getDurasiBerjalan():0);
	       userResponden.setDurasiBerjalan(durasi+10);
	       userRespondenMapper.update(userResponden);
       }
    }*/
	
	@RequestMapping(value = "/mulai", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> mulai(
			@RequestParam("id") Optional<String> id,
			@RequestParam("id_responden") Optional<String> idResponden,
			@RequestParam("jumlah_kompetensi_pilihan") Optional<Integer> totalKompPilihan,
			HttpSession session,
			HttpServletRequest request, Model model) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();
		if(!isTokenValid(request)) { 
			resp.setCode(HttpStatus.UNAUTHORIZED.value());
			resp.setMessage("Sesi Login anda telah habis/Anda telah melakukan login pada device yang lain.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		String modeJenisKomp="TEKNIS_INTI";
		String[] jkt = request.getParameterValues("teknis_inti");
		
		if(jkt==null) {
			modeJenisKomp="TEKNIS_PILIHAN";
			jkt = request.getParameterValues("teknis_pilihan");
		}
		if(jkt==null) {
			modeJenisKomp="MANAJERIAL";
			jkt = request.getParameterValues("manajerial");
		}
		if(jkt==null) {
			modeJenisKomp="SOSIOKULTURAL";
			jkt = request.getParameterValues("sosiokultural");
		}
		
		if(modeJenisKomp.equals("TEKNIS_PILIHAN")) {
			//total kompetensi - kompetensi yg dipilih
			int calc=totalKompPilihan.get()-jkt.length;
			if(calc==totalKompPilihan.get()) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Kompetensi PILIHAN belum dipilih!");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}else {
				if(totalKompPilihan.get() > 1) {
					if(jkt.length < 2) {
						resp.setCode(HttpStatus.BAD_REQUEST.value());
						resp.setMessage("Minimal 2 Kompetensi PILIHAN wajib dipilih!");
						return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
					}
				}
			}
		}
		StringJoiner strJkt = new StringJoiner(", ");
		for(String jenisKomp : jkt) {
			strJkt.add(jenisKomp);
		}

		UserResponden userResponden = userRespondenMapper.getEntity(idResponden.get());
		//get limit soal from setting konfig
		QueryParameter paramConfig = new QueryParameter();
		paramConfig.setClause(paramConfig.getClause()+" AND "+Config.CODE+" ='"+"LIMIT_SOAL"+"'");
		Config config = configMapper.getList(paramConfig).get(0);
		String limit = config.getValue();
		System.out.println("strJkt "+strJkt);
		System.out.println("limit "+limit);
		
		QueryParameter paramPernyataan = new QueryParameter();
		paramPernyataan.setClause(paramPernyataan.getClause() + " AND " + Pertanyaan.ID_UNIT_KOMPETENSI + " in(" + strJkt + ")");
		if(modeJenisKomp.toUpperCase().equals("MANAJERIAL") || modeJenisKomp.toUpperCase().equals("SOSIOKULTURAL")) {
			paramPernyataan.setSecondClause(paramPernyataan.getSecondClause() + " AND " + Pertanyaan.JENIS + "='"+modeJenisKomp.toUpperCase()+"'");
		}else {
			paramPernyataan.setSecondClause(paramPernyataan.getSecondClause() + " AND " + Pertanyaan.JENIS + "='"+"TEKNIS"+"'");
			paramPernyataan.setSecondClause(paramPernyataan.getSecondClause() + " AND " + Pertanyaan.ID_BIDANG_TEKNIS + "='"+userResponden.getIdBidangTeknis()+"'");
		}
		paramPernyataan.setClause(paramPernyataan.getClause() + " AND " + "no_row"+" <='"+limit+"'"); 
		
		pertanyaanMapper.getListNew(paramPernyataan);
		List<Pertanyaan> dataInti=pertanyaanMapper.getListNew(paramPernyataan);
		if(modeJenisKomp.toUpperCase().equals("MANAJERIAL") || modeJenisKomp.toUpperCase().equals("SOSIOKULTURAL")) {
			pertanyaanMapper.getListNewNonTeknis(paramPernyataan);
			dataInti=pertanyaanMapper.getListNewNonTeknis(paramPernyataan);
		}
		
		String kompString="";
		boolean passKomp=false;
		if(userResponden.getStatusKompInti()!=null && modeJenisKomp=="TEKNIS_INTI") {
			kompString="INTI";
			passKomp=true;
		}else if(userResponden.getStatusKompPilihan()!=null && modeJenisKomp=="TEKNIS_PILIHAN") {
			kompString="PILIHAN";
			passKomp=true;
		}else if(userResponden.getStatusKompManajerial()!=null && modeJenisKomp=="MANAJERIAL") {
			kompString="MANAJERIAL";
			passKomp=true;
		}else if(userResponden.getStatusKompSosiokultural()!=null && modeJenisKomp=="SOSIOKULTURAL") {
			kompString="SOSIOKULTURAAL";
			passKomp=true;
		}
		
		if(passKomp) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Anda tidak dapat memilih unit kompetensi "+kompString+" 2x, refresh halaman anda untuk lanjut, atau klik <a href='javascript:void(0)' onclick='window.location.reload()'>disini</a>");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		//reset mark before insert
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+userResponden.getId()+"'");
		param.setClause(param.getClause()+" AND "+UserPertanyaan.STATUS+" ='"+1+"'");
		
		for(UserPertanyaan x: userPertanyaanMapper.getList(param)){
			x.setStatus(null);
			userPertanyaanMapper.update(x);
		}
		
		if(userResponden.getStatusKompInti()==null || userResponden.getStatusKompPilihan()==null || userResponden.getStatusKompManajerial()==null || userResponden.getStatusKompSosiokultural()==null) {
			//do insert pertanyaaan n jawaban here
			setlistdata(userResponden, dataInti, request, modeJenisKomp.toUpperCase());
		}
		userResponden.setStatus("PROSES");
		userRespondenMapper.update(userResponden);
		
		resp.setData(userResponden);
		session.setAttribute("jenisKompetensi",modeJenisKomp); 
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	public void setlistdata(UserResponden userResponden, List<Pertanyaan>listPernyataan, HttpServletRequest request, String jenisKompetensi) {
		int no=1;
		for (Pertanyaan pertanyaan : listPernyataan) {
				UserPertanyaan userPertanyaan = new UserPertanyaan(Utils.getUUIDString());
				userPertanyaan.setIdResponden(userResponden.getId());
				userPertanyaan.setIdAccount(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
				userPertanyaan.setKode(pertanyaan.getKode());
				userPertanyaan.setIdUnitKompetensi(pertanyaan.getIdUnitKompetensi());
				userPertanyaan.setIsi(pertanyaan.getIsi());
				userPertanyaan.setKeterangan(pertanyaan.getKeterangan());
				userPertanyaan.setBobot(pertanyaan.getBobot());
				userPertanyaan.setKunciJawaban(pertanyaan.getKunciJawaban());
				userPertanyaan.setTipeSoal(pertanyaan.getTipeSoal());
				userPertanyaan.setDurasi(pertanyaan.getDurasi());
				userPertanyaan.setJenisKompetensi(jenisKompetensi);
				userPertanyaan.setNo(no);
				//
				if(pertanyaan.getFile()!=null) {
					String extforsoal = FilenameUtils.getExtension(pertanyaan.getFile());
					if(!extforsoal.equals("null"))  userPertanyaan.setFile(pertanyaan.getId()+"."+extforsoal);
				}
				//
				if(no==1) userPertanyaan.setStatus("1");
				userPertanyaanMapper.insert(userPertanyaan);
				
				for (PertanyaanJawaban pertanyaanJawaban : pertanyaan.getListPertanyaanJawaban()) {
					UserPertanyaanJawaban userPertanyaanJawaban = new UserPertanyaanJawaban(Utils.getUUIDString());
					userPertanyaanJawaban.setIdHeader(userPertanyaan.getId());
					userPertanyaanJawaban.setOrder(pertanyaanJawaban.getOrder());
					userPertanyaanJawaban.setIsi(pertanyaanJawaban.getIsi());
					userPertanyaanJawaban.setBobot(pertanyaanJawaban.getBobot());
					if(pertanyaanJawaban.getFile()!=null) {
						String ext = FilenameUtils.getExtension(pertanyaanJawaban.getFile());
						if(!ext.equals("null")) {
							userPertanyaanJawaban.setFile(pertanyaanJawaban.getId()+"."+ext);
							userPertanyaanJawaban.setFilename(pertanyaanJawaban.getFilename());
						}
					}
					userPertanyaanJawabanMapper.insert(userPertanyaanJawaban);
				}
			no++;
		}
	}    

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> save(
			@RequestParam("id") Optional<String> id,
			@RequestParam("id_sesi") Optional<String> idSesi,
			@RequestParam("nip") Optional<String> nip, 
			@RequestParam("golongan") Optional<String> golongan, 
			@RequestParam("nama") Optional<String> nama, 
			@RequestParam("id_eselon1") Optional<String> idEselon1, 
			@RequestParam("id_unit_kerja") Optional<String> idUnitKerja, 
			@RequestParam("id_propinsi") Optional<String> idPropinsi, 
			@RequestParam("jenis_kelamin") Optional<String> jenisKelamin, 
			@RequestParam("tanggal_lahir") @DateTimeFormat(pattern = "yyyy-MM-dd") Optional<Date> tanggalLahir,
    		@RequestParam("id_bidang_teknis") Optional<String> idBidangTeknis,
			@RequestParam("id_jabatan") Optional<String> idJabatan,
			HttpSession session,
			HttpServletRequest request) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();
		if(!isTokenValid(request)) { 
			resp.setCode(HttpStatus.UNAUTHORIZED.value());
			resp.setMessage("Sesi Login anda telah habis/Anda telah melakukan login pada device yang lain.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		//
		UserResponden data = new UserResponden(Utils.getLongNumberID());
		if(id.isPresent()) {
			data = userRespondenMapper.getEntity(id.get());
		}
		if(data==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if(nip.isPresent()) data.setNip(nip.get());
		if(golongan.isPresent()) data.setGolongan(golongan.get());
		if(nama.isPresent()) data.setNama(nama.get());
		if(idEselon1.isPresent()) data.setIdEselon1(idEselon1.get());
		if(idUnitKerja.isPresent()) data.setIdUnitKerja(idUnitKerja.get());
		if(idPropinsi.isPresent())data.setIdPropinsi(idPropinsi.get());
		if(jenisKelamin.isPresent()) data.setJenisKelamin(jenisKelamin.get());
		if(tanggalLahir.isPresent()) data.setTglLahir(tanggalLahir.get());
		if(idBidangTeknis.isPresent()) data.setIdBidangTeknis(idBidangTeknis.get());
		if(idJabatan.isPresent()) data.setIdJabatan(idJabatan.get());
		
		
		boolean pass = true;
		if (data.getGolongan() == null || data.getIdBidangTeknis() == null || data.getIdJabatan() == null || data.getIdEselon1() == null  || data.getIdUnitKerja() == null || data.getIdPropinsi() == null || data.getTglLahir() == null) {
			pass = false;
		} else {
			if (data.getGolongan().equals("") || data.getIdBidangTeknis().equals("") || data.getIdJabatan().equals("") || data.getIdEselon1().equals("") || data.getIdUnitKerja().equals("") || data.getIdPropinsi().equals("")) {
				pass = false;
			}
		}
		
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+UserResponden.NIP+"='"+data.getNip()+"'");
		param.setClause(param.getClause()+" AND "+UserResponden.ID_SESI+"='"+sesiMapper.getEntityActive().getId()+"'");
		
		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Golongan, Eselon1, Satker, Tanggal Lahir, Bidang Teknis Atau Jabatan anda belum lengkap..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		if(id.isPresent()) {
			System.out.println("data: "+data.getStatusUnitKompetensi());
			System.out.println("Jawaban benar:");
			System.out.println("Jawaban salah:");
			System.out.println("Total Nilai:");
			
			if(data.getStatusKompInti()==null) {
				data.setStatusUnitKompetensi("TEKNIS_INTI");
				data.setStatus("");
			}else if(data.getStatusKompPilihan()==null) {
				data.setStatusUnitKompetensi("TEKNIS_PILIHAN");
				data.setStatus("");
			}else if(data.getStatusKompManajerial()==null) {
				data.setStatusUnitKompetensi("MANAJERIAL");
				data.setStatus("");
			}else if(data.getStatusKompSosiokultural()==null) {
				data.setStatusUnitKompetensi("SOSIOKULTURAL");
				data.setStatus("");
			}else {
				data.setStatusUnitKompetensi(null);
				data.setStatus("SELESAI");
			}

			
			data.setIdPropinsi(data.getUnitAuditi().getIdPropinsiUnitAuditi());
			userRespondenMapper.update(data);
		}else {
			if(userRespondenMapper.getCount(param) > 0) {
				//check id accountnya masih ada atau tidak
				//apabila tidak ada, maka beri warning akunnya telah dilakukan perubahan pada admin
				param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+UserResponden.NIP+"='"+data.getNip()+"'");
				param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+"='"+sesiMapper.getEntityActive().getId()+"'");
				if(mappingJabatanUnitAuditiMapper.getCount(param)==0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
					resp.setMessage("Sistem mendeteksi admin telah melakukan perubahan informasi pada akun anda, silahkan hubungi admin dan logout aplikasi spektra segera, agar ditindaklanjuti, terima kasih.");
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}else {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
					resp.setMessage("Sistem mendeteksi anda telah melakukan konfirmasi form sebelumnya, silahkan <a href='javascript:void(0);' onclick='window.location.reload()'>refresh</a> halaman ini untuk menuju kesesi kompetensi, terima kasih.");
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
			}

			data.setIdSesi(idSesi.get());
			data.setIdAccount(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
			data.setStatusUnitKompetensi("TEKNIS_INTI");

			if(data.getIdSesi()==null || data.getIdSesi().equals("")) {
				if(configMapper.getEntityByKodeActive("NO_SESI")!=null) {
					data.setIdSesi(sesiMapper.getEntityActive().getId());
					/*resp.setCode(HttpStatus.BAD_REQUEST.value());
					resp.setMessage("ID SESI:"+data.getIdSesi());
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));*/
				}
			}
			
			if(data.getIdSesi()==null) {
				pass=false;
			}else {
				if(data.getIdSesi().equals("")){
					pass=false;
				}
			}

			if (!pass) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Sesi masih kosong, coba logout, kemudian login lagi, hingga pesan ini tidak tampil lagi.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
			
			userRespondenMapper.insert(data);
			
			session.setAttribute("jenisKompetensi", "");
			//
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_ACCOUNT+" ='"+data.getIdAccount()+"'");
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+" ='"+data.getIdSesi()+"'");
			mappingJabatanUnitAuditiMapper.deleteBatch(param);
			MappingJabatanUnitAuditi mappingJabatanUnitAuditi = new MappingJabatanUnitAuditi(Utils.getUUIDString());
			mappingJabatanUnitAuditi.setIdAccount(data.getIdAccount());
			mappingJabatanUnitAuditi.setIdSesi(data.getIdSesi());
			mappingJabatanUnitAuditi.setIdJabatan(data.getIdJabatan());
			mappingJabatanUnitAuditi.setIdUnitAuditi(data.getIdUnitKerja());
			mappingJabatanUnitAuditiMapper.insert(mappingJabatanUnitAuditi);
		}
		resp.setData(data);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/jawab", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> jawab(
			@RequestParam("id") Optional<String> id,
			@RequestParam("id_pertanyaan") Optional<String> idPertanyaan, 
			@RequestParam("id_jawaban") Optional<String> idJawaban,
			HttpServletRequest request) throws Exception {
		
		ResponseWrapper resp = new ResponseWrapper();

		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + UserPertanyaanJawaban.ID_HEADER + " ='" + idPertanyaan.get() + "'");
		//UserPertanyaan up = userPertanyaanMapper.getEntityNormal(idPertanyaan.get());
		//String idResponden = up.getIdResponden();
		//UserResponden userResponden = userRespondenMapper.getEntity(idResponden);
		//System.out.println("====");
		//System.out.println("Responden :"+userResponden.getNip()+":"+userResponden.getNama()+":"+sesiMapper.getEntity(userResponden.getIdSesi()).getKode());
		//System.out.println("soal => "+up.getNo()+":"+up.getIsiAndFilename());
		for(UserPertanyaanJawaban upj : userPertanyaanJawabanMapper.getList(param)) {
			if(idJawaban.get().equals(upj.getId())) {
				upj.setIsChecked(1);
			}else {
				upj.setIsChecked(0);
			}
			userPertanyaanJawabanMapper.update(upj);
		}
		
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
}
