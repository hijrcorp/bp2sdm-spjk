package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MappingFungsiDasarDetil {
	public static final String ID = "id_mapping_fungsi_dasar_detil";
	public static final String ID_MAPPING_FUNGSI_UTAMA_DETIL = "id_mapping_fungsi_utama_detil_mapping_fungsi_dasar_detil";
	public static final String ID_UNIT_KOMPETENSI = "id_unit_kompetensi_mapping_fungsi_dasar_detil";

	private String id;
	private String idMappingFungsiUtamaDetil;
	private String idUnitKompetensi;

	public MappingFungsiDasarDetil() {

	}

	public MappingFungsiDasarDetil(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_mapping_fungsi_utama_detil")
	public String getIdMappingFungsiUtamaDetil() {
		return idMappingFungsiUtamaDetil;
	}

	public void setIdMappingFungsiUtamaDetil(String idMappingFungsiUtamaDetil) {
		this.idMappingFungsiUtamaDetil = idMappingFungsiUtamaDetil;
	}

	@JsonProperty("id_unit_kompetensi")
	public String getIdUnitKompetensi() {
		return idUnitKompetensi;
	}

	public void setIdUnitKompetensi(String idUnitKompetensi) {
		this.idUnitKompetensi = idUnitKompetensi;
	}
	
	private UnitKompetensi unitKompetensi;

	public UnitKompetensi getUnitKompetensi() {
		return unitKompetensi;
	}

	public void setUnitKompetensi(UnitKompetensi unitKompetensi) {
		this.unitKompetensi = unitKompetensi;
	}
	
}
