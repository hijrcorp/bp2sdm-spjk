package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiManajerial;

@Mapper
public interface UnitKompetensiManajerialMapper {
	@Insert("INSERT INTO spjk_unit_kompetensi_manajerial (id_unit_kompetensi_manajerial, id_kelompok_manajerial_unit_kompetensi_manajerial, kode_unit_kompetensi_manajerial, judul_unit_kompetensi_manajerial, id_source_unit_kompetensi_manajerial, id_account_added_unit_kompetensi_manajerial, timestamp_added_unit_kompetensi_manajerial, id_account_modified_unit_kompetensi_manajerial, timestamp_modified_unit_kompetensi_manajerial) VALUES (#{id:VARCHAR}, #{idKelompokManajerial:VARCHAR}, #{kode:VARCHAR}, #{judul:VARCHAR}, #{idSource:VARCHAR}, #{idAccountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{idAccountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(UnitKompetensiManajerial unitKompetensiManajerial);

	@Update("UPDATE spjk_unit_kompetensi_manajerial SET id_unit_kompetensi_manajerial=#{id:VARCHAR}, id_kelompok_manajerial_unit_kompetensi_manajerial=#{idKelompokManajerial:VARCHAR}, kode_unit_kompetensi_manajerial=#{kode:VARCHAR}, judul_unit_kompetensi_manajerial=#{judul:VARCHAR}, id_source_unit_kompetensi_manajerial=#{idSource:VARCHAR}, id_account_added_unit_kompetensi_manajerial=#{idAccountAdded:VARCHAR}, timestamp_added_unit_kompetensi_manajerial=#{timestampAdded:TIMESTAMP}, id_account_modified_unit_kompetensi_manajerial=#{idAccountModified:VARCHAR}, timestamp_modified_unit_kompetensi_manajerial=#{timestampModified:TIMESTAMP} WHERE id_unit_kompetensi_manajerial=#{id}")
	void update(UnitKompetensiManajerial unitKompetensiManajerial);

	@Delete("DELETE FROM spjk_unit_kompetensi_manajerial WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_unit_kompetensi_manajerial WHERE id_unit_kompetensi_manajerial=#{id}")
	void delete(UnitKompetensiManajerial unitKompetensiManajerial);

	List<UnitKompetensiManajerial> getList(QueryParameter param);

	UnitKompetensiManajerial getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}