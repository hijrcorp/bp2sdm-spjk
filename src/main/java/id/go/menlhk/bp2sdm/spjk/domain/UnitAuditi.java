package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UnitAuditi {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	public static final String ID_UNIT_AUDITI = "id_unit_auditi";
	public static final String KODE_UNIT_AUDITI = "kode_unit_auditi";
	public static final String NAMA_UNIT_AUDITI = "nama_unit_auditi";
	public static final String ID_PROPINSI_UNIT_AUDITI = "id_propinsi_unit_auditi";
	public static final String ID_DISTRIK_UNIT_AUDITI = "id_distrik_unit_auditi";
	public static final String ID_ESELON1_UNIT_AUDITI = "id_eselon1_unit_auditi";
	public static final String ID_STATUS_AUDITI_UNIT_AUDITI = "id_status_auditi_unit_auditi";
	public static final String KODE_SIMPEG_UNIT_AUDITI = "kode_simpeg_unit_auditi";

	private String idUnitAuditi;
	private String kodeUnitAuditi;
	private String namaUnitAuditi;
	private String idPropinsiUnitAuditi;
	private String idDistrikUnitAuditi;
	private String idEselon1UnitAuditi;
	private String idStatusAuditiUnitAuditi;
	private String kodeSimpegUnitAuditi;
	//
	private String userAdded;
	private String userModified;
	private String userDeleted;

	private Propinsi propinsi;
	private Distrik distrik;
	private Eselon1 eselon1;
	private StatusAuditi statusAuditi;

	public UnitAuditi() {

	}

	public UnitAuditi(String id) {
		this.idUnitAuditi = id;
	}

	@JsonProperty(ID_UNIT_AUDITI)
	public String getIdUnitAuditi() {
		return idUnitAuditi;
	}

	public void setIdUnitAuditi(String idUnitAuditi) {
		this.idUnitAuditi = idUnitAuditi;
	}

	@JsonProperty(NAMA_UNIT_AUDITI)
	public String getNamaUnitAuditi() {
		return namaUnitAuditi;
	}

	public void setNamaUnitAuditi(String namaUnitAuditi) {
		this.namaUnitAuditi = namaUnitAuditi;
	}
	
	@JsonProperty(KODE_UNIT_AUDITI)
	public String getKodeUnitAuditi() {
		return kodeUnitAuditi;
	}

	public void setKodeUnitAuditi(String kodeUnitAuditi) {
		this.kodeUnitAuditi = kodeUnitAuditi;
	}

	@JsonProperty(ID_PROPINSI_UNIT_AUDITI)
	public String getIdPropinsiUnitAuditi() {
		return idPropinsiUnitAuditi;
	}

	public void setIdPropinsiUnitAuditi(String idPropinsiUnitAuditi) {
		this.idPropinsiUnitAuditi = idPropinsiUnitAuditi;
	}
	
	@JsonProperty(ID_DISTRIK_UNIT_AUDITI)
	public String getIdDistrikUnitAuditi() {
		return idDistrikUnitAuditi;
	}

	public void setIdDistrikUnitAuditi(String idDistrikUnitAuditi) {
		this.idDistrikUnitAuditi = idDistrikUnitAuditi;
	}

	@JsonProperty(ID_ESELON1_UNIT_AUDITI)
	public String getIdEselon1UnitAuditi() {
		return idEselon1UnitAuditi;
	}

	public void setIdEselon1UnitAuditi(String idEselon1UnitAuditi) {
		this.idEselon1UnitAuditi = idEselon1UnitAuditi;
	}
	
	@JsonProperty(KODE_SIMPEG_UNIT_AUDITI)
	public String getKodeSimpegUnitAuditi() {
		return kodeSimpegUnitAuditi;
	}

	public void setKodeSimpegUnitAuditi(String kodeSimpegUnitAuditi) {
		this.kodeSimpegUnitAuditi = kodeSimpegUnitAuditi;
	}
	
	
	//costume default
	public String getUserAdded() {
		return userAdded;
	}

	public void setUserAdded(String userAdded) {
		this.userAdded = userAdded;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	public String getUserDeleted() {
		return userDeleted;
	}

	public void setUserDeleted(String userDeleted) {
		this.userDeleted = userDeleted;
	}
	
	//costume added
	public Propinsi getPropinsi() {
		return propinsi;
	}

	public void setPropinsi(Propinsi propinsi) {
		this.propinsi = propinsi;
	}
	
	public Distrik getDistrik() {
		return distrik;
	}

	public void setDistrik(Distrik distrik) {
		this.distrik = distrik;
	}

	public Eselon1 getEselon1() {
		return eselon1;
	}

	public void setEselon1(Eselon1 eselon1) {
		this.eselon1 = eselon1;
	}

	
	public StatusAuditi getStatusAuditi() {
		return statusAuditi;
	}

	public void setStatusAuditi(StatusAuditi statusAuditi) {
		this.statusAuditi = statusAuditi;
	}

	@JsonProperty(ID_STATUS_AUDITI_UNIT_AUDITI)

	public String getIdStatusAuditiUnitAuditi() {
		return idStatusAuditiUnitAuditi;
	}

	public void setIdStatusAuditiUnitAuditi(String idStatusAuditiUnitAuditi) {
		this.idStatusAuditiUnitAuditi = idStatusAuditiUnitAuditi;
	}


	/**********************************
	 * - End Generate -
	 ************************************/

	private String namaPropinsi;
	private String namaDistrik;
	private String namaEselon1;
	private String namaStatusAuditi;

	@JsonProperty("nama_propinsi")
	public String getNamaPropinsi() {
		return namaPropinsi;
	}

	public void setNamaPropinsi(String namaPropinsi) {
		this.namaPropinsi = namaPropinsi;
	}
	
	@JsonProperty("nama_distrik")
	public String getNamaDistrik() {
		return namaDistrik;
	}

	public void setNamaDistrik(String namaDistrik) {
		this.namaDistrik = namaDistrik;
	}

	@JsonProperty("nama_eselon1")
	public String getNamaEselon1() {
		return namaEselon1;
	}

	public void setNamaEselon1(String namaEselon1) {
		this.namaEselon1 = namaEselon1;
	}
	
	@JsonProperty("nama_status_auditi")
	public String getNamaStatusAuditi() {
		return namaStatusAuditi;
	}

	public void setNamaStatusAuditi(String namaStatusAuditi) {
		this.namaStatusAuditi = namaStatusAuditi;
	}
	
}
