package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UserResponden;

public interface UserRespondenMapper {

	@Insert("INSERT INTO spjk_user_responden (id_user_responden, id_sesi_user_responden, id_account_user_responden, saran_user_responden, nip_user_responden, golongan_user_responden, nama_user_responden, jenis_kelamin_user_responden, tgl_lahir_user_responden, id_eselon1_user_responden, id_unit_kerja_user_responden, id_propinsi_user_responden, id_bidang_teknis_user_responden, id_jabatan_user_responden, status_unit_kompetensi_user_responden, status_user_responden, durasi_berjalan_user_responden, date_added_user_responden) VALUES (#{id:VARCHAR}, #{idSesi:VARCHAR}, #{idAccount:VARCHAR}, #{saran:VARCHAR}, #{nip:VARCHAR}, #{golongan:VARCHAR}, #{nama:VARCHAR}, #{jenisKelamin:VARCHAR}, #{tglLahir:DATE}, #{idEselon1:VARCHAR}, #{idUnitKerja:VARCHAR}, #{idPropinsi:VARCHAR}, #{idBidangTeknis:VARCHAR}, #{idJabatan:VARCHAR}, #{statusUnitKompetensi:VARCHAR}, #{status:VARCHAR}, #{durasiBerjalan:NUMERIC}, NOW())")
	void insert(UserResponden userResponden);

	@Update("UPDATE spjk_user_responden SET id_user_responden=#{id:VARCHAR}, id_sesi_user_responden=#{idSesi:VARCHAR}, id_account_user_responden=#{idAccount:VARCHAR}, saran_user_responden=#{saran:VARCHAR}, nip_user_responden=#{nip:VARCHAR}, golongan_user_responden=#{golongan:VARCHAR}, nama_user_responden=#{nama:VARCHAR}, jenis_kelamin_user_responden=#{jenisKelamin:VARCHAR}, tgl_lahir_user_responden=#{tglLahir:DATE}, id_eselon1_user_responden=#{idEselon1:VARCHAR}, id_unit_kerja_user_responden=#{idUnitKerja:VARCHAR}, id_propinsi_user_responden=#{idPropinsi:VARCHAR}, id_bidang_teknis_user_responden=#{idBidangTeknis:VARCHAR}, id_jabatan_user_responden=#{idJabatan:VARCHAR}, status_unit_kompetensi_user_responden=#{statusUnitKompetensi:VARCHAR}, status_user_responden=#{status:VARCHAR}, durasi_berjalan_user_responden=#{durasiBerjalan:NUMERIC}, date_modified_user_responden=NOW() WHERE id_user_responden=#{id}")
	void update(UserResponden userResponden);

	@Delete("DELETE FROM spjk_user_responden WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_user_responden WHERE id_user_responden=#{id}")
	void delete(UserResponden userResponden);

	List<UserResponden> getList(QueryParameter param);

	UserResponden getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	//costum
	long getCountGeneral(QueryParameter param);
	List<UserResponden> getListDataGeneral(QueryParameter param);
	UserResponden checkRespondenDataGeneral(String id);

}
