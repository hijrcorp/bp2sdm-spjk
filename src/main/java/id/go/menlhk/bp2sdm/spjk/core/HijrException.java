package id.go.menlhk.bp2sdm.spjk.core;

public class HijrException extends Exception {

	public final static String ERR_DUPLICATE = "DUPLICATE";

	public HijrException(String errorType, String message) {
		super(errorType + ":" + message);
	}

}
