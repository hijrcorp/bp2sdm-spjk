package id.go.menlhk.bp2sdm.spjk.core;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import id.go.menlhk.bp2sdm.spjk.domain.User;

public abstract class BaseServiceImpl {

	protected User userLogin;
	protected OAuth2Authentication userAuthentication;

	public void setUserLogin(User user) {
		this.userLogin = user;
	}

	public void loadUserLogin(OAuth2Authentication authentication) {
		System.out.println("loaded..");

		userAuthentication = authentication;
	}

	public OAuth2Authentication getAuthentication() {
		return userAuthentication;
	}

}
