package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DashboardBidangSPJK {
	//public static final String ID = "id_pertanyaan";
	
	private String namaBidangTeknis;
	private Integer jumlahTotalUnitKompetensi;
	private Integer jumlahTerisiUnitKompetensi;
	private Float persentaseTerisiUnitKompetensi;

	//@JsonProperty("id")
	
	public DashboardBidangSPJK() {
	
	}
	
	public DashboardBidangSPJK(String id) {
		
	}

	
	public String getNamaBidangTeknis() {
		return namaBidangTeknis;
	}

	public void setNamaBidangTeknis(String namaBidangTeknis) {
		this.namaBidangTeknis = namaBidangTeknis;
	}

	public Integer getJumlahTotalUnitKompetensi() {
		return jumlahTotalUnitKompetensi;
	}

	public void setJumlahTotalUnitKompetensi(Integer jumlahTotalUnitKompetensi) {
		this.jumlahTotalUnitKompetensi = jumlahTotalUnitKompetensi;
	}

	public Integer getJumlahTerisiUnitKompetensi() {
		return jumlahTerisiUnitKompetensi;
	}

	public void setJumlahTerisiUnitKompetensi(Integer jumlahTerisiUnitKompetensi) {
		this.jumlahTerisiUnitKompetensi = jumlahTerisiUnitKompetensi;
	}

	public Float getPersentaseTerisiUnitKompetensi() {
		return persentaseTerisiUnitKompetensi;
	}

	public void setPersentaseTerisiUnitKompetensi(Float persentaseTerisiUnitKompetensi) {
		this.persentaseTerisiUnitKompetensi = persentaseTerisiUnitKompetensi;
	}
	
	

}