package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MappingFungsiUtamaDetil {
	public static final String ID = "id_mapping_fungsi_utama_detil";
	public static final String ID_MAPPING_FUNGSI_KUNCI_DETIL = "id_mapping_fungsi_kunci_detil_mapping_fungsi_utama_detil";
	public static final String FUNGSI_UTAMA = "fungsi_utama_mapping_fungsi_utama_detil";

	private String id;
	private String idMappingFungsiKunciDetil;
	private String fungsiUtama;

	public MappingFungsiUtamaDetil() {

	}

	public MappingFungsiUtamaDetil(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_mapping_fungsi_kunci_detil")
	public String getIdMappingFungsiKunciDetil() {
		return idMappingFungsiKunciDetil;
	}

	public void setIdMappingFungsiKunciDetil(String idMappingFungsiKunciDetil) {
		this.idMappingFungsiKunciDetil = idMappingFungsiKunciDetil;
	}

	@JsonProperty("fungsi_utama")
	public String getFungsiUtama() {
		return fungsiUtama;
	}

	public void setFungsiUtama(String fungsiUtama) {
		this.fungsiUtama = fungsiUtama;
	}

	private List<MappingFungsiDasarDetil> listMappingFungsiDasarDetil = new ArrayList<MappingFungsiDasarDetil>();

	public List<MappingFungsiDasarDetil> getListMappingFungsiDasarDetil() {
		return listMappingFungsiDasarDetil;
	}

	public void setListMappingFungsiDasarDetil(List<MappingFungsiDasarDetil> listMappingFungsiDasarDetil) {
		this.listMappingFungsiDasarDetil = listMappingFungsiDasarDetil;
	}
	
}
