package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Distrik;
import id.go.menlhk.bp2sdm.spjk.domain.Kelompok;
import id.go.menlhk.bp2sdm.spjk.domain.KelompokJabatan;
import id.go.menlhk.bp2sdm.spjk.domain.KelompokTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.Organisasi;
import id.go.menlhk.bp2sdm.spjk.domain.Pegawai;
import id.go.menlhk.bp2sdm.spjk.domain.Propinsi;
import id.go.menlhk.bp2sdm.spjk.domain.Sumber;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.ViewJabatan;

@Mapper
public interface ReferenceMapper {
	//
	// @Select("select `value` from tbl_config where `key`=concat(#{key},(select
	// `value` from tbl_config where `key`='DEV_CONFIG_SUFFIX'))")
	// String getConfigValue(@Param("key") String key);
	//
	// @Select("select * from tbl_kelompok_upt")
	// List<KelompokUPT> getListKelompokUPT();
	//
	// @Select("select * from tbl_program")
	// List<Program> getListProgram();
	//
	// @Select("select * from tbl_tugas_fungsi")
	// List<TugasFungsi> getListTugasFungsi();
	//
	// @Select("select * from tbl_jabatan")
	// List<Jabatan> getListJabatan();
	//
	//// @Select("SELECT tugas_fungsi_id, uraian_tugas_fungsi, unit_auditi_id,
	// kode_unit_auditi, nama_unit_auditi, nama_kelompok_upt FROM tbl_unit_auditi a
	// INNER JOIN tbl_kelompok_upt b ON a.kelompok_upt_id=b.kelompok_upt_id INNER
	// JOIN tbl_tugas_fungsi c ON a.kelompok_upt_id=c.kelompok_upt_id \r\n" +
	//// "WHERE unit_auditi_id=#{id}\r\n" +
	//// "ORDER BY tugas_fungsi_id, uraian_tugas_fungsi")
	//// UnitAuditi findBySatker(@Param("id") String id);
	//
	// @Select("SELECT tugas_fungsi_id, uraian_tugas_fungsi, unit_auditi_id,
	// kode_unit_auditi, nama_unit_auditi, nama_kelompok_upt FROM tbl_unit_auditi a
	// INNER JOIN tbl_kelompok_upt b ON a.kelompok_upt_id=b.kelompok_upt_id INNER
	// JOIN tbl_tugas_fungsi c ON a.kelompok_upt_id=c.kelompok_upt_id \r\n" +
	// "WHERE unit_auditi_id=#{id}\r\n" +
	// "ORDER BY tugas_fungsi_id, uraian_tugas_fungsi")
	// List<UnitAuditia> findBySatker(@Param("id") String id);
	//
	//
	
	Pegawai getPegawai(String nip);
	
	ViewJabatan getJabatan(String kode);
	
	Organisasi getOrganisasi(String kode_organisasi);
	
	@Select("select * from spjk_propinsi where ${clause}")
	List<Propinsi> getListPropinsi(QueryParameter param);
	
	@Select("select * from spjk_distrik where ${clause}")
	List<Distrik> getListDistrik(QueryParameter param);

	@Select("select * from survey_sumber")
	List<Sumber> getListSumber();

//	@Select("select * from spjk_kelompok_manajerial")
	List<Kelompok> getListKelompokManajerial();

//	@Select("select * from spjk_kelompok_sosiokultural")
	List<Kelompok> getListKelompokSosiokultural();

	@Select("SELECT count(*) from spjk_unit_kompetensi_teknis WHERE kode_unit_kompetensi_teknis=#{kode}")
	int checkUnitKompetensiTeknis(@Param("kode") String kode);
	
	@Select("SELECT * from spjk_unit_kompetensi_teknis")
	List<UnitKompetensiTeknis> unitKompetensiTeknis(@Param("kode") String kode);
	
	List<KelompokTeknis> kelompokTeknis(@Param("id_bidang_teknis") String idBidangTeknis);

//	@Select("SELECT * from spjk_unit_auditi WHERE kode_simpeg_unit_auditi=#{kode}")
//	List<UnitAuditi> getListUnitAuditi(@Param("kode") String kode);
	
	@Select("SELECT * from spjk_unit_auditi")
	List<UnitAuditi> getListUnitAuditi();
	

	@Select("select id_propinsi from spjk_view_organisasi_mapping where id_organisasi=#{id}")
	List<String> findOrganisasiPropinsiById(@Param("id") String id);
	


	@Select("select a.*, a.nama_kelompok_jabatan nama, a.id_kelompok_jabatan id from spjk_kelompok_jabatan a")
	List<KelompokJabatan> getListKelompokJabatan(QueryParameter param);
}
