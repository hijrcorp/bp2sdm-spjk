package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import id.go.menlhk.bp2sdm.spjk.MainApplication;
import id.go.menlhk.bp2sdm.spjk.common.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.core.Token;
import id.go.menlhk.bp2sdm.spjk.domain.Control;
import id.go.menlhk.bp2sdm.spjk.domain.MappingJabatanUnitAuditi;
import id.go.menlhk.bp2sdm.spjk.helper.BasicAuthRestTemplate;
import id.go.menlhk.bp2sdm.spjk.helper.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountGroupMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ApplicationMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.AuthoritiesMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ControlMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper;
import id.go.menlhk.bp2sdm.spjk.model.Account;
import id.go.menlhk.bp2sdm.spjk.model.Application;
import id.go.menlhk.bp2sdm.spjk.model.Authorities;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;


@Configuration
@Controller
public class GlobalController extends BaseController {

	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Autowired
    private AccountMapper accountMapper;
	
	@Autowired
	private AccountGroupMapper accountGroupMapper;

	
	@Autowired
	private AuthoritiesMapper authoritiesMapper;
	
	@Autowired
	private ApplicationMapper applicationMapper;
	
	@Autowired
	private ControlMapper controlMapper;

	
	@Autowired
	private SesiMapper sesiMapper;

	@RequestMapping(value="/check_token", method = RequestMethod.GET, produces = "application/json")
 	public ResponseEntity checkToken(HttpServletRequest request) {
		if(isTokenValid(request)) {
			return new ResponseEntity(HttpStatus.OK);
		}
		
		return new ResponseEntity(HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value = "/login-perform", method = RequestMethod.GET)
	public String getLoginPerform(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String code, @RequestParam String page) throws Exception {

		boolean isSuccess = false;
//		String redirect = request.getContextPath() + "/page/" + page;

		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);

		ResponseEntity<Token> result = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			// construct original url waktu request authorization code (WAJIB SAMA)
			String requestPage = request.getRequestURL().toString()+"?page="+page;
	    		
	    	//System.out.println(requestPage);

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("code", code);
			map.add("redirect_uri", requestPage);
			map.add("grant_type", "authorization_code");

			result = restTemplate.postForEntity(tokenEndpointUrl + "/oauth/token", map, Token.class);
			Token token = result.getBody();
			
			//System.out.println(result);
			

			setCookie(request, response, token);
			isSuccess = true;
			//System.out.println(page);
			response.sendRedirect(page);
			//if(token.g .equals("Responden")) redirect="page";

		} catch (HttpClientErrorException e) {
			//System.out.println(e.getResponseBodyAsString());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			//System.out.println(e.getMessage());
		}
		if (!isSuccess) {
			String loginError = ssoEndpointUrl + "/login?error";
			response.sendRedirect(loginError);
		}

		return "blank";
	}
	
	@RequestMapping("/login-logout")
	public void logout(HttpServletRequest request, HttpServletResponse response) throws Exception{ 
		
		
		Cookie cookie1 = new Cookie(cookieName, null);
		cookie1.setPath("/");
		cookie1.setMaxAge(0);
		
		if(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")) {
			cookie1.setDomain(request.getServerName());
		}else {
			cookie1.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
		}
		response.addCookie(cookie1);

		System.out.println("Utils ip? "+(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")));
		System.out.println("breakdown 1 "+(Utils.ip(request.getServerName()))+"="+request.getServerName());
		System.out.println("breakdown 2 is local? "+(request.getServerName().equals("localhost")));
		System.out.println("breakdown request "+(cookie1.getDomain()));
		System.out.println("Is this? "+cookie1);
		System.out.println("never ? "+("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", "")));
		
		String param="";
		if(request.getParameter("duplicate")!=null) {
			param="?duplicate";
		}
		if(request.getParameter("connection")!=null) {
			param="?connection";
		}
		if(request.getParameter("banned")!=null) {
			param="?banned";
		}
		if(request.getParameter("maintenance")!=null) {
			param="?maintenance";
		}
		if(request.getParameter("tryagain")!=null) {
			param="?tryagain";
		}
		String loginPath = ssoEndpointUrl + "/login"+param;//getConstructURL(request, param);
		
		System.out.println("loginPath: "+loginPath);
		response.sendRedirect(ssoEndpointUrl + "/logout?redirect_uri="+loginPath);	
	}
	/*
	String loginError = ssoEndpointUrl + "/login?error";
	response.sendRedirect(loginError);
	*/
	@RequestMapping("/login-password")
	public void password(HttpServletRequest request, HttpServletResponse response) throws Exception{ 

		response.sendRedirect(ssoEndpointUrl + "/password");	
		
		
	}
	

	@RequestMapping("/")
    public String loadIndex(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(pass) {
			addModelTokenInfo(request, locale, model,accessTokenValue);
			
		}
        return "index";
    }

	@RequestMapping(value="/daftar/baru", method = RequestMethod.GET)
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public String daftarBaru(Model model,
    			@RequestParam("email") Optional<String> emails,
    			@RequestParam("mobile") Optional<String> mobiles,
    			HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		return "register";
    }

	@RequestMapping("/register")
    public String register(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		return loadRegister(model, request, response);
    }
	
	private String loadRegister(Model model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if(sesiMapper.getEntityActive()==null) {
			response.sendRedirect(ssoEndpointUrl + "/login?sesierror");
		}
		
		model.addAttribute("action", request.getContextPath() + "/daftar/baru");

		return "register";
	}

    
}


