package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ViewJabatan {
	public static final String KODE = "kode_jabatan";
	public static final String NAMA = "nama_jabatan";
	public static final String KODE_ESELON = "kode_eselon_jabatan";
	public static final String NAMA_ESELON = "nama_eselon_jabatan";
	public static final String KODE_KELOMPOK = "kode_kelompok_jabatan";
	public static final String NAMA_KELOMPOK = "nama_kelompok_jabatan";
	public static final String KODE_KELOMPOK_ESELON = "kode_kelompok_eselon_jabatan";
	public static final String NAMA_KELOMPOK_ESELON = "nama_kelompok_eselon_jabatan";

	private String kode;
	private String nama;
	private String kodeEselon;
	private String namaEselon;
	private String kodeKelompok;
	private String namaKelompok;
	private String kodeKelompokEselon;
	private String namaKelompokEselon;

	public ViewJabatan() {

	}


	@JsonProperty("kode")
	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("kode_eselon")
	public String getKodeEselon() {
		return kodeEselon;
	}

	public void setKodeEselon(String kodeEselon) {
		this.kodeEselon = kodeEselon;
	}

	@JsonProperty("nama_eselon")
	public String getNamaEselon() {
		return namaEselon;
	}

	public void setNamaEselon(String namaEselon) {
		this.namaEselon = namaEselon;
	}

	@JsonProperty("kode_kelompok")
	public String getKodeKelompok() {
		return kodeKelompok;
	}

	public void setKodeKelompok(String kodeKelompok) {
		this.kodeKelompok = kodeKelompok;
	}

	@JsonProperty("nama_kelompok")
	public String getNamaKelompok() {
		return namaKelompok;
	}

	public void setNamaKelompok(String namaKelompok) {
		this.namaKelompok = namaKelompok;
	}

	@JsonProperty("kode_kelompok_eselon")
	public String getKodeKelompokEselon() {
		return kodeKelompokEselon;
	}

	public void setKodeKelompokEselon(String kodeKelompokEselon) {
		this.kodeKelompokEselon = kodeKelompokEselon;
	}

	@JsonProperty("nama_kelompok_eselon")
	public String getNamaKelompokEselon() {
		return namaKelompokEselon;
	}

	public void setNamaKelompokEselon(String namaKelompokEselon) {
		this.namaKelompokEselon = namaKelompokEselon;
	}

}
