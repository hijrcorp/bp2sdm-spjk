package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.domain.User;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthRoleOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUser;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOperatorOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOrganizationUnit;

@Mapper
public interface UserMapper {

	public static final String QRY_MASTER_USER = "select a.*, c.position_id, name_position position_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id";
	public static final String QRY_MASTER_LIST_USER = "select a.* from (select a.*, c.position_id, name_position, concat(concat(first_name,' '),last_name) real_name, organization_id from (select id, id user_id, username, first_name, last_name, email description, \"\" token, now() expired from tbl_oauth_user) a inner join tbl_oauth_user_position b on a.user_id=b.user_id inner join tbl_position c on b.position_id=c.position_id ) a inner join (select * from tbl_oauth_user_position where organization_id='1508629134827') b on a.user_id!=b.user_id";

	public static final String QRY_USER_ORGANIZATION = "select a.*, c.position_id, name_position position_name, d.id organization_id, d.code organization_code, d.name organization_name, CONCAT(a.`first_name`,' ',a.`last_name`) full_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id inner join tbl_organization d on b.organization_id=d.id";
	public static final String QRY_USER_ORGANIZATION_LOCAL = "select a.* from (select a.*, c.position_id, name_position position_name, d.id organization_id, d.code organization_code, d.name organization_name, CONCAT(a.`first_name`,' ',a.`last_name`) full_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id inner join tbl_organization d on b.organization_id=d.id) a inner join (select * from tbl_oauth_user_position where organization_id='1508629134827') b on a.id!=b.user_id";
	public static final String QRY_USER_ORGANIZATION_UNIT = "select a.*, c.unit_kerja_id, nama_unit_kerja unit_kerja_name from ("
			+ QRY_USER_ORGANIZATION_LOCAL
			+ ") a left join tbl_unit_kerja_user b on a.id=b.user_id left join tbl_unit_kerja c on b.unit_kerja_id=c.unit_kerja_id";
	public static final String QRY_USER_ORGANIZATION_OPERATOR = "select a.*, b.unit_auditi_id, c.nama_unit_auditi unit_auditi_name from ("
			+ QRY_USER_ORGANIZATION_LOCAL
			+ ") a left join tbl_unit_auditi_user b on a.id=b.user_id inner join tbl_unit_auditi c on b.unit_auditi_id=c.unit_auditi_id";

	@Select("select username_account from spjk_account where id_account=#{userId}")
	List<String> findNIPByUserId(@Param("userId") String userId);
	
	@Select("select unit_kerja_id from tbl_unit_kerja_user where user_id=#{userId}")
	List<String> findUnitKerjaByUserId(@Param("userId") String userId);

	@Select("select unit_auditi_id from tbl_unit_auditi_user where user_id=#{userId}")
	List<String> findUnitAuditiByUserId(@Param("userId") String userId);
	
	@Select("select eselon1_id from tbl_eselon1_user where user_id=#{userId}")
	List<String> findEselon1ByUserId(@Param("userId") String userId);

	@Select("select propinsi_id from tbl_propinsi_user where user_id=#{userId}")
	List<String> findPropinsiByUserId(@Param("userId") String userId);

	@Select("select organisasi_id from tbl_organisasi_user where user_id=#{userId}")
	List<String> findOrganisasiByUserId(@Param("userId") String userId);
	
	@Select("select propinsi_id from spjk_view_organisasi_user where user_id=#{userId}")
	List<String> findOrganisasiPropinsiByUserId(@Param("userId") String userId);

	@Select("select reference_id from tbl_reference_user where user_id=#{userId}")
	List<String> findReferenceByUserId(@Param("userId") String userId, @Param("referenceType") String referenceType);

	@Select("select lhp_id from tbl_lhp_analis where analis_id=#{userId}")
	List<String> findLhpIdByUserId(@Param("userId") String userId);

	@Select("select unit_kerja_id from tbl_unit_kerja_user where user_id=#{userId}")
	List<String> findUnitKerjaUser(@Param("userId") String userId);

	@Select("select unit_auditi_id from tbl_unit_auditi_user where user_id=#{userId}")
	List<String> findUnitAuditiUser(@Param("userId") String userId);

	@Insert("insert into tbl_unit_kerja_user values (#{unitKerjaId}, #{userId})")
	void insertUnitKerjaUser(@Param("unitKerjaId") String unitKerjaId, @Param("userId") String userId);

	@Delete("delete from tbl_unit_kerja_user where unit_kerja_id=#{unitKerjaId} and user_id=#{userId}")
	void deleteUnitKerjaUser(@Param("unitKerjaId") String unitKerjaId, @Param("userId") String userId);

	@Delete("delete from tbl_unit_kerja_user where user_id=#{userId}")
	void deleteAllUnitKerjaUser(@Param("userId") String userId);

	//satker
	@Select("select id_eselon1_unit_auditi from tbl_unit_auditi_user a left join spjk_unit_auditi b on b.id_unit_auditi = a.unit_auditi_id where user_id=#{userId}")
	List<String> findEselon1ByUnitAuditiUserId(@Param("userId") String userId);
	
	@Insert("insert into tbl_unit_auditi_user (unit_auditi_id, user_id) values (#{unitAuditiId}, #{userId})")
	void insertUnitAuditiUser(@Param("unitAuditiId") String unitAuditiId, @Param("userId") String userId);

	@Delete("delete from tbl_unit_auditi_user where unit_auditi_id=#{unitAuditiId} and user_id=#{userId}")
	void deleteUnitAuditiUser(@Param("unitAuditiId") String unitAuditiId, @Param("userId") String userId);

	@Delete("delete from tbl_unit_auditi_user where user_id=#{userId}")
	void deleteAllUnitAuditiUser(@Param("userId") String userId);
	
	//eselon1
	@Insert("insert into tbl_eselon1_user (eselon1_id, user_id) values (#{eselon1Id}, #{userId})")
	void insertEselon1User(@Param("eselon1Id") String eselon1Id, @Param("userId") String userId);

	@Delete("delete from tbl_eselon1_user where eselon1_id=#{eselon1Id} and user_id=#{userId}")
	void deleteEselon1User(@Param("eselon1Id") String eselon1Id, @Param("userId") String userId);

	@Delete("delete from tbl_eselon1_user where user_id=#{userId}")
	void deleteAllEselon1User(@Param("userId") String userId);

	//propinsi
	@Insert("insert into tbl_propinsi_user (propinsi_id, user_id) values (#{propinsiId}, #{userId})")
	void insertPropinsiUser(@Param("propinsiId") String propinsiId, @Param("userId") String userId);

	@Delete("delete from tbl_propinsi_user where propinsi_id=#{propinsiId} and user_id=#{userId}")
	void deletePropinsiUser(@Param("propinsiId") String propinsiId, @Param("userId") String userId);

	@Delete("delete from tbl_propinsi_user where user_id=#{userId}")
	void deleteAllPropinsiUser(@Param("userId") String userId);
	
	//organisasi
	@Insert("insert into tbl_organisasi_user (organisasi_id, user_id, organisasi_tipe) values (#{organisasiId}, #{userId}, #{organisasiTipe})")
	void insertOrganisasiUser(@Param("organisasiId") String organisasiId, @Param("userId") String userId, @Param("organisasiTipe") String organisasiTipe);

	@Delete("delete from tbl_organisasi_user where organisasi_id=#{organisasiId} and user_id=#{userId} and organisasi_type=#{organisasiTipe}")
	void deleteOrganisasiUser(@Param("organisasiId") String organisasiId, @Param("userId") String userId, @Param("organisasiTipe") String organisasiTipe);

	@Delete("delete from tbl_organisasi_user where user_id=#{userId}")
	void deleteAllOrganisasiUser(@Param("userId") String userId);
	
	//kelompok jabatan
	@Insert("insert into tbl_reference_user (reference_id, user_id, reference_type) values (#{referenceId}, #{userId}, #{referenceType})")
	void insertReferenceUser(@Param("referenceId") String referenceId, @Param("userId") String userId, @Param("referenceType") String referenceType);

	@Delete("delete from tbl_reference_user where reference_id=#{referenceId} and user_id=#{userId} and reference_type=#{referenceType}")
	void deleteReferenceUser(@Param("referenceId") String referenceId, @Param("userId") String userId, @Param("referenceType") String referenceType);

	@Delete("delete from tbl_reference_user where user_id=#{userId}")
	void deleteAllReferenceUser(@Param("userId") String userId);
	//
	
	@Select("select * from (" + QRY_USER_ORGANIZATION_OPERATOR + ") as a where ${clause}")
	List<OauthUserOperatorOrganization> findListWithOrganizationOperator(@Param("clause") String clause);

	@Select("select count(*) from (" + QRY_USER_ORGANIZATION_OPERATOR + ") t where ${clause}")
	long findCountListWithOrganizationOperator(@Param("clause") String clause);

	@Select("select * from (" + QRY_USER_ORGANIZATION_LOCAL + ") as a where ${clause}")
	List<OauthUserOrganization> findListWithOrganizationLocal(@Param("clause") String clause);

	@Select("select count(*) from (" + QRY_USER_ORGANIZATION_LOCAL + ") t where ${clause}")
	long findCountListWithOrganizationLocal(@Param("clause") String clause);

	@Select("select * from (" + QRY_USER_ORGANIZATION_UNIT + ") as a where ${clause}")
	List<OauthUserOrganizationUnit> findListWithOrganizationUnit(@Param("clause") String clause);

	@Select("select count(*) from (" + QRY_USER_ORGANIZATION_UNIT + ") as a where ${clause}")
	long findCountWithOrganizationUnit(@Param("clause") String clause);

	@Select("select * from (" + QRY_USER_ORGANIZATION + ") as a where ${clause}")
	List<OauthUserOrganization> findListWithOrganization(@Param("clause") String clause);

	@Select("select count(*) from (" + QRY_USER_ORGANIZATION + ") t where ${clause}")
	long findCountListWithOrganization(@Param("clause") String clause);

	@Select("select c.name from tbl_oauth_user_position a inner join tbl_oauth_position_role b on a.position_id=b.position_id inner join tbl_oauth_role c on role_id=c.id where user_id=#{userId} and organization_id=#{organizationId}")
	List<String> findRolesByUserOrganization(@Param("userId") String userId,
			@Param("organizationId") String organizationId);

	// new
	@Select("select a.*, b.code code_organization, b.name name_organization, c.name_position from tbl_organization_position a left join tbl_organization b on id = a.organization_id left join tbl_position c on c.position_id = a.position_id where organization_id=#{organizationId}")
	List<OauthRoleOrganization> findRolesByOrganization(@Param("organizationId") String organizationId);

	//
	@Select("select * from (" + QRY_MASTER_USER
			+ ") as a where id=#{loginId} or username=#{loginId} or email=#{loginId} or mobile_phone=#{loginId}")
	OauthUser findUserByLoginId(@Param("loginId") String loginId);

	@Select("select a.*, b.* from tbl_oauth_user a " + "inner join tbl_oauth_user_position b on b.user_id = a.id "
			+ "where id=#{id}")
	OauthUser findUserById(@Param("id") String id);

	// basic
	@Select("select count(*) from(" + QRY_MASTER_LIST_USER + ") t where ${filter}")
	Integer getCount(@Param("filter") String filter);

	@Select("select * from (" + QRY_MASTER_LIST_USER + ") t where user_id=#{id}")
	User findById(@Param("id") String id);

	@Select("select * from (" + QRY_MASTER_LIST_USER + ") t where ${filter}")
	List<User> getList(@Param("filter") String filter);

	@Insert("insert into tbl_oauth_user\r\n"
			+ "		(id,first_name, last_name,username,password,email, gender, enabled, account_non_expired, credentials_non_expired, account_non_locked) \r\n"
			+ "		values \r\n"
			+ "		(#{id:NUMERIC},#{firstName:VARCHAR},#{lastName:VARCHAR},#{username:VARCHAR},#{password:VARCHAR},#{email:VARCHAR},'M',1,1,1,1\r\n"
			+ "		)")
	void insert(User user);

	@Update("update tbl_oauth_user set \r\n" + "		first_name=#{firstName:VARCHAR},\r\n"
			+ "		last_name=#{lastName:VARCHAR}, \r\n" + "		username=#{username:VARCHAR},\r\n"
			+ "		email=#{email:VARCHAR}\r\n" + "	where id=#{id}")
	void update(User user);

	@Update("update tbl_oauth_user set password=#{password:VARCHAR} where id=#{id}")
	void updatePassword(User user);

	// end here
	@Select("select b.name from tbl_oauth_user_role a inner join tbl_oauth_role b on a.role_id=b.id where a.user_id=#{userId}")
	List<String> findRolesByUserId(@Param("userId") String userId);

	@Insert("insert into tbl_oauth_user_position (user_id, position_id, organization_id) values (#{userId}, #{positionId}, #{organizationId})")
	void insertUserPosition(@Param("userId") long userId, @Param("positionId") long positionId,
			@Param("organizationId") String organizationId);

	@Insert("insert into tbl_oauth_user_role select #{userId} user_id, role_id from tbl_oauth_position_role a inner join tbl_position b on a.position_id=b.position_id where a.position_id=#{positionId}")
	void insertUserRole(@Param("userId") long userId, @Param("positionId") long positionId);

	@Update("update tbl_oauth_user_position set position_id =#{positionId} where user_id=#{userId}")
	void updateUserPosition(@Param("userId") long userId, @Param("positionId") long positionId);

	@Delete("delete from tbl_oauth_user_position where user_id=#{userId}")
	void deleteUserAllPosition(@Param("userId") long userId);

	@Delete("delete from tbl_oauth_user_position where user_id=#{userId} and organization_id=#{organizationId}")
	void deleteUserPosition(@Param("userId") long userId, @Param("organizationId") String organizationId);

	@Delete("delete from tbl_oauth_user_role where user_id=#{userId}")
	void deleteUserRole(@Param("userId") long userId);

	@Delete("delete from tbl_oauth_user where id=#{userId}")
	void deleteUser(@Param("userId") long userId);

	@Update("update tbl_oauth_user set password =#{newPassword} where id=#{userId}")
	void updateUserPassword(@Param("userId") String userId, @Param("newPassword") String newPassword);

	@Select("select * from (select a.*, unit_kerja_id, d.position_id, d.name_position position_name from tbl_oauth_user a inner join tbl_unit_kerja_user b on a.id=b.user_id inner join tbl_oauth_user_position c on a.id=c.user_id inner join tbl_position d on c.position_id=d.position_id) t where ${clause}")
	List<OauthUser> findUserUnitKerja(@Param("clause") String clause);

	@Select("select * from (select a.*, c.position_id, c.name_position position_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id) t where ${clause}")
	List<OauthUser> findUserPosition(@Param("clause") String clause);

}
