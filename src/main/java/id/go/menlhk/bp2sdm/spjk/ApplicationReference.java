package id.go.menlhk.bp2sdm.spjk;

public enum ApplicationReference {
	SURVEY("SURVEY"), SPJK("SPJK");

	private final String text;

	/**
	 * @param text
	 */
	private ApplicationReference(final String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
