package id.go.menlhk.bp2sdm.spjk.domain;

import java.io.Serializable;
import id.go.menlhk.bp2sdm.spjk.core.Metadata;

public class Role extends Metadata implements Serializable {

	private String name;
	private String description;

	public Role() {

	}

	public Role(int id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
