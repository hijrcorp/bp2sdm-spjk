package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.DetilIndividu;

public interface DetilIndividuMapper {
	
	@Insert("INSERT INTO spjk_detil_individu (id_detil_individu, id_user_responden_detil_individu, id_unit_kompetensi_detil_individu, kode_unit_kompetensi_detil_individu, nama_unit_kompetensi_detil_individu, nama_kelompok_detil_individu, nilai_standar_detil_individu, nilai_detil_individu, jenis_kompetensi_detil_individu, kode_sesi_detil_individu) VALUES (#{id:VARCHAR}, #{idUserResponden:VARCHAR}, #{idUnitKompetensi:VARCHAR}, #{kodeUnitKompetensi:VARCHAR}, #{namaUnitKompetensi:VARCHAR}, #{namaKelompok:VARCHAR}, #{nilaiStandar:NUMERIC}, #{nilai:NUMERIC}, #{jenisKompetensi:VARCHAR}, #{kodeSesi:VARCHAR})")
	void insert(DetilIndividu detilIndividu);

	@Update("UPDATE spjk_detil_individu SET id_detil_individu=#{id:VARCHAR}, id_user_responden_detil_individu=#{idUserResponden:VARCHAR}, id_unit_kompetensi_detil_individu=#{idUnitKompetensi:VARCHAR}, kode_unit_kompetensi_detil_individu=#{kodeUnitKompetensi:VARCHAR}, nama_unit_kompetensi_detil_individu=#{namaUnitKompetensi:VARCHAR}, nama_kelompok_detil_individu=#{namaKelompok:VARCHAR}, nilai_standar_detil_individu=#{nilaiStandar:NUMERIC}, nilai_detil_individu=#{nilai:NUMERIC}, jenis_kompetensi_detil_individu=#{jenisKompetensi:VARCHAR}, kode_sesi_detil_individu=#{kodeSesi:VARCHAR} WHERE id_detil_individu=#{id}")
	void update(DetilIndividu detilIndividu);

	@Delete("DELETE FROM spjk_detil_individu WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_detil_individu WHERE id_detil_individu=#{id}")
	void delete(DetilIndividu detilIndividu);

	List<DetilIndividu> getList(QueryParameter param);

	DetilIndividu getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

	//perjabatan teknis
	List<DetilIndividu> getListDetilPerjabatan(QueryParameter param);
	//perjabatan non teknis
	List<DetilIndividu> getListDetilPerjabatanNonTeknis(QueryParameter param);
}
