package id.go.menlhk.bp2sdm.spjk.domain;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;

public class DPdf extends PdfPageEventHelper {

	protected Rectangle pageSize;
	protected float mLeft, mRight, mTop, mBottom;
	protected Rectangle footer;

	// initialized upon opening the document
	protected PdfTemplate background;
	protected PdfTemplate total;

	public Rectangle getPageSize() {
		return pageSize;
	}

	public void setPageSize(Rectangle pageSize) {
		this.pageSize = pageSize;
	}

	public float getmLeft() {
		return mLeft;
	}

	public void setmLeft(float mLeft) {
		this.mLeft = mLeft;
	}

	public float getmRight() {
		return mRight;
	}

	public void setmRight(float mRight) {
		this.mRight = mRight;
	}

	public float getmTop() {
		return mTop;
	}

	public void setmTop(float mTop) {
		this.mTop = mTop;
	}

	public float getmBottom() {
		return mBottom;
	}

	public void setmBottom(float mBottom) {
		this.mBottom = mBottom;
	}

	public Rectangle getFooter() {
		return footer;
	}

	public void setFooter(Rectangle footer) {
		this.footer = footer;
	}

	public PdfTemplate getBackground() {
		return background;
	}

	public void setBackground(PdfTemplate background) {
		this.background = background;
	}

	public PdfTemplate getTotal() {
		return total;
	}

	public void setTotal(PdfTemplate total) {
		this.total = total;
	}

}
