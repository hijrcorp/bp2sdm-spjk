package id.go.menlhk.bp2sdm.spjk.core;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class CommonUtil {

	public final static String QRY_CANDIDATE_DATE = "SELECT #{begin} + INTERVAL i DAY AS cdate FROM (SELECT (hundreds.i * 100) + (tens.i * 10) + units.i AS i FROM integers AS units CROSS JOIN integers AS tens CROSS JOIN integers AS hundreds) integers WHERE i < DATEDIFF(#{end},#{begin})+1";

	public static Boolean isNotNull(Object obj) {
		return (obj != null);
	}

	public static final Double DOUBLE_ZERO = new Double(0);

	public static Boolean isNotNullAndZero(Object obj) {
		if (obj != null) {
			if (obj instanceof Double) {
				Double objDouble = (Double) obj;
				return (objDouble.compareTo(DOUBLE_ZERO) != 0);
			}

			if (obj instanceof BigDecimal) {
				BigDecimal objDouble = (BigDecimal) obj;
				return (objDouble.compareTo(BigDecimal.ZERO) != 0);
			}

			if (obj instanceof Long) {
				Long objDouble = (Long) obj;
				return (objDouble != 0);
			}

		}
		return false;
	}

	public static Boolean isNotNullAndEmpty(Collection<?> obj) {
		if (isNotNull(obj)) {
			if (obj instanceof Set<?>) {
				Set<?> objDouble = (Set<?>) obj;
				return !objDouble.isEmpty();
			}
		}
		return false;
	}

	public static Boolean isNull(Object obj) {
		return (obj == null);
	}

	public static Boolean isNotNullAndTrue(Boolean obj) {
		return !isNull(obj) && obj;
	}

	public static String md5(String str) {
		String enc = str;
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.update(str.getBytes(), 0, str.length());
			enc = new BigInteger(1, m.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return enc;
	}

	public static int randomPlus() {
		int min = 100;
		int max = 999;

		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}

	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	public static boolean isValidMobileNumber(String mobile) {
		boolean result = true;
		if (!mobile.startsWith("08")) {
			result = false;
		}
		if (mobile.replace("-", "").length() < 10) {
			result = false;
		}
		return result;
	}

	public static int getIndex(Set<? extends Object> set, Object value) {
		int result = 0;
		for (Object entry : set) {
			if (entry.equals(value))
				return result;
			result++;
		}
		return -1;
	}

	public static List<String> removeDuplicate(String obj) {
		List<String> ht = new ArrayList<>();
		String name = obj;
		int indexOf = ht.indexOf(name);
		if (indexOf == -1)
			ht.add(name);
		else
			ht.add("");
		return ht;
	}

	public String getIdx(List<String> ht, int index) {
		return ht.get(index);
	}

	// public static void main(String[] args) {
	// System.out.println(CommonUtil.isValidEmailAddress("asd"));
	// }
}
