package id.go.menlhk.bp2sdm.spjk.ref;

public enum AccountLoginInfo {
	ACCOUNT(0), SOURCE(1), ACCOUNTNAME(2);

	private int id;

	private AccountLoginInfo(final int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}

}
