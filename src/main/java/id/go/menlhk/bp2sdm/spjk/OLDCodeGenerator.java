package id.go.menlhk.bp2sdm.spjk;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

public class OLDCodeGenerator {

	private static boolean append = true;

	final static String host = "localhost";
	final static String port = "3306";
	final static String database = "spjkdb";
	final static String username = "root";
	final static String password = "";
	final static String tableTarget = "mapping_fungsi_kunci_detil";
	final static String tablePrefix = "tbl_";
	final static String packageName = "id.go.menlhk.bp2sdm.spjk";

	public static void main(String[] args) {

		String model = "";
		model += "package " + packageName + ".domain;\n\n\n";
		boolean usingDate = true;

		String mapper = "";
		mapper += "package " + packageName + ".mapper;\n\n\n";

		String controller = "";

		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?" + "user="
					+ username + "&password=" + password + "");

			// Do something with the Connection
			// System.out.println(conn);

			String selectTableSQL = "SELECT COLUMN_NAME AS `Field`, COLUMN_TYPE AS `Type`, IS_NULLABLE AS `NULL`, \n"
					+ "       COLUMN_KEY AS `Key`, COLUMN_DEFAULT AS `Default`, EXTRA AS `Extra`, TABLE_NAME AS `table`\n"
					+ "FROM information_schema.COLUMNS  \n" + "WHERE TABLE_SCHEMA = '" + database
					+ "' AND TABLE_NAME LIKE '" + tableTarget + "';";
			System.out.println(selectTableSQL);
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery(selectTableSQL);
			String tmp = "";
			String sqlUpdate = "";
			String sqlInsert = "";
			String sqlInsertValues = "";
			String type = "";
			String javaType = "";
			String requestParam = "";
			String setParam = "";
			String columnMapping = "";
			String modelAttributes = "";
			String modelAttributesRelation = "";
			List<String> listNamaObjectRelation = new ArrayList<String>();
			String field = "";
			String table = "";
			String classMethod = "";
			String classMethodRelation = "";
			String classMethodRelationMapper = "";
			String fieldCap = "";
			String str = "";

			// Iterasi semua baris dari select lalu print
			while (rs.next()) {
				System.out.println(rs.getString("FIELD") + " | " + rs.getString("TYPE"));
				if (rs.getString("TYPE").startsWith("varchar") || rs.getString("TYPE").startsWith("text")
						|| rs.getString("TYPE").startsWith("enum")) {
					type = ":VARCHAR";
					javaType = "String";
				} else if (rs.getString("TYPE").startsWith("decimal") || rs.getString("TYPE").startsWith("int")
						|| rs.getString("TYPE").startsWith("bigint") || rs.getString("TYPE").startsWith("tinyint")) {
					type = ":NUMERIC";
					if (rs.getString("TYPE").startsWith("bigint")) {
						javaType = "Long";
					} else if (rs.getString("TYPE").startsWith("tinyint")) {
						javaType = "Byte";
					} else if (rs.getString("TYPE").startsWith("decimal")) {
						javaType = "Double";
					} else {
						javaType = "Integer";
					}

				} else if (rs.getString("TYPE").startsWith("boolean")) {
					type = ":BOOLEAN";
					javaType = "Boolean";

				} else if (rs.getString("TYPE").equals("date")) {
					type = ":DATE";
					javaType = "Date";
					usingDate = true;
				} else if (rs.getString("TYPE").equals("datetime")) {
					type = ":TIMESTAMP";
					javaType = "Date";
					usingDate = true;
				} else {
					type = "";
				}

				fieldCap = WordUtils.capitalizeFully(rs.getString("FIELD").replace("_", " ")).replace(" ", "");
				field = StringUtils.uncapitalize(fieldCap);
				table = WordUtils.capitalizeFully(tmp.replace(tablePrefix, "").replace("_", " ")).replace(" ", "");

				if (rs.getString("FIELD").equals("id") || rs.getString("FIELD").startsWith("id_")
						|| rs.getString("FIELD").startsWith("user_")) {
					javaType = "String";
					type = ":VARCHAR";
				}

				String requestType = "Optional<" + javaType + ">";
				if (rs.getString("TYPE").equals("date")) {
					requestType = " @DateTimeFormat(pattern = \"dd-MM-yyyy\") Optional<" + javaType + ">";
				} else if (rs.getString("TYPE").startsWith("decimal")) {
					requestType = " @NumberFormat(pattern = \"#,###.##\") Optional<" + javaType + ">";
				}

				if (tmp.equals("") || !rs.getString("TABLE").equals(tmp)) {
					if (!tmp.equals("")) {
						sqlInsert += ")";
						sqlInsertValues += ")";
						// System.out.println("@Insert(\""+sqlInsert+sqlInsertValues+"\")");
						// System.out.println("void insert("+table+"
						// "+StringUtils.uncapitalize(table)+");\n");
						// System.out.println("@Update(\""+sqlUpdate+" WHERE id=#{id}\")");
						// System.out.println("void update("+table+"
						// "+StringUtils.uncapitalize(table)+");\n");
						// System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE ${clause}\")");
						// System.out.println("void deleteBatch(QueryParameter param);\n");
						// System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE id=#{id}\")");
						// System.out.println("void delete("+table+"
						// "+StringUtils.uncapitalize(table)+");\n");
						//
						// System.out.println("List<"+table+"> getList(QueryParameter param);\n");
						// System.out.println(table+" getEntity(String id);\n");
						// System.out.println("long getCount(QueryParameter param);\n");
						//
						// System.out.println(requestParam);
						// System.out.println(columnMapping);
						// System.out.println(modelAttributes);
						// System.out.println(classMethod);

						str += "@Insert(\"" + sqlInsert + sqlInsertValues + "\")\n";
						str += "void insert(" + table + " " + StringUtils.uncapitalize(table) + ");\n\n";
						str += "@Update(\"" + sqlUpdate + " WHERE id_" + tmp.replace(tablePrefix, "") + "=#{id" + table
								+ "}\")\n";
						str += "void update(" + table + " " + StringUtils.uncapitalize(table) + ");\n\n";
						str += "@Delete(\"DELETE FROM " + tmp + " WHERE ${clause}\")\n";
						str += "void deleteBatch(QueryParameter param);\n\n";
						str += "@Delete(\"DELETE FROM " + tmp + " WHERE id_" + tmp.replace(tablePrefix, "") + "=#{id"
								+ table + "}\")\n";
						str += "void delete(" + table + " " + StringUtils.uncapitalize(table) + ");\n\n";
						str += "@Delete(\"INSERT INTO " + tmp + " SELECT * FROM " + tmp + "_test\")\n";
						str += "void copyTest();\n\n";

						str += "List<" + table + "> getList(QueryParameter param);\n\n";
						str += table + " getEntity(String id);\n\n";
						str += "long getCount(QueryParameter param);\n\n";
						str += "int getNewId();\n\n";

						str += setParam + "\n";
						str += requestParam + "\n";
						str += columnMapping + "\n";
						str += modelAttributes + "\n";
						str += "public " + table + "() {\n\n}" + "\n";
						str += "public " + table + "(String id) {\n\tthis.id" + table + " = id;\n}\n\n";
						str += classMethod + "\n";
						str += "\n\n\n/**********************************************************************/\n\n";

						columnMapping = "";
						modelAttributes = "";
						classMethod = "";
					}

					tmp = rs.getString("TABLE");
					sqlUpdate = "UPDATE " + rs.getString("TABLE") + " SET " + rs.getString("FIELD") + "=#{" + field
							+ type + "}";

					sqlInsert = "INSERT INTO " + rs.getString("TABLE") + " (" + rs.getString("FIELD");

					sqlInsertValues = " VALUES (#{" + field + type + "}";

					if (requestType.contains("Optional")) {
						setParam = "if(" + field + ".isPresent()) data.set" + fieldCap + "(" + field + ".get());\n";
					} else {
						setParam = "data.set" + fieldCap + "(" + field + ");\n";
					}

					requestParam = "@RequestParam(\"" + rs.getString("FIELD") + "\") " + requestType + " " + field
							+ ",\n";
				} else {

					sqlUpdate += ", " + rs.getString("FIELD") + "=#{" + field + type + "}";

					sqlInsert += ", " + rs.getString("FIELD");
					sqlInsertValues += ", #{" + field + type + "}";

					if (requestType.contains("Optional")) {
						setParam += "if(" + field + ".isPresent()) data.set" + fieldCap + "(" + field + ".get());\n";
					} else {
						setParam += "data.set" + fieldCap + "(" + field + ");\n";
					}

					requestParam += "@RequestParam(\"" + rs.getString("FIELD") + "\") " + requestType + " " + field
							+ ",\n";
				}

				if (javaType.equals("Boolean")) {
					classMethod += "@JsonProperty(" + rs.getString("FIELD").toUpperCase() + ")\npublic " + javaType
							+ " is" + fieldCap + "() {\n\treturn " + field + ";\n}\n\n";
				} else {
					classMethod += "@JsonProperty(" + rs.getString("FIELD").toUpperCase() + ")\npublic " + javaType
							+ " get" + fieldCap + "() {\n\treturn " + field + ";\n}\n\n";
				}

				classMethod += "public void set" + fieldCap + "(" + javaType + " " + field + ") {\n\tthis." + field
						+ " = " + field + ";\n}\n\n";
				modelAttributes += "private " + javaType + " " + field + ";\n";
				columnMapping += "public static final String " + rs.getString("FIELD").toUpperCase() + " = \""
						+ rs.getString("FIELD") + "\";\n";
				// System.out.println("is fieldCap:"+fieldCap);
				// System.out.println("is fieldCap.length:"+fieldCap.length());
				// System.out.println("is table.length:"+table.length());
				//
				// System.out.println("is this:"+fieldCap.substring(fieldCap.length() -
				// table.length(), fieldCap.length()));

				if (!table.equals("") && fieldCap.substring(0, 2).equals("Id")
						&& fieldCap.substring(fieldCap.length() - table.length(), fieldCap.length()).equals(table)) {
					String namaClass = fieldCap.substring(2, field.length() - table.length());
					String namaObjectClass = namaClass.substring(0, 1).toLowerCase() + namaClass.substring(1);
					listNamaObjectRelation.add(namaObjectClass);
					modelAttributesRelation += "private " + namaClass + " " + namaObjectClass + ";\n";
					classMethodRelation += "public void set" + namaClass + "(" + namaClass + " " + namaObjectClass
							+ ") {\n\tthis." + namaObjectClass + " = " + namaObjectClass + ";\n}\n\n";
					classMethodRelation += "public " + namaClass + " get" + namaClass + "() {\n\treturn "
							+ namaObjectClass + ";\n}\n\n";
					classMethodRelationMapper += "";
				}

			}

			modelAttributes += "\n" + modelAttributesRelation;
			classMethod += "\n" + classMethodRelation;

			/*
			 * System.out.println("modelAttributesRelation");
			 * System.out.println(modelAttributesRelation);
			 * 
			 * System.out.println("classMethodRelation");
			 * System.out.println(classMethodRelation);
			 */

			sqlInsert += ")";
			sqlInsertValues += ")";
			// System.out.println("@Insert(\""+sqlInsert+sqlInsertValues+"\")");
			// System.out.println("void insert("+table+"
			// "+StringUtils.uncapitalize(table)+");\n");
			// System.out.println("@Update(\""+sqlUpdate+" WHERE id=#{id}\")");
			// System.out.println("void update("+table+"
			// "+StringUtils.uncapitalize(table)+");\n");
			// System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE ${clause}\")");
			// System.out.println("void deleteBatch(QueryParameter param);\n");
			// System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE id=#{id}\")");
			// System.out.println("void delete("+table+"
			// "+StringUtils.uncapitalize(table)+");\n");
			//
			// System.out.println("List<"+table+"> getList(QueryParameter param);\n");
			// System.out.println(table+" getEntity(String id);\n");
			// System.out.println("long getCount(QueryParameter param);\n");
			//
			// System.out.println(requestParam);
			// System.out.println(columnMapping);
			// System.out.println(modelAttributes);
			// System.out.println(classMethod);

			mapper += "import java.util.List;\n\n";

			mapper += "import org.apache.ibatis.annotations.Delete;\n";
			mapper += "import org.apache.ibatis.annotations.Insert;\n";
			mapper += "import org.apache.ibatis.annotations.Update;\n\n";

			mapper += "import " + packageName + ".core.QueryParameter;\n";
			mapper += "import " + packageName + ".domain." + table + ";\n\n";

			mapper += "public interface " + table + "Mapper {\n\n";

			String mapperMethods = "\t";
			mapperMethods += "/********************************** - Begin Generate - ************************************/\n\n";
			mapperMethods += "@Insert(\"" + sqlInsert + sqlInsertValues + "\")\n";
			mapperMethods += "void insert(" + table + " " + StringUtils.uncapitalize(table) + ");\n\n";
			mapperMethods += "@Update(\"" + sqlUpdate + " WHERE id_" + tmp.replace(tablePrefix, "") + "=#{id" + table
					+ "}\")\n";
			mapperMethods += "void update(" + table + " " + StringUtils.uncapitalize(table) + ");\n\n";
			mapperMethods += "@Delete(\"DELETE FROM " + tmp + " WHERE ${clause}\")\n";
			mapperMethods += "void deleteBatch(QueryParameter param);\n\n";
			mapperMethods += "@Delete(\"DELETE FROM " + tmp + " WHERE id_" + tmp.replace(tablePrefix, "") + "=#{id"
					+ table + "}\")\n";
			mapperMethods += "void delete(" + table + " " + StringUtils.uncapitalize(table) + ");\n\n";
			mapperMethods += "@Delete(\"INSERT INTO " + tmp + " SELECT * FROM " + tmp + "_test\")\n";
			mapperMethods += "void copyTest();\n\n";

			mapperMethods += "List<" + table + "> getList(QueryParameter param);\n\n";
			mapperMethods += table + " getEntity(String id);\n\n";
			mapperMethods += "long getCount(QueryParameter param);\n\n";
			mapperMethods += "int getNewId();\n\n";
			mapperMethods += "/********************************** - End Generate - ************************************/";
			mapperMethods = addTab(mapperMethods);

			mapper += mapperMethods + "\n\n}\n";

			str += "\n\n" + mapper;
			str += setParam + "\n";
			String controllerRequestParam = requestParam;
			str += requestParam + "\n";

			str += "\n/**********************************************************************/\n\n";
			if (usingDate)
				model += "import java.util.Date;\n";
			model += "\nimport com.fasterxml.jackson.annotation.JsonProperty;\n\n";
			model += "public class " + table + " {\n\n";
			String modelAttributesAndMethods = "\t";
			modelAttributesAndMethods += "/********************************** - Begin Generate - ************************************/\n";
			modelAttributesAndMethods += addTab("\n" + columnMapping + "\n");
			modelAttributesAndMethods += addTab(modelAttributes + "\n");
			modelAttributesAndMethods += addTab("public " + table + "() {\n\n}\n");
			modelAttributesAndMethods += addTab(
					"public " + table + "(String id) {\n\tthis.id" + table + " = id;\n}\n\n");
			modelAttributesAndMethods += addTab(classMethod);
			modelAttributesAndMethods += "/********************************** - End Generate - ************************************/";
			model += modelAttributesAndMethods;
			model += "\n\n}\n";
			str += model;
			str += "\n/**********************************************************************/\n\n";

			// System.out.println(str);
			// generateModel(table, model, modelAttributesAndMethods);
			// generateMapper(table, mapper, mapperMethods);
			generateXMLMapper(table, listNamaObjectRelation);
			// generateController(table, controller, controllerRequestParam);

		} catch (Exception ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private static void generateModel(String modelName, String content, String modelAttributesAndMethods)
			throws IOException {
		String text = "";
		File file = new File(getSourcePath("domain", modelName));
		if (!file.exists() && !file.isDirectory()) {
			StringBuilder contentBuilder = new StringBuilder();
			try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8)) {
				stream.forEach(s -> {
					if (s.trim().equals(
							"/********************************** - Begin Generate - ************************************/"
									.trim())) {
						setAppend(false);
						contentBuilder.append(modelAttributesAndMethods).append("\n");
					}
					if (isAppend()) {
						contentBuilder.append(s).append("\n");
					}
					if (s.trim().equals(
							"/********************************** - End Generate - ************************************/"
									.trim())) {
						setAppend(true);
					}
				});
				text = contentBuilder.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			text = content;
		}
		System.out.println(getSourcePath("domain", modelName));
		BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
		writer.write(text);
		writer.close();
	}

	private static void generateMapper(String modelName, String content, String mapperMethods) throws IOException {
		String text = "";
		File file = new File(getSourcePath("mapper", modelName));
		if (!file.exists() && !file.isDirectory()) {
			StringBuilder contentBuilder = new StringBuilder();
			try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8)) {
				stream.forEach(s -> {
					if (s.trim().equals(
							"/********************************** - Begin Generate - ************************************/"
									.trim())) {
						setAppend(false);
						contentBuilder.append(mapperMethods).append("\n");
						System.out.println("BEGIN KETEMU");
					}
					if (isAppend()) {
						contentBuilder.append(s).append("\n");
					}
					if (s.trim().equals(
							"/********************************** - End Generate - ************************************/"
									.trim())) {
						System.out.println("END KETEMU");
						setAppend(true);
					}
				});
				text = contentBuilder.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			text = content;
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
		writer.write(text);
		writer.close();

		File xmlBase = new File(getSourcePath("xml", "base"));
		File xmlFile = new File(getSourcePath("xml", modelName));
		String textXml = "";
		if (!xmlBase.exists() && !xmlBase.isDirectory()) {
			StringBuilder contentBuilder = new StringBuilder();
			try (Stream<String> stream = Files.lines(Paths.get(xmlBase.getAbsolutePath()), StandardCharsets.UTF_8)) {
				stream.forEach(s -> contentBuilder.append(s).append("\n"));
				textXml = contentBuilder.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		textXml.replace("<!-- Relation -->", "Halo halo\n asdf \nmsdf");

		BufferedWriter writerXML = new BufferedWriter(new FileWriter(xmlFile.getAbsolutePath()));
		writerXML.write(textXml);
		writerXML.close();

	}

	private static void generateXMLMapper(String modelName, List<String> listNamaObjectRelation) throws IOException {

		/* -- Begin mybatis-config -- */
		File myBatisConfig = new File(getMyBatisConfig());
		if (myBatisConfig.exists() && !myBatisConfig.isDirectory()) {
			StringBuilder contentBuilder = new StringBuilder();
			StringBuilder finalContentBuilder = new StringBuilder();
			try (Stream<String> stream = Files.lines(Paths.get(myBatisConfig.getAbsolutePath()),
					StandardCharsets.UTF_8)) {
				setAppend(false);
				stream.forEach(s -> {
					if (s.trim().equals("<!-- Begin Generate -->")) {
						setAppend(true);
						return;
					}
					if (s.trim().equals("<!-- End Generate -->")) {
						setAppend(false);
						return;
					}
					if (isAppend())
						contentBuilder.append(s).append("\n");
				});
				if (!contentBuilder.toString().contains("<typeAlias alias=\"" + getObjectName(modelName) + "\" type=\""
						+ packageName + ".domain." + modelName + "\" />")) {
					contentBuilder.append("\t\t<typeAlias alias=\"" + getObjectName(modelName) + "\" type=\""
							+ packageName + ".domain." + modelName + "\" />").append("\n");
				}
			}

			try (Stream<String> stream = Files.lines(Paths.get(myBatisConfig.getAbsolutePath()),
					StandardCharsets.UTF_8)) {
				setAppend(true);
				stream.forEach(s -> {
					if (s.trim().equals("<!-- Begin Generate -->")) {
						finalContentBuilder.append(s).append("\n");
						finalContentBuilder.append(contentBuilder.toString());
						setAppend(false);
					}
					if (s.trim().equals("<!-- End Generate -->")) {
						finalContentBuilder.append(s).append("\n");
						setAppend(true);
						return;
					}
					if (isAppend())
						finalContentBuilder.append(s).append("\n");
				});
			}

			BufferedWriter writer = new BufferedWriter(new FileWriter(myBatisConfig.getAbsolutePath()));
			writer.write(finalContentBuilder.toString());
			writer.close();
		}
		/* -- End mybatis-config -- */

		String text = "";
		File base = new File(getSourcePath("xml", "base"));
		File file = new File(getSourcePath("xml", modelName));

		String generate = "";
		for (String o : listNamaObjectRelation) {
			generate += "\t\t<association property=\"" + o + "\" autoMapping=\"true\" javaType=\"" + getSnakeCase(o)
					+ "\" />\n";
		}
		final String finalGenerate = generate;
		StringBuilder contentBuilder = new StringBuilder();

		if (!file.exists() && !file.isDirectory()) { // Update
			try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8)) {
				stream.forEach(s -> {
					if (s.trim().equals("<!-- Begin Generate -->".trim())) {
						setAppend(false);
						contentBuilder.append(s).append("\n").append(finalGenerate);
					}
					if (s.trim().equals("<!-- End Generate -->".trim()))
						setAppend(true);
					if (isAppend())
						contentBuilder.append(s).append("\n");
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (base.exists() && !base.isDirectory()) { // Add
			try (Stream<String> stream = Files.lines(Paths.get(base.getAbsolutePath()), StandardCharsets.UTF_8)) {
				stream.forEach(s -> {
					if (s.trim().equals("<!-- Begin Generate -->".trim())) {
						setAppend(false);
						contentBuilder.append(s).append("\n").append(finalGenerate);
					}
					if (s.trim().equals("<!-- End Generate -->".trim()))
						setAppend(true);
					if (isAppend())
						contentBuilder.append(s).append("\n");
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		text = contentBuilder.toString();
		text = text.replace("{classMapperName}", packageName + ".mapper." + modelName + "Mapper");
		text = text.replace("{type}", modelName.toLowerCase());
		text = text.replace("{id_kolom}", "id_" + modelName.toLowerCase());
		text = text.replace("{map}", modelName.toLowerCase() + "_map");
		text = text.replace("{table_name}", tableTarget);
		text = text.replace("{table_prefix}", tablePrefix);

		BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
		writer.write(text);
		writer.close();
	}

	private static void generateController(String modelName, String content, String mapperMethods) throws IOException {
		String text = "";
		File file = new File(getSourcePath("mapper", modelName));
		if (!file.exists() && !file.isDirectory()) {
			StringBuilder contentBuilder = new StringBuilder();
			try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8)) {
				stream.forEach(s -> {
					if (s.trim().equals(
							"/********************************** - Begin Generate - ************************************/"
									.trim())) {
						setAppend(false);
						contentBuilder.append(mapperMethods).append("\n");
						System.out.println("BEGIN KETEMU");
					}
					if (isAppend()) {
						contentBuilder.append(s).append("\n");
					}
					if (s.trim().equals(
							"/********************************** - End Generate - ************************************/"
									.trim())) {
						System.out.println("END KETEMU");
						setAppend(true);
					}
				});
				text = contentBuilder.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			text = content;
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
		writer.write(text);

		writer.close();
	}

	private static String getSourcePath(String packageName, String className) {
		String CodeGenerator = new OLDCodeGenerator().getClass().getName();
		CodeGenerator = CodeGenerator.replace("CodeGenerator", "");
		CodeGenerator = CodeGenerator.replace(".", "/");
		String path = System.getProperty("user.dir") + "/src/main/java/" + CodeGenerator;
		// System.out.println("ini packageName : "+packageName);
		// System.out.println("ini className : "+className);
		// System.out.println("ini codegenerator name : "+new
		// CodeGenerator().getClass().getName());
		if (packageName.equals("mapper"))
			className += "Mapper";
		if (packageName.equals("xml")) {
			path = path.replace("/java/", "/resources/");
			return path.split("services")[0] + "mybatis" + "/" + getSnakeCase(className) + ".xml";
		} else {
			return path.split("services")[0] + packageName + "/" + className + ".java";
		}
	}

	public static String getMyBatisConfig() {
		return System.getProperty("user.dir") + "/src/main/resources/mybatis-config.xml";
	}

	public static String getSnakeCase(String text) {
		return text.replaceAll("([a-z])([A-Z]+)", "$1_$2").toLowerCase();
	}

	public static String getObjectName(String objectName) {
		return objectName.substring(0, 1).toLowerCase() + objectName.substring(1);
	}

	private static String addTab(String txt) {
		return txt.replace("\n", "\n\t");
	}

	public static boolean isAppend() {
		return append;
	}

	public static void setAppend(boolean append) {
		OLDCodeGenerator.append = append;
	}

}
