package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Propinsi {
	private String idPropinsi;
	private String namaPropinsi;

	public Propinsi() {

	}

	public Propinsi(String idPropinsi) {
		this.idPropinsi = idPropinsi;
	}

	@JsonProperty("id_propinsi")
	public String getIdPropinsi() {
		return idPropinsi;
	}

	public void setIdPropinsi(String idPropinsi) {
		this.idPropinsi = idPropinsi;
	}

	@JsonProperty("nama_propinsi")
	public String getNamaPropinsi() {
		return namaPropinsi;
	}

	public void setNamaPropinsi(String namaPropinsi) {
		this.namaPropinsi = namaPropinsi;
	}

}
