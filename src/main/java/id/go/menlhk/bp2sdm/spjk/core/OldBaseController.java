/*package id.go.menlhk.bp2sdm.spjk.core;

import java.util.Base64;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.go.menlhk.bp2sdm.spjk.domain.Login;
import id.go.menlhk.bp2sdm.spjk.helper.BasicAuthRestTemplate;
import id.go.menlhk.bp2sdm.spjk.mapper.LoginMapper;
import id.go.menlhk.bp2sdm.spjk.services.AppManagerService;

@Configuration
public abstract class OldBaseController {

	private Pagination pagination = new Pagination();

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	@Value("${security.oauth2.client.id}")
	private String clientId;

	@Value("${security.oauth2.client.secret}")
	private String clientSecret;

	@Value("${app.url.sso}")
	private String ssoEndpointUrl;

	@Value("${app.cookie.name}")
	private String cookieName;

	@Autowired
	private AppManagerService appManagerService;

	protected String getAccessToken(HttpServletRequest request) throws Exception {
		String accessTokenValue = "";
		Cookie[] cookies = request.getCookies();
		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i].getName().equalsIgnoreCase(cookieName)) {
				accessTokenValue = cookies[i].getValue();
			}

		}
		return accessTokenValue;
	}

	protected boolean passCookieAuthorization(HttpServletRequest request) throws Exception {
		String accessTokenValue = "";
		Cookie[] cookies = request.getCookies();
		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i].getName().equalsIgnoreCase(cookieName)) {
				accessTokenValue = cookies[i].getValue();
			}

		}
		if (!accessTokenValue.equals("")) {
			BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
			ResponseEntity<String> result = null;
			String resp = "";
			boolean pass = true;
			try {
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);

				MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
				map.add("token", accessTokenValue);

				result = restTemplate.postForEntity(ssoEndpointUrl + "/oauth/check_token", map, String.class);
				// Token token = result.getBody();
				// System.out.println(token.getAccessToken());

				resp = result.getBody();
			} catch (HttpClientErrorException e) {
				resp = e.getResponseBodyAsString();
				pass = false;
			} catch (Exception e) {
				// TODO: handle exception
				pass = false;
			}

			return pass;
		}
		return false;
	}

	protected boolean passTokenAuthorization(HttpServletRequest request, Optional<String> token) throws Exception {
		String accessTokenValue = "";
		Cookie[] cookies = request.getCookies();
		for (int i = 0; i < cookies.length; i++) {
			if (cookies[i].getName().equalsIgnoreCase(cookieName)) {
				accessTokenValue = cookies[i].getValue();
			}

		}
		if (accessTokenValue.equals("") && token.isPresent()) {
			accessTokenValue = token.get();
		}
		System.out.println("token: " + accessTokenValue);
		if (accessTokenValue != null && !accessTokenValue.equals("")) {
			BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
			ResponseEntity<String> result = null;
			String resp = "";
			boolean pass = true;
			try {
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);

				MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
				map.add("token", accessTokenValue);

				result = restTemplate.postForEntity(ssoEndpointUrl + "/oauth/check_token", map, String.class);
				// Token token = result.getBody();
				// System.out.println(token.getAccessToken());

				resp = result.getBody();
			} catch (HttpClientErrorException e) {
				resp = e.getResponseBodyAsString();
				pass = false;
			} catch (Exception e) {
				// TODO: handle exception
				pass = false;
			}

			return pass;
		}
		return false;
	}

	public Map<String, Object> getAccessTokenMap(HttpServletRequest request) throws Exception {
		String accessToken = getAccessToken(request);
		// System.out.println("token: " + accessToken);
		String[] tokenArr = accessToken.split("\\.");
		String payload = new String(Base64.getDecoder().decode(tokenArr[1]));
		ObjectMapper mapper = new ObjectMapper();
		// System.out.println(payload);
		// convert JSON string to Map
		return mapper.readValue(payload, new TypeReference<Map<String, Object>>() {
		});
	}

}
*/