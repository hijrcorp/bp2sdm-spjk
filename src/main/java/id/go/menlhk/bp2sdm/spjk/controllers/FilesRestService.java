package id.go.menlhk.bp2sdm.spjk.controllers;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import id.go.menlhk.bp2sdm.spjk.common.FileTypeSupport;
import id.go.menlhk.bp2sdm.spjk.core.CodeGenerator;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.repo.StorageService;



@RestController
@RequestMapping("/files")
public class FilesRestService extends BaseController {
	@Autowired
	
	@Qualifier("fileSystemStorage")
	protected StorageService fileSystemService;
	
//	@Autowired
//	private UploadMapper uploadMapper;
//	
//	@RequestMapping(value="/{id}/delete", method = RequestMethod.POST, produces = "application/json")
//	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<id.co.lhk.hedes.helper.ResponseWrapper> delete(
//			@PathVariable String id
//    		) throws Exception {
//		ResponseWrapper resp = new ResponseWrapper();
//		Upload data = uploadMapper.getEntity(id);
//		uploadMapper.delete(data);
//		resp.setData(data);
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
	
	
//	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
//	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseWrapperList getList(
//    		@RequestParam("filter_keyword") Optional<String> filter_keyword,
//    		@RequestParam("filter_jenis") Optional<String> filter_jenis,
//    		@RequestParam("filter_referensi") Optional<String> filter_referensi,
//    		HttpServletRequest request,
//			HttpServletResponse response
//    		)throws Exception {
//
//		QueryParameter param = new QueryParameter();
//		if(filter_jenis.isPresent()) param.setClause(param.getClause() + " AND "+ Upload.JENIS_REFERENSI_UPLOAD + " = '"+filter_jenis.get()+"'");
//		if(filter_referensi.isPresent()) param.setClause(param.getClause() + " AND "+ Upload.REFERENSI_ID_UPLOAD + " = '"+filter_referensi.get()+"'");
//		if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+Upload.KETERANGAN_FILE_UPLOAD + " LIKE '%"+filter_keyword.get()+"%'");
//		
//		param.setLimit(null);
//		param.setOffset(null);
//		
//		ResponseWrapperList resp = new ResponseWrapperList();
//		List<Upload> data = uploadMapper.getList(param);
//		
//		resp.setData(data);
//		
//        return resp;
//    }
	
//	@RequestMapping(value = "/{filename}/", method = RequestMethod.GET)
//	public void loadFile(
//			@PathVariable String filename,
//			HttpServletRequest request,
//			HttpServletResponse response) throws Exception{
//		
//		String accessTokenValue = getCookieTokenValue(request);
//		boolean pass = isTokenValid(accessTokenValue);
//		
//		if(pass) {
//			response.setHeader("Content-Disposition", "filename=\""+filename+"\"");
//			
//			String[] partName = filename.split("\\.");
//			String ext = partName[partName.length-1];
//			String type = "application";
//			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
//				type="image";
//			}
//			
//			response.setContentType(type+"/"+ext);
//			
//			File file = fileSystemService.load(filename);
//			FileInputStream fileInputStream = new FileInputStream(file);
//			OutputStream responseOutputStream = response.getOutputStream();
//			int bytes;
//			while ((bytes = fileInputStream.read()) != -1) {
//				responseOutputStream.write(bytes);
//			}
//			fileInputStream.close();
//		}else {
//			String[] partName = filename.split("\\.");
//			String ext = partName[partName.length-1];
//			
//			filename = "accessDenied."+ext;
//			response.setHeader("Content-Disposition", "filename=\""+filename+"\"");
//			
//			
//			String type = "application";
//			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
//				type="image";
//			}
//			
//			response.setContentType(type+"/"+ext);
//			
//			File file = fileSystemService.load(filename);
//			FileInputStream fileInputStream = new FileInputStream(file);
//			OutputStream responseOutputStream = response.getOutputStream();
//			int bytes;
//			while ((bytes = fileInputStream.read()) != -1) {
//				responseOutputStream.write(bytes);
//			}
//			fileInputStream.close();
//		}
//	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public void loadFile(
			@PathVariable String id,
			@RequestParam("filename") String filename,
			@RequestParam("preview") Optional<String> preview,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		
			
			//String[] partName = filename.split("\\.");
			//String ext = partName[partName.length-1];
			String ext = FilenameUtils.getExtension(filename);
			String type = "application";
			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
				type="image";
				if(preview.isPresent()) {
					id = "small_"+id;
				}
			}else {
				if(preview.isPresent()) {
					response.setHeader("Content-Disposition", "filename=\""+filename+"\"");
				}else {
					response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
				}
			}
			
			response.setContentType(type+"/"+ext);
			
			File file = fileSystemService.load(id + "." + ext);
			FileInputStream fileInputStream = new FileInputStream(file);
			OutputStream responseOutputStream = response.getOutputStream();
			int bytes;
			while ((bytes = fileInputStream.read()) != -1) {
				responseOutputStream.write(bytes);
			}
			fileInputStream.close();
	}
	

	@RequestMapping(value = "/{filenames}/download", method = RequestMethod.GET)
	public void loadFiles(
			@PathVariable String filenames,
			HttpServletRequest request,
			HttpServletResponse response
			) throws Exception{
		
		getFile(filenames, request, response, "N/A");
	}
	
	private void getFile(String filename,
			HttpServletRequest request,
			HttpServletResponse response,
			String barcode)  throws Exception{
		String accessTokenValue = getCookieTokenValue(request, filename);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(true) {
			response.setHeader("Content-Disposition", "filename=\""+filename+"\"");
			
			String[] partName = filename.split("\\.");
			String ext = partName[partName.length-1];
			String type = "application";
			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
				type="image";
			}
			
			response.setContentType(type+"/"+ext);
			
			File file = fileSystemService.load(filename);
			FileInputStream fileInputStream = new FileInputStream(file);
			
			
			System.out.println(ext);
			if(ext.toLowerCase().equals("pdf")) {
				PdfReader pdfReader = new PdfReader(file.getAbsolutePath());
				
				File stamp = File.createTempFile(filename+"-stamp", ".pdf");
				FileOutputStream os = new FileOutputStream(stamp);

				PdfStamper pdfStamper = new PdfStamper(pdfReader, os);
				int size = 50;
				Image image = Image.getInstance(CodeGenerator.QRCode(barcode, size, "png").getAbsolutePath());

				for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {

					PdfContentByte content = pdfStamper.getUnderContent(i);

					Rectangle r = pdfReader.getPageSize(i);
					image.setAbsolutePosition((r.getWidth() - size), 0);

					content.addImage(image);
				}

				pdfStamper.close();
				
				fileInputStream = new FileInputStream(stamp);
			}
			
			
			OutputStream responseOutputStream = response.getOutputStream();
			int bytes;
			while ((bytes = fileInputStream.read()) != -1) {
				responseOutputStream.write(bytes);
			}
			fileInputStream.close();
		}else {
			String[] partName = filename.split("\\.");
			String ext = partName[partName.length-1];
			
			filename = "accessDenied."+ext;
			response.setHeader("Content-Disposition", "filename=\""+filename+"\"");
			
			
			String type = "application";
			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
				type="image";
			}
			
			response.setContentType(type+"/"+ext);
			
			File file = fileSystemService.load(filename);
			
			FileInputStream fileInputStream = new FileInputStream(file);
			
			
			
			OutputStream responseOutputStream = response.getOutputStream();
			int bytes;
			while ((bytes = fileInputStream.read()) != -1) {
				responseOutputStream.write(bytes);
			}
			fileInputStream.close();
		}
	}
}
