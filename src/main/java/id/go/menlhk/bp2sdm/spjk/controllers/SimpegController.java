package id.go.menlhk.bp2sdm.spjk.controllers;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import id.go.menlhk.bp2sdm.spjk.common.Utils;
import id.go.menlhk.bp2sdm.spjk.core.CommonUtil;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.SimpegJabatan;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.mapper.Eselon1Mapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SimpegJabatanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;



@RestController
@RequestMapping("/simpeg")
public class SimpegController {
	
	@Autowired
	private SimpegJabatanMapper simpegJabatanMapper;
	
	@Autowired
	private Eselon1Mapper eselon1Mapper;
	
	@Autowired
	private UnitAuditiMapper unitAuditiMapper;
	
	@Value("${app.url.simpeg_key}")
    private String simpegKey;
	
	@Value("${app.url.simpeg}")
    private String simpeg;

	@Value("${app.state}")
	private String appState;

	public String getResourceFileAsString(String fileName) throws Exception {
		File file = ResourceUtils.getFile("classpath:"+fileName);
        InputStream is = new FileInputStream(file);
	    
	    if (is != null) {
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	        return reader.lines().collect(Collectors.joining(System.lineSeparator()));
	    } else {
	        return null;
	    }
	}
    @RequestMapping(value="/pegawai", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getPegawai(
    		HttpServletRequest request,
    		@RequestParam("filter_nip") Optional<String> filter_nip
		)throws Exception {
    	
		ResponseWrapper resp = new ResponseWrapper();
		
        RestTemplate restTemplate = new RestTemplate();
        String url = "";
        if(filter_nip.isPresent() && !filter_nip.get().equals(""))
    		url = Utils.getSIMPEGURLPegawai(filter_nip.get());//simpeg + "?api_key="+ CommonUtil.md5("simpeg@77"+new SimpleDateFormat("dd-MM-Y").format(new Date())) + "&nip="+filter_nip.get();
        else{
        	resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
			resp.setMessage("NIP masih kosong!");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    	}
        System.out.println("url: " + url);
        
        String yourString = "";
		HttpStatus status = HttpStatus.OK;
		
		if(appState.equals("development")) {
			try {
				yourString = getResourceFileAsString("local_json/"+filter_nip.get()+".json");
				System.out.println("LOCAL");;
			} catch (Exception e) {
				resp = new ResponseWrapper();
		    	resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Data tidak ditemukan.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			} 
		}else {
			try {
				ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
				status = response.getStatusCode();
				yourString = response.getBody()!=null?response.getBody():"";
				System.out.println("ONLINE");
		    } catch (HttpClientErrorException | ResourceAccessException | HttpServerErrorException f) {
		    	resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
				resp.setMessage("Data tidak ditemukan.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
		}
		//
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;
		//
        if(status.equals(HttpStatus.OK)) {
        	try {
				System.out.println("kodeSimpeg "+mapper.readTree(yourString));
				root = mapper.readTree(yourString);
            	String kodeSimpeg = root.path("lok").textValue()+root.path("kdu1").textValue()+root.path("kdu2").textValue()+root.path("kdu3").textValue()+root.path("kdu4").textValue();
            	String kodeSimpegAlternatif = root.path("lok").textValue()+root.path("kdu1").textValue()+root.path("kdu2").textValue()+root.path("kdu3").textValue();
            	String kodeSimpegAlternatif3 = root.path("lok").textValue()+root.path("kdu1").textValue()+root.path("kdu2").textValue();
            	String kodeSimpegAlternatif4 = root.path("lok").textValue()+root.path("kdu1").textValue();
            	
            	((ObjectNode)root).put("kode_simpeg", kodeSimpeg);
            	
            	List<Map<String, Object>> lstRes = unitAuditiMapper.getListByKodeSimpeg(kodeSimpeg + "%");
            	if(lstRes.size()==0) lstRes = unitAuditiMapper.getListByKodeSimpeg(kodeSimpegAlternatif + "%");
            	if(lstRes.size()==0) lstRes = unitAuditiMapper.getListByKodeSimpeg(kodeSimpegAlternatif3 + "%");
            	if(lstRes.size()==0) lstRes = unitAuditiMapper.getListByKodeSimpeg(kodeSimpegAlternatif4 + "%");
            	if(lstRes.size() > 0) {
    				Map<String, Object> res = lstRes.get(0);
    				if(lstRes.size()==1) {
	    				((ObjectNode)root).put("id_unit_auditi", Integer.valueOf(res.get("id_unit_auditi_mapping").toString()));
	    				((ObjectNode)root).put("id_eselon1", Integer.valueOf(res.get("id_eselon1_mapping").toString()));
	    				((ObjectNode)root).put("nama_unit_auditi", res.get("nama_unit_auditi_mapping").toString());
    				}
    				
    				String pisahkanValueJabatan = root.path("jabatan").textValue().split("pada")[0].substring(0, root.path("jabatan").textValue().split("pada")[0].length()-1);
                	String pisahkanValueSatker = root.path("jabatan").textValue().split("pada")[1].substring(0, root.path("jabatan").textValue().split("pada")[1].length()-1);
                	System.out.println(">>>"+pisahkanValueJabatan+"<<<<");
                	pisahkanValueSatker=pisahkanValueSatker.split(",")[0];
                	System.out.println(">>>"+pisahkanValueSatker+"<<<<");
                	
                	QueryParameter paramsatker = new QueryParameter();
                	paramsatker.setLimit(10000);
                	//
            		String[] newua = pisahkanValueSatker.split(" ");
            		System.out.println("? "+newua[newua.length-2]+" "+newua[newua.length-1]);
                	//
                	for(UnitAuditi ua : unitAuditiMapper.getList(paramsatker)) {
                		String[] newnamaunitauditiString= ua.getNamaUnitAuditi().toString().split(" ");
                		//System.out.println((newua[newua.length-2]+" "+newua[newua.length-1].toString())+"=="+(newnamaunitauditiString[newnamaunitauditiString.length-2]+" "+newnamaunitauditiString[newnamaunitauditiString.length-1]));
                		if((newua[newua.length-2]+" "+newua[newua.length-1].toString()).equals((newnamaunitauditiString[newnamaunitauditiString.length-2]+" "+newnamaunitauditiString[newnamaunitauditiString.length-1]))) {
                			((ObjectNode)root).put("id_unit_auditi", Integer.valueOf(ua.getIdUnitAuditi()));
                			((ObjectNode)root).put("id_eselon1", Integer.valueOf(ua.getIdEselon1UnitAuditi()));
                			((ObjectNode)root).put("nama_unit_auditi", ua.getNamaUnitAuditi());
            			}
                	}
            	}
            	QueryParameter param = new QueryParameter();
        		param.setClause(param.getClause() + " AND "+ SimpegJabatan.JABATAN + " LIKE '%"+root.path("kode_jabatan").textValue()+"%'");
        		
        		List<SimpegJabatan> data = simpegJabatanMapper.getList(param);
        		for(SimpegJabatan o : data) {
        			((ObjectNode)root).put("kode_jabatan", o.getKode());
    				((ObjectNode)root).put("jabatan", o.getJabatan());
        		}
        		/*if(simpegJabatanMapper.getCount(param) == 0) {
        			SimpegJabatan jabatan = new SimpegJabatan();
        			jabatan.setKode(Utils.randomPlus()+"");
        			jabatan.setJabatan(root.path("kode_jabatan").textValue());
        			jabatan.setEselon(root.path("eselon").textValue());
        			simpegJabatanMapper.insert(jabatan);
        			((ObjectNode)root).put("kode_jabatan", jabatan.getKode());
        			System.out.println("the methode do save");
        		}*/
    			resp.setMessage("NIP ditemukan.");
			} catch (Exception e) {
				e.printStackTrace();
//				if(isJSON) resp.setMessage("NIP tidak ditemukan.");
//				else resp.setMessage(yourString);
			}
        	
        }else {
        	resp.setMessage("Request Gagal : "+"Dari Server.");
        }
        resp.setData(root);
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    @RequestMapping(value="/{nip}", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseWrapper getPegawaiOnline(
    		HttpServletRequest request,
    		@PathVariable String nip,
    		@RequestParam("key") Optional<String> key
		)throws Exception {
    	
		ResponseWrapper resp = new ResponseWrapper();
		
        RestTemplate restTemplate = new RestTemplate();
        if(!key.get().equals("hijrgs")){
        	resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
			resp.setMessage("KEY SALAH");
			return resp;
        }
        
        String url = simpegKey;
        if(!nip.equals("")) 
    		url = simpeg + "?api_key="+ CommonUtil.md5("simpeg@77"+new SimpleDateFormat("dd-MM-yyyy").format(new Date())) + "&nip="+nip;
        else{
        	resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
			resp.setMessage("NIP masih kosong!");
			return resp;
    	}
		
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		

		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;
        if(response.getStatusCode().equals(HttpStatus.OK)) {
    		String yourString = response.getBody()!=null?response.getBody():"";
			root = mapper.readTree(yourString);
	        resp.setData(root);
			resp.setMessage("NIP ditemukan.");
        }else {
        	resp.setMessage("Request Gagal : "+"Dari Server.");
        }
        return resp;
    }
    
}
