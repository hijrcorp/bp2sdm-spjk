//package id.go.menlhk.bp2sdm.spjk.repo;
//
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.stereotype.Repository;
//import org.springframework.util.FileSystemUtils;
//import org.springframework.web.multipart.MultipartFile;
//
//import com.itextpdf.text.Image;
//import com.itextpdf.text.Rectangle;
//import com.itextpdf.text.pdf.PdfContentByte;
//import com.itextpdf.text.pdf.PdfReader;
//import com.itextpdf.text.pdf.PdfStamper;
//import id.go.menlhk.bp2sdm.spjk.core.CodeGenerator;
//
//@Repository("fileSystemStorage")
//@Configuration
//public class FileSystemStorageImplold implements StorageService {
//
//	@Value("${app.folder}")
//	private String appFolder;
//
//	@Override
//	public File load(String filename) throws Exception {
//		// TODO Auto-generated method stub
//		System.out.println("load from file system");
//
//		File file = new File(appFolder + filename);
//		if (!file.exists()) {
//			file = new File(appFolder + "FileNotFound.pdf");
//		}
//		return file;
//	}
//
//	@Override
//	public void delete(String filename) throws Exception {
//		// TODO Auto-generated method stub
//		File file = new File(appFolder + filename);
//		if (file.exists()) {
//			// FileCopyUtils.copy(file, new File(trashFolder + filename));
//			FileSystemUtils.deleteRecursively(file);
//			System.out.println("delete from file system");
//		}
//	}
//
//	@Override
//	public void store(MultipartFile file, String filename) throws Exception {
//		// TODO Auto-generated method stub
//		System.out.println("store in file system: " + appFolder);
//
//		if (!file.isEmpty()) {
//			try {
//				byte[] bytes = file.getBytes();
//				BufferedOutputStream stream = new BufferedOutputStream(
//						new FileOutputStream(new File(appFolder + filename)));
//				stream.write(bytes);
//				stream.close();
//
//				PdfReader pdfReader = new PdfReader(appFolder + filename);
//
//				PdfStamper pdfStamper = new PdfStamper(pdfReader,
//						new FileOutputStream(appFolder + "stamp-" + filename));
//				int size = 50;
//				Image image = Image
//						.getInstance(CodeGenerator.QRCode("http://lhp.hijr.co.id", size, "png").getAbsolutePath());
//
//				for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
//
//					PdfContentByte content = pdfStamper.getUnderContent(i);
//
//					Rectangle r = pdfReader.getPageSize(i);
//					image.setAbsolutePosition((r.getWidth() - size), 0);
//
//					content.addImage(image);
//				}
//
//				pdfStamper.close();
//
//			} catch (Exception e) {
//
//			}
//		} else {
//			throw new Exception("Failed store file.");
//		}
//	}
//
//}
