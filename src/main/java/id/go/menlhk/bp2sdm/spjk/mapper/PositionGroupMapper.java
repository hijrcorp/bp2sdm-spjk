package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.common.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.model.PositionGroup;

@Mapper
public interface PositionGroupMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_position_group (id_position_group, name_position_group, organization_id_position_group) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{organizationId:VARCHAR})")
	void insert(PositionGroup positionGroup);

	@Update("UPDATE hijr_position_group SET id_position_group=#{id:VARCHAR}, name_position_group=#{name:VARCHAR}, organization_id_position_group=#{organizationId:VARCHAR} WHERE id_position_group=#{id}")
	void update(PositionGroup positionGroup);

	@Delete("DELETE FROM hijr_position_group WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_position_group WHERE id_position_group=#{id}")
	void delete(PositionGroup positionGroup);

	List<PositionGroup> getList(QueryParameter param);

	PositionGroup getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
