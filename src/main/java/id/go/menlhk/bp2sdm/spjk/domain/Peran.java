package id.go.menlhk.bp2sdm.spjk.domain;

import java.io.Serializable;

import id.go.menlhk.bp2sdm.spjk.core.Metadata;

public class Peran extends Metadata implements Serializable {
	private String kodePeran;
	private String namaPeran;

	public Peran() {
		super();
	}

	public Peran(int id) {
		super(id);
	}

	public String getKodePeran() {
		return kodePeran;
	}

	public void setKodePeran(String kodePeran) {
		this.kodePeran = kodePeran;
	}

	public String getNamaPeran() {
		return namaPeran;
	}

	public void setNamaPeran(String namaPeran) {
		this.namaPeran = namaPeran;
	}

}
