package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Pertanyaan;

public interface PertanyaanMapper {
	@Insert("INSERT INTO spjk_pertanyaan (id_pertanyaan, kode_pertanyaan, id_unit_kompetensi_pertanyaan, isi_pertanyaan, keterangan_pertanyaan, bobot_pertanyaan, kunci_jawaban_pertanyaan, tipe_soal_pertanyaan, durasi_pertanyaan, jenis_pertanyaan, id_bidang_teknis_pertanyaan, file_pertanyaan) VALUES (#{id:VARCHAR}, #{kode:VARCHAR}, #{idUnitKompetensi:VARCHAR}, #{isi:VARCHAR}, #{keterangan:VARCHAR}, #{bobot:NUMERIC}, #{kunciJawaban}, #{tipeSoal:VARCHAR}, #{durasi:NUMERIC}, #{jenis:VARCHAR}, #{idBidangTeknis:VARCHAR}, #{file:VARCHAR})")
	void insert(Pertanyaan pertanyaan);

	@Update("UPDATE spjk_pertanyaan SET id_pertanyaan=#{id:VARCHAR}, kode_pertanyaan=#{kode:VARCHAR}, id_unit_kompetensi_pertanyaan=#{idUnitKompetensi:VARCHAR}, isi_pertanyaan=#{isi:VARCHAR}, keterangan_pertanyaan=#{keterangan:VARCHAR}, bobot_pertanyaan=#{bobot:NUMERIC}, kunci_jawaban_pertanyaan=#{kunciJawaban}, tipe_soal_pertanyaan=#{tipeSoal:VARCHAR}, durasi_pertanyaan=#{durasi:NUMERIC}, jenis_pertanyaan=#{jenis:VARCHAR}, id_bidang_teknis_pertanyaan=#{idBidangTeknis:VARCHAR}, file_pertanyaan=#{file:VARCHAR} WHERE id_pertanyaan=#{id}")
	void update(Pertanyaan pertanyaan);

	@Delete("DELETE FROM spjk_pertanyaan WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_pertanyaan WHERE id_pertanyaan=#{id}")
	void delete(Pertanyaan pertanyaan);

	List<Pertanyaan> getList(QueryParameter param);

	Pertanyaan getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	//own
	List<Pertanyaan> getListNew(QueryParameter param);
	List<Pertanyaan> getListNewNonTeknis(QueryParameter param);
}
