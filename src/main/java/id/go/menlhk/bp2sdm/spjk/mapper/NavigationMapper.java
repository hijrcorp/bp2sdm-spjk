package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import id.go.menlhk.bp2sdm.spjk.domain.Navigation;

@Mapper
public interface NavigationMapper {
	@Select("select distinct a.* from tbl_navigation a inner join tbl_navigation_role b on a.id=b.nav_id inner join tbl_oauth_role c on b.role_id=c.id where a.group='LIST_LHP' and c.name in (${roles}) order by seq")
	List<Navigation> getListByUserId(@Param("roles") String roles);
	/*
	 * 
	 * @Select("select distinct a.* from tbl_navigation a inner join tbl_navigation_role b on a.id=b.nav_id inner join tbl_oauth_role c on b.role_id=c.id where c.name in ('ROLE_ADMIN') order by seq"
	 * ) List<Navigation> getListByUserId(@Param("roles") String roles);
	 */
}