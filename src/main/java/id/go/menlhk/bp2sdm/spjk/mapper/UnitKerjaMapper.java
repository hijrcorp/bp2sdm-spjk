package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKerja;

public interface UnitKerjaMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	@Insert("INSERT INTO survey_unit_kerja (id_unit_kerja, kode_unit_kerja, nama_unit_kerja, alamat_unit_kerja, telepon_unit_kerja) VALUES (#{idUnitKerja:VARCHAR}, #{kodeUnitKerja:VARCHAR}, #{namaUnitKerja:VARCHAR}, #{alamatUnitKerja:VARCHAR}, #{teleponUnitKerja:VARCHAR})")
	void insert(UnitKerja unitKerja);

	@Update("UPDATE survey_unit_kerja SET id_unit_kerja=#{idUnitKerja:VARCHAR}, kode_unit_kerja=#{kodeUnitKerja:VARCHAR}, nama_unit_kerja=#{namaUnitKerja:VARCHAR}, alamat_unit_kerja=#{alamatUnitKerja:VARCHAR}, telepon_unit_kerja=#{teleponUnitKerja:VARCHAR} WHERE id_unit_kerja=#{idUnitKerja}")
	void update(UnitKerja unitKerja);

	@Delete("DELETE FROM survey_unit_kerja WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM survey_unit_kerja WHERE id_unit_kerja=#{idUnitKerja}")
	void delete(UnitKerja unitKerja);

	@Delete("INSERT INTO survey_unit_kerja SELECT * FROM survey_unit_kerja_test")
	void copyTest();

	List<UnitKerja> getList(QueryParameter param);

	UnitKerja getEntity(String id);

	long getCount(QueryParameter param);

	int getNewId();

	/**********************************
	 * - End Generate -
	 ************************************/

}
