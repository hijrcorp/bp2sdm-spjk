package id.go.menlhk.bp2sdm.spjk;

public enum OrganizationReference {
	SURVEY("SURVEY", "1508628921431"), SPJK("SPJK", "2008628921431");

	private final String text;
	private final String value;

	/**
	 * @param text
	 */
	private OrganizationReference(final String text, final String value) {
		this.text = text;
		this.value = value;
	}

	public String value() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
