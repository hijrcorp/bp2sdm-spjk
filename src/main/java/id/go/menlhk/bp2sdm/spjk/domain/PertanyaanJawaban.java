package id.go.menlhk.bp2sdm.spjk.domain;

import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PertanyaanJawaban {

	public static final String ID = "id_pertanyaan_jawaban";
	public static final String ID_HEADER = "id_header_pertanyaan_jawaban";
	public static final String ORDER = "order_pertanyaan_jawaban";
	public static final String ISI = "isi_pertanyaan_jawaban";
	public static final String BOBOT = "bobot_pertanyaan_jawaban";
	public static final String FILE = "file_pertanyaan_jawaban";
	public static final String FILENAME = "filename_pertanyaan_jawaban";

	private String id;
	private String idHeader;
	private Integer order;
	private String isi;
	private Integer bobot;
	private String file;
	private String filename;

	public PertanyaanJawaban() {

	}

	public PertanyaanJawaban(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_header")
	public String getIdHeader() {
		return idHeader;
	}

	public void setIdHeader(String idHeader) {
		this.idHeader = idHeader;
	}

	@JsonProperty("order")
	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	@JsonProperty("isi")
	public String getIsi() {
		return isi;
	}

	public void setIsi(String isi) {
		this.isi = isi;
	}

	@JsonProperty("bobot")
	public Integer getBobot() {
		return bobot;
	}

	public void setBobot(Integer bobot) {
		this.bobot = bobot;
	}

	@JsonProperty("file")
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	@JsonProperty("filename")
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getIsiAndFilename() {
		String temp=isi;
		if(file!=null) {
			temp+="<br/>"+"<img src='"+"/spektra/files/"+this.id+"?filename="+file+"'/>";
		}
		return temp;
	}


	/**********************************************************************/
}
