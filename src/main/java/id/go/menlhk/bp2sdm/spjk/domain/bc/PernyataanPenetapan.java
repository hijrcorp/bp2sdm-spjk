package id.go.menlhk.bp2sdm.spjk.domain.bc;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PernyataanPenetapan {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	public static final String ID_PERNYATAAN_PENETAPAN = "id_pernyataan_penetapan";
	public static final String PERNYATAAN_ID_PERNYATAAN_PENETAPAN = "pernyataan_id_pernyataan_penetapan";
	public static final String TAHUN_PERNYATAAN_PENETAPAN = "tahun_pernyataan_penetapan";
	public static final String STATUS_PERNYATAAN_PENETAPAN = "status_pernyataan_penetapan";

	private String idPernyataanPenetapan;
	private String pernyataanIdPernyataanPenetapan;
	private String tahunPernyataanPenetapan;
	private Integer statusPernyataanPenetapan;

	public PernyataanPenetapan() {

	}

	public PernyataanPenetapan(String id) {
		this.idPernyataanPenetapan = id;
	}

	@JsonProperty(ID_PERNYATAAN_PENETAPAN)
	public String getIdPernyataanPenetapan() {
		return idPernyataanPenetapan;
	}

	public void setIdPernyataanPenetapan(String idPernyataanPenetapan) {
		this.idPernyataanPenetapan = idPernyataanPenetapan;
	}

	@JsonProperty(PERNYATAAN_ID_PERNYATAAN_PENETAPAN)
	public String getPernyataanIdPernyataanPenetapan() {
		return pernyataanIdPernyataanPenetapan;
	}

	public void setPernyataanIdPernyataanPenetapan(String pernyataanIdPernyataanPenetapan) {
		this.pernyataanIdPernyataanPenetapan = pernyataanIdPernyataanPenetapan;
	}

	@JsonProperty(TAHUN_PERNYATAAN_PENETAPAN)
	public String getTahunPernyataanPenetapan() {
		return tahunPernyataanPenetapan;
	}

	public void setTahunPernyataanPenetapan(String tahunPernyataanPenetapan) {
		this.tahunPernyataanPenetapan = tahunPernyataanPenetapan;
	}

	@JsonProperty(STATUS_PERNYATAAN_PENETAPAN)
	public Integer getStatusPernyataanPenetapan() {
		return statusPernyataanPenetapan;
	}

	public void setStatusPernyataanPenetapan(Integer statusPernyataanPenetapan) {
		this.statusPernyataanPenetapan = statusPernyataanPenetapan;
	}

	/**********************************
	 * - End Generate -
	 ************************************/

	public static final String ID_PERNYATAAN = "id_pernyataan";

	public static final String ISI_PERNYATAAN = "isi_pernyataan";
	public static final String JENIS_AUDIT_ID_PERNYATAAN = "jenis_audit_id_pernyataan";
	public static final String ASPEK_ID_PERNYATAAN = "aspek_id_pernyataan";

	private String idPernyataan;
	private String isiPernyataan;
	private String jenisAuditIdPernyataan;
	private String aspekIdPernyataan;
	private List<PernyataanJawaban> listPernyataanJawaban;

	@JsonProperty(ISI_PERNYATAAN)
	public String getIsiPernyataan() {
		return isiPernyataan;
	}

	public void setIsiPernyataan(String isiPernyataan) {
		this.isiPernyataan = isiPernyataan;
	}

	@JsonProperty(JENIS_AUDIT_ID_PERNYATAAN)
	public String getJenisAuditIdPernyataan() {
		return jenisAuditIdPernyataan;
	}

	public void setJenisAuditIdPernyataan(String jenisAuditIdPernyataan) {
		this.jenisAuditIdPernyataan = jenisAuditIdPernyataan;
	}

	@JsonProperty(ASPEK_ID_PERNYATAAN)
	public String getAspekIdPernyataan() {
		return aspekIdPernyataan;
	}

	public void setAspekIdPernyataan(String aspekIdPernyataan) {
		this.aspekIdPernyataan = aspekIdPernyataan;
	}

	public List<PernyataanJawaban> getListPernyataanJawaban() {
		return listPernyataanJawaban;
	}

	public void setListPernyataanJawaban(List<PernyataanJawaban> listPernyataanJawaban) {
		this.listPernyataanJawaban = listPernyataanJawaban;
	}

	@JsonProperty(ID_PERNYATAAN)
	public String getIdPernyataan() {
		return idPernyataan;
	}

	public void setIdPernyataan(String idPernyataan) {
		this.idPernyataan = idPernyataan;
	}
}
