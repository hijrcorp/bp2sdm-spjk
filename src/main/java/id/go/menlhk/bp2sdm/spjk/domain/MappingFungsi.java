package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class MappingFungsi {
	public static final String ID = "id_mapping_fungsi";
	public static final String TUJUAN_UTAMA = "tujuan_utama_mapping_fungsi";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_mapping_fungsi";
	public static final String ID_SOURCE = "id_source_mapping_fungsi";
	public static final String ID_ACCOUNT_ADDED = "id_account_added_mapping_fungsi";
	public static final String TIMESTAMP_ADDED = "timestamp_added_mapping_fungsi";
	public static final String ID_ACCOUNT_MODIFIED = "id_account_modified_mapping_fungsi";
	public static final String TIMESTAMP_MODIFIED = "timestamp_modified_mapping_fungsi";

	private String id;
	private String tujuanUtama;
	private String idKelompokJabatan;
	private String idSource;
	private String idAccountAdded;
	private Date timestampAdded;
	private String idAccountModified;
	private Date timestampModified;

	public MappingFungsi() {

	}

	public MappingFungsi(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("tujuan_utama")
	public String getTujuanUtama() {
		return tujuanUtama;
	}

	public void setTujuanUtama(String tujuanUtama) {
		this.tujuanUtama = tujuanUtama;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	@JsonProperty("id_account_added")
	public String getIdAccountAdded() {
		return idAccountAdded;
	}

	public void setIdAccountAdded(String idAccountAdded) {
		this.idAccountAdded = idAccountAdded;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}

	@JsonProperty("id_account_modified")
	public String getIdAccountModified() {
		return idAccountModified;
	}

	public void setIdAccountModified(String idAccountModified) {
		this.idAccountModified = idAccountModified;
	}

	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}

	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}
	

	private List<MappingFungsiKunciDetil> listMappingFungsiKunciDetil = new ArrayList<MappingFungsiKunciDetil>();
	private KelompokJabatan KelompokJabatan;
	
	public List<MappingFungsiKunciDetil> getListMappingFungsiKunciDetil() {
		return listMappingFungsiKunciDetil;
	}

	public void setListMappingFungsiKunciDetil(List<MappingFungsiKunciDetil> listMappingFungsiKunciDetil) {
		this.listMappingFungsiKunciDetil = listMappingFungsiKunciDetil;
	}

	public KelompokJabatan getKelompokJabatan() {
		return KelompokJabatan;
	}

	public void setKelompokJabatan(KelompokJabatan kelompokJabatan) {
		KelompokJabatan = kelompokJabatan;
	}

	
}
