package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LaporanDynamic {
	
	private String id;
	private String kode;
	private String judul;
	private Double nilai;

	//@JsonProperty("id")
	public LaporanDynamic() {
	
	}
	
	public LaporanDynamic(String id) {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getJudul() {
		return judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	public Double getNilai() {
		return nilai;
	}

	public void setNilai(Double nilai) {
		this.nilai = nilai;
	}

	
	
}