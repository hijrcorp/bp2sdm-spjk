/*package id.go.menlhk.bp2sdm.spjk.common;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@Configuration
@Controller
public class PageDefaultController extends BaseController {
	
	

    @RequestMapping("/page/{jsp}")
    public String loadPage(Model model, Locale locale, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(!pass) {
			String requestPage = request.getRequestURL().toString();
	    		if(request.getQueryString() != null) {
	    			requestPage += "?"+request.getQueryString();
	    		}
	    		
	    		String resultPath = getConstructURL(request, "login-perform?page="+requestPage); 
	    		
	    		String params = "response_type=code";
			params += "&client_id=" + clientId;
			params += "&redirect_uri=" + resultPath + "";
			System.out.println(ssoEndpointUrl + "/oauth/authorize?" + params);
			response.sendRedirect(ssoEndpointUrl + "/oauth/authorize?" + params);
		}else {
			
			addModelTokenInfo(request, locale, model,accessTokenValue);
			
			return "pages/"+jsp;
		}
		
		
		
        return "error";
        
    }
    
    @RequestMapping("/page/")
    public String loadMain(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response) throws Exception{
    		String jsp = "index"; // default page, please check
		return loadPage(model, locale, jsp, request, response);
    }
    
    @RequestMapping("/page")
    public String loadMainNoSlash(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response) throws Exception{
    		String jsp = "index"; // default page, please check
		return loadPage(model, locale, jsp, request, response);
    }
    
}


*/