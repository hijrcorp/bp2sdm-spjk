package id.go.menlhk.bp2sdm.spjk.repo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaan;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanMapper;

@Component
public class ExamWebSocketHandler extends TextWebSocketHandler {

	@Autowired
	private UserPertanyaanMapper userPertanyaanMapper;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
    	// Process incoming messages if necessary (e.g., client sends updates).
        ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(message.getPayload());
		System.out.println("Received message JSON: "+root);

		String id = root.path("id").textValue(); String mark = "";String jenis = ""; 
		String durasi = root.path("duration").textValue(); Integer durasi_actual = root.path("duration_actual").intValue();
		UserPertanyaan data = new UserPertanyaan(id);
		data.setDurasiBerjalan(durasi_actual-Integer.valueOf(durasi));
		userPertanyaanMapper.updateDurasiOnly(data);
        // Optionally broadcast the update to all connected clients:
        session.sendMessage(new TextMessage("Update Time has received at server"));
		
    }
}