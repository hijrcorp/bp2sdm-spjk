package id.go.menlhk.bp2sdm.spjk.controllers;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import id.go.menlhk.bp2sdm.spjk.MainApplication;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.common.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.common.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.common.Utils;
import id.go.menlhk.bp2sdm.spjk.domain.Control;
import id.go.menlhk.bp2sdm.spjk.domain.MappingJabatanUnitAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.Pertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.PertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountGroupMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ApplicationMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.AssignmentMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.AuthoritiesMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ControlMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingJabatanUnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.OrganizationMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PositionMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.model.Account;
import id.go.menlhk.bp2sdm.spjk.model.Application;
import id.go.menlhk.bp2sdm.spjk.model.Assignment;
import id.go.menlhk.bp2sdm.spjk.model.Authorities;
import id.go.menlhk.bp2sdm.spjk.ref.AccountGroupInfo;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@Controller
@RequestMapping("/account")
public class AccountRestController extends BaseController {

	public final static String DELETE = "delete";
	public final static String RESET = "reset";
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private MappingJabatanUnitAuditiMapper mappingJabatanUnitAuditiMapper;
	
	@Autowired
	private AssignmentMapper assignmentMapper;
	
	@Autowired
	private AuthoritiesMapper authoritiesMapper;
	
	@Autowired
	private AccountGroupMapper accountGroupMapper;
	
	@Autowired
	private ApplicationMapper applicationMapper;
	
	@Autowired
	private ControlMapper controlMapper;
	
	@Autowired
	private SesiMapper sesiMapper;
	
	@Autowired
	private UnitAuditiMapper unitAuditiMapper;


	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
	    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Account.FIRST_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
	    		param.setClause(param.getClause() + " OR " + Account.LAST_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
	    		param.setClause(param.getClause() + " OR " + Account.USERNAME + " LIKE '%"+filterKeyword.get()+"%')");
	    	}
	    	
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Account> data = accountMapper.getList(param);
		
		resp.setCount(accountMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action,
			HttpServletRequest request
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		Account data = accountMapper.getEntity(id+extractAccountLogin(request, AccountLoginInfo.SOURCE));
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getUsername()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("username") Optional<String> username,
    		@RequestParam("password") Optional<String> password,
    		@RequestParam("email") Optional<String> email,
    		@RequestParam("email_status") Optional<Boolean> emailStatus,
    		@RequestParam("first_name") Optional<String> firstName,
    		@RequestParam("last_name") Optional<String> lastName,
    		@RequestParam("mobile") Optional<String> mobile,
    		@RequestParam("mobile_status") Optional<Boolean> mobileStatus,
    		@RequestParam("male_status") Optional<Boolean> maleStatus,
    		//@RequestParam("birth_date")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> birthDate,
			@RequestParam("birth_date") @DateTimeFormat(pattern = "yyyy-MM-dd") Optional<Date> birthDate,
    		@RequestParam("enabled") Optional<Boolean> enabled,
    		@RequestParam("account_non_expired") Optional<Boolean> accountNonExpired,
    		@RequestParam("credentials_non_expired") Optional<Boolean> credentialsNonExpired,
    		@RequestParam("account_non_locked") Optional<Boolean> accountNonLocked,
    		@RequestParam("unit_auditi_id") Optional<String> unitAuditiId,
    		@RequestParam("eselon1_id") Optional<String> eselon1Id,
    		@RequestParam("propinsi_id") Optional<String> propinsiId,
    		//carefull
    		@RequestParam("mapping_organisasi_id") Optional<String> bdlhkId,
    		@RequestParam("id_kelompok_jabatan") Optional<String> idKelompokJabatan,
    		@RequestParam("id_jabatan") Optional<String> idJabatan,
    		//carefull
    		@RequestParam("organization_id") Optional<String> organizationId,
    		@RequestParam("group_id") Optional<String> groupId,
    		@RequestParam("application_id") Optional<String> applicationId,
    		@RequestParam("force_password") Optional<String> forcepassword,//ini buat kebutuhan simpeg
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Account data = new Account(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = accountMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
//    		Thread.sleep(1000);
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(username.isPresent()) data.setUsername(username.get());
    		if(password.isPresent()) data.setPassword(password.get());
    		if(email.isPresent()) data.setEmail(email.get());
    		if(emailStatus.isPresent()) data.setEmailStatus(emailStatus.get());
    		if(firstName.isPresent()) data.setFirstName(firstName.get());
    		if(lastName.isPresent()) data.setLastName(lastName.get());
    		if(mobile.isPresent()) data.setMobile(mobile.get());
    		if(mobileStatus.isPresent()) data.setMobileStatus(mobileStatus.get());
    		if(maleStatus.isPresent()) data.setMaleStatus(maleStatus.get());
    		if(birthDate.isPresent()) data.setBirthDate(new Date());
    		
    		if(enabled.isPresent()) {
    			data.setEnabled(false);
    		}else {
    			data.setEnabled(true);
    		}
    		//default
			data.setAccountNonExpired(true);
			data.setCredentialsNonExpired(true);
			data.setAccountNonLocked(true);
    		/*if(accountNonExpired.isPresent()) 
    			data.setAccountNonExpired(accountNonExpired.get());
    		if(credentialsNonExpired.isPresent()) 
    			data.setCredentialsNonExpired(credentialsNonExpired.get());
    		if(accountNonLocked.isPresent()) 
    			data.setAccountNonLocked(accountNonLocked.get());*/
    		
    		boolean pass = true;
    		if( data.getUsername() == null || data.getFirstName() == null || data.getLastName() == null) {
    			pass = false;
    		}else {
    			if(data.getUsername().equals("") || data.getFirstName().equals("") || data.getLastName().equals("")) {
        			pass = false;
        		}
    		}
    		
    		if(applicationId.isPresent() && !applicationId.get().equals("")) {
	    		QueryParameter param = new QueryParameter();
	    		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
	    		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
	    		
	    		List<Application> lst = applicationMapper.getList(param);
	    		if(lst.size() == 0) {
	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Aplikasi SPEKTRA belum terdaftar didatabase.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
	    		}
    		}
    		
    		if(groupId.isPresent() && !groupId.get().equals("")) {
	    		if(accountGroupMapper.getEntity(groupId.get()) == null) {
	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Group "+groupId.get()+" belum terdaftar didatabase.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
	    		}
	    		
	    		if(groupId.get().equals(AccountGroupInfo.SATKER.getId()) || groupId.get().equals(AccountGroupInfo.RESPONDEN.getId())) {
	    			if(unitAuditiId.get().equals("")) {
		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
		    			resp.setMessage("Satker masih belum disi.");
		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	    			}
	    		}
	    		
	    		if(groupId.get().equals(AccountGroupInfo.ESELON1.getId())) {
	    			if(eselon1Id.get().equals("")) {
		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
		    			resp.setMessage("Eselon1 masih belum disi.");
		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	    			}
	    		}
	    		
	    		if(groupId.get().equals(AccountGroupInfo.ORGANISASI.getId())) {
	    			if(bdlhkId.get().equals("")) {
		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
		    			resp.setMessage("Organisasi masih belum disi.");
		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	    			}
	    		}
	    		
	    		if(groupId.get().equals(AccountGroupInfo.UNIT_PEMBINA.getId())) {
	    			if(idKelompokJabatan.get().equals("")) {
		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
		    			resp.setMessage("Kelompok Jabatan masih belum disi.");
		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	    			}
	    		}
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}

    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause()+" AND "+Account.USERNAME+" ='"+data.getUsername()+"'");
    		if(accountMapper.getCount(param) > 0 && !id.isPresent()) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Duplication entry for username '"+data.getUsername()+"'");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		Authorities authorities = null;
    		if(groupId.isPresent() && applicationId.isPresent() && !groupId.get().equals("") && !applicationId.get().equals("")) {
    			authorities = new Authorities(Utils.getLongNumberID());
    			    		
    			param = new QueryParameter();
        		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
        		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
        		
        		List<Application> lst = applicationMapper.getList(param);
        		System.out.println("STILL "+lst.size());
        		if(lst.size() > 0) {
        			Application app = lst.get(0);
        			QueryParameter authparam = new QueryParameter();
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+data.getId()+"'");
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
            		List<Authorities> authlst = authoritiesMapper.getList(authparam);
            		if(authlst.size() > 0) {
            			authorities = authlst.get(0);
            		}
        			
        			authorities.setAccountId(data.getId());
            		authorities.setAccount(data);
            		authorities.setGroup(accountGroupMapper.getEntity(groupId.get()));
            		authorities.setGroupId(groupId.get());
            		authorities.setApplication(app);
            		authorities.setApplicationId(app.getId());
            		
            		authorities.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
            		authorities.setTimeAdded(new Date());
            		

            		if(sesiMapper.getEntityActive()==null && groupId.get().equals("2002200")) {
            			resp.setCode(HttpStatus.BAD_REQUEST.value());
            			resp.setMessage("Tidak dapat membuat user,karena sesi ujian yang aktif tidak tersedia.");
            			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
        			}

            		
            		if(authlst.size() > 0) {
            			authoritiesMapper.update(authorities);
            		}else {
            			authoritiesMapper.insert(authorities);	
            		}
        		}
    		}
    		
    		if(id.isPresent()) {
				data.setPersonModified(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
				data.setTimeModified(new Date());
				
    			accountMapper.update(data);
    		}else {
    			data.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
				data.setTimeAdded(new Date());
				
    			accountMapper.insert(data);	
    		}

			
			//check first if usergrop not same with old
			//if(usergrop not same) {
				//eselon1
				if(userMapper.findEselon1ByUserId(data.getId()).size() > 0) {
					userMapper.deleteAllEselon1User(data.getId());
				}
				
				//propinsi
				if(userMapper.findPropinsiByUserId(data.getId()).size() > 0){
					userMapper.deleteAllPropinsiUser(data.getId());
				}
				//organisasi
				if(userMapper.findOrganisasiByUserId(data.getId()).size() > 0) {
					userMapper.deleteAllOrganisasiUser(data.getId());
				}
				//reference
				if(userMapper.findReferenceByUserId(data.getId(), "UNIT_PEMBINA").size() > 0) {
					userMapper.deleteAllReferenceUser(data.getId());
				}
			//}
				
			if(unitAuditiId.isPresent() && !unitAuditiId.get().equals("")) {
				userMapper.deleteAllUnitAuditiUser(data.getId());
				userMapper.insertUnitAuditiUser(unitAuditiId.get(), data.getId());
    			//responden if true
    			if(authorities.getGroupId().equals("2002200")) {
    				MappingJabatanUnitAuditi mju = new MappingJabatanUnitAuditi(Utils.getLongNumberID());
    				mju.setIdJabatan(idJabatan.get());
    				mju.setIdAccount(data.getId());
    				mju.setIdUnitAuditi(unitAuditiId.get());
    				mju.setIdSesi(sesiMapper.getEntityActive().getId());
    				
    				mappingJabatanUnitAuditiMapper.insert(mju);
    			}
			}

			if(eselon1Id.isPresent() && !eselon1Id.get().equals("")) {
				userMapper.deleteAllEselon1User(data.getId());
				userMapper.insertEselon1User(eselon1Id.get(), data.getId());
			}

			if(propinsiId.isPresent() && !propinsiId.get().equals("")) {
				userMapper.deleteAllPropinsiUser(data.getId());
				userMapper.insertPropinsiUser(propinsiId.get(), data.getId());
			}

			if(bdlhkId.isPresent() && !bdlhkId.get().equals("")) {
				userMapper.deleteAllOrganisasiUser(data.getId());
				userMapper.insertOrganisasiUser(bdlhkId.get(), data.getId(), "BDLHK");
			}

			if(idKelompokJabatan.isPresent() && !idKelompokJabatan.get().equals("")) {
				userMapper.deleteAllReferenceUser(data.getId());
				userMapper.insertReferenceUser(idKelompokJabatan.get(), data.getId(), "UNIT_PEMBINA");
			}
			
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+Control.ACCOUNT_ID+" ='"+data.getId()+"'");
			param.setClause(param.getClause()+" AND "+Control.SOURCE_ID+" ='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
			controlMapper.deleteBatch(param);
			
			Control control = new Control(Utils.getLongNumberID());
			control.setAccountId(data.getId());
			control.setSourceId(extractAccountLogin(request, AccountLoginInfo.SOURCE));
			if(authorities.getGroupId().equals("2002200")) {
				control.setIsAdmin(false);
			}else {
				control.setIsAdmin(true);
			}
			controlMapper.insert(control);
    		
    		if(authorities != null) { 
    			resp.setData(authorities);
    		}else{
    			resp.setData(data);
    		}
    		
    		if(forcepassword.isPresent() && !forcepassword.get().equals("")) {

    			Account ac = accountMapper.getEntity(data.getId());
    			ac.setPassword(passwordEncoder.encode(forcepassword.get()));
				accountMapper.update(ac);
    			
    		}
    		resp.setMessage("Data telah berhasil disimpan.");
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
    @RequestMapping(value="/savemulti", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> saveMulti(
    		@RequestParam("group_id") Optional<String> groupId,
    		@RequestParam("application_id") Optional<String> applicationId,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		//check aplikasi
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
    		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
    		List<Application> lst = applicationMapper.getList(param);
    		if(lst == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Aplikasi "+applicationId.get()+" tidak dikenal!");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		//check sesi
    		if(sesiMapper.getEntityActive()==null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Tidak dapat membuat user,karena sesi ujian yang aktif tidak tersedia.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
			}
    		//
    		String[] ids = request.getParameterValues("id");
    		String[] usernames = request.getParameterValues("username");
    		String[] names = request.getParameterValues("nama");
    		String[] jabatans = request.getParameterValues("id_jabatan");
    		String[] bidangTeknis = request.getParameterValues("id_bidang_teknis");
    		String[] unitAuditis = request.getParameterValues("id_unit_auditi");

    		List<Authorities> listAuthorities = new ArrayList<Authorities>();
    		List<Account> arr_users = new ArrayList<Account>();
    		List<String> arr_users_temp = new ArrayList<String>();
    		List<String> arr_users_dup = new ArrayList<String>();
    		List<String> arr_users_dup_cell = new ArrayList<String>();
    		List<String> arr_users_not_num = new ArrayList<String>();
    		List<String> arr_users_not_good_num = new ArrayList<String>();
    		List<String> arr_users_not_length = new ArrayList<String>();
    		List<String> arr_users_not_good_gender = new ArrayList<String>();
    		
    		System.out.println("jumlah user "+usernames.length);
    		if (usernames.length > 0) {
    			int ke = 0;
    			for (String o : usernames) {
	    			Account account = new Account(Utils.getLongNumberID());
					account.setUsername(usernames[ke]);
					if(!org.apache.commons.lang3.StringUtils.isNumeric(account.getUsername())) {
						arr_users_not_num.add(account.getUsername());
					}
					//check format nip
					String nip = usernames[ke];
					String tglLahir = nip.substring(0, 4)+"-"+nip.substring(4, 6)+"-"+nip.substring(6, 8);
					tglLahir = nip.substring(6, 8)+"/"+nip.substring(4, 6)+"/"+nip.substring(0, 4);
					boolean maleStatus=true;
					String strMaleStatus = nip.substring(14, 15);
					
			        if(!Utils.dateValidation(tglLahir, "dd/MM/yyyy")) {
			            arr_users_not_good_num.add(account.getUsername());
					}
			        if(o.length()!=18) {
			        	arr_users_not_length.add(account.getUsername());
					}
			        
			        if(strMaleStatus.equals("1")) {
			        	maleStatus=true;
			        }else if(strMaleStatus.equals("2")) {
			        	maleStatus=false;
			        }else {
			        	arr_users_not_good_gender.add(account.getUsername());
					}
					
					
					String []mynames=names[ke].split(" ");
            		account.setFirstName(mynames[0]);
            		String lastname="";
					if(mynames.length>1) {
						for(int istr=1; istr < mynames.length;istr++) {
							lastname+=" "+mynames[istr];
						}
					}
					account.setLastName(lastname);
					account.setMaleStatus(maleStatus);
					account.setEnabled(false);
					
		    		//default
					account.setAccountNonExpired(true);
					account.setCredentialsNonExpired(true);
					account.setAccountNonLocked(true);
            		
					
					param = new QueryParameter();
					param.setClause(param.getClause()+" AND "+Account.USERNAME+" ='"+usernames[ke]+"'");
					if(accountMapper.getList(param).size() > 0) {
						arr_users_dup.add(usernames[ke]);
					}
					
			        if (arr_users_temp.contains(usernames[ke])){
			        	arr_users_dup_cell.add(usernames[ke]);
					} else {
						arr_users_temp.add(usernames[ke]);
			        }
			        
			        arr_users.add(account);
        			ke++;
    			}
    			
				if(arr_users_dup.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada database, user berikut :"+Arrays.asList(arr_users_dup).get(0));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(arr_users_dup_cell.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada row tabel, user berikut :<br/>"+Arrays.asList(arr_users_dup_cell).get(0).toString().replaceAll(",", "<br/>"));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(arr_users_not_num.size() > 0 || arr_users_not_good_num.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			if(arr_users_not_num.size() > 0) 
	    				resp.setMessage("Kesalahan format data pastikan username hanya berupa angka dan tidak mengandung spasi, mohon periksa user berikut :<br/>"+Arrays.asList(arr_users_not_num).get(0).toString().replaceAll(",", "<br/>"));
	    			else
	    				resp.setMessage("Kesalahan format data pastikan username/nip(tgl_lahir) sudah sesuai, mohon periksa user berikut :<br/>"+Arrays.asList(arr_users_not_good_num).get(0).toString().replaceAll(",", "<br/>"));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(arr_users_not_num.size() > 0 || arr_users_not_good_num.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			if(arr_users_not_num.size() > 0) 
	    				resp.setMessage("Kesalahan format data pastikan username hanya berupa angka dan tidak mengandung spasi, mohon periksa user berikut :<br/>"+Arrays.asList(arr_users_not_num).get(0).toString().replaceAll(",", "<br/>"));
	    			else
	    				resp.setMessage("Kesalahan format data pastikan username/nip(tgl_lahir) sudah sesuai, mohon periksa user berikut :<br/>"+Arrays.asList(arr_users_not_good_num).get(0).toString().replaceAll(",", "<br/>"));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(arr_users_not_length.size() > 0 || arr_users_not_good_gender.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			if(arr_users_not_length.size() > 0) 
	    				resp.setMessage("Kesalahan format data pastikan username hanya 18 digit, mohon periksa user berikut :<br/>"+Arrays.asList(arr_users_not_length).get(0).toString().replaceAll(",", "<br/>"));
	    			else
	    				resp.setMessage("Kesalahan format data pastikan username/nip(kode jenis kelamin) sudah sesuai, mohon periksa user berikut :<br/>"+Arrays.asList(arr_users_not_good_gender).get(0).toString().replaceAll(",", "<br/>"));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
	    		int i = 0;
    			for (Account account : arr_users) {
    				account.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
    				account.setTimeAdded(new Date());
        			accountMapper.insert(account);
        			System.out.println("1. INSERRT account");
		    		
					//second
		    		Authorities authorities = new Authorities(Utils.getLongNumberID());
        			Application app = lst.get(0);
        			QueryParameter authparam = new QueryParameter();
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+account.getId()+"'");
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
            		List<Authorities> authlst = authoritiesMapper.getList(authparam);
            		if(authlst.size() > 0) {
            			authorities = authlst.get(0);
            		}
        			authorities.setAccountId(account.getId());
            		authorities.setAccount(account);
            		authorities.setGroup(accountGroupMapper.getEntity(groupId.get()));
            		authorities.setGroupId(groupId.get());
            		authorities.setApplication(app);
            		authorities.setApplicationId(app.getId());
            		authorities.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
            		authorities.setTimeAdded(new Date());

            		listAuthorities.add(authorities);

        			//if(!jabatans[ke].equals("") && !unitAuditis[ke].equals("")) {
        				userMapper.deleteAllUnitAuditiUser(account.getId());
        				userMapper.insertUnitAuditiUser(unitAuditis[i], account.getId());
	        			System.out.println("2.DELETE deleteAllUnitAuditiUser");
	        			System.out.println("2.INSERT insertUnitAuditiUser");
        				
        				MappingJabatanUnitAuditi mju = new MappingJabatanUnitAuditi(Utils.getLongNumberID());
        				mju.setIdJabatan(jabatans[i]);
        				//mju.setIdBidangTeknis(bidangTeknis[i]);
        				mju.setIdAccount(account.getId());
        				mju.setIdUnitAuditi(unitAuditis[i]);
        				mju.setIdSesi(sesiMapper.getEntityActive().getId());
        				mju.setGolongan("");
        				mappingJabatanUnitAuditiMapper.insert(mju);
            			System.out.println("3.INSERT MappingJabatanUnitAuditi");
        			//}
            		
            		if(authlst.size() > 0) {
            			authoritiesMapper.update(authorities);
            			System.out.println("4.UPDATE authorities");
            		}else {
            			authoritiesMapper.insert(authorities);
            			System.out.println("4.INSERT authorities");	
            		}
            		
        			param = new QueryParameter();
        			param.setClause(param.getClause()+" AND "+Control.ACCOUNT_ID+" ='"+account.getId()+"'");
        			param.setClause(param.getClause()+" AND "+Control.SOURCE_ID+" ='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
        			controlMapper.deleteBatch(param);
        			System.out.println("5.DELETE CONTROL");
        			
        			Control control = new Control(Utils.getLongNumberID());
        			control.setAccountId(account.getId());
        			control.setSourceId(extractAccountLogin(request, AccountLoginInfo.SOURCE));
        			
        			//2002200 is responden
        			if(authorities.getGroupId().equals("2002200")) {
        				control.setIsAdmin(false);
        			}else {
        				control.setIsAdmin(true);
        			}
        			System.out.println("6.INSERT CONTROL");
        			controlMapper.insert(control);
        			i++;
    			}
    			
				resp.setData(listAuthorities);
        		resp.setMessage("Data telah berhasil disimpan.");
    		}
    		//
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action,
			HttpServletRequest request
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			Account data = accountMapper.getEntity(id+extractAccountLogin(request, AccountLoginInfo.SOURCE));
			resp.setData(data);
			accountMapper.delete(data);
			
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + Assignment.ACCOUNT_ID + "='"+data.getId()+"'");
			assignmentMapper.deleteBatch(param);
			
			resp.setMessage("Data '"+data.getUsername()+"' telah dihapus.");
		}
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

	@RequestMapping(value = "/sesi-mapping/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> deleteSoal(
			RestQLController controller,
//			@RequestParam("id") Optional<String> id,
//			@RequestParam("id_action") Optional<String> idAction, 
			HttpServletRequest request

	) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();

		String[] ids = request.getParameterValues("cek_id_account");
		if(ids==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Belum ada user yang dipilih.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}

		String id="";
		for (String o : ids) { id+=",'"+o+"'"; }
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID+" in("+id.substring(1, id.length())+")");
		mappingJabatanUnitAuditiMapper.deleteBatch(param);
		/*List<MappingJabatanUnitAuditi> newdata = mappingJabatanUnitAuditiMapper.getList(param);
		for (MappingJabatanUnitAuditi o : newdata) {
			//Pertanyaan data = pertanyaanMapper.getEntity(idPertanyaan);
			mappingJabatanUnitAuditiMapper.delete(o);
		}*/
		resp.setMessage("Data telah berhasil disimpan.");
		
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/sesi-mapping/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> saveSesiMapping(
    		@RequestParam("group_id") Optional<String> groupId,
    		@RequestParam("application_id") Optional<String> applicationId,
    		@RequestParam("sesi_id") Optional<String> sesiId,
    		@RequestParam("from_sesi_id") Optional<String> fromSesiId,
    		@RequestParam("kelompok_jabatan_id") Optional<String> kelompokJabatanId,
    		@RequestParam("check_all") Optional<String> checkAll,
    		@RequestParam("mode_copy") Optional<String> modeCopy,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();

			if(sesiId.isPresent()) {
				if(sesiId.get().equals("")) {
	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Input parameter belum lengkap, harap dilengkapi terlebih dahulu.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
				}
    		}
			
    		QueryParameter param = new QueryParameter();
    		//
    		if(modeCopy.isPresent()) {
    			String[] ids = request.getParameterValues("cek_id_account_modal");
        		if(ids==null && modeCopy.get().equals("1")) {
	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Belum ada user yang dipilih.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
	    		}
    			if(modeCopy.get().equals("2")) {
    				//List<Account> data = accountMapper.getList(param);
        			param.setLimit(100000);
        			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+" ='"+fromSesiId.get()+"'");
        			if(!kelompokJabatanId.get().equals("NON_TEKNIS")) param.setClause(param.getClause()+" AND "+" id_kelompok_master_jabatan "+" ='"+kelompokJabatanId.get()+"'");
        			mappingJabatanUnitAuditiMapper.copyTest(param.getClause(), sesiId.get());
        			
        			//List<MappingJabatanUnitAuditi> newdata = mappingJabatanUnitAuditiMapper.getList(param);
        			/*mappingJabatanUnitAuditiMapper.copyTest(param);*/
        			System.out.println("try save all");
        			/*for (MappingJabatanUnitAuditi o : newdata) {
        				try {
            				o.setId(Utils.getLongNumberID());
            				o.setIdSesi(sesiId.get());
        					param = new QueryParameter();
        					param.setLimit(100000);
        					param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_ACCOUNT+" ='"+o.getIdAccount()+"'");
        					param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+" ='"+sesiId.get()+"'");
        					if(mappingJabatanUnitAuditiMapper.getCount(param) == 0) mappingJabatanUnitAuditiMapper.insert(o);
            			} catch (Exception e) {
            				resp.setCode(HttpStatus.BAD_REQUEST.value());
            				resp.setMessage("FATAL ERROR!!! Contact administrator");
            				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
            			}
        			}*/
            		resp.setMessage("Data telah berhasil disimpan.");
    			}else {
    				//save with costum
            		
            		String id="";
        			for (String o : ids) {
        				id+=",'"+o+"'";
        			}
        			System.out.println("TRY SUBMIT "+id.substring(1, id.length()));
        			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID+" in("+id.substring(1, id.length())+")");
        			mappingJabatanUnitAuditiMapper.copyTest(param.getClause(), sesiId.get());
        			
        			/*List<MappingJabatanUnitAuditi> newdata = mappingJabatanUnitAuditiMapper.getList(param);

            		System.out.println("jumlah newdata "+newdata.size());
            		System.out.println("jumlah user "+ids.length);
            		
        			for (MappingJabatanUnitAuditi o : newdata) {
        				try {
            				o.setId(Utils.getLongNumberID());
            				o.setIdSesi(sesiId.get());
        					
            				param = new QueryParameter();
        					param.setLimit(100000);
        					param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_ACCOUNT+" ='"+o.getIdAccount()+"'");
        					param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+" ='"+sesiId.get()+"'");
        					//only execute when data still not exist
        					if(mappingJabatanUnitAuditiMapper.getCount(param)==0) mappingJabatanUnitAuditiMapper.insert(o);
            			} catch (Exception e) {
            				resp.setCode(HttpStatus.BAD_REQUEST.value());
            				resp.setMessage("FATAL ERROR!!! Contact administrator");
            				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
            			}
        			}*/
            		resp.setMessage("Data telah berhasil disimpan.");
    			}
    		}
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    @RequestMapping(value = "/preview-import", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> importAccount(
			@RequestParam("mode") Optional<String> mode,
			@RequestParam("file_excel") Optional<MultipartFile> fileExcel,
			@RequestParam("start_read_row") Optional<Integer> startReadRow,
			HttpServletRequest request,
			MultipartHttpServletRequest req
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		QueryParameter param = new QueryParameter();
		List<Account> listAccount = new ArrayList<Account>();
		List<Authorities> listAuthorities = new ArrayList<Authorities>();

		//other
		String applicationId = "200";
		String groupId = "2002300";
		
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId+"'");
		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId+"')");
		
		List<Application> lst = applicationMapper.getList(param);
		
		if(lst.size() == 0) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Aplikasi SPEKTRA belum terdaftar didatabase.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		//end other
		
		if(fileExcel.isPresent() && !fileExcel.get().isEmpty()) {
			Iterator<String> itr = req.getFileNames();
			MultipartFile mpf = req.getFile(itr.next());

			InputStream file = mpf.getInputStream();

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet;
			for(int i=0; i < workbook.getNumberOfSheets();i++) {
				sheet = workbook.getSheetAt(i);

				String sheetname = sheet.getSheetName().replaceAll(" ", "");
				System.out.println(sheetname);
				/*if(unitKompetensiTeknisMapper.getList(param).size() == 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Kode "+sheetname+" Tidak valid.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}else {
					sheetid=unitKompetensiTeknisMapper.getList(param).get(0).getId();
				}*/
				
				Iterator<Row> rowIterator = sheet.iterator();
				int iRow = 0;
				int defaultRow = 2;
				if(startReadRow.isPresent()) {
					defaultRow = startReadRow.get()-2;
				}
				
				//for check duplicate
				String dupValue = null;
				ArrayList<String> tempList = new ArrayList<String>();
				ArrayList<String> dupList = new ArrayList<String>();
				ArrayList<String> dupCellList = new ArrayList<String>();
				ArrayList<String> errList = new ArrayList<String>();
				
				while(rowIterator.hasNext()) {
					Row row = rowIterator.next();
					if(row.getRowNum() > defaultRow) {
						//System.out.println(row.getCell(1).getStringCellValue());
						
						/*
						if(row.getCell(0)==null && row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK&& row.getCell(3, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK&& row.getCell(4, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK) {
							resp.setCode(HttpStatus.BAD_REQUEST.value());
							resp.setMessage("Terjadi masalah pada :<br/>"+
									"sheet="+sheetname+"<br/>no.soal masing kosong/format tidak sesuai, periksa row:"+(row.getRowNum()+1));
							return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
						}
						if(row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK && row.getCell(1,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK) {
							//convert ke numveric jika cell pertama mengandung string
							if(row.getCell(0).getCellType()==CellType.STRING) {
								if(row.getCell(0).getStringCellValue().matches("^\\s*$")) {
									resp.setCode(HttpStatus.BAD_REQUEST.value());
									resp.setMessage("Terjadi masalah pada :<br/>"+
											"sheet="+sheetname+"<br/>format tidak sesuai, periksa row:"+(row.getRowNum()+1));
									return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
								}
								String newNo0 = row.getCell(0).getStringCellValue().replaceAll("\'", "");
								String newNo = newNo0.replaceAll("\\.", ""); 
								//System.out.println("witch: "+newNo);
								row.getCell(0).setCellType(CellType.NUMERIC);
								row.getCell(0).setCellValue(Integer.valueOf(newNo.replaceAll(" ", "")));
							}
							if(row.getCell(0).getCellType()==CellType.NUMERIC) {
								//System.out.println("phan: "+row.getCell(3).getCellType());
								if(row.getCell(3, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
									//if(row.getCell(3).getStringCellValue().equals("")) {
										resp.setCode(HttpStatus.BAD_REQUEST.value());
										resp.setMessage("Terjadi masalah pada :<br/>"+
												"sheet="+sheetname+"<br/>no.soal:<strong>"+row.getCell(0).getNumericCellValue()+"</strong> dan jawaban="+"Masing Kosong");//row.getCell(3).getStringCellValue()
										return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
									//}
								}
								if(row.getCell(4, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
									//if(row.getCell(4).getStringCellValue().equals("")) {
										resp.setCode(HttpStatus.BAD_REQUEST.value());
										resp.setMessage("Terjadi masalah pada :<br/>"+
												"sheet="+sheetname+"<br/>no.soal:<strong>"+row.getCell(0).getNumericCellValue()+"</strong> dan kategori.soal="+"Masih Kosong");//row.getCell(4).getStringCellValue()
										return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
									//}
								}
							}else {

							}
						}else {
							if(row.getCell(0)==null && row.getCell(1).getCellType()!=CellType.BLANK&& row.getCell(3).getCellType()!=CellType.BLANK&& row.getCell(4).getCellType()!=CellType.BLANK) {
								resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>no.soal masing kosong/format tidak sesuai="+"row.getCell(0).getStringCellValue()");
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							}
							
							if(row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK && row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK && row.getCell(2, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
								resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>soal masing kosong, periksa row:"+(row.getRowNum()+1+" atau no.soal:"+row.getCell(0).getStringCellValue()));
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							}
							
						}*/
						
						if(row.getCell(4)!=null) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+UnitAuditi.NAMA_UNIT_AUDITI+" ='"+row.getCell(4).getStringCellValue()+"'");

							if(unitAuditiMapper.getList(param).size() == 0) {
								errList.add(row.getCell(4).getStringCellValue());
							}
						}
						
						if(row.getCell(2)!=null) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+Account.USERNAME+" ='"+row.getCell(2).getStringCellValue()+"'");

							if(accountMapper.getList(param).size() > 0) {
								dupList.add(row.getCell(2).getStringCellValue());
							}
							
							dupValue = row.getCell(2).getStringCellValue();
					        if (tempList.contains(dupValue)){
					        	dupCellList.add(dupValue);
							} else {
								tempList.add(dupValue);
					        }
					        
						}
						
					}
				}
				
				if(errList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Satker Belum terdaftar pada master database :"+Arrays.asList(errList).get(0));
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(dupList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada database, user berikut :"+Arrays.asList(dupList).get(0));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(dupCellList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada row excel, user berikut :<br/>"+Arrays.asList(dupCellList).get(0).toString().replaceAll(",", "<br/>"));
					//resp.setMessage("Terjadi duplikat masalah pada :<br/>"+
							//"sheet="+sheetname+"<br/>username="+row.getCell(1).getStringCellValue());
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				
				//reinit the iterator, cause we use this twice
				rowIterator = sheet.iterator();
				
				while(rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if(row.getRowNum() > defaultRow) {
						
						Account account = new Account(Utils.getUUIDString());
						account.setFirstName(row.getCell(1).getStringCellValue());
						account.setLastName("");
						account.setUsername(row.getCell(2).getStringCellValue());
						if(row.getCell(3).getStringCellValue().equals("")) {
							account.setEnabled(false);
			    		}else {
			    			account.setPassword(passwordEncoder.encode(row.getCell(3).getStringCellValue()));
			    			//account.setEnabled(true);
			    			account.setEnabled(false);
			    		}
			    		//default
						account.setAccountNonExpired(true);
						account.setCredentialsNonExpired(true);
						account.setAccountNonLocked(true);
						
						//set satker
		        		param = new QueryParameter();
						param.setClause(param.getClause()+" AND "+UnitAuditi.NAMA_UNIT_AUDITI+" ='"+row.getCell(4).getStringCellValue()+"'");
						account.setUnitAuditiId(unitAuditiMapper.getList(param).get(0).getIdUnitAuditi());
						
						listAccount.add(account);
					}
					iRow++;
				}
			}
			
			for(Account account : listAccount) {
				Authorities authorities = new Authorities(Utils.getLongNumberID());
    			Application app = lst.get(0);
    			QueryParameter authparam = new QueryParameter();
    			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+account.getId()+"'");
    			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
        		List<Authorities> authlst = authoritiesMapper.getList(authparam);
        		if(authlst.size() > 0) {
        			authorities = authlst.get(0);
        		}
    			authorities.setAccountId(account.getId());
        		authorities.setAccount(account);
        		authorities.setGroup(accountGroupMapper.getEntity(groupId));
        		authorities.setGroupId(groupId);
        		authorities.setApplication(app);
        		authorities.setApplicationId(app.getId());
        		authorities.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
        		authorities.setTimeAdded(new Date());
        		param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+UnitAuditi.ID_UNIT_AUDITI+" ='"+account.getUnitAuditiId()+"'");
				authorities.setUnitAuditi(unitAuditiMapper.getList(param).get(0));
        		
    			listAuthorities.add(authorities);
			}
			
			System.out.println("before delete");
			System.out.println("listAuthorities size:"+listAuthorities.size());
			System.out.println("listAccount size:"+listAccount.size());
			
			if(mode.isPresent() && mode.get().equals("save")) {
				
				
				Authorities authorities = null;
				for(Account account : listAccount) {
    				account.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
    				account.setTimeAdded(new Date());
					accountMapper.insert(account);
					
					authorities = new Authorities(Utils.getLongNumberID());
        			Application app = lst.get(0);
        			QueryParameter authparam = new QueryParameter();
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+account.getId()+"'");
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
            		List<Authorities> authlst = authoritiesMapper.getList(authparam);
            		if(authlst.size() > 0) {
            			authorities = authlst.get(0);
            		}
        			authorities.setAccountId(account.getId());
            		authorities.setAccount(account);
            		authorities.setGroup(accountGroupMapper.getEntity(groupId));
            		authorities.setGroupId(groupId);
            		authorities.setApplication(app);
            		authorities.setApplicationId(app.getId());
            		
            		authorities.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
            		authorities.setTimeAdded(new Date());
            		
            		if(authlst.size() > 0) {
            			authoritiesMapper.update(authorities);
            		}else {
            			authoritiesMapper.insert(authorities);	
            		}
            		
            		userMapper.deleteAllUnitAuditiUser(account.getId());
    				userMapper.insertUnitAuditiUser(account.getUnitAuditiId(), account.getId());
            		
        			param = new QueryParameter();
        			param.setClause(param.getClause()+" AND "+Control.ACCOUNT_ID+" ='"+account.getId()+"'");
        			param.setClause(param.getClause()+" AND "+Control.SOURCE_ID+" ='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
        			controlMapper.deleteBatch(param);
        			
        			Control control = new Control(Utils.getLongNumberID());
        			control.setAccountId(account.getId());
        			control.setSourceId(extractAccountLogin(request, AccountLoginInfo.SOURCE));
        			control.setIsAdmin(true);
        			controlMapper.insert(control);
				}
				//resp.setData(authorities);
				resp.setData("save");
			}else {
				resp.setData(listAuthorities);
			}
		}

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
}
