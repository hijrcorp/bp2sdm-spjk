package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Eselon1 {

public static final String ID = "id_eselon1";
public static final String KODE = "kode_eselon1";
public static final String NAMA = "nama_eselon1";
public static final String KODE_SIMPEG = "kode_simpeg_eselon1";
public static final String ID_SIMPEG = "id_simpeg_eselon1";
public static final String ID_BIDANG_TEKNIS = "id_bidang_teknis_eselon1";

private String id;
private String kode;
private String nama;
private String kodeSimpeg;
private String idSimpeg;
private String idBidangTeknis;
private String idKelompokJabatan;

public Eselon1() {

}

public Eselon1(String id) {
	this.id = id;
}

@JsonProperty("id")
public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

@JsonProperty("kode")
public String getKode() {
	return kode;
}

public void setKode(String kode) {
	this.kode = kode;
}

@JsonProperty("nama")
public String getNama() {
	return nama;
}

public void setNama(String nama) {
	this.nama = nama;
}

@JsonProperty("kode_simpeg")
public String getKodeSimpeg() {
	return kodeSimpeg;
}

public void setKodeSimpeg(String kodeSimpeg) {
	this.kodeSimpeg = kodeSimpeg;
}

@JsonProperty("id_simpeg")
public String getIdSimpeg() {
	return idSimpeg;
}

public void setIdSimpeg(String idSimpeg) {
	this.idSimpeg = idSimpeg;
}

@JsonProperty("id_bidang_teknis")
public String getIdBidangTeknis() {
	return idBidangTeknis;
}

public void setIdBidangTeknis(String idBidangTeknis) {
	this.idBidangTeknis = idBidangTeknis;
}

@JsonProperty("id_kelompok_jabatan")
public String getIdKelompokJabatan() {
	return idKelompokJabatan;
}

public void setIdKelompokJabatan(String idKelompokJabatan) {
	this.idKelompokJabatan = idKelompokJabatan;
}



	/**********************************************************************/

}
