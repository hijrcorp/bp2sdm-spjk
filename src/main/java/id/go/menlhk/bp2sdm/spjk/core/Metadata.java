package id.go.menlhk.bp2sdm.spjk.core;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public abstract class Metadata extends Entity {

	private Date dateAdded;
	private Date dateModified;
	private String userAdded;
	private String userModified;
	private Date dateDeleted;
	private String userDeleted;

	public Metadata() {
		super(0);
	}

	public Metadata(long id) {
		super(id);
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public String getUserAdded() {
		return userAdded;
	}

	public void setUserAdded(String userAdded) {
		this.userAdded = userAdded;
	}

	public String getUserModified() {
		return userModified;
	}

	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	public Date getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public String getUserDeleted() {
		return userDeleted;
	}

	public void setUserDeleted(String userDeleted) {
		this.userDeleted = userDeleted;
	}
}
