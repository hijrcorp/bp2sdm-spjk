package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardBidangSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardMonitoringSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardProgress;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.Laporan;
import id.go.menlhk.bp2sdm.spjk.domain.LaporanDynamic;

public interface LaporanMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	List<Laporan> getListLaporanNilaiPemetaan(QueryParameter param);
	List<LaporanDynamic> getListLaporanPerformaFUtamaStandarKomp(QueryParameter param);
	List<LaporanDynamic> getListNewLaporanPerformaFUtamaStandarKomp(QueryParameter param);
	List<Laporan> getListNewLaporanNilaiPemetaan(QueryParameter param);
	List<Laporan> getListNewLaporanNilaiPemetaanNonTeknis(QueryParameter param);
	List<DashboardProgress> getDashboardProgress(QueryParameter param);

	/**********************************
	 * - End Generate -
	 ************************************/

}
