package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.RataRataIndividuExisting;

public interface RataRataIndividuExistingMapper {

	@Insert("INSERT INTO spjk_rata_rata_individu_existing (id_rata_rata_individu_existing, id_user_responden_rata_rata_individu_existing, nip_rata_rata_individu_existing, nama_rata_rata_individu_existing, tgl_lahir_rata_rata_individu_existing, jenis_kelamin_rata_rata_individu_existing, golongan_rata_rata_individu_existing, id_eselon1_rata_rata_individu_existing, id_unit_auditi_rata_rata_individu_existing, id_kelompok_jabatan_rata_rata_individu_existing, id_tingkat_jabatan_rata_rata_individu_existing, id_jenjang_jabatan_rata_rata_individu_existing, kode_sesi_rata_rata_individu_existing, status_responden_rata_rata_individu_existing) VALUES (#{id:VARCHAR}, #{idUserResponden:VARCHAR}, #{nip:VARCHAR}, #{nama:VARCHAR}, #{tglLahir:DATE}, #{jenisKelamin:VARCHAR}, #{golongan:VARCHAR}, #{idEselon1:VARCHAR}, #{idUnitAuditi:VARCHAR}, #{idKelompokJabatan:VARCHAR}, #{idTingkatJabatan:VARCHAR}, #{idJenjangJabatan:VARCHAR}, #{kodeSesi:VARCHAR}, #{statusResponden:VARCHAR})")
	void insert(RataRataIndividuExisting rataRataIndividuExisting);

	@Update("UPDATE spjk_rata_rata_individu_existing SET id_rata_rata_individu_existing=#{id:VARCHAR}, id_user_responden_rata_rata_individu_existing=#{idUserResponden:VARCHAR}, nip_rata_rata_individu_existing=#{nip:VARCHAR}, nama_rata_rata_individu_existing=#{nama:VARCHAR}, tgl_lahir_rata_rata_individu_existing=#{tglLahir:DATE}, jenis_kelamin_rata_rata_individu_existing=#{jenisKelamin:VARCHAR}, golongan_rata_rata_individu_existing=#{golongan:VARCHAR}, id_eselon1_rata_rata_individu_existing=#{idEselon1:VARCHAR}, id_unit_auditi_rata_rata_individu_existing=#{idUnitAuditi:VARCHAR}, id_kelompok_jabatan_rata_rata_individu_existing=#{idKelompokJabatan:VARCHAR}, id_tingkat_jabatan_rata_rata_individu_existing=#{idTingkatJabatan:VARCHAR}, id_jenjang_jabatan_rata_rata_individu_existing=#{idJenjangJabatan:VARCHAR}, kode_sesi_rata_rata_individu_existing=#{kodeSesi:VARCHAR}, status_responden_rata_rata_individu_existing=#{statusResponden:VARCHAR} WHERE id_rata_rata_individu_existing=#{id}")
	void update(RataRataIndividuExisting rataRataIndividuExisting);

	@Delete("DELETE FROM spjk_rata_rata_individu_existing WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_rata_rata_individu_existing WHERE id_rata_rata_individu_existing=#{id}")
	void delete(RataRataIndividuExisting rataRataIndividuExisting);

	List<RataRataIndividuExisting> getList(QueryParameter param);

	RataRataIndividuExisting getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

}
