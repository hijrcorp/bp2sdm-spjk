package id.go.menlhk.bp2sdm.spjk.domain;

import java.io.Serializable;

import id.go.menlhk.bp2sdm.spjk.core.Metadata;

public class CountLhp extends Metadata implements Serializable {
	private String groupName;
	private int count;

	public CountLhp() {
		super();
	}

	public CountLhp(int id) {
		super(id);
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
