package id.go.menlhk.bp2sdm.spjk.domain;


import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpegEselon {

	/********************************** - Begin Generate - ************************************/

	public static final String KODE = "kode";
	public static final String ESELON = "eselon";
	public static final String KELOMPOK_ESELON = "kelompok_eselon";
	
	private String kode;
	private String eselon;
	private String kelompokEselon;
	
	
	public SimpegEselon() {
	
	}
	public SimpegEselon(String kode) {
		this.kode = kode;
	}
	
	@JsonProperty(KODE)
	public String getKode() {
		return kode;
	}
	
	public void setKode(String kode) {
		this.kode = kode;
	}
	
	@JsonProperty(ESELON)
	public String getEselon() {
		return eselon;
	}
	
	public void setEselon(String eselon) {
		this.eselon = eselon;
	}
	
	@JsonProperty(KELOMPOK_ESELON)
	public String getKelompokEselon() {
		return kelompokEselon;
	}
	
	public void setKelompokEselon(String kelompokEselon) {
		this.kelompokEselon = kelompokEselon;
	}
	
	
	/********************************** - End Generate - ************************************/

}
