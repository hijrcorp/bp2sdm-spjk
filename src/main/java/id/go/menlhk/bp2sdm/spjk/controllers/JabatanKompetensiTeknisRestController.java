package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.go.menlhk.bp2sdm.spjk.MainApplication;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanManajerial;
import id.go.menlhk.bp2sdm.spjk.domain.MasterJabatan;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.helper.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.JabatanKompetensiTeknisMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.JabatanManajerialMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MasterJabatanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ReferenceMapper;

@Controller
@RequestMapping("/jabatan-kompetensi-teknis")
public class JabatanKompetensiTeknisRestController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	private JabatanKompetensiTeknisMapper jabatanKompetensiTeknisMapper;
	
	@Autowired
	private MasterJabatanMapper masterJabatanMapper;

	@Autowired
	private ReferenceMapper referenceMapper;
	
//	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
//	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<ResponseWrapper> getList(
//    		@RequestParam("limit") Optional<Integer> limit,
//    		@RequestParam("no_offset") Optional<Boolean> noOffset,
//    		@RequestParam("page") Optional<Integer> page,
//    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
//    		@RequestParam("filter_group") Optional<String> filterGroup,
//    		HttpServletRequest request
//    		)throws Exception {
//    	
//		QueryParameter param = new QueryParameter();
//		
//	    	/*if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
//	    		param.setClause(param.getClause() + " AND (" + Position.NAME + " LIKE '%"+filterKeyword.get()+"%')");
//	    	}
//	    	
//	    	if(filterGroup.isPresent() && !filterGroup.get().equals("")) {
//	    		param.setClause(param.getClause() + " AND (" + Position.GROUP_ID + " = '"+filterGroup.get()+"')");
//	    	}*/
//	    	
//	    	//param.setClause(param.getClause() + " AND " + Position.ORGANIZATION_ID + "='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
//	    	
//	    	if(limit.isPresent()) {
//	    		param.setLimit(limit.get());
//	    	}
//	    	
//	    	int pPage = 1;
//	    	if(page.isPresent()) {
//	    		pPage = page.get();
//	    		int offset = (pPage-1)*param.getLimit();
//	    		param.setOffset(offset);
//	    		
//	    		if(noOffset.isPresent()) {
//	    			param.setOffset(0);
//	    			param.setLimit(pPage*param.getLimit());
//	    		}
//	    	}
//    	
//    	
//		ResponseWrapperList resp = new ResponseWrapperList();
//		List<JabatanManajerial> data = jabatanManajerialMapper.getList(param);
//		
//		resp.setCount(jabatanManajerialMapper.getCount(param));
//		resp.setData(data);
//		if(noOffset.isPresent()) {
//			resp.setNextMore(data.size() < resp.getCount());
//		}else {
//			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
//		}
//		
//		String qryString = "?page="+(pPage+1);
//		if(limit.isPresent()){
//			qryString += "&limit="+limit.get();
//		}
//		resp.setNextPageNumber(pPage+1);
//		resp.setNextPage(request.getRequestURL().toString()+qryString);
//		
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
	
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
//	public ResponseEntity<ResponseWrapper> getById(
//			@PathVariable String id,
//			@RequestParam("action") Optional<String> action
//			) throws Exception {
//		ResponseWrapper resp = new ResponseWrapper();
//		Pegawai data = pegawaiMapper.getEntity(id);
//		
//		if(action.isPresent()) {
//			if(action.get().toLowerCase().equals(DELETE)) {
//				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
//			}
//			
//		}
//		
//		resp.setData(data);
//		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//	}
//	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("id_master_jabatan") Optional<String> idMasterJabatan,
    		@RequestParam("id_kelompok_jabatan") Optional<String> idKelompokJabatan,
    		@RequestParam("id_bidang_teknis") Optional<String> idBidangTeknis,
    		@RequestParam("id_kelompok_teknis") Optional<String> idKelompokTeknis,
    		@RequestParam("jenis_kompetensi") Optional<String> jenisKompetensi,
    		@RequestParam("id_unit_kompetensi_teknis") Optional<String> idUnitKompetensiTeknis,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		JabatanKompetensiTeknis data = new JabatanKompetensiTeknis(Utils.getLongNumberID());
//    		if(id.isPresent()) {
//    			data.setId(id.get());
//    			data = jabatanKompetensiTeknisMapper.getEntity(id.get());
//    		}
//    		if(data == null) {
//    			resp.setCode(HttpStatus.BAD_REQUEST.value());
//    			resp.setMessage("Data tidak ditemukan.");
//    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    		}
    		
//    		if(id.isPresent()) data.setId(id.get());
    		if(idMasterJabatan.isPresent()) data.setIdMasterJabatan(idMasterJabatan.get());
    		if(idKelompokJabatan.isPresent()) data.setIdKelompokJabatan(idKelompokJabatan.get());
    		if(idBidangTeknis.isPresent()) data.setIdBidangTeknis(idBidangTeknis.get());
    		if(idKelompokTeknis.isPresent()) data.setIdKelompokTeknis(idKelompokTeknis.get());
    		if(jenisKompetensi.isPresent()) data.setJenisKompetensi(jenisKompetensi.get());
    		if(idUnitKompetensiTeknis.isPresent()) data.setIdUnitKompetensiTeknis(idUnitKompetensiTeknis.get());
    		
    		//if(idSource.isPresent()) data.setIdSource(idSource.get());
    		//if(idAccountAdded.isPresent()) data.setIdAccountAdded(idAccountAdded.get());
    		//if(timestampAdded.isPresent()) data.setTimestampAdded(timestampAdded.get());
    		//if(idAccountModified.isPresent()) data.setIdAccountModified(idAccountModified.get());
    		//if(timestampModified.isPresent()) data.setTimestampModified(timestampModified.get());
    		
    		//data.setOrganizationId(extractAccountLogin(request, AccountLoginInfo.SOURCE));

    		boolean pass = true;
    		if(data.getIdMasterJabatan() == null || data.getIdKelompokJabatan() == null || data.getIdBidangTeknis() == null || data.getIdUnitKompetensiTeknis() == null) {
    			pass = false;
    		}else {
    			if(data.getIdMasterJabatan().equals("") || data.getIdKelompokJabatan().equals("") || data.getIdBidangTeknis().equals("") || data.getIdUnitKompetensiTeknis().equals("")) {
        			pass = false;
        		}
    		}
    		if(data.getJenisKompetensi().equals("PILIHAN")) {
    			if(data.getIdKelompokTeknis() == null) {
    				pass = false;
    			}else {
	    			if(data.getIdKelompokTeknis().equals("")) {
	    				pass=false;
	    			}
    			}
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
//    		if(id.isPresent()) {
//    			jabatanKompetensiTeknisMapper.update(data);
//    		}else {
//    			jabatanKompetensiTeknisMapper.insert(data);	
//    		}
    		
    		String[] listIdUnitKompetensiTeknis = request.getParameterValues("id_unit_kompetensi_teknis");
    		
			int idx=0;
			for(String o : listIdUnitKompetensiTeknis) {
				data = new JabatanKompetensiTeknis();
//					if(id.isPresent()) data.setId(id.get());
		    		if(idMasterJabatan.isPresent()) data.setIdMasterJabatan(idMasterJabatan.get());
		    		if(idKelompokJabatan.isPresent()) data.setIdKelompokJabatan(idKelompokJabatan.get());
		    		if(idBidangTeknis.isPresent()) data.setIdBidangTeknis(idBidangTeknis.get());
		    		if(idKelompokTeknis.isPresent()) data.setIdKelompokTeknis(idKelompokTeknis.get());
		    		if(jenisKompetensi.isPresent()) data.setJenisKompetensi(jenisKompetensi.get());

		    		data.setIdUnitKompetensiTeknis(o);
//					QueryParameter param = new QueryParameter();
//					param.setClause(param.getClause()+ " AND "+JabatanManajerial.ID_MASTER_JABATAN+" ='"+idMasterJabatan+"'");
//					param.setClause(param.getClause()+ " AND "+JabatanManajerial.ID_KELOMPOK_MANAJERIAL+" ='"+listIdKelompokManajerial[idx]+"'");
//					param.setClause(param.getClause()+ " AND "+JabatanManajerial.JENIS+" ='"+data.getJenis()+"'");
//					jabatanManajerialMapper.deleteBatch(param);
		    		jabatanKompetensiTeknisMapper.insert(data);
				idx++;
			}
			
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
//    
//   
//
//    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
//	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<ResponseWrapper> doAction(
//			@PathVariable String id,
//			@PathVariable String action
//    		) throws Exception {
//		ResponseWrapper resp = new ResponseWrapper();
//		
//		if(action.equals(DELETE)) {
//			
//			Pegawai data = pegawaiMapper.getEntity(id);
//			resp.setData(data);
//			pegawaiMapper.delete(data);
//			
//			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
//		}
//		
//		
//		
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
    
//    @RequestMapping(value="/{jenis}", method = RequestMethod.GET, produces = "application/json")
//	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<ResponseWrapper> getListJenis(
//    		@RequestParam("limit") Optional<Integer> limit,
//    		@RequestParam("no_offset") Optional<Boolean> noOffset,
//    		@RequestParam("page") Optional<Integer> page,
//    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
//    		@RequestParam("filter_id_kelompok") Optional<String> filterKelompok,
//			@PathVariable String jenis,
//    		HttpServletRequest request
//    		)throws Exception {
//    	
//    	Map<String, Object> map = new HashMap<String, Object>();
//		QueryParameter param = new QueryParameter();
//		
//	    	/*if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
//	    		param.setClause(param.getClause() + " AND (" + Position.NAME + " LIKE '%"+filterKeyword.get()+"%')");
//	    	}
//	    	
//	    	if(filterGroup.isPresent() && !filterGroup.get().equals("")) {
//	    		param.setClause(param.getClause() + " AND (" + Position.GROUP_ID + " = '"+filterGroup.get()+"')");
//	    	}*/
//	    	
//	    	//param.setClause(param.getClause() + " AND " + Position.ORGANIZATION_ID + "='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
//	    	
//	    	if(limit.isPresent()) {
//	    		param.setLimit(limit.get());
//	    	}
//	    	
//	    	int pPage = 1;
//	    	if(page.isPresent()) {
//	    		pPage = page.get();
//	    		int offset = (pPage-1)*param.getLimit();
//	    		param.setOffset(offset);
//	    		
//	    		if(noOffset.isPresent()) {
//	    			param.setOffset(0);
//	    			param.setLimit(pPage*param.getLimit());
//	    		}
//	    	}
//    	
//    	
//		ResponseWrapperList resp = new ResponseWrapperList();
//		param.setClause(param.getClause()+ " AND "+"jenis_jabatan_manajerial"+"='"+jenis+"'");
//		param.setClause(param.getClause()+ " AND "+"id_kelompok_master_jabatan"+"='"+filterKelompok.get()+"'");
//		List<JabatanManajerial> data = jabatanManajerialMapper.getList(param);
//		map.put("jabatan_manajerial", data);
//		param = new QueryParameter();
//		param.setClause(param.getClause()+ " AND "+MasterJabatan.ID_KELOMPOK+"='"+filterKelompok.get()+"'");
//		map.put("master_jabatan", masterJabatanMapper.getList(param));
//		if(jenis.equals("manajerial")) {
//			map.put("kelompok", referenceMapper.getListKelompokManajerial());
//		}
//		if(jenis.equals("sosiokultural")) {
//			map.put("kelompok", referenceMapper.getListKelompokSosiokultural());
//		}
////		resp.setCount(jabatanManajerialMapper.getCount(param));
//		resp.setData(map);
//		if(noOffset.isPresent()) {
//			resp.setNextMore(data.size() < resp.getCount());
//		}else {
//			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
//		}
//		
//		String qryString = "?page="+(pPage+1);
//		if(limit.isPresent()){
//			qryString += "&limit="+limit.get();
//		}
//		resp.setNextPageNumber(pPage+1);
//		resp.setNextPage(request.getRequestURL().toString()+qryString);
//		
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
}
