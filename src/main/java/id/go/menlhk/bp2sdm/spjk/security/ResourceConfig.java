package id.go.menlhk.bp2sdm.spjk.security;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableResourceServer
public class ResourceConfig extends ResourceServerConfigurerAdapter {

	@Value("${security.oauth2.resource.id}")
	private String resourceId;

	@Value("${security.oauth2.client.id}")
	private String clientId;

	@Value("${security.oauth2.client.secret}")
	private String clientSecret;

	@Value("${app.url.sso}")
	private String ssoEndpointUrl;
	
	@Value("${security.oauth2.token.keyfile}")
    private String publicKeyFile;

	
	// To allow the rResourceServerConfigurerAdapter to understand the token,
	// it must share the same characteristics with AuthorizationServerConfigurerAdapter.
	// So, we must wire it up the beans in the ResourceServerSecurityConfigurer.
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(resourceId)
//		.tokenServices(remoteTokenServices())
		.tokenStore(tokenStore())
		;
	}
	
	@Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());

    }
	
	@Bean
    public JwtAccessTokenConverter accessTokenConverter() {
    	JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        Resource resource = new ClassPathResource(publicKeyFile);
        String publicKey = null;
        try {
            publicKey = IOUtils.toString(resource.getInputStream());
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
        converter.setVerifierKey(publicKey);
        return converter;
        
    }

	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.cors().and()
				// .anonymous().disable()
				// .csrf().disable()
				.authorizeRequests()

				.antMatchers("/daftar/baru**","/register**","/", "/images/**","/files/**","/**/download", "/css/**", "/scss/**", "/vendor/**", "/js/**", "/login**","/page/**", "/admin**", "/admin/*", "/admin-old/*", "/dashboard**", "/dashboard/*", "/assets/**", "/assets-dashboard/**", "/**/console/**", "/preview-export/**","/**/preview-export","/simpeg/*", "/ref/**", "/exam-updates/**", "/exam-updates")
				.permitAll()
				.anyRequest().authenticated();
	}
	
/*
	@Bean
	public ResourceServerTokenServices remoteTokenServices() {
		RemoteTokenServices tokenService = new RemoteTokenServices();
		tokenService.setClientId(clientId);
		tokenService.setClientSecret(clientSecret);
		tokenService.setAccessTokenConverter(new JwtAccessTokenConverter());
		tokenService.setCheckTokenEndpointUrl(ssoEndpointUrl + "/oauth/check_token");
		return tokenService;
	}
*/
}
