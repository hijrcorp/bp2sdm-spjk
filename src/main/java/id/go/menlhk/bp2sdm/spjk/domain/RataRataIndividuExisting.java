package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RataRataIndividuExisting {

	public static final String ID = "id_rata_rata_individu_existing";
	public static final String ID_USER_RESPONDEN = "id_user_responden_rata_rata_individu_existing";
	public static final String NIP = "nip_rata_rata_individu_existing";
	public static final String NAMA = "nama_rata_rata_individu_existing";
	public static final String TGL_LAHIR = "tgl_lahir_rata_rata_individu_existing";
	public static final String JENIS_KELAMIN = "jenis_kelamin_rata_rata_individu_existing";
	public static final String GOLONGAN = "golongan_rata_rata_individu_existing";
	public static final String ID_ESELON1 = "id_eselon1_rata_rata_individu_existing";
	public static final String ID_UNIT_AUDITI = "id_unit_auditi_rata_rata_individu_existing";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_rata_rata_individu_existing";
	public static final String ID_TINGKAT_JABATAN = "id_tingkat_jabatan_rata_rata_individu_existing";
	public static final String ID_JENJANG_JABATAN = "id_jenjang_jabatan_rata_rata_individu_existing";
	public static final String KODE_SESI = "kode_sesi_rata_rata_individu_existing";
	public static final String STATUS_RESPONDEN = "status_responden_rata_rata_individu_existing";

	private String id;
	private String idUserResponden;
	private String nip;
	private String nama;
	private Date tglLahir;
	private String jenisKelamin;
	private String golongan;
	private String idEselon1;
	private String idUnitAuditi;
	private String idKelompokJabatan;
	private String idTingkatJabatan;
	private String idJenjangJabatan;
	private String kodeSesi;
	private String statusResponden;

	public RataRataIndividuExisting() {

	}

	public RataRataIndividuExisting(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_user_responden")
	public String getIdUserResponden() {
		return idUserResponden;
	}

	public void setIdUserResponden(String idUserResponden) {
		this.idUserResponden = idUserResponden;
	}

	@JsonProperty("nip")
	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("tgl_lahir")
	public Date getTglLahir() {
		return tglLahir;
	}

	public void setTglLahir(Date tglLahir) {
		this.tglLahir = tglLahir;
	}

	@JsonProperty("jenis_kelamin")
	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	@JsonProperty("golongan")
	public String getGolongan() {
		return golongan;
	}

	public void setGolongan(String golongan) {
		this.golongan = golongan;
	}

	@JsonProperty("id_eselon1")
	public String getIdEselon1() {
		return idEselon1;
	}

	public void setIdEselon1(String idEselon1) {
		this.idEselon1 = idEselon1;
	}

	@JsonProperty("id_unit_auditi")
	public String getIdUnitAuditi() {
		return idUnitAuditi;
	}

	public void setIdUnitAuditi(String idUnitAuditi) {
		this.idUnitAuditi = idUnitAuditi;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	@JsonProperty("id_tingkat_jabatan")
	public String getIdTingkatJabatan() {
		return idTingkatJabatan;
	}

	public void setIdTingkatJabatan(String idTingkatJabatan) {
		this.idTingkatJabatan = idTingkatJabatan;
	}

	@JsonProperty("id_jenjang_jabatan")
	public String getIdJenjangJabatan() {
		return idJenjangJabatan;
	}

	public void setIdJenjangJabatan(String idJenjangJabatan) {
		this.idJenjangJabatan = idJenjangJabatan;
	}

	@JsonProperty("kode_sesi")
	public String getKodeSesi() {
		return kodeSesi;
	}

	public void setKodeSesi(String kodeSesi) {
		this.kodeSesi = kodeSesi;
	}

	@JsonProperty("status_responden")
	public String getStatusResponden() {
		return statusResponden;
	}

	public void setStatusResponden(String statusResponden) {
		this.statusResponden = statusResponden;
	}


	/**********************************************************************/

	public static final String STATUS_PEMETAAN = "status_pemetaan_rata_rata_individu_existing";
	
	private String namaKelompokJabatan;
	private String namaTingkatJabatan;
	private String namaJenjangJabatan;
	private String namaEselon1;
	private String namaUnitAuditi;
	private Integer umur;
	private String statusPemetaan;

	public String getNamaKelompokJabatan() {
		return namaKelompokJabatan;
	}

	public void setNamaKelompokJabatan(String namaKelompokJabatan) {
		this.namaKelompokJabatan = namaKelompokJabatan;
	}

	public String getNamaTingkatJabatan() {
		return namaTingkatJabatan;
	}

	public void setNamaTingkatJabatan(String namaTingkatJabatan) {
		this.namaTingkatJabatan = namaTingkatJabatan;
	}

	public String getNamaJenjangJabatan() {
		return namaJenjangJabatan;
	}

	public void setNamaJenjangJabatan(String namaJenjangJabatan) {
		this.namaJenjangJabatan = namaJenjangJabatan;
	}

	public String getNamaEselon1() {
		return namaEselon1;
	}

	public void setNamaEselon1(String namaEselon1) {
		this.namaEselon1 = namaEselon1;
	}

	public String getNamaUnitAuditi() {
		return namaUnitAuditi;
	}

	public void setNamaUnitAuditi(String namaUnitAuditi) {
		this.namaUnitAuditi = namaUnitAuditi;
	}

	public Integer getUmur() {
		return umur;
	}

	public void setUmur(Integer umur) {
		this.umur = umur;
	}

	public String getStatusPemetaan() {
		return statusPemetaan;
	}

	public void setStatusPemetaan(String statusPemetaan) {
		this.statusPemetaan = statusPemetaan;
	}
	
}
