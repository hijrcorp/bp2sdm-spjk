package id.go.menlhk.bp2sdm.spjk.ref;

public enum StatusLaporan {
	MASA_PELAPORAN("Masuk dalam masa pelaporan", "green"), LEWAT_MASA_PELAPORAN("Lewat 2x masa pelaporan",
			"yellow"), LEWAT_BATAS_MASA_PELAPORAN("Lewat 3x atau lebih masa pelaporan", "red"), SELESAI_PELAPORAN(
					"Laporan telah disubmit",
					"none"), BELUM_MASA_PELAPORAN("Masa pelaporan belum dimulai", "none"), DRAFT("DRAFT", "none"),;

	private String id;
	private String warna;

	private StatusLaporan(final String id, final String warna) {
		this.id = id;
		this.warna = warna;
	}

	@Override
	public String toString() {
		return this.id;
	}

	public String getWarna() {
		return this.warna;
	}
}
