package id.go.menlhk.bp2sdm.spjk.core;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import id.go.menlhk.bp2sdm.spjk.domain.User;

public interface BaseService {

	public void setUserLogin(User user);

	public void loadUserLogin(OAuth2Authentication authentication);

}
