package id.go.menlhk.bp2sdm.spjk.domain.bc;

public class OauthUserOrganization extends OauthUser {

	public static final String POSITION_ID = "position_id";
	public static final String POSITION_NAME = "position_name";
	public static final String ORGANIZATION_ID = "organization_id";
	public static final String ORGANIZATION_CODE = "organization_code";
	public static final String ORGANIZATION_NAME = "organization_name";

	private String positionId;
	private String positionName;
	private String organizationId;
	private String organizationCode;
	private String organizationName;

	public OauthUserOrganization() {

	}

	public OauthUserOrganization(String id) {
		super(id);
	}

	// @JsonProperty(POSITION_ID)
	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	// @JsonProperty(POSITION_NAME)
	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	// @JsonProperty(ORGANIZATION_ID)
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	// @JsonProperty(ORGANIZATION_NAME)
	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	// @JsonProperty(ORGANIZATION_CODE)
	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

}
