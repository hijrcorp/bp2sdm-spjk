package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanKompetensiTeknis;

@Mapper
public interface JabatanKompetensiTeknisMapper {
	@Insert("INSERT INTO spjk_jabatan_kompetensi_teknis (id_jabatan_kompetensi_teknis, id_master_jabatan_jabatan_kompetensi_teknis, id_kelompok_jabatan_jabatan_kompetensi_teknis, id_bidang_teknis_jabatan_kompetensi_teknis, id_kelompok_teknis_jabatan_kompetensi_teknis, id_unit_kompetensi_teknis_jabatan_kompetensi_teknis, jenis_kompetensi_jabatan_kompetensi_teknis, id_source_jabatan_kompetensi_teknis, id_account_added_jabatan_kompetensi_teknis, timestamp_added_jabatan_kompetensi_teknis, id_account_modified_jabatan_kompetensi_teknis, timestamp_modified_jabatan_kompetensi_teknis) VALUES (#{id:VARCHAR}, #{idMasterJabatan:VARCHAR}, #{idKelompokJabatan:VARCHAR}, #{idBidangTeknis:VARCHAR}, #{idKelompokTeknis:VARCHAR}, #{idUnitKompetensiTeknis:VARCHAR}, #{jenisKompetensi:VARCHAR}, #{idSource:VARCHAR}, #{idAccountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{idAccountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(JabatanKompetensiTeknis jabatanKompetensiTeknis);

	@Update("UPDATE spjk_jabatan_kompetensi_teknis SET id_jabatan_kompetensi_teknis=#{id:VARCHAR}, id_master_jabatan_jabatan_kompetensi_teknis=#{idMasterJabatan:VARCHAR}, id_kelompok_jabatan_jabatan_kompetensi_teknis=#{idKelompokJabatan:VARCHAR}, id_bidang_teknis_jabatan_kompetensi_teknis=#{idBidangTeknis:VARCHAR}, id_kelompok_teknis_jabatan_kompetensi_teknis=#{idKelompokTeknis:VARCHAR}, id_unit_kompetensi_teknis_jabatan_kompetensi_teknis=#{idUnitKompetensiTeknis:VARCHAR}, jenis_kompetensi_jabatan_kompetensi_teknis=#{jenisKompetensi:VARCHAR}, id_source_jabatan_kompetensi_teknis=#{idSource:VARCHAR}, id_account_added_jabatan_kompetensi_teknis=#{idAccountAdded:VARCHAR}, timestamp_added_jabatan_kompetensi_teknis=#{timestampAdded:TIMESTAMP}, id_account_modified_jabatan_kompetensi_teknis=#{idAccountModified:VARCHAR}, timestamp_modified_jabatan_kompetensi_teknis=#{timestampModified:TIMESTAMP} WHERE id_jabatan_kompetensi_teknis=#{id}")
	void update(JabatanKompetensiTeknis jabatanKompetensiTeknis);

	@Delete("DELETE FROM spjk_jabatan_kompetensi_teknis WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_jabatan_kompetensi_teknis WHERE id_jabatan_kompetensi_teknis=#{id}")
	void delete(JabatanKompetensiTeknis jabatanKompetensiTeknis);

	List<JabatanKompetensiTeknis> getList(QueryParameter param);

	List<JabatanKompetensiTeknis> getListUniq(QueryParameter param);

	JabatanKompetensiTeknis getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

}