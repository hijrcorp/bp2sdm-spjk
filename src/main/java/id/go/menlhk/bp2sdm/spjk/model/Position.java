package id.go.menlhk.bp2sdm.spjk.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Position {

	public static final String ID = "id_position";
	public static final String NAME = "name_position";
	public static final String DESCRIPTION = "description_position";
	public static final String GROUP_ID = "group_id_position";
	public static final String ORGANIZATION_ID = "organization_id_position";

	private String id;
	private String name;
	private String description;
	private String groupId;
	private String organizationId;

	public Position() {

	}

	public Position(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("group_id")
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}



	/**********************************************************************/
	
	public static final String COUNT_POSITION = "count_account_position";
	
	private PositionGroup group = new PositionGroup();
	private int countAccount;

	public PositionGroup getGroup() {
		return group;
	}

	public void setGroup(PositionGroup group) {
		this.group = group;
	}

	@JsonProperty("count_account")
	public int getCountAccount() {
		return countAccount;
	}

	public void setCountAccount(int countAccount) {
		this.countAccount = countAccount;
	}

}
