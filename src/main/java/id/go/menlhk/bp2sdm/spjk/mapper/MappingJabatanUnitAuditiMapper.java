package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.MappingJabatanUnitAuditi;

public interface MappingJabatanUnitAuditiMapper {
	
	@Insert("INSERT INTO spjk_mapping_jabatan_unit_auditi (id_mapping_jabatan_unit_auditi, id_jabatan_mapping_jabatan_unit_auditi, id_account_mapping_jabatan_unit_auditi, id_unit_auditi_mapping_jabatan_unit_auditi, id_sesi_mapping_jabatan_unit_auditi, golongan_mapping_jabatan_unit_auditi) VALUES (#{id:VARCHAR}, #{idJabatan:VARCHAR}, #{idAccount:VARCHAR}, #{idUnitAuditi:VARCHAR}, #{idSesi:VARCHAR}, #{golongan:VARCHAR})")
	void insert(MappingJabatanUnitAuditi mappingJabatanUnitAuditi);

	@Update("UPDATE spjk_mapping_jabatan_unit_auditi SET id_mapping_jabatan_unit_auditi=#{id:VARCHAR}, id_jabatan_mapping_jabatan_unit_auditi=#{idJabatan:VARCHAR}, id_account_mapping_jabatan_unit_auditi=#{idAccount:VARCHAR}, id_unit_auditi_mapping_jabatan_unit_auditi=#{idUnitAuditi:VARCHAR}, id_sesi_mapping_jabatan_unit_auditi=#{idSesi:VARCHAR}, golongan_mapping_jabatan_unit_auditi=#{golongan:VARCHAR} WHERE id_mapping_jabatan_unit_auditi=#{id}")
	void update(MappingJabatanUnitAuditi mappingJabatanUnitAuditi);

	@Delete("DELETE FROM spjk_mapping_jabatan_unit_auditi WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_mapping_jabatan_unit_auditi WHERE id_mapping_jabatan_unit_auditi=#{id}")
	void delete(MappingJabatanUnitAuditi mappingJabatanUnitAuditi);

	List<MappingJabatanUnitAuditi> getList(QueryParameter param);

	MappingJabatanUnitAuditi getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
//	@Insert("INSERT INTO spjk_mapping_jabatan_unit_auditi (id_mapping_jabatan_unit_auditi, id_jabatan_mapping_jabatan_unit_auditi, id_account_mapping_jabatan_unit_auditi, id_unit_auditi_mapping_jabatan_unit_auditi, id_sesi_mapping_jabatan_unit_auditi) VALUES (#{id:VARCHAR}, #{idJabatan:VARCHAR}, #{idAccount:VARCHAR}, #{idUnitAuditi:VARCHAR}, #{idSesi:VARCHAR})")
//	void insert(MappingJabatanUnitAuditi mappingJabatanUnitAuditi);
//
//	@Update("UPDATE spjk_mapping_jabatan_unit_auditi SET id_mapping_jabatan_unit_auditi=#{id:VARCHAR}, id_jabatan_mapping_jabatan_unit_auditi=#{idJabatan:VARCHAR}, id_account_mapping_jabatan_unit_auditi=#{idAccount:VARCHAR}, id_unit_auditi_mapping_jabatan_unit_auditi=#{idUnitAuditi:VARCHAR}, id_sesi_mapping_jabatan_unit_auditi=#{idSesi:VARCHAR} WHERE id_mapping_jabatan_unit_auditi=#{id}")
//	void update(MappingJabatanUnitAuditi mappingJabatanUnitAuditi);
//
//	@Delete("DELETE FROM spjk_mapping_jabatan_unit_auditi WHERE ${clause}")
//	void deleteBatch(QueryParameter param);
//
//	@Delete("DELETE FROM spjk_mapping_jabatan_unit_auditi WHERE id_mapping_jabatan_unit_auditi=#{id}")
//	void delete(MappingJabatanUnitAuditi mappingJabatanUnitAuditi);
//	
//	List<MappingJabatanUnitAuditi> getList(QueryParameter param);
//
//	MappingJabatanUnitAuditi getEntity(String id);
//
//	long getCount(QueryParameter param);
//
//	String getNewId();
	
	//costume
	@Delete("INSERT IGNORE INTO spjk_mapping_jabatan_unit_auditi "
			+ "SELECT uuid(), id_jabatan_mapping_jabatan_unit_auditi, id_account_mapping_jabatan_unit_auditi, id_unit_auditi_mapping_jabatan_unit_auditi, #{idsesi}, golongan_mapping_jabatan_unit_auditi,\n"
			+ "is_requested_mapping_jabatan_unit_auditi\n"
			+ "FROM spjk_mapping_jabatan_unit_auditi WHERE ${clause}")
	void copyTest(@Param("clause") String clause, @Param("idsesi") String idsesi);
	
}
