package id.go.menlhk.bp2sdm.spjk.core;

import java.util.ArrayList;
import java.util.List;

public class QueryParameter {

	private String locale = "en_US";
	private String clause = "1";
	private String values = "";
	private String order;
	private String group;
	private Integer limit = 100;
	private Integer offset = 0;

	public QueryParameter() {
		// TODO Auto-generated constructor stub
	}

	public String getClause() {
		return clause;
	}

	public void setClause(String clause) {
		this.clause = clause;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	/**** costume ****/

	private String headerClause = "1";
	private String secondClause = "1";
	private String thirdClause = "1";


	public String getHeaderClause() {
		return headerClause;
	}

	public void setHeaderClause(String headerClause) {
		this.headerClause = headerClause;
	}

	public String getSecondClause() {
		return secondClause;
	}

	public void setSecondClause(String secondClause) {
		this.secondClause = secondClause;
	}
	
	public String getThirdClause() {
		return thirdClause;
	}

	public void setThirdClause(String thirdClause) {
		this.thirdClause = thirdClause;
	}
}
