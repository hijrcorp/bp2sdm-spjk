package id.go.menlhk.bp2sdm.spjk.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.go.menlhk.bp2sdm.spjk.common.Entity;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;

public class Authorities extends Entity {
	public static final String ID = "id_authorities";
	public static final String ACCOUNT_ID = "account_id_authorities";
	public static final String GROUP_ID = "group_id_authorities";
	public static final String APPLICATION_ID = "application_id_authorities";
	public static final String PERSON_ADDED = "person_added_authorities";
	public static final String TIME_ADDED = "time_added_authorities";

	private String id;
	private String accountId;
	private String groupId;
	private String applicationId;
	private String personAdded;
	private Date timeAdded;

	public Authorities() {

	}

	public Authorities(String id) {
		this.id = id;
	}

	@Override
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("account_id")
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonProperty("group_id")
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@JsonProperty("application_id")
	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	@Override
	@JsonProperty("person_added")
	public String getPersonAdded() {
		return personAdded;
	}

	@Override
	public void setPersonAdded(String personAdded) {
		this.personAdded = personAdded;
	}

	@Override
	@JsonProperty("time_added")
	public Date getTimeAdded() {
		return timeAdded;
	}

	@Override
	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}



	/**********************************************************************/
	
	private Account account = new Account();
	private AccountGroup group;// = new AccountGroup();
	private Application application = new Application();
	private Position position = new Position();
	private UnitAuditi unitAuditi = new UnitAuditi();
	private String namaJabatan;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	
	@JsonProperty("group")
	public AccountGroup getGroup() {
		return group;
	}

	public void setGroup(AccountGroup group) {
		this.group = group;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public UnitAuditi getUnitAuditi() {
		return unitAuditi;
	}

	public void setUnitAuditi(UnitAuditi unitAuditi) {
		this.unitAuditi = unitAuditi;
	}

	@JsonProperty("nama_jabatan")
	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}
	
	
}
