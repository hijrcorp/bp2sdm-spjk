package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PeranResponden {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	public static final String ID_PERAN_RESPONDEN = "id_peran_responden";
	public static final String NAMA_PERAN_RESPONDEN = "nama_peran_responden";

	private String idPeranResponden;
	private String namaPeranResponden;

	public PeranResponden() {

	}

	public PeranResponden(String id) {
		this.idPeranResponden = id;
	}

	@JsonProperty(ID_PERAN_RESPONDEN)
	public String getIdPeranResponden() {
		return idPeranResponden;
	}

	public void setIdPeranResponden(String idPeranResponden) {
		this.idPeranResponden = idPeranResponden;
	}

	@JsonProperty(NAMA_PERAN_RESPONDEN)
	public String getNamaPeranResponden() {
		return namaPeranResponden;
	}

	public void setNamaPeranResponden(String namaPeranResponden) {
		this.namaPeranResponden = namaPeranResponden;
	}

	/**********************************
	 * - End Generate -
	 ************************************/

}
