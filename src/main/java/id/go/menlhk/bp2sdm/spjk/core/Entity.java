package id.go.menlhk.bp2sdm.spjk.core;

public class Entity {

	private long id;
	private int rowNum;

	public Entity() {
		this(0);
	}

	public Entity(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getRowNum() {
		return rowNum;
	}

	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}

}
