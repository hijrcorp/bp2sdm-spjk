package id.go.menlhk.bp2sdm.spjk.core;

public class NewClause {
	private static final long serialVersionUID = 3849306663516449211L;
	private static final String DEFAULT_SUCCESS = "Success.";
	public final String AND = " AND ";
	public final String OR = " OR ";

	private String column;
	private String value;
	private String operator;
	private String result;

	public NewClause() {
		// TODO Auto-generated constructor stub
	}

	public NewClause(String column, String value) {
		this(column, "=", value);
	}

	public NewClause(String column, String operator, String value) {
		this.operator = operator;
		this.column = column;
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getQuery() {
		String query = this.getColumn() + " " + this.getOperator() + this.getValue();
		return query;
	}

	public String getValue() {
		if (operator.toLowerCase().contains("is") || operator.toLowerCase().contains("in")
				|| operator.toLowerCase().equals("between")) {
			return value;
		} else if (operator.toLowerCase().equals("like")) {
			if (value.contains("%")) {
				return "'" + value + "'";
			} else {
				return "'%" + value + "%'";
			}
		}
		return "'" + value + "'";
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String QRY(String kolom, String operator, String value) {
		if (operator.toLowerCase().equals("like")) {
			this.result = kolom + " LIKE " + "'%" + value + "%'";
		} else {
			this.result = kolom + " = " + "'" + value + "'";
		}
		return this.result;
	}

	public String WHERE(String kolom, String operator, String value) {
		if (operator.toLowerCase().equals("like")) {
			this.result = kolom + " LIKE " + "'%" + value + "%'";
		} else {
			this.result = kolom + " = " + "'" + value + "'";
		}
		return this.result;
	}

	public String Between(String kolom, String value, String value2) {
		this.result = kolom + " " + "between" + " " + "'" + value + "'" + " AND " + "'" + value2 + "'";

		return this.result;
	}
}