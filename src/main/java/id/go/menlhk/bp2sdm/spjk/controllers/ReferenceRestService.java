package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.core.Message;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.MasterJabatan;
import id.go.menlhk.bp2sdm.spjk.domain.Propinsi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.mapper.MasterJabatanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ReferenceMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SimpegEselonMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@RestController
@RequestMapping("/ref")
public class ReferenceRestService extends BaseController {


	@Autowired
	private ReferenceMapper refMapper;


	@Autowired
	private UserMapper userMapper;

	@Autowired
	private MasterJabatanMapper masterJabatanMapper;

//	@Autowired
//	private SimpegOrganisasiMapper simpegOrganisasiMapper;
	@Autowired
	private SimpegEselonMapper simpegOrganisasiMapper;
	
	@RequestMapping(value = "/simpeg_organisasi", method = RequestMethod.GET,headers="Accept=application/json")
	public Message getListSimpegOrganisasi() throws Exception{
		Message resp = new Message(Message.SUCCESS_CODE);
		resp.setData(simpegOrganisasiMapper.getList(new QueryParameter()));
		return resp;
	}
	
	// @RequestMapping(value = "/kelompokUPT", method =
	// RequestMethod.GET,headers="Accept=application/json")
	// public Message getListCategory() throws Exception{
	// Message resp = new Message(Message.SUCCESS_CODE);
	// resp.setData(refMapper.getListKelompokUPT());
	// return resp;
	// }
	// @RequestMapping(value = "/indikator-kinerja", method =
	// RequestMethod.GET,headers="Accept=application/json")
	// public Message getListIndikatorKinerja(
	// @RequestParam("indikator_kinerja_id") Optional<String> id,
	// @RequestParam("jenis") Optional<String> jenis
	// ) throws Exception{
	// Message resp = new Message(Message.SUCCESS_CODE);
	// QueryParameter param = new QueryParameter();
	// if(jenis.isPresent()) param.setClause(param.getClause() + " AND "+
	// IndikatorKinerja.JENIS + " LIKE '%"+jenis.get()+"%'");
	// List<IndikatorKinerja> data = indikatorKinerjaMapper.getList(param);
	// resp.setData(data);
	// return resp;
	// }
	// @RequestMapping(value = "/program", method =
	// RequestMethod.GET,headers="Accept=application/json")
	// public Message getListProgram() throws Exception{
	// Message resp = new Message(Message.SUCCESS_CODE);
	// resp.setData(refMapper.getListProgram());
	// return resp;
	// }
	// @RequestMapping(value = "/tugasFungsi/", method =
	// RequestMethod.GET,headers="Accept=application/json")
	// public Message getListTugasFungsi() throws Exception{
	// Message resp = new Message(Message.SUCCESS_CODE);
	// resp.setData(refMapper.getListTugasFungsi());
	// return resp;
	// }
	// @RequestMapping(value = "/unitAuditiForTugasFungsi/{id}", method =
	// RequestMethod.GET,headers="Accept=application/json")
	// public Message getUnitAuditiForTugasFungsi(@PathVariable String id) throws
	// Exception{
	// Message resp = new Message(Message.SUCCESS_CODE);
	// resp.setData(refMapper.findBySatker(id));
	// return resp;
	// }
	// //new here
	// @RequestMapping(value = "/unit-kerja", method =
	// RequestMethod.GET,headers="Accept=application/json")
	// public Message getListUnitKerja() throws Exception{
	// Message resp = new Message(Message.SUCCESS_CODE);
	// resp.setData(refMapper.getListUnitKerja());
	// return resp;
	// }

	@RequestMapping(value = "/propinsi/list", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseWrapperList getListJenisKegiatan(HttpServletRequest request) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		QueryParameter param = new QueryParameter();
		if(userMapper.findPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + UnitAuditi.ID_UNIT_AUDITI + "='"+userMapper.findPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		List<Propinsi> data = refMapper.getListPropinsi(param);
		resp.setData(data);
		return resp;
	}

	@RequestMapping(value = "/sumber/list", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseWrapperList getListSumber() throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		resp.setData(refMapper.getListSumber());
		return resp;
	}
	@RequestMapping(value = "/unit-auditi/list", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseWrapperList getListUnitAuditi() throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		resp.setData(refMapper.getListUnitAuditi());
		return resp;
	}

	
	@RequestMapping(value = "/master-jabatan", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseWrapperList getList(HttpServletRequest request,
			@RequestParam("filter_id_jabatan_simpeg") Optional<String> filter_id_jabatan_simpeg,
			@RequestParam("filter_keyword") Optional<String> filter_keyword,
			@RequestParam("filter_nama") Optional<String> filter_nama,
			@RequestParam("filter_nama_array") Optional<String[]> filter_nama_array,
			@RequestParam("filter_propinsi") Optional<String> filter_propinsi,
			@RequestParam("filter_eselon1") Optional<String> filter_eselon1,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {
		/*System.out.println(filter_nama_array.get().length);
		System.out.println(filter_nama_array.get());
		for(String v : filter_nama_array.get()) {
			System.out.println(v);
		}*/
		QueryParameter param = new QueryParameter();
		if(filter_nama_array.isPresent()) {
			for(int i=0; i < filter_nama_array.get().length; i++) {
				param.setClause(param.getClause() + " AND " + MasterJabatan.NAMA + " LIKE '%" + filter_nama_array.get()[i] + "%'");
				//param.setClause(param.getClause() + " AND " + "nama_master_jabatan" + " LIKE '%" + filter_nama.get() + "%'");
			}
		}
		if (filter_nama.isPresent()) {
			param.setClause(param.getClause() + " AND " + "nama_alias_master_jabatan" + " LIKE '%" + filter_nama.get() + "%'");
			param.setClause(param.getClause() + " OR " + "nama_real_master_jabatan" + " LIKE '%" + filter_nama.get() + "%'");
			param.setClause(param.getClause() + " OR " + "nama_master_jabatan" + " LIKE '%" + filter_nama.get() + "%'");
		}
		
		if (limit.isPresent()) {
			param.setLimit(limit.get());
			if (limit.get() > 2000)
				param.setLimit(2000);
		} else {
			param.setLimit(2000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		ResponseWrapperList resp = new ResponseWrapperList();
		
		/*if(filter_nama_array.isPresent() && filter_nama_array.get().length>0) {
			StringBuilder order = new StringBuilder();
			order.append("CASE");
				for(int i=0; i < filter_nama_array.get().length; i++) {
					order.append(" WHEN nama_unit_auditi LIKE '%"+filter_nama_array.get()[i]+"%' THEN "+(i+1));
				}
			order.append(" END");
			param.setOrder(order.toString());
		}else {
			param.setOrder("nama_unit_auditi");
		}*/
		if (filter_id_jabatan_simpeg.isPresent()) {
			param.setClause(param.getClause() + " AND " + "id_jabatan_simpeg_jabatan" + " = '" + filter_id_jabatan_simpeg.get() + "'");
		}
		List<MasterJabatan> data = masterJabatanMapper.getList(param);
		resp.setCount(masterJabatanMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return resp;
	}
}
