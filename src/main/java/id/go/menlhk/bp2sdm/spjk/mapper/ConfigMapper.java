package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Config;

@Mapper
public interface ConfigMapper {
	@Insert("INSERT INTO spjk_config (id_config, code_config, value_config) VALUES (#{id:VARCHAR}, #{code:VARCHAR}, #{value:VARCHAR})")
	void insert(Config config);

	@Update("UPDATE spjk_config SET id_config=#{id:VARCHAR}, code_config=#{code:VARCHAR}, value_config=#{value:VARCHAR} WHERE id_config=#{id}")
	void update(Config config);

	@Delete("DELETE FROM spjk_config WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_config WHERE id_config=#{id}")
	void delete(Config config);

	List<Config> getList(QueryParameter param);

	Config getEntity(String id);

	Config getEntityByKode(String code);
	Config getEntityByKodeActive(String code);

	long getCount(QueryParameter param);

	String getNewId();
}