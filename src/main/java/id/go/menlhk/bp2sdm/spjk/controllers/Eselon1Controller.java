package id.go.menlhk.bp2sdm.spjk.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.core.Clause;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Eselon1;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.helper.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.Eselon1Mapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;


@RestController
@RequestMapping("/eselon1")
public class Eselon1Controller extends BaseController {

	@Autowired
	private Eselon1Mapper eselon1Mapper;

	@Autowired
	private UserMapper userMapper;
	
    @RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseWrapperList getList(
    		HttpServletRequest request,
    		@RequestParam("filter_keyword") Optional<String> filter_keyword,
    		@RequestParam("filter_kode") Optional<String> filter_kode,
    		@RequestParam("filter_nama") Optional<String> filter_nama,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page
    		)throws Exception {
//    	MessageResult resp = new MessageResult(Message.SUCCESS_CODE);
    	
		QueryParameter param = new QueryParameter();
//		if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+ Eselon1.NAMA_ESELON1 + " LIKE '%"+filter_keyword.get()+"%'");
		//if(filter_kode.isPresent()) param.setClause(param.getClause() + " AND "+ Eselon1.KODE_ESELON1 + " LIKE '%"+filter_kode.get()+"%'");
		//if(filter_nama.isPresent()) param.setClause(param.getClause() + " AND "+ Eselon1.NAMA_ESELON1 + " LIKE '%"+filter_nama.get()+"%'");

		if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + Eselon1.ID + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		
		if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    		if(limit.get() > 2000) param.setLimit(2000);
	    	}else {
	        	param.setLimit(2000);
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    	}
	    	
	    	ResponseWrapperList resp = new ResponseWrapperList();
			//param.setClause("1");
			List<Eselon1> data = eselon1Mapper.getList(param);
			resp.setCount(eselon1Mapper.getCount(param));
			resp.setData(data);
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
			
			String qryString = "?page="+(pPage+1);
			if(limit.isPresent()){
				qryString += "&limit="+limit.get();
			}
			resp.setNextPageNumber(pPage+1);
			resp.setNextPage(request.getRequestURL().toString()+qryString);

        return resp;
    }
//    
//    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
//	public ResponseEntity<ResponseWrapper> getById(@PathVariable String id) throws Exception {
//		ResponseWrapper resp = new id.co.lhk.survey.helper.ResponseWrapper();
//		Eselon1 data = eselon1Mapper.getEntity(id);
//		resp.setData(data);
//		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//	}

	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("kode") Optional<String> kode,
    		@RequestParam("nama") Optional<String> nama,
    		@RequestParam("kode_simpeg") Optional<String> kodeSimpeg
    		) throws Exception {
    	 	
    		ResponseWrapper resp = new ResponseWrapper();
    		String existSatker ="";
    		Eselon1 data = new Eselon1(String.valueOf(eselon1Mapper.getNewId()));
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = eselon1Mapper.getEntity(id.get());
				existSatker = data.getNama();
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		if(kode.isPresent()) data.setKode(kode.get());
    		if(nama.isPresent()) data.setNama(nama.get());
    		if(kodeSimpeg.isPresent()) data.setKodeSimpeg(kodeSimpeg.get());
    		
    		boolean pass = true;
    		if(data.getNama() == null) {
    			pass = false;
    		}else {
    			if(data.getNama().equals("")) {
    				pass = false;
    			}
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		

    		QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+Eselon1.NAMA+" ='"+data.getNama()+"'");
			
			if(id.isPresent()) {
				if(!existSatker.equals(data.getNama())) {
					if(eselon1Mapper.getCount(param) > 0) {
						pass=false;
					}
				}
			}else {
				if(eselon1Mapper.getCount(param) > 0) {
					pass=false;
				}
			}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Duplikat data: "+data.getNama());
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
			
    		if(id.isPresent()) {
    			eselon1Mapper.update(data);
    		}else {
    			eselon1Mapper.insert(data);	
    		}
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }


    @RequestMapping(value="/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> delete(
			@PathVariable String id, 
			@RequestParam("delete-permanent") Optional<String> deletePermanent,
			HttpServletRequest request
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Eselon1 data = eselon1Mapper.getEntity(id);
		resp.setData(data);
		
		if(deletePermanent.isPresent() || request.isUserInRole("DEVELOPER")) {
			eselon1Mapper.delete(data);
		}else {
			eselon1Mapper.deleteTemp(id, extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
		}
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
}
