//package id.go.menlhk.bp2sdm.spjk.services;
//
//import java.io.FileInputStream;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//
//import com.itextpdf.text.Document;
//import com.itextpdf.text.pdf.PdfWriter;
//
//import id.go.menlhk.bp2sdm.spjk.core.BaseService;
//import id.go.menlhk.bp2sdm.spjk.core.Clause;
//import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
//import id.go.menlhk.bp2sdm.spjk.dao.PositionDao;
//import id.go.menlhk.bp2sdm.spjk.dao.RoleDao;
//import id.go.menlhk.bp2sdm.spjk.domain.Login;
//
//public interface AppManagerService extends BaseService {
//
//	public OAuth2Authentication getAuthentication();
//
//	public RoleDao getRoleDao();
//
//	public PositionDao getPositionDao();
//
//	// public ProfilSatkerDao getProfilSatkerDao();
//
//	// batas
//	public Login findActiveLoginByAccessToken(String accessToken);
//
//}
