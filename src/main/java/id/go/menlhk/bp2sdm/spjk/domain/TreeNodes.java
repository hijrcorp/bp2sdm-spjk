package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.ArrayList;
import java.util.List;

public class TreeNodes {

	private long id;
	private String text;
	private List<TreeNodes> nodes = new ArrayList<>();
	private int level;
	private long upperId;

	public TreeNodes() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<TreeNodes> getNodes() {
		return nodes;
	}

	public void setNodes(List<TreeNodes> nodes) {
		this.nodes = nodes;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public long getUpperId() {
		return upperId;
	}

	public void setUpperId(long upperId) {
		this.upperId = upperId;
	}

}
