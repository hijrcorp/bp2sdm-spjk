package id.go.menlhk.bp2sdm.spjk.domain.bc;

import java.io.Serializable;
import id.go.menlhk.bp2sdm.spjk.core.Metadata;

public class Position extends Metadata implements Serializable {

	private String name;
	private String description;

	public Position() {

	}

	public Position(int id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
