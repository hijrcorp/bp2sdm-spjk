package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Organisasi {
	public static final String KODE = "kode_organisasi";
	public static final String KODE_LEVEL_1 = "kode_level_1_organisasi";
	public static final String KODE_LEVEL_2 = "kode_level_2_organisasi";
	public static final String KODE_LEVEL_3 = "kode_level_3_organisasi";
	public static final String KODE_KELOMPOK = "kode_kelompok_organisasi";
	public static final String NAMA_KELOMPOK = "nama_kelompok_organisasi";
	public static final String NAMA = "nama_organisasi";
	public static final String TINGKAT_ESELON = "tingkat_eselon_organisasi";
	public static final String KODE_ESELON = "kode_eselon_organisasi";
	public static final String NAMA_ESELON = "nama_eselon_organisasi";
	public static final String KODE_JABATAN = "kode_jabatan_organisasi";
	public static final String NAMA_JABATAN = "nama_jabatan_organisasi";
	public static final String KODE_PROPINSI = "kode_propinsi_organisasi";
	public static final String NAMA_PROPINSI = "nama_propinsi_organisasi";

	private String kode;
	private String kodeLevel1;
	private String kodeLevel2;
	private String kodeLevel3;
	private String kodeKelompok;
	private String namaKelompok;
	private String nama;
	private String tingkatEselon;
	private String kodeEselon;
	private String namaEselon;
	private String kodeJabatan;
	private String namaJabatan;
	private String kodePropinsi;
	private String namaPropinsi;

	public Organisasi() {

	}

	@JsonProperty("kode")
	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	@JsonProperty("kode_level_1")
	public String getKodeLevel1() {
		return kodeLevel1;
	}

	public void setKodeLevel1(String kodeLevel1) {
		this.kodeLevel1 = kodeLevel1;
	}

	@JsonProperty("kode_level_2")
	public String getKodeLevel2() {
		return kodeLevel2;
	}

	public void setKodeLevel2(String kodeLevel2) {
		this.kodeLevel2 = kodeLevel2;
	}

	@JsonProperty("kode_level_3")
	public String getKodeLevel3() {
		return kodeLevel3;
	}

	public void setKodeLevel3(String kodeLevel3) {
		this.kodeLevel3 = kodeLevel3;
	}

	@JsonProperty("kode_kelompok")
	public String getKodeKelompok() {
		return kodeKelompok;
	}

	public void setKodeKelompok(String kodeKelompok) {
		this.kodeKelompok = kodeKelompok;
	}

	@JsonProperty("nama_kelompok")
	public String getNamaKelompok() {
		return namaKelompok;
	}

	public void setNamaKelompok(String namaKelompok) {
		this.namaKelompok = namaKelompok;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("tingkat_eselon")
	public String getTingkatEselon() {
		return tingkatEselon;
	}

	public void setTingkatEselon(String tingkatEselon) {
		this.tingkatEselon = tingkatEselon;
	}

	@JsonProperty("kode_eselon")
	public String getKodeEselon() {
		return kodeEselon;
	}

	public void setKodeEselon(String kodeEselon) {
		this.kodeEselon = kodeEselon;
	}

	@JsonProperty("nama_eselon")
	public String getNamaEselon() {
		return namaEselon;
	}

	public void setNamaEselon(String namaEselon) {
		this.namaEselon = namaEselon;
	}

	@JsonProperty("kode_jabatan")
	public String getKodeJabatan() {
		return kodeJabatan;
	}

	public void setKodeJabatan(String kodeJabatan) {
		this.kodeJabatan = kodeJabatan;
	}

	@JsonProperty("nama_jabatan")
	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	@JsonProperty("kode_propinsi")
	public String getKodePropinsi() {
		return kodePropinsi;
	}

	public void setKodePropinsi(String kodePropinsi) {
		this.kodePropinsi = kodePropinsi;
	}

	@JsonProperty("nama_propinsi")
	public String getNamaPropinsi() {
		return namaPropinsi;
	}

	public void setNamaPropinsi(String namaPropinsi) {
		this.namaPropinsi = namaPropinsi;
	}

	/**********************************************************************/
	
	public static final String KODE_INDUK = "kode_induk_organisasi";
	
	private String kodeIndukOrganisasi;

	@JsonProperty("kode_induk")
	public String getKodeIndukOrganisasi() {
		return kodeIndukOrganisasi;
	}

	public void setKodeIndukOrganisasi(String kodeIndukOrganisasi) {
		this.kodeIndukOrganisasi = kodeIndukOrganisasi;
	}
	
	
}
