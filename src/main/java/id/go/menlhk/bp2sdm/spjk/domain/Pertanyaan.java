package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pertanyaan {
	public static final String ID = "id_pertanyaan";
	public static final String KODE = "kode_pertanyaan";
	public static final String ID_UNIT_KOMPETENSI = "id_unit_kompetensi_pertanyaan";
	public static final String ISI = "isi_pertanyaan";
	public static final String KETERANGAN = "keterangan_pertanyaan";
	public static final String BOBOT = "bobot_pertanyaan";
	public static final String KUNCI_JAWABAN = "kunci_jawaban_pertanyaan";
	public static final String TIPE_SOAL = "tipe_soal_pertanyaan";
	public static final String DURASI = "durasi_pertanyaan";
	public static final String JENIS = "jenis_pertanyaan";
	public static final String ID_BIDANG_TEKNIS = "id_bidang_teknis_pertanyaan";
	public static final String FILE = "file_pertanyaan";
	
	private String id;
	private String kode;
	private String idUnitKompetensi;
	private String isi;
	private String keterangan;
	private Integer bobot;
	private Integer kunciJawaban;
	private String tipeSoal;
	private Integer durasi;
	private String jenis;
	private String idBidangTeknis;
	private String file;
	
	public Pertanyaan() {
	
	}
	
	public Pertanyaan(String id) {
		this.id = id;
	}
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonProperty("kode")
	public String getKode() {
		return kode;
	}
	
	public void setKode(String kode) {
		this.kode = kode;
	}
	
	@JsonProperty("id_unit_kompetensi")
	public String getIdUnitKompetensi() {
		return idUnitKompetensi;
	}
	
	public void setIdUnitKompetensi(String idUnitKompetensi) {
		this.idUnitKompetensi = idUnitKompetensi;
	}
	
	@JsonProperty("isi")
	public String getIsi() {
		return isi;
	}
	
	public void setIsi(String isi) {
		this.isi = isi;
	}
	
	@JsonProperty("keterangan")
	public String getKeterangan() {
		return keterangan;
	}
	
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	@JsonProperty("bobot")
	public Integer getBobot() {
		return bobot;
	}
	
	public void setBobot(Integer bobot) {
		this.bobot = bobot;
	}
	
	@JsonProperty("kunci_jawaban")
	public Integer getKunciJawaban() {
		return kunciJawaban;
	}
	
	public void setKunciJawaban(Integer kunciJawaban) {
		this.kunciJawaban = kunciJawaban;
	}
	
	@JsonProperty("tipe_soal")
	public String getTipeSoal() {
		return tipeSoal;
	}
	
	public void setTipeSoal(String tipeSoal) {
		this.tipeSoal = tipeSoal;
	}
	
	@JsonProperty("durasi")
	public Integer getDurasi() {
		return durasi;
	}
	
	public void setDurasi(Integer durasi) {
		this.durasi = durasi;
	}
	
	@JsonProperty("jenis")
	public String getJenis() {
		return jenis;
	}
	
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}
	
	@JsonProperty("id_bidang_teknis")
	public String getIdBidangTeknis() {
		return idBidangTeknis;
	}
	
	public void setIdBidangTeknis(String idBidangTeknis) {
		this.idBidangTeknis = idBidangTeknis;
	}

	@JsonProperty("file")
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}


	/**********************************************************************/

	public String getIsiAndFilename() {
		String temp=isi;
		if(file!=null) {
			//ctx+"/"+"files/"+value.id+"?filename="+value.file+""
			String ext = FilenameUtils.getExtension(file);
			temp+="<br/>"+"<img class='img-fluid' src='"+"/spektra/files/"+this.id+"?filename="+file+"'/>";
		}//"&preview=1"+
		return temp;
	}
	
	private List<PertanyaanJawaban> listPertanyaanJawaban;

	public List<PertanyaanJawaban> getListPertanyaanJawaban() {
		return listPertanyaanJawaban;
	}

	public void setListPertanyaanJawaban(List<PertanyaanJawaban> listPertanyaanJawaban) {
		this.listPertanyaanJawaban = listPertanyaanJawaban;
	}
	
	
}