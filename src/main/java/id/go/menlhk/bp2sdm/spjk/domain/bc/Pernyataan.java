package id.go.menlhk.bp2sdm.spjk.domain.bc;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pernyataan {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	public static final String ID_PERNYATAAN = "id_pernyataan";
	public static final String ASPEK_ID_PERNYATAAN = "aspek_id_pernyataan";
	public static final String KODE_PERNYATAAN = "kode_pernyataan";
	public static final String ISI_PERNYATAAN = "isi_pernyataan";
	public static final String KETERANGAN_PERNYATAAN = "keterangan_pernyataan";

	private String idPernyataan;
	private String aspekIdPernyataan;
	private String kodePernyataan;
	private String isiPernyataan;
	private String keteranganPernyataan;

	public Pernyataan() {

	}

	public Pernyataan(String id) {
		this.idPernyataan = id;
	}

	@JsonProperty(ID_PERNYATAAN)
	public String getIdPernyataan() {
		return idPernyataan;
	}

	public void setIdPernyataan(String idPernyataan) {
		this.idPernyataan = idPernyataan;
	}

	@JsonProperty(ASPEK_ID_PERNYATAAN)
	public String getAspekIdPernyataan() {
		return aspekIdPernyataan;
	}

	public void setAspekIdPernyataan(String aspekIdPernyataan) {
		this.aspekIdPernyataan = aspekIdPernyataan;
	}

	@JsonProperty(ISI_PERNYATAAN)
	public String getIsiPernyataan() {
		return isiPernyataan;
	}

	public void setIsiPernyataan(String isiPernyataan) {
		this.isiPernyataan = isiPernyataan;
	}

	@JsonProperty(KODE_PERNYATAAN)
	public String getKodePernyataan() {
		return kodePernyataan;
	}

	public void setKodePernyataan(String kodePernyataan) {
		this.kodePernyataan = kodePernyataan;
	}

	@JsonProperty(KETERANGAN_PERNYATAAN)
	public String getKeteranganPernyataan() {
		return keteranganPernyataan;
	}

	public void setKeteranganPernyataan(String keteranganPernyataan) {
		this.keteranganPernyataan = keteranganPernyataan;
	}

	/**********************************
	 * - End Generate -
	 ************************************/
	public static final String NAMA_ASPEK = "nama_aspek";
	public static final String NAMA_JENIS_AUDIT = "nama_jenis_audit";

	private String namaAspek;
	private String namaJenisAudit;

	private List<PernyataanJawaban> listPernyataanJawaban;

	@JsonProperty(NAMA_ASPEK)
	public String getNamaAspek() {
		return namaAspek;
	}

	public void setNamaAspek(String namaAspek) {
		this.namaAspek = namaAspek;
	}

	@JsonProperty(NAMA_JENIS_AUDIT)
	public String getNamaJenisAudit() {
		return namaJenisAudit;
	}

	public void setNamaJenisAudit(String namaJenisAudit) {
		this.namaJenisAudit = namaJenisAudit;
	}

	public List<PernyataanJawaban> getListPernyataanJawaban() {
		return listPernyataanJawaban;
	}

	public void setListPernyataanJawaban(List<PernyataanJawaban> listPernyataanJawaban) {
		this.listPernyataanJawaban = listPernyataanJawaban;
	}
}