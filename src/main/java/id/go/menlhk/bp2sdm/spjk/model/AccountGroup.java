package id.go.menlhk.bp2sdm.spjk.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountGroup {
	public static final String ID = "id_account_group";
	public static final String NAME = "name_account_group";
	public static final String DESCRIPTION = "description_account_group";
	public static final String APPLICATION_ID = "application_id_account_group";
	public static final String SEQUENCE = "sequence_account_group";

	private String id;
	private String name;
	private String description;
	private String applicationId;
	private Integer sequence;

	public AccountGroup() {

	}

	public AccountGroup(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("application_id")
	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	@JsonProperty("sequence")
	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}



	/**********************************************************************/
	
	public static final String COUNT = "count_account_group";
	
	private Application application = new Application();
	private int count;
	
	

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}



}
