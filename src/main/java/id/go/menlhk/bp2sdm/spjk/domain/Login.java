package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;

public class Login {

	public static final String ID = "id";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String CLIENT_ID = "client_id";
	public static final String USER_NAME = "user_name";
	public static final String REFRESH_TOKEN = "refresh_token";
	public static final String CREATED_TIME = "created_time";
	public static final String EXPIRE_TIME = "expire_time";
	public static final String TOKEN_OBJECT = "token_object";
	public static final String USER_ID = "user_id";
	public static final String ORGANIZATION_ID = "organization_id";

	private String id;
	private String accessToken;
	private String clientId;
	private String userName;
	private String refreshToken;
	private Date createdTime;
	private Date expireTime;
	private byte[] tokenObject;
	private String userId;
	private String organizationId;

	public Login() {
		// TODO Auto-generated constructor stub
	}

	public Login(String id) {
		// TODO Auto-generated constructor stub
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public byte[] getTokenObject() {
		return tokenObject;
	}

	public void setTokenObject(byte[] tokenObject) {
		this.tokenObject = tokenObject;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

}
