package id.go.menlhk.bp2sdm.spjk.repo;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

@Repository("smsMessaging")
@Configuration
public class SmsMessagingImpl implements MessagingService {

	// @Value("${security.oauth2.token.key}")
	private String url;

	@Override
	public void send(String destination, Map<String, Object> payload) throws Exception {
		// TODO Auto-generated method stub

		url = url.replace("{mobile}", destination);
		url = url.replace("{message}", payload.get("message").toString());
		// new RestTemplate().getForObject(url, String.class);
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		System.out.println(con.getResponseCode());
	}

}
