package id.go.menlhk.bp2sdm.spjk.services;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.DetilIndividu;
import id.go.menlhk.bp2sdm.spjk.domain.Eselon1;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanManajerial;
import id.go.menlhk.bp2sdm.spjk.domain.PertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.RataRataKompetensiIndividu;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.mapper.Eselon1Mapper;
import id.go.menlhk.bp2sdm.spjk.mapper.JabatanKompetensiTeknisMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.JabatanManajerialMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.RataRataKompetensiIndividuMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@Service
public class RestQLTriggerService {

	@Autowired
	private JabatanManajerialMapper jabatanManajerialMapper;

	@Autowired
	private PertanyaanJawabanMapper pertanyaanJawabanMapper;

	@Autowired
	private JabatanKompetensiTeknisMapper jabatanKompetensiTeknisMapper;

	@Autowired
	private UnitAuditiMapper unitAuditiMapper;

	@Autowired
	private Eselon1Mapper eselon1Mapper;

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private RataRataKompetensiIndividuMapper rataKompetensiIndividuMapper;
	
	public void onAfterInsert(String entity, Map<String, Object> data) {
		System.out.println("onAfterInsert: >> call another service method: " + data.get("id"));
		
	}
	
	public void onAfterUpdate(String entity, Map<String, Object> data, Map<String, Object> prev) {
		System.out.println("onAfterUpdate: >> call another service method");
		System.out.println(prev.get("alamat_rumah") + " >> DIFF << " + data.get("alamat_rumah"));
	}
	
	public void onAfterDelete(String entity, Map<String, Object> data) {
		System.out.println("onAfterDelete: >> call another service method: " + data.get("id"));
		if(entity.equals("jabatan")) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+JabatanManajerial.ID_MASTER_JABATAN+" ='"+data.get("id")+"'");
			jabatanManajerialMapper.deleteBatch(param);
		}
		if(entity.equals("pertanyaan")) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+PertanyaanJawaban.ID_HEADER+" ='"+data.get("id")+"'");
			pertanyaanJawabanMapper.deleteBatch(param);
		}
	}
	
	public StringBuilder onGetUnitAuditiByUser(String entity, String idAccount, StringBuilder sql) {
		//StringBuilder sql = new StringBuilder();
		String idUnitAuditi=userMapper.findUnitAuditiByUserId(idAccount).get(0);
		//String idEselon1=userMapper.findEselon1ByUnitAuditiUserId(idAccount).get(0);
		//sql.append("='").append(idUnitAuditi).append("'");
		//return idUnitAuditi;
		///
		if(entity.equals("progress_peserta_ujian_detil")) {
			sql.append(" AND id_unit_auditi_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}
		if(entity.equals("view_mapping_jabatan_unit_auditi")) {
			sql.append(" AND id_unit_auditi_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}
		if(entity.equals("detil_perjabatan")) {
			sql.append(" AND id_unit_kerja_user_responden_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}
		if(entity.equals("rata_rata_individu_existing")) {
			sql.append(" AND id_unit_auditi_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}
		if(entity.equals("rata_rata_kompetensi_individu")) {
			sql.append(" AND id_unit_auditi_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}
		if(entity.equals("unit_auditi")) {
			sql.append(" AND id_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}
		
		return sql;
	}
	
	public StringBuilder onGetEselon1ByUser(String entity, String idAccount, StringBuilder sql) {
		String idEselon1=userMapper.findEselon1ByUserId(idAccount).get(0);
		
		if(entity.equals("progress_peserta_ujian_detil")) {
			sql.append(" AND id_eselon1_responden_").append(entity);
			sql.append("='").append(idEselon1).append("'");
		}
		
		
		if(entity.equals("detil_perjabatan")) {
			sql.append(" AND id_eselon1_user_responden_").append(entity);
			sql.append("='").append(idEselon1).append("'");
		}
		if(entity.equals("rata_rata_individu_existing")) {
			sql.append(" AND id_eselon1_").append(entity);
			sql.append("='").append(idEselon1).append("'");
		}
		/*if(entity.equals("rata_rata_kompetensi_individu")) {
			sql.append(" AND id_unit_auditi_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}*/
		
		if(entity.equals("unit_auditi")) {
			sql.append(" AND id_eselon1_").append(entity);
			sql.append("='").append(idEselon1).append("'");
		}
		if(entity.equals("eselon1")) {
			sql.append(" AND id_").append(entity);
			sql.append("='").append(idEselon1).append("'");
		}
		return sql;
	}
	
	public StringBuilder onGetOrganisasiByUser(String entity, String idAccount, StringBuilder sql, Optional<String> filter) {
		//String idEselon1=userMapper.findEselon1ByUserId(idAccount).get(0);
		String idProp = userMapper.findOrganisasiPropinsiByUserId(idAccount).get(0);

		/*if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + UnitAuditi.ID_PROPINSI_UNIT_AUDITI + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}*/
		
		if(entity.equals("progress_peserta_ujian_detil")) {
			sql.append(" AND id_propinsi_").append("unit_auditi");
			sql.append(" IN (").append(idProp).append(")");
		}
		
		
		if(entity.equals("detil_perjabatan")) {
			sql.append(" AND id_propinsi_unit_auditi_").append(entity);
			sql.append(" IN (").append(idProp).append(")");
		}
		if(entity.equals("rata_rata_individu_existing")) {
			sql.append(" AND id_propinsi_").append(entity);
			sql.append(" IN (").append(idProp).append(")");
		}
		if(entity.equals("view_mapping_jabatan_unit_auditi")) {
			sql.append(" AND id_propinsi_").append("unit_auditi");
			sql.append(" IN (").append(idProp).append(")");
		}
		
		/*if(entity.equals("rata_rata_kompetensi_individu")) {
			sql.append(" AND id_unit_auditi_").append(entity);
			sql.append("='").append(idUnitAuditi).append("'");
		}*/
		
		
		if(entity.equals("unit_auditi")) {
			sql.append(" AND id_propinsi_").append(entity);

			String otherColumn="";
			String otherValue="";
			if(filter.isPresent()) {
				String[] cols = filter.get().split(",(?=[^\\)]*(\\(|$))");
				for(String c: cols) {

					String[] f = c.split("\\.");
					
					System.out.println("f[0] "+ f[0]);
					otherColumn=f[0];
					if(f[1].toLowerCase().startsWith("eq")) {
						System.out.println("f[1].substring(3, f[1].length()-1) "+ f[1].substring(3, f[1].length()-1));
						otherValue=f[1].substring(3, f[1].length()-1);
					}
					
				}
			}
			if(otherColumn=="") {
				sql.append(" IN (").append(idProp).append(")");
			}
		}
		if(entity.equals("propinsi")) {
			sql.append(" AND id_").append(entity);
			sql.append(" IN (").append(idProp).append(")");
		}
		if(entity.equals("mapping_organisasi")) {
			String idOrganisasi = userMapper.findOrganisasiByUserId(idAccount).get(0);
			sql.append(" AND id_").append(entity);
			sql.append(" IN (").append(idOrganisasi).append(")");
		}
		
		return sql;
	}

	public StringBuilder onGetIdRespondenByUser(String entity, StringBuilder sql, String idAccount, Optional<String> filter) {

		String idReference = userMapper.findNIPByUserId(idAccount).get(0);

		if(entity.equals("rata_rata_kompetensi_individu")) {
			sql.append(" AND nip_").append(entity);
			sql.append(" = '").append(idReference).append("'");
			

			String filterKodeSesi=getFilterBy(filter, "kode_sesi");
			sql.append(" AND kode_sesi_").append(entity);
			sql.append(" = ").append(filterKodeSesi).append("");
		}
		return sql;
	}
	
	public String getFilterBy(Optional<String> filter, String param) {
		String otherValue="";
		if(filter.isPresent()) {
			String[] cols = filter.get().split(",(?=[^\\)]*(\\(|$))");
			for(String c: cols) {
				String[] f = c.split("\\.");
				System.out.println("f[0] "+ f[0]);
				if(f[1].toLowerCase().startsWith("eq")) {
					System.out.println("f[1].substring(3, f[1].length()-1) "+ f[1].substring(3, f[1].length()-1));
					
					if(f[0].equals(param)) otherValue=f[1].substring(3, f[1].length()-1);
				}
				
			}
		}
		System.out.println("LOL "+otherValue);
		return otherValue;
	}
	
	public StringBuilder onGetRespondenByUser(String entity, String idAccount, StringBuilder sql, Optional<String> filter) {
		//String idEselon1=userMapper.findEselon1ByUserId(idAccount).get(0);
		String idReference = userMapper.findNIPByUserId(idAccount).get(0);

		if(entity.equals("rata_rata_kompetensi_individu")) {
			sql.append(" AND nip_").append(entity);
			sql.append(" IN (").append(idReference).append(")");
			//sql.append(" = '").append(idReference).append("'");
		}

		if(entity.equals("detil_individu")) {
			QueryParameter paramOwn = new QueryParameter();
			String filterKodeSesi=getFilterBy(filter, "kode_sesi");
			paramOwn.setClause(paramOwn.getClause()+" AND "+RataRataKompetensiIndividu.KODE_SESI+" ="+filterKodeSesi+"");
			paramOwn.setClause(paramOwn.getClause()+" AND "+RataRataKompetensiIndividu.NIP+" ='"+idReference+"'");
			
			String idResponden=rataKompetensiIndividuMapper.getList(paramOwn).get(0).getIdUserResponden();
			
			sql.append(" AND id_user_responden_").append(entity);
			sql.append(" = '").append(idResponden).append("'");
		}
		return sql;
	}
	
	public StringBuilder onGetUnitPembinaByUser(String entity, String idAccount, StringBuilder sql, Optional<String> filter) {
		//String idEselon1=userMapper.findEselon1ByUserId(idAccount).get(0);
		String idReference = userMapper.findReferenceByUserId(idAccount, "UNIT_PEMBINA").get(0);

		/*if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + UnitAuditi.ID_PROPINSI_UNIT_AUDITI + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}*/
		//id_kelompok_jabatan_progress_peserta_ujian_detil
		if(entity.equals("progress_peserta_ujian_detil")) {
			sql.append(" AND id_kelompok_jabatan_").append(entity);
			sql.append(" IN (").append(idReference).append(")");
		}
		/*
		if(entity.equals("detil_perjabatan")) {
			sql.append(" AND id_propinsi_unit_auditi_").append(entity);
			sql.append(" IN (").append(idProp).append(")");
		}
		
		if(entity.equals("rata_rata_individu_existing")) {
			sql.append(" AND id_propinsi_").append(entity);
			sql.append(" IN (").append(idProp).append(")");
		}
		
		if(entity.equals("view_mapping_jabatan_unit_auditi")) {
			sql.append(" AND id_propinsi_").append("unit_auditi");
			sql.append(" IN (").append(idProp).append(")");
		}
		
		//if(entity.equals("rata_rata_kompetensi_individu")) {
		//	sql.append(" AND id_unit_auditi_").append(entity);
		//	sql.append("='").append(idUnitAuditi).append("'");
		//}
		
		
		if(entity.equals("unit_auditi")) {
			sql.append(" AND id_propinsi_").append(entity);

			String otherColumn="";
			String otherValue="";
			if(filter.isPresent()) {
				String[] cols = filter.get().split(",(?=[^\\)]*(\\(|$))");
				for(String c: cols) {

					String[] f = c.split("\\.");
					
					System.out.println("f[0] "+ f[0]);
					otherColumn=f[0];
					if(f[1].toLowerCase().startsWith("eq")) {
						System.out.println("f[1].substring(3, f[1].length()-1) "+ f[1].substring(3, f[1].length()-1));
						otherValue=f[1].substring(3, f[1].length()-1);
					}
					
				}
			}
			if(otherColumn=="") {
				sql.append(" IN (").append(idProp).append(")");
			}
		}
		
		if(entity.equals("propinsi")) {
			sql.append(" AND id_").append(entity);
			sql.append(" IN (").append(idProp).append(")");
		}
		
		if(entity.equals("mapping_organisasi")) {
			String idOrganisasi = userMapper.findOrganisasiByUserId(idAccount).get(0);
			sql.append(" AND id_").append(entity);
			sql.append(" IN (").append(idOrganisasi).append(")");
		}*/
		
		return sql;
	}
	
	
	public boolean onCheck(String entity, Map<String, Object> data) {
		//System.out.println("onAfterDelete: >> call another service method: " + data.get("id"));
		boolean pass=true;
		if(entity.equals("unit_kompetensi_teknis")) {
			if(data.get("kode").toString().equals("")) {
				pass=false;
			}
		}
		if(entity.equals("kelompok_jabatan")) {
			if(data.get("nama").toString().equals("")) {
				pass=false;
			}
		}
		if(entity.equals("tingkat_jabatan")) {
			if(data.get("nama").toString().equals("")) {
				pass=false;
			}
		}
		if(entity.equals("jabatan_kompetensi_teknis")) {
			if(data.get("id_master_jabatan").toString().equals("") || data.get("id_unit_kompetensi_teknis").toString().equals("")) {
				pass=false;
			}
		}
		return pass;
	}
	
	public boolean onNewCheck(String[] data) {
		boolean pass=true;
		//dataMainColumn
		for(String column : data) {
			if(column=="") pass=false;
		}
		return pass;
	}
	
	public boolean onCheckDuplicate(String entity, Map<String, Object> data) {
		//System.out.println("onAfterDelete: >> call another service method: " + data.get("id"));
		boolean pass=true;
		if(entity.equals("jabatan_kompetensi_teknis")) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+JabatanKompetensiTeknis.ID_MASTER_JABATAN+" ='"+data.get("id_master_jabatan").toString()+"'");
			param.setClause(param.getClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+data.get("id_bidang_teknis").toString()+"'");
			param.setClause(param.getClause()+" AND "+JabatanKompetensiTeknis.ID_UNIT_KOMPETENSI_TEKNIS+" ='"+data.get("id_unit_kompetensi_teknis").toString()+"'");
			if(jabatanKompetensiTeknisMapper.getCount(param) > 0) {
			//data.get("id_master_jabatan").toString().equals("") || data.get("id_kelompok_teknis").toString().equals("") || data.get("id_unit_kompetensi_teknis").toString().equals("")) 
				pass=false;
			}
		}
		if(entity.equals("unit_auditi")) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+UnitAuditi.NAMA_UNIT_AUDITI+" ='"+data.get("nama").toString()+"'");
			if(unitAuditiMapper.getCount(param) > 0) {
				if(data.get("id")==null)
					pass=false;
				else {
					 String existSatker = unitAuditiMapper.getEntity(data.get("id").toString()).getNamaUnitAuditi();
					 if(!existSatker.equals(data.get("nama"))) {
						 pass=false;
					 }
				}
			}
		}
		return pass;
	}
	
}
