package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.go.menlhk.bp2sdm.spjk.model.Account;

public class DetilIndividu {
	public static final String ID = "id_detil_individu";
	public static final String ID_USER_RESPONDEN = "id_user_responden_detil_individu";
	public static final String ID_UNIT_KOMPETENSI = "id_unit_kompetensi_detil_individu";
	public static final String KODE_UNIT_KOMPETENSI = "kode_unit_kompetensi_detil_individu";
	public static final String NAMA_UNIT_KOMPETENSI = "nama_unit_kompetensi_detil_individu";
	public static final String NAMA_KELOMPOK = "nama_kelompok_detil_individu";
	public static final String NILAI_STANDAR = "nilai_standar_detil_individu";
	public static final String NILAI = "nilai_detil_individu";
	public static final String PENGEMBANGAN = "pengembangan_detil_individu";
	public static final String JENIS_KOMPETENSI = "jenis_kompetensi_detil_individu";
	public static final String KODE_SESI = "kode_sesi_detil_individu";

	private String id;
	private String idUserResponden;
	private String idUnitKompetensi;
	private String kodeUnitKompetensi;
	private String namaUnitKompetensi;
	private String namaKelompok;
	private Double nilaiStandar;
	private Double nilai;
	private String pengembangan;
	private String jenisKompetensi;
	private String kodeSesi;

	public DetilIndividu() {

	}

	public DetilIndividu(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_user_responden")
	public String getIdUserResponden() {
		return idUserResponden;
	}

	public void setIdUserResponden(String idUserResponden) {
		this.idUserResponden = idUserResponden;
	}

	@JsonProperty("id_unit_kompetensi")
	public String getIdUnitKompetensi() {
		return idUnitKompetensi;
	}

	public void setIdUnitKompetensi(String idUnitKompetensi) {
		this.idUnitKompetensi = idUnitKompetensi;
	}

	@JsonProperty("kode_unit_kompetensi")
	public String getKodeUnitKompetensi() {
		return kodeUnitKompetensi;
	}

	public void setKodeUnitKompetensi(String kodeUnitKompetensi) {
		this.kodeUnitKompetensi = kodeUnitKompetensi;
	}

	@JsonProperty("nama_unit_kompetensi")
	public String getNamaUnitKompetensi() {
		return namaUnitKompetensi;
	}

	public void setNamaUnitKompetensi(String namaUnitKompetensi) {
		this.namaUnitKompetensi = namaUnitKompetensi;
	}

	@JsonProperty("nama_kelompok")
	public String getNamaKelompok() {
		return namaKelompok;
	}

	public void setNamaKelompok(String namaKelompok) {
		this.namaKelompok = namaKelompok;
	}

	@JsonProperty("nilai_standar")
	public Double getNilaiStandar() {
		return nilaiStandar;
	}

	public void setNilaiStandar(Double nilaiStandar) {
		this.nilaiStandar = nilaiStandar;
	}

	@JsonProperty("nilai")
	public Double getNilai() {
		return nilai;
	}

	public void setNilai(Double nilai) {
		this.nilai = nilai;
	}

	@JsonProperty("pengembangan")
	public String getPengembangan() {
		return pengembangan;
	}

	public void setPengembangan(String pengembangan) {
		this.pengembangan = pengembangan;
	}

	@JsonProperty("jenis_kompetensi")
	public String getJenisKompetensi() {
		return jenisKompetensi;
	}

	public void setJenisKompetensi(String jenisKompetensi) {
		this.jenisKompetensi = jenisKompetensi;
	}

	@JsonProperty("kode_sesi")
	public String getKodeSesi() {
		return kodeSesi;
	}

	public void setKodeSesi(String kodeSesi) {
		this.kodeSesi = kodeSesi;
	}
	
	//costume

	public static final String ID_ESELON1 = "id_eselon1_detil_individu";
	public static final String ID_PROPINSI = "id_propinsi_detil_individu";
	public static final String ID_UNIT_AUDITI = "id_unit_auditi_detil_individu";
	public static final String NILAI_ALT = "nilai_alt_detil_individu";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_detil_individu";
	public static final String ID_TINGKAT_JABATAN = "id_tingkat_jabatan_detil_individu";
	public static final String ID_JENJANG_JABATAN = "id_jenjang_jabatan_detil_individu";

	private Double nilaiStandarAlt;
	private Double nilaiAlt;
	private String idEselon1;
	private String idUnitAuditi;
	private String idKelompokJabatan;
	private String idTingkatJabatan;
	private String idJenjangJabatan;
	private Integer jumlahUnitKomp;

	@JsonProperty("nilai_alt")
	public Double getNilaiAlt() {
		return nilaiAlt;
	}

	public void setNilaiAlt(Double nilaiAlt) {
		this.nilaiAlt = nilaiAlt;
	}
	
	@JsonProperty("nilai_standar_alt")
	public Double getNilaiStandarAlt() {
		return nilaiStandarAlt;
	}

	public void setNilaiStandarAlt(Double nilaiStandarAlt) {
		this.nilaiStandarAlt = nilaiStandarAlt;
	}

	@JsonProperty("id_eselon1")
	public String getIdEselon1() {
		return idEselon1;
	}

	public void setIdEselon1(String idEselon1) {
		this.idEselon1 = idEselon1;
	}

	@JsonProperty("id_unit_auditi")
	public String getIdUnitAuditi() {
		return idUnitAuditi;
	}

	public void setIdUnitAuditi(String idUnitAuditi) {
		this.idUnitAuditi = idUnitAuditi;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	@JsonProperty("id_tingkat_jabatan")
	public String getIdTingkatJabatan() {
		return idTingkatJabatan;
	}

	public void setIdTingkatJabatan(String idTingkatJabatan) {
		this.idTingkatJabatan = idTingkatJabatan;
	}

	@JsonProperty("id_jenjang_jabatan")
	public String getIdJenjangJabatan() {
		return idJenjangJabatan;
	}

	public void setIdJenjangJabatan(String idJenjangJabatan) {
		this.idJenjangJabatan = idJenjangJabatan;
	}

	@JsonProperty("jumlah_unit_komp")
	public Integer getJumlahUnitKomp() {
		return jumlahUnitKomp;
	}

	public void setJumlahUnitKomp(Integer jumlahUnitKomp) {
		this.jumlahUnitKomp = jumlahUnitKomp;
	}
	
	
	
}
