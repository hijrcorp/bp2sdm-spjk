package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonProperty;

public class JabatanKompetensiTeknis {

	public static final String ID = "id_jabatan_kompetensi_teknis";
	public static final String ID_MASTER_JABATAN = "id_master_jabatan_jabatan_kompetensi_teknis";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_jabatan_kompetensi_teknis";
	public static final String ID_BIDANG_TEKNIS = "id_bidang_teknis_jabatan_kompetensi_teknis";
	public static final String ID_KELOMPOK_TEKNIS = "id_kelompok_teknis_jabatan_kompetensi_teknis";
	public static final String ID_UNIT_KOMPETENSI_TEKNIS = "id_unit_kompetensi_teknis_jabatan_kompetensi_teknis";
	public static final String JENIS_KOMPETENSI = "jenis_kompetensi_jabatan_kompetensi_teknis";
	public static final String ID_SOURCE = "id_source_jabatan_kompetensi_teknis";
	public static final String ID_ACCOUNT_ADDED = "id_account_added_jabatan_kompetensi_teknis";
	public static final String TIMESTAMP_ADDED = "timestamp_added_jabatan_kompetensi_teknis";
	public static final String ID_ACCOUNT_MODIFIED = "id_account_modified_jabatan_kompetensi_teknis";
	public static final String TIMESTAMP_MODIFIED = "timestamp_modified_jabatan_kompetensi_teknis";

	private String id;
	private String idMasterJabatan;
	private String idKelompokJabatan;
	//
	private String namaKelompokJabatan;
	private String judulUnitKompetensiTeknis;
	private String kodeUnitKompetensiTeknis;
	//
	private String idBidangTeknis;
	private String idKelompokTeknis;
	private String idUnitKompetensiTeknis;
	private String jenisKompetensi;
	private String idSource;
	private String idAccountAdded;
	private Date timestampAdded;
	private String idAccountModified;
	private Date timestampModified;

	private Integer jumlahPertanyaan;

	public JabatanKompetensiTeknis() {

	}

	public JabatanKompetensiTeknis(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_master_jabatan")
	public String getIdMasterJabatan() {
		return idMasterJabatan;
	}

	public void setIdMasterJabatan(String idMasterJabatan) {
		this.idMasterJabatan = idMasterJabatan;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}
	
	@JsonProperty("nama_kelompok_jabatan")
	public String getNamaKelompokJabatan() {
		return namaKelompokJabatan;
	}

	public void setNamaKelompokJabatan(String namaKelompokJabatan) {
		this.namaKelompokJabatan = namaKelompokJabatan;
	}
	
	@JsonProperty("judul_kompetensi_teknis")
	public String getJudulUnitKompetensiTeknis() {
		return judulUnitKompetensiTeknis;
	}

	public void setJudulUnitKompetensiTeknis(String judulUnitKompetensiTeknis) {
		this.judulUnitKompetensiTeknis = judulUnitKompetensiTeknis;
	}
	
	@JsonProperty("kode_kompetensi_teknis")
	public String getKodeUnitKompetensiTeknis() {
		return kodeUnitKompetensiTeknis;
	}

	public void setKodeUnitKompetensiTeknis(String kodeUnitKompetensiTeknis) {
		this.kodeUnitKompetensiTeknis = kodeUnitKompetensiTeknis;
	}

	@JsonProperty("id_bidang_teknis")
	public String getIdBidangTeknis() {
		return idBidangTeknis;
	}

	public void setIdBidangTeknis(String idBidangTeknis) {
		this.idBidangTeknis = idBidangTeknis;
	}

	@JsonProperty("id_kelompok_teknis")
	public String getIdKelompokTeknis() {
		return idKelompokTeknis;
	}

	public void setIdKelompokTeknis(String idKelompokTeknis) {
		this.idKelompokTeknis = idKelompokTeknis;
	}

	@JsonProperty("id_unit_kompetensi_teknis")
	public String getIdUnitKompetensiTeknis() {
		return idUnitKompetensiTeknis;
	}

	public void setIdUnitKompetensiTeknis(String idUnitKompetensiTeknis) {
		this.idUnitKompetensiTeknis = idUnitKompetensiTeknis;
	}

	@JsonProperty("jenis_kompetensi")
	public String getJenisKompetensi() {
		return jenisKompetensi;
	}

	public void setJenisKompetensi(String jenisKompetensi) {
		this.jenisKompetensi = jenisKompetensi;
	}

	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	@JsonProperty("id_account_added")
	public String getIdAccountAdded() {
		return idAccountAdded;
	}

	public void setIdAccountAdded(String idAccountAdded) {
		this.idAccountAdded = idAccountAdded;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}

	@JsonProperty("id_account_modified")
	public String getIdAccountModified() {
		return idAccountModified;
	}

	public void setIdAccountModified(String idAccountModified) {
		this.idAccountModified = idAccountModified;
	}

	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}

	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}

	@JsonProperty("jumlah_pertanyaan")
	public Integer getJumlahPertanyaan() {
		return jumlahPertanyaan;
	}

	public void setJumlahPertanyaan(Integer jumlahPertanyaan) {
		this.jumlahPertanyaan = jumlahPertanyaan;
	}
	
	
}
