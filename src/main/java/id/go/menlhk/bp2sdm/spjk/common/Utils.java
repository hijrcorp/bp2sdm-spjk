package id.go.menlhk.bp2sdm.spjk.common;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

public class Utils {

	private static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;

	public static String getUUIDString() {
		NoArgGenerator timeBasedGenerator = Generators.timeBasedGenerator();

		// Generate time based UUID
		UUID firstUUID = timeBasedGenerator.generate();
		return firstUUID.toString();
	}

	public static Date getUUIDDate(String uuidString) {
		return new Date((UUID.fromString(uuidString).timestamp() - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000);
	}

	public static long getDateDiffFromNow(Date d1) {
		Date d2 = new Date();
		long seconds = (d2.getTime() - d1.getTime()) / 1000;
		return seconds;
	}

	public static Dimension getImageDimension(File imgFile) throws IOException {
		int pos = imgFile.getName().lastIndexOf(".");
		if (pos == -1)
			throw new IOException("No extension for file: " + imgFile.getAbsolutePath());
		String suffix = imgFile.getName().substring(pos + 1);
		Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
		while (iter.hasNext()) {
			ImageReader reader = iter.next();
			try {
				ImageInputStream stream = new FileImageInputStream(imgFile);
				reader.setInput(stream);
				int width = reader.getWidth(reader.getMinIndex());
				int height = reader.getHeight(reader.getMinIndex());
				return new Dimension(width, height);
			} catch (IOException e) {
				System.out.println("Error reading: " + imgFile.getAbsolutePath());
			} finally {
				reader.dispose();
			}
		}

		throw new IOException("Not a known image file: " + imgFile.getAbsolutePath());
	}

	public static int randomPlus() {
		int min = 100;
		int max = 999;

		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}
	
	public static String getLongNumberID() {
		return (new Date().getTime() + "" + randomPlus());
	}

	public static String getFullURL(HttpServletRequest request) {
		StringBuffer requestURL = request.getRequestURL();
		String queryString = request.getQueryString();

		if (queryString == null) {
			return requestURL.toString() + "?1";
		} else {
			return requestURL.append('?').append(queryString).toString();
		}
	}

	public static String formatSqlDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		return sdf.format(date);
	}

	public static Date formatDate(String date) throws ParseException {
		DateFormat format = new SimpleDateFormat("dd MMMM yyyy");
		Date dateFormat = null;
		if (!date.equals("")) {
			dateFormat = format.parse(date);
		}
		return dateFormat;
	}

	public static Date formatDate(String date, String myformat) throws ParseException {
		DateFormat format = new SimpleDateFormat(myformat);
		Date dateFormat = null;
		if (!date.equals("")) {
			dateFormat = format.parse(date);
		}
		return dateFormat;
	}

	public static String getFullFormattedDate(Date date) {
		try {
			String strDate = new SimpleDateFormat("dd MMMM yyyy").format(date);
			strDate = strDate.replace("January", "Januari");
			strDate = strDate.replace("February", "Febuari");
			strDate = strDate.replace("March", "Maret");
			strDate = strDate.replace("April", "April");
			strDate = strDate.replace("May", "Mei");
			strDate = strDate.replace("June", "Juni");
			strDate = strDate.replace("July", "Juli");
			strDate = strDate.replace("August", "Agustus");
			strDate = strDate.replace("September", "September");
			strDate = strDate.replace("October", "Oktober");
			strDate = strDate.replace("November", "November");
			strDate = strDate.replace("December", "Desember");
			return strDate;
		} catch (Exception e) {
			return "";
		}
	}
	
	public static boolean ip(String text) {
	    Pattern p = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
	    Matcher m = p.matcher(text);
	    return m.find();
	}
	
	public static String toAlphabetic(int i) {
	    if( i<0 ) {
	        return "-"+toAlphabetic(-i-1);
	    }

	    int quot = i/26;
	    int rem = i%26;
	    char letter = (char)((int)'A' + rem);
	    if( quot == 0 ) {
	        return ""+letter;
	    } else {
	        return toAlphabetic(quot-1) + letter;
	    }
	}
	
	public static boolean dateValidation(String date, String formatdate)
	    {
	      boolean status = false;
	    if (checkDate(date)) {
	      DateFormat dateFormat = new SimpleDateFormat(formatdate);//("dd/MM/yyyy");
	      dateFormat.setLenient(false);
	      try {
	        dateFormat.parse(date);
	        status = true;
	      } catch (Exception e) {
	        status = false;
	      }
	    }
	    return status;
	    }
	    
	 public static boolean checkDate(String date) {
	    String pattern = "(0?[1-9]|[12][0-9]|3[01])\\/(0?[1-9]|1[0-2])\\/([0-9]{4})";
	    boolean flag = false;
	    if (date.matches(pattern)) {
	      flag = true;
	    }
	    return flag;
	  }
	 
	 public static String MD5(String md5) {
			try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				byte[] array = md.digest(md5.getBytes());
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < array.length; ++i) {
					sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
				}
				return sb.toString();
			} catch (java.security.NoSuchAlgorithmException e) {
			}
			return null;
		}

		public static String getSIMPEGApiKey(String OP) {
			final String DATE = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
			final String TEXT = "simpeg_" + OP + "@77" + DATE;
			final String API_KEY = Utils.MD5(TEXT);
			return API_KEY;
		}
		public static String getSIMPEGURLPegawai(String nip) throws Exception {
			final String OP = "pegawai";
			final String API_KEY = getSIMPEGApiKey(OP);
			final String simpegUrl = "https://simpeg.menlhk.go.id/";
			final String URL = simpegUrl+"json.php?op="+OP+"&api_key="+API_KEY+"&nip="+nip;
			return URL;
		}
}
