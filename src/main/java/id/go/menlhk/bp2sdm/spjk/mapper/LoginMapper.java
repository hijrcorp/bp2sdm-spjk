package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Login;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthClient;

@Mapper
public interface LoginMapper {

//	@Insert("insert into tbl_login (id, access_token, client_id, user_name, refresh_token, created_time, expire_time, token_object) values (#{id}, #{accessToken}, #{clientId}, #{userName}, #{refreshToken}, now(), #{expireTime}, #{tokenObject})")
//	void insert(Login login);
//
//	@Select("select * from tbl_login where access_token=#{accessToken} and expire_time > now()")
//	Login findActiveLoginByAccessToken(@Param("accessToken") String accessToken);
//
//	@Select("select * from tbl_login where refresh_token=#{refreshToken}")
//	Login findLoginByRefreshToken(@Param("refreshToken") String refreshToken);
//
//	@Select("select * from tbl_login where client_id=#{clientId} and user_name=#{userName} and expire_time > now()")
//	Login findActiveLoginByClientAndUser(@Param("clientId") String clientId, @Param("userName") String userName);
//
//	@Delete("delete from tbl_login where id=#{id}")
//	void deleteActiveLogin(Login login);
//
//	@Insert("insert into tbl_login_history (id, access_token, client_id, user_name, refresh_token, created_time, expire_time, token_object, status, user_id, organization_id) values (#{id}, #{accessToken}, #{clientId}, #{userName}, #{refreshToken}, #{createdTime}, #{expireTime}, #{tokenObject}, #{status}, #{userId}, #{organizationId})")
//	void insertLoginHistory(LoginHistory loginHistory);

	@Select("select * from oauth_client_details where client_id=#{clientId} and client_secret=#{clientSecret}")
	OauthClient findOauthClient(@Param("clientId") String clientId, @Param("clientSecret") String clientSecret);

	@Select("select * from oauth_client_details where client_id=#{clientId}")
	OauthClient findOauthClientById(@Param("clientId") String clientId);

//	@Insert("insert into tbl_login_request (id, client_id, request_token, callback, request_time) values (#{id}, #{clientId}, #{requestToken}, #{callback}, now())")
//	void insertLoginRequest(LoginRequest loginRequest);

	@Select("select a.* from oauth_client_details a inner join tbl_login_request b where a.client_id=b.client_id and a.client_id=#{clientId} and request_token=#{requestToken} and callback=#{callback}")
	OauthClient findOauthClientByLoginRequest(@Param("clientId") String clientId,
			@Param("requestToken") String requestToken, @Param("callback") String callback);

//	@Insert("insert into tbl_mobile_token (fcm_token) values (#{fcmToken})")
//	void insertMobileToken(@Param("fcmToken") String fcmToken);
//
//	@Update("insert into tbl_mobile_token (user_id, fcm_token) values (#{userId}, #{fcmToken}) on duplicate key update user_id=#{userId}")
//	void updateMobileUserToken(@Param("userId") String userId, @Param("fcmToken") String fcmToken);
//
//	@Select("select fcm_token from tbl_mobile_token where user_id=#{userId}")
//	String findMobilToken(@Param("userId") String userId);
	
	/**********************************************************************/
	@Insert("INSERT INTO hijr_login (id_login, access_token_login, client_id_login, username_login, refresh_token_login, created_time_login, expire_time_login, token_object_login, account_id_login, source_id_login) VALUES (#{id:VARCHAR}, #{accessToken}, #{clientId:VARCHAR}, #{username:VARCHAR}, #{refreshToken}, #{createdTime:TIMESTAMP}, #{expireTime:TIMESTAMP}, #{tokenObject:BLOB}, #{accountId:VARCHAR}, #{sourceId:VARCHAR})")
	void insert(Login login);

	@Update("UPDATE hijr_login SET id_login=#{id:VARCHAR}, access_token_login=#{accessToken}, client_id_login=#{clientId:VARCHAR}, username_login=#{username:VARCHAR}, refresh_token_login=#{refreshToken}, created_time_login=#{createdTime:TIMESTAMP}, expire_time_login=#{expireTime:TIMESTAMP}, token_object_login=#{tokenObject:BLOB}, account_id_login=#{accountId:VARCHAR}, source_id_login=#{sourceId:VARCHAR} WHERE id_login=#{id}")
	void update(Login login);

	@Delete("DELETE FROM hijr_login WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_login WHERE id_login=#{id}")
	void delete(Login login);

	List<Login> getList(QueryParameter param);

	Login getEntity(String id);

	long getCount(QueryParameter param);
		
	@Insert("INSERT INTO hijr_login_archive (id_login, access_token_login, client_id_login, username_login, refresh_token_login, created_time_login, expire_time_login, token_object_login, account_id_login, source_id_login, status_login) VALUES (#{id:VARCHAR}, #{accessToken}, #{clientId:VARCHAR}, #{username:VARCHAR}, #{refreshToken}, #{createdTime:DATE}, #{expireTime:DATE}, #{tokenObject:BLOB}, #{accountId:VARCHAR}, #{sourceId:VARCHAR}, #{status:VARCHAR})")
	void insertArchive(Login login);
	

	@Select("SELECT * FROM hijr_login WHERE client_id_login='spjk-web' AND created_time_login BETWEEN DATE(NOW()) AND DATE(NOW()+ INTERVAL 1 DAY)")
	List<Login> whoLogin();
}
