package id.go.menlhk.bp2sdm.spjk.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.lang3.*;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


import id.go.menlhk.bp2sdm.spjk.OrganizationReference;
import id.go.menlhk.bp2sdm.spjk.core.CommonUtil;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.BidangTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.Eselon1;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanManajerial;
import id.go.menlhk.bp2sdm.spjk.domain.MappingJabatanUnitAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.Organisasi;
import id.go.menlhk.bp2sdm.spjk.domain.Pegawai;
import id.go.menlhk.bp2sdm.spjk.domain.Pertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.Sesi;
import id.go.menlhk.bp2sdm.spjk.domain.SimpegJabatan;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.UserResponden;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.BidangTeknisMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ConfigMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.Eselon1Mapper;
import id.go.menlhk.bp2sdm.spjk.mapper.JabatanKompetensiTeknisMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.JabatanManajerialMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingJabatanUnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MasterJabatanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ReferenceMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SimpegJabatanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitKompetensiManajerialMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitKompetensiSosiokulturalMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserRespondenMapper;
import id.go.menlhk.bp2sdm.spjk.model.Account;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@Configuration
@Controller
public class PageController extends BaseController {

	@Autowired
    private ReferenceMapper referenceMapper;
	
	@Autowired
    private JabatanKompetensiTeknisMapper jabatanKompetensiTeknisMapper;
	
	@Autowired
    private JabatanManajerialMapper jabatanManajerialMapper;
	
	@Autowired
    private UnitKompetensiManajerialMapper unitKompetensiManajerialMapper;
	
	@Autowired
    private UnitKompetensiSosiokulturalMapper unitKompetensiSosiokulturalMapper;
	
	@Autowired
    private UserPertanyaanMapper userPertanyaanMapper;
	

	@Value("${app.url.simpeg_key}")
    private String simpegKey;

	@Value("${app.url.simpeg}")
    private String simpeg;


	@Autowired
	private UnitAuditiMapper unitAuditiMapper;
	
	@Autowired
	private SimpegJabatanMapper simpegJabatanMapper;
	
	@Autowired
	private Eselon1Mapper eselon1Mapper;

	@Autowired
	private UserRespondenMapper userRespondenMapper;

	@Autowired
	private SesiMapper sesiMapper;

	@Autowired
	private MasterJabatanMapper masterJabatanMapper;

	@Autowired
	private BidangTeknisMapper bidangTeknisMapper;

	@Autowired
	private ConfigMapper configMapper;

	@Autowired
	private MappingJabatanUnitAuditiMapper mappingJabatanUnitAuditiMapper;

	@Autowired
	private AccountMapper accountMapper;


	@Autowired
	private UserMapper userMapper;

	@Value("${app.state}")
	private String appState;
	
    @RequestMapping("/admin/{jsp}")
    public String loadPageAdmin(Model model, Locale locale, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(!pass) {
			String requestPage = request.getRequestURL().toString();
    		if(request.getQueryString() != null) {
    			requestPage += "?"+request.getQueryString();
    		}
    		
    		String resultPath = getConstructURL(request, "login-perform?page="+requestPage); 
    		
    		String params = "response_type=code";
			params += "&client_id=" + clientId;
			params += "&redirect_uri=" + resultPath + "";
			//System.out.println(ssoEndpointUrl + "/oauth/authorize?" + params);
			response.sendRedirect(ssoEndpointUrl + "/oauth/authorize?" + params);
		}else {
			if(appState.equals("development")) {
		    	model.addAttribute("contextPath", request.getContextPath());	
			}else {
		    	model.addAttribute("contextPath", "");
			}
			DecodedJWT jwt = JWT.decode(accessTokenValue);
			List<String> rolelist=jwt.getClaim(id.go.menlhk.bp2sdm.spjk.core.Token.AUTHORITIES).asList(String.class);
   		 	if(rolelist.contains("ROLE_SPJK_RESPONDEN")) {
   		 		response.sendRedirect(request.getContextPath() + "/page/index");
   		 	}
			addModelTokenInfo(request, locale, model,accessTokenValue);

			return "pages/admin/" + jsp;
		}
        return "nopage"; //error
    }
    
	@RequestMapping({"/admin/", "/admin"})
	public String loadMainAdmin(Model model, Locale locale, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String jsp = "index"; // default page, please check
		return loadPageAdmin(model, locale, jsp, request, response);
	}

	
	public String getResourceFileAsString(String fileName) throws Exception {
		try {
			File file = ResourceUtils.getFile("classpath:"+fileName);
			InputStream is = new FileInputStream(file);
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	        return reader.lines().collect(Collectors.joining(System.lineSeparator()));
			
		} catch (Exception e) {
			return null;
		}
	}
    @RequestMapping("/page/{jsp}")
    public String loadPage(Model model, Locale locale, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response, HttpSession session) throws Exception{
		
    	String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		if(!pass) {
			String requestPage = request.getRequestURL().toString();
    		if(request.getQueryString() != null) {
    			requestPage += "?"+request.getQueryString();
    		}
    		String resultPath = getConstructURL(request, "login-perform?page="+requestPage); 
    		String params = "response_type=code";
			params += "&client_id=" + clientId;
			params += "&redirect_uri=" + resultPath + "";
			response.sendRedirect(ssoEndpointUrl + "/oauth/authorize?" + params);
		}else {
			model.addAttribute("contextPath", request.getContextPath());
	    	model.addAttribute("ssoPath", ssoEndpointUrl);
			setBrowserAccess(model, request);

			if(appState.equals("development")) {
		    	model.addAttribute("contextPathdashboard", request.getContextPath());	
			}else {
				model.addAttribute("contextPathdashboard", "/spektra");
			}
			addModelTokenInfo(request, locale, model,accessTokenValue);
		
			DecodedJWT jwt = JWT.decode(accessTokenValue);
	    	model.addAttribute("assetPath", request.getContextPath()+"/assets-dashboard");
			
			//this valication to check page for responden or not
			if(StringUtils.isNumeric(jwt.getClaim("user_name").asString().split("::")[AccountLoginInfo.ACCOUNTNAME.getId()])) {
				return loadPageResponden(request,jsp,model, jwt, session);
			}else {
				return "pages/admin/"+jsp;
			}
		}
        return "error";
    }
    
    private void setBrowserAccess(Model model, HttpServletRequest request) {
    	/*String browserName = "";
    	String  browserVer = "";
    	String substring = "";
    	String userAgent=request.getHeader("user-agent");
    	if(userAgent.contains("Chrome")){
	        substring=userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0];
	        browserName=substring.split("/")[0];
	        browserVer=substring.split("/")[1];
	    }else if(userAgent.contains("Firefox")){
	        substring=userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0];
	        browserName=substring.split("/")[0];
	        browserVer=substring.split("/")[1];
	    }
    	Map<String, String> browser =  new HashMap<String, String>();
    	browser.put("name", browserName);
	    browser.put("ver", browserVer);
    	model.addAttribute("browser", browser);
    	System.out.println("Access from Browser:"+browserName+" "+browserVer);
    	*/
    	Map<String, String> browser =  new HashMap<String, String>();
    	browser.put("name", "Chrome");
    	model.addAttribute("browser", browser);
    }


	private String loadPageResponden(HttpServletRequest request, String jsp, Model model, DecodedJWT jwt, HttpSession session) throws Exception {

    	String username = jwt.getClaim("user_name").asString();
		String idUser = username.split("::")[AccountLoginInfo.ACCOUNT.getId()];
    	System.out.println("TES1");
    	//#1check sesi
    	Sesi sesi = sesiMapper.getEntityActive();
		if(sesi==null) {
			if(sesiMapper.getEntityByKodeSesi(request.getParameter("kode-sesi"))==null) {
				model.addAttribute("statusResponden", "NO_SESI");
				return "pages/user/"+jsp;
			}else {
				sesi = sesiMapper.getEntityByKodeSesi(request.getParameter("kode-sesi"));
			}
		}
		System.out.println("TES2");
		//start cek sesi expired here
		QueryParameter paramMapJabSatker = new QueryParameter();
		paramMapJabSatker.setClause(paramMapJabSatker.getClause()+" AND "+ MappingJabatanUnitAuditi.ID_ACCOUNT+" ='"+jwt.getClaim("user_name").asString().split("::")[AccountLoginInfo.ACCOUNT.getId()]+"'");
		paramMapJabSatker.setClause(paramMapJabSatker.getClause()+" AND "+ MappingJabatanUnitAuditi.ID_SESI+" ='"+sesi.getId()+"'");
		if(mappingJabatanUnitAuditiMapper.getCount(paramMapJabSatker)==0) {
			if(configMapper.getEntityByKodeActive("NO_SESI")==null) {
				model.addAttribute("statusResponden", "NO_SESI_JABATAN");
			}
		}else {
			model.addAttribute("namaSesi", sesi.getKode());
			model.addAttribute("idSesi", sesi.getId());
		}
		//end cek sesi expired
		System.out.println("TES3");
		//#2cek responden if exist not need checking the simpeg anymore
		QueryParameter params = new QueryParameter();
		params.setClause(params.getClause()+" AND "+UserResponden.ID_ACCOUNT+"='"+username.split("::")[AccountLoginInfo.ACCOUNT.getId()]+"'");
		params.setClause(params.getClause()+" AND "+UserResponden.ID_SESI+"='"+sesi.getId()+"'");
		
		UserResponden userResponden = new UserResponden();
		List<UserResponden> listuserResponden = userRespondenMapper.getList(params);
		long countResponden = listuserResponden.size();
		if(countResponden > 1) {
			return "double-responden";
		}else if(countResponden == 1) {
			userResponden = listuserResponden.get(0);

			model.addAttribute("responden", userResponden);
			model.addAttribute("responden_tglLahir", new SimpleDateFormat("yyyy-MM-dd").format(userResponden.getTglLahir()));
			
			session.setAttribute("idResponden", userResponden.getId());
			if(session.getAttribute("jenisKompetensi")==null) {
				session.setAttribute("jenisKompetensi", userResponden.getStatusUnitKompetensi());
			}
		}else {
			//hybdrideSimpegNonSimpeg(idUser, sesi, model, jwt);
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_ACCOUNT+" ='"+idUser+"'");
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+" ='"+sesi.getId()+"'");
			List<MappingJabatanUnitAuditi> listMapAccount = mappingJabatanUnitAuditiMapper.getList(param);
			MappingJabatanUnitAuditi mapAccount = new MappingJabatanUnitAuditi();
			if(listMapAccount.size()>0) {
				mapAccount = listMapAccount.get(0);
				setModelNonSimpeg(model, jwt, mapAccount);
			}else {
				model.addAttribute("statusResponden", "NO_SESI_JABATAN");
			}
		}
		System.out.println("TES4");
		//start for info dashboard individu
		try {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_ACCOUNT+" ='"+idUser+"'");
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+" ='"+sesi.getId()+"'");
			MappingJabatanUnitAuditi mapAccount = mappingJabatanUnitAuditiMapper.getList(param).get(0);
			model.addAttribute("namaSatker", mapAccount.getUnitAuditi().getNamaUnitAuditi());
		} catch (Exception e) {System.out.println("something wrong when load MappingJabatanUnitAuditi, for info dashboard individu");}
		//end info dashboard individu
		
		setGeneralData(model, jwt, userResponden);
		System.out.println("TES5");
		return "pages/user/"+jsp;
	}

	private void hybdrideSimpegNonSimpeg(String idUser, Sesi sesi, Model model, DecodedJWT jwt) throws Exception {
		//non simpeg
		if(userMapper.findEselon1ByUnitAuditiUserId(idUser).size() > 0 && userMapper.findEselon1ByUnitAuditiUserId(idUser).get(0).equals("100")) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_ACCOUNT+" ='"+idUser+"'");
			param.setClause(param.getClause()+" AND "+MappingJabatanUnitAuditi.ID_SESI+" ='"+sesi.getId()+"'");
			List<MappingJabatanUnitAuditi> listMapAccount = mappingJabatanUnitAuditiMapper.getList(param);
			MappingJabatanUnitAuditi mapAccount = new MappingJabatanUnitAuditi();
			if(listMapAccount.size()>0) {
				mapAccount = listMapAccount.get(0);
				setModelNonSimpeg(model, jwt, mapAccount);
			}else {
				model.addAttribute("statusResponden", "NO_SESI_JABATAN");
			}
		}else {
			//simpeg
			String yourJSON = "";
			String nipPegawai = jwt.getClaim("user_name").asString().split("::")[AccountLoginInfo.ACCOUNTNAME.getId()];
			String url = id.go.menlhk.bp2sdm.spjk.common.Utils.getSIMPEGURLPegawai(nipPegawai);

			HttpStatus status = HttpStatus.OK;
			try {
				if(appState.equals("development")) {
					yourJSON = getResourceFileAsString("local_json/"+nipPegawai+".json");
					if(yourJSON==null) status = HttpStatus.NOT_FOUND;
				}else {
					RestTemplate restTemplate = new RestTemplate();
					ResponseEntity<String> resp = restTemplate.getForEntity(url, String.class);
					status = resp.getStatusCode();
					yourJSON = resp.getBody()!=null?resp.getBody():"";
				}
		    } catch (HttpClientErrorException | ResourceAccessException | HttpServerErrorException f) {
		    	status = HttpStatus.NOT_FOUND;
			}
		
			if(status.equals(HttpStatus.OK)) {
				setModelSimpeg(model, jwt, yourJSON);
			}else {
				model.addAttribute("statusResponden", "NO_SIMPEG");
			}
		}
	}

	private void setModelSimpeg(Model model, DecodedJWT jwt, String yourString) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;
			try {
				System.out.println("kodeSimpeg "+mapper.readTree(yourString));
				root = mapper.readTree(yourString);
				
				model.addAttribute("nama", root.path("nama").textValue());
				model.addAttribute("nip", root.path("nip").textValue());
				String nip = root.path("nip").textValue();
				String tglLahir = nip.substring(0, 4)+"-"+nip.substring(4, 6)+"-"+nip.substring(6, 8);
				model.addAttribute("tanggalLahir", tglLahir);
				model.addAttribute("golongan", root.path("pangkat").textValue());
				String jk="";
				if(root.path("sex").textValue().equals("L")) jk="LAKI-LAKI";
				else if(root.path("sex").textValue().equals("P")) jk="PEREMPUAN";
				model.addAttribute("jenisKelamin", jk);
				
				//
				model.addAttribute("jabatan", root.path("nm_jabatan").textValue());
				model.addAttribute("fullJabatan", root.path("unit_jabatan").textValue());
				
				
			} catch (Exception e) {e.printStackTrace();}
		//}
	}


	private void setModelNonSimpeg(Model model, DecodedJWT jwt, MappingJabatanUnitAuditi mapAccount) {
		try {
			model.addAttribute("nama", mapAccount.getAccount().getName().trim().replaceAll(" +", " "));
			model.addAttribute("nip", mapAccount.getAccount().getUsername());
			model.addAttribute("golongan", mapAccount.getGolongan());
			model.addAttribute("eselon1", mapAccount.getEselon1().getNama());
			model.addAttribute("idEselon1", mapAccount.getEselon1().getId());
			model.addAttribute("idSatker", mapAccount.getUnitAuditi().getIdUnitAuditi());
			model.addAttribute("idPropinsi", mapAccount.getUnitAuditi().getIdPropinsiUnitAuditi());
			model.addAttribute("namaSatker", mapAccount.getUnitAuditi().getNamaUnitAuditi());
			model.addAttribute("jabatan", mapAccount.getMasterJabatan().getNama());
			
			//start for info dashboard individu
			model.addAttribute("mobile", mapAccount.getAccount().getMobile());
			model.addAttribute("email", mapAccount.getAccount().getEmail());
			//end info
			
			String nip = mapAccount.getAccount().getUsername();
			if (nip.length() >= 15) {
			    String jk = nip.substring(14, 15);
			    jk = "1".equals(jk) ? "LAKI-LAKI" : "2".equals(jk) ? "PEREMPUAN" : "-";
			    model.addAttribute("jenisKelamin", jk);
			}
	
			if (nip.length() >= 8) {
			    String tglLahir = nip.substring(0, 4) + "-" + nip.substring(4, 6) + "-" + nip.substring(6, 8);
			    model.addAttribute("tanggalLahir", tglLahir);
			}
		} catch (Exception e) {System.out.println("SOMETHING STUPID HAPPEN???");}
	}


	private void setGeneralData(Model model, DecodedJWT jwt, UserResponden userResponden) {
		QueryParameter paramNOLIMIT = new QueryParameter();
		paramNOLIMIT.setLimit(1000000);
		model.addAttribute("eselon1New", eselon1Mapper.getList(paramNOLIMIT));
		
		model.addAttribute("listBidangTeknis", bidangTeknisMapper.getList(new QueryParameter()));
		model.addAttribute("listMasterJabatan", masterJabatanMapper.getList(new QueryParameter()));
		
		//cek kompetensi apakah semua telah digenerate, apabila belum maka set status komp tsb.
		if(userResponden.getId()!=null) {
			model.addAttribute("idUserResponden", userResponden.getId());
			model.addAttribute("idResponden", userResponden.getId());
			model.addAttribute("statusResponden", userResponden.getStatus());
			model.addAttribute("statusUnitKompResponden", userResponden.getStatusUnitKompetensi());
			
			if(userResponden.getStatusKompInti()==null) {
				//model.addAttribute("statusResponden", null);
				model.addAttribute("statusUnitKompResponden", "TEKNIS_INTI");
			}
			else if(userResponden.getStatusKompPilihan()==null) {
				//model.addAttribute("statusResponden", null);
				model.addAttribute("statusUnitKompResponden", "TEKNIS_PILIHAN");
			}
			else if(userResponden.getStatusKompManajerial()==null) {
				//model.addAttribute("statusResponden", null);
				model.addAttribute("statusUnitKompResponden", "MANAJERIAL");
			}
			else if(userResponden.getStatusKompSosiokultural()==null) {
				//model.addAttribute("statusResponden", null);
				model.addAttribute("statusUnitKompResponden", "SOSIOKULTURAL");
			}
			else {
				if(userResponden.getStatus().equals("SELESAI")) {
					model.addAttribute("statusResponden", "SELESAI");
				}else {
					model.addAttribute("statusResponden", "PROSES");
				}
				QueryParameter param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+userResponden.getId()+"'");
				param.setClause(param.getClause()+" AND "+UserPertanyaan.STATUS+" ='"+1+"'");
				List<UserPertanyaan> oldData = userPertanyaanMapper.getList(param);

				if(oldData.size()==0) {
					param = new QueryParameter();
					param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+userResponden.getId()+"'");
					param.setClause(param.getClause()+" AND "+UserPertanyaan.JENIS_KOMPETENSI+" ='"+userResponden.getStatusUnitKompetensi()+"'");
					
					UserPertanyaan userPertanyaan = userPertanyaanMapper.getList(param).get(0);
					userPertanyaan.setStatus("1");
					userPertanyaanMapper.updateStatus(userPertanyaan);
				}else {
					if(userResponden.getStatus().equals("")) {
						for(UserPertanyaan up : oldData) {
							up.setStatus(null);
							userPertanyaanMapper.updateStatus(up);
						}
						param = new QueryParameter();
						param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+userResponden.getId()+"'");
						param.setClause(param.getClause()+" AND "+UserPertanyaan.JENIS_KOMPETENSI+" ='"+userResponden.getStatusUnitKompetensi()+"'");
						
						UserPertanyaan userPertanyaan = userPertanyaanMapper.getList(param).get(0);
						userPertanyaan.setStatus("1");
						userPertanyaanMapper.updateStatus(userPertanyaan);
					}
				}
				
			}
		}
		//end cek kompetensi


		//start set model list kompetensi
		QueryParameter params = new QueryParameter();
		try {
			if(userResponden.getStatusUnitKompetensi().equals("TEKNIS_INTI")) {
				params = new QueryParameter();
				params.setOrder("kode_unit_kompetensi_teknis");
				params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.ID_MASTER_JABATAN+" ='"+userResponden.getIdJabatan()+"'");
				params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+userResponden.getIdBidangTeknis()+"'");
				params.setSecondClause(params.getSecondClause()+" AND "+Pertanyaan.ID_BIDANG_TEKNIS+" ='"+userResponden.getIdBidangTeknis()+"'");
				params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.JENIS_KOMPETENSI+" ='INTI'");
				model.addAttribute("listIntiJkt", jabatanKompetensiTeknisMapper.getListUniq(params));
			}else if(userResponden.getStatusUnitKompetensi().equals("TEKNIS_PILIHAN")) {
				params = new QueryParameter();
				params.setOrder("kode_unit_kompetensi_teknis");
				params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.ID_MASTER_JABATAN+" ='"+userResponden.getIdJabatan()+"'");
				params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+userResponden.getIdBidangTeknis()+"'");
				params.setSecondClause(params.getSecondClause()+" AND "+Pertanyaan.ID_BIDANG_TEKNIS+" ='"+userResponden.getIdBidangTeknis()+"'");
				params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.JENIS_KOMPETENSI+" ='PILIHAN'");
				model.addAttribute("listPilihanJkt", jabatanKompetensiTeknisMapper.getListUniq(params));
			}else if(userResponden.getStatusUnitKompetensi().equals("MANAJERIAL")) {
				params = new QueryParameter();
				params.setOrder("kode_unit_kompetensi_manajerial");
				params.setClause(params.getClause()+" AND "+JabatanManajerial.ID_MASTER_JABATAN+" ='"+userResponden.getIdJabatan()+"'");
				params.setClause(params.getClause()+" AND "+JabatanManajerial.NILAI_MINIMUM+" is not null");
				params.setClause(params.getClause()+" AND "+JabatanManajerial.NILAI_MINIMUM+" !=0");
				params.setClause(params.getClause()+" AND "+JabatanManajerial.JENIS+" ='MANAJERIAL'");
				model.addAttribute("listManajerialJkt", jabatanManajerialMapper.getList(params));
			}else if(userResponden.getStatusUnitKompetensi().equals("SOSIOKULTURAL")) {
				//this state will be improve when the data hasbeen complete, cause for now, the data just one
				params = new QueryParameter();
				//params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.ID_MASTER_JABATAN+" ='"+userResponden.getIdJabatan()+"'");
				//params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+userResponden.getIdBidangTeknis()+"'");
				//params.setClause(params.getClause()+" AND "+JabatanKompetensiTeknis.JENIS_KOMPETENSI+" ='PILIHAN'");
				model.addAttribute("listSosiokulturalJkt", unitKompetensiSosiokulturalMapper.getList(params));
			}
		} catch (Exception e) {
			System.out.println("RESPONDEN Sedang LOGIN: "+userResponden.getStatus()+":"+userResponden.getNip()+":"+userResponden.getNama());
		}
		//end set model list kompetensi
	
		model.addAttribute("introduceSoal", configMapper.getEntityByKode("INTRODUCE_SOAL"));
		model.addAttribute("infoPemberitahuan", configMapper.getEntityByKode("MAINTENANCE"));
		model.addAttribute("hideDanger", configMapper.getEntityByKodeActive("HIDE_IF_EMPTY"));
	}
	
	@RequestMapping({"/page/", "/page"})
    public String loadMain(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response, HttpSession session) throws Exception{
    		String jsp = "index"; // default page, please check
		return loadPage(model, locale, jsp, request, response, session);
    }
    
    
    /*** Halaman Admin New ***/

    @RequestMapping("/dashboard/{jsp}")
    public String loadPageDashboard(Model model, Locale locale, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response, HttpSession session) throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(!pass) {
			String requestPage = request.getRequestURL().toString();
    		if(request.getQueryString() != null) {
    			requestPage += "?"+request.getQueryString();
    		}
    		
    		String resultPath = getConstructURL(request, "login-perform?page="+requestPage); 
    		
    		String params = "response_type=code";
			params += "&client_id=" + clientId;
			params += "&redirect_uri=" + resultPath + "";
			response.sendRedirect(ssoEndpointUrl + "/oauth/authorize?" + params);
		}else {
			addModelTokenInfo(request, locale, model,accessTokenValue);
			DecodedJWT jwt = JWT.decode(accessTokenValue);
	    	model.addAttribute("assetPath", request.getContextPath()+"/assets-dashboard");
	    	if(userMapper.findUnitAuditiByUserId(jwt.getClaim("account_id").asString()).size() > 0) {
	    		model.addAttribute("satkerId", userMapper.findUnitAuditiByUserId(jwt.getClaim("account_id").asString()).get(0));
	    	}
	    	if(userMapper.findReferenceByUserId(jwt.getClaim("account_id").asString(), "UNIT_PEMBINA").size() > 0) {
	    		model.addAttribute("unitPembinaId", userMapper.findReferenceByUserId(jwt.getClaim("account_id").asString(), "UNIT_PEMBINA").get(0));
	    	}

			model.addAttribute("listKelompokJabatan", referenceMapper.getListKelompokJabatan(new QueryParameter()));
	    	
	    	return "pages/dashboard/"+jsp;
		}
		
        return "error";
        
    }
    @RequestMapping({"/dashboard/", "/dashboard"})
    public String loadMainAdminNew(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response, HttpSession session) throws Exception{
    	String jsp = "index"; // default page, please check
		return loadPageDashboard(model, locale, jsp, request, response, session);
    }
}
