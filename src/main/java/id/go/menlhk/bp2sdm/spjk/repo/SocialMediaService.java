package id.go.menlhk.bp2sdm.spjk.repo;

import java.io.File;

public interface SocialMediaService {

	public void post(String title, String content, File photo);
}
