package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.bc.PernyataanJawaban;

public interface PernyataanJawabanMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	@Insert("INSERT INTO survey_pernyataan_jawaban (id_pernyataan_jawaban, header_pernyataan_id_pernyataan_jawaban, order_pernyataan_jawaban, isi_pernyataan_jawaban, bobot_pernyataan_jawaban) VALUES (#{idPernyataanJawaban:VARCHAR}, #{headerPernyataanIdPernyataanJawaban:VARCHAR}, #{orderPernyataanJawaban:NUMERIC}, #{isiPernyataanJawaban:VARCHAR}, #{bobotPernyataanJawaban:NUMERIC})")
	void insert(PernyataanJawaban pernyataanJawaban);

	@Update("UPDATE survey_pernyataan_jawaban SET id_pernyataan_jawaban=#{idPernyataanJawaban:VARCHAR}, header_pernyataan_id_pernyataan_jawaban=#{headerPernyataanIdPernyataanJawaban:VARCHAR}, order_pernyataan_jawaban=#{orderPernyataanJawaban:NUMERIC}, isi_pernyataan_jawaban=#{isiPernyataanJawaban:VARCHAR}, bobot_pernyataan_jawaban=#{bobotPernyataanJawaban:NUMERIC} WHERE id_pernyataan_jawaban=#{idPernyataanJawaban}")
	void update(PernyataanJawaban pernyataanJawaban);

	@Delete("DELETE FROM survey_pernyataan_jawaban WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM survey_pernyataan_jawaban WHERE id_pernyataan_jawaban=#{idPernyataanJawaban}")
	void delete(PernyataanJawaban pernyataanJawaban);

	@Delete("INSERT INTO survey_pernyataan_jawaban SELECT * FROM survey_pernyataan_jawaban_test")
	void copyTest();

	List<PernyataanJawaban> getList(QueryParameter param);

	PernyataanJawaban getEntity(String id);

	long getCount(QueryParameter param);

	int getNewId();

	/**********************************
	 * - End Generate -
	 ************************************/

}
