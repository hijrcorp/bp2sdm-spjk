package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MasterJabatan {
	public static final String ID = "id_master_jabatan";
	public static final String NAMA = "nama_master_jabatan";
	public static final String ID_KELOMPOK = "id_kelompok_master_jabatan";
	public static final String KODE_KELOMPOK = "kode_kelompok_master_jabatan";
	public static final String NAMA_KELOMPOK = "nama_kelompok_master_jabatan";
	public static final String ID_TINGKAT = "id_tingkat_master_jabatan";
	public static final String NAMA_TINGKAT = "nama_tingkat_master_jabatan";
	public static final String ID_JENJANG = "id_jenjang_master_jabatan";
	public static final String NAMA_JENJANG = "nama_jenjang_master_jabatan";

	private String id;
	private String nama;
	private String idKelompok;
	private String kodeKelompok;
	private String namaKelompok;
	private String idTingkat;
	private String namaTingkat;
	private String idJenjang;
	private String namaJenjang;

	public MasterJabatan() {

	}

	public MasterJabatan(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("id_kelompok")
	public String getIdKelompok() {
		return idKelompok;
	}

	public void setIdKelompok(String idKelompok) {
		this.idKelompok = idKelompok;
	}

	@JsonProperty("kode_kelompok")
	public String getKodeKelompok() {
		return kodeKelompok;
	}

	public void setKodeKelompok(String kodeKelompok) {
		this.kodeKelompok = kodeKelompok;
	}

	@JsonProperty("nama_kelompok")
	public String getNamaKelompok() {
		return namaKelompok;
	}

	public void setNamaKelompok(String namaKelompok) {
		this.namaKelompok = namaKelompok;
	}

	@JsonProperty("id_tingkat")
	public String getIdTingkat() {
		return idTingkat;
	}

	public void setIdTingkat(String idTingkat) {
		this.idTingkat = idTingkat;
	}

	@JsonProperty("nama_tingkat")
	public String getNamaTingkat() {
		return namaTingkat;
	}

	public void setNamaTingkat(String namaTingkat) {
		this.namaTingkat = namaTingkat;
	}

	@JsonProperty("id_jenjang")
	public String getIdJenjang() {
		return idJenjang;
	}

	public void setIdJenjang(String idJenjang) {
		this.idJenjang = idJenjang;
	}

	@JsonProperty("nama_jenjang")
	public String getNamaJenjang() {
		return namaJenjang;
	}

	public void setNamaJenjang(String namaJenjang) {
		this.namaJenjang = namaJenjang;
	}



	/**********************************************************************/
}
