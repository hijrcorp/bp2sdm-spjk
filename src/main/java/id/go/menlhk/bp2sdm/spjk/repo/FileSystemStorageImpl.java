package id.go.menlhk.bp2sdm.spjk.repo;

import java.awt.Dimension;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;

import id.go.menlhk.bp2sdm.spjk.core.CodeGenerator;
import id.go.menlhk.bp2sdm.spjk.common.Utils;
import id.go.menlhk.bp2sdm.spjk.common.FileTypeSupport;

@Repository("fileSystemStorage")
@Configuration
public class FileSystemStorageImpl implements StorageService {

	@Value("${app.folder}")
	private String appFolder;
	
	@Value("${imagemagick.path}")
	private String imagemagickPath;
	
	@Value("${app.folder.dev}")
	private String appFolderDev;
	
	@Value("${imagemagick.path.dev}")
	private String imagemagickPathDev;
	
	@Value("${app.state}")
	private String appState;

	public void store(MultipartFile file) throws Exception{
		store(file, file.getName());
	}

	@Override
	public File load(String filename)  throws Exception {
		// TODO Auto-generated method stub
		File file =  new File(appFolder + filename);
		
		if(appState.equals("development")) {
			file =  new File(appFolderDev + filename);
		}
		
		System.out.println("load from file system: "+ file.getAbsolutePath());
		
		if(!file.exists() || file.isDirectory()){
			String[] partName = filename.split("\\.");
			String ext = partName[partName.length-1];
			
			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
				file =  new File(appFolder + "notfound.png");
				if(appState.equals("development")) {
					file = new File(appFolderDev + "notfound.png");
				}
			}else if(Arrays.asList(FileTypeSupport.DOCUMENT.extensions()).contains(ext)) {
				file =  new File(appFolder + "notfound.pdf");
				if(appState.equals("development")) {
					file = new File(appFolderDev + "notfound.pdf");
				}
			}
		}
		return file;
	}
	
	
	@Override
	public void delete(String filename) throws Exception {
		// TODO Auto-generated method stub
		File file = new File(appFolder + filename);
		
		if(appState.equals("development")) {
			file =  new File(appFolderDev + filename);
		}
		
		if(file.exists()){
//			FileCopyUtils.copy(file, new File(trashFolder + filename));
//			FileCopyUtils.copy(file, new File(appFolder + "trash/" + filename));
			FileSystemUtils.deleteRecursively(file);
			System.out.println("delete from file system");
		}
	}

	@Override
	public void store(MultipartFile file, String filename) throws Exception {
		// TODO Auto-generated method stub
		
		
		if(appState.equals("development")) {
			System.out.println("store in file system: " + appFolderDev);	
		}else {
			System.out.println("store in file system: " + appFolder);
		}
	
		  
		if (!file.isEmpty()) {
            try {
            		File f = new File(appFolder + filename);
            		if(appState.equals("development")) {
            			f =  new File(appFolderDev + filename);
            		}
            		
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = 
                        new BufferedOutputStream(new FileOutputStream(f));
                stream.write(bytes);
                stream.close();
                if(file.getContentType().startsWith("image/")){
                	
                		Dimension d = Utils.getImageDimension(f);
                		// generate smaller image if resolution more than 640px
                		if(d.getHeight() > 640 || d.getWidth() > 640) {
                			String w = "640x";
                    		if(d.getHeight() > d.getWidth()) {
                    			w = "480x";
                    		}
                    	
                    		if(appState.equals("development")) {
                    			if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
		    	                		String[] s = imagemagickPathDev.split("/");
		    	                		Process p = Runtime.getRuntime().exec(imagemagickPathDev+" "+appFolderDev+filename+" -resize "+w+" "+appFolderDev+"small_"+filename, null, new File(imagemagickPathDev.replace(s[s.length-1], "")));
		    	    	                p.waitFor();
		    	    	                
		    	                	}else{
		    	                		Process p = Runtime.getRuntime().exec(new String[]{"sh","-c",imagemagickPathDev+" "+appFolderDev+filename+" -resize "+w+" "+appFolderDev+"small_"+filename});
		    	    	                p.waitFor();
		    	                	}	          
                    		}else {
                    			if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
		    	                		String[] s = imagemagickPath.split("/");
		    	                		Process p = Runtime.getRuntime().exec(imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename, null, new File(imagemagickPath.replace(s[s.length-1], "")));
		    	    	                p.waitFor();
		    	    	                
		    	                	}else{
		    	                		Process p = Runtime.getRuntime().exec(new String[]{"sh","-c",imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename});
		    	    	                p.waitFor();
		    	                	}	          
                    		}
	    	                	
                		}else {
                			File fd = new File(appFolder + "small_"+filename);
                			if(appState.equals("development")) {
                				fd = new File(appFolderDev + "small_"+filename);
                    		}
                			FileSystemUtils.copyRecursively(f, fd);
                		}
                		
                		      
                }
            } catch (Exception e) {
                
            }
        } else {
            throw new Exception("Failed store file.");
        }
	}

}
