package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;

public interface UnitAuditiMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	@Insert("INSERT INTO tbl_unit_auditi (unit_auditi_id, kode_unit_auditi, nama_unit_auditi, propinsi, kabupaten, eselon1, status_auditi, kode_simpeg_unit_auditi, user_added, date_added) VALUES (#{idUnitAuditi:VARCHAR}, #{kodeUnitAuditi:VARCHAR}, #{namaUnitAuditi:VARCHAR}, #{idPropinsiUnitAuditi:VARCHAR}, #{idDistrikUnitAuditi:VARCHAR}, #{idEselon1UnitAuditi:VARCHAR}, #{idStatusAuditiUnitAuditi:VARCHAR}, #{kodeSimpegUnitAuditi:VARCHAR}, #{userAdded:VARCHAR}, NOW())")
	void insert(UnitAuditi unitAuditi);

	@Update("UPDATE tbl_unit_auditi SET kode_unit_auditi=#{kodeUnitAuditi:VARCHAR}, nama_unit_auditi=#{namaUnitAuditi:VARCHAR}, propinsi=#{idPropinsiUnitAuditi:VARCHAR}, kabupaten=#{idDistrikUnitAuditi:VARCHAR}, eselon1=#{idEselon1UnitAuditi:VARCHAR}, status_auditi=#{idStatusAuditiUnitAuditi:VARCHAR}, kode_simpeg_unit_auditi=#{kodeSimpegUnitAuditi:VARCHAR}, user_modified=#{userModified:VARCHAR}, date_modified=NOW() WHERE unit_auditi_id=#{idUnitAuditi}")
	void update(UnitAuditi unitAuditi);

	@Delete("DELETE FROM tbl_unit_auditi WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_unit_auditi WHERE id_unit_auditi=#{idUnitAuditi}")
	void delete(UnitAuditi unitAuditi);

	@Delete("INSERT INTO tbl_unit_auditi SELECT * FROM spjk_unit_auditi_test")
	void copyTest();

	List<UnitAuditi> getList(QueryParameter param);

	UnitAuditi getEntity(String id);

	long getCount(QueryParameter param);

	int getNewId();

	/**********************************
	 * - End Generate -
	 ************************************/

	List<Map<String, Object>> getListByKodeSimpeg(String kode);
	Map<String, Object> getMappingById(String id);

	@Update("UPDATE spjk_unit_auditi SET id_account_deleted_unit_auditi=#{userId}, timestamp_deleted_unit_auditi=now() WHERE id_unit_auditi=#{idUnitAuditi}")
	void deleteTemp(@Param("idUnitAuditi") String idUnitAuditi, @Param("userId") String userId);
}
