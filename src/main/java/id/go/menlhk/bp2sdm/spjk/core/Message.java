package id.go.menlhk.bp2sdm.spjk.core;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Message implements Serializable {

	private static final long serialVersionUID = 3849306663516449211L;

	public static final int ERROR_CODE = -1;
	public static final int SUCCESS_CODE = 1;
	public static final int WARNING_CODE = -2;

	private static final String DEFAULT_SUCCESS = "Success.";
	private static final String DEFAULT_ERROR = "Error.";
	private static final String DEFAULT_WARNING = "Warning.";

	private String status;
	private String message;
	private int code;
	private Object data;

	public Message(String message) {
		this.message = message;
	}

	public Message(String message, int code) {
		this.message = message;
		this.code = code;
	}

	public Message(int code) {
		this.code = code;

		if (this.code == SUCCESS_CODE) {
			this.message = DEFAULT_SUCCESS;
		} else if (this.code == ERROR_CODE) {
			this.message = DEFAULT_ERROR;
		} else if (this.code == WARNING_CODE) {
			this.message = DEFAULT_WARNING;
		}
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@JsonIgnore
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getStatus() {
		if (this.code == SUCCESS_CODE) {
			status = "200";
		}

		if (this.code == ERROR_CODE) {
			status = "999";
		}
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
