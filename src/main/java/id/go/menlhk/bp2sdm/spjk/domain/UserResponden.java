package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserResponden {
	public static final String ID = "id_user_responden";
	public static final String ID_SESI = "id_sesi_user_responden";
	public static final String ID_ACCOUNT = "id_account_user_responden";
	public static final String SARAN = "saran_user_responden";
	public static final String NIP = "nip_user_responden";
	public static final String GOLONGAN = "golongan_user_responden";
	public static final String NAMA = "nama_user_responden";
	public static final String JENIS_KELAMIN = "jenis_kelamin_user_responden";
	public static final String TGL_LAHIR = "tgl_lahir_user_responden";
	public static final String ID_ESELON1 = "id_eselon1_user_responden";
	public static final String ID_UNIT_KERJA = "id_unit_kerja_user_responden";
	public static final String ID_PROPINSI = "id_propinsi_user_responden";
	public static final String ID_BIDANG_TEKNIS = "id_bidang_teknis_user_responden";
	public static final String ID_JABATAN = "id_jabatan_user_responden";
	public static final String STATUS_UNIT_KOMPETENSI = "status_unit_kompetensi_user_responden";
	public static final String STATUS = "status_user_responden";
	public static final String DURASI_BERJALAN = "durasi_berjalan_user_responden";
	public static final String DURASI = "durasi_user_responden";
	public static final String SISA_DURASI = "sisa_durasi_user_responden";

	private String id;
	private String idSesi;
	private String idAccount;
	private String saran;
	private String nip;
	private String golongan;
	private String nama;
	private String jenisKelamin;
	private Date tglLahir;
	private String idEselon1;
	private String idUnitKerja;
	private String idPropinsi;
	private String idBidangTeknis;
	private String idJabatan;
	private String statusUnitKompetensi;
	private String status;
	private Integer durasiBerjalan;
	private Integer durasi;
	private Integer sisaDurasi;


	public UserResponden() {

	}

	public UserResponden(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_sesi")
	public String getIdSesi() {
		return idSesi;
	}

	public void setIdSesi(String idSesi) {
		this.idSesi = idSesi;
	}

	@JsonProperty("id_account")
	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	@JsonProperty("saran")
	public String getSaran() {
		return saran;
	}

	public void setSaran(String saran) {
		this.saran = saran;
	}

	@JsonProperty("nip")
	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}
	
	@JsonProperty("golongan")
	public String getGolongan() {
		return golongan;
	}

	public void setGolongan(String golongan) {
		this.golongan = golongan;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("jenis_kelamin")
	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	@JsonProperty("tgl_lahir")
	public Date getTglLahir() {
		return tglLahir;
	}

	public void setTglLahir(Date tglLahir) {
		this.tglLahir = tglLahir;
	}

	@JsonProperty("id_eselon1")
	public String getIdEselon1() {
		return idEselon1;
	}

	public void setIdEselon1(String idEselon1) {
		this.idEselon1 = idEselon1;
	}

	@JsonProperty("id_unit_kerja")
	public String getIdUnitKerja() {
		return idUnitKerja;
	}

	@JsonProperty("id_propinsi")
	public String getIdPropinsi() {
		return idPropinsi;
	}

	public void setIdPropinsi(String idPropinsi) {
		this.idPropinsi = idPropinsi;
	}

	public void setIdUnitKerja(String idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}

	@JsonProperty("id_bidang_teknis")
	public String getIdBidangTeknis() {
		return idBidangTeknis;
	}

	public void setIdBidangTeknis(String idBidangTeknis) {
		this.idBidangTeknis = idBidangTeknis;
	}

	@JsonProperty("id_jabatan")
	public String getIdJabatan() {
		return idJabatan;
	}

	public void setIdJabatan(String idJabatan) {
		this.idJabatan = idJabatan;
	}

	@JsonProperty("status_unit_kompetensi")
	public String getStatusUnitKompetensi() {
		return statusUnitKompetensi;
	}

	public void setStatusUnitKompetensi(String statusUnitKompetensi) {
		this.statusUnitKompetensi = statusUnitKompetensi;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonProperty("durasi_berjalan")
	public Integer getDurasiBerjalan() {
		return durasiBerjalan;
	}

	public void setDurasiBerjalan(Integer durasiBerjalan) {
		this.durasiBerjalan = durasiBerjalan;
	}

	@JsonProperty("durasi")
	public Integer getDurasi() {
		return durasi;
	}

	public void setDurasi(Integer durasi) {
		this.durasi = durasi;
	}

	@JsonProperty("sisa_durasi")
	public Integer getSisaDurasi() {
		return sisaDurasi;
	}

	public void setSisaDurasi(Integer sisaDurasi) {
		this.sisaDurasi = sisaDurasi;
	}
	
	
	/**********************************************************************/
	

	private String statusKompInti;
	private String statusKompPilihan;
	private String statusKompManajerial;
	private String statusKompSosiokultural;
	
	private UnitAuditi unitAuditi;
	

	public String getStatusKompInti() {
		return statusKompInti;
	}

	public void setStatusKompInti(String statusKompInti) {
		this.statusKompInti = statusKompInti;
	}

	public String getStatusKompPilihan() {
		return statusKompPilihan;
	}

	public void setStatusKompPilihan(String statusKompPilihan) {
		this.statusKompPilihan = statusKompPilihan;
	}

	public String getStatusKompManajerial() {
		return statusKompManajerial;
	}

	public void setStatusKompManajerial(String statusKompManajerial) {
		this.statusKompManajerial = statusKompManajerial;
	}

	public String getStatusKompSosiokultural() {
		return statusKompSosiokultural;
	}

	public void setStatusKompSosiokultural(String statusKompSosiokultural) {
		this.statusKompSosiokultural = statusKompSosiokultural;
	}

	public UnitAuditi getUnitAuditi() {
		return unitAuditi;
	}

	public void setUnitAuditi(UnitAuditi unitAuditi) {
		this.unitAuditi = unitAuditi;
	}
	
}
