package id.go.menlhk.bp2sdm.spjk.controllers;

import static org.mockito.Matchers.longThat;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.cookie.Cookie;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.go.menlhk.bp2sdm.spjk.common.Utils;
import id.go.menlhk.bp2sdm.spjk.core.Clause;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.core.StyleExcel;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardBidangSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardMonitoringSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardProgress;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.DetilIndividu;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.KelompokJabatan;
import id.go.menlhk.bp2sdm.spjk.domain.Laporan;
import id.go.menlhk.bp2sdm.spjk.domain.LaporanDynamic;
import id.go.menlhk.bp2sdm.spjk.domain.MappingJabatanUnitAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.Pertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.PertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.RataRataIndividuExisting;
import id.go.menlhk.bp2sdm.spjk.domain.RataRataKompetensiIndividu;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.UserResponden;
import id.go.menlhk.bp2sdm.spjk.domain.bc.Pernyataan;
import id.go.menlhk.bp2sdm.spjk.domain.bc.PernyataanJawaban;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ConfigMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.DashboardSPJKMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.DetilIndividuMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.LaporanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingJabatanUnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PernyataanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PernyataanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.RataRataIndividuExistingMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.RataRataKompetensiIndividuMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ReferenceMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitKompetensiTeknisMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserRespondenMapper;
import id.go.menlhk.bp2sdm.spjk.model.Account;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@RestController
@RequestMapping("/dashboard")
public class DashboardController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	private PertanyaanMapper pertanyaanMapper;
	
	@Autowired
	private PertanyaanJawabanMapper pertanyaanJawabanMapper;

	@Autowired
	private ReferenceMapper referenceMapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private DashboardSPJKMapper dashboardSPJKMapper;

	@Autowired
	private RataRataIndividuExistingMapper rataIndividuExistingMapper;

	@Value("${app.url.sso}")
	protected String ssoEndpointUrl;
	
//	@Value("${app.mode}")
	protected String appMode="cloud";
	
	@Value("${app.cookie.name}")
	protected String cookieName;
	
	@Autowired
	private RataRataKompetensiIndividuMapper rataKompetensiIndividuMapper;
	
	@Autowired
	private MappingJabatanUnitAuditiMapper mappingJabatanUnitAuditiMapper;

	@Autowired
	private DetilIndividuMapper detilIndividuMapper;

	@Autowired
	private LaporanMapper laporanMapper;
	
	@Autowired
	private UserRespondenMapper userRespondenMapper;
	
	@Autowired
	private AccountMapper accountMapper;

	@RequestMapping(value = "/main", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseWrapperList getListMain(HttpServletRequest request,
			@RequestParam("filter_debug") Optional<String> filterDebug,
			@RequestParam("filter_keyword") Optional<String> filter_keyword,
			@RequestParam("filter_jenis") Optional<String> filterJenis,
			@RequestParam("filter_id_bidang_teknis") Optional<String> filterIdBidangTeknis,
			@RequestParam("filter_id_unit_kompetensi") Optional<String> filterIdUnitKompetensi,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {
		Map<String, Object> map = new HashMap<>();
		QueryParameter param = new QueryParameter();
		ResponseWrapperList resp = new ResponseWrapperList();
		// param.setClause("1");
		List<DashboardSPJK> data = dashboardSPJKMapper.getListMain(param);
		List<DashboardBidangSPJK> dataBidang = dashboardSPJKMapper.getListBidangPEH(param);
		List<DashboardMonitoringSPJK> dataMonitoring = new ArrayList<DashboardMonitoringSPJK>();
				if(filterDebug.isPresent()) dataMonitoring = dashboardSPJKMapper.getListMonitoringStatus(param);
		map.put("main", data);
		map.put("bidang", dataBidang);
		map.put("monitoring", dataMonitoring);
		resp.setData(map);
		return resp;
	}
	
	@RequestMapping(value = "/update-biodata", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> updateBiodata(
			@RequestParam("id") Optional<String> id,
			@RequestParam("golongan") Optional<String> golongan, 
			@RequestParam("email") Optional<String> email,
			@RequestParam("mobile_phone") Optional<String> mobilePhone, 
			HttpServletRequest request) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();
		
		UserResponden data = userRespondenMapper.getEntity(id.get());
		if (data==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		//for edit
		if(golongan.isPresent()) data.setGolongan(golongan.get());
		
		boolean pass = true;
		if (data.getGolongan() == null || data.getIdBidangTeknis() == null || data.getIdJabatan() == null || data.getIdEselon1() == null  || data.getIdUnitKerja() == null) {
			pass = false;
		} else {
			if (data.getGolongan().equals("") || data.getIdBidangTeknis().equals("") || data.getIdJabatan().equals("") || data.getIdEselon1().equals("") || data.getIdUnitKerja().equals("")) {
				pass = false;
			}
		}
		
		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Golongan, Email, Atau No.Handphone anda belum lengkap..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		if(id.isPresent()) {
			//update user responden golongan
			userRespondenMapper.update(data);
			//update account email, no.hp
			//Account account = accountMapper.getEntity(data.getIdAccount());
			//account.setEmail(email.get());
			//account.setMobile(mobilePhone.get());
			//accountMapper.update(account);
		}
		resp.setMessage("Data berhasil disimpan.");
		resp.setData(data);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	

	@RequestMapping(value = "/sinkorn-biodata", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> sinkornBiodata(
			@RequestParam("id") Optional<String> id,
			HttpServletRequest request) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();
		
		UserResponden data = userRespondenMapper.getEntity(id.get());
		if (data==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		//update user responden jk, tgl lahir

		String nip = data.getNip();
		String jk = nip.substring(14, 15);
		if(jk.equals("1")) jk="LAKI-LAKI";
		else if(jk.equals("2")) jk="PEREMPUAN";
		data.setJenisKelamin(jk);
		
		String tglLahir = nip.substring(0, 4)+"-"+nip.substring(4, 6)+"-"+nip.substring(6, 8);
		data.setTglLahir(Utils.formatDate(tglLahir, "yyyy-MM-dd"));
		
		boolean pass = true;
		if (data.getJenisKelamin() == null || data.getTglLahir() == null) {
			pass = false;
		} else {
			if (data.getJenisKelamin().equals("") || data.getTglLahir().equals(null)) {
				pass = false;
			}
		}
		
		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Jenis Kelamin, Atau Tanggal Lahir anda belum lengkap..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		if(id.isPresent()) {
			userRespondenMapper.update(data);
		}
		resp.setMessage("Sinkorn berhasil.");
		resp.setData(data);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	
	@RequestMapping(value = "/laporan-individu", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getDashboardLaporanIndividuList(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_organisasi") Optional<String> filterIdOrganisasi,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_id_propinsi") Optional<String> filterIdPropinsi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjang_jabatan,
			@RequestParam("filter_status") Optional<String> filterStatus,
			@RequestParam("filter_nip") Optional<String> filterNip,
			@RequestParam("filter_me") Optional<String> filterMe,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		//
		QueryParameter param = new QueryParameter();
		// if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+
		// Pertanyaan.NAMA_KATEGORI_PERTANYAAN + " LIKE '%"+filter_keyword.get()+"%'");
		if(filterIdEeselon1.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");
		if(filterIdPropinsi.isPresent()) param.setClause(param.getClause()+" AND "+UnitAuditi.ID_PROPINSI_UNIT_AUDITI+" ='"+filterIdPropinsi.get()+"'");
		
		if(filterIdKelompokJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterIdJenjang_jabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjang_jabatan.get()+"'");
		if(filterStatus.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.STATUS_RESPONDEN+" ='"+filterStatus.get()+"'");
		if(filterNip.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.NIP+" ='"+filterNip.get()+"'");

		System.out.println("request.isUserInRole(\"SPJK_RESPONDEN\")"+ request.isUserInRole("SPJK_RESPONDEN"));
		System.out.println("request.isUserInRole(\"ROLE_SPJK_RESPONDEN\")"+ request.isUserInRole("ROLE_SPJK_RESPONDEN"));
		System.out.println("request.isUserInRole(\"RESPONDEN\")"+ request.isUserInRole("RESPONDEN"));
		if(filterMe.isPresent() || request.isUserInRole("SPJK_RESPONDEN"))  {
			param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_USER_RESPONDEN+" ='"+extractAccountLogin(request, AccountLoginInfo.ACCOUNT)+"'");
		}
		
		if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		
		if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_ESELON1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + UnitAuditi.ID_PROPINSI_UNIT_AUDITI + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}
		
		if(userMapper.findReferenceByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT), "UNIT_PEMBINA").size() > 0) {
			param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_KELOMPOK_JABATAN + " IN("+userMapper.findReferenceByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT), "UNIT_PEMBINA").get(0)+")");
		}


		if(filterIdOrganisasi.isPresent()) {
			param.setClause(param.getClause() + " AND " + UnitAuditi.ID_PROPINSI_UNIT_AUDITI + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		
		if (limit.isPresent()) {
			if (limit.get() > 2000) param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		//param.setOrder(UnitKompetensiTeknis.ID);
		if(filterKodeSesi.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
		List<RataRataKompetensiIndividu> data = rataKompetensiIndividuMapper.getList(param);

		resp.setCount(rataKompetensiIndividuMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/laporan-progress-responden", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getDashboardProgress(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_organisasi") Optional<String> filterIdOrganisasi,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_id_propinsi") Optional<String> filterIdPropinsi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjang_jabatan,
			@RequestParam("filter_status") Optional<String> filterStatus,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		QueryParameter param = new QueryParameter();
		
		if(filterIdEeselon1.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_eselon1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_unit_auditi+" ='"+filterIdUnitAuditi.get()+"'");
		if(filterIdPropinsi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_propinsi+" ='"+filterIdPropinsi.get()+"'");
		
		//if(filterIdKelompokJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_kelompok_jabatan+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterIdJenjang_jabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_jenjang_jabatan+" ='"+filterIdJenjang_jabatan.get()+"'");
		if(filterStatus.isPresent()) param.setThirdClause(param.getThirdClause()+" AND "+DashboardProgress.status+" ='"+filterStatus.get()+"'");
		
		//check satker
		if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_unit_auditi + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		//check eselon1
		if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_eselon1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		//check bdlhk
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_propinsi + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}

//		if(userMapper.findReferenceByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT), "UNIT_PEMBINA").size() > 0) {
//			param.setClause(param.getClause() + " AND " + KelompokJabatan.ID + " IN("+userMapper.findReferenceByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT), "UNIT_PEMBINA").get(0)+")");
//		}
		
		if(filterIdOrganisasi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_propinsi + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		
		if(filterKodeSesi.isPresent()) {
			param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.kode_sesi+" ='"+filterKodeSesi.get()+"'");
			param.setHeaderClause(filterKodeSesi.get());
		}
		
		List<DashboardProgress> data = laporanMapper.getDashboardProgress(param);

		resp.setData(data);
	
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/laporan-demografi", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getDashboardDemografi(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_organisasi") Optional<String> filterIdOrganisasi,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_id_propinsi") Optional<String> filterIdPropinsi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjang_jabatan,
			@RequestParam("filter_status") Optional<String> filterStatus,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		QueryParameter param = new QueryParameter();
		
		if(filterIdEeselon1.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_eselon1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_unit_auditi+" ='"+filterIdUnitAuditi.get()+"'");
		if(filterIdPropinsi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_propinsi+" ='"+filterIdPropinsi.get()+"'");
		
		if(filterIdKelompokJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_kelompok_jabatan+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterIdJenjang_jabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.id_jenjang_jabatan+" ='"+filterIdJenjang_jabatan.get()+"'");
		//if(filterStatus.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.status+" ='"+filterStatus.get()+"'");
		
		//check satker
		if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_unit_auditi + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		//check eselon1
		if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_eselon1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		//check bdlhk
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_propinsi + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}
		
		if(filterIdOrganisasi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DashboardProgress.id_propinsi + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		
		if(filterKodeSesi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DashboardProgress.kode_sesi+" ='"+filterKodeSesi.get()+"'");
		
		QueryParameter myparama= new QueryParameter();
		myparama.setLimit(1000000);
		if(filterIdKelompokJabatan.isPresent()) myparama.setClause(myparama.getClause()+" AND "+RataRataIndividuExisting.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterIdEeselon1.isPresent()) myparama.setClause(myparama.getClause()+" AND "+RataRataIndividuExisting.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) myparama.setClause(myparama.getClause()+" AND "+RataRataIndividuExisting.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");
		if(filterIdPropinsi.isPresent()) myparama.setClause(myparama.getClause()+" AND "+"id_propinsi_rata_rata_individu_existing"+" ='"+filterIdPropinsi.get()+"'");
		
		//check satker
		if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			myparama.setClause(myparama.getClause() + " AND " + RataRataIndividuExisting.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		//check eselon1
		if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			myparama.setClause(myparama.getClause() + " AND " + RataRataIndividuExisting.ID_ESELON1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		//check bdlhk
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			myparama.setClause(myparama.getClause() + " AND " + "id_propinsi_rata_rata_individu_existing" + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}

		if(userMapper.findReferenceByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT), "UNIT_PEMBINA").size() > 0) {
			myparama.setClause(myparama.getClause() + " AND " + RataRataIndividuExisting.ID_KELOMPOK_JABATAN + " IN("+userMapper.findReferenceByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT), "UNIT_PEMBINA").get(0)+")");
		}
		
		if(filterIdOrganisasi.isPresent()) {
			myparama.setClause(myparama.getClause() + " AND " + "id_propinsi_rata_rata_individu_existing" + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		
		if(filterKodeSesi.isPresent()) myparama.setClause(myparama.getClause()+" AND "+RataRataIndividuExisting.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
		
		List<RataRataIndividuExisting> data = rataIndividuExistingMapper.getList(myparama);
		
		myparama.setClause(myparama.getClause()+" AND "+RataRataIndividuExisting.STATUS_PEMETAAN+" ='"+"Y"+"'");
		List<RataRataIndividuExisting> dataPemetaan = rataIndividuExistingMapper.getList(myparama);
		
		boolean kelompokJabatanIsPresent = false;
		if(filterIdKelompokJabatan.isPresent()) kelompokJabatanIsPresent=true;
		Map<String, Object> mapExisting = createDataDemografi(data, kelompokJabatanIsPresent);
		Map<String, Object> mapPemetaan = createDataDemografi(dataPemetaan, kelompokJabatanIsPresent);
		
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("dataExisting", mapExisting);
		mapData.put("dataPemetaan", mapPemetaan);
		resp.setData(mapData);
	
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	private Map<String, Object> createDataDemografi(List<RataRataIndividuExisting> data, boolean kelompokJabatanIsPresent) throws JSONException {
		//start master
	    ArrayList<String> masterJenjang = new ArrayList<String>();
	    ArrayList<String> jenjangUnique = new ArrayList<String>();
	    
	    for(RataRataIndividuExisting o : data) {
    		//untuk custom label pelaksana if ini
	    	try {
		    	String idUnique=o.getIdJenjangJabatan();
		    	String namaUnique="'"+o.getNamaJenjangJabatan().toString()+"'";
		    	if(jenjangUnique.indexOf(namaUnique) == -1) {
		    		jenjangUnique.add(namaUnique);
		    		
		    		if(kelompokJabatanIsPresent){
			    		masterJenjang.add("{\"id\" : "+idUnique+", \"nama\" : "+namaUnique+", \"jumlah\" : 0, \"jumlahPercentage\" : 0, \"total\" : 0}");
			    	}else {
				    	if(!o.getIdJenjangJabatan().equals("4") && !o.getIdJenjangJabatan().equals("5")) {
				    		masterJenjang.add("{\"id\" : "+idUnique+", \"nama\" : "+namaUnique+", \"jumlah\" : 0, \"jumlahPercentage\" : 0, \"total\" : 0}");
				    	}
			    	}
			    }
			} catch (Exception e) {System.out.println("something error laporan-demografi");}
	    }
	    //end master
	    
	    List<JSONObject> listJson = new ArrayList<JSONObject>();
	    for(String oString : masterJenjang) {
	    	JSONObject jo = new JSONObject(oString);
	    	int count=0;
	    	int countAll=0;
	    	for(RataRataIndividuExisting o : data) {
	    		//untuk custom label pelaksana 
	    		if(kelompokJabatanIsPresent==false) {
	    			if(o.getIdJenjangJabatan().equals("4")) o.setIdJenjangJabatan("2");
	    			if(o.getIdJenjangJabatan().equals("5")) o.setIdJenjangJabatan("3");
	    		}
	    		//end here
		    	if(jo.getInt("id")==Integer.valueOf(o.getIdJenjangJabatan())) {
		    		count++;
			    	jo.remove("jumlah");
			    	jo.put("jumlah", count);
		    	}
		    	countAll++;
		    }
	    	jo.remove("jumlahPercentage");
			float nu =  (((float)jo.getInt("jumlah")/countAll)*100);
			jo.put("jumlahPercentage", nu);
			jo.remove("total");
			jo.put("total", countAll);
	    	listJson.add(jo);
	    }

		//for mapping string to object
	    ArrayList<Object> arrDataJenjangExisting = new ArrayList<>();
	    for(JSONObject jo : listJson) {
		    Map<String, Object> mapDataExisting = new HashMap<>();
	    	mapDataExisting.put("id", jo.getString("id"));
	    	mapDataExisting.put("nama", jo.getString("nama"));
	    	mapDataExisting.put("jumlah", jo.getString("jumlah"));
	    	mapDataExisting.put("jumlahPercentage", jo.getString("jumlahPercentage"));
	    	mapDataExisting.put("total", jo.getString("total"));
	    	arrDataJenjangExisting.add(mapDataExisting);
	    }
	    
	    //start render master usia dataq
	    ArrayList<String> masterUsia = new ArrayList<String>();
	    masterUsia.add("{\"L\" : 0, \"P\" : 0, \"label\" : \"20-29\", \"start\" : 20, \"end\" : 29, \"total\" : 0}");
	    masterUsia.add("{\"L\" : 0, \"P\" : 0, \"label\" : \"30-39\", \"start\" : 30, \"end\" : 39, \"total\" : 0}");
	    masterUsia.add("{\"L\" : 0, \"P\" : 0, \"label\" : \"40-49\", \"start\" : 40, \"end\" : 49, \"total\" : 0}");
	    masterUsia.add("{\"L\" : 0, \"P\" : 0, \"label\" : \"50-59\", \"start\" : 50, \"end\" : 59, \"total\" : 0}");
	    masterUsia.add("{\"L\" : 0, \"P\" : 0, \"label\" : \"60+\", \"start\" : 60, \"end\" : 89, \"total\" : 0}");

	    List<JSONObject> listJsonMasterUsia = new ArrayList<JSONObject>();
	    for(String temp_value : masterUsia) {
	    	JSONObject obj_value = new JSONObject(temp_value);
	    	int countAll=0;
	    	int countL=0, countP=0;
	    	for(RataRataIndividuExisting o : data) {
	    		if(o.getUmur() <= obj_value.getInt("end") && o.getUmur() >= obj_value.getInt("start")){
			    	if(o.getJenisKelamin().equals("LAKI-LAKI")) {
			    		countL++;
			    		obj_value.remove("L");
			    		obj_value.put("L", countL);
			    	}
			    	if(o.getJenisKelamin().equals("PEREMPUAN")) {
			    		countP++;
			    		obj_value.remove("P");
			    		obj_value.put("P", countP);
			    	}
	    		}
     	    	countAll++;
		    }
    		obj_value.remove("total");
    		obj_value.put("total", countAll);
    		listJsonMasterUsia.add(obj_value);
	    }

		//for mapping string to object
	    ArrayList<Object> arrDataUsiaExisting = new ArrayList<>();
	    int key=0;
	    for(JSONObject jo : listJsonMasterUsia) {
		    Map<String, Object> mapUsiaExisting = new HashMap<>();
		    mapUsiaExisting.put("id", key);
		    mapUsiaExisting.put("L", jo.getInt("L"));
		    mapUsiaExisting.put("P", jo.getInt("P"));
		    mapUsiaExisting.put("label", jo.getString("label"));
		    mapUsiaExisting.put("start", jo.getInt("start"));
		    mapUsiaExisting.put("end", jo.getInt("end"));
		    mapUsiaExisting.put("total", jo.getInt("total"));
	    	arrDataUsiaExisting.add(mapUsiaExisting);
	    	key++;
	    }

		Map<String, Object> mapExisting = new HashMap<>();
		mapExisting.put("dataUsia", arrDataUsiaExisting);
		mapExisting.put("dataJenjang", arrDataJenjangExisting);
		
		return mapExisting;
	}
	
	@RequestMapping(value = "/detil-perjabatan-kompetensi", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getListDetilPerjabatanKompTeknis(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_id_organisasi") Optional<String> filterIdOrganisasi,
			@RequestParam("filter_id_propinsi") Optional<String> filterIdPropinsi,
			@RequestParam("filter_jenis_kompetensi") Optional<String> filterJenisKompetensi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_tingkat_jabatan") Optional<String> filterIdTingkatJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjangJabatan,
			@RequestParam("filter_is_non_teknis") Optional<String> filterIsNonTeknis,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		//
		QueryParameter param = new QueryParameter();
		if(filterIdEeselon1.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");
		if(request.isUserInRole("ROLE_SPJK_ESELON1")) {
			if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_ESELON1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		if(request.isUserInRole("ROLE_SPJK_SATKER")) {
			if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		
		//
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + "id_propinsi_unit_auditi" + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}
		if(filterIdOrganisasi.isPresent()) {
			param.setClause(param.getClause() + " AND " + "id_propinsi_unit_auditi" + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		if(filterIdPropinsi.isPresent()) {
			param.setClause(param.getClause() + " AND " + "id_propinsi_unit_auditi" + " ='"+filterIdPropinsi.get()+"'");
		}
		//
		
		if(filterJenisKompetensi.isPresent()) {
			if(filterJenisKompetensi.get().equals("TEKNIS")) {
				param.setClause(param.getClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" IN('TEKNIS_INTI', 'TEKNIS_PILIHAN')");
			}else {
				param.setClause(param.getClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" ='"+filterJenisKompetensi.get()+"'");
				param.setSecondClause(param.getSecondClause()+" AND "+"jenis_jabatan_manajerial"+" ='"+filterJenisKompetensi.get()+"'");
			}
		}
		
		if(filterIdKelompokJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterIdTingkatJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");

		if(filterKodeSesi.isPresent()) param.setClause(param.getClause()+" AND "+DetilIndividu.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
		
		if(limit.isPresent()) {
			if (limit.get() > 2000) param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}
		//param.setOrder(UnitKompetensiTeknis.ID);
		List<DetilIndividu> data = detilIndividuMapper.getListDetilPerjabatan(param);
		//carefull to read this
		//this not wrong method, but only when u need this model with ur condition
		//i write this, cause sometimes i forget why make this, and i always almost delete this method
		if(filterIsNonTeknis.isPresent()) {
			data = detilIndividuMapper.getListDetilPerjabatanNonTeknis(param);
		}
		
		resp.setCount(data.size());
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	
	@RequestMapping(value = "/detil-individu-responden", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getListDetilIndividuResponden(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		//
		QueryParameter param = new QueryParameter();
		/*
		if(filterIdEeselon1.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");
		if(request.isUserInRole("ROLE_SPJK_ESELON1")) {
			if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_ESELON1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		if(request.isUserInRole("ROLE_SPJK_SATKER")) {
			if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		
		//
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + "id_propinsi_unit_auditi" + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}
		if(filterIdOrganisasi.isPresent()) {
			param.setClause(param.getClause() + " AND " + "id_propinsi_unit_auditi" + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		if(filterIdPropinsi.isPresent()) {
			param.setClause(param.getClause() + " AND " + "id_propinsi_unit_auditi" + " ='"+filterIdPropinsi.get()+"'");
		}
		//
		
		if(filterJenisKompetensi.isPresent()) {
			if(filterJenisKompetensi.get().equals("TEKNIS")) {
				param.setClause(param.getClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" IN('TEKNIS_INTI', 'TEKNIS_PILIHAN')");
			}else {
				param.setClause(param.getClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" ='"+filterJenisKompetensi.get()+"'");
				param.setSecondClause(param.getSecondClause()+" AND "+"jenis_jabatan_manajerial"+" ='"+filterJenisKompetensi.get()+"'");
			}
		}
		
		if(filterIdKelompokJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterIdTingkatJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");*/
		if(request.isUserInRole("ROLE_SPJK_RESPONDEN")) {
			//if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				
				QueryParameter paramOwn = new QueryParameter();
				if(filterKodeSesi.isPresent()) paramOwn.setClause(paramOwn.getClause()+" AND "+DetilIndividu.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
				paramOwn.setClause(paramOwn.getClause()+" AND "+RataRataKompetensiIndividu.NIP+" ='"+userMapper.findNIPByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
				String idResponden=rataKompetensiIndividuMapper.getList(paramOwn).get(0).getIdUserResponden();
				param.setClause(param.getClause() + " AND " + DetilIndividu.ID_USER_RESPONDEN + "='"+idResponden+"'");
			//}
		}
		
		if(limit.isPresent()) {
			if (limit.get() > 2000) param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}
		//param.setOrder(UnitKompetensiTeknis.ID);
		List<DetilIndividu> data = detilIndividuMapper.getList(param);
		
		resp.setCount(data.size());
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/laporan-nilai-pemetaan", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getListNilaiPemetaan(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_id_organisasi") Optional<String> filterIdOrganisasi,
			@RequestParam("filter_id_propinsi") Optional<String> filterIdPropinsi,
			@RequestParam("filter_jenis_kompetensi") Optional<String> filterJenisKompetensi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_tingkat_jabatan") Optional<String> filterIdTingkatJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjangJabatan,
			@RequestParam("filter_id_bidang_teknis") Optional<String> filterIdBidangTeknis,
			@RequestParam("filter_is_non_teknis") Optional<String> filterIsNonTeknis,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		
		QueryParameter param = new QueryParameter();
		
		//second
		if(filterIdKelompokJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterJenisKompetensi.isPresent()) {
			if(filterJenisKompetensi.get().equals("TEKNIS")) {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" IN('TEKNIS_INTI', 'TEKNIS_PILIHAN')");
			}else {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" ='"+filterJenisKompetensi.get()+"'");
			}
		}
		if(filterKodeSesi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
		if(filterIdEeselon1.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");
		
		if(request.isUserInRole("ROLE_SPJK_ESELON1")) {
			if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_ESELON1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		if(request.isUserInRole("ROLE_SPJK_SATKER")) {
			if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}

		if(filterIdOrganisasi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		if(filterIdPropinsi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " ='"+filterIdPropinsi.get()+"'");
		}
		//
		
//		if(request.isUserInRole("ROLE_SPJK_BDLHK")) {
//			String idAccount = extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
//			triggerService.onGetOrganisasiByUser(entity, idAccount, sql, filter);
//		}
		
		if(filterIdTingkatJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");
		
		//header
		if(filterIdKelompokJabatan.isPresent()) {
			param.setHeaderClause(param.getHeaderClause()+" AND "+Laporan.ID_KELOMPOK_JABATAN_UNIT_KOMPETENSI_TEKNIS+" ='"+filterIdKelompokJabatan.get()+"'");
		}
		
		if(limit.isPresent()) {
			if (limit.get() > 2000) param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}
		//param.setOrder(UnitKompetensiTeknis.ID);

		List<Laporan> data = laporanMapper.getListLaporanNilaiPemetaan(param);
		//carefull to read this
		/*if(filterIsNonTeknis.isPresent()) {
			data = detilIndividuMapper.getListDetilPerjabatanNonTeknis(param);
		}*/
		

		Map<String, Object> map = new HashMap<>();
		//carefull to read this
		/*if(filterIsNonTeknis.isPresent()) {
			data = detilIndividuMapper.getListDetilPerjabatanNonTeknis(param);
		}*/
		//seconClause
		if(filterIdBidangTeknis.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+UserResponden.ID_BIDANG_TEKNIS+" ='"+filterIdBidangTeknis.get()+"'");
		
		//header
		if(filterIdTingkatJabatan.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+DetilIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+DetilIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");
		if(filterIdBidangTeknis.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+filterIdBidangTeknis.get()+"'");
		if(filterKodeSesi.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+"kode_sesi_history_jabatan_kompetensi_teknis"+" ='"+filterKodeSesi.get()+"'");
		
		List<Laporan> newKompTeknis = laporanMapper.getListNewLaporanNilaiPemetaan(param);
		System.out.println("getHeaderClause.toString(): "+param.getHeaderClause());
		System.out.println("getSecondClause.toString(): "+param.getSecondClause());
		System.out.println("getClause.toString(): "+param.getClause());
		System.out.println("QRY: "+"select GROUP_CONCAT(nama_w_type_metode_pengembangan_jenis SEPARATOR ',') pengembangan_result, a.* from( select x.*, y.nilai_alt_detil_individu nilai_unit_kompetensi_teknis, FLOOR(y.nilai_alt_detil_individu) nilai_hasil_result, y.jumlah_unit_komp_detil_individu jumlah_unit_kompetensi_teknis, z.* from( select * from( select id_unit_kompetensi_teknis_history_jabatan_kompetensi_teknis id_unit_kompetensi_teknis, id_kelompok_jabatan_history_jabatan_kompetensi_teknis id_kelompok_jabatan_unit_kompetensi_teknis, id_fungsi_utama_unit_kompetensi_teknis, kode_unit_kompetensi_teknis, judul_unit_kompetensi_teknis, jenis_kompetensi_history_jabatan_kompetensi_teknis jenis_unit_kompetensi_teknis, id_bidang_teknis_history_jabatan_kompetensi_teknis id_bidang_teknis_jabatan_kompetensi_teknis, id_tingkat_master_jabatan id_tingkat_jabatan_detil_individu, id_jenjang_master_jabatan id_jenjang_jabatan_detil_individu, kode_sesi_history_jabatan_kompetensi_teknis from( select b.id_tingkat_master_jabatan, b.id_jenjang_master_jabatan, b.nama_jenjang_master_jabatan, c.id_fungsi_utama_unit_kompetensi_teknis, c.kode_unit_kompetensi_teknis, c.judul_unit_kompetensi_teknis, a.* from spjk_history_jabatan_kompetensi_teknis a left join spjk_master_jabatan b on b.id_master_jabatan = a.id_master_jabatan_history_jabatan_kompetensi_teknis left join spjk_unit_kompetensi_teknis c on c.id_unit_kompetensi_teknis = a.id_unit_kompetensi_teknis_history_jabatan_kompetensi_teknis ) a ) aa #key-param:kode_sesi, id_kelompok_jabatan_unit_kompetensi_teknis, id_bidang_teknis_jabatan_kompetensi_teknis, id_jenjang_master_jabatan WHERE 1 AND id_kelompok_jabatan_unit_kompetensi_teknis ='5' AND id_bidang_teknis_jabatan_kompetensi_teknis ='10' AND kode_sesi_history_jabatan_kompetensi_teknis ='KD_002_2021' ) x left join( select x.*, format(sum(x.nilai_detil_individu)/count(x.nilai_detil_individu),2) nilai_alt_detil_individu, count(*) AS jumlah_unit_komp_detil_individu from( select a.*, b.id_bidang_teknis_user_responden from( select b.*, a.id_eselon1_rata_rata_kompetensi_individu id_eselon1_detil_individu, a.id_unit_auditi_rata_rata_kompetensi_individu id_unit_auditi_detil_individu, a.id_kelompok_jabatan_rata_rata_kompetensi_individu id_kelompok_jabatan_detil_individu, a.id_tingkat_jabatan_rata_rata_kompetensi_individu id_tingkat_jabatan_detil_individu, a.id_jenjang_jabatan_rata_rata_kompetensi_individu id_jenjang_jabatan_detil_individu, a.id_bidang_teknis_rata_rata_kompetensi_individu id_bidang_teknis_detil_individu, a.id_propinsi_rata_rata_kompetensi_individu id_propinsi_detil_individu from spjk_rata_rata_kompetensi_individu a left join spjk_detil_individu b on b.id_user_responden_detil_individu = a.id_user_responden_rata_rata_kompetensi_individu group by id_detil_individu ) a left join spjk_user_responden b on b.id_user_responden = a.id_user_responden_detil_individu left join spjk_unit_auditi c on c.id_unit_auditi = a.id_unit_auditi_detil_individu WHERE 1 AND id_kelompok_jabatan_detil_individu ='5' AND jenis_kompetensi_detil_individu IN('TEKNIS_INTI', 'TEKNIS_PILIHAN') AND kode_sesi_detil_individu ='KD_002_2021' AND id_bidang_teknis_user_responden ='10' #where id_kelompok_jabatan_detil_individu = 5 and jenis_kompetensi_detil_individu like '%TEKNIS_%' and kode_sesi_detil_individu='KD_001_2020' #and id_tingkat_jabatan_detil_individu = 1 and id_jenjang_jabatan_detil_individu = 1 --> ) x group by id_unit_kompetensi_detil_individu, jenis_kompetensi_detil_individu order by kode_unit_kompetensi_detil_individu ) y on y.id_unit_kompetensi_detil_individu = id_unit_kompetensi_teknis left join spjk_fungsi_utama z on z.id_fungsi_utama = x.id_fungsi_utama_unit_kompetensi_teknis group by id_unit_kompetensi_teknis order by id_unit_kompetensi_teknis ) a left join spjk_master_jabatan b on b.id_tingkat_master_jabatan=a.id_tingkat_jabatan_detil_individu and b.id_jenjang_master_jabatan = a.id_jenjang_jabatan_detil_individu and b.id_kelompok_master_jabatan = a.id_kelompok_jabatan_unit_kompetensi_teknis left join( select a.*, b.*, CONCAT(type_metode_pengembangan_jenis,':', a.nama_metode_pengembangan_jenis) nama_w_type_metode_pengembangan_jenis from ( select * from spjk_history_metode_pengembangan_kompetensi a left join spjk_metode_pengembangan_jenis c on c.id_metode_pengembangan_jenis = a.id_metode_jenis_history_metode_pengembangan_kompetensi ) a left join spjk_history_metode_pengembangan_kelompok_nilai b on b.id_master_jabatan_history_metode_pengembangan_kelompok_nilai = a.id_master_jabatan_history_metode_pengembangan_kompetensi and b.id_bidang_teknis_history_metode_pengembangan_kelompok_nilai = a.id_bidang_teknis_history_metode_pengembangan_kompetensi and b.id_metode_jenis_history_metode_pengembangan_kelompok_nilai = a.id_metode_jenis_history_metode_pengembangan_kompetensi and b.other_history_metode_pengembangan_kelompok_nilai is null group by id_history_metode_pengembangan_kompetensi ) c on #c.id_kelompok_jabatan_history_metode_pengembangan_kompetensi=b.id_kelompok_master_jabatan and c.id_master_jabatan_history_metode_pengembangan_kompetensi=b.id_master_jabatan and #c.id_master_jabatan_metode_pengembangan_kompetensi = b.id_master_jabatan and #b.id_bidang_teknis_metode_pengembangan_kompetensi = a.id_bidang_teknis_user_responden and c.id_unit_kompetensi_teknis_history_metode_pengembangan_kompetensi = a.id_unit_kompetensi_teknis and a.nilai_hasil_result <= c.nilai_history_metode_pengembangan_kelompok_nilai group by id_unit_kompetensi_teknis");
		map.put("kompTeknis", data);
		map.put("newKompTeknis", newKompTeknis);
		
		resp.setCount(data.size());
		resp.setData(map);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	

	
	@RequestMapping(value = "/laporan-rekomendasi-pengembangan", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getListRekomendasiPengembangan(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_id_organisasi") Optional<String> filterIdOrganisasi,
			@RequestParam("filter_id_propinsi") Optional<String> filterIdPropinsi,
			@RequestParam("filter_jenis_kompetensi") Optional<String> filterJenisKompetensi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_tingkat_jabatan") Optional<String> filterIdTingkatJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjangJabatan,
			@RequestParam("filter_id_bidang_teknis") Optional<String> filterIdBidangTeknis,
			@RequestParam("filter_is_non_teknis") Optional<String> filterIsNonTeknis,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		
		QueryParameter param = new QueryParameter();
		
		//second
		if(filterIdKelompokJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterJenisKompetensi.isPresent()) {
			if(filterJenisKompetensi.get().equals("TEKNIS")) {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" IN('TEKNIS_INTI', 'TEKNIS_PILIHAN')");
			}else {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" IN('MANAJERIAL', 'SOSIOKULTURAL')");
				//param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" ='"+filterJenisKompetensi.get()+"'");
			}
		}
		if(filterKodeSesi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
		if(filterIdEeselon1.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");
		
		if(request.isUserInRole("ROLE_SPJK_ESELON1")) {
			if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_ESELON1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		if(request.isUserInRole("ROLE_SPJK_SATKER")) {
			if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		
		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}

		if(filterIdOrganisasi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		if(filterIdPropinsi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " ='"+filterIdPropinsi.get()+"'");
		}
		//
		
		if(filterIdTingkatJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");
		
		//header
		if(filterIdKelompokJabatan.isPresent()) {
			param.setHeaderClause(param.getHeaderClause()+" AND "+Laporan.ID_KELOMPOK_JABATAN_UNIT_KOMPETENSI_TEKNIS+" ='"+filterIdKelompokJabatan.get()+"'");
		}
		
		if(limit.isPresent()) {
			if (limit.get() > 2000) param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}
		//param.setOrder(UnitKompetensiTeknis.ID);

		//List<Laporan> data = laporanMapper.getListLaporanNilaiPemetaan(param);
		//carefull to read this
		/*if(filterIsNonTeknis.isPresent()) {
			data = detilIndividuMapper.getListDetilPerjabatanNonTeknis(param);
		}*/
		

		Map<String, Object> map = new HashMap<>();
		//carefull to read this
		/*if(filterIsNonTeknis.isPresent()) {
			data = detilIndividuMapper.getListDetilPerjabatanNonTeknis(param);
		}*/
		//seconClause
		//no for non teknis
		//if(filterIdBidangTeknis.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+UserResponden.ID_BIDANG_TEKNIS+" ='"+filterIdBidangTeknis.get()+"'");
		
		//header
		if(filterIdTingkatJabatan.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+DetilIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+DetilIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");
		if(filterKodeSesi.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+"kode_sesi_history_jabatan_manajerial"+" ='"+filterKodeSesi.get()+"'");
		
		//no for non teknis
		//if(filterIdBidangTeknis.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+filterIdBidangTeknis.get()+"'");
		
		//List<Laporan> newKompTeknis = laporanMapper.getListNewLaporanNilaiPemetaan(param);
		List<Laporan> data = laporanMapper.getListNewLaporanNilaiPemetaanNonTeknis(param);
		//map.put("kompTeknis", data);
		//map.put("newKompTeknis", newKompTeknis);
		
		resp.setCount(data.size());
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	//is this
	@RequestMapping(value = "/laporan-nilai-analisis", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getListNilaiAnalisis(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_id_organisasi") Optional<String> filterIdOrganisasi,
			@RequestParam("filter_id_propinsi") Optional<String> filterIdPropinsi,
			@RequestParam("filter_jenis_kompetensi") Optional<String> filterJenisKompetensi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_tingkat_jabatan") Optional<String> filterIdTingkatJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjangJabatan,
			@RequestParam("filter_id_bidang_teknis") Optional<String> filterIdBidangTeknis,
			@RequestParam("filter_is_non_teknis") Optional<String> filterIsNonTeknis,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		
		QueryParameter param = new QueryParameter();
		
		//second
		if(filterIdKelompokJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterJenisKompetensi.isPresent()) {
			if(filterJenisKompetensi.get().equals("TEKNIS")) {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" IN('TEKNIS_INTI', 'TEKNIS_PILIHAN')");
			}else {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" ='"+filterJenisKompetensi.get()+"'");
			}
		}
		if(filterKodeSesi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
		if(filterIdEeselon1.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");

//		if(request.isUserInRole("ROLE_SPJK_ESELON1")) {
//			if(userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
//				param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_ESELON1 + "='"+userMapper.findEselon1ByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
//			}
//		}
		
		if(request.isUserInRole("ROLE_SPJK_SATKER")) {
			if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}

		if(userMapper.findOrganisasiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " IN("+userMapper.findOrganisasiPropinsiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+")");
		}

		if(filterIdOrganisasi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " IN("+referenceMapper.findOrganisasiPropinsiById(filterIdOrganisasi.get()).get(0)+")");
		}
		if(filterIdPropinsi.isPresent()) {
			param.setSecondClause(param.getSecondClause() + " AND " + DetilIndividu.ID_PROPINSI + " ='"+filterIdPropinsi.get()+"'");
		}
		if(filterIdTingkatJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");
		
		
		//header
		if(filterIdKelompokJabatan.isPresent()) {
			param.setHeaderClause(param.getHeaderClause()+" AND "+Laporan.ID_KELOMPOK_JABATAN_UNIT_KOMPETENSI_TEKNIS+" ='"+filterIdKelompokJabatan.get()+"'");
		}
		
		if(limit.isPresent()) {
			if (limit.get() > 2000) param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}
		//param.setOrder(UnitKompetensiTeknis.ID);

		List<Laporan> data = laporanMapper.getListLaporanNilaiPemetaan(param);
		//carefull to read this
		/*if(filterIsNonTeknis.isPresent()) {
			data = detilIndividuMapper.getListDetilPerjabatanNonTeknis(param);
		}*/

		
		Map<String, Object> map = new HashMap<>();

		//seconClause
		if(filterIdBidangTeknis.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+UserResponden.ID_BIDANG_TEKNIS+" ='"+filterIdBidangTeknis.get()+"'");
		
		//header
		if(filterIdTingkatJabatan.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+DetilIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+DetilIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");
		if(filterIdBidangTeknis.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+filterIdBidangTeknis.get()+"'");
		if(filterKodeSesi.isPresent()) param.setHeaderClause(param.getHeaderClause()+" AND "+"kode_sesi_history_jabatan_kompetensi_teknis"+" ='"+filterKodeSesi.get()+"'");
		
		List<Laporan> newKompTeknis = laporanMapper.getListNewLaporanNilaiPemetaan(param);
		map.put("kompTeknis", newKompTeknis);
		//map.put("newKompTeknis", newKompTeknis);

		//run this param only for fungsi utmaa
		if(filterIdKelompokJabatan.isPresent()) {
			param.setClause(param.getClause()+" AND "+"id_kelompok_jabatan_fungsi_utama"+" ='"+filterIdKelompokJabatan.get()+"'");
		}
		//end param
		//List<LaporanDynamic> fungsiUtama = laporanMapper.getListLaporanPerformaFUtamaStandarKomp(param);

		List<LaporanDynamic> newfungsiUtama = laporanMapper.getListNewLaporanPerformaFUtamaStandarKomp(param);

		//map.put("newFungsiUtama", newfungsiUtama);
		map.put("fungsiUtama", newfungsiUtama);
		
		resp.setCount(data.size());
		resp.setData(map);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	//not this
	@RequestMapping(value = "/not/laporan-nilai-analisis", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getListNilaiAnalisisOLD(
			@RequestParam("filter_kode_sesi") Optional<String> filterKodeSesi,
			@RequestParam("filter_id_eselon1") Optional<String> filterIdEeselon1,
			@RequestParam("filter_id_unit_auditi") Optional<String> filterIdUnitAuditi,
			@RequestParam("filter_jenis_kompetensi") Optional<String> filterJenisKompetensi,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filterIdKelompokJabatan,
			@RequestParam("filter_id_tingkat_jabatan") Optional<String> filterIdTingkatJabatan,
			@RequestParam("filter_id_jenjang_jabatan") Optional<String> filterIdJenjangJabatan,
			@RequestParam("filter_is_non_teknis") Optional<String> filterIsNonTeknis,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		ResponseWrapperList resp = new ResponseWrapperList();
		
		QueryParameter param = new QueryParameter();
		
		//second
		if(filterIdKelompokJabatan.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_KELOMPOK_JABATAN+" ='"+filterIdKelompokJabatan.get()+"'");
		if(filterJenisKompetensi.isPresent()) {
			if(filterJenisKompetensi.get().equals("TEKNIS")) {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" IN('TEKNIS_INTI', 'TEKNIS_PILIHAN')");
			}else {
				param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.JENIS_KOMPETENSI+" ='"+filterJenisKompetensi.get()+"'");
			}
		}
		if(filterKodeSesi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.KODE_SESI+" ='"+filterKodeSesi.get()+"'");
		if(filterIdEeselon1.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_ESELON1+" ='"+filterIdEeselon1.get()+"'");
		if(filterIdUnitAuditi.isPresent()) param.setSecondClause(param.getSecondClause()+" AND "+DetilIndividu.ID_UNIT_AUDITI+" ='"+filterIdUnitAuditi.get()+"'");
		/*
		if(filterIdTingkatJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_TINGKAT_JABATAN+" ='"+filterIdTingkatJabatan.get()+"'");
		if(filterIdJenjangJabatan.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.ID_JENJANG_JABATAN+" ='"+filterIdJenjangJabatan.get()+"'");
		*/
		//header
		if(filterIdKelompokJabatan.isPresent()) {
			param.setHeaderClause(param.getHeaderClause()+" AND "+Laporan.ID_KELOMPOK_JABATAN_UNIT_KOMPETENSI_TEKNIS+" ='"+filterIdKelompokJabatan.get()+"'");
		}
		
		if(limit.isPresent()) {
			if (limit.get() > 2000) param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}
		//param.setOrder(UnitKompetensiTeknis.ID);

		Map<String, Object> map = new HashMap<>();
		List<Laporan> data = laporanMapper.getListLaporanNilaiPemetaan(param);
		//carefull to read this
		/*if(filterIsNonTeknis.isPresent()) {
			data = detilIndividuMapper.getListDetilPerjabatanNonTeknis(param);
		}*/
		
		//run this param only for fungsi utmaa
		if(filterIdKelompokJabatan.isPresent()) {
			param.setClause(param.getClause()+" AND "+"id_kelompok_jabatan_fungsi_utama"+" ='"+filterIdKelompokJabatan.get()+"'");
		}
		//end param
		List<LaporanDynamic> fungsiUtama = laporanMapper.getListLaporanPerformaFUtamaStandarKomp(param);
		
		map.put("kompTeknis", data);
		map.put("fungsiUtama", fungsiUtama);
		
		resp.setCount(data.size());
		resp.setData(map);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/laporan-export", method = RequestMethod.GET)
	public void exportOutput(
			@RequestParam("id_bidang_teknis") Optional<String> idBidangTeknis,
			@RequestParam("kode_sesi") Optional<String> kodeSesi,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {


		long startTime = System.currentTimeMillis();
		
		System.out.println("Utils ip? "+(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")));
		System.out.println("breakdown 1 "+(Utils.ip(request.getServerName()))+"="+request.getServerName());
		System.out.println("breakdown 2 is local? "+(request.getServerName().equals("localhost")));
//		System.out.println("breakdown request "+(cookie1.getDomain()));
//		System.out.println("Is this? "+cookie1);
		System.out.println("never ? "+("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", "")));
		System.out.println("PORT "+request.getServerPort());
		//
		String url = "https://simrenbang.bp2sdm.menlhk.go.id/spektra/restql/rata_rata_kompetensi_individu";
		//if(request.getServerName().equals("localhost")) {
			//url = request.getServerName()+"/spektra/restql/rata_rata_kompetensi_individu";
		//}
			System.out.println("is:"+request.getServerName()+"/spektra/restql/rata_rata_kompetensi_individu");
		System.out.println(url);
		System.out.println("url2: "+request.getServerName()+":" + request.getServerPort() + "/spektra/restql/rata_rata_kompetensi_individu");
		System.out.println("My cookie "+getCookieTokenValue(request, cookieName));
		String yourString = "";
		String stringUnitKompTeknis = "";
		HttpStatus status = HttpStatus.OK;
		ResponseEntity<String> result = null;
		try {
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
	        headers.add("Content-Type", "application/json");
	        headers.add("Authorization", "Bearer " + getCookieTokenValue(request, cookieName));
			String serverName="simrenbang.bp2sdm.menlhk.go.id";
	        String model = "kelompok_manajerial";
	        String params="";
	        //params="?limit=100000";
	        //params+="&join=user_responden,kelompok_jabatan,jenjang_jabatan,eselon1,unit_auditi";
	        
//	        QueryParameter parampertanyaan = new QueryParameter();
//			parampertanyaan.setClause(parampertanyaan.getClause()+" AND "+Pertanyaan.ID_UNIT_KOMPETENSI+"='"+ukt.getId()+"'");
//			parampertanyaan.setClause(parampertanyaan.getClause()+" AND "+Pertanyaan.ID_BIDANG_TEKNIS+"='"+idBidangTeknis.get()+"'");
//			parampertanyaan.setLimit(100000000);
	       url = (request.getServerPort()==80?"https://":"http://")+ (request.getServerPort()==80?serverName:request.getServerName())+ (request.getServerPort()==80?"":":" +request.getServerPort()) + "/spektra/restql/"+model;
	         
	        ResponseEntity<String> resp = new TestRestTemplate().exchange(
                url+params, 
                HttpMethod.GET, 
                new HttpEntity<Object>(headers), 
                String.class
	        );
	        
			//status = resp.getStatusCode(); 
			yourString = resp.getBody()!=null?resp.getBody():"";
			//
			model = "unit_kompetensi_teknis";
			url = (request.getServerPort()==80?"https://":"http://")+ (request.getServerPort()==80?serverName:request.getServerName())+ (request.getServerPort()==80?"":":" +request.getServerPort()) + "/spektra/restql/"+model;
	        System.out.println("newurl"+ url);
	        resp = new TestRestTemplate().exchange(
                url+params, 
                HttpMethod.GET, 
                new HttpEntity<Object>(headers), 
                String.class
	        );
			status = resp.getStatusCode(); 
			stringUnitKompTeknis = resp.getBody()!=null?resp.getBody():"";
			//
		} catch (Exception e) {
	    	System.out.println(e);
	    	e.printStackTrace();
		}


		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;
		ObjectMapper mapperUnitKompTeknis = new ObjectMapper();
		JsonNode rootUnitKompTeknis = null;
		if(status.equals(HttpStatus.OK)) {
			//System.out.println("kelompok-manajerial "+mapper.readTree(yourString));
			root = mapper.readTree(yourString);
			rootUnitKompTeknis = mapperUnitKompTeknis.readTree(stringUnitKompTeknis);
			//System.out.println("root"+root.get("data"));
			//System.out.println("root"+root.get("data"));
		}

		XSSFWorkbook workbook = new XSSFWorkbook();	
		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true); 
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		StyleExcel style = new StyleExcel();
		

		QueryParameter param = new QueryParameter();
		if(kodeSesi.isPresent()) param.setClause(param.getClause()+" AND "+RataRataKompetensiIndividu.KODE_SESI+" ='"+kodeSesi.get()+"'");
		if(request.isUserInRole("ROLE_SPJK_SATKER")) {
			if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
				param.setClause(param.getClause() + " AND " + RataRataKompetensiIndividu.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
			}
		}
		param.setLimit(10000000);
		XSSFSheet sheet = workbook.createSheet("Responden Telah Login("+rataKompetensiIndividuMapper.getList(param).size()+")");
			// create header row
			CellRangeAddress range = null;
			int idxRow = 0;
			XSSFRow title = sheet.createRow(idxRow);
			title.createCell(0).setCellValue("Daftar Responden Telah Login");
			title.getCell(0).setCellStyle(style.styleBody(workbook, mapBody));
			range = new CellRangeAddress(idxRow, idxRow, 0, 5);
			sheet.addMergedRegion(range);

			idxRow++;
			idxRow++;

			XSSFRow header = sheet.createRow(idxRow++);
			int idx = 0;
			String[] headerArr = {"NO:2:1","Nama:2:1","NIP:2:1","Gol:2:1","Jabatan:2:1","Jenjang:2:1","Eselon1:2:1","Satuan Kerja:2:1"};
			List<String> listHeader = new ArrayList<String>();
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }

//			String[] headerArr2 = {"Manajerial:2:1","Sosiokultural:2:1","Teknis:2:1"};
//			for(String namaHeader : headerArr2) { listHeader.add(namaHeader); }

			for(int k=0; k < root.get("data").size(); k++) {
				JsonNode o2 = root.get("data").get(k);
				listHeader.add(o2.get("nama").toString().replaceAll("\"", "")+":2:1");
			}
			listHeader.add("Sosiokultural"+":2:1");
//			for(int k=0; k < rootUnitKompTeknis.get("data").size(); k++) {
//				JsonNode o2 = rootUnitKompTeknis.get("data").get(k);
//				listHeader.add(o2.get("kode").toString().replaceAll("\"", "")+":2:1");
//			}
			String newKString="";
			for(int k=1; k <= 279; k++) {
				if(String.valueOf(k).length() < 3) {
					if(String.valueOf(k).length()==1) {
						newKString="00"+String.valueOf(k);
					}
					if(String.valueOf(k).length()==2) {
						newKString="0"+String.valueOf(k);
					}
				}else {
					newKString=String.valueOf(k);
				}
				listHeader.add("UK"+newKString+":2:1");
			}
			
			int maxBaris = 1;
			for(String namaHeader : listHeader) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0];
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapBody));
					
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
			
			//
				idx=4;
				int i =0;
				CellStyle cellStyleBody = style.styleBody(workbook, mapBody);
				for(RataRataKompetensiIndividu o : rataKompetensiIndividuMapper.getList(param)) {
//				for(int i=0; i < root.get("data").size(); i++) {
					//JsonNode o = root.get("data").get(i);
					//System.out.println("no:"+Integer.valueOf(i+1)+",nama : "+root.get("data").get(i).get("nama"));

				//for(Pertanyaan p : pertanyaanMapper.getList(parampertanyaan)) {
					XSSFRow aRow = sheet.createRow(idx);
					aRow.createCell(0).setCellValue(i+1);
					aRow.getCell(0).setCellStyle(cellStyleBody);

					aRow.createCell(1).setCellValue(o.getNama());
					aRow.getCell(1).setCellStyle(cellStyleBody);
					
					aRow.createCell(2).setCellValue(o.getNip());
					aRow.getCell(2).setCellStyle(cellStyleBody);
				
					aRow.createCell(3).setCellValue(o.getGolongan());
					aRow.getCell(3).setCellStyle(cellStyleBody);
					
					String bidang = "";
					if(o.getIdKelompokJabatan().equals("1") && o.getNamaBidangTeknis()!=null) bidang = " - " + o.getNamaBidangTeknis().substring(4);
					aRow.createCell(4).setCellValue(o.getNamaKelompokJabatan()+bidang);
					aRow.getCell(4).setCellStyle(cellStyleBody);
				
					aRow.createCell(5).setCellValue(o.getNamaJenjangJabatan());
					aRow.getCell(5).setCellStyle(cellStyleBody);
				
					aRow.createCell(6).setCellValue(o.getNamaEselon1());
					aRow.getCell(6).setCellStyle(cellStyleBody);
				
					aRow.createCell(7).setCellValue(o.getNamaUnitAuditi());
					aRow.getCell(7).setCellStyle(cellStyleBody);
				
					int idxsub=8;
					for(int k=0; k < root.get("data").size(); k++) {
						JsonNode o2 = root.get("data").get(k);
						aRow.createCell(idxsub).setCellValue("-");
						//aRow.getCell(idxsub).setCellStyle(style.styleBody(workbook, mapBody));
						for(DetilIndividu o3 : o.getDetilIndividu()) {
							if(o3.getJenisKompetensi().equals("MANAJERIAL")) {
								if(o2.get("id").toString().replaceAll("\"", "").equals((o3.getIdUnitKompetensi()!=null?o3.getIdUnitKompetensi():""))) {
									aRow.getCell(idxsub).setCellValue(o3.getNilai()!=null?o3.getNilai():0);
								}
							}
						}
						idxsub++;
					}
				
					aRow.createCell(idxsub).setCellValue(o.getNilaiAltKompetensiSosiokultural());
					//aRow.getCell(idxsub).setCellStyle(style.styleBody(workbook, mapBody));
					
					idxsub=+idxsub;
					

					for(DetilIndividu o3 : o.getDetilIndividu()) {
						if(o3.getJenisKompetensi().equals("TEKNIS_INTI") || o3.getJenisKompetensi().equals("TEKNIS_PILIHAN")) {
							String kode = o3.getKodeUnitKompetensi().toString().split("\\.")[2];
							/*System.out.println("kode1 "+(kode));
							System.out.println("kode2 "+(Integer.parseInt(kode)));*/
							Integer kodeInt = Integer.parseInt(kode);
							//System.out.println("kode3 "+(idxsub+kodeInt));
							//for testing => kode+":"+
							aRow.createCell(idxsub+kodeInt).setCellValue((o3.getNilai()!=null?o3.getNilai():0));
							//aRow.getCell(idxsub+kodeInt).setCellStyle(style.styleBody(workbook, mapBody));
						}
					}
					
					idx++;
					i++;
				//}
				}

				sheet.autoSizeColumn(0);
				sheet.autoSizeColumn(1);
				sheet.autoSizeColumn(2);
				sheet.autoSizeColumn(4);
				sheet.autoSizeColumn(5);
				sheet.autoSizeColumn(6);
				sheet.autoSizeColumn(7);
				sheet.setColumnWidth(8,(20  * 256) + 200);
				sheet.setColumnWidth(9,(20  * 256) + 200);
				sheet.setColumnWidth(10,(20  * 256) + 200);
				
				/*
				param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+UserResponden.ID+" IS NULL");
				param.setLimit(1000000);
				sheet = workbook.createSheet("Responden Belum Login("+mappingJabatanUnitAuditiMapper.getCount(param)+")");
				// create header row
				range = null;
				idxRow = 0;
				title = sheet.createRow(idxRow);
				title.createCell(0).setCellValue("Daftar Responden Belum Login");
				title.getCell(0).setCellStyle(style.styleBody(workbook, mapBody));
				range = new CellRangeAddress(idxRow, idxRow, 0, 5);
				sheet.addMergedRegion(range);

				idxRow++;
				idxRow++;

				header = sheet.createRow(idxRow++);
				idx = 0;
				//String[] headerArr = {"NO:2:1","Nama:2:1","NIP:2:1","Gol:2:1","Jabatan:2:1","Jenjang:2:1","Eselon1:2:1","Satuan Kerja:2:1"};
				//List<String> listHeader = new ArrayList<String>();
				//for(String namaHeader : headerArr) { listHeader.add(namaHeader); }

				//String[] headerArr2 = {"Manajerial:2:1","Sosiokultural:2:1","Teknis:2:1"};
				//for(String namaHeader : headerArr2) { listHeader.add(namaHeader); }

				maxBaris = 1;
				for(String namaHeader : listHeader) {
					if(namaHeader.split(":").length > 1) {
						String nama = namaHeader.split(":")[0];
						int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
						int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
						maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
						header.createCell(idx).setCellValue(nama);
						header.getCell(idx).setCellStyle(style.styleBody(workbook, mapBody));
						
						if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
							range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
							RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
							RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
							RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
							RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
							sheet.addMergedRegion(range);
						}
						idx += jumlahKolom;
					}
					else {
						header.createCell(idx).setCellValue(namaHeader);
						header.getCell(idx).setCellStyle(style.styleHeader(workbook));
						idx++;
					}
				}
				
				//

					idx=4;
					i =0;
					for(MappingJabatanUnitAuditi o : mappingJabatanUnitAuditiMapper.getList(param)) {
//					for(int i=0; i < root.get("data").size(); i++) {
						//JsonNode o = root.get("data").get(i);
						//System.out.println("no:"+Integer.valueOf(i+1)+",nama : "+root.get("data").get(i).get("nama"));

					//for(Pertanyaan p : pertanyaanMapper.getList(parampertanyaan)) {
						XSSFRow aRow = sheet.createRow(idx);
						aRow.createCell(0).setCellValue(i+1);
						aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapBody));

						aRow.createCell(1).setCellValue(o.getAccount().getFirstName()+""+o.getAccount().getLastName().toString().replaceAll("\"", ""));
						aRow.getCell(1).setCellStyle(style.styleBody(workbook, mapBody));
						
						aRow.createCell(2).setCellValue(o.getAccount().getUsername().toString().replaceAll("\"", ""));
						aRow.getCell(2).setCellStyle(style.styleBody(workbook, mapBody));
					
						aRow.createCell(3).setCellValue("-".toString().replaceAll("\"", ""));
						aRow.getCell(3).setCellStyle(style.styleBody(workbook, mapBody));
//					
						aRow.createCell(4).setCellValue(o.getMasterJabatan().getNamaKelompok().toString().replaceAll("\"", ""));
						aRow.getCell(4).setCellStyle(style.styleBody(workbook, mapBody));
					
						aRow.createCell(5).setCellValue(o.getMasterJabatan().getNamaJenjang().toString().replaceAll("\"", ""));
						aRow.getCell(5).setCellStyle(style.styleBody(workbook, mapBody));
					
						aRow.createCell(6).setCellValue(o.getEselon1().getNama().toString().replaceAll("\"", ""));
						aRow.getCell(6).setCellStyle(style.styleBody(workbook, mapBody));
					
						aRow.createCell(7).setCellValue(o.getUnitAuditi().getNamaUnitAuditi().toString().replaceAll("\"", ""));
						aRow.getCell(7).setCellStyle(style.styleBody(workbook, mapBody));
					
//						aRow.createCell(8).setCellValue("-");
//						aRow.getCell(8).setCellStyle(style.styleBody(workbook, mapBody));
//					
//						aRow.createCell(9).setCellValue("-");
//						aRow.getCell(9).setCellStyle(style.styleBody(workbook, mapBody));
//					
//						aRow.createCell(10).setCellValue("-");
//						aRow.getCell(10).setCellStyle(style.styleBody(workbook, mapBody));
						idx++;
						i++;
					//}
					}
				//

	//}
			
//			sheet.setColumnWidth(0, (5  * 256) + 200); 
//			sheet.setColumnWidth(2, (75  * 256) + 200); 
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
//			sheet.setColumnWidth(4, (15  * 256) + 200); 
			sheet.autoSizeColumn(4);
			sheet.autoSizeColumn(5);
			sheet.autoSizeColumn(6);
			sheet.autoSizeColumn(7);

			sheet.setColumnWidth(8,(20  * 256) + 200);
			sheet.setColumnWidth(9,(20  * 256) + 200);
			sheet.setColumnWidth(10,(20  * 256) + 200);
			*/
		
		//1
		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename=SPJK-"+"export-pertanyaan"+"-"+new Date()+".xlsx"); //name file
		
		workbook.write(response.getOutputStream());
		workbook.close();
		
		
		//2	
//		ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
//		workbook.write(outByteStream);
//		byte[] outArray = outByteStream.toByteArray();
//		
//		response.setContentType("application/xlsx");
//		response.setContentLength(outArray.length);
//		response.setHeader("Expires:", "0");
//		response.setHeader("Content-Disposition", "attachment; filename=SPJK-"+"export-pertanyaan"+"-"+new Date()+".xlsx"); //name file
//		OutputStream outputStream = response.getOutputStream();
//		outputStream.write(outArray);
//		outputStream.flush();
//		workbook.close();
		//3
//		String filename="SPJK-"+"export-responden"+"-"+new Date()+".xlsx";
//		try {
//			filename = new String(filename.getBytes("UTF-8"), "ISO-8859-1");
//			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
//			OutputStream stream = response.getOutputStream();
//			if(null != workbook && null != stream) {
//				workbook.write(stream);
//				workbook.close();
//				stream.close();
//				long stopTime = System.currentTimeMillis();
//				System.out.println("write xlsx file time: "+ (stopTime - startTime)/1000 + "m");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
}
