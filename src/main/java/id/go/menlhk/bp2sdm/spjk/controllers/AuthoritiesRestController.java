package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

import id.go.menlhk.bp2sdm.spjk.MainApplication;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.common.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.common.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.common.Utils;
import id.go.menlhk.bp2sdm.spjk.core.Token;
import id.go.menlhk.bp2sdm.spjk.domain.MappingJabatanUnitAuditi;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.AssignmentMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.AuthoritiesMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingJabatanUnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.model.Account;
import id.go.menlhk.bp2sdm.spjk.model.Application;
import id.go.menlhk.bp2sdm.spjk.model.Assignment;
import id.go.menlhk.bp2sdm.spjk.model.Authorities;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@Controller
@RequestMapping("/authorities")
public class AuthoritiesRestController extends BaseController {

	public final static String DELETE = "delete";
	public final static String RESET = "reset";
	
	@Autowired
	private AssignmentMapper assignmentMapper;
	
	@Autowired
	private AuthoritiesMapper authoritiesMapper;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
    private MessageSource messageSource;
	
	@Autowired
	private UserMapper userMapper;

	
	@Autowired
	private MappingJabatanUnitAuditiMapper mappingJabatanUnitAuditiMapper;
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		@RequestParam("filter_application") String filterApplication,
    		@RequestParam("filter_group") Optional<String> filterGroup,
    		HttpServletRequest request, Locale locale
    		)throws Exception {
		
		// System.out.println("test: "+messageSource.getMessage("account.kolom.label.firstname", null, locale));
		
		QueryParameter param = new QueryParameter();
		
    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + Account.USERNAME + " LIKE '%"+filterKeyword.get()+"%' ");
    		param.setClause(param.getClause() + " OR " + Account.FIRST_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
    		param.setClause(param.getClause() + " OR " + "full_name_account" + " LIKE '%"+filterKeyword.get()+"%' ");
    		param.setClause(param.getClause() + " OR " + Account.LAST_NAME + " LIKE '%"+filterKeyword.get()+"%')");
    	}
    	
    	
    	param.setClause(param.getClause() + " AND (" + Application.ID + "='"+filterApplication+"'");
    	param.setClause(param.getClause() + " OR " + Application.CODE + "='"+filterApplication+"')");
    	
    	//Map<String, Object> loginUserMap = getLoginUserMap(request);

	    DecodedJWT jwt = JWT.decode(extractHeaderToken(request));
		System.out.println("Ve could, loginUserMap "+jwt.getClaim(Token.AUTHORITIES).asList(String.class));
		// filter only run when satker detectec
		//if(jwt.getClaim(Token.AUTHORITIES) .asList(String.class).toString().equalsIgnoreCase("ROLE_SPJK_SATKER")) {
		if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + Account.UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		//}
    	if(filterGroup.isPresent() && !filterGroup.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + Authorities.GROUP_ID + " = '"+filterGroup.get()+"')");
    	}
    	
    	if(!jwt.getClaim(Token.AUTHORITIES).asList(String.class).contains("ROLE_DEVELOPER")) {
    		param.setClause(param.getClause() + " AND (" + Authorities.GROUP_ID + " != '"+"9999999999999"+"')");
    	}
    	
    	if(limit.isPresent()) {
    		param.setLimit(limit.get());
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    		
    		if(noOffset.isPresent()) {
    			param.setOffset(0);
    			param.setLimit(pPage*param.getLimit());
    		}
    	}
	
    	param.setLocale(locale.toString());
    	param.setOrder(Account.TIME_ADDED+" desc");
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Authorities> data = authoritiesMapper.getList(param);
		
		resp.setCount(authoritiesMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Map<String, Object> data = new HashMap<String, Object>();
		Authorities authorities = authoritiesMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+authorities.getAccount().getUsername()+"'?");
			}
		}
		//default
		data.put("account", authorities);
		if(userMapper.findUnitAuditiUser(authorities.getAccountId()).size()>0)
			data.put("unit_auditi", userMapper.findUnitAuditiUser(authorities.getAccountId()).get(0));
		if(userMapper.findEselon1ByUserId(authorities.getAccountId()).size()>0)
			data.put("eselon1", userMapper.findEselon1ByUserId(authorities.getAccountId()).get(0));
		if(userMapper.findPropinsiByUserId(authorities.getAccountId()).size()>0)
			data.put("propinsi", userMapper.findPropinsiByUserId(authorities.getAccountId()).get(0));
		if(userMapper.findOrganisasiByUserId(authorities.getAccountId()).size()>0)
			data.put("organisasi", userMapper.findOrganisasiByUserId(authorities.getAccountId()).get(0));
		//Thread.sleep(1000);
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("account_id") Optional<String> accountId,
    		@RequestParam("group_id") Optional<String> groupId,
    		@RequestParam("application_id") Optional<String> applicationId,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Authorities data = new Authorities(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = authoritiesMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(accountId.isPresent()) data.setAccountId(accountId.get());
    		if(groupId.isPresent()) data.setGroupId(groupId.get());
    		if(applicationId.isPresent()) data.setApplicationId(applicationId.get());
    		

    		boolean pass = true;
    		if(data.getAccountId() == null || data.getGroupId() == null || data.getApplicationId() == null) {
    			pass = false;
    		}else {
    			if(data.getAccountId().equals("") || data.getGroupId().equals("") || data.getApplicationId().equals("")) {
        			pass = false;
        		}
    		}
    		
    		

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		
    		try {
    			if(id.isPresent()) {
    				
        			authoritiesMapper.update(data);
        		}else {
        			data.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
            		data.setTimeAdded(new Date());
    				
        			authoritiesMapper.insert(data);	
        		}
		} catch (Exception e) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("FATAL ERROR!!! Contact administrator");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action,
			@RequestParam("force") Optional<String> force,
			HttpServletRequest request
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			Authorities data = authoritiesMapper.getEntity(id);
			resp.setData(data);
			authoritiesMapper.delete(data);
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+data.getAccount().getId()+"'");
			if(authoritiesMapper.getCount(param)==0) {
				accountMapper.delete(data.getAccount());
			}
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + "account_id_control" + "='"+data.getAccount().getId()+"'");
			param.setClause(param.getClause() + " AND " + "source_id_control" + "='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
			authoritiesMapper.deleteControlBatch(param);
			/*if(force.isPresent() && (force.get().equals("true") || force.get().equals("1")) && request.isUserInRole("SUPER_ADMIN")) {
				accountMapper.delete(data.getAccount());
				
				// hapus semua athorities dari account yang bersangkutan
				QueryParameter param = new QueryParameter();
				param.setClause(param.getClause() + " AND " + Assignment.ACCOUNT_ID + "='"+data.getAccount().getId()+"'");
				//assignmentMapper.deleteBatch(param);
			}*/
			//delete mapping jabatan if exist
			id.go.menlhk.bp2sdm.spjk.core.QueryParameter params = new id.go.menlhk.bp2sdm.spjk.core.QueryParameter();
			params.setClause(params.getClause() + " AND " + MappingJabatanUnitAuditi.ID_ACCOUNT + "='"+data.getAccount().getId()+"'");
			mappingJabatanUnitAuditiMapper.deleteBatch(params);
			
			//delete mapping satker if exist
			userMapper.deleteAllUnitAuditiUser(data.getAccountId());
			
			resp.setMessage("Data telah dihapus.");
		}
		if(action.equals(RESET)) {
			Authorities data = authoritiesMapper.getEntity(id);
			System.out.println("try reset "+data.getAccountId());
			Account account = accountMapper.getEntity(data.getAccount().getId());//+extractAccountLogin(request, AccountLoginInfo.SOURCE)
			account.setEnabled(false);
			accountMapper.update(account);
			data.getAccount().setEnabled(false);
			resp.setData(data);
			resp.setMessage("Data '"+account.getUsername()+"' telah "+RESET+".");
		}
		
		
//		Thread.sleep(1000);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
