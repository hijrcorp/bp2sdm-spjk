package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.MappingFungsi;
import id.go.menlhk.bp2sdm.spjk.domain.PeranResponden;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingFungsiMapper;

@RestController
@RequestMapping("/mapping-fungsi")
public class MappingFungsiController {

	@Autowired
	private MappingFungsiMapper mappingFungsiMapper;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseWrapperList getList(HttpServletRequest request,
			@RequestParam("filter_keyword") Optional<String> filter_keyword,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {

		QueryParameter param = new QueryParameter();
		if (filter_keyword.isPresent())
			param.setClause(param.getClause() + " AND " + PeranResponden.NAMA_PERAN_RESPONDEN + " LIKE '%"
					+ filter_keyword.get() + "%'");
		if (limit.isPresent()) {
			param.setLimit(limit.get());
			if (limit.get() > 2000) param.setLimit(2000);
		} else {
			param.setLimit(2000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		ResponseWrapperList resp = new ResponseWrapperList();
		// param.setClause("1");
		List<MappingFungsi> data = mappingFungsiMapper.getList(param);
		resp.setCount(mappingFungsiMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return resp;
	}

}
