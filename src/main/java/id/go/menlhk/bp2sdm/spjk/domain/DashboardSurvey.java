package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.go.menlhk.bp2sdm.spjk.domain.bc.Pernyataan;

public class DashboardSurvey extends Pernyataan {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	public static final String ID_UNIT_AUDITI = "id_unit_auditi";
	public static final String NAMA_UNIT_AUDITI = "nama_unit_auditi";
	public static final String ID_PROPINSI_UNIT_AUDITI = "id_propinsi_unit_auditi";
	public static final String RESPONDEN = "responden";
	public static final String TIDAK_RESPON = "tidak_respon";
	public static final String PROSES_RESPON = "proses_respon";
	public static final String SELESAI_RESPON = "selesai_respon";

	private String idUnitAuditi;
	private String namaUnitAuditi;
	private String idPropinsiUnitAuditi;
	private Integer responden;
	private Integer tidakRespon;
	private Integer prosesRespon;
	private Integer selesaiRespon;

	public DashboardSurvey() {

	}

	@JsonProperty(ID_UNIT_AUDITI)
	public String getIdUnitAuditi() {
		return idUnitAuditi;
	}

	public void setIdUnitAuditi(String idUnitAuditi) {
		this.idUnitAuditi = idUnitAuditi;
	}

	@JsonProperty(NAMA_UNIT_AUDITI)
	public String getNamaUnitAuditi() {
		return namaUnitAuditi;
	}

	public void setNamaUnitAuditi(String namaUnitAuditi) {
		this.namaUnitAuditi = namaUnitAuditi;
	}

	@JsonProperty(ID_PROPINSI_UNIT_AUDITI)
	public String getIdPropinsiUnitAuditi() {
		return idPropinsiUnitAuditi;
	}

	public void setIdPropinsiUnitAuditi(String idPropinsiUnitAuditi) {
		this.idPropinsiUnitAuditi = idPropinsiUnitAuditi;
	}

	@JsonProperty(RESPONDEN)
	public Integer getResponden() {
		return responden;
	}

	public void setResponden(Integer responden) {
		this.responden = responden;
	}

	@JsonProperty(TIDAK_RESPON)
	public Integer getTidakRespon() {
		return tidakRespon;
	}

	public void setTidakRespon(Integer tidakRespon) {
		this.tidakRespon = tidakRespon;
	}

	@JsonProperty(PROSES_RESPON)
	public Integer getProsesRespon() {
		return prosesRespon;
	}

	public void setProsesRespon(Integer prosesRespon) {
		this.prosesRespon = prosesRespon;
	}

	@JsonProperty(SELESAI_RESPON)
	public Integer getSelesaiRespon() {
		return selesaiRespon;
	}

	public void setSelesaiRespon(Integer selesaiRespon) {
		this.selesaiRespon = selesaiRespon;
	}

}
