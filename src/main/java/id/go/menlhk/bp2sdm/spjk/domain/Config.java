package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Config {
	public static final String ID = "id_config";
	public static final String CODE = "code_config";
	public static final String VALUE = "value_config";
	public static final String STATUS = "status_config";

	private String id;
	private String code;
	private String value;
	private String status;

	public Config() {

	}

	public Config(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	/**********************************************************************/
}
