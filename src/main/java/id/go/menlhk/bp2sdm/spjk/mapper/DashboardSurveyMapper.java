package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardTahunSurvey;

public interface DashboardSurveyMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	List<DashboardTahunSurvey> getListTahunSurvey(QueryParameter param);

	/**********************************
	 * - End Generate -
	 ************************************/

}
