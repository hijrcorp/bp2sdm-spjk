package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.PertanyaanJawaban;

public interface PertanyaanJawabanMapper {

	@Insert("INSERT INTO spjk_pertanyaan_jawaban (id_pertanyaan_jawaban, id_header_pertanyaan_jawaban, order_pertanyaan_jawaban, isi_pertanyaan_jawaban, bobot_pertanyaan_jawaban, file_pertanyaan_jawaban) VALUES (#{id:VARCHAR}, #{idHeader:VARCHAR}, #{order:NUMERIC}, #{isi:VARCHAR}, #{bobot:NUMERIC}, #{file:NUMERIC})")
	void insert(PertanyaanJawaban pertanyaanJawaban);

	@Update("UPDATE spjk_pertanyaan_jawaban SET id_pertanyaan_jawaban=#{id:VARCHAR}, id_header_pertanyaan_jawaban=#{idHeader:VARCHAR}, order_pertanyaan_jawaban=#{order:NUMERIC}, isi_pertanyaan_jawaban=#{isi:VARCHAR}, bobot_pertanyaan_jawaban=#{bobot:NUMERIC}, file_pertanyaan_jawaban=#{file:NUMERIC} WHERE id_pertanyaan_jawaban=#{id}")
	void update(PertanyaanJawaban pertanyaanJawaban);

	@Delete("DELETE FROM spjk_pertanyaan_jawaban WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_pertanyaan_jawaban WHERE id_pertanyaan_jawaban=#{id}")
	void delete(PertanyaanJawaban pertanyaanJawaban);

	List<PertanyaanJawaban> getList(QueryParameter param);

	PertanyaanJawaban getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

}
