package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MappingFungsiKunciDetil {
	public static final String ID = "id_mapping_fungsi_kunci_detil";
	public static final String ID_MAPPING_FUNGSI = "id_mapping_fungsi_mapping_fungsi_kunci_detil";
	public static final String FUNGSI_KUNCI = "fungsi_kunci_mapping_fungsi_kunci_detil";

	private String id;
	private String idMappingFungsi;
	private String fungsiKunci;

	public MappingFungsiKunciDetil() {

	}

	public MappingFungsiKunciDetil(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_mapping_fungsi")
	public String getIdMappingFungsi() {
		return idMappingFungsi;
	}

	public void setIdMappingFungsi(String idMappingFungsi) {
		this.idMappingFungsi = idMappingFungsi;
	}

	@JsonProperty("fungsi_kunci")
	public String getFungsiKunci() {
		return fungsiKunci;
	}

	public void setFungsiKunci(String fungsiKunci) {
		this.fungsiKunci = fungsiKunci;
	}
	
	private List<MappingFungsiUtamaDetil> listMappingFungsiUtamaDetil = new ArrayList<MappingFungsiUtamaDetil>();

	public List<MappingFungsiUtamaDetil> getListMappingFungsiUtamaDetil() {
		return listMappingFungsiUtamaDetil;
	}

	public void setListMappingFungsiUtamaDetil(List<MappingFungsiUtamaDetil> listMappingFungsiUtamaDetil) {
		this.listMappingFungsiUtamaDetil = listMappingFungsiUtamaDetil;
	}
}
