package id.go.menlhk.bp2sdm.spjk.common;

import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class StyleExcel {

	// start dynamic style
//	public XSSFCellStyle styleAlign(XSSFWorkbook wb, String fn) {
//		XSSFCellStyle style = wb.createCellStyle();
//		if(fn.equals("center")) {
//			style.setAlignment(CellStyle.ALIGN_CENTER);
//			style.setVerticalAlignment(CellStyle.VERTICAL_TOP);
//		}else if(fn.equals("verticalTop")) {
//			style.setAlignment(HorizontalAlignment.LEFT);
//			style.setWrapText(true);
//			style.setVerticalAlignment(CellStyle.VERTICAL_TOP);
//		}else if(fn.equals("wrap")) {
//			style.setWrapText(true);
//		}else if(fn.equals("title")) {
//			style.setWrapText(true);
//			style.setAlignment(CellStyle.ALIGN_CENTER);
//			style.setVerticalAlignment(CellStyle.VERTICAL_TOP);
//		}
//		return style;
//	}
//	public XSSFCellStyle fontStyle(XSSFWorkbook wb, String fn) {
//		XSSFCellStyle styleHead=wb.createCellStyle();
//        XSSFFont font = wb.createFont();
//        font.setFontHeightInPoints((short) 12);
//        if(fn.equals("bold")) {
//        	font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//        }
//        styleHead.setFont(font);
//        return styleHead;
//	}
//	public XSSFCellStyle styleHeader(XSSFWorkbook workbook,String fn) {
//		XSSFCellStyle styleSection=workbook.createCellStyle();
//		XSSFFont font = workbook.createFont();
//		font = workbook.createFont();
//        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//        styleSection.setFont(font);
//        styleSection.setFillForegroundColor(new XSSFColor(new java.awt.Color(233, 236, 239)));
//        styleSection.setFillPattern(CellStyle.SOLID_FOREGROUND);    
//        styleSection.setAlignment(CellStyle.ALIGN_CENTER);
//        styleSection.setBorderBottom(CellStyle.BORDER_THIN);
//        styleSection.setBorderTop(CellStyle.BORDER_THIN);
//        styleSection.setBorderRight(CellStyle.BORDER_THIN);
//        styleSection.setBorderLeft(CellStyle.BORDER_THIN);
//        styleSection.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//        if(fn == "wrap")styleSection.setWrapText(true);
//        return styleSection;
//	}
//	public XSSFCellStyle styleTitle(XSSFWorkbook workbook,String fn) {
//		XSSFCellStyle styleSection=workbook.createCellStyle();
//		XSSFFont font = workbook.createFont();
//		font = workbook.createFont();
//        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//        font.setFontHeightInPoints((short) 12);
//        styleSection.setFont(font);  
//        styleSection.setAlignment(CellStyle.ALIGN_CENTER);
//        styleSection.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//        if(fn == "wrap")styleSection.setWrapText(true);
//        return styleSection;
//	}
//	public XSSFCellStyle styleHeader(XSSFWorkbook workbook) {
//		XSSFCellStyle styleSection=workbook.createCellStyle();
//		XSSFFont font = workbook.createFont();
//		font = workbook.createFont();
//        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//        styleSection.setFont(font);
//        styleSection.setFillForegroundColor(new XSSFColor(new java.awt.Color(233, 236, 239)));
//        styleSection.setFillPattern(CellStyle.SOLID_FOREGROUND);    
//        styleSection.setAlignment(CellStyle.ALIGN_CENTER);
//        styleSection.setBorderBottom(CellStyle.BORDER_THIN);
//        styleSection.setBorderTop(CellStyle.BORDER_THIN);
//        styleSection.setBorderRight(CellStyle.BORDER_THIN);
//        styleSection.setBorderLeft(CellStyle.BORDER_THIN);
//        styleSection.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//        return styleSection;
//	}
//	public XSSFCellStyle styleBody(XSSFWorkbook workbook,Map<String, Boolean> fn) {
//		XSSFCellStyle style=workbook.createCellStyle();
//		XSSFFont font = workbook.createFont();
//		style.setFont(font);
////		try {
////            if(fn.get("center")) {
////            	style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
////        		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
////            }
////		} catch(NullPointerException e) {
////		    e.printStackTrace();
////		}
////		try {
////            if(fn.get("verticalTop")) {
////    			style.setAlignment(HorizontalAlignment.LEFT);
////    			style.setWrapText(true);
////    			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);
////            }
////		} catch(NullPointerException e) {
////		    e.printStackTrace();
////		}
////		try {
////			 if(fn.get("formatNumber")) {
////		        XSSFDataFormat format = workbook.createDataFormat();
////		        style.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);
////		        style.setDataFormat(format.getFormat("#,###,###,###,###"));
////			 }
////		}catch (Exception e) {
////			e.printStackTrace();
////		}
////        try {
////            if(fn.get("wrap"))style.setWrapText(true);
////		} catch(NullPointerException e) {
////			e.printStackTrace();
////		}
////        style.setWrapText(true);
//		style.setBorderBottom(CellStyle.BORDER_THIN);
//		style.setBorderTop(CellStyle.BORDER_THIN);
//		style.setBorderRight(CellStyle.BORDER_THIN);
//		style.setBorderLeft(CellStyle.BORDER_THIN);
//		return style;
//	}
//	public XSSFCellStyle formatNumber(XSSFWorkbook workbook) {
//        XSSFCellStyle styleNumber=workbook.createCellStyle();
//        XSSFDataFormat format = workbook.createDataFormat();
//        styleNumber.setVerticalAlignment(CellStyle.VERTICAL_TOP);
//        styleNumber.setDataFormat(format.getFormat("#,###,###,###,###"));
//        return styleNumber;
//	}
//	public XSSFCellStyle formatNumber(XSSFWorkbook workbook,int backComma) {
//        XSSFCellStyle styleNumber=workbook.createCellStyle();
//        XSSFDataFormat format = workbook.createDataFormat();
//        styleNumber.setVerticalAlignment(CellStyle.VERTICAL_TOP);
//        styleNumber.setDataFormat(format.getFormat("#,###,###,###,###.00"));
//        return styleNumber;
//	}
	//end here
}
