package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Kelompok {

	public static final String ID_KM = "id_kelompok_manajerial";
	public static final String NAMA_KM = "nama_kelompok_manajerial";
	public static final String ID_SOURCE_KM = "id_source_kelompok_manajerial";
	public static final String ID_ACCOUNT_ADDED_KM = "id_account_added_kelompok_manajerial";
	public static final String TIMESTAMP_ADDED_KM = "timestamp_added_kelompok_manajerial";
	public static final String ID_ACCOUNT_MODIFIED_KM = "id_account_modified_kelompok_manajerial";
	public static final String TIMESTAMP_MODIFIED_KM = "timestamp_modified_kelompok_manajerial";

	public static final String ID_KS = "id_kelompok_sosiokultural";
	public static final String NAMA_KS = "nama_kelompok_sosiokultural";
	public static final String ID_SOURCE_KS = "id_source_kelompok_sosiokultural";
	public static final String ID_ACCOUNT_ADDED_KS = "id_account_added_kelompok_sosiokultural";
	public static final String TIMESTAMP_ADDED_KS = "timestamp_added_kelompok_sosiokultural";
	public static final String ID_ACCOUNT_MODIFIED_KS = "id_account_modified_kelompok_sosiokultural";
	public static final String TIMESTAMP_MODIFIED_KS = "timestamp_modified_kelompok_sosiokultural";
	
	private String id;
	private String nama;
	private String idSource;
	private String idAccountAdded;
	private Date timestampAdded;
	private String idAccountModified;
	private Date timestampModified;
	
	public Kelompok() {
	
	}
	
	public Kelompok(String id) {
		this.id = id;
	}
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}
	
	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}
	
	@JsonProperty("id_account_added")
	public String getIdAccountAdded() {
		return idAccountAdded;
	}
	
	public void setIdAccountAdded(String idAccountAdded) {
		this.idAccountAdded = idAccountAdded;
	}
	
	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}
	
	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}
	
	@JsonProperty("id_account_modified")
	public String getIdAccountModified() {
		return idAccountModified;
	}
	
	public void setIdAccountModified(String idAccountModified) {
		this.idAccountModified = idAccountModified;
	}
	
	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}
	
	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}
	
	
	
	/**********************************************************************/
}
