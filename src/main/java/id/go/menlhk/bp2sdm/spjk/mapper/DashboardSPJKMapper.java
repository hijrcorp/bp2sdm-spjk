package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardBidangSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardMonitoringSPJK;
import id.go.menlhk.bp2sdm.spjk.domain.DashboardSPJK;

public interface DashboardSPJKMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	List<DashboardSPJK> getListMain(QueryParameter param);

	List<DashboardBidangSPJK> getListBidangPEH(QueryParameter param);

	List<DashboardMonitoringSPJK> getListMonitoringStatus(QueryParameter param);

	/**********************************
	 * - End Generate -
	 ************************************/

}
