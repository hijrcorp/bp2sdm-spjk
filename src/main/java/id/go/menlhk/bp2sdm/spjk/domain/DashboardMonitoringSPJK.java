package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DashboardMonitoringSPJK {
	
	private Integer totalResponden;
	private Integer respondenSelesai;
	private Integer respondenProses;
	private Integer respondenNoProses;
	private Integer respondenDuplicate;
	private Integer menjawabDuplicate;
	private Integer bidangTeknisInvalid;
	private Integer jabatanInvalid;

	
	public DashboardMonitoringSPJK() {
	
	}
	
	public DashboardMonitoringSPJK(String id) {
		
	}
	
	public Integer getTotalResponden() {
		return totalResponden;
	}

	public void setTotalResponden(Integer totalResponden) {
		this.totalResponden = totalResponden;
	}

	public Integer getRespondenSelesai() {
		return respondenSelesai;
	}

	public void setRespondenSelesai(Integer respondenSelesai) {
		this.respondenSelesai = respondenSelesai;
	}

	public Integer getRespondenProses() {
		return respondenProses;
	}

	public void setRespondenProses(Integer respondenProses) {
		this.respondenProses = respondenProses;
	}

	public Integer getRespondenNoProses() {
		return respondenNoProses;
	}

	public void setRespondenNoProses(Integer respondenNoProses) {
		this.respondenNoProses = respondenNoProses;
	}

	public Integer getRespondenDuplicate() {
		return respondenDuplicate;
	}

	public void setRespondenDuplicate(Integer respondenDuplicate) {
		this.respondenDuplicate = respondenDuplicate;
	}

	
	public Integer getMenjawabDuplicate() {
		return menjawabDuplicate;
	}

	public void setMenjawabDuplicate(Integer menjawabDuplicate) {
		this.menjawabDuplicate = menjawabDuplicate;
	}

	public Integer getBidangTeknisInvalid() {
		return bidangTeknisInvalid;
	}

	public void setBidangTeknisInvalid(Integer bidangTeknisInvalid) {
		this.bidangTeknisInvalid = bidangTeknisInvalid;
	}

	public Integer getJabatanInvalid() {
		return jabatanInvalid;
	}

	public void setJabatanInvalid(Integer jabatanInvalid) {
		this.jabatanInvalid = jabatanInvalid;
	}

	
}