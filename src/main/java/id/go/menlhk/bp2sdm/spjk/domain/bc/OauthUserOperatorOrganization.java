package id.go.menlhk.bp2sdm.spjk.domain.bc;

public class OauthUserOperatorOrganization extends OauthUserOrganization {

	public static final String UNIT_AUDITI_ID = "unit_name_id";
	public static final String UNIT_AUDITI_NAME = "unit_auditi_name";

	private String unitAuditiId;
	private String unitAuditiName;

	public OauthUserOperatorOrganization() {

	}

	public OauthUserOperatorOrganization(String id) {
		super(id);
	}

	public String getUnitAuditiId() {
		return unitAuditiId;
	}

	public void setUnitAuditiId(String unitAuditiId) {
		this.unitAuditiId = unitAuditiId;
	}

	public String getUnitAuditiName() {
		return unitAuditiName;
	}

	public void setUnitAuditiName(String unitAuditiName) {
		this.unitAuditiName = unitAuditiName;
	}

}
