package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.PeranResponden;

public interface PeranRespondenMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	@Insert("INSERT INTO survey_peran_responden (id_peran_responden, nama_peran_responden) VALUES (#{idPeranResponden:VARCHAR}, #{namaPeranResponden:VARCHAR})")
	void insert(PeranResponden peranResponden);

	@Update("UPDATE survey_peran_responden SET id_peran_responden=#{idPeranResponden:VARCHAR}, nama_peran_responden=#{namaPeranResponden:VARCHAR} WHERE id_peran_responden=#{idPeranResponden}")
	void update(PeranResponden peranResponden);

	@Delete("DELETE FROM survey_peran_responden WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM survey_peran_responden WHERE id_peran_responden=#{idPeranResponden}")
	void delete(PeranResponden peranResponden);

	@Delete("INSERT INTO survey_peran_responden SELECT * FROM survey_peran_responden_test")
	void copyTest();

	List<PeranResponden> getList(QueryParameter param);

	PeranResponden getEntity(String id);

	long getCount(QueryParameter param);

	int getNewId();

	/**********************************
	 * - End Generate -
	 ************************************/

}
