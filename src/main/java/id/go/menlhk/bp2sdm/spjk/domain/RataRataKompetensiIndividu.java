package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RataRataKompetensiIndividu {

	public static final String ID = "id_rata_rata_kompetensi_individu";
	public static final String ID_USER_RESPONDEN = "id_user_responden_rata_rata_kompetensi_individu";
	public static final String NIP = "nip_rata_rata_kompetensi_individu";
	public static final String NAMA = "nama_rata_rata_kompetensi_individu";
	public static final String GOLONGAN = "golongan_rata_rata_kompetensi_individu";
	public static final String ID_ESELON1 = "id_eselon1_rata_rata_kompetensi_individu";
	public static final String ID_UNIT_AUDITI = "id_unit_auditi_rata_rata_kompetensi_individu";
	public static final String ID_BIDANG_TEKNIS = "id_bidang_teknis_rata_rata_kompetensi_individu";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_rata_rata_kompetensi_individu";
	public static final String ID_TINGKAT_JABATAN = "id_tingkat_jabatan_rata_rata_kompetensi_individu";
	public static final String ID_JENJANG_JABATAN = "id_jenjang_jabatan_rata_rata_kompetensi_individu";
	public static final String NILAI_KOMPETENSI_MANAJERIAL = "nilai_kompetensi_manajerial_rata_rata_kompetensi_individu";
	public static final String NILAI_KOMPETENSI_SOSIOKULTURAL = "nilai_kompetensi_sosiokultural_rata_rata_kompetensi_individu";
	public static final String NILAI_KOMPETENSI_TEKNIS = "nilai_kompetensi_teknis_rata_rata_kompetensi_individu";
	public static final String KODE_SESI = "kode_sesi_rata_rata_kompetensi_individu";
	public static final String STATUS_RESPONDEN = "status_responden_rata_rata_kompetensi_individu";

	private String id;
	private String idUserResponden;
	private String nip;
	private String nama;
	private String golongan;
	private String idEselon1;
	private String idUnitAuditi;
	private String idBidangTeknis;
	private String idKelompokJabatan;
	private String idTingkatJabatan;
	private String idJenjangJabatan;
	private String nilaiKompetensiManajerial;
	private String nilaiKompetensiSosiokultural;
	private String nilaiKompetensiTeknis;
	private String kodeSesi;
	private String statusResponden;

	public RataRataKompetensiIndividu() {

	}

	public RataRataKompetensiIndividu(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_user_responden")
	public String getIdUserResponden() {
		return idUserResponden;
	}

	public void setIdUserResponden(String idUserResponden) {
		this.idUserResponden = idUserResponden;
	}

	@JsonProperty("nip")
	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("golongan")
	public String getGolongan() {
		return golongan;
	}

	public void setGolongan(String golongan) {
		this.golongan = golongan;
	}

	@JsonProperty("id_eselon1")
	public String getIdEselon1() {
		return idEselon1;
	}

	public void setIdEselon1(String idEselon1) {
		this.idEselon1 = idEselon1;
	}

	@JsonProperty("id_unit_auditi")
	public String getIdUnitAuditi() {
		return idUnitAuditi;
	}

	public void setIdUnitAuditi(String idUnitAuditi) {
		this.idUnitAuditi = idUnitAuditi;
	}
	
	@JsonProperty("id_bidang_teknis")
	public String getIdBidangTeknis() {
		return idBidangTeknis;
	}

	public void setIdBidangTeknis(String idBidangTeknis) {
		this.idBidangTeknis = idBidangTeknis;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	@JsonProperty("id_tingkat_jabatan")
	public String getIdTingkatJabatan() {
		return idTingkatJabatan;
	}

	public void setIdTingkatJabatan(String idTingkatJabatan) {
		this.idTingkatJabatan = idTingkatJabatan;
	}

	@JsonProperty("id_jenjang_jabatan")
	public String getIdJenjangJabatan() {
		return idJenjangJabatan;
	}

	public void setIdJenjangJabatan(String idJenjangJabatan) {
		this.idJenjangJabatan = idJenjangJabatan;
	}

	@JsonProperty("nilai_kompetensi_manajerial")
	public String getNilaiKompetensiManajerial() {
		return nilaiKompetensiManajerial;
	}

	public void setNilaiKompetensiManajerial(String nilaiKompetensiManajerial) {
		this.nilaiKompetensiManajerial = nilaiKompetensiManajerial;
	}

	@JsonProperty("nilai_kompetensi_sosiokultural")
	public String getNilaiKompetensiSosiokultural() {
		return nilaiKompetensiSosiokultural;
	}

	public void setNilaiKompetensiSosiokultural(String nilaiKompetensiSosiokultural) {
		this.nilaiKompetensiSosiokultural = nilaiKompetensiSosiokultural;
	}

	@JsonProperty("nilai_kompetensi_teknis")
	public String getNilaiKompetensiTeknis() {
		return nilaiKompetensiTeknis;
	}

	public void setNilaiKompetensiTeknis(String nilaiKompetensiTeknis) {
		this.nilaiKompetensiTeknis = nilaiKompetensiTeknis;
	}

	@JsonProperty("kode_sesi")
	public String getKodeSesi() {
		return kodeSesi;
	}

	public void setKodeSesi(String kodeSesi) {
		this.kodeSesi = kodeSesi;
	}

	@JsonProperty("status_responden")
	public String getStatusResponden() {
		return statusResponden;
	}

	public void setStatusResponden(String statusResponden) {
		this.statusResponden = statusResponden;
	}



	/**********************************************************************/

	private String nilaiAltKompetensiManajerial;
	private String nilaiAltKompetensiSosiokultural;
	private String nilaiAltKompetensiTeknis;
	
	private String namaKelompokJabatan;
	private String namaTingkatJabatan;
	private String namaJenjangJabatan;
	private String kodeEselon1;
	private String namaPropinsi;
	private String namaEselon1;
	private String namaUnitAuditi;
	private String namaBidangTeknis;
	private String mobileAccount;
	private List<DetilIndividu> detilIndividu;
	
	
	@JsonProperty("nilai_alt_kompetensi_manajerial")
	public String getNilaiAltKompetensiManajerial() {
		return nilaiAltKompetensiManajerial;
	}

	public void setNilaiAltKompetensiManajerial(String nilaiAltKompetensiManajerial) {
		this.nilaiAltKompetensiManajerial = nilaiAltKompetensiManajerial;
	}

	@JsonProperty("nilai_alt_kompetensi_sosiokultural")
	public String getNilaiAltKompetensiSosiokultural() {
		return nilaiAltKompetensiSosiokultural;
	}

	public void setNilaiAltKompetensiSosiokultural(String nilaiAltKompetensiSosiokultural) {
		this.nilaiAltKompetensiSosiokultural = nilaiAltKompetensiSosiokultural;
	}

	@JsonProperty("nilai_alt_kompetensi_teknis")
	public String getNilaiAltKompetensiTeknis() {
		return nilaiAltKompetensiTeknis;
	}

	public void setNilaiAltKompetensiTeknis(String nilaiAltKompetensiTeknis) {
		this.nilaiAltKompetensiTeknis = nilaiAltKompetensiTeknis;
	}

	public String getNamaKelompokJabatan() {
		return namaKelompokJabatan;
	}

	public void setNamaKelompokJabatan(String namaKelompokJabatan) {
		this.namaKelompokJabatan = namaKelompokJabatan;
	}


	public String getNamaTingkatJabatan() {
		return namaTingkatJabatan;
	}

	public void setNamaTingkatJabatan(String namaTingkatJabatan) {
		this.namaTingkatJabatan = namaTingkatJabatan;
	}

	public String getNamaJenjangJabatan() {
		return namaJenjangJabatan;
	}

	public void setNamaJenjangJabatan(String namaJenjangJabatan) {
		this.namaJenjangJabatan = namaJenjangJabatan;
	}
	
	

	public String getKodeEselon1() {
		return kodeEselon1;
	}

	public void setKodeEselon1(String kodeEselon1) {
		this.kodeEselon1 = kodeEselon1;
	}

	public String getNamaPropinsi() {
		return namaPropinsi;
	}

	public void setNamaPropinsi(String namaPropinsi) {
		this.namaPropinsi = namaPropinsi;
	}

	public String getNamaEselon1() {
		return namaEselon1;
	}

	public void setNamaEselon1(String namaEselon1) {
		this.namaEselon1 = namaEselon1;
	}

	public String getNamaUnitAuditi() {
		return namaUnitAuditi;
	}

	public void setNamaUnitAuditi(String namaUnitAuditi) {
		this.namaUnitAuditi = namaUnitAuditi;
	}
	
	public String getNamaBidangTeknis() {
		return namaBidangTeknis;
	}

	public void setNamaBidangTeknis(String namaBidangTeknis) {
		this.namaBidangTeknis = namaBidangTeknis;
	}

	public String getMobileAccount() {
		return mobileAccount;
	}

	public void setMobileAccount(String mobileAccount) {
		this.mobileAccount = mobileAccount;
	}

	public List<DetilIndividu> getDetilIndividu() {
		return detilIndividu;
	}

	public void setDetilIndividu(List<DetilIndividu> detilIndividu) {
		this.detilIndividu = detilIndividu;
	}
	
	
}
