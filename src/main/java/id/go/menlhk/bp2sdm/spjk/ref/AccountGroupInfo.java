package id.go.menlhk.bp2sdm.spjk.ref;

public enum AccountGroupInfo {
	//makesure this id same like in database hijr_account_group
	ADMINISTRATOR("1543544802020"), DEVELOPER("999999999"), RESPONDEN("2002200"),
	SATKER("2002300"), ESELON1("2002400"), PROPINSI("2002500"), ORGANISASI("2002600"), UNIT_PEMBINA("2002800")
	;

	private String id;

	private AccountGroupInfo(final String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}

}
