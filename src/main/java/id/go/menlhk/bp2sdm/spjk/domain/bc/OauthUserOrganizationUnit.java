package id.go.menlhk.bp2sdm.spjk.domain.bc;

public class OauthUserOrganizationUnit extends OauthUserOrganization {

	public static final String UNIT_KERJA_ID = "unit_kerja_id";
	public static final String UNIT_KERJA_NAME = "unit_kerja_name";

	private String unitKerjaId;
	private String unitKerjaName;

	public OauthUserOrganizationUnit() {

	}

	public OauthUserOrganizationUnit(String id) {
		super(id);
	}

	public String getUnitKerjaId() {
		return unitKerjaId;
	}

	public void setUnitKerjaId(String unitKerjaId) {
		this.unitKerjaId = unitKerjaId;
	}

	public String getUnitKerjaName() {
		return unitKerjaName;
	}

	public void setUnitKerjaName(String unitKerjaName) {
		this.unitKerjaName = unitKerjaName;
	}

}
