package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DashboardProgress {
	public static final String id_eselon1 = "id_eselon1_dashboard_progress";
	public static final String id_unit_auditi = "id_unit_auditi_dashboard_progress";
	public static final String id_propinsi = "id_propinsi_dashboard_progress";
	public static final String id_kelompok_jabatan = "id_kelompok_jabatan_dashboard_progress";
	public static final String id_tingkat_jabatan = "id_tingkat_jabatan_dashboard_progress";
	public static final String id_jenjang_jabatan = "id_jenjang_jabatan_dashboard_progress";
	public static final String status = "status_dashboard_progress";
	public static final String kode_sesi = "kode_sesi_dashboard_progress";

	private String idKelompokJabatan;
	private String kodeKelompokJabatan;
	private String namaKelompokJabatan;
	private Integer totalProgress;
	private Integer totalExisting;

	//@JsonProperty("id")
	public DashboardProgress() {
	
	}
	
	public DashboardProgress(String id) {
		
	}

	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	public String getKodeKelompokJabatan() {
		return kodeKelompokJabatan;
	}

	public void setKodeKelompokJabatan(String kodeKelompokJabatan) {
		this.kodeKelompokJabatan = kodeKelompokJabatan;
	}

	public String getNamaKelompokJabatan() {
		return namaKelompokJabatan;
	}

	public void setNamaKelompokJabatan(String namaKelompokJabatan) {
		this.namaKelompokJabatan = namaKelompokJabatan;
	}

	public Integer getTotalProgress() {
		return totalProgress;
	}

	public void setTotalProgress(Integer totalProgress) {
		this.totalProgress = totalProgress;
	}

	public Integer getTotalExisting() {
		return totalExisting;
	}

	public void setTotalExisting(Integer totalExisting) {
		this.totalExisting = totalExisting;
	}
	
}