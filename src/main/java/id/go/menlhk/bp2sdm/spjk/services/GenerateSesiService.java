package id.go.menlhk.bp2sdm.spjk.services;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import id.go.menlhk.bp2sdm.spjk.OrganizationReference;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Sesi;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUser;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOrganization;
import id.go.menlhk.bp2sdm.spjk.mapper.NilaiRataRataMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;

@Service
@Component
public class GenerateSesiService {


	@Autowired
	private NilaiRataRataMapper nilaiRataRataMapper;
	
	@Autowired
	private SesiMapper sesiMapper;
	
	//@Value("${app.organization.id}")
	private String organizationId;

	//@Value("${app.organization.code}")
	private String organizationCode;

	@Value("${app.folder}")
	private String appFolder;

	@Value("${app.url.ntfc}")
	private String appUrlNtfc;

	@Value("${email.setting}")
	private String emailSetting;

	private SimpleDateFormat sda = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	static String capitailizeFirstWord(String str) {
		// str = str.toLowerCase();
		StringBuffer s = new StringBuffer();
		char ch = ' ';
		for (int i = 0; i < str.length(); i++) {
			if (ch == ' ' && str.charAt(i) != ' ')
				s.append(Character.toUpperCase(str.charAt(i)));
			else
				s.append(str.charAt(i));
			ch = str.charAt(i);
		}
		return s.toString().trim();
	}

	@Transactional
	@Async
	public void doGenerate(Optional<String> idSesi, String oldSumber)
			throws Exception {
		QueryParameter param = new QueryParameter();
		if(idSesi.isPresent() && !idSesi.get().equals("")) {
    		param.setClause(param.getClause() + " AND " + Sesi.KODE + " = '"+idSesi.get()+"'");
    	}
		List<Sesi> listsesi = sesiMapper.getList(param);
		
		try {
			//delete
			nilaiRataRataMapper.deleteIndividuExistingBatch(idSesi.get());
			nilaiRataRataMapper.deleteIndividuBatch(idSesi.get());
			nilaiRataRataMapper.deleteTeknisBatch(idSesi.get());
			nilaiRataRataMapper.deleteManajerialBatch(idSesi.get());
			nilaiRataRataMapper.deleteSosiokulturalBatch(idSesi.get());
			nilaiRataRataMapper.deletePesertaUjianBatch(idSesi.get());
			nilaiRataRataMapper.deleteDetilPesertaUjianBatch(idSesi.get());
			nilaiRataRataMapper.deleteIndividuDetilBatch(idSesi.get());
			//new 2021
			nilaiRataRataMapper.deleteHistoryJabatanKompTeknisBatch(idSesi.get());
			nilaiRataRataMapper.deleteHistoryJabatanKompNonTeknisBatch(idSesi.get());
			nilaiRataRataMapper.deleteHistoryMetodePengemKelompokNilaiBatch(idSesi.get());
			nilaiRataRataMapper.deleteHistoryMetodePengemKompBatch(idSesi.get());
			nilaiRataRataMapper.deleteHistoryMetodePengemKompNonBatch(idSesi.get());
			
			//insert
			nilaiRataRataMapper.copyIndividuExisting(idSesi.get());
			nilaiRataRataMapper.copy(idSesi.get());
			//nilaiRataRataMapper.copyTeknis(idSesi.get());
			//nilaiRataRataMapper.copyManajerial(idSesi.get());
			//nilaiRataRataMapper.copySosiokultural(idSesi.get());
			//nilaiRataRataMapper.copyPesertaUjian(idSesi.get());
			nilaiRataRataMapper.copyDetilPesertaUjian(idSesi.get());
			nilaiRataRataMapper.copyIndividuTeknis(idSesi.get());
			nilaiRataRataMapper.copyIndividuManajerSosio(idSesi.get());
			//new 2021
			boolean pass = true;
			if(pass) {
				nilaiRataRataMapper.copyHistoryJabatanKompTeknis(idSesi.get());
				nilaiRataRataMapper.copyHistoryJabatanKompNonTeknis(idSesi.get());
				nilaiRataRataMapper.copyHistoryMetodePengemKelompokNilai(idSesi.get());
				nilaiRataRataMapper.copyHistoryMetodePengemKomp(idSesi.get());
				nilaiRataRataMapper.copyHistoryMetodePengemKompNon(idSesi.get());
			}
			
			Sesi data = listsesi.get(0);
			data.setSumber(oldSumber);
			data.setTimestampModified(new Date());
			sesiMapper.updateTimeSumber(data);
			System.out.println("WE DO GENERATE IN SERVICE");
		} catch (Exception e) {
	        // Log and handle exception
	        System.err.println("Error in doGenerate: " + e.getMessage());
	        Sesi data = listsesi.get(0);
			data.setSumber(oldSumber);
			sesiMapper.updateSumber(data);
	        throw e; // Optionally rethrow the exception
	    }
	}
}
