package id.go.menlhk.bp2sdm.spjk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.core.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.mapper.NavigationMapper;

@RestController
@RequestMapping("/nav")
public class NavigationController {

	@Autowired
	private NavigationMapper nagivationMapper;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getList() throws Exception {
		// Thread.sleep(1000);

		Object[] roles = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray();

		String strRoles = "";
		for (Object r : roles) {
			strRoles += ",'" + r + "'";
		}

		ResponseWrapper resp = new ResponseWrapper();
		resp.setData(nagivationMapper.getListByUserId(strRoles.substring(1)));
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.OK);
	}

}
