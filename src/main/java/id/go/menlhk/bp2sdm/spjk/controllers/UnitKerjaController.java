package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKerja;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitKerjaMapper;

@RestController
@RequestMapping("/unit_kerja")
public class UnitKerjaController {

	@Autowired
	private UnitKerjaMapper unitKerjaMapper;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseWrapperList getList(HttpServletRequest request,
			@RequestParam("filter_keyword") Optional<String> filter_keyword,
			@RequestParam("filter_kode") Optional<String> filter_kode,
			@RequestParam("filter_nama") Optional<String> filter_nama,
			@RequestParam("filter_alamat") Optional<String> filter_alamat,
			@RequestParam("filter_telepon") Optional<String> filter_telepon,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {
		// MessageResult resp = new MessageResult(Message.SUCCESS_CODE);

		QueryParameter param = new QueryParameter();
		// if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+
		// UnitKerja.NAMA_UNIT_KERJA + " LIKE '%"+filter_keyword.get()+"%'");
		if (filter_kode.isPresent())
			param.setClause(
					param.getClause() + " AND " + UnitKerja.KODE_UNIT_KERJA + " LIKE '%" + filter_kode.get() + "%'");
		if (filter_nama.isPresent())
			param.setClause(
					param.getClause() + " AND " + UnitKerja.NAMA_UNIT_KERJA + " LIKE '%" + filter_nama.get() + "%'");
		if (filter_alamat.isPresent())
			param.setClause(param.getClause() + " AND " + UnitKerja.ALAMAT_UNIT_KERJA + " LIKE '%" + filter_alamat.get()
					+ "%'");
		if (filter_telepon.isPresent())
			param.setClause(param.getClause() + " AND " + UnitKerja.TELEPON_UNIT_KERJA + " LIKE '%"
					+ filter_telepon.get() + "%'");
		if (limit.isPresent()) {
			param.setLimit(limit.get());
			if (limit.get() > 2000)
				param.setLimit(2000);
		} else {
			param.setLimit(2000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		ResponseWrapperList resp = new ResponseWrapperList();
		// param.setClause("1");
		List<UnitKerja> data = unitKerjaMapper.getList(param);
		resp.setCount(unitKerjaMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return resp;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(@PathVariable String id) throws Exception {
		ResponseWrapper resp = new id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper();
		UnitKerja data = unitKerjaMapper.getEntity(id);
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> save(@RequestParam("id") Optional<String> id,
			@RequestParam("kode") Optional<String> kode, @RequestParam("nama") Optional<String> nama,
			@RequestParam("alamat") Optional<String> alamat, @RequestParam("telepon") Optional<String> telepon)
			throws Exception {

		ResponseWrapper resp = new ResponseWrapper();

		UnitKerja data = new UnitKerja(String.valueOf(unitKerjaMapper.getNewId()));
		if (id.isPresent()) {
			data.setIdUnitKerja(id.get());
			data = unitKerjaMapper.getEntity(id.get());
		}
		if (data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		if (kode.isPresent())
			data.setKodeUnitKerja(kode.get());
		if (nama.isPresent())
			data.setNamaUnitKerja(nama.get());
		if (alamat.isPresent())
			data.setAlamatUnitKerja(alamat.get());
		if (telepon.isPresent())
			data.setTeleponUnitKerja(telepon.get());

		boolean pass = true;
		if (data.getNamaUnitKerja() == null) {
			pass = false;
		} else {
			if (data.getNamaUnitKerja().equals("")) {
				pass = false;
			}
		}

		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if (id.isPresent()) {
			unitKerjaMapper.update(data);
		} else {
			unitKerjaMapper.insert(data);
		}
		resp.setData(data);

		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> delete(@PathVariable String id) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		UnitKerja data = unitKerjaMapper.getEntity(id);
		resp.setData(data);
		unitKerjaMapper.delete(data);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

}
