package id.go.menlhk.bp2sdm.spjk.controllers;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.go.menlhk.bp2sdm.spjk.MainApplication;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanManajerial;
import id.go.menlhk.bp2sdm.spjk.domain.Sesi;
import id.go.menlhk.bp2sdm.spjk.domain.User;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.UserResponden;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.helper.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.JabatanManajerialMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.NilaiRataRataMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserRespondenMapper;
import id.go.menlhk.bp2sdm.spjk.services.GenerateSesiService;

@Controller
@RequestMapping("/sesi")
public class SesiRestController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	private SesiMapper sesiMapper;
	
	@Autowired
	private UserRespondenMapper userRespondenMapper;
	
	@Autowired
	private UserPertanyaanMapper userPertanyaanMapper;
	
	@Autowired
	private UserPertanyaanJawabanMapper userPertanyaanJawabanMapper;
	@Autowired
	private NilaiRataRataMapper nilaiRataRataMapper;

	@Autowired
	private GenerateSesiService generateSesiService;
	

    public void callSendEmailService(LinkedMultiValueMap<String, Object> payload, HttpServletRequest request) {
    	String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
    	
        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        
        String workingURL = request.getRequestURL().toString().replaceAll("sesi/copy-hasil-survey","");
        System.out.println(">>"+appState);
        if((appState.equals("production"))){
        	workingURL = workingURL.replaceAll("http", "https");
		}
        String urlApi = workingURL + "sesi/save";
        System.out.println("url: " + urlApi);
        //restTemplate.post

        // set headers
       // HttpHeaders headers = new HttpHeaders();
        ////headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + accessTokenValue);
		//set headers
		HttpEntity<String> requestHeader = new HttpEntity<String>(headers);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestData = new HttpEntity<LinkedMultiValueMap<String, Object>>(payload, headers);
        
        //ResponseEntity<String> response = restTemplate.postForEntity(urlApi, requestData, String.class);
        ResponseEntity<String> result = restTemplate.exchange(urlApi, HttpMethod.POST, requestData, String.class);
        
        System.out.println("hasil api: " + result);
        
        
	}
	
	@RequestMapping(value="/copy-hasil-survey", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> copyHasilSurvey(
    		@RequestParam("id_sesi") Optional<String> idSesi,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		ResponseWrapperList resp = new ResponseWrapperList();
		if(idSesi.isPresent() && !idSesi.get().equals("")) param.setClause(param.getClause() + " AND " + Sesi.KODE + " = '"+idSesi.get()+"'");
    	
		List<Sesi> data = sesiMapper.getList(param);
		String oldSumber = data.get(0).getSumber();
		if(oldSumber.equals("3")) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Permintaan generate sesi anda sedang diproses, mohon menunggu bebera saat, hingga status generate selesai atau tanggal Update berubah.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		/*LinkedMultiValueMap<String, Object> payload = new LinkedMultiValueMap<String, Object>();
		payload.add("id", data.get(0).getId());
		callSendEmailService(payload, request);*/
		
		try {
			data.get(0).setSumber("3"); //set when first time run
			sesiMapper.updateSumber(data.get(0));
			generateSesiService.doGenerate(idSesi, oldSumber);
			
			resp.setData(data);
			resp.setMessage("Proses copy sedang berjalan, mohon tunggu hingga tanggal update berubah");
		} catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage(e.getRootCause().getMessage());
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			//throw new AccessDeniedException("Access denied", e);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			resp.setCode(HttpStatus.BAD_GATEWAY.value());
			resp.setMessage(e.getMessage());
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			//throw new AccessDeniedException("Access denied", e);
		}
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value="/list-hasil-ujian", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getListHasilUjian(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		@RequestParam("filter_id") Optional<String> filterId,
    		@RequestParam("filter_kode") Optional<String> filterKode,
    		@RequestParam("filter_group") Optional<String> filterGroup,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
	    	/*if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Position.NAME + " LIKE '%"+filterKeyword.get()+"%')");
	    	}
	    	*/
		
	    	if(filterId.isPresent() && !filterId.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Sesi.ID + " = '"+filterId.get()+"')");
	    	}
	    	if(filterKode.isPresent() && !filterKode.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Sesi.KODE + " like '%"+filterKode.get()+"%')");
	    	}
	    	
	    	//param.setClause(param.getClause() + " AND " + Position.ORGANIZATION_ID + "='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
	    	
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}else {
	    		param.setLimit(1000000);
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
    	
    	param.setGroup("id_sesi");
//    	/id_header_user_pertanyaan_jawaban, order_user_pertanyaan_jawaban
    	//param.setOrder("id_header_user_pertanyaan_jawaban,order_user_pertanyaan_jawaban");
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Sesi> data = sesiMapper.getListHasilUjian(param);
		
//		int idx=0;
//		String status_temp="";
//		for(Sesi s : data) {
//			//s.setJumlahResponden(10);
//			LocalDate now = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//			LocalDate start = s.getPeriodeAwal().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//			LocalDate end = s.getPeriodeAkhir().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//			if(s.getExpiredSesi()<0) s.setSumber("DONE");
//			else s.setSumber("IN_COMMING");
//			for (LocalDate date = start; date.isBefore(end); date = date.plusDays(1)) {
//				System.out.println(idx);
//				if(date.equals(now)) {
//					//status_temp="SCHEDULED_NOW";
//					s.setSumber("SCHEDULED_NOW");
//				}
//			}
//			idx++;
//		}
		resp.setCount(sesiMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

    @RequestMapping(value="/reset-hasil-ujian", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> resetHasilUjian(
    		@RequestParam("id_sesi") Optional<String> idSesi,
    		@RequestParam("id_account") Optional<String> idAccount,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause()+" AND "+UserResponden.ID_ACCOUNT+" ='"+idAccount.get()+"'");
    		param.setClause(param.getClause()+" AND "+UserResponden.ID_SESI+" ='"+idSesi.get()+"'");
    		List<UserResponden> data = userRespondenMapper.getList(param);
    		
    		/*if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}*/
    		data.get(0).setStatus("PROSES");
    		userRespondenMapper.update(data.get(0));
    		param = new QueryParameter();
    		param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_ACCOUNT+" ='"+idAccount.get()+"'");
    		param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+data.get(0).getId()+"'");
    		List<UserPertanyaan> userPertanyaan = userPertanyaanMapper.getList(param);
    		for(UserPertanyaan up : userPertanyaan) {
    			up.setStatus(null);
    			userPertanyaanMapper.update(up);
    			for(UserPertanyaanJawaban upj : up.getUserPertanyaanJawaban()) {
    				upj.setIsChecked(null);
    				if(upj.getIsChecked()!=null) userPertanyaanJawabanMapper.update(upj);
        		}
    		}
    	
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
	@RequestMapping(value = "/user-responden/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> deleteHasilUjian(@PathVariable String id, HttpSession session) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		UserResponden data = userRespondenMapper.getEntity(id);
		data.setStatus(null);
		//userRespondenMapper.delete(data);
		userRespondenMapper.update(data);
		resp.setData(data);
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+id+"'");
		for(UserPertanyaan o : userPertanyaanMapper.getList(param)) {
			userPertanyaanMapper.delete(o);
			for(UserPertanyaanJawaban o2 : o.getUserPertanyaanJawaban())
			userPertanyaanJawabanMapper.delete(o2);
		}
		//session.removeAttribute("jenisKompetensi");
		//session.setAttribute("jenisKompetensi","INTI"); //default session
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_kode") Optional<String> filterKode,
    		@RequestParam("filter_tahun") Optional<String> filterTahun,
    		@RequestParam("filter_awal") Optional<String> filter_awal,
    		@RequestParam("filter_akhir") Optional<String> filter_akhir,
    		@RequestParam("filter_group") Optional<String> filterGroup,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
	    	if(filterKode.isPresent() && !filterKode.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Sesi.KODE + " like '%"+filterKode.get()+"%')");
	    	}
	    	
	    	if(filter_awal.isPresent() && !filter_akhir.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Sesi.PERIODE_AWAL + " >= '"+filter_awal.get()+"'");
	    	}else if(!filter_awal.isPresent() && filter_akhir.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Sesi.PERIODE_AKHIR + " <= '"+filter_akhir.get()+"'");
	    	}else if(filter_awal.isPresent() && filter_akhir.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Sesi.PERIODE_AWAL + " BETWEEN '"+filter_awal.get()+"' AND '"+filter_akhir.get()+"'");
	    		//param.setClause(param.getClause() + " AND " + Sesi.PERIODE_AKHIR + " BETWEEN '"+Utils.formatSqlDate(filter_awal.get())+"' AND '"+Utils.formatSqlDate(filter_akhir.get())+"'");
	    	}
	    	//param.setClause(param.getClause() + " AND " + Position.ORGANIZATION_ID + "='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
	    	
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Sesi> data = sesiMapper.getList(param);
		
		for(Sesi s : data) {
			//s.setJumlahResponden(10);
			LocalDate now = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate start = s.getPeriodeAwal().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate end = s.getPeriodeAkhir().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			if(s.getExpiredSesi()<0) s.setSumber("DONE");
			else s.setSumber("IN_COMMING");
			for (LocalDate date = start; date.isBefore(end); date = date.plusDays(1)) {
				if(date.equals(now)) s.setSumber("SCHEDULED_NOW");
			}
		}
		resp.setCount(sesiMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("kode") Optional<String> kode,
    		@RequestParam("tahun") Optional<Integer> tahun,
    		@RequestParam("periode_awal")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> periodeAwal,
    		@RequestParam("periode_akhir")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> periodeAkhir,
    		@RequestParam("keterangan") Optional<String> keterangan,
    		@RequestParam("sumber") Optional<String> sumber,
    		@RequestParam("periode_aktif") Optional<String> periodeAktif,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Sesi data = new Sesi(Utils.getLongNumberID());
    		if(id.isPresent() && !id.get().equals("")) {
    			data.setId(id.get());
    			data = sesiMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		//
			if(kode.isPresent()) data.setKode(kode.get());
			if(tahun.isPresent()) data.setTahun(tahun.get());
			if(periodeAwal.isPresent()) data.setPeriodeAwal(periodeAwal.get());
			if(periodeAkhir.isPresent()) data.setPeriodeAkhir(periodeAkhir.get());
			if(keterangan.isPresent()) data.setKeterangan(keterangan.get());
			//data.setJumlahResponden(jumlahResponden.get());
			if(sumber.isPresent()) data.setSumber(sumber.get());
			
    		boolean pass = true;
    		if(data.getKode() == null || data.getTahun() == null ||  data.getPeriodeAwal() == null || data.getPeriodeAkhir() == null) {
    			pass = false;
    		}else {
    			if(data.getKode().equals("") || data.getKeterangan().equals("")) {
    				pass = false;
    			}
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		if(data.getPeriodeAwal().equals(data.getPeriodeAkhir())) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Periode terlalu singkat, mohon maaf tidak dapat dilanjutkan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		String whatProblem="";
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause()+" AND "+Sesi.KODE+" ='"+data.getKode()+"'");
    		List<Sesi> lst = sesiMapper.getList(param);
    		if(lst.size() > 0 && !data.getId().equals(lst.get(0).getId())) {
    			whatProblem="kode:"+data.getKode();
    		}else {
    			param = new QueryParameter();
    			param.setClause(param.getClause()+" AND "+Sesi.ID+" !='"+data.getId()+"'");
    			for(Sesi s : sesiMapper.getList(param)) {
    				LocalDate start = s.getPeriodeAwal().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        			LocalDate end = s.getPeriodeAkhir().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        			for (LocalDate date = start; date.isBefore(end); date = date.plusDays(1)) {
        				for (LocalDate date2 = data.getPeriodeAwal().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); date2.isBefore(data.getPeriodeAkhir().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()); date2 = date2.plusDays(1)) {
            				//System.out.println("looping all time: "+date);
        					//System.out.println(date+"=="+date2);
        					if(date.equals(date2)) {
             			    	whatProblem="periode: "+"<strong>"+date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))+"</strong>";
             			   }
             			}
        			}
    			}
    			
    		}
    		if(!whatProblem.equals("") && periodeAktif.isPresent()) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Sesi dengan "+whatProblem+" sudah ada");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		//if(idSource.isPresent()) data.setIdSource(idSource.get());
    		//if(idAccountAdded.isPresent()) data.setIdAccountAdded(idAccountAdded.get());
    		//if(timestampAdded.isPresent()) data.setTimestampAdded(timestampAdded.get());
    		//if(idAccountModified.isPresent()) data.setIdAccountModified(idAccountModified.get());
    		//if(timestampModified.isPresent()) data.setTimestampModified(timestampModified.get());
    		//data.setOrganizationId(extractAccountLogin(request, AccountLoginInfo.SOURCE));

    		
    		if(id.isPresent() && !id.get().equals("")) {
    			//data.setTimestampModified(new Date());
    			sesiMapper.update(data);
    		}else {
    			//data.setTimestampAdded(new Date());
    			sesiMapper.insert(data);	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

//    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
//	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<ResponseWrapper> doAction(
//			@PathVariable String id,
//			@PathVariable String action
//    		) throws Exception {
//		ResponseWrapper resp = new ResponseWrapper();
//		
//		if(action.equals(DELETE)) {
//			
//			Pegawai data = pegawaiMapper.getEntity(id);
//			resp.setData(data);
//			pegawaiMapper.delete(data);
//			
//			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
//		}
//		
//		
//		
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
    

}
