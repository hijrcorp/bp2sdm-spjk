package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaan;

public interface UserPertanyaanMapper {

	@Insert("INSERT INTO spjk_user_pertanyaan (id_user_pertanyaan, id_responden_user_pertanyaan, no_user_pertanyaan, id_account_user_pertanyaan, kode_user_pertanyaan, id_unit_kompetensi_user_pertanyaan, isi_user_pertanyaan, keterangan_user_pertanyaan, bobot_user_pertanyaan, kunci_jawaban_user_pertanyaan, tipe_soal_user_pertanyaan, durasi_user_pertanyaan, durasi_berjalan_user_pertanyaan, status_user_pertanyaan, jenis_kompetensi_user_pertanyaan, file_user_pertanyaan) VALUES (#{id:VARCHAR}, #{idResponden:VARCHAR}, #{no:NUMERIC}, #{idAccount:VARCHAR}, #{kode:VARCHAR}, #{idUnitKompetensi:VARCHAR}, #{isi:VARCHAR}, #{keterangan:VARCHAR}, #{bobot:NUMERIC}, #{kunciJawaban}, #{tipeSoal:VARCHAR}, #{durasi:NUMERIC}, #{durasiBerjalan:NUMERIC}, #{status:VARCHAR}, #{jenisKompetensi:VARCHAR}, #{file:VARCHAR})")
	void insert(UserPertanyaan userPertanyaan);

	@Update("UPDATE spjk_user_pertanyaan SET id_user_pertanyaan=#{id:VARCHAR}, id_responden_user_pertanyaan=#{idResponden:VARCHAR}, no_user_pertanyaan=#{no:NUMERIC}, id_account_user_pertanyaan=#{idAccount:VARCHAR}, kode_user_pertanyaan=#{kode:VARCHAR}, id_unit_kompetensi_user_pertanyaan=#{idUnitKompetensi:VARCHAR}, isi_user_pertanyaan=#{isi:VARCHAR}, keterangan_user_pertanyaan=#{keterangan:VARCHAR}, bobot_user_pertanyaan=#{bobot:NUMERIC}, kunci_jawaban_user_pertanyaan=#{kunciJawaban}, tipe_soal_user_pertanyaan=#{tipeSoal:VARCHAR}, durasi_user_pertanyaan=#{durasi:NUMERIC}, durasi_berjalan_user_pertanyaan=#{durasiBerjalan:NUMERIC}, status_user_pertanyaan=#{status:VARCHAR}, jenis_kompetensi_user_pertanyaan=#{jenisKompetensi:VARCHAR} WHERE id_user_pertanyaan=#{id}")
	void update(UserPertanyaan userPertanyaan);

	@Delete("DELETE FROM spjk_user_pertanyaan WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_user_pertanyaan WHERE id_user_pertanyaan=#{id}")
	void delete(UserPertanyaan userPertanyaan);

	List<UserPertanyaan> getList(QueryParameter param);

	UserPertanyaan getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	//

	List<UserPertanyaan> getListNonTeknis(QueryParameter param);

	UserPertanyaan getEntityNormal(String id);
	

	@Update("UPDATE spjk_user_pertanyaan SET id_user_pertanyaan=#{id:VARCHAR}, id_responden_user_pertanyaan=#{idResponden:VARCHAR}, no_user_pertanyaan=#{no:NUMERIC}, id_account_user_pertanyaan=#{idAccount:VARCHAR}, kode_user_pertanyaan=#{kode:VARCHAR}, id_unit_kompetensi_user_pertanyaan=#{idUnitKompetensi:VARCHAR}, isi_user_pertanyaan=#{isi:VARCHAR}, keterangan_user_pertanyaan=#{keterangan:VARCHAR}, bobot_user_pertanyaan=#{bobot:NUMERIC}, kunci_jawaban_user_pertanyaan=#{kunciJawaban}, tipe_soal_user_pertanyaan=#{tipeSoal:VARCHAR}, status_user_pertanyaan=#{status:VARCHAR}, jenis_kompetensi_user_pertanyaan=#{jenisKompetensi:VARCHAR} WHERE id_user_pertanyaan=#{id}")
	void updateStatus(UserPertanyaan userPertanyaan);
	

	@Update("UPDATE spjk_user_pertanyaan SET id_user_pertanyaan=#{id:VARCHAR}, id_responden_user_pertanyaan=#{idResponden:VARCHAR}, no_user_pertanyaan=#{no:NUMERIC}, id_account_user_pertanyaan=#{idAccount:VARCHAR}, kode_user_pertanyaan=#{kode:VARCHAR}, id_unit_kompetensi_user_pertanyaan=#{idUnitKompetensi:VARCHAR}, isi_user_pertanyaan=#{isi:VARCHAR}, keterangan_user_pertanyaan=#{keterangan:VARCHAR}, bobot_user_pertanyaan=#{bobot:NUMERIC}, kunci_jawaban_user_pertanyaan=#{kunciJawaban}, tipe_soal_user_pertanyaan=#{tipeSoal:VARCHAR}, durasi_user_pertanyaan=#{durasi:NUMERIC}, durasi_berjalan_user_pertanyaan=#{durasiBerjalan:NUMERIC}, jenis_kompetensi_user_pertanyaan=#{jenisKompetensi:VARCHAR} WHERE id_user_pertanyaan=#{id}")
	void updateDurasi(UserPertanyaan userPertanyaan);
	
	@Update("UPDATE spjk_user_pertanyaan SET durasi_berjalan_user_pertanyaan=#{durasiBerjalan:NUMERIC} WHERE id_user_pertanyaan=#{id}")
	void updateDurasiOnly(UserPertanyaan userPertanyaan);
	
}
