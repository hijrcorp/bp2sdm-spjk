package id.go.menlhk.bp2sdm.spjk.domain;

import java.io.Serializable;

import id.go.menlhk.bp2sdm.spjk.core.Metadata;

public class StatusAuditi extends Metadata implements Serializable {
	private String kodeStatusAuditi;
	private String namaStatusAuditi;

	public StatusAuditi() {
		super();
	}

	public StatusAuditi(int id) {
		super(id);
	}

	public String getKodeStatusAuditi() {
		return kodeStatusAuditi;
	}

	public void setKodeStatusAuditi(String kodeStatusAuditi) {
		this.kodeStatusAuditi = kodeStatusAuditi;
	}

	public String getNamaStatusAuditi() {
		return namaStatusAuditi;
	}

	public void setNamaStatusAuditi(String namaStatusAuditi) {
		this.namaStatusAuditi = namaStatusAuditi;
	}

}
