package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.bc.Pernyataan;

public interface PernyataanMapper {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	@Insert("INSERT INTO survey_pernyataan (id_pernyataan, kode_pernyataan, aspek_id_pernyataan, isi_pernyataan, keterangan_pernyataan) VALUES (#{idPernyataan:VARCHAR}, #{kodePernyataan:VARCHAR}, #{aspekIdPernyataan:VARCHAR}, #{isiPernyataan:VARCHAR}, #{keteranganPernyataan:VARCHAR})")
	void insert(Pernyataan pernyataan);

	@Update("UPDATE survey_pernyataan SET id_pernyataan=#{idPernyataan:VARCHAR}, kode_pernyataan=#{kodePernyataan:VARCHAR}, aspek_id_pernyataan=#{aspekIdPernyataan:VARCHAR}, isi_pernyataan=#{isiPernyataan:VARCHAR}, keterangan_pernyataan=#{keteranganPernyataan:VARCHAR} WHERE id_pernyataan=#{idPernyataan}")
	void update(Pernyataan pernyataan);

	@Delete("DELETE FROM survey_pernyataan WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM survey_pernyataan WHERE id_pernyataan=#{idPernyataan}")
	void delete(Pernyataan pernyataan);

	@Delete("INSERT INTO survey_pernyataan SELECT * FROM survey_pernyataan_test")
	void copyTest();

	@Insert("INSERT INTO survey_sesi_pernyataan (id_sesi_pernyataan, sesi_id_sesi_pernyataan, kode_sesi_pernyataan, aspek_id_sesi_pernyataan, isi_sesi_pernyataan, keterangan_sesi_pernyataan) VALUES (#{idPernyataan:VARCHAR}, #{idSesi}, #{kodePernyataan:VARCHAR}, #{aspekIdPernyataan:VARCHAR}, #{isiPernyataan:VARCHAR}, #{keteranganPernyataan:VARCHAR})")
	// @Insert("INSERT INTO survey_pernyataan (id_pernyataan,
	// sesi_id_sesi_pernyataan, kode_pernyataan, aspek_id_pernyataan,
	// isi_pernyataan, keterangan_pernyataan) VALUES (#{idPernyataan:VARCHAR},
	// #{idSesi}, #{kodePernyataan:VARCHAR}, #{aspekIdPernyataan:VARCHAR},
	// #{isiPernyataan:VARCHAR}, #{keteranganPernyataan:VARCHAR})")
	void copyToSesiPernyataan(Pernyataan pernyataan, @Param("idSesi") String idSesi);

	List<Pernyataan> getList(QueryParameter param);

	Pernyataan getEntity(String id);

	long getCount(QueryParameter param);

	int getNewId();

	/**********************************
	 * - End Generate -
	 ************************************/

}
