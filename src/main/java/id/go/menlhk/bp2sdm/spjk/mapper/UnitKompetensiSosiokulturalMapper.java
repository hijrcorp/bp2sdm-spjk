package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiSosiokultural;

@Mapper
public interface UnitKompetensiSosiokulturalMapper {
	@Insert("INSERT INTO spjk_unit_kompetensi_sosiokultural (id_unit_kompetensi_sosiokultural, id_kelompok_sosiokultural_unit_kompetensi_sosiokultural, kode_unit_kompetensi_sosiokultural, judul_unit_kompetensi_sosiokultural, id_source_unit_kompetensi_sosiokultural, id_account_added_unit_kompetensi_sosiokultural, timestamp_added_unit_kompetensi_sosiokultural, id_account_modified_unit_kompetensi_sosiokultural, timestamp_modified_unit_kompetensi_sosiokultural) VALUES (#{id:VARCHAR}, #{idKelompokSosiokultural:VARCHAR}, #{kode:VARCHAR}, #{judul:VARCHAR}, #{idSource:VARCHAR}, #{idAccountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{idAccountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(UnitKompetensiSosiokultural unitKompetensiSosiokultural);

	@Update("UPDATE spjk_unit_kompetensi_sosiokultural SET id_unit_kompetensi_sosiokultural=#{id:VARCHAR}, id_kelompok_sosiokultural_unit_kompetensi_sosiokultural=#{idKelompokSosiokultural:VARCHAR}, kode_unit_kompetensi_sosiokultural=#{kode:VARCHAR}, judul_unit_kompetensi_sosiokultural=#{judul:VARCHAR}, id_source_unit_kompetensi_sosiokultural=#{idSource:VARCHAR}, id_account_added_unit_kompetensi_sosiokultural=#{idAccountAdded:VARCHAR}, timestamp_added_unit_kompetensi_sosiokultural=#{timestampAdded:TIMESTAMP}, id_account_modified_unit_kompetensi_sosiokultural=#{idAccountModified:VARCHAR}, timestamp_modified_unit_kompetensi_sosiokultural=#{timestampModified:TIMESTAMP} WHERE id_unit_kompetensi_sosiokultural=#{id}")
	void update(UnitKompetensiSosiokultural unitKompetensiSosiokultural);

	@Delete("DELETE FROM spjk_unit_kompetensi_sosiokultural WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_unit_kompetensi_sosiokultural WHERE id_unit_kompetensi_sosiokultural=#{id}")
	void delete(UnitKompetensiSosiokultural unitKompetensiSosiokultural);

	List<UnitKompetensiSosiokultural> getList(QueryParameter param);

	UnitKompetensiSosiokultural getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}