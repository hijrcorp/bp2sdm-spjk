package id.go.menlhk.bp2sdm.spjk.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

public class CobaPdfElement extends PdfPageEventHelper {

	protected PdfReader reader;
	protected Rectangle pageSize;
	protected Rectangle tableC;
	protected float mLeft, mRight, mTop, mBottom;
	protected Rectangle footer;
	protected Font font;
	protected Rectangle lblTahun;

	// initialized upon opening the document
	protected PdfTemplate background;
	protected PdfTemplate total;

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		CobaPdfElement pdf = new CobaPdfElement();

		Document document = new Document(pdf.getPageSize(), pdf.getmLeft(), pdf.getmRight(), pdf.getmTop(),
				pdf.getmBottom());

		File out = File.createTempFile("slip-result", ".pdf");
		System.out.println(out.getAbsolutePath());
		PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(out));

		pdfWriter.setPageEvent(pdf);

		pdfWriter.close();

		document.open();
		ElementList elementsAA = CobaPdfElement.parseHtml("/Users/iman/git/lha/files/contoh.html",
				"/Users/iman/git/lha/files/style-aa.css", Tags.getHtmlTagProcessorFactory());
		for (Element e : elementsAA) {
			document.add(e);
		}
		document.close();

	}

	public CobaPdfElement() throws Exception {
		// TODO Auto-generated constructor stub
		// reader = new PdfReader("/Users/iman/git/lha/files/slip.pdf");
		reader = new PdfReader("C:/Users/Iman/git/lha/files/slip.pdf");

		AcroFields fields = reader.getAcroFields();

		pageSize = reader.getPageSize(1);
		lblTahun = fields.getFieldPositions("lblTahun").get(0).position;
		tableC = fields.getFieldPositions("tableC").get(0).position;
		mLeft = tableC.getLeft() - pageSize.getLeft();
		mRight = pageSize.getRight() - tableC.getRight();
		mTop = pageSize.getTop() - tableC.getTop();
		mBottom = tableC.getBottom() - pageSize.getBottom();

		font = new Font(BaseFont.createFont(), 11);
	}

	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		background = writer.getImportedPage(reader, 1);
		total = writer.getDirectContent().createTemplate(30, 15);
	}

	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {
		// we only know the total number of pages at the moment the document is closed.
		String s = "/" + (writer.getPageNumber() - 1);
		Phrase p = new Phrase(12, s, font);
		ColumnText.showTextAligned(total, Element.ALIGN_LEFT, p, 0.5f, 0, 0);
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		// TODO Auto-generated method stub
		PdfContentByte canvas = writer.getDirectContentUnder();
		canvas.addTemplate(background, 0, 0);
		try {
			ColumnText ct = new ColumnText(canvas);
			ct.setSimpleColumn(lblTahun);
			ct.addElement(new Paragraph("2018", font));
			ct.go();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static ElementList parseHtml(String content, String style, TagProcessorFactory tagProcessors)
			throws IOException {
		// CSS
		CSSResolver cssResolver = new StyleAttrCSSResolver();
		CssFile cssFile = XMLWorkerHelper.getCSS(new FileInputStream(style));
		cssResolver.addCss(cssFile);
		// HTML
		HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
		htmlContext.setTagFactory(tagProcessors);
		htmlContext.autoBookmark(false);
		// Pipelines
		ElementList elements = new ElementList();
		ElementHandlerPipeline end = new ElementHandlerPipeline(elements, null);
		HtmlPipeline html = new HtmlPipeline(htmlContext, end);
		CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
		// XML Worker
		XMLWorker worker = new XMLWorker(css, true);
		XMLParser p = new XMLParser(worker);

		p.parse(new FileInputStream(content), Charset.forName("cp1252"));
		return elements;
	}

	public Rectangle getPageSize() {
		return pageSize;
	}

	public float getmLeft() {
		return mLeft;
	}

	public float getmRight() {
		return mRight;
	}

	public float getmTop() {
		return mTop;
	}

	public float getmBottom() {
		return mBottom;
	}

}
