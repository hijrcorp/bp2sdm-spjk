package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.go.menlhk.bp2sdm.spjk.model.Account;

public class MappingJabatanUnitAuditi {
	public static final String ID = "id_mapping_jabatan_unit_auditi";
	public static final String ID_JABATAN = "id_jabatan_mapping_jabatan_unit_auditi";
	public static final String ID_ACCOUNT = "id_account_mapping_jabatan_unit_auditi";
	public static final String ID_UNIT_AUDITI = "id_unit_auditi_mapping_jabatan_unit_auditi";
	public static final String ID_SESI = "id_sesi_mapping_jabatan_unit_auditi";
	public static final String GOLONGAN = "golongan_mapping_jabatan_unit_auditi";

	private String id;
	private String idJabatan;
	private String idAccount;
	private String idUnitAuditi;
	private String idSesi;
	private String golongan;

	public MappingJabatanUnitAuditi() {

	}

	public MappingJabatanUnitAuditi(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_jabatan")
	public String getIdJabatan() {
		return idJabatan;
	}

	public void setIdJabatan(String idJabatan) {
		this.idJabatan = idJabatan;
	}

	@JsonProperty("id_account")
	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	@JsonProperty("id_unit_auditi")
	public String getIdUnitAuditi() {
		return idUnitAuditi;
	}

	public void setIdUnitAuditi(String idUnitAuditi) {
		this.idUnitAuditi = idUnitAuditi;
	}

	@JsonProperty("id_sesi")
	public String getIdSesi() {
		return idSesi;
	}

	public void setIdSesi(String idSesi) {
		this.idSesi = idSesi;
	}

	@JsonProperty("golongan")
	public String getGolongan() {
		return golongan;
	}
	
	public void setGolongan(String golongan) {
		this.golongan = golongan;
	}

	/**********************************************************************/
	private Account account;
	private MasterJabatan masterJabatan;
	private Eselon1 eselon1;
	private UnitAuditi unitAuditi;

	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public MasterJabatan getMasterJabatan() {
		return masterJabatan;
	}

	public void setMasterJabatan(MasterJabatan masterJabatan) {
		this.masterJabatan = masterJabatan;
	}

	public Eselon1 getEselon1() {
		return eselon1;
	}

	public void setEselon1(Eselon1 eselon1) {
		this.eselon1 = eselon1;
	}

	public UnitAuditi getUnitAuditi() {
		return unitAuditi;
	}

	public void setUnitAuditi(UnitAuditi unitAuditi) {
		this.unitAuditi = unitAuditi;
	}
	
	
}
