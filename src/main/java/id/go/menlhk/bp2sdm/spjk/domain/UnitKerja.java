package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UnitKerja {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	public static final String ID_UNIT_KERJA = "id_unit_kerja";
	public static final String KODE_UNIT_KERJA = "kode_unit_kerja";
	public static final String NAMA_UNIT_KERJA = "nama_unit_kerja";
	public static final String ALAMAT_UNIT_KERJA = "alamat_unit_kerja";
	public static final String TELEPON_UNIT_KERJA = "telepon_unit_kerja";

	private String idUnitKerja;
	private String kodeUnitKerja;
	private String namaUnitKerja;
	private String alamatUnitKerja;
	private String teleponUnitKerja;

	public UnitKerja() {

	}

	public UnitKerja(String id) {
		this.idUnitKerja = id;
	}

	@JsonProperty(ID_UNIT_KERJA)
	public String getIdUnitKerja() {
		return idUnitKerja;
	}

	public void setIdUnitKerja(String idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}

	@JsonProperty(KODE_UNIT_KERJA)
	public String getKodeUnitKerja() {
		return kodeUnitKerja;
	}

	public void setKodeUnitKerja(String kodeUnitKerja) {
		this.kodeUnitKerja = kodeUnitKerja;
	}

	@JsonProperty(NAMA_UNIT_KERJA)
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}

	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}

	@JsonProperty(ALAMAT_UNIT_KERJA)
	public String getAlamatUnitKerja() {
		return alamatUnitKerja;
	}

	public void setAlamatUnitKerja(String alamatUnitKerja) {
		this.alamatUnitKerja = alamatUnitKerja;
	}

	@JsonProperty(TELEPON_UNIT_KERJA)
	public String getTeleponUnitKerja() {
		return teleponUnitKerja;
	}

	public void setTeleponUnitKerja(String teleponUnitKerja) {
		this.teleponUnitKerja = teleponUnitKerja;
	}

	/**********************************
	 * - End Generate -
	 ************************************/

}
