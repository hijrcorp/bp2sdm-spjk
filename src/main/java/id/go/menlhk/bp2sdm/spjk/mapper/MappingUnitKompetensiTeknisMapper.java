package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.MappingUnitKompetensiTeknis;

public interface MappingUnitKompetensiTeknisMapper {
	
	List<MappingUnitKompetensiTeknis> getList(QueryParameter param);

	MappingUnitKompetensiTeknis getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
