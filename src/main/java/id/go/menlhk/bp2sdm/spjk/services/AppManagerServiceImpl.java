/*package id.go.menlhk.bp2sdm.spjk.services;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.scripting.xmltags.VarDeclSqlNode;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import id.go.menlhk.bp2sdm.spjk.OrganizationReference;
import id.go.menlhk.bp2sdm.spjk.core.ApplicationUtils;
import id.go.menlhk.bp2sdm.spjk.core.BaseServiceImpl;
import id.go.menlhk.bp2sdm.spjk.core.Clause;
import id.go.menlhk.bp2sdm.spjk.core.CodeGenerator;
import id.go.menlhk.bp2sdm.spjk.core.HijrException;
import id.go.menlhk.bp2sdm.spjk.core.Pagination;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.core.StyleExcel;
import id.go.menlhk.bp2sdm.spjk.dao.PeranDao;
import id.go.menlhk.bp2sdm.spjk.dao.PositionDao;
import id.go.menlhk.bp2sdm.spjk.dao.RoleDao;
import id.go.menlhk.bp2sdm.spjk.dao.StatusAuditiDao;
import id.go.menlhk.bp2sdm.spjk.dao.UploadFileDao;
import id.go.menlhk.bp2sdm.spjk.domain.DPdf;
import id.go.menlhk.bp2sdm.spjk.domain.Eselon1;
import id.go.menlhk.bp2sdm.spjk.domain.Kegiatan;
import id.go.menlhk.bp2sdm.spjk.domain.Login;
import id.go.menlhk.bp2sdm.spjk.domain.OauthUser;
import id.go.menlhk.bp2sdm.spjk.domain.OauthUserOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.OauthUserOrganizationUnit;
import id.go.menlhk.bp2sdm.spjk.domain.Peran;
import id.go.menlhk.bp2sdm.spjk.domain.Propinsi;
import id.go.menlhk.bp2sdm.spjk.domain.StatusAuditi;
import id.go.menlhk.bp2sdm.spjk.domain.TreeNodes;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKerja;
import id.go.menlhk.bp2sdm.spjk.domain.UploadFile;
import id.go.menlhk.bp2sdm.spjk.domain.User;
import id.go.menlhk.bp2sdm.spjk.mapper.KegiatanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.LoginMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;

@Named
public class AppManagerServiceImpl extends BaseServiceImpl implements AppManagerService {
	@Value("${app.folder}")
	private String appFolder;
	@Autowired
	private KegiatanMapper kegiatanMapper;

	// public PropinsiDao getPropinsiDao() {
	// return propinsiDao;
	// }
	//
	// public void setPropinsiDao(PropinsiDao propinsiDao) {
	// this.propinsiDao = propinsiDao;
	// }

	

	// batas
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserMapper userMapper;

	private void copyRow(XSSFWorkbook workbook, XSSFSheet worksheet, int sourceRowNum, int destinationRowNum) {
		// Get the source / new row
		XSSFRow newRow = worksheet.getRow(destinationRowNum);
		XSSFRow sourceRow = worksheet.getRow(sourceRowNum);

		// If the row exist in destination, push down all rows by 1 else create
		// a new row
		if (newRow != null) {
			System.out.println("shifting");
			worksheet.shiftRows(destinationRowNum + 1, worksheet.getLastRowNum(), 1);
		} else {
			newRow = worksheet.createRow(destinationRowNum);
		}

		// Loop through source columns to add to new row
		for (int i = 0; i < 20; i++) {
			// Grab a copy of the old/new cell
			XSSFCell oldCell = sourceRow.getCell(i);
			XSSFCell newCell = newRow.createCell(i);

			// If the old cell is null jump to next cell
			if (oldCell == null) {
				newCell = null;
				continue;
			}

			// Copy style from old cell and apply to new cell
			XSSFCellStyle newCellStyle = workbook.createCellStyle();
			newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
			;
			newCell.setCellStyle(newCellStyle);

			// If there is a cell comment, copy
			if (oldCell.getCellComment() != null) {
				newCell.setCellComment(oldCell.getCellComment());
			}

			// If there is a cell hyperlink, copy
			if (oldCell.getHyperlink() != null) {
				newCell.setHyperlink(oldCell.getHyperlink());
			}

			// Set the cell data type
			newCell.setCellType(oldCell.getCellType());

			// Set the cell data value
			switch (oldCell.getCellType()) {
			case Cell.CELL_TYPE_BLANK:
				newCell.setCellValue(oldCell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				newCell.setCellValue(oldCell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_ERROR:
				newCell.setCellErrorValue(oldCell.getErrorCellValue());
				break;
			case Cell.CELL_TYPE_FORMULA:
				newCell.setCellFormula(oldCell.getCellFormula());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				newCell.setCellValue(oldCell.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_STRING:
				newCell.setCellValue(oldCell.getRichStringCellValue());
				break;
			}
		}

		// If there are are any merged regions in the source row, copy to new
		// row
		for (int i = 0; i < worksheet.getNumMergedRegions(); i++) {
			CellRangeAddress cellRangeAddress = worksheet.getMergedRegion(i);
			if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
				CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(),
						(newRow.getRowNum() + (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow())),
						cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn());
				worksheet.addMergedRegion(newCellRangeAddress);
			}
		}
	}

	private PdfPCell createCell(String text, int align, int fontStyle) {
		com.itextpdf.text.Font font = new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 11,
				fontStyle);

		PdfPCell cell = new PdfPCell(new Phrase(text, font));
		cell.setHorizontalAlignment(align);

		return cell;
	}

	*//**
	 * create a library of cell styles
	 *//*
	private Map<String, XSSFCellStyle> createStyles(XSSFWorkbook wb) {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		DataFormat df = wb.createDataFormat();

		XSSFCellStyle style;
		XSSFFont headerFont = wb.createFont();
		headerFont.setBold(true);
		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setFont(headerFont);
		styles.put("header", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setWrapText(true);
		styles.put("cell_normal", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setWrapText(true);
		styles.put("cell_normal_centered", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.RIGHT);
		style.setWrapText(true);
		styles.put("cell_normal_right", style);

		return styles;
	}

	private XSSFCellStyle createStyle(XSSFWorkbook wb) {
		XSSFCellStyle style;
		XSSFFont headerFont = wb.createFont();
		headerFont.setBold(true);
		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private XSSFCellStyle createBorderedStyle(XSSFWorkbook wb) {
		BorderStyle thin = BorderStyle.THIN;
		short black = IndexedColors.BLACK.getIndex();

		XSSFCellStyle style = wb.createCellStyle();
		style.setBorderRight(thin);
		style.setRightBorderColor(black);
		style.setBorderBottom(thin);
		style.setBottomBorderColor(black);
		style.setBorderLeft(thin);
		style.setLeftBorderColor(black);
		style.setBorderTop(thin);
		style.setTopBorderColor(black);
		return style;
	}

	*//**
	 * Checking if every row and cell in merging region exists, and create those
	 * which are not
	 * 
	 * @param sheet
	 *            in which check is performed
	 * @param region
	 *            to check
	 * @param cellStyle
	 *            cell style to apply for whole region
	 *//*
	private void cleanBeforeMergeOnValidCells(XSSFSheet sheet, CellRangeAddress region, XSSFCellStyle cellStyle) {
		for (int rowNum = region.getFirstRow(); rowNum <= region.getLastRow(); rowNum++) {
			XSSFRow row = sheet.getRow(rowNum);
			if (row == null) {
				sheet.createRow(rowNum);
			}
			for (int colNum = region.getFirstColumn(); colNum <= region.getLastColumn(); colNum++) {
				XSSFCell currentCell = row.getCell(colNum);
				if (currentCell == null) {
					currentCell = row.createCell(colNum);
				}

				currentCell.setCellStyle(cellStyle);

			}
		}

	}

	public FileInputStream downloadReportKelompokTemuan(String path, String namaFile) throws Exception {
		String folderCustom = "files/report/kelompok temuan";
		return new FileInputStream(new File(path + "/" + folderCustom + "/" + namaFile));
	}

	public FileInputStream downloadReportKelompokEselon1(String path, String namaFile) throws Exception {
		String folderCustom = "files/report/kelompok temuan";
		return new FileInputStream(new File(path + "/" + folderCustom + "/" + namaFile));
	}

	private String sqlDate(String strdate) {
		String arrdate[] = strdate.split("/");// .split('/');
		return "'" + arrdate[2] + "-" + arrdate[1] + "-" + arrdate[0] + "'";
	}

	private XSSFRow create_header_ku(XSSFRow header, StyleExcel styleExcel, String label, int key) {
		header.createCell(key).setCellStyle((CellStyle) styleExcel);
		header.getCell(key).setCellValue(label);
		return header;
	}

	private XSSFRow create_header_ku(XSSFRow header, CellStyle styleExcel, String label, int key) {
		header.createCell(key).setCellStyle(styleExcel);
		header.getCell(key).setCellValue(label);
		return header;
	}

	public FileInputStream downloadBantuan(String path, String namaFiles, String role) throws Exception {
		String folderCustom = "files/bantuan";
		String namaFile = "";
		System.out.println(role);

		System.out.println(namaFile);
		// System.out.println(role.equals("analis"));
		if (role.equals("admin")) {
			namaFile = "Petunjuk-For-Admin.pdf";
		}
		if (role.equals("analis")) {
			namaFile = "Petunjuk-For-Analis.pdf";
		}
		if (role.equals("tu")) {
			namaFile = "Petunjuk-For-Tu.pdf";
		}
		return new FileInputStream(new File(path + "/" + folderCustom + "/" + namaFile));
	}

	public XSSFCellStyle setStyle(XSSFWorkbook workbook, String type) {
		XSSFCellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		if (type == "body") {
			style.setFont(font);
			style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		} else {
			style.setFillForegroundColor(HSSFColor.BLACK.index);
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.WHITE.index);
			style.setFont(font);
			style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		}
		return style;
	}

	private XSSFCellStyle style(XSSFWorkbook workbook, String type, String align) {
		XSSFCellStyle styleSection = (XSSFCellStyle) workbook.createCellStyle();
		if (type.equals("header")) {
			Font font = workbook.createFont();
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			font.setFontHeightInPoints((short) 11);
			styleSection.setFont(font);
			styleSection.setFillForegroundColor(new XSSFColor(new Color(196, 215, 155)));
			styleSection.setFillPattern(CellStyle.SOLID_FOREGROUND);
			styleSection.setBorderBottom(XSSFCellStyle.BORDER_THIN);
			styleSection.setBorderTop(XSSFCellStyle.BORDER_THIN);
			styleSection.setBorderRight(XSSFCellStyle.BORDER_THIN);
			styleSection.setBorderLeft(XSSFCellStyle.BORDER_THIN);
			styleSection.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleSection.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		} else if (type.equals("fontHeader")) {
			Font font = workbook.createFont();
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			font.setFontHeightInPoints((short) 12);
			styleSection.setFont(font);
		}
		if (align.equals("center")) {
			styleSection.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleSection.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		}
		return styleSection;
	}

	private void setAllBoxCellBorder(CellRangeAddress range, Sheet sheet, Workbook workbook) {
		RegionUtil.setBorderRight(1, range, sheet, workbook);
		RegionUtil.setBorderLeft(1, range, sheet, workbook);
		RegionUtil.setBorderTop(1, range, sheet, workbook);
		RegionUtil.setBorderBottom(1, range, sheet, workbook);
	}

	private void setBoxCellBorder(CellRangeAddress cellAddress, XSSFSheet sheet, XSSFWorkbook workbook) {
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, cellAddress, sheet, workbook);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, cellAddress, sheet, workbook);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, cellAddress, sheet, workbook);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, cellAddress, sheet, workbook);
	}


	@Autowired
	private LoginMapper loginMapper;

	public Login findActiveLoginByAccessToken(String accessToken) {
		return loginMapper.findActiveLoginByAccessToken(accessToken);
	}

	public void renderKolom(int firstRow, int lastRow, String[] data, int idx_kol, int data_length, XSSFCellStyle style,
			Row header, CellRangeAddress range, Sheet sheet, Workbook workbook) {
		for (int i = idx_kol; i < data_length; i++) {
			header.createCell(i).setCellValue(data[i]);
			header.getCell(i).setCellStyle(style);

			range = new CellRangeAddress(firstRow, lastRow, i, i);

			sheet.addMergedRegion(range);
			setAllBoxCellBorder(range, sheet, workbook);
		}
	}

	public void renderColom(int firstRow, int lastRow, String[] data, int start_col, int max_col, int min_bug,
			XSSFCellStyle style, Row header, CellRangeAddress range, Sheet sheet, Workbook workbook) {
		for (int i = start_col; i < max_col; i++) {
			header.createCell(i).setCellValue(data[i - min_bug]);
			header.getCell(i).setCellStyle(style);
			// if(max_col > 1) {
			// range = new CellRangeAddress(firstRow, lastRow, i, max_col);
			// }else {
			range = new CellRangeAddress(firstRow, lastRow, i, i);
			// }

			sheet.addMergedRegion(range);
			setAllBoxCellBorder(range, sheet, workbook);
		}
	}

	public void renderColom(int firstRow, int lastRow, String[] data, int start_col, int max_col, int min_bug,
			int force_merge, XSSFCellStyle style, Row header, CellRangeAddress range, Sheet sheet, Workbook workbook) {
		for (int i = start_col; i < max_col; i++) {
			header.createCell(i).setCellValue(data[i - min_bug]);
			header.getCell(i).setCellStyle(style);
			range = new CellRangeAddress(firstRow, lastRow, i, i);

			sheet.addMergedRegion(range);
			setAllBoxCellBorder(range, sheet, workbook);
		}
	}

	public void renderColomSingle(int firstRow, int lastRow, String value, int start_col, int max_col, int min_bug,
			int force_merge, XSSFCellStyle style, Row header, CellRangeAddress range, Sheet sheet, Workbook workbook) {
		for (int i = start_col; i <= max_col; i++) {
			header.createCell(i).setCellValue(value);
			header.getCell(i).setCellStyle(style);

			range = new CellRangeAddress(firstRow, lastRow, i, max_col);

			sheet.addMergedRegion(range);
			setAllBoxCellBorder(range, sheet, workbook);
		}
	}

	public void renderNormCol(ArrayList<String> data_arr, int start_col_idx, int start_idxRow, int until_idxRow,
			XSSFCellStyle style, Row header, CellRangeAddress range, Sheet sheet, Workbook workbook) {
		int key_col = start_col_idx;
		for (String str : data_arr) {
			header.createCell(key_col).setCellValue(str);
			header.getCell(key_col).setCellStyle(style);
			range = new CellRangeAddress(start_idxRow, until_idxRow, key_col, key_col);
			if (key_col != key_col) {
				sheet.addMergedRegion(range);
			}
			setAllBoxCellBorder(range, sheet, workbook);
			key_col++;
		}
	}

	public void renderNormCol(ArrayList<String> data_arr, int start_col_idx, int until_col_idx, int start_idxRow,
			int until_idxRow, XSSFCellStyle style, Row header, CellRangeAddress range, Sheet sheet, Workbook workbook) {
		int key_col = start_col_idx;
		for (String str : data_arr) {
			header.createCell(key_col).setCellValue(str);
			header.getCell(key_col).setCellStyle(style);
			range = new CellRangeAddress(start_idxRow, until_idxRow, key_col, until_col_idx);
			sheet.addMergedRegion(range);
			setAllBoxCellBorder(range, sheet, workbook);
			key_col++;
		}
	}

}*/