package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MappingUnitKompetensiTeknis {

	public static final String ID = "id_mapping_unit_kompetensi_teknis";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_mapping_unit_kompetensi_teknis";
	public static final String ID_BIDANG_TEKNIS = "id_bidang_teknis_mapping_unit_kompetensi_teknis";
	public static final String ID_UNIT_KOMPETENSI = "id_unit_kompetensi_mapping_unit_kompetensi_teknis";
	public static final String NAMA_KELOMPOK_JABATAN = "nama_kelompok_jabatan_mapping_unit_kompetensi_teknis";
	public static final String NAMA_BIDANG_TEKNIS = "nama_bidang_teknis_mapping_unit_kompetensi_teknis";
	public static final String KODE_UNIT_KOMPETENSI_TEKNIS = "kode_unit_kompetensi_teknis_mapping_unit_kompetensi_teknis";
	public static final String JUDUL_UNIT_KOMPETENSI_TEKNIS = "judul_unit_kompetensi_teknis_mapping_unit_kompetensi_teknis";

	private String id;
	private String idKelompokJabatan;
	private String idBidangTeknis;
	private String idUnitKompetensi;
	private String namaKelompokJabatan;
	private String namaBidangTeknis;
	private String kodeUnitKompetensiTeknis;
	private String judulUnitKompetensiTeknis;

	public MappingUnitKompetensiTeknis() {

	}

	public MappingUnitKompetensiTeknis(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	@JsonProperty("id_bidang_teknis")
	public String getIdBidangTeknis() {
		return idBidangTeknis;
	}

	public void setIdBidangTeknis(String idBidangTeknis) {
		this.idBidangTeknis = idBidangTeknis;
	}

	@JsonProperty("id_unit_kompetensi")
	public String getIdUnitKompetensi() {
		return idUnitKompetensi;
	}

	public void setIdUnitKompetensi(String idUnitKompetensi) {
		this.idUnitKompetensi = idUnitKompetensi;
	}

	@JsonProperty("nama_kelompok_jabatan")
	public String getNamaKelompokJabatan() {
		return namaKelompokJabatan;
	}

	public void setNamaKelompokJabatan(String namaKelompokJabatan) {
		this.namaKelompokJabatan = namaKelompokJabatan;
	}

	@JsonProperty("nama_bidang_teknis")
	public String getNamaBidangTeknis() {
		return namaBidangTeknis;
	}

	public void setNamaBidangTeknis(String namaBidangTeknis) {
		this.namaBidangTeknis = namaBidangTeknis;
	}

	@JsonProperty("kode_unit_kompetensi_teknis")
	public String getKodeUnitKompetensiTeknis() {
		return kodeUnitKompetensiTeknis;
	}

	public void setKodeUnitKompetensiTeknis(String kodeUnitKompetensiTeknis) {
		this.kodeUnitKompetensiTeknis = kodeUnitKompetensiTeknis;
	}

	@JsonProperty("judul_unit_kompetensi_teknis")
	public String getJudulUnitKompetensiTeknis() {
		return judulUnitKompetensiTeknis;
	}

	public void setJudulUnitKompetensiTeknis(String judulUnitKompetensiTeknis) {
		this.judulUnitKompetensiTeknis = judulUnitKompetensiTeknis;
	}
	/**********************************************************************/
	public static final String IDR_KELOMPOK_JABATAN = "id_kelompok_jabatan_mapping_bidang_unit_kompetensi";
	public static final String IDR_BIDANG_TEKNIS = "id_bidang_teknis_mapping_bidang_unit_kompetensi";
	public static final String IDR_UNIT_KOMPETENSI = "id_unit_kompetensi_mapping_bidang_unit_kompetensi";
	
}