package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.annotation.JsonProperty;


public class UserPertanyaan {
	public static final String ID = "id_user_pertanyaan";
	public static final String ID_RESPONDEN = "id_responden_user_pertanyaan";
	public static final String NO = "no_user_pertanyaan";
	public static final String ID_ACCOUNT = "id_account_user_pertanyaan";
	public static final String KODE = "kode_user_pertanyaan";
	public static final String ID_UNIT_KOMPETENSI = "id_unit_kompetensi_user_pertanyaan";
	public static final String ISI = "isi_user_pertanyaan";
	public static final String KETERANGAN = "keterangan_user_pertanyaan";
	public static final String BOBOT = "bobot_user_pertanyaan";
	public static final String KUNCI_JAWABAN = "kunci_jawaban_user_pertanyaan";
	public static final String TIPE_SOAL = "tipe_soal_user_pertanyaan";
	public static final String DURASI = "durasi_user_pertanyaan";
	public static final String DURASI_BERJALAN = "durasi_berjalan_user_pertanyaan";
	public static final String STATUS = "status_user_pertanyaan";
	public static final String JENIS_KOMPETENSI = "jenis_kompetensi_user_pertanyaan";
	public static final String FILE = "file_user_pertanyaan";

	private String id;
	private String idResponden;
	private Integer no;
	private String idAccount;
	private String kode;
	private String idUnitKompetensi;
	private String isi;
	private String keterangan;
	private Integer bobot;
	private Integer kunciJawaban;
	private String tipeSoal;
	private Integer durasi;
	private Integer durasiBerjalan;
	private String status;
	private String file;
	
	public UserPertanyaan() {

	}

	public UserPertanyaan(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("no")
	public Integer getNo() {
		return no;
	}
	
	public void setNo(Integer no) {
		this.no = no;
	}
	
	@JsonProperty("id_responden")
	public String getIdResponden() {
		return idResponden;
	}

	public void setIdResponden(String idResponden) {
		this.idResponden = idResponden;
	}

	@JsonProperty("id_account")
	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	@JsonProperty("kode")
	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	@JsonProperty("id_unit_kompetensi")
	public String getIdUnitKompetensi() {
		return idUnitKompetensi;
	}

	public void setIdUnitKompetensi(String idUnitKompetensi) {
		this.idUnitKompetensi = idUnitKompetensi;
	}

	@JsonProperty("isi")
	public String getIsi() {
		return isi;
	}

	public void setIsi(String isi) {
		this.isi = isi;
	}

	@JsonProperty("keterangan")
	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@JsonProperty("bobot")
	public Integer getBobot() {
		return bobot;
	}

	public void setBobot(Integer bobot) {
		this.bobot = bobot;
	}

	@JsonProperty("kunci_jawaban")
	public Integer getKunciJawaban() {
		return kunciJawaban;
	}

	public void setKunciJawaban(Integer kunciJawaban) {
		this.kunciJawaban = kunciJawaban;
	}

	@JsonProperty("tipe_soal")
	public String getTipeSoal() {
		return tipeSoal;
	}

	public void setTipeSoal(String tipeSoal) {
		this.tipeSoal = tipeSoal;
	}
	
	@JsonProperty("durasi")
	public Integer getDurasi() {
		return durasi;
	}

	public void setDurasi(Integer durasi) {
		this.durasi = durasi;
	}
	
	@JsonProperty("durasi_berjalan")
	public Integer getDurasiBerjalan() {
		return durasiBerjalan;
	}

	public void setDurasiBerjalan(Integer durasiBerjalan) {
		this.durasiBerjalan = durasiBerjalan;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonProperty("file")
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}


	public String getIsiAndFilename() {
		String temp=isi;
		if(file!=null) {
			String id=FilenameUtils.getBaseName(file);
			String ext=FilenameUtils.getExtension(file);
			if(!ext.equals("null")) {
				temp+="<br/>"+"<img class='img-fluid' src='"+"/spektra/files/"+id+"?filename="+file+"'/>";
			}
		}
		return temp;
	}
	/**********************************************************************/

	private List<UserPertanyaanJawaban> userPertanyaanJawaban = new ArrayList<UserPertanyaanJawaban>();

	public List<UserPertanyaanJawaban> getUserPertanyaanJawaban() {
		return userPertanyaanJawaban;
	}

	public void setUserPertanyaanJawaban(List<UserPertanyaanJawaban> userPertanyaanJawaban) {
		this.userPertanyaanJawaban = userPertanyaanJawaban;
	}

	private String order;
	private String jenisKompetensi;
	private String jawaban;
	
	
	@JsonProperty("order")
	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@JsonProperty("jenis_kompetensi")
	public String getJenisKompetensi() {
		return jenisKompetensi;
	}

	public void setJenisKompetensi(String jenisKompetensi) {
		this.jenisKompetensi = jenisKompetensi;
	}

	@JsonProperty("jawaban")
	public String getJawaban() {
		return jawaban;
	}

	public void setJawaban(String jawaban) {
		this.jawaban = jawaban;
	}

	
}
