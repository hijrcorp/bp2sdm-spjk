package id.go.menlhk.bp2sdm.spjk.repo;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

@Repository("firebaseMessaging")
@Configuration
public class FirebaseNotification implements MessagingService {

	@Value("${firebase.server.url}")
	private String url;

	@Value("${firebase.server.key}")
	private String key;

	@Override
	public void send(String destination, Map<String, Object> payload) throws Exception {
		HttpClient client = HttpClientBuilder.create().build();
		System.out.println(url + "--" + key);
		HttpPost post = new HttpPost(url);
		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization", "key=" + key);

		JSONObject msgObj = new JSONObject();
		msgObj.put("to", destination);
		msgObj.put("priority", "high");

		JSONObject notification = new JSONObject();
		notification.put("body", payload.get("message"));
		notification.put("title", payload.get("title"));
		notification.put("sound", "default");
		// notification.put("icon", "myicon");

		JSONObject data = new JSONObject();
		data.put("body", payload.get("message"));
		data.put("title", payload.get("title"));
		data.put("ref_id", payload.get("ref_id"));
		data.put("ref_name", payload.get("ref_name"));

		msgObj.put("notification", notification);
		msgObj.put("data", data);

		post.setEntity(new StringEntity(msgObj.toString(), "UTF-8"));
		HttpResponse response = null;
		try {
			response = client.execute(post);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(response);
		System.out.println(msgObj);
	}

}