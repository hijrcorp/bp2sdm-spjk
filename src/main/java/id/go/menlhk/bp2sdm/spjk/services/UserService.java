package id.go.menlhk.bp2sdm.spjk.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import id.go.menlhk.bp2sdm.spjk.OrganizationReference;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUser;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOrganization;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;

public class UserService implements UserDetailsService {

	// private final Logger logger = Logger.getLogger(UserService.class);

	@Autowired
	private UserMapper userMapper;

	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		String[] userOrg = s.split("::");
		String username = userOrg[0];
		String orgcode = OrganizationReference.SPJK.toString();
		if (userOrg.length > 1) {
			orgcode = userOrg[1];
		}
		String clause = "(" + OauthUser.USERNAME + "='" + username + "' or " + OauthUser.EMAIL
				+ "='" + userOrg[0] + "' )";
		clause += " and " + OauthUserOrganization.ORGANIZATION_CODE + "='" + orgcode + "'";
		List<OauthUserOrganization> lst = userMapper.findListWithOrganization(clause);

		if (lst.size() > 0) {
			OauthUserOrganization user = lst.get(0);

			Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			List<String> roles = userMapper.findRolesByUserOrganization(user.getId(), user.getOrganizationId());
			for (String role : roles) {
				grantedAuthorities.add(new SimpleGrantedAuthority(role));
			}
			user.setAuthorities(grantedAuthorities);

			// UserDetails userDetails = new
			// org.springframework.security.core.userdetails.User(user.getId(),
			// user.getPassword(), user.isEnabled(),
			// user.isAccountNonExpired(), user.isCredentialsNonExpired(),
			// user.isAccountNonLocked(), grantedAuthorities);
			//
			// return userDetails;
			user.setUsername(user.getId()+"::"+user.getOrganizationId()+"::"+user.getUsername());
            
			return user;
		} else {
			throw new UsernameNotFoundException(String.format("User with login ID [%s] not found", s));
		}
	}

}
