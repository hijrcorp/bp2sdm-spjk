package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class UnitKompetensiSosiokultural {
	public static final String ID = "id_unit_kompetensi_sosiokultural";
	public static final String ID_KELOMPOK_SOSIOKULTURAL = "id_kelompok_sosiokultural_unit_kompetensi_sosiokultural";
	public static final String KODE = "kode_unit_kompetensi_sosiokultural";
	public static final String JUDUL = "judul_unit_kompetensi_sosiokultural";
	public static final String ID_SOURCE = "id_source_unit_kompetensi_sosiokultural";
	public static final String ID_ACCOUNT_ADDED = "id_account_added_unit_kompetensi_sosiokultural";
	public static final String TIMESTAMP_ADDED = "timestamp_added_unit_kompetensi_sosiokultural";
	public static final String ID_ACCOUNT_MODIFIED = "id_account_modified_unit_kompetensi_sosiokultural";
	public static final String TIMESTAMP_MODIFIED = "timestamp_modified_unit_kompetensi_sosiokultural";

	private String id;
	private String idKelompokSosiokultural;
	private String kode;
	private String judul;
	private String idSource;
	private String idAccountAdded;
	private Date timestampAdded;
	private String idAccountModified;
	private Date timestampModified;

	public UnitKompetensiSosiokultural() {

	}

	public UnitKompetensiSosiokultural(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_kelompok_sosiokultural")
	public String getIdKelompokSosiokultural() {
		return idKelompokSosiokultural;
	}

	public void setIdKelompokSosiokultural(String idKelompokSosiokultural) {
		this.idKelompokSosiokultural = idKelompokSosiokultural;
	}

	@JsonProperty("kode")
	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	@JsonProperty("judul")
	public String getJudul() {
		return judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	@JsonProperty("id_account_added")
	public String getIdAccountAdded() {
		return idAccountAdded;
	}

	public void setIdAccountAdded(String idAccountAdded) {
		this.idAccountAdded = idAccountAdded;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}

	@JsonProperty("id_account_modified")
	public String getIdAccountModified() {
		return idAccountModified;
	}

	public void setIdAccountModified(String idAccountModified) {
		this.idAccountModified = idAccountModified;
	}

	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}

	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}



	/**********************************************************************/

	
	private List<Pertanyaan> listPertanyaan;

	public List<Pertanyaan> getListPertanyaan() {
		return listPertanyaan;
	}

	public void setListPertanyaan(List<Pertanyaan> listPertanyaan) {
		this.listPertanyaan = listPertanyaan;
	}

	
}
