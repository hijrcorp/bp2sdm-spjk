package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DashboardTahunSurvey {
	public static final String TAHUN_SESI = "tahun_sesi";
	public static final String JUMLAH_RESPONDEN = "jumlah_responden";
	public static final String JUMLAH_SURVEY_PROGRESS = "jumlah_survey_progress";
	public static final String JUMLAH_SURVEY_SELESAI = "jumlah_survey_selesai";
	public static final String JUMLAH_SURVEY_TOTAL = "jumlah_survey_total";

	private String tahunSesi;
	private String jumlahResponden;
	private Integer jumlahSurveyProgress;
	private Integer jumlahSurveySelesai;

	public DashboardTahunSurvey() {

	}

	@JsonProperty(TAHUN_SESI)
	public String getTahunSesi() {
		return tahunSesi;
	}

	public void setTahunSesi(String tahunSesi) {
		this.tahunSesi = tahunSesi;
	}

	@JsonProperty(JUMLAH_RESPONDEN)
	public String getJumlahResponden() {
		return jumlahResponden;
	}

	public void setJumlahResponden(String jumlahResponden) {
		this.jumlahResponden = jumlahResponden;
	}

	@JsonProperty(JUMLAH_SURVEY_PROGRESS)
	public Integer getJumlahSurveyProgress() {
		return jumlahSurveyProgress;
	}

	public void setJumlahSurveyProgress(Integer jumlahSurveyProgress) {
		this.jumlahSurveyProgress = jumlahSurveyProgress;
	}

	@JsonProperty(JUMLAH_SURVEY_SELESAI)
	public Integer getJumlahSurveySelesai() {
		return jumlahSurveySelesai;
	}

	public void setJumlahSurveySelesai(Integer jumlahSurveySelesai) {
		this.jumlahSurveySelesai = jumlahSurveySelesai;
	}

	@JsonProperty(JUMLAH_SURVEY_TOTAL)
	public Integer getJumlahSurveyTotal() {
		return jumlahSurveySelesai + jumlahSurveyProgress;
	}

}
