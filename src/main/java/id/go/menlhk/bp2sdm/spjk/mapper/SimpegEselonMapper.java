package id.go.menlhk.bp2sdm.spjk.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.SimpegEselon;

public interface SimpegEselonMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO simpeg_eselon (kode, eselon, kelompok_eselon) VALUES (#{kode:VARCHAR}, #{eselon:VARCHAR}, #{kelompokEselon:VARCHAR})")
	void insert(SimpegEselon eselon);
	
	@Update("UPDATE simpeg_eselon SET kode=#{kode:VARCHAR}, eselon=#{eselon:VARCHAR}, kelompok_eselon=#{kelompokEselon:VARCHAR} WHERE id_eselon=#{idEselon}")
	void update(SimpegEselon eselon);
	
	@Delete("DELETE FROM simpeg_eselon WHERE ${clause}")
	void deleteBatch(QueryParameter param);
	
	@Delete("DELETE FROM simpeg_eselon WHERE id_eselon=#{idEselon}")
	void delete(SimpegEselon eselon);
	
	@Delete("INSERT INTO simpeg_eselon SELECT * FROM simpeg_eselon_test")
	void copyTest();
	
	List<SimpegEselon> getList(QueryParameter param);
	
	SimpegEselon getEntity(String id);
	
	long getCount(QueryParameter param);
	
	int getNewId();
	
	/********************************** - End Generate - ************************************/

}
