package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MappingBidangUnitKompetensi {

	public static final String ID = "id_mapping_bidang_unit_kompetensi";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_mapping_bidang_unit_kompetensi";
	public static final String ID_BIDANG_TEKNIS = "id_bidang_teknis_mapping_bidang_unit_kompetensi";
	public static final String ID_UNIT_KOMPETENSI = "id_unit_kompetensi_mapping_bidang_unit_kompetensi";

	private String id;
	private String idKelompokJabatan;
	private String idBidangTeknis;
	private String idUnitKompetensi;

	public MappingBidangUnitKompetensi() {

	}

	public MappingBidangUnitKompetensi(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	@JsonProperty("id_bidang_teknis")
	public String getIdBidangTeknis() {
		return idBidangTeknis;
	}

	public void setIdBidangTeknis(String idBidangTeknis) {
		this.idBidangTeknis = idBidangTeknis;
	}

	@JsonProperty("id_unit_kompetensi")
	public String getIdUnitKompetensi() {
		return idUnitKompetensi;
	}

	public void setIdUnitKompetensi(String idUnitKompetensi) {
		this.idUnitKompetensi = idUnitKompetensi;
	}
}