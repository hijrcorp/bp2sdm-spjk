package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BidangTeknis {
	public static final String ID = "id_bidang_teknis";
	public static final String NAMA = "nama_bidang_teknis";
	public static final String ID_KELOMPOK_JABATAN = "id_kelompok_jabatan_bidang_teknis";
	public static final String ID_SOURCE = "id_source_bidang_teknis";
	public static final String ID_ACCOUNT_ADDED = "id_account_added_bidang_teknis";
	public static final String TIMESTAMP_ADDED = "timestamp_added_bidang_teknis";
	public static final String ID_ACCOUNT_MODIFIED = "id_account_modified_bidang_teknis";
	public static final String TIMESTAMP_MODIFIED = "timestamp_modified_bidang_teknis";

	private String id;
	private String nama;
	private String idKelompokJabatan;
	private String idSource;
	private String idAccountAdded;
	private Date timestampAdded;
	private String idAccountModified;
	private Date timestampModified;

	public BidangTeknis() {

	}

	public BidangTeknis(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("id_kelompok_jabatan")
	public String getIdKelompokJabatan() {
		return idKelompokJabatan;
	}

	public void setIdKelompokJabatan(String idKelompokJabatan) {
		this.idKelompokJabatan = idKelompokJabatan;
	}

	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	@JsonProperty("id_account_added")
	public String getIdAccountAdded() {
		return idAccountAdded;
	}

	public void setIdAccountAdded(String idAccountAdded) {
		this.idAccountAdded = idAccountAdded;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}

	@JsonProperty("id_account_modified")
	public String getIdAccountModified() {
		return idAccountModified;
	}

	public void setIdAccountModified(String idAccountModified) {
		this.idAccountModified = idAccountModified;
	}

	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}

	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}



	/**********************************************************************/
}
