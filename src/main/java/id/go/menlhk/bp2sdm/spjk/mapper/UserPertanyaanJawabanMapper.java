package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaanJawaban;

public interface UserPertanyaanJawabanMapper {

	@Insert("INSERT INTO spjk_user_pertanyaan_jawaban (id_user_pertanyaan_jawaban, id_header_user_pertanyaan_jawaban, order_user_pertanyaan_jawaban, isi_user_pertanyaan_jawaban, bobot_user_pertanyaan_jawaban, is_checked_user_pertanyaan_jawaban, file_user_pertanyaan_jawaban) VALUES (#{id:VARCHAR}, #{idHeader:VARCHAR}, #{order:NUMERIC}, #{isi:VARCHAR}, #{bobot:NUMERIC}, #{isChecked}, #{file:VARCHAR})")
	void insert(UserPertanyaanJawaban userPertanyaanJawaban);

	@Update("UPDATE spjk_user_pertanyaan_jawaban SET id_user_pertanyaan_jawaban=#{id:VARCHAR}, id_header_user_pertanyaan_jawaban=#{idHeader:VARCHAR}, order_user_pertanyaan_jawaban=#{order:NUMERIC}, isi_user_pertanyaan_jawaban=#{isi:VARCHAR}, bobot_user_pertanyaan_jawaban=#{bobot:NUMERIC}, is_checked_user_pertanyaan_jawaban=#{isChecked} WHERE id_user_pertanyaan_jawaban=#{id}")
	void update(UserPertanyaanJawaban userPertanyaanJawaban);

	@Delete("DELETE FROM spjk_user_pertanyaan_jawaban WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_user_pertanyaan_jawaban WHERE id_user_pertanyaan_jawaban=#{id}")
	void delete(UserPertanyaanJawaban userPertanyaanJawaban);

	List<UserPertanyaanJawaban> getList(QueryParameter param);

	UserPertanyaanJawaban getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

	@Update("UPDATE spjk_user_pertanyaan_jawaban SET id_user_pertanyaan_jawaban=#{id:VARCHAR}, id_header_user_pertanyaan_jawaban=#{idHeader:VARCHAR}, order_user_pertanyaan_jawaban=#{order:NUMERIC}, isi_user_pertanyaan_jawaban=#{isi:VARCHAR}, bobot_user_pertanyaan_jawaban=#{bobot:NUMERIC}, is_checked_user_pertanyaan_jawaban=#{isChecked} WHERE id_header_user_pertanyaan_jawaban=#{idHeader}")
	void updateReset(UserPertanyaanJawaban userPertanyaanJawaban);
}
