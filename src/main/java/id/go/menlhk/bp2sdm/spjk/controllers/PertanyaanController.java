package id.go.menlhk.bp2sdm.spjk.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import id.go.menlhk.bp2sdm.spjk.core.Clause;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.core.StyleExcel;
import id.go.menlhk.bp2sdm.spjk.domain.Pertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.PertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.UserPertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.UserResponden;
import id.go.menlhk.bp2sdm.spjk.domain.bc.Pernyataan;
import id.go.menlhk.bp2sdm.spjk.domain.bc.PernyataanJawaban;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.helper.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.AccountMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ConfigMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PernyataanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PernyataanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ReferenceMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitKompetensiTeknisMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserPertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserRespondenMapper;
import id.go.menlhk.bp2sdm.spjk.model.Account;
import id.go.menlhk.bp2sdm.spjk.repo.StorageService;

@RestController
@RequestMapping("/pertanyaan")
public class PertanyaanController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	private PertanyaanMapper pertanyaanMapper;
	@Autowired
	private PertanyaanJawabanMapper pertanyaanJawabanMapper;

	@Autowired
	private ConfigMapper configMapper;

	@Autowired
	private ReferenceMapper referenceMapper;

	@Autowired
	private UnitKompetensiTeknisMapper unitKompetensiTeknisMapper;
	
	@Autowired
	private StorageService storageService;

	@Autowired
	private UserPertanyaanMapper userPertanyaanMapper;

	@Autowired
	private UserPertanyaanJawabanMapper userPertanyaanJawabanMapper;
	
	@Autowired
	private UserRespondenMapper userRespondenMapper;
	
	@Autowired
	private SesiMapper sesiMapper;
	
	@Autowired
	private AccountMapper accountMapper;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseWrapperList getList(HttpServletRequest request,
			@RequestParam("filter_keyword") Optional<String> filter_keyword,
			@RequestParam("filter_jenis") Optional<String> filterJenis,
			@RequestParam("filter_id_bidang_teknis") Optional<String> filterIdBidangTeknis,
			@RequestParam("filter_id_unit_kompetensi") Optional<String> filterIdUnitKompetensi,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {
		// MessageResult resp = new MessageResult(Message.SUCCESS_CODE);

		QueryParameter param = new QueryParameter();
		// if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+
		// Pertanyaan.NAMA_KATEGORI_PERTANYAAN + " LIKE '%"+filter_keyword.get()+"%'");
		// if(aspek_pernyataan.isPresent()) param.setClause(param.getClause() + " AND "+
		// Pernyataan.ASPEK_ID_PERNYATAAN + " = '"+aspek_pernyataan.get()+"'");
		if (filterIdUnitKompetensi.isPresent())
			param.setClause(param.getClause() + " AND " + Pertanyaan.ID_UNIT_KOMPETENSI + " = '" + filterIdUnitKompetensi.get() + "'");

		if(filterJenis.isPresent()) param.setClause(param.getClause() + " AND "+ Pertanyaan.JENIS + " = '"+filterJenis.get().toUpperCase()+"'");
		if(filterIdBidangTeknis.isPresent()) param.setClause(param.getClause() + " AND "+ Pertanyaan.ID_BIDANG_TEKNIS + " = '"+filterIdBidangTeknis.get()+"'");
		
		if (limit.isPresent()) {
			param.setLimit(limit.get());
			if (limit.get() > 2000)
				param.setLimit(2000);
		} else {
			param.setLimit(2000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		ResponseWrapperList resp = new ResponseWrapperList();
		// param.setClause("1");
		List<Pertanyaan> data = pertanyaanMapper.getList(param);
		resp.setCount(pertanyaanMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return resp;
	}

	@RequestMapping(value = "/list/unit-kompetensi-teknis", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseWrapperList getListUnitKompetensiTeknis(HttpServletRequest request,
			@RequestParam("filter_keyword") Optional<String> filter_keyword,
			@RequestParam("filter_id_kelompok_jabatan") Optional<String> filter_id_kelompok_jabatan,
			@RequestParam("filter_id_bidang_teknis") Optional<String> filter_id_bidang_teknis,
			@RequestParam("header_responden") Optional<String> header_responden,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {
		// MessageResult resp = new MessageResult(Message.SUCCESS_CODE);

		QueryParameter param = new QueryParameter();
		// if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+
		// Pertanyaan.NAMA_KATEGORI_PERTANYAAN + " LIKE '%"+filter_keyword.get()+"%'");
		if(filter_id_kelompok_jabatan.isPresent()) param.setClause(param.getClause()+" AND "+UnitKompetensiTeknis.ID_KELOMPOK_JABATAN+" ='"+filter_id_kelompok_jabatan.get()+"'");
		if(filter_id_bidang_teknis.isPresent()) param.setClause(param.getClause()+" AND "+Pertanyaan.ID_BIDANG_TEKNIS+" ='"+filter_id_bidang_teknis.get()+"'");
		
		if (limit.isPresent()) {
			if (limit.get() > 2000)
				param.setLimit(2000);
			param.setLimit(limit.get());
		} else {
			param.setLimit(10000000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		ResponseWrapperList resp = new ResponseWrapperList();
		param.setOrder(UnitKompetensiTeknis.ID);
		List<UnitKompetensiTeknis> data = unitKompetensiTeknisMapper.getListWithPertanyaan(param);
//		System.out.println("ter "+data.size());
//		QueryParameter paramPertanyaan = new QueryParameter();
//		paramPertanyaan.setLimit(10000000);
//		paramPertanyaan.setClause(paramPertanyaan.getClause()+" AND "+Pertanyaan.JENIS+" ='"+"TEKNIS"+"'");
//		for(UnitKompetensiTeknis o : data) {
//			List<Pertanyaan> listPer = new ArrayList<>();
//			for(Pertanyaan p : pertanyaanMapper.getList(paramPertanyaan)) {
//				if(o.getId().equals(p.getIdUnitKompetensi())) {
//					listPer.add(p);
//				}
//			}
//			o.setListPertanyaan(listPer);
//		}
		//resp.setCount(unitKompetensiTeknisMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return resp;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(@PathVariable String id,
			@RequestParam("action") Optional<String> action) throws Exception {
		ResponseWrapper resp = new id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper();
		Pertanyaan data = pertanyaanMapper.getEntity(id);
		if(action.isPresent() && action.get().equals(DELETE)) {
			resp.setMessage("Apakah anda yakin ingin menghapus data ini?");
		}
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	  @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	  @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	  public ResponseEntity<ResponseWrapper> doAction(
				@PathVariable String id,
				@PathVariable String action
	  		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Pertanyaan data = pertanyaanMapper.getEntity(id);
		QueryParameter param = new QueryParameter();
		if(action.equals(DELETE)) {
			pertanyaanMapper.delete(data);
			//always run this even the file not exist to  make simple function
			String ext = FilenameUtils.getExtension(data.getFile());
			storageService.delete(data.getId() + "."+ext);
			
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PertanyaanJawaban.ID_HEADER + "='" + data.getId() + "'");
			pertanyaanJawabanMapper.deleteBatch(param);
			for(PertanyaanJawaban pJawaban : pertanyaanJawabanMapper.getList(param)) {
				ext = FilenameUtils.getExtension(pJawaban.getFile());
				storageService.delete(pJawaban.getId() + "."+ext);
			}
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	  }
		
	@RequestMapping(value = "/update-durasi", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> updateDurasi(
			RestQLController controller,
			@RequestParam("id") Optional<String> id,
			@RequestParam("id_action") Optional<String> idAction, 
			@RequestParam("durasi") Optional<Integer> durasi, HttpServletRequest request

	) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		boolean pass = true;
		if (idAction.isPresent() && durasi.isPresent()) {
			if(idAction.get().equals("UPDATE") && durasi.get().toString().equals("")) {
				pass = false;
			}
		}
		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		String[] idArr=id.get().split(",");
		for(String idPertanyaan : idArr) {
			Pertanyaan data = pertanyaanMapper.getEntity(idPertanyaan);
			if(durasi.get().toString().equals(""))
				data.setDurasi(Integer.valueOf(configMapper.getEntityByKode("DURASI_SOAL").getValue()));
			else 
				data.setDurasi(durasi.get());
			pertanyaanMapper.update(data);
		}
		System.out.println(idArr);

		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/delete-soal", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> deleteSoal(
			RestQLController controller,
			@RequestParam("id") Optional<String> id,
			@RequestParam("id_action") Optional<String> idAction, HttpServletRequest request

	) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		boolean pass = true;
		
		if (idAction.isPresent()) {
			if(idAction.get().equals("DELETE")) {
				String[] idArr=id.get().split(",");
				for(String idPertanyaan : idArr) {
					Pertanyaan data = pertanyaanMapper.getEntity(idPertanyaan);
					//always run this even the file not exist to  make simple function]
					String ext = FilenameUtils.getExtension(data.getFile());
					storageService.delete(data.getId() + "."+ext);
					
					pertanyaanMapper.delete(data);
					QueryParameter param = new QueryParameter();
					param.setClause(param.getClause() + " AND " + PertanyaanJawaban.ID_HEADER + "='" + data.getId() + "'");
					pertanyaanJawabanMapper.deleteBatch(param);
				}
//				System.out.println(idArr);
			}
		}else {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> save(
			RestQLController controller,
			@RequestParam("id") Optional<String> id,
			@RequestParam("kode") Optional<String> kode,
			@RequestParam("id_unit_kompetensi") Optional<String> idUnitKompetensi,
			@RequestParam("id_bidang_teknis") Optional<String> idBidangTeknis,
			@RequestParam("isi") Optional<String> isi,
			//
			@RequestParam("file") Optional<MultipartFile> file,
			@RequestParam("file_jawaban") Optional<MultipartFile>[] fileJawaban,
			//@RequestParam("files") MultipartFile[] files
			//
			@RequestParam("keterangan") Optional<String> keterangan,
			@RequestParam("kunci_jawaban") Optional<Integer> kunciJawaban,
			@RequestParam("kunci_jawaban_work") Optional<Integer> kunciJawabanWork,
			@RequestParam("tipe_soal") Optional<String> tipeSoal,
			@RequestParam("bobot") Optional<Integer> bobot, 
			@RequestParam("jenis") Optional<String> jenis, 
			@RequestParam("durasi") Optional<Integer> durasi, HttpServletRequest request

	) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();

		Pertanyaan data = new Pertanyaan(Utils.getLongNumberID());
		if (id.isPresent()) {
			data.setId(id.get());
			data = pertanyaanMapper.getEntity(id.get());
		}
		if (data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		if (kode.isPresent()) data.setKode(kode.get());
		if (idUnitKompetensi.isPresent()) data.setIdUnitKompetensi(idUnitKompetensi.get());
		if (idBidangTeknis.isPresent()) data.setIdBidangTeknis(idBidangTeknis.get());
		if (isi.isPresent()) data.setIsi(isi.get());
		if (keterangan.isPresent()) data.setKeterangan(keterangan.get());
		if (bobot.isPresent()) data.setBobot(bobot.get());
		if (kunciJawaban.isPresent()) data.setKunciJawaban(kunciJawaban.get());
		if (kunciJawabanWork.isPresent()) data.setKunciJawaban(kunciJawabanWork.get());
		if (tipeSoal.isPresent()) data.setTipeSoal(tipeSoal.get());
		if (durasi.isPresent()) data.setDurasi(durasi.get());
		if (jenis.isPresent()) data.setJenis(jenis.get().toUpperCase());

		boolean pass = true;
		if (data.getIdUnitKompetensi() == null && data.getBobot() == null && data.getKunciJawaban() == null) {
			pass = false;
		} else {
			if (data.getIdUnitKompetensi().equals("")) {
				pass = false;
			}
		}
		
		if(data.getJenis().equals("TEKNIS")) {
			if(data.getIsi() == null && data.getIdBidangTeknis() == null) pass = false;
			else {
				if(data.getIsi().equals("") && data.getIdBidangTeknis().equals("")) {
					pass = false;
				}
			}
		}
		
		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		//only execute when input file have files
		if(file.isPresent() && !file.get().isEmpty()) {
			data.setFile(file.get().getOriginalFilename());
			String ext = FilenameUtils.getExtension(data.getFile());
			//impossible to duplicate cause set name file as id
			storageService.store(file.get(), data.getId() + "."+ext);
		}
		if (id.isPresent()) {
			pertanyaanMapper.update(data);
		} else {
			pertanyaanMapper.insert(data);
		}
		
		//
		int i = 1;
		String[] bobotJawaban = request.getParameterValues("bobot_jawaban");
		String[] idJawaban = request.getParameterValues("id_jawaban");
		String[] isiJawaban = request.getParameterValues("isi_jawaban");
		
		//Part fileJawabans = request.getPart("file_jawaban");
		/*
		List<Part> fileParts = request.getParts().stream().filter(part -> "file".equals(part.getName()) && part.getSize() > 0).collect(Collectors.toList()); // Retrieves <input type="file" name="file" multiple="true">

	    for (Part filePart : fileParts) {
	        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
	        InputStream fileContent = filePart.getInputStream();
	        // ... (do your job here)
	    }
		*/
		List<PertanyaanJawaban> arr_pernyataan_jawaban = new ArrayList<PertanyaanJawaban>();
		System.out.println("count");
		System.out.println(idJawaban.length);
		if (idJawaban.length > 0) {
			System.out.println("TRY UPDATE");
			int ke = 0;
			for (String o : idJawaban) {
				PertanyaanJawaban jawaban = new PertanyaanJawaban();
				if (!o.equals("")) {
					jawaban = pertanyaanJawabanMapper.getEntity(o);
				} else {
					jawaban = new PertanyaanJawaban(Utils.getLongNumberID());
				}
				//
				if(fileJawaban.length > 0 && !fileJawaban[ke].get().isEmpty()) {
					jawaban.setFile(fileJawaban[ke].get().getOriginalFilename());
					String ext = FilenameUtils.getExtension(jawaban.getFile());
					//impossible to duplicate cause set name file as id
					storageService.store(fileJawaban[ke].get(), jawaban.getId() + "."+ext);
				}
				if (!o.equals("")) {
					System.out.println("RUN: " + i);
					System.out.println("OBJ: " + isiJawaban[ke]);
					// jawaban.setHeaderPernyataanIdPernyataanJawaban(data.getIdPernyataan());
					// jawaban.setOrderPernyataanJawaban(i);
					jawaban.setIsi(isiJawaban[ke]);
					// jawaban.setBobotPernyataanJawaban(i);
					pertanyaanJawabanMapper.update(jawaban);
					arr_pernyataan_jawaban.add(jawaban);
					ke++;
				} else {
					jawaban.setIdHeader(data.getId());
					jawaban.setOrder(ke+1);
					jawaban.setIsi(isiJawaban[ke]);
					if(kunciJawaban.isPresent()) {
						if(ke==kunciJawaban.get()-1) {
							jawaban.setBobot(1);
						}else {
							jawaban.setBobot(0);
						}
						
					}else {
						jawaban.setBobot(Integer.valueOf(bobotJawaban[ke]));
					}
					
					pertanyaanJawabanMapper.insert(jawaban);
					arr_pernyataan_jawaban.add(jawaban);
					ke++;
				}
			}
		}else{
			
			/*System.out.println("TRY SAVE");
			for (String o : isiJawaban) {
				if (o.equals("")) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
					resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}

				PertanyaanJawaban jawaban = new PertanyaanJawaban(Utils.getLongNumberID());
				jawaban.setHeaderPertanyaanId(data.getId());
				jawaban.setOrder(i);
				jawaban.setIsi(o);
				jawaban.setBobot(i);
				pertanyaanJawabanMapper.insert(jawaban);
				arr_pernyataan_jawaban.add(jawaban);
				i++;
			}*/
			
		}

		data.setListPertanyaanJawaban(arr_pernyataan_jawaban);
		resp.setData(data);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/preview-import", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<ResponseWrapper> importOutput(
			@RequestParam("mode") Optional<String> mode,
			@RequestParam("id_bidang_teknis") Optional<String> idBidangTeknis,
			@RequestParam("file_excel") Optional<MultipartFile> fileExcel,
			@RequestParam("start_read_row") Optional<Integer> startReadRow,
			HttpServletRequest request,
			MultipartHttpServletRequest req
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		QueryParameter param = new QueryParameter();
		QueryParameter ownParam = new QueryParameter();
		
		List<UnitKompetensiTeknis> listUnitKompetensiTeknis = new ArrayList<UnitKompetensiTeknis>();
		List<Pertanyaan> listPertanyaan = new ArrayList<Pertanyaan>();
		List<PertanyaanJawaban> listPertanyaanJawaban = new ArrayList<PertanyaanJawaban>();
		List<UnitKompetensiTeknis> listUkp = unitKompetensiTeknisMapper.getList(new QueryParameter());
		if(fileExcel.isPresent() && !fileExcel.get().isEmpty()) {
			Iterator<String> itr = req.getFileNames();
			MultipartFile mpf = req.getFile(itr.next());

			InputStream file = mpf.getInputStream();

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet;
			String sheetComb="";
			for(int i=1; i < workbook.getNumberOfSheets();i++) {
				sheet = workbook.getSheetAt(i);

				String sheetname = sheet.getSheetName().replaceAll(" ", "");
				String sheetid = "";
				param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+UnitKompetensiTeknis.KODE+" ='"+sheetname+"'");
				if(unitKompetensiTeknisMapper.getList(param).size() == 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Kode "+sheetname+" Tidak valid.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}else {
					sheetid=unitKompetensiTeknisMapper.getList(param).get(0).getId();
				}
				Iterator<Row> rowIterator = sheet.iterator();
				int iRow = 0;
				int defaultRow = 2;
				if(startReadRow.isPresent()) {
					defaultRow = startReadRow.get()-2;
				}
				String idHead="";
				Integer tesString=0;
				//for check duplicate
				Double dupValue = null;
				ArrayList<Double> dupList = new ArrayList<Double>();
				//
				while(rowIterator.hasNext()) {
					Row row = rowIterator.next();
					//if(row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType() == CellType.BLANK)break;
					//skip header
					//String sheetname = sheet.getSheetName().replaceAll(" ", "");
					//System.out.println("Read from: "+defaultRow);
					if(row.getRowNum() > defaultRow) {
//						if(row.getCell(0)==null && row.getCell(1)!=null) {
//							//if(row.getCell(3).getStringCellValue().equals("")) {
//								resp.setCode(HttpStatus.BAD_REQUEST.value());
//								resp.setMessage("Terjadi masalah pada :<br/>"+
//										"sheet="+sheetname+"<br/>no.soal:<strong>"+row.getCell(0).getNumericCellValue()+"</strong> "+"Masing Kosong");//row.getCell(3).getStringCellValue()
//								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//							//}
//						}
						if(row.getCell(0)==null && row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK&& row.getCell(3, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK&& row.getCell(4, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK) {
							resp.setCode(HttpStatus.BAD_REQUEST.value());
							resp.setMessage("Terjadi masalah pada :<br/>"+
									"sheet="+sheetname+"<br/>no.soal masing kosong/format tidak sesuai, periksa row:"+(row.getRowNum()+1));
							return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
						}
						if(row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK && row.getCell(1,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK) {
							//convert ke numveric jika cell pertama mengandung string
							if(row.getCell(0).getCellType()==CellType.STRING) {
								if(row.getCell(0).getStringCellValue().matches("^\\s*$")) {
									resp.setCode(HttpStatus.BAD_REQUEST.value());
									resp.setMessage("Terjadi masalah pada :<br/>"+
											"sheet="+sheetname+"<br/>format tidak sesuai, periksa row:"+(row.getRowNum()+1));
									return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
								}
								String newNo0 = row.getCell(0).getStringCellValue().replaceAll("\'", "");
								String newNo = newNo0.replaceAll("\\.", ""); 
								//System.out.println("witch: "+newNo);
								row.getCell(0).setCellType(CellType.NUMERIC);
								row.getCell(0).setCellValue(Integer.valueOf(newNo.replaceAll(" ", "")));
							}
							if(row.getCell(0).getCellType()==CellType.NUMERIC) {
								//System.out.println("phan: "+row.getCell(3).getCellType());
								if(row.getCell(3, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
									//if(row.getCell(3).getStringCellValue().equals("")) {
										resp.setCode(HttpStatus.BAD_REQUEST.value());
										resp.setMessage("Terjadi masalah pada :<br/>"+
												"sheet="+sheetname+"<br/>no.soal:<strong>"+row.getCell(0).getNumericCellValue()+"</strong> dan jawaban="+"Masing Kosong");//row.getCell(3).getStringCellValue()
										return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
									//}
								}
								if(row.getCell(4, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
									//if(row.getCell(4).getStringCellValue().equals("")) {
										resp.setCode(HttpStatus.BAD_REQUEST.value());
										resp.setMessage("Terjadi masalah pada :<br/>"+
												"sheet="+sheetname+"<br/>no.soal:<strong>"+row.getCell(0).getNumericCellValue()+"</strong> dan kategori.soal="+"Masih Kosong");//row.getCell(4).getStringCellValue()
										return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
									//}
								}
							}else {
								/*
								String newNo0 = row.getCell(0).getStringCellValue().replaceAll("\'", "");
								String newNo = newNo0.replaceAll("\\.", ""); 
								//System.out.println("witch: "+newNo);
								row.getCell(0).setCellType(CellType.NUMERIC);
								row.getCell(0).setCellValue(Integer.valueOf(newNo.replaceAll(" ", "")));
								*/
							}
						}else {
							if(row.getCell(0)==null && row.getCell(1).getCellType()!=CellType.BLANK&& row.getCell(3).getCellType()!=CellType.BLANK&& row.getCell(4).getCellType()!=CellType.BLANK) {
								resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>no.soal masing kosong/format tidak sesuai="+"row.getCell(0).getStringCellValue()");
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							}
							
							if(row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK && row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK && row.getCell(2, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
								resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>soal masing kosong, periksa row:"+(row.getRowNum()+1+" atau no.soal:"+row.getCell(0).getStringCellValue()));
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							}
							
							/*
							if(row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()!=CellType.BLANK && row.getCell(1,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK && row.getCell(2,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
								if(row.getCell(0).getCellType()==CellType.STRING) {
									String newNo0 = row.getCell(0).getStringCellValue().replaceAll("\'", "");
									String newNo = newNo0.replaceAll("\\.", ""); 
									//System.out.println("witch: "+newNo);
									row.getCell(0).setCellType(CellType.NUMERIC);
									row.getCell(0).setCellValue(Integer.valueOf(newNo.replaceAll(" ", "")));
								}
								
								if(row.getCell(3, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
									//if(row.getCell(3).getStringCellValue().equals("")) {
										resp.setCode(HttpStatus.BAD_REQUEST.value());
										resp.setMessage("Terjadi masalah pada :<br/>"+
												"sheet="+sheetname+"<br/>no.soal:<strong>"+row.getCell(0).getNumericCellValue()+"</strong> dan jawaban="+"Masing Kosong");//row.getCell(3).getStringCellValue()
										return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
									//}
								}
								if(row.getCell(4, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType()==CellType.BLANK) {
									//if(row.getCell(4).getStringCellValue().equals("")) {
										resp.setCode(HttpStatus.BAD_REQUEST.value());
										resp.setMessage("Terjadi masalah pada :<br/>"+
												"sheet="+sheetname+"<br/>no.soal:<strong>"+row.getCell(0).getNumericCellValue()+"</strong> dan kategori.soal="+"Masih Kosong");//row.getCell(4).getStringCellValue()
										return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
									//}
								}
							}*/
						}
						
//						row.getCell(0).setCellType(CellType.STRING);
//						if(!row.getCell(0).getStringCellValue().equals("")) {
//							resp.setCode(HttpStatus.BAD_REQUEST.value());
//							resp.setMessage("Terjadi masalah pada sheet:"+row.getCell(0).getStringCellValue()+row.getCell(1).getStringCellValue()+"-no.soal "+"");
//							return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//						}
						
						/*else if(row.getCell(2).getCellType()==CellType.STRING) {
							if(row.getCell(2).getStringCellValue().equals("")) {
								resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>jawaban="+row.getCell(2).getStringCellValue());
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							}
						}else if(row.getCell(3).getCellType()==CellType.STRING) {
							if(row.getCell(3).getStringCellValue().equals("")) {
								resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>kategori.soal="+row.getCell(3).getStringCellValue());
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							}
						}*/

						/*
						if(row.getCell(0).getCellType()==CellType.STRING) {
							//String regex = "[0-9, /,]+";
							//System.out.println(row.getCell(0).getStringCellValue().contains("."));
							if(row.getCell(0).getStringCellValue().contains(".")) {
								row.getCell(0).getStringCellValue().replaceAll(".", "");
							}else if(row.getCell(0).getStringCellValue().contains("'")) {
								row.getCell(0).getStringCellValue().replaceAll("'", "");
							}else {
								resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>no.soal="+row.getCell(0).getStringCellValue());
								resp.setMessage(resp.getMessage()+"<hr/>*Pastikan nomor soal tidak mengandung simbol, karena hanya dapat mendukung format angka/numerik.");
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							}
						}*/
						if(row.getCell(0)!=null && row.getCell(0).getNumericCellValue()!=0) {

							//temp
							//row.getCell(0).setCellType(CellType.STRING);
					        dupValue = row.getCell(0).getNumericCellValue();
					        //System.out.println("order id,ctn,item id: "+dupValue);
							//
					        if (dupList.contains(dupValue)){
					            resp.setCode(HttpStatus.BAD_REQUEST.value());
								resp.setMessage("Terjadi duplikat masalah pada :<br/>"+
										"sheet="+sheetname+"<br/>no.soal="+row.getCell(0).getNumericCellValue());
								return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
							} else {
					            dupList.add(dupValue);
					        }
					        //end
					        
							String pattern="000";
							DecimalFormat myFormatter = new DecimalFormat(pattern);
							String output = myFormatter.format(row.getCell(0).getNumericCellValue());
							idHead=sheetname+"."+output;
							System.out.println("no "+row.getCell(0).getNumericCellValue()+"-"+sheetname);
							Pertanyaan pertanyaan = new Pertanyaan(id.go.menlhk.bp2sdm.spjk.common.Utils.getUUIDString());
							pertanyaan.setKode(idHead);
							pertanyaan.setIdUnitKompetensi(sheetid);
							pertanyaan.setIsi(row.getCell(1).getStringCellValue());
							pertanyaan.setBobot(1);
							pertanyaan.setDurasi(Integer.valueOf(configMapper.getEntityByKode("DURASI_SOAL").getValue()));
							pertanyaan.setIdBidangTeknis(idBidangTeknis.get());
							char ch = row.getCell(3).getStringCellValue().toLowerCase().charAt(0);
							int pos = ch - 'a' + 1;
							pertanyaan.setKunciJawaban(pos);
							tesString=pos;
							pertanyaan.setTipeSoal(row.getCell(4).getStringCellValue().toUpperCase());
							pertanyaan.setJenis("TEKNIS");
							listPertanyaan.add(pertanyaan);
						}

						String check1 = (row.getCell(1)!=null?row.getCell(1).getStringCellValue():"");
						String check2="";
						if(row.getCell(2)!=null) {
							if(row.getCell(2).getCellType()==CellType.STRING) {
								check2 = row.getCell(2).getStringCellValue(); 
							}else if(row.getCell(2).getCellType()==CellType.NUMERIC) { 
								check2 = String.valueOf(row.getCell(2).getNumericCellValue());
							}
						}
						//= (row.getCell(2)!=null?row.getCell(2).getCellType()==CellType.STRING.getStringCellValue():"");
//						if(cell.getCellType()==CellType.STRING) 
//						    data = cell.getStringCellValue(); 
//						else if(cell.getCellType()==CellType.NUMERIC) 
//						    data = String.valueOf(cell.getNumericCellValue());
						String check3 = (row.getCell(3)!=null?row.getCell(3).getStringCellValue():"");
						if(!check1.equals("") && !check2.equals("") && check3.equals("")) {
							System.out.println("order "+row.getCell(1).getStringCellValue()+"."+check2+":type:"+row.getCell(2).getCellType());
							PertanyaanJawaban pertanyaanJawaban = new PertanyaanJawaban();
							pertanyaanJawaban.setIdHeader(idHead);
							char ch = row.getCell(1).getStringCellValue().charAt(0);
							int pos = ch - 'a' + 1;
							pertanyaanJawaban.setOrder(pos);
							pertanyaanJawaban.setIsi(check2);
							if(tesString==pos) {
								pertanyaanJawaban.setBobot(1);
							}else {
								pertanyaanJawaban.setBobot(0);
							}
							
							
							listPertanyaanJawaban.add(pertanyaanJawaban);
						}
					}
					iRow++;
				}
				UnitKompetensiTeknis uk = new UnitKompetensiTeknis();
				uk.setId(sheetid);
				uk.setKode(sheetname);
				listUnitKompetensiTeknis.add(uk);
				sheetComb+=","+sheetid;
				//ownParam.setClause(ownParam.getClause()+" AND "+Pertanyaan.ID_UNIT_KOMPETENSI+" ='"+sheetid+"'");
			}
			ownParam.setClause(ownParam.getClause()+" AND "+Pertanyaan.ID_UNIT_KOMPETENSI+" in("+sheetComb.substring(1)+")");
			ownParam.setClause(ownParam.getClause()+" AND "+Pertanyaan.ID_BIDANG_TEKNIS+" ='"+idBidangTeknis.get()+"'");
			
			System.out.println("before delete");
			//System.out.println(ownParam.getClause());
			System.out.println("pertyanyaan size:"+listPertanyaan.size());
			System.out.println("pertyanyaan Jawaban size:"+listPertanyaanJawaban.size());
			
			//always delete when run this func
			if(mode.isPresent() && mode.get().equals("save")) {
				System.out.println("is size real: "+pertanyaanMapper.getList(ownParam).size());
				QueryParameter newownParam = new QueryParameter();
				QueryParameter newparam = new QueryParameter();
				String concateIdPer="";
				for(Pertanyaan p : pertanyaanMapper.getList(ownParam)) {
					//System.out.println("ID "+p.getId());
					concateIdPer+=","+p.getId();
				}
				System.out.println("Wjhat "+concateIdPer);
				if(pertanyaanMapper.getList(ownParam).size()>0) {
					newparam.setClause(newparam.getClause()+" AND "+Pertanyaan.ID+" in("+concateIdPer.substring(1)+")");
					newownParam.setClause(newownParam.getClause()+" AND "+PertanyaanJawaban.ID_HEADER+" in("+concateIdPer.substring(1)+")");
					pertanyaanMapper.deleteBatch(newparam); 
					pertanyaanJawabanMapper.deleteBatch(newownParam);
				}
			}
			for(Pertanyaan p : listPertanyaan) {
				List<PertanyaanJawaban> listPertanyaanJawabanTemp = new ArrayList<>();
				if(mode.isPresent() && mode.get().equals("save")) 
					pertanyaanMapper.insert(p);
				for(PertanyaanJawaban pb : listPertanyaanJawaban) {
					if(p.getKode().equals(pb.getIdHeader())) {
						listPertanyaanJawabanTemp.add(pb);
						pb.setId(id.go.menlhk.bp2sdm.spjk.common.Utils.getUUIDString());
						pb.setIdHeader(p.getId());
						if(mode.isPresent() && mode.get().equals("save")) 
							pertanyaanJawabanMapper.insert(pb);
					}
				}
				p.setListPertanyaanJawaban(listPertanyaanJawabanTemp);
			}
			for(UnitKompetensiTeknis ukt : listUnitKompetensiTeknis) {
				List<Pertanyaan> listPertanyaanTemp = new ArrayList<>();
				for(Pertanyaan p : listPertanyaan) {
					if(ukt.getId().equals(p.getIdUnitKompetensi()))
					listPertanyaanTemp.add(p);
				}
				ownParam.setLimit(100000);
				for(Pertanyaan o : pertanyaanMapper.getList(ownParam)) {
					//System.out.println("ID "+o.getId());
					if(o.getIdUnitKompetensi().equals(ukt.getId())) {
						ukt.setJudul("DUPLICATED");
					}
				}
				ukt.setListPertanyaan(listPertanyaanTemp);
			}
		}
		if(mode.isPresent() && mode.get().equals("save")) resp.setData("save");
		else resp.setData(listUnitKompetensiTeknis);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/preview-export", method = RequestMethod.GET)
	public void exportOutput(
			@RequestParam("id_bidang_teknis") Optional<String> idBidangTeknis,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		String[] list_kode_unit = request.getParameterValues("kode_unit[]");
		String temp="";
		for(String kode_unit : list_kode_unit) {temp+=","+kode_unit;}
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+UnitKompetensiTeknis.ID+" IN("+temp.substring(1)+")");
		List<UnitKompetensiTeknis> listUnitKompTeknis = unitKompetensiTeknisMapper.getList(param);
				
		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true); 
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		StyleExcel style = new StyleExcel();
		
		XSSFSheet sheet = workbook.createSheet("Master");
		for(UnitKompetensiTeknis ukt : listUnitKompTeknis) {
			sheet = workbook.createSheet(ukt.getKode()); //name sheet

			// create header row
			CellRangeAddress range = null;
			int idxRow = 0;
			XSSFRow title = sheet.createRow(idxRow);
			title.createCell(0).setCellValue(ukt.getKode() + " " + ukt.getJudul());
			title.getCell(0).setCellStyle(style.styleBody(workbook, mapBody));
			range = new CellRangeAddress(idxRow, idxRow, 0, 5);
			sheet.addMergedRegion(range);

			idxRow++;
			idxRow++;

			XSSFRow header = sheet.createRow(idxRow++);
			int idx = 0;
			String[] headerArr = {"NO:1:1"," :1:1","PERTANYAAN:1:1","KUNCI JAWABAN:1:1","TIPE SOAL:1:1"};
			List<String> listHeader = new ArrayList<String>();
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }
			int maxBaris = 1;
			for(String namaHeader : listHeader) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0];
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapBody));
					
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
			idx=3;
			QueryParameter parampertanyaan = new QueryParameter();
			parampertanyaan.setClause(parampertanyaan.getClause()+" AND "+Pertanyaan.ID_UNIT_KOMPETENSI+"='"+ukt.getId()+"'");
			parampertanyaan.setClause(parampertanyaan.getClause()+" AND "+Pertanyaan.ID_BIDANG_TEKNIS+"='"+idBidangTeknis.get()+"'");
			parampertanyaan.setLimit(100000000);
			for(Pertanyaan p : pertanyaanMapper.getList(parampertanyaan)) {
				//System.out.println("KODE");
				//System.out.println(p.getKode());
				int le = p.getKode().split("\\.").length;
				String kode=p.getKode();
				String subKode = kode.substring(0, kode.lastIndexOf("."));
				Integer nomor=Integer.valueOf(p.getKode().split("\\.",0)[le-1]);
				//if(p.getJenis().equals("TEKNIS") && p.getIdUnitKompetensi().equals(ukt.getId())) {//){
					XSSFRow aRow = sheet.createRow(idx);
					aRow.createCell(0).setCellValue(nomor);
					aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapBody));
	
					aRow.createCell(1).setCellValue(p.getIsi());
					aRow.getCell(1).setCellStyle(style.styleBody(workbook, mapBody));
					range = new CellRangeAddress(idx, idx, 1, 2);
					RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
					sheet.addMergedRegion(range);

					idx=idx+1;
					for(PertanyaanJawaban pb : p.getListPertanyaanJawaban()) {
						XSSFRow aRows = sheet.createRow(idx);
						aRows.createCell(1).setCellValue(id.go.menlhk.bp2sdm.spjk.common.Utils.toAlphabetic(pb.getOrder()-1).toLowerCase());
						aRows.getCell(1).setCellStyle(style.styleBody(workbook, mapBody));
					
						aRows.createCell(2).setCellValue(pb.getIsi());
						aRows.getCell(2).setCellStyle(style.styleBody(workbook, mapBody));
						idx++;
					}
					
					aRow.createCell(3).setCellValue(id.go.menlhk.bp2sdm.spjk.common.Utils.toAlphabetic(p.getKunciJawaban()-1));
					aRow.getCell(3).setCellStyle(style.styleBody(workbook, map));
				
					aRow.createCell(4).setCellValue(p.getTipeSoal());
					aRow.getCell(4).setCellStyle(style.styleBody(workbook, mapBody));
					idx++;
				//}
			}
			
			
			sheet.setColumnWidth(0, (5  * 256) + 200); 
			sheet.setColumnWidth(2, (75  * 256) + 200); 
			sheet.autoSizeColumn(1);
			sheet.setColumnWidth(3, (20  * 256) + 200); 
			sheet.setColumnWidth(4, (15  * 256) + 200); 
		}

		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename=SPJK-"+"export-pertanyaan"+"-"+new Date()+".xlsx"); //name file
		
		workbook.write(response.getOutputStream());
		workbook.close();
	}

	@RequestMapping(value = "/user-pertanyaan/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> getUserPertanyaanList(HttpServletRequest request,
			@RequestParam("filter_jenis") Optional<String> filterJenis,
			@RequestParam("filter_nip") Optional<String> filterNip,
			@RequestParam("limit") Optional<Integer> limit, 
			@RequestParam("page") Optional<Integer> page)
			throws Exception {

		ResponseWrapperList resp = new ResponseWrapperList();
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+UserResponden.NIP+" ='"+filterNip.get()+"'");
		param.setClause(param.getClause()+" AND "+UserResponden.ID_SESI+" ='"+sesiMapper.getEntityActive().getId()+"'");
		List<UserResponden> userResponden = userRespondenMapper.getListDataGeneral(param);
		if(userRespondenMapper.getCountGeneral(param)==0) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan, atau coba refresh halaman anda.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		param = new QueryParameter();
		if(filterNip.isPresent()) param.setClause(param.getClause() + " AND "+ UserPertanyaan.ID_RESPONDEN + " = '"+userResponden.get(0).getId()+"'");
		if(filterJenis.isPresent())  {
			if(!filterJenis.get().equals("SEMUA_KOMPETENSI")) {
				param.setClause(param.getClause() + " AND "+ UserPertanyaan.JENIS_KOMPETENSI + " = '"+filterJenis.get().toUpperCase()+"'");
			}
		}
		
		if(limit.isPresent()) {
			param.setLimit(limit.get());
			if(limit.get() > 2000) param.setLimit(2000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		List<UserPertanyaan> data = userPertanyaanMapper.getList(param);
		resp.setCount(userPertanyaanMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/user-pertanyaan/update-status-active", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> updateStatusActive(
			@RequestParam("id") Optional<String> id

	) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		System.out.println("sis: "+id.get());
		UserPertanyaan data = userPertanyaanMapper.getEntityNormal(id.get());
		if(data==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan, coba refresh halaman anda.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		UserResponden userResponden = userRespondenMapper.getEntity(data.getIdResponden());
		
		//if(userResponden.getStatusUnitKompetensi().equals(data.getJenisKompetensi())) {
			//delete lama
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+data.getIdResponden()+"'");
			param.setClause(param.getClause()+" AND "+UserPertanyaan.STATUS+" ='"+1+"'");
			List<UserPertanyaan> oldData = userPertanyaanMapper.getList(param);
			for(UserPertanyaan up : oldData) {
				up.setStatus(null);
				userPertanyaanMapper.updateStatus(up);
			}
			data.setStatus("1");
			//update baru
			userPertanyaanMapper.updateStatus(data);
			userResponden.setStatus("PROSES");
			userResponden.setStatusUnitKompetensi(data.getJenisKompetensi());
			userRespondenMapper.update(userResponden);
		//}else {
			//resp.setCode(HttpStatus.BAD_REQUEST.value());
			//resp.setMessage("Perubahan hanya dapat dilakukan pada Kompetensi terakhir yang dipilih:<strong>"+userResponden.getStatusUnitKompetensi()+"</strong><br/>Apabila anda tetap ingin mengubah, anda bisa menggunakan reset kompetensi/mengulangan dari awal.");
			//return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		//}

		resp.setData(data);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/user-pertanyaan/reset-soal", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> resetSoal(
			RestQLController controller,
			@RequestParam("id") Optional<String> id,
			HttpServletRequest request
	) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(id.isPresent() && !id.get().equals("")) {
			String[] idArr=id.get().split(",");
			for(String idPertanyaan : idArr) {
				UserPertanyaan data = userPertanyaanMapper.getEntityNormal(idPertanyaan);
				data.setDurasiBerjalan(null);
				userPertanyaanMapper.updateDurasi(data);
				QueryParameter param = new QueryParameter();
				param.setClause(param.getClause() + " AND " + UserPertanyaanJawaban.ID_HEADER + "='" + data.getId() + "'");
				for(UserPertanyaanJawaban o : userPertanyaanJawabanMapper.getList(param)) {
					o.setIsChecked(0);
					userPertanyaanJawabanMapper.update(o);
				}
			}
		}else {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Soal yang dipilih tidak ditemukan, coba refresh halaman anda, kemudian coba lagi.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/user-pertanyaan/reset-responden", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> resetResponden(
			RestQLController controller,
			@RequestParam("id") Optional<String> id,
			@RequestParam("nip") Optional<String> nip,
			@RequestParam("check") Optional<String> check,
			HttpServletRequest request
	) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		if(sesiMapper.getEntityActive()==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Tidak ditemukan sesi yang aktif..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		UserResponden data = new UserResponden();
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+UserResponden.NIP+" ='"+(nip.isPresent()?nip.get():"")+"'");
		param.setClause(param.getClause()+" AND "+UserResponden.ID_SESI+" ='"+sesiMapper.getEntityActive().getId()+"'");
		
		System.out.println(param.getClause());
		if(userRespondenMapper.getCountGeneral(param) > 0) {
			data=userRespondenMapper.getListDataGeneral(param).get(0);
		}

		if(data==null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Responden tidak ditemukan, coba refresh halaman anda, kemudian coba lagi.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		
		//default do check
		boolean pass = true;
		if(check.isPresent()) {
			if(check.get().equals("2") || check.get().equals("3")) pass=false;
		}
		
		if(pass) {
			if(accountMapper.getEntity(data.getIdAccount())==null) {
				data.setStatus("NOT_VALID");
			}
		}else {
			//cause the account hasbeen missing, so we need get the new id
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+Account.USERNAME+" ='"+data.getNip()+"'");
			String idAccount = accountMapper.getList(param).get(0).getId();
			String idOldAccount = data.getIdAccount();
			//end here

			QueryParameter paramPertanyaan = new QueryParameter();
			paramPertanyaan.setClause(paramPertanyaan.getClause()+" AND "+UserPertanyaan.ID_RESPONDEN+" ='"+data.getId()+"'");
			
			if(check.get().equals("2")) {
				userRespondenMapper.delete(data);

				QueryParameter paramJawaban = new QueryParameter();
				String clause="";
				for(UserPertanyaan oPertanyaan : userPertanyaanMapper.getList(paramPertanyaan)) {clause+=",'"+oPertanyaan.getId()+"'";}
				if(!clause.equals("")) {
					paramJawaban.setClause(paramJawaban.getClause()+" AND "+UserPertanyaanJawaban.ID_HEADER+" IN("+clause.substring(1)+")");
					userPertanyaanJawabanMapper.deleteBatch(paramJawaban);
				}
				userPertanyaanMapper.deleteBatch(paramPertanyaan);
			}else if(check.get().equals("3")) {
				data.setIdAccount(idAccount);
				userRespondenMapper.update(data);
				
				for(UserPertanyaan oPertanyaan : userPertanyaanMapper.getList(paramPertanyaan)) {
					oPertanyaan.setIdAccount(idAccount);
					userPertanyaanMapper.update(oPertanyaan);
					System.out.println("FIX soal:"+oPertanyaan.getId()+":OLD account:"+idOldAccount+":"+"NEW account:"+idAccount+":"+oPertanyaan.getIsi());
				}
			}
		}
		resp.setData(data);
	
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
}
