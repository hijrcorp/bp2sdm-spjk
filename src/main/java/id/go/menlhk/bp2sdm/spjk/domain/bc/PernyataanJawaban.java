package id.go.menlhk.bp2sdm.spjk.domain.bc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PernyataanJawaban {

	/**********************************
	 * - Begin Generate -
	 ************************************/

	public static final String ID_PERNYATAAN_JAWABAN = "id_pernyataan_jawaban";
	public static final String HEADER_PERNYATAAN_ID_PERNYATAAN_JAWABAN = "header_pernyataan_id_pernyataan_jawaban";
	public static final String ORDER_PERNYATAAN_JAWABAN = "order_pernyataan_jawaban";
	public static final String ISI_PERNYATAAN_JAWABAN = "isi_pernyataan_jawaban";
	public static final String BOBOT_PERNYATAAN_JAWABAN = "bobot_pernyataan_jawaban";

	private String idPernyataanJawaban;
	private String headerPernyataanIdPernyataanJawaban;
	private Integer orderPernyataanJawaban;
	private String isiPernyataanJawaban;
	private Integer bobotPernyataanJawaban;

	public PernyataanJawaban() {

	}

	public PernyataanJawaban(String id) {
		this.idPernyataanJawaban = id;
	}

	@JsonProperty(ID_PERNYATAAN_JAWABAN)
	public String getIdPernyataanJawaban() {
		return idPernyataanJawaban;
	}

	public void setIdPernyataanJawaban(String idPernyataanJawaban) {
		this.idPernyataanJawaban = idPernyataanJawaban;
	}

	@JsonProperty(HEADER_PERNYATAAN_ID_PERNYATAAN_JAWABAN)
	public String getHeaderPernyataanIdPernyataanJawaban() {
		return headerPernyataanIdPernyataanJawaban;
	}

	public void setHeaderPernyataanIdPernyataanJawaban(String headerPernyataanIdPernyataanJawaban) {
		this.headerPernyataanIdPernyataanJawaban = headerPernyataanIdPernyataanJawaban;
	}

	@JsonProperty(ORDER_PERNYATAAN_JAWABAN)
	public Integer getOrderPernyataanJawaban() {
		return orderPernyataanJawaban;
	}

	public void setOrderPernyataanJawaban(Integer orderPernyataanJawaban) {
		this.orderPernyataanJawaban = orderPernyataanJawaban;
	}

	@JsonProperty(ISI_PERNYATAAN_JAWABAN)
	public String getIsiPernyataanJawaban() {
		return isiPernyataanJawaban;
	}

	public void setIsiPernyataanJawaban(String isiPernyataanJawaban) {
		this.isiPernyataanJawaban = isiPernyataanJawaban;
	}

	@JsonProperty(BOBOT_PERNYATAAN_JAWABAN)
	public Integer getBobotPernyataanJawaban() {
		return bobotPernyataanJawaban;
	}

	public void setBobotPernyataanJawaban(Integer bobotPernyataanJawaban) {
		this.bobotPernyataanJawaban = bobotPernyataanJawaban;
	}

	/**********************************
	 * - End Generate -
	 ************************************/

}
