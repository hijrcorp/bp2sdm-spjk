package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.MappingBidangUnitKompetensi;

public interface MappingBidangUnitKompetensiMapper {
	
	@Insert("INSERT INTO spjk_mapping_bidang_unit_kompetensi (id_mapping_bidang_unit_kompetensi, id_kelompok_jabatan_mapping_bidang_unit_kompetensi, id_bidang_teknis_mapping_bidang_unit_kompetensi, id_unit_kompetensi_mapping_bidang_unit_kompetensi) VALUES (#{id:VARCHAR}, #{idKelompokJabatan:VARCHAR}, #{idBidangTeknis:VARCHAR}, #{idUnitKompetensi:VARCHAR})")
	void insert(MappingBidangUnitKompetensi mappingBidangUnitKompetensi);

	@Update("UPDATE spjk_mapping_bidang_unit_kompetensi SET id_mapping_bidang_unit_kompetensi=#{id:VARCHAR}, id_kelompok_jabatan_mapping_bidang_unit_kompetensi=#{idKelompokJabatan:VARCHAR}, id_bidang_teknis_mapping_bidang_unit_kompetensi=#{idBidangTeknis:VARCHAR}, id_unit_kompetensi_mapping_bidang_unit_kompetensi=#{idUnitKompetensi:VARCHAR} WHERE id_mapping_bidang_unit_kompetensi=#{id}")
	void update(MappingBidangUnitKompetensi mappingBidangUnitKompetensi);

	@Delete("DELETE FROM spjk_mapping_bidang_unit_kompetensi WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_mapping_bidang_unit_kompetensi WHERE id_mapping_bidang_unit_kompetensi=#{id}")
	void delete(MappingBidangUnitKompetensi mappingBidangUnitKompetensi);

	List<MappingBidangUnitKompetensi> getList(QueryParameter param);

	MappingBidangUnitKompetensi getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
