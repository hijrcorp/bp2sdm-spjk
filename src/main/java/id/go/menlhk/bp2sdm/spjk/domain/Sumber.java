package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Sumber {
	private String namaSumber;

	public Sumber() {

	}

	@JsonProperty("nama_sumber")
	public String getNamaSumber() {
		return namaSumber;
	}

	public void setNamaSumber(String namaSumber) {
		this.namaSumber = namaSumber;
	}

}
