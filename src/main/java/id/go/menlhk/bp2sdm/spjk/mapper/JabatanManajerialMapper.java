package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.JabatanManajerial;

@Mapper
public interface JabatanManajerialMapper {
	@Insert("INSERT INTO spjk_jabatan_manajerial (id_jabatan_manajerial, id_master_jabatan_jabatan_manajerial, id_kelompok_manajerial_jabatan_manajerial, nilai_minimum_jabatan_manajerial, jenis_jabatan_manajerial, id_source_jabatan_manajerial, id_account_added_jabatan_manajerial, timestamp_added_jabatan_manajerial, id_account_modified_jabatan_manajerial, timestamp_modified_jabatan_manajerial) VALUES (#{id:VARCHAR}, #{idMasterJabatan:VARCHAR}, #{idKelompokManajerial:VARCHAR}, #{nilaiMinimum:NUMERIC}, #{jenis:VARCHAR}, #{idSource:VARCHAR}, #{idAccountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{idAccountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(JabatanManajerial jabatanManajerial);

	@Update("UPDATE spjk_jabatan_manajerial SET id_jabatan_manajerial=#{id:VARCHAR}, id_master_jabatan_jabatan_manajerial=#{idMasterJabatan:VARCHAR}, id_kelompok_manajerial_jabatan_manajerial=#{idKelompokManajerial:VARCHAR}, nilai_minimum_jabatan_manajerial=#{nilaiMinimum:NUMERIC}, jenis_jabatan_manajerial=#{jenis:VARCHAR}, id_source_jabatan_manajerial=#{idSource:VARCHAR}, id_account_added_jabatan_manajerial=#{idAccountAdded:VARCHAR}, timestamp_added_jabatan_manajerial=#{timestampAdded:TIMESTAMP}, id_account_modified_jabatan_manajerial=#{idAccountModified:VARCHAR}, timestamp_modified_jabatan_manajerial=#{timestampModified:TIMESTAMP} WHERE id_jabatan_manajerial=#{id}")
	void update(JabatanManajerial jabatanManajerial);

	@Delete("DELETE FROM spjk_jabatan_manajerial WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_jabatan_manajerial WHERE id_jabatan_manajerial=#{id}")
	void delete(JabatanManajerial jabatanManajerial);

	List<JabatanManajerial> getList(QueryParameter param);

	JabatanManajerial getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

}