package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.RataRataKompetensiIndividu;

public interface RataRataKompetensiIndividuMapper {

	@Insert("INSERT INTO spjk_rata_rata_kompetensi_individu (id_rata_rata_kompetensi_individu, id_user_responden_rata_rata_kompetensi_individu, nip_rata_rata_kompetensi_individu, nama_rata_rata_kompetensi_individu, golongan_rata_rata_kompetensi_individu, id_eselon1_rata_rata_kompetensi_individu, id_unit_auditi_rata_rata_kompetensi_individu, id_kelompok_jabatan_rata_rata_kompetensi_individu, id_tingkat_jabatan_rata_rata_kompetensi_individu, id_jenjang_jabatan_rata_rata_kompetensi_individu, nilai_kompetensi_manajerial_rata_rata_kompetensi_individu, nilai_kompetensi_sosiokultural_rata_rata_kompetensi_individu, nilai_kompetensi_teknis_rata_rata_kompetensi_individu, kode_sesi_rata_rata_kompetensi_individu, status_responden_rata_rata_kompetensi_individu) VALUES (#{id:VARCHAR}, #{idUserResponden:VARCHAR}, #{nip:VARCHAR}, #{nama:VARCHAR}, #{golongan:VARCHAR}, #{idEselon1:VARCHAR}, #{idUnitAuditi:VARCHAR}, #{idKelompokJabatan:VARCHAR}, #{idTingkatJabatan:VARCHAR}, #{idJenjangJabatan:VARCHAR}, #{nilaiKompetensiManajerial}, #{nilaiKompetensiSosiokultural}, #{nilaiKompetensiTeknis}, #{kodeSesi:VARCHAR}, #{statusResponden:VARCHAR})")
	void insert(RataRataKompetensiIndividu rataRataKompetensiIndividu);

	@Update("UPDATE spjk_rata_rata_kompetensi_individu SET id_rata_rata_kompetensi_individu=#{id:VARCHAR}, id_user_responden_rata_rata_kompetensi_individu=#{idUserResponden:VARCHAR}, nip_rata_rata_kompetensi_individu=#{nip:VARCHAR}, nama_rata_rata_kompetensi_individu=#{nama:VARCHAR}, golongan_rata_rata_kompetensi_individu=#{golongan:VARCHAR}, id_eselon1_rata_rata_kompetensi_individu=#{idEselon1:VARCHAR}, id_unit_auditi_rata_rata_kompetensi_individu=#{idUnitAuditi:VARCHAR}, id_kelompok_jabatan_rata_rata_kompetensi_individu=#{idKelompokJabatan:VARCHAR}, id_tingkat_jabatan_rata_rata_kompetensi_individu=#{idTingkatJabatan:VARCHAR}, id_jenjang_jabatan_rata_rata_kompetensi_individu=#{idJenjangJabatan:VARCHAR}, nilai_kompetensi_manajerial_rata_rata_kompetensi_individu=#{nilaiKompetensiManajerial}, nilai_kompetensi_sosiokultural_rata_rata_kompetensi_individu=#{nilaiKompetensiSosiokultural}, nilai_kompetensi_teknis_rata_rata_kompetensi_individu=#{nilaiKompetensiTeknis}, kode_sesi_rata_rata_kompetensi_individu=#{kodeSesi:VARCHAR}, status_responden_rata_rata_kompetensi_individu=#{statusResponden:VARCHAR} WHERE id_rata_rata_kompetensi_individu=#{id}")
	void update(RataRataKompetensiIndividu rataRataKompetensiIndividu);

	@Delete("DELETE FROM spjk_rata_rata_kompetensi_individu WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_rata_rata_kompetensi_individu WHERE id_rata_rata_kompetensi_individu=#{id}")
	void delete(RataRataKompetensiIndividu rataRataKompetensiIndividu);

	List<RataRataKompetensiIndividu> getList(QueryParameter param);

	RataRataKompetensiIndividu getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

}
