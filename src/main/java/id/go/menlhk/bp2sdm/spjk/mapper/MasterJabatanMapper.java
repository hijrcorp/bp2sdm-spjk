package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.MasterJabatan;

@Mapper
public interface MasterJabatanMapper {
	@Insert("INSERT INTO spjk_master_jabatan (id_master_jabatan, nama_master_jabatan, id_kelompok_master_jabatan, kode_kelompok_master_jabatan, nama_kelompok_master_jabatan, id_tingkat_master_jabatan, nama_tingkat_master_jabatan, id_jenjang_master_jabatan, nama_jenjang_master_jabatan) VALUES (#{id:VARCHAR}, #{nama:VARCHAR}, #{idKelompok:VARCHAR}, #{kodeKelompok:VARCHAR}, #{namaKelompok:VARCHAR}, #{idTingkat:VARCHAR}, #{namaTingkat:VARCHAR}, #{idJenjang:VARCHAR}, #{namaJenjang:VARCHAR})")
	void insert(MasterJabatan masterJabatan);

	@Update("UPDATE spjk_master_jabatan SET id_master_jabatan=#{id:VARCHAR}, nama_master_jabatan=#{nama:VARCHAR}, id_kelompok_master_jabatan=#{idKelompok:VARCHAR}, kode_kelompok_master_jabatan=#{kodeKelompok:VARCHAR}, nama_kelompok_master_jabatan=#{namaKelompok:VARCHAR}, id_tingkat_master_jabatan=#{idTingkat:VARCHAR}, nama_tingkat_master_jabatan=#{namaTingkat:VARCHAR}, id_jenjang_master_jabatan=#{idJenjang:VARCHAR}, nama_jenjang_master_jabatan=#{namaJenjang:VARCHAR} WHERE id_master_jabatan=#{id}")
	void update(MasterJabatan masterJabatan);

	@Delete("DELETE FROM spjk_master_jabatan WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_master_jabatan WHERE id_master_jabatan=#{id}")
	void delete(MasterJabatan masterJabatan);

	List<MasterJabatan> getList(QueryParameter param);

	MasterJabatan getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

}