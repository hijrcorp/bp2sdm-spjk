package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Laporan {
	public static final String ID_KELOMPOK_JABATAN_UNIT_KOMPETENSI_TEKNIS = "id_kelompok_jabatan_unit_kompetensi_teknis";

	private String idUnitKompetensiTeknis;
	private String idKelompokJabatanUnitKompetensiTeknis;
	private String idFungsiUtamaUnitKompetensiTeknis;
	private String kodeUnitKompetensiTeknis;
	private String judulUnitKompetensiTeknis;
	private String jenisUnitKompetensiTeknis;
	private String pengembanganResult;
	private Double nilaiUnitKompetensiTeknis;
	private Double nilaiStandarUnitKompetensiTeknis;
	private Integer jumlahUnitKompetensiTeknis;

	//@JsonProperty("id")
	public Laporan() {
	
	}
	
	public Laporan(String id) {
		
	}

	public String getIdUnitKompetensiTeknis() {
		return idUnitKompetensiTeknis;
	}

	public void setIdUnitKompetensiTeknis(String idUnitKompetensiTeknis) {
		this.idUnitKompetensiTeknis = idUnitKompetensiTeknis;
	}

	public String getIdKelompokJabatanUnitKompetensiTeknis() {
		return idKelompokJabatanUnitKompetensiTeknis;
	}

	public void setIdKelompokJabatanUnitKompetensiTeknis(String idKelompokJabatanUnitKompetensiTeknis) {
		this.idKelompokJabatanUnitKompetensiTeknis = idKelompokJabatanUnitKompetensiTeknis;
	}

	public String getIdFungsiUtamaUnitKompetensiTeknis() {
		return idFungsiUtamaUnitKompetensiTeknis;
	}

	public void setIdFungsiUtamaUnitKompetensiTeknis(String idFungsiUtamaUnitKompetensiTeknis) {
		this.idFungsiUtamaUnitKompetensiTeknis = idFungsiUtamaUnitKompetensiTeknis;
	}

	public String getKodeUnitKompetensiTeknis() {
		return kodeUnitKompetensiTeknis;
	}

	public void setKodeUnitKompetensiTeknis(String kodeUnitKompetensiTeknis) {
		this.kodeUnitKompetensiTeknis = kodeUnitKompetensiTeknis;
	}

	public String getJudulUnitKompetensiTeknis() {
		return judulUnitKompetensiTeknis;
	}

	public void setJudulUnitKompetensiTeknis(String judulUnitKompetensiTeknis) {
		this.judulUnitKompetensiTeknis = judulUnitKompetensiTeknis;
	}

	public String getJenisUnitKompetensiTeknis() {
		return jenisUnitKompetensiTeknis;
	}

	public void setJenisUnitKompetensiTeknis(String jenisUnitKompetensiTeknis) {
		this.jenisUnitKompetensiTeknis = jenisUnitKompetensiTeknis;
	}
	

	public String getPengembanganResult() {
		return pengembanganResult;
	}

	public void setPengembanganResult(String pengembanganResult) {
		this.pengembanganResult = pengembanganResult;
	}

	public Double getNilaiUnitKompetensiTeknis() {
		return nilaiUnitKompetensiTeknis;
	}

	public void setNilaiUnitKompetensiTeknis(Double nilaiUnitKompetensiTeknis) {
		this.nilaiUnitKompetensiTeknis = nilaiUnitKompetensiTeknis;
	}

	public Double getNilaiStandarUnitKompetensiTeknis() {
		return nilaiStandarUnitKompetensiTeknis;
	}

	public void setNilaiStandarUnitKompetensiTeknis(Double nilaiStandarUnitKompetensiTeknis) {
		this.nilaiStandarUnitKompetensiTeknis = nilaiStandarUnitKompetensiTeknis;
	}

	public Integer getJumlahUnitKompetensiTeknis() {
		return jumlahUnitKompetensiTeknis;
	}

	public void setJumlahUnitKompetensiTeknis(Integer jumlahUnitKompetensiTeknis) {
		this.jumlahUnitKompetensiTeknis = jumlahUnitKompetensiTeknis;
	}
	
	//Get Refrence model Fungsi Utama
	private String kodeFungsiUtama;
	private String judulFungsiUtama;

	public String getKodeFungsiUtama() {
		return kodeFungsiUtama;
	}

	public void setKodeFungsiUtama(String kodeFungsiUtama) {
		this.kodeFungsiUtama = kodeFungsiUtama;
	}

	public String getJudulFungsiUtama() {
		return judulFungsiUtama;
	}

	public void setJudulFungsiUtama(String judulFungsiUtama) {
		this.judulFungsiUtama = judulFungsiUtama;
	}
	
}