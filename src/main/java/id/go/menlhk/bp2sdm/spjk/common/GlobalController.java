/*package id.go.menlhk.bp2sdm.spjk.common;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

import id.co.hijr.sistem.helper.BasicAuthRestTemplate;


@Configuration
@Controller
public class GlobalController extends BaseController {
	
	@RequestMapping(value = "/login-perform", method = RequestMethod.GET)
	public String getLoginPerform(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String code, @RequestParam String page) throws Exception {

		boolean isSuccess = false;
//		String redirect = request.getContextPath() + "/page/" + page;

		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);

		ResponseEntity<Token> result = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			// construct original url waktu request authorization code (WAJIB SAMA)
			String requestPage = request.getRequestURL().toString()+"?page="+page;
	    		
	    		System.out.println(requestPage);

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("code", code);
			map.add("redirect_uri", requestPage);
			map.add("grant_type", "authorization_code");

			result = restTemplate.postForEntity(ssoEndpointUrl + "/oauth/token", map, Token.class);
			Token token = result.getBody();
			
			System.out.println(token.getAccessToken());
			

			setCookie(request, response, token);
			isSuccess = true;
			System.out.println(page);
			response.sendRedirect(page);

		} catch (HttpClientErrorException e) {
			System.out.println(e.getResponseBodyAsString());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		if (!isSuccess) {
			String loginError = ssoEndpointUrl + "/login?error";
			response.sendRedirect(loginError);
		}

		return "blank";
	}
	
	@RequestMapping("/login-logout")
	public void logout(HttpServletRequest request, HttpServletResponse response) throws Exception{ 
		
		
			
		Cookie cookie1 = new Cookie(cookieName, null);
		cookie1.setPath("/");
		cookie1.setMaxAge(0);
		if(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")) {
			cookie1.setDomain(request.getServerName());
		}else {
			cookie1.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
		}
		response.addCookie(cookie1);
		

		String loginPath = getConstructURL(request, "");
		
		response.sendRedirect(ssoEndpointUrl + "/logout?redirect_uri="+loginPath);	
		
		
	}
	
	@RequestMapping("/login-password")
	public void password(HttpServletRequest request, HttpServletResponse response) throws Exception{ 

		response.sendRedirect(ssoEndpointUrl + "/password");	
		
		
	}
	

	@RequestMapping("/")
    public String loadIndex(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(pass) {
			addModelTokenInfo(request, locale, model,accessTokenValue);
			
		}
        return "index";
    }

    
   
    
}


*/