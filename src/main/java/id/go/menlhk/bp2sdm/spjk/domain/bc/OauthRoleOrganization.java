package id.go.menlhk.bp2sdm.spjk.domain.bc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OauthRoleOrganization {

	public static final String ORGANIZATION_ID = "organization_id";
	public static final String POSITION_ID = "position_id";

	private String organizationId;
	private String positionId;
	private String codeOrganization;
	private String nameOrganization;
	private String namePosition;

	public OauthRoleOrganization() {
		super();
	}

	@JsonProperty(ORGANIZATION_ID)
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	@JsonProperty(POSITION_ID)
	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	@JsonProperty("code_organization")
	public String getCodeOrganization() {
		return codeOrganization;
	}

	public void setCodeOrganization(String codeOrganization) {
		this.codeOrganization = codeOrganization;
	}

	@JsonProperty("name_organization")
	public String getNameOrganization() {
		return nameOrganization;
	}

	public void setNameOrganization(String nameOrganization) {
		this.nameOrganization = nameOrganization;
	}

	@JsonProperty("name_position")
	public String getNamePosition() {
		return namePosition;
	}

	public void setNamePosition(String namePosition) {
		this.namePosition = namePosition;
	}

}
