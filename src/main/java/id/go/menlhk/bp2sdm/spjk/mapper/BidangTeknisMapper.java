package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.BidangTeknis;

@Mapper
public interface BidangTeknisMapper {
	@Insert("INSERT INTO spjk_bidang_teknis (id_bidang_teknis, nama_bidang_teknis, id_kelompok_jabatan_bidang_teknis, id_source_bidang_teknis, id_account_added_bidang_teknis, timestamp_added_bidang_teknis, id_account_modified_bidang_teknis, timestamp_modified_bidang_teknis) VALUES (#{id:VARCHAR}, #{nama:VARCHAR}, #{idKelompokJabatan:VARCHAR}, #{idSource:VARCHAR}, #{idAccountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{idAccountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(BidangTeknis bidangTeknis);

	@Update("UPDATE spjk_bidang_teknis SET id_bidang_teknis=#{id:VARCHAR}, nama_bidang_teknis=#{nama:VARCHAR}, id_kelompok_jabatan_bidang_teknis=#{idKelompokJabatan:VARCHAR}, id_source_bidang_teknis=#{idSource:VARCHAR}, id_account_added_bidang_teknis=#{idAccountAdded:VARCHAR}, timestamp_added_bidang_teknis=#{timestampAdded:TIMESTAMP}, id_account_modified_bidang_teknis=#{idAccountModified:VARCHAR}, timestamp_modified_bidang_teknis=#{timestampModified:TIMESTAMP} WHERE id_bidang_teknis=#{id}")
	void update(BidangTeknis bidangTeknis);

	@Delete("DELETE FROM spjk_bidang_teknis WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_bidang_teknis WHERE id_bidang_teknis=#{id}")
	void delete(BidangTeknis bidangTeknis);

	List<BidangTeknis> getList(QueryParameter param);

	BidangTeknis getEntity(String id);


}