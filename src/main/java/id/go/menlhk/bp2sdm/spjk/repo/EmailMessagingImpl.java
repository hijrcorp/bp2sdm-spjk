package id.go.menlhk.bp2sdm.spjk.repo;

import java.util.Map;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

@Repository("emailMessaging")
@Configuration
public class EmailMessagingImpl implements MessagingService {

	// @Value("${security.oauth2.token.key}")
	private String host;
	private String user;
	private String pass;
	private String port;
	private String from;

	@Override
	public void send(String destination, Map<String, Object> payload) throws Exception {
		// TODO Auto-generated method stub
		Email email = new SimpleEmail();
		email.setHostName(this.host);
		email.setSmtpPort(Integer.parseInt(this.port));
		email.setAuthenticator(new DefaultAuthenticator(this.user, this.pass));
		email.setSSLOnConnect(true);
		try {
			email.setFrom(this.from);
			email.setSubject(payload.get("subject").toString());
			email.setMsg(payload.get("body").toString());
			email.addTo(destination);

			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
		}

	}

}
