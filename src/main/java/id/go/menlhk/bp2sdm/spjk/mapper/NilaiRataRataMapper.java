package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.ui.Model;

import id.go.menlhk.bp2sdm.spjk.common.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Control;

@Mapper
public interface NilaiRataRataMapper {

	@Delete("DELETE FROM spjk_rata_rata_individu_existing WHERE kode_sesi_rata_rata_individu_existing=#{kodeSesi}")
	void deleteIndividuExistingBatch(String kodeSesi);

	@Delete("DELETE FROM spjk_rata_rata_kompetensi_individu WHERE kode_sesi_rata_rata_kompetensi_individu=#{kodeSesi}")
	void deleteIndividuBatch(String kodeSesi);

	@Delete("DELETE FROM spjk_rata_rata_kompetensi_teknis WHERE kode_sesi_rata_rata_kompetensi_teknis=#{kodeSesi}")
	void deleteTeknisBatch(String kodeSesi);

	@Delete("DELETE FROM spjk_rata_rata_kompetensi_manajerial WHERE kode_sesi_rata_rata_kompetensi_manajerial=#{kodeSesi}")
	void deleteManajerialBatch(String kodeSesi);

	@Delete("DELETE FROM spjk_rata_rata_kompetensi_sosiokultural WHERE kode_sesi_rata_rata_kompetensi_sosiokultural=#{kodeSesi}")
	void deleteSosiokulturalBatch(String kodeSesi);

	@Delete("DELETE FROM spjk_progress_peserta_ujian WHERE kode_sesi_progress_peserta_ujian=#{kodeSesi}")
	void deletePesertaUjianBatch(String kodeSesi);
	
	@Delete("DELETE FROM spjk_progress_peserta_ujian_detil WHERE kode_sesi_progress_peserta_ujian_detil=#{kodeSesi}")
	void deleteDetilPesertaUjianBatch(String kodeSesi);
	
	@Delete("DELETE FROM spjk_detil_individu WHERE kode_sesi_detil_individu=#{kodeSesi}")
	void deleteIndividuDetilBatch(String kodeSesi);
	
	//new
	@Delete("DELETE FROM spjk_history_jabatan_kompetensi_teknis WHERE kode_sesi_history_jabatan_kompetensi_teknis=#{kodeSesi}")
	void deleteHistoryJabatanKompTeknisBatch(String kodeSesi);
	
	@Delete("DELETE FROM spjk_history_jabatan_manajerial WHERE kode_sesi_history_jabatan_manajerial=#{kodeSesi}")
	void deleteHistoryJabatanKompNonTeknisBatch(String kodeSesi);
	
	@Delete("DELETE FROM spjk_history_metode_pengembangan_kelompok_nilai WHERE kode_sesi_history_metode_pengembangan_kelompok_nilai=#{kodeSesi}")
	void deleteHistoryMetodePengemKelompokNilaiBatch(String kodeSesi);
	
	@Delete("DELETE FROM spjk_history_metode_pengembangan_kompetensi WHERE kode_sesi_history_metode_pengembangan_kompetensi=#{kodeSesi}")
	void deleteHistoryMetodePengemKompBatch(String kodeSesi);
	
	@Delete("DELETE FROM spjk_history_metode_pengembangan_kompetensi_non WHERE kode_sesi_history_metode_pengembangan_kompetensi_non=#{kodeSesi}")
	void deleteHistoryMetodePengemKompNonBatch(String kodeSesi);
	//end new

	List<Model> getListNilai(QueryParameter param);

	Control getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	//custom
	void copyIndividuExisting(String kodeSesi);
	void copy(String kodeSesi);
	void copyTeknis(String kodeSesi);
	void copyManajerial(String kodeSesi);
	void copySosiokultural(String kodeSesi);
	void copyPesertaUjian(String kodeSesi);
	void copyDetilPesertaUjian(String kodeSesi);
	void copyIndividuTeknis(String kodeSesi);
	void copyIndividuManajerSosio(String kodeSesi);
	//new
	void copyHistoryJabatanKompTeknis(String kodeSesi);
	void copyHistoryJabatanKompNonTeknis(String kodeSesi);
	void copyHistoryMetodePengemKelompokNilai(String kodeSesi);
	void copyHistoryMetodePengemKomp(String kodeSesi);
	void copyHistoryMetodePengemKompNon(String kodeSesi);
	//end new

}