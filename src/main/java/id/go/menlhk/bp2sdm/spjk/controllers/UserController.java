package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.OrganizationReference;
import id.go.menlhk.bp2sdm.spjk.core.Clause;
import id.go.menlhk.bp2sdm.spjk.core.CommonUtil;
import id.go.menlhk.bp2sdm.spjk.core.Message;
import id.go.menlhk.bp2sdm.spjk.core.NewMessageResult;
import id.go.menlhk.bp2sdm.spjk.domain.User;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthRoleOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUser;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOperatorOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOrganization;
import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUserOrganizationUnit;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(value = "/unit/{unitKerjaId}", method = RequestMethod.GET)
	public NewMessageResult getListUserUnitKerja(@PathVariable String unitKerjaId,
			@RequestParam Optional<String> position) {
		NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);

		// try {
		// Thread.sleep(1000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		String strPos = OauthUserOrganization.ORGANIZATION_ID + "='" + OrganizationReference.SURVEY.value() + "' ";
		if (position.isPresent()) {
			String[] arrPos = position.get().trim().split(",");
			if (arrPos.length > 1) {
				String inPos = "";
				for (String s : arrPos) {
					inPos += ",'" + s.trim() + "'";
				}
				strPos += " and " + OauthUserOrganization.POSITION_ID + " in (" + inPos.substring(1) + ")";
			} else {
				strPos += " and " + OauthUserOrganization.POSITION_ID + "=" + position.get();
			}

		}
		if (unitKerjaId.equals("all")) {
			resp.setData(userMapper.findListWithOrganizationUnit(strPos));
		} else {
			resp.setData(userMapper.findListWithOrganizationUnit(
					strPos + " and " + OauthUserOrganizationUnit.UNIT_KERJA_ID + "=" + unitKerjaId));
		}

		return resp;
	}

	@RequestMapping(value = "/unit/{unitKerjaId}/save", method = RequestMethod.POST)
	public NewMessageResult saveUnitKerjaUser(@PathVariable String unitKerjaId, @RequestParam String userId) {
		NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);

		userMapper.insertUnitKerjaUser(unitKerjaId, userId);
		return resp;
	}

	@RequestMapping(value = "/unit/{unitKerjaId}/remove", method = RequestMethod.POST)
	public NewMessageResult deleteUnitKerjaUser(@PathVariable String unitKerjaId, @RequestParam String userId) {
		NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);

		userMapper.deleteUnitKerjaUser(unitKerjaId, userId);
		return resp;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Message getList(HttpServletRequest request, @RequestParam("limit") Optional<Integer> limit,
			@RequestParam("page") Optional<Integer> page, @RequestParam("real_name") Optional<String> namaLengkap,
			@RequestParam("username") Optional<String> username, @RequestParam("name_role") Optional<String> position,
			@RequestParam("satker") Optional<String> satker,
			@RequestParam("filter_tanpa_responden") Optional<String> filter_tanpa_responden

	) throws Exception {
		NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);
		int pLimit = 10;
		int pPage = 1;
		if (limit.isPresent())
			pLimit = limit.get();
		if (page.isPresent())
			pPage = page.get();
		int offset = (pPage - 1) * pLimit;

		String filter = OauthUserOrganization.ORGANIZATION_ID + "='" + OrganizationReference.SURVEY.value() + "' ";
		Clause q = new Clause();
		if (namaLengkap.isPresent())
			filter += Clause.AND + q.QRY("full_name", "LIKE", namaLengkap.get());
		if (username.isPresent())
			filter += Clause.AND + q.QRY(OauthUser.USERNAME, "LIKE", username.get());
		if (position.isPresent())
			filter += Clause.AND + "(" + q.QRY(OauthUserOrganization.POSITION_NAME, "=", position.get()) + ")";

		if (satker.isPresent() && position.isPresent()) {
			filter += Clause.AND + q.QRY(OauthUserOperatorOrganization.UNIT_AUDITI_NAME, "LIKE", satker.get());
		}
		if (filter_tanpa_responden.isPresent()) {
			filter += Clause.AND + q.QRY("position_id", "=", filter_tanpa_responden.get());
		}
		String newParam = filter + " order by " + OauthUser.FIRST_NAME;
		newParam += " LIMIT " + (pLimit) + " OFFSET " + offset;
		if (limit.isPresent()) {
			newParam = filter + " LIMIT " + (pPage * pLimit) + " OFFSET " + offset;
		}

		// List<User> data = userMapper.getList(newParam);
		// if(position.isPresent() && position.get().equals("6")) {
		// List<OauthUserOperatorOrganization> data =
		// userMapper.findListWithOrganizationOperator(newParam );
		// resp.setData(data);
		// resp.setCount(userMapper.getCount(filter));
		// resp.setCount(userMapper.findCountListWithOrganizationOperator(filter));
		// }else {
		// List<OauthUserOrganization> data =
		// userMapper.findListWithOrganizationLocal(newParam );
		// resp.setData(data);
		// resp.setCount(userMapper.getCount(filter));
		// resp.setCount(userMapper.findCountListWithOrganizationLocal(filter));
		// }
		resp.setData(userMapper.findListWithOrganizationUnit(newParam));
		resp.setCount(userMapper.findCountWithOrganizationUnit(filter));

		resp.setLimit(pLimit);
		resp.setPageCount(Double.valueOf(Math.ceil((double) resp.getCount() / pLimit)).intValue());
		resp.setActivePage(pPage);
		return resp;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Message getById(@PathVariable String id) throws Exception {
		Message resp = new Message(Message.SUCCESS_CODE);
		// OauthUser data = userMapper.findUserById(id);
		// Map<String, Object> data = new HashMap<String, Object>();
		String clause = OauthUser.ID + "='" + id + "'";
		clause += " and " + OauthUserOrganization.ORGANIZATION_ID + "='" + OrganizationReference.SURVEY.value() + "'";
		List<OauthUserOrganizationUnit> lst = userMapper.findListWithOrganizationUnit(clause);
		// if(lst.size() > 0) {
		// OauthUserOrganization user = lst.get(0);
		// data.put("user", user);
		// data.put("unit_auditi", userMapper.findUnitAuditiUser(user.getId()));
		// data.put("unit_kerja", userMapper.findUnitKerjaUser(user.getId()));
		// }
		resp.setData(lst.get(0));
		return resp;
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Message insert(HttpServletRequest request, @RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("username") String username,
			@RequestParam("position") String position, @RequestParam("password") String password,
			@RequestParam("email") Optional<String> email, @RequestParam("unitAuditi") Optional<String> unitAuditi,
			@RequestParam("unitKerja") Optional<String> unitKerja) throws Exception {

		// System.out.println(unitAuditi.get());

		Message resp = new Message(Message.SUCCESS_CODE);
		User o = new User(String.valueOf((new Date().getTime() + CommonUtil.randomPlus())));
		o.setFirstName(firstName);
		o.setLastName(lastName);
		o.setUsername(username);
		o.setPassword("" + passwordEncoder.encode(password) + "");
		o.setPosition(position);
		if (email.isPresent())
			o.setEmail(email.get());

		if (firstName.isEmpty() || lastName.isEmpty() || username.isEmpty() || position.equals("0")
				|| password.isEmpty()) {
			resp = new Message(Message.WARNING_CODE);
			resp.setMessage("Input masih belum lengkap.");
		} else {
			userMapper.insert(o);

			userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.SURVEY.value());
			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()),
					OrganizationReference.SURVEY.value());

			// userMapper.deleteUnitAuditiUser(unitAuditi.get(), o.getId());
			userMapper.deleteAllUnitKerjaUser(o.getId());

			// if(position.equals("3") || position.equals("4")) {
			if (unitKerja.isPresent()) {
				userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
			}
			// }
			// if(position.equals("6")) {
			// if(unitAuditi.isPresent() && !unitAuditi.get().equals("")) {
			// System.out.println(unitAuditi.get());
			//
			// userMapper.insertUnitAuditiUser(unitAuditi.get(), o.getId());
			// }
			// }

			resp.setData(o);
		}
		return resp;
	}

	@RequestMapping(value = "/{id}/update", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Message update(HttpServletRequest request, @PathVariable String id,
			@RequestParam("firstName") Optional<String> firstName, @RequestParam("lastName") Optional<String> lastName,
			@RequestParam("username") Optional<String> username, @RequestParam("position") Optional<String> position,
			@RequestParam("email") Optional<String> email, @RequestParam("unitAuditi") Optional<String> unitAuditi,
			@RequestParam("unitKerja") Optional<String> unitKerja) throws Exception {

		Message resp = new Message(Message.SUCCESS_CODE);
		User o = userMapper.findById(id);
		if (firstName.isPresent())
			o.setFirstName(firstName.get());
		if (lastName.isPresent())
			o.setLastName(lastName.get());
		if (username.isPresent())
			o.setUsername(username.get());
		if (position.isPresent())
			o.setPosition(position.get());
		// o.setPassword("" + passwordEncoder.encode(o.getPassword()) + "");
		if (email.isPresent())
			o.setEmail(email.get());

		userMapper.update(o);
		// deleteAndInsertPositionWithRole(o.getId(),o.getPosition());

		userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.SURVEY.value());
		userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()),
				OrganizationReference.SURVEY.value());

		// userMapper.deleteUnitAuditiUser(unitAuditi.get(), o.getId());
		userMapper.deleteAllUnitKerjaUser(o.getId());

		if (unitKerja.isPresent()) {
			userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
		}

		resp.setData(o);
		return resp;
	}

	// new method
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> save(HttpServletRequest request, @RequestParam("id") Optional<String> id,
			@RequestParam("first_name") Optional<String> first_name,
			@RequestParam("last_name") Optional<String> last_name, @RequestParam("username") Optional<String> username,
			@RequestParam("position") Optional<String> position, @RequestParam("password") Optional<String> password,
			@RequestParam("email") Optional<String> email, @RequestParam("unit_auditi") Optional<String> unitAuditi,
			@RequestParam("unit_kerja") Optional<String> unitKerja) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();
		User o = new User(String.valueOf((new Date().getTime() + CommonUtil.randomPlus())));
		if (id.isPresent()) {
			o.setId(String.valueOf(id.get()));
			o = userMapper.findById(id.get());
		}
		if (o == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		if (first_name.isPresent())
			o.setFirstName(first_name.get());
		if (last_name.isPresent())
			o.setLastName(last_name.get());
		if (username.isPresent())
			o.setUsername(username.get());
		// if(password.isPresent()) o.setPassword("" +
		// passwordEncoder.encode(password.get()) + "");
		if (position.isPresent())
			o.setPosition(position.get());
		if (email.isPresent())
			o.setEmail(email.get());

		boolean pass = true;
		if (o.getFirstName() == null && o.getLastName() == null && o.getUsername() == null && o.getPosition() == null) {
			pass = false;
		} else {
			if (o.getFirstName().equals("") && o.getLastName().equals("") && o.getUsername().equals("")
					&& o.getPosition().equals("0")) {
				pass = false;
			}
		}

		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if (id.isPresent()) {
			userMapper.update(o);
			// deleteAndInsertPositionWithRole(o.getId(),o.getPosition());

			userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.SURVEY.value());
			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()),
					OrganizationReference.SURVEY.value());

			// userMapper.deleteUnitAuditiUser(unitAuditi.get(), o.getId());
			userMapper.deleteAllUnitKerjaUser(o.getId());

			if (unitKerja.isPresent()) {
				userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
			}
		} else {
			if (password.isPresent())
				o.setPassword("" + passwordEncoder.encode(password.get()) + "");
			else {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("password masih kosong, harap dilengkapi terlebih dahulu.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
			userMapper.insert(o);

			userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.SURVEY.value());
			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()),
					OrganizationReference.SURVEY.value());

			userMapper.deleteAllUnitKerjaUser(o.getId());

			if (unitKerja.isPresent()) {
				userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
			}
			resp.setData(o);
		}

		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public ResponseEntity<ResponseWrapper> changePassword(@RequestParam("oldPassword") Optional<String> oldPassword,
			@RequestParam("newPassword") Optional<String> newPassword,
			@RequestParam("newPassword2") Optional<String> newPassword2) throws Exception {
		OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();
		OauthUserOrganization userAuthentication = (OauthUserOrganization) oAuth2Authentication.getUserAuthentication()
				.getPrincipal();

		ResponseWrapper resp = new ResponseWrapper();
		// System.out.println("ini adalah : " + userAuthentication.getId());
		// System.out.println("die" + oldPassword.isPresent());
		OauthUser o = userMapper.findUserById(userAuthentication.getId());
		if (oldPassword.isPresent()) {
			if (oldPassword.get().equals("")) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Kata kunci lama tidak boleh kosong");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
		}
		if (newPassword.isPresent()) {
			if (newPassword.get().equals("")) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Kata kunci baru tidak boleh kosong");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
		}
		if (newPassword.isPresent() && newPassword2.isPresent()) {
			if (!newPassword.get().equals(newPassword2.get())) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Ketikan ulang kata kunci tidak cocok");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			} else {
				if (!passwordEncoder.matches(oldPassword.get(), o.getPassword())) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
					resp.setMessage("Kata kunci lama anda salah");
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				} else {
					userMapper.updateUserPassword(o.getId(), passwordEncoder.encode(newPassword.get()));
					resp.setData(o);
				}

			}
		}
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/{id}/updatePass", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> updatePass(@PathVariable String id,
			@RequestParam("password") Optional<String> password,
			@RequestParam("password2nd") Optional<String> password2nd) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		User o = userMapper.findById(id);

		if (password.get().isEmpty()) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Kata kunci tidak boleh kosong");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		} else if (!password.get().equals(password2nd.get())) {

			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Ketikan ulang kata kunci tidak cocok");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		} else {
			o.setPassword("" + passwordEncoder.encode(password.get()) + "");
			userMapper.updatePassword(o);
			resp.setData(o);
		}
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Message delete(@PathVariable String id) throws Exception {
		Message resp = new Message(Message.SUCCESS_CODE);
		OauthUser o = userMapper.findUserById(id);
		userMapper.deleteUserPosition(Long.valueOf(id), OrganizationReference.SURVEY.value());
		userMapper.deleteAllUnitAuditiUser(o.getId());
		userMapper.deleteAllUnitKerjaUser(o.getId());
		// userMapper.deleteUserRole(Long.valueOf(id));
		userMapper.deleteUser(Long.valueOf(id));
		resp.setData(o);
		return resp;
	}

	@RequestMapping(value = "/position/", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseWrapper listPosition() {
		ResponseWrapper resp = new ResponseWrapper();

		List<OauthRoleOrganization> data = userMapper.findRolesByOrganization(OrganizationReference.SURVEY.value());
		resp.setData(data);

		return resp;
	}
}
