package id.go.menlhk.bp2sdm.spjk.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Pegawai {
	public static final String NIP = "nip_pegawai";
	public static final String NAMA = "nama_pegawai";
	public static final String PASSWORD = "password_pegawai";
	public static final String EMAIL = "email_pegawai";
	public static final String MOBILE = "mobile_pegawai";
	public static final String STATUS = "status_pegawai";
	public static final String KODE_JABATAN = "kode_jabatan_pegawai";
	public static final String NAMA_JABATAN = "nama_jabatan_pegawai";
	public static final String KODE_ESELON_JABATAN = "kode_eselon_jabatan_pegawai";
	public static final String NAMA_ESELON_JABATAN = "nama_eselon_jabatan_pegawai";
	public static final String KODE_KELOMPOK_JABATAN = "kode_kelompok_jabatan_pegawai";
	public static final String NAMA_KELOMPOK_JABATAN = "nama_kelompok_jabatan_pegawai";
	public static final String KODE_ORGANISASI = "kode_organisasi_pegawai";
	public static final String KODE_KELOMPOK_ORGANISASI = "kode_kelompok_organisasi_pegawai";
	public static final String NAMA_KELOMPOK_ORGANISASI = "nama_kelompok_organisasi_pegawai";
	public static final String NAMA_ORGANISASI = "nama_organisasi_pegawai";
	public static final String TINGKAT_ESELON_ORGANISASI = "tingkat_eselon_organisasi_pegawai";
	public static final String KODE_ESELON_ORGANISASI = "kode_eselon_organisasi_pegawai";
	public static final String NAMA_ESELON_ORGANISASI = "nama_eselon_organisasi_pegawai";
	public static final String KODE_JABATAN_ORGANISASI = "kode_jabatan_organisasi_pegawai";
	public static final String NAMA_JABATAN_ORGANISASI = "nama_jabatan_organisasi_pegawai";
	public static final String KODE_PROPINSI_ORGANISASI = "kode_propinsi_organisasi_pegawai";
	public static final String NAMA_PROPINSI_ORGANISASI = "nama_propinsi_organisasi_pegawai";

	private String nip;
	private String nama;
	private String password;
	private String email;
	private String mobile;
	private boolean status;
	private String kodeJabatan;
	private String namaJabatan;
	private String kodeEselonJabatan;
	private String namaEselonJabatan;
	private String kodeKelompokJabatan;
	private String namaKelompokJabatan;
	private String kodeOrganisasi;
	private String kodeKelompokOrganisasi;
	private String namaKelompokOrganisasi;
	private String namaOrganisasi;
	private String tingkatEselonOrganisasi;
	private String kodeEselonOrganisasi;
	private String namaEselonOrganisasi;
	private String kodeJabatanOrganisasi;
	private String namaJabatanOrganisasi;
	private String kodePropinsiOrganisasi;
	private String namaPropinsiOrganisasi;

	public Pegawai() {

	}

	public Pegawai(String nip) {
		this.nip = nip;
	}

	@JsonProperty("nip")
	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonIgnore
	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("mobile")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@JsonProperty("status")
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@JsonProperty("kode_jabatan")
	public String getKodeJabatan() {
		return kodeJabatan;
	}

	public void setKodeJabatan(String kodeJabatan) {
		this.kodeJabatan = kodeJabatan;
	}

	@JsonProperty("nama_jabatan")
	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	@JsonProperty("kode_eselon_jabatan")
	public String getKodeEselonJabatan() {
		return kodeEselonJabatan;
	}

	public void setKodeEselonJabatan(String kodeEselonJabatan) {
		this.kodeEselonJabatan = kodeEselonJabatan;
	}

	@JsonProperty("nama_eselon_jabatan")
	public String getNamaEselonJabatan() {
		return namaEselonJabatan;
	}

	public void setNamaEselonJabatan(String namaEselonJabatan) {
		this.namaEselonJabatan = namaEselonJabatan;
	}

	@JsonProperty("kode_kelompok_jabatan")
	public String getKodeKelompokJabatan() {
		return kodeKelompokJabatan;
	}

	public void setKodeKelompokJabatan(String kodeKelompokJabatan) {
		this.kodeKelompokJabatan = kodeKelompokJabatan;
	}

	@JsonProperty("nama_kelompok_jabatan")
	public String getNamaKelompokJabatan() {
		return namaKelompokJabatan;
	}

	public void setNamaKelompokJabatan(String namaKelompokJabatan) {
		this.namaKelompokJabatan = namaKelompokJabatan;
	}

	@JsonProperty("kode_organisasi")
	public String getKodeOrganisasi() {
		return kodeOrganisasi;
	}

	public void setKodeOrganisasi(String kodeOrganisasi) {
		this.kodeOrganisasi = kodeOrganisasi;
	}

	@JsonProperty("kode_kelompok_organisasi")
	public String getKodeKelompokOrganisasi() {
		return kodeKelompokOrganisasi;
	}

	public void setKodeKelompokOrganisasi(String kodeKelompokOrganisasi) {
		this.kodeKelompokOrganisasi = kodeKelompokOrganisasi;
	}

	@JsonProperty("nama_kelompok_organisasi")
	public String getNamaKelompokOrganisasi() {
		return namaKelompokOrganisasi;
	}

	public void setNamaKelompokOrganisasi(String namaKelompokOrganisasi) {
		this.namaKelompokOrganisasi = namaKelompokOrganisasi;
	}

	@JsonProperty("nama_organisasi")
	public String getNamaOrganisasi() {
		return namaOrganisasi;
	}

	public void setNamaOrganisasi(String namaOrganisasi) {
		this.namaOrganisasi = namaOrganisasi;
	}

	@JsonProperty("tingkat_eselon_organisasi")
	public String getTingkatEselonOrganisasi() {
		return tingkatEselonOrganisasi;
	}

	public void setTingkatEselonOrganisasi(String tingkatEselonOrganisasi) {
		this.tingkatEselonOrganisasi = tingkatEselonOrganisasi;
	}

	@JsonProperty("kode_eselon_organisasi")
	public String getKodeEselonOrganisasi() {
		return kodeEselonOrganisasi;
	}

	public void setKodeEselonOrganisasi(String kodeEselonOrganisasi) {
		this.kodeEselonOrganisasi = kodeEselonOrganisasi;
	}

	@JsonProperty("nama_eselon_organisasi")
	public String getNamaEselonOrganisasi() {
		return namaEselonOrganisasi;
	}

	public void setNamaEselonOrganisasi(String namaEselonOrganisasi) {
		this.namaEselonOrganisasi = namaEselonOrganisasi;
	}

	@JsonProperty("kode_jabatan_organisasi")
	public String getKodeJabatanOrganisasi() {
		return kodeJabatanOrganisasi;
	}

	public void setKodeJabatanOrganisasi(String kodeJabatanOrganisasi) {
		this.kodeJabatanOrganisasi = kodeJabatanOrganisasi;
	}

	@JsonProperty("nama_jabatan_organisasi")
	public String getNamaJabatanOrganisasi() {
		return namaJabatanOrganisasi;
	}

	public void setNamaJabatanOrganisasi(String namaJabatanOrganisasi) {
		this.namaJabatanOrganisasi = namaJabatanOrganisasi;
	}

	@JsonProperty("kode_propinsi_organisasi")
	public String getKodePropinsiOrganisasi() {
		return kodePropinsiOrganisasi;
	}

	public void setKodePropinsiOrganisasi(String kodePropinsiOrganisasi) {
		this.kodePropinsiOrganisasi = kodePropinsiOrganisasi;
	}

	@JsonProperty("nama_propinsi_organisasi")
	public String getNamaPropinsiOrganisasi() {
		return namaPropinsiOrganisasi;
	}

	public void setNamaPropinsiOrganisasi(String namaPropinsiOrganisasi) {
		this.namaPropinsiOrganisasi = namaPropinsiOrganisasi;
	}


//	public static final String ID_PEGAWAI = "id_pegawai";
//	public static final String NIP_PEGAWAI = "nip_pegawai";
//	public static final String NAMA_PEGAWAI = "nama_pegawai";
//	public static final String JABATAN = "jabatan";
//	public static final String GOLONGAN = "golongan";
//	public static final String EMAIL_PEGAWAI = "email_pegawai";
//	public static final String ID_UNIT_KERJA_PEGAWAI = "id_unit_kerja_pegawai";
//	public static final String NAMA_UNIT_KERJA_PEGAWAI = "nama_unit_kerja_pegawai";
//
//	private String idPegawai;
//	private String nipPegawai;
//	private String namaPegawai;
//	private String jabatan;
//	private String golongan;
//	private String emailPegawai;
//	private String idUnitKerjaPegawai;
//	private String namaUnitKerjaPegawai;
//
//	public Pegawai() {
//
//	}
//
//	public Pegawai(String id) {
//		this.idPegawai = id;
//	}
//
//	@JsonProperty(ID_PEGAWAI)
//	public String getIdPegawai() {
//		return idPegawai;
//	}
//
//	public void setIdPegawai(String idPegawai) {
//		this.idPegawai = idPegawai;
//	}
//
//	@JsonProperty(NIP_PEGAWAI)
//	public String getNipPegawai() {
//		return nipPegawai;
//	}
//
//	public void setNipPegawai(String nipPegawai) {
//		this.nipPegawai = nipPegawai;
//	}
//
//	@JsonProperty(NAMA_PEGAWAI)
//	public String getNamaPegawai() {
//		return namaPegawai;
//	}
//
//	public void setNamaPegawai(String namaPegawai) {
//		this.namaPegawai = namaPegawai;
//	}
//
//	@JsonProperty(JABATAN)
//	public String getJabatan() {
//		return jabatan;
//	}
//
//	public void setJabatan(String jabatan) {
//		this.jabatan = jabatan;
//	}
//
//	@JsonProperty(GOLONGAN)
//	public String getGolongan() {
//		return golongan;
//	}
//
//	public void setGolongan(String golongan) {
//		this.golongan = golongan;
//	}
//
//	@JsonProperty(EMAIL_PEGAWAI)
//	public String getEmailPegawai() {
//		return emailPegawai;
//	}
//
//	public void setEmailPegawai(String emailPegawai) {
//		this.emailPegawai = emailPegawai;
//	}
//
//	@JsonProperty(ID_UNIT_KERJA_PEGAWAI)
//	public String getIdUnitKerjaPegawai() {
//		return idUnitKerjaPegawai;
//	}
//
//	public void setIdUnitKerjaPegawai(String idUnitKerjaPegawai) {
//		this.idUnitKerjaPegawai = idUnitKerjaPegawai;
//	}
//
//	@JsonProperty(NAMA_UNIT_KERJA_PEGAWAI)
//	public String getNamaUnitKerjaPegawai() {
//		return namaUnitKerjaPegawai;
//	}
//
//	public void setNamaUnitKerjaPegawai(String namaUnitKerjaPegawai) {
//		this.namaUnitKerjaPegawai = namaUnitKerjaPegawai;
//	}

	/**********************************************************************/
}
