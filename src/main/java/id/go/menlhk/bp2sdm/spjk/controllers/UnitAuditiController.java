package id.go.menlhk.bp2sdm.spjk.controllers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import id.go.menlhk.bp2sdm.spjk.MainApplication;
import id.go.menlhk.bp2sdm.spjk.common.Utils;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.core.Token;
import id.go.menlhk.bp2sdm.spjk.domain.Control;
import id.go.menlhk.bp2sdm.spjk.domain.Distrik;
import id.go.menlhk.bp2sdm.spjk.domain.Eselon1;
import id.go.menlhk.bp2sdm.spjk.domain.Propinsi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.mapper.Eselon1Mapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ReferenceMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitAuditiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UserMapper;
import id.go.menlhk.bp2sdm.spjk.model.Account;
import id.go.menlhk.bp2sdm.spjk.model.Application;
import id.go.menlhk.bp2sdm.spjk.model.Authorities;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;

@RestController
@RequestMapping("/unit-auditi")
public class UnitAuditiController extends BaseController {

	@Autowired
	private UnitAuditiMapper unitAuditiMapper;

	@Autowired
	private Eselon1Mapper eselon1Mapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private ReferenceMapper referenceMapper;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseWrapperList getList(HttpServletRequest request,
			@RequestParam("filter_keyword") Optional<String> filter_keyword,
			@RequestParam("filter_nama") Optional<String> filter_nama,
			@RequestParam("filter_propinsi") Optional<String> filter_propinsi,
			@RequestParam("filter_eselon1") Optional<String> filter_eselon1,
			@RequestParam("limit") Optional<Integer> limit, @RequestParam("page") Optional<Integer> page)
			throws Exception {
		// MessageResult resp = new MessageResult(Message.SUCCESS_CODE);

		QueryParameter param = new QueryParameter();
		// if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+
		// UnitAuditi.NAMA_UNIT_KERJA + " LIKE '%"+filter_keyword.get()+"%'");
		if (filter_nama.isPresent())
			param.setClause(
					param.getClause() + " AND " + UnitAuditi.NAMA_UNIT_AUDITI + " LIKE '%" + filter_nama.get() + "%'");
		if (filter_propinsi.isPresent())
			param.setClause(param.getClause() + " AND " + "nama_propinsi" + " LIKE '%" + filter_propinsi.get() + "%'");
		if (filter_eselon1.isPresent())
			param.setClause(param.getClause() + " AND " + "nama_eselon1" + " LIKE '%" + filter_eselon1.get() + "%'");
		
		if(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).size() > 0) {
			param.setClause(param.getClause() + " AND " + UnitAuditi.ID_UNIT_AUDITI + "='"+userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0)+"'");
		}
		
		if (limit.isPresent()) {
			param.setLimit(limit.get());
			if (limit.get() > 2000)
				param.setLimit(2000);
		} else {
			param.setLimit(2000);
		}

		int pPage = 1;
		if (page.isPresent()) {
			pPage = page.get();
			int offset = (pPage - 1) * param.getLimit();
			param.setOffset(offset);
		}

		ResponseWrapperList resp = new ResponseWrapperList();
		param.setOrder("nama_unit_auditi");
		List<UnitAuditi> data = unitAuditiMapper.getList(param);
		resp.setCount(unitAuditiMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size() + ((pPage - 1) * param.getLimit()) < resp.getCount());

		String qryString = "?page=" + (pPage + 1);
		if (limit.isPresent()) {
			qryString += "&limit=" + limit.get();
		}
		resp.setNextPageNumber(pPage + 1);
		resp.setNextPage(request.getRequestURL().toString() + qryString);

		return resp;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(@PathVariable String id) throws Exception {
		ResponseWrapper resp = new id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper();
		UnitAuditi data = unitAuditiMapper.getEntity(id);
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> save(
			@RequestParam("id") Optional<String> id,
			@RequestParam("kode") Optional<String> kode,
			@RequestParam("nama") Optional<String> nama,
			@RequestParam("id_propinsi") Optional<String> propinsi,
			@RequestParam("id_distrik") Optional<String> distrik,
			@RequestParam("kode_simpeg") Optional<String> kodeSimpeg,
			@RequestParam("id_eselon1") Optional<String> eselon1,
			@RequestParam("id_status_auditi") Optional<String> idStatusAuditi) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();

		UnitAuditi data = new UnitAuditi(String.valueOf(unitAuditiMapper.getNewId()));
		if (id.isPresent()) {
			data.setIdUnitAuditi(id.get());
			data = unitAuditiMapper.getEntity(id.get());
		}
		
		if (data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if(kode.isPresent()) data.setKodeUnitAuditi(kode.get());
		if(nama.isPresent()) data.setNamaUnitAuditi(nama.get());
		if(propinsi.isPresent()) data.setIdPropinsiUnitAuditi(propinsi.get());
		if(distrik.isPresent()) data.setIdDistrikUnitAuditi(distrik.get());
		if(eselon1.isPresent()) data.setIdEselon1UnitAuditi(eselon1.get());
		if(idStatusAuditi.isPresent()) data.setIdStatusAuditiUnitAuditi(idStatusAuditi.get());
		if(kodeSimpeg.isPresent()) data.setKodeSimpegUnitAuditi(kodeSimpeg.get());

		boolean pass = true;
		if(data.getNamaUnitAuditi() == null) {
			pass = false;
		}else {
			if (data.getNamaUnitAuditi().equals("")) {
				pass = false;
			}
		}

		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if (id.isPresent()) {
			unitAuditiMapper.update(data);
		} else {
			unitAuditiMapper.insert(data);
		}
		resp.setData(data);

		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/eselon1/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> saveEselon1(
			@RequestParam("id") Optional<String> id,
			@RequestParam("kode") Optional<String> kode,
			@RequestParam("nama") Optional<String> nama,
			@RequestParam("kode_simpeg") Optional<String> kodeSimpeg) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();

		Eselon1 data = new Eselon1(String.valueOf(eselon1Mapper.getNewId()));
		if (id.isPresent()) {
			data.setId(id.get());
			data = eselon1Mapper.getEntity(id.get());
		}
		
		if (data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if(kode.isPresent()) data.setKode(kode.get());
		if(nama.isPresent()) data.setNama(nama.get());
		if(kodeSimpeg.isPresent()) data.setKodeSimpeg(kodeSimpeg.get());

		boolean pass = true;
		if(data.getNama() == null) {
			pass = false;
		}else {
			if (data.getNama().equals("")) {
				pass = false;
			}
		}

		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if (id.isPresent()) {
			eselon1Mapper.update(data);
		} else {
			eselon1Mapper.insert(data);
		}
		resp.setData(data);

		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> delete(
			@PathVariable String id, 
			@RequestParam("delete-permanent") Optional<String> deletePermanent,
			HttpServletRequest request) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		UnitAuditi data = unitAuditiMapper.getEntity(id);
		resp.setData(data);

		if(deletePermanent.isPresent() || request.isUserInRole("DEVELOPER")) {
			unitAuditiMapper.delete(data);
			//System.out.println("PERMANENT DELETE");
		}else {
			//System.out.println("TEMP DELETE");
			unitAuditiMapper.deleteTemp(id, extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
		}
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/preview-import", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<ResponseWrapper> importSatker(
			@RequestParam("mode") Optional<String> mode,
			@RequestParam("file_excel") Optional<MultipartFile> fileExcel,
			@RequestParam("start_read_row") Optional<Integer> startReadRow,
			HttpServletRequest request,
			MultipartHttpServletRequest req
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		QueryParameter param = new QueryParameter();
		List<UnitAuditi> listUnitAUditi = new ArrayList<UnitAuditi>();
		
		if(fileExcel.isPresent() && !fileExcel.get().isEmpty()) {
			Iterator<String> itr = req.getFileNames();
			MultipartFile mpf = req.getFile(itr.next());

			InputStream file = mpf.getInputStream();

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet;
			for(int i=0; i < workbook.getNumberOfSheets();i++) {
				sheet = workbook.getSheetAt(i);

				String sheetname = sheet.getSheetName().replaceAll(" ", "");
				System.out.println(sheetname);
				/*if(unitKompetensiTeknisMapper.getList(param).size() == 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Kode "+sheetname+" Tidak valid.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}else {
					sheetid=unitKompetensiTeknisMapper.getList(param).get(0).getId();
				}*/
				
				Iterator<Row> rowIterator = sheet.iterator();
				int iRow = 0;
				int defaultRow = 2;
				if(startReadRow.isPresent()) {
					defaultRow = startReadRow.get()-2;
				}
				
				//for check duplicate
				String dupKdValue = null;
				String dupNmValue = null;
				ArrayList<String> tempKdList = new ArrayList<String>();
				ArrayList<String> tempNmList = new ArrayList<String>();

				ArrayList<String> dupKdSatList = new ArrayList<String>();
				ArrayList<String> dupNmSatList = new ArrayList<String>();
				ArrayList<String> dupDBKdSatList = new ArrayList<String>();
				ArrayList<String> dupDBNmSatList = new ArrayList<String>();

				ArrayList<String> errPropList = new ArrayList<String>();
				ArrayList<String> errDistList = new ArrayList<String>();
				ArrayList<String> errEselList = new ArrayList<String>();
				ArrayList<String> errStsAudList = new ArrayList<String>();
				
				while(rowIterator.hasNext()) {
					Row row = rowIterator.next();
					if(row.getRowNum() > defaultRow) {

						if(row.getCell(6).getCellType()==CellType.STRING) {
							resp.setCode(HttpStatus.BAD_REQUEST.value());
							resp.setMessage("Terdeteksi format String masalah pada row:"+(row.getRowNum()+1)+", pastikan format hanya berupa integer/numeric/angka.");
							return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
						}

						if(row.getCell(1)!=null && !row.getCell(1).getStringCellValue().equals("")) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+UnitAuditi.KODE_UNIT_AUDITI+" ='"+row.getCell(1).getStringCellValue()+"'");

							if(unitAuditiMapper.getList(param).size() > 0) {
								dupKdSatList.add(row.getCell(1).getStringCellValue());
							}

							dupKdValue = row.getCell(1).getStringCellValue();
					        if (tempKdList.contains(dupKdValue)){
					        	dupDBKdSatList.add(dupKdValue);
							} else {
								tempKdList.add(dupKdValue);
					        }
						}
						
						if(row.getCell(2)!=null) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+UnitAuditi.NAMA_UNIT_AUDITI+" ='"+row.getCell(2).getStringCellValue()+"'");

							if(unitAuditiMapper.getList(param).size() > 0) {
								dupNmSatList.add(row.getCell(2).getStringCellValue());
							}
							
							dupNmValue = row.getCell(2).getStringCellValue();
					        if (tempNmList.contains(dupNmValue)){
					        	dupDBNmSatList.add(dupNmValue);
							} else {
								tempNmList.add(dupNmValue);
					        }
					        
						}
						//this for propinsi/eselon1/statusAuditi
						if(row.getCell(3)!=null) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+"nama_propinsi"+" ='"+row.getCell(3).getStringCellValue()+"'");

							if(referenceMapper.getListPropinsi(param).size() == 0) {
								errPropList.add(row.getCell(3).getStringCellValue());
							}
						}

						if(row.getCell(4)!=null && !row.getCell(4).getStringCellValue().equals("NONE")) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+Distrik.NAMA+" ='"+row.getCell(4).getStringCellValue().trim()+"'");

							if(referenceMapper.getListDistrik(param).size() == 0) {
								errDistList.add(row.getCell(4).getStringCellValue());
							}
						}
						
						if(row.getCell(5)!=null) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+Eselon1.NAMA+" ='"+row.getCell(5).getStringCellValue()+"'");

							if(eselon1Mapper.getList(param).size() == 0) {
								errEselList.add(row.getCell(5).getStringCellValue());
							}
						}
						
					}
				}

				if(errPropList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Propinsi Belum terdaftar pada master database :"+Arrays.asList(errPropList).get(0));
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}

				if(errDistList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("<strong>Kota/Kabupaten Belum terdaftar pada master database :</strong><br/>"+Arrays.asList(errDistList).get(0));
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}

				if(errEselList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Eselon1 Belum terdaftar pada master database :"+Arrays.asList(errEselList).get(0));
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(dupKdSatList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada database, kode satker berikut :"+Arrays.asList(dupKdSatList).get(0));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(dupDBKdSatList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada row excel, kode satker berikut :<br/>"+Arrays.asList(dupDBKdSatList).get(0).toString().replaceAll(",", "<br/>"));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(dupNmSatList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada database, kode satker berikut :"+Arrays.asList(dupNmSatList).get(0));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				if(dupDBNmSatList.size() > 0) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Terjadi duplikat pada row excel, kode satker berikut :<br/>"+Arrays.asList(dupDBNmSatList).get(0).toString().replaceAll(",", "<br/>"));
					return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				
				
				//reinit the iterator, cause we use this twice
				rowIterator = sheet.iterator();
				
				while(rowIterator.hasNext()) {
					Row row = rowIterator.next();

					if(row.getRowNum() > defaultRow) {
						
						UnitAuditi unitAuditi = new UnitAuditi();
						unitAuditi.setKodeUnitAuditi((row.getCell(1)!=null?row.getCell(1).getStringCellValue():""));
						unitAuditi.setNamaUnitAuditi(row.getCell(2).getStringCellValue());


						param = new QueryParameter();
						param.setClause(param.getClause()+" AND "+"nama_propinsi"+" ='"+row.getCell(3).getStringCellValue()+"'");
						unitAuditi.setIdPropinsiUnitAuditi(referenceMapper.getListPropinsi(param).get(0).getIdPropinsi());
						unitAuditi.setNamaPropinsi(referenceMapper.getListPropinsi(param).get(0).getNamaPropinsi());
						
						if(row.getCell(4)!=null && !row.getCell(4).getStringCellValue().equals("NONE")) {
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+Distrik.NAMA+" ='"+row.getCell(4).getStringCellValue()+"'");
							unitAuditi.setIdDistrikUnitAuditi(referenceMapper.getListDistrik(param).get(0).getIdDistrik());
							unitAuditi.setNamaDistrik(referenceMapper.getListDistrik(param).get(0).getNamaDistrik());
						}
						
						param = new QueryParameter();
						param.setClause(param.getClause()+" AND "+Eselon1.NAMA+" ='"+row.getCell(5).getStringCellValue()+"'");
						unitAuditi.setIdEselon1UnitAuditi(eselon1Mapper.getList(param).get(0).getId());
						unitAuditi.setNamaEselon1(eselon1Mapper.getList(param).get(0).getNama());
						
						unitAuditi.setIdStatusAuditiUnitAuditi(row.getCell(6).getNumericCellValue()+"");
						unitAuditi.setKodeSimpegUnitAuditi((row.getCell(7)!=null?row.getCell(7).getStringCellValue():""));
						
						//find satker
		        		/*param = new QueryParameter();
						param.setClause(param.getClause()+" AND "+UnitAuditi.NAMA_UNIT_AUDITI+" ='"+row.getCell(4).getStringCellValue()+"'");
						String ids = unitAuditiMapper.getList(param).get(0).getIdUnitAuditi();*/
						
						listUnitAUditi.add(unitAuditi);
					}
					iRow++;
				}
			}
			
			System.out.println("before delete");
			System.out.println("listUnitAUditi size:"+listUnitAUditi.size());
			
			if(mode.isPresent() && mode.get().equals("save")) {
				for(UnitAuditi unitAuditi : listUnitAUditi) {
					unitAuditi.setUserAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
					unitAuditiMapper.insert(unitAuditi);
				}
				//resp.setData(listUnitAUditi);
				resp.setData("save");
			}else {
				resp.setData(listUnitAUditi);
			}
		}

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

}
