package id.go.menlhk.bp2sdm.spjk.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;

public class QueryBase extends SqlSessionDaoSupport {

	private String namespace;

	public QueryBase(SqlSessionFactory sqlSessionFactory, String namespace) {
		setSqlSessionFactory(sqlSessionFactory);
		this.namespace = namespace;
	}

	protected String getNamespace() {
		return namespace;
	}

	protected <T> List<T> list(String query, Class<T> returnType) {
		List<Clause> filter = new ArrayList<Clause>();
		int index = -1;
		int count = -1;
		String order = "";
		return list(query, returnType, filter, index, count, order);
	}

	protected <T> List<T> list(String query, Class<T> returnType, List<Clause> filter) {
		int index = -1;
		int count = -1;
		String order = "";
		return list(query, returnType, filter, index, count, order);
	}

	protected <T> List<T> list(String query, Class<T> returnType, List<Clause> filter, String order) {
		int index = -1;
		int count = -1;
		return list(query, returnType, filter, index, count, order);
	}

	protected <T> List<T> list(String query, Class<T> returnType, int index, int count) {
		List<Clause> filter = new ArrayList<Clause>();
		String order = "";
		return list(query, returnType, filter, index, count, order);
	}

	protected <T> List<T> list(String query, Class<T> returnType, int index, int count, String order) {
		List<Clause> filter = new ArrayList<Clause>();
		return list(query, returnType, filter, index, count, order);
	}

	protected <T> List<T> list(String query, Class<T> returnType, List<Clause> filter, int index, int count) {
		String order = "";
		return list(query, returnType, filter, index, count, order);
	}

	protected <T> List<T> list(String query, Class<T> returnType, List<Clause> filter, int index, int count,
			String order) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		String strLimit = "";
		String strOffset = "";
		if (index >= 0 && count >= 0) {
			strLimit = "LIMIT " + count;
			strOffset = "OFFSET " + index;
		}
		param.put("limit", strLimit); // number of rows to be display
		param.put("offset", strOffset); // start from row index to be display
		if (!order.equals(""))
			param.put("order", order);
		if (filter.size() > 0)
			param.put("filter", filter);

		MappedStatement ms = getSqlSession().getConfiguration().getMappedStatement(this.namespace + "." + query);
		if (ms.getResultMaps().size() > 0) {
			String mapId = ms.getResultMaps().get(0).getId();
			for (Clause c : filter) {
				String foundColumn = getColumnMapping(mapId, c.getColumn());
				if (foundColumn != null)
					c.setColumn(foundColumn);
			}
		}

		return getSqlSession().selectList(this.namespace + "." + query, param);
	}

	protected <T> List<T> listOrWithAndFilter(String query, Class<T> returnType, List<Clause> filterAnd,
			List<Clause> filterOr, int index, int count, String order) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		String strLimit = "";
		String strOffset = "";
		if (index >= 0 && count >= 0) {
			strLimit = "LIMIT " + count;
			strOffset = "OFFSET " + index;
		}
		param.put("limit", strLimit); // number of rows to be display
		param.put("offset", strOffset); // start from row index to be display
		if (!order.equals(""))
			param.put("order", order);
		if (filterOr.size() > 0) {
			param.put("filter", filterOr);
		}

		if (filterAnd.size() > 0) {
			param.put("filterAnd", filterAnd);
		}

		MappedStatement ms = getSqlSession().getConfiguration().getMappedStatement(this.namespace + "." + query);
		if (ms.getResultMaps().size() > 0) {
			String mapId = ms.getResultMaps().get(0).getId();
			for (Clause c : filterOr) {
				String foundColumn = getColumnMapping(mapId, c.getColumn());
				if (foundColumn != null)
					c.setColumn(foundColumn);
			}
		}

		return getSqlSession().selectList(this.namespace + "." + query, param);
	}

	protected void update(String query, Object parameter) {
		getSqlSession().update(getNamespace() + "." + query, parameter);
	}

	protected void update(String query) {
		getSqlSession().update(getNamespace() + "." + query);
	}

	protected void insert(String query, Object parameter) {
		getSqlSession().insert(getNamespace() + "." + query, parameter);
	}

	protected void insert(String query) {
		getSqlSession().insert(getNamespace() + "." + query);
	}

	protected void delete(String query, Object parameter) {
		getSqlSession().delete(getNamespace() + "." + query, parameter);
	}

	protected void delete(String query) {
		getSqlSession().delete(getNamespace() + "." + query);
	}

	private String getColumnMapping(String mapId, String property) {
		ResultMap rm = getSqlSession().getConfiguration().getResultMap(mapId);
		for (ResultMapping o : rm.getPropertyResultMappings()) {
			if (property.equals(o.getProperty()))
				return o.getColumn();
		}
		return null;
	}

}
