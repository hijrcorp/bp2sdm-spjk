package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonProperty;

public class JabatanManajerial {

	public static final String ID = "id_jabatan_manajerial";
	public static final String ID_MASTER_JABATAN = "id_master_jabatan_jabatan_manajerial";
	public static final String ID_KELOMPOK_MANAJERIAL = "id_kelompok_manajerial_jabatan_manajerial";
	public static final String NILAI_MINIMUM = "nilai_minimum_jabatan_manajerial";
	public static final String JENIS = "jenis_jabatan_manajerial";
	public static final String ID_SOURCE = "id_source_jabatan_manajerial";
	public static final String ID_ACCOUNT_ADDED = "id_account_added_jabatan_manajerial";
	public static final String TIMESTAMP_ADDED = "timestamp_added_jabatan_manajerial";
	public static final String ID_ACCOUNT_MODIFIED = "id_account_modified_jabatan_manajerial";
	public static final String TIMESTAMP_MODIFIED = "timestamp_modified_jabatan_manajerial";

	private String id;
	private String idMasterJabatan;
	private String idKelompokManajerial;
	private Integer nilaiMinimum;
	private String jenis;
	private String idSource;
	private String idAccountAdded;
	private Date timestampAdded;
	private String idAccountModified;
	private Date timestampModified;

	public JabatanManajerial() {

	}

	public JabatanManajerial(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_master_jabatan")
	public String getIdMasterJabatan() {
		return idMasterJabatan;
	}

	public void setIdMasterJabatan(String idMasterJabatan) {
		this.idMasterJabatan = idMasterJabatan;
	}

	@JsonProperty("id_kelompok_manajerial")
	public String getIdKelompokManajerial() {
		return idKelompokManajerial;
	}

	public void setIdKelompokManajerial(String idKelompokManajerial) {
		this.idKelompokManajerial = idKelompokManajerial;
	}

	@JsonProperty("nilai_minimum")
	public Integer getNilaiMinimum() {
		return nilaiMinimum;
	}

	public void setNilaiMinimum(Integer nilaiMinimum) {
		this.nilaiMinimum = nilaiMinimum;
	}

	@JsonProperty("jenis")
	public String getJenis() {
		return jenis;
	}
	
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	@JsonProperty("id_account_added")
	public String getIdAccountAdded() {
		return idAccountAdded;
	}

	public void setIdAccountAdded(String idAccountAdded) {
		this.idAccountAdded = idAccountAdded;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}

	@JsonProperty("id_account_modified")
	public String getIdAccountModified() {
		return idAccountModified;
	}

	public void setIdAccountModified(String idAccountModified) {
		this.idAccountModified = idAccountModified;
	}

	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}

	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}
	
	/* costume */
	private String idKelompokManajerialUnitKomp;
	private String idUnitKomp;
	private String kodeUnitKomp;
	private String judulUnitKomp;

	private Integer jumlahPertanyaan;

	public String getIdKelompokManajerialUnitKomp() {
		return idKelompokManajerialUnitKomp;
	}

	public void setIdKelompokManajerialUnitKomp(String idKelompokManajerialUnitKomp) {
		this.idKelompokManajerialUnitKomp = idKelompokManajerialUnitKomp;
	}

	public String getIdUnitKomp() {
		return idUnitKomp;
	}

	public void setIdUnitKomp(String idUnitKomp) {
		this.idUnitKomp = idUnitKomp;
	}

	public String getKodeUnitKomp() {
		return kodeUnitKomp;
	}

	public void setKodeUnitKomp(String kodeUnitKomp) {
		this.kodeUnitKomp = kodeUnitKomp;
	}

	public String getJudulUnitKomp() {
		return judulUnitKomp;
	}

	public void setJudulUnitKomp(String judulUnitKomp) {
		this.judulUnitKomp = judulUnitKomp;
	}
	


	@JsonProperty("jumlah_pertanyaan")
	public Integer getJumlahPertanyaan() {
		return jumlahPertanyaan;
	}

	public void setJumlahPertanyaan(Integer jumlahPertanyaan) {
		this.jumlahPertanyaan = jumlahPertanyaan;
	}
	
}
