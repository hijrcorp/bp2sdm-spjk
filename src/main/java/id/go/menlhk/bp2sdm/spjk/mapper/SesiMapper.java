package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Sesi;

public interface SesiMapper {

	@Insert("INSERT INTO spjk_sesi (id_sesi, kode_sesi, tahun_sesi, periode_awal_sesi, periode_akhir_sesi, keterangan_sesi, sumber_sesi, timestamp_added_sesi) VALUES (#{id:VARCHAR}, #{kode:VARCHAR}, #{tahun:NUMERIC}, #{periodeAwal:DATE}, #{periodeAkhir:DATE}, #{keterangan:VARCHAR}, #{sumber:VARCHAR}, NOW())")
	void insert(Sesi sesi);

	@Update("UPDATE spjk_sesi SET id_sesi=#{id:VARCHAR}, kode_sesi=#{kode:VARCHAR}, tahun_sesi=#{tahun:NUMERIC}, periode_awal_sesi=#{periodeAwal:DATE}, periode_akhir_sesi=#{periodeAkhir:DATE}, keterangan_sesi=#{keterangan:VARCHAR}, sumber_sesi=#{sumber:VARCHAR}, timestamp_modified_sesi=NOW() WHERE id_sesi=#{id}")
	void update(Sesi sesi);


	@Update("UPDATE spjk_sesi SET sumber_sesi=#{sumber:VARCHAR}, timestamp_modified_sesi=NOW() WHERE id_sesi=#{id}")
	void updateTimeSumber(Sesi sesi);
	
	@Update("UPDATE spjk_sesi SET sumber_sesi=#{sumber:VARCHAR} WHERE id_sesi=#{id}")
	void updateSumber(Sesi sesi);

	@Delete("DELETE FROM spjk_sesi WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM spjk_sesi WHERE id_sesi=#{id}")
	void delete(Sesi sesi);

	List<Sesi> getList(QueryParameter param);

	Sesi getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

	//@Delete("INSERT INTO survey_sesi SELECT * FROM survey_sesi_test")
	//void copyTest();

	List<Sesi> getListHasilUjian(QueryParameter param);

	Sesi getEntityActive();

	Sesi getEntityByKodeSesi(String kodeSesi);

	Sesi getCekSesiStatus(String kodeSesi);
}
