package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.model.AccountGroup;

@Mapper
public interface AccountGroupMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_account_group (id_account_group, name_account_group, description_account_group, application_id_account_group, sequence_account_group) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{description:VARCHAR}, #{applicationId:VARCHAR}, #{sequence:NUMERIC})")
	void insert(AccountGroup accountGroup);

	@Update("UPDATE hijr_account_group SET id_account_group=#{id:VARCHAR}, name_account_group=#{name:VARCHAR}, description_account_group=#{description:VARCHAR}, application_id_account_group=#{applicationId:VARCHAR}, sequence_account_group=#{sequence:NUMERIC} WHERE id_account_group=#{id}")
	void update(AccountGroup accountGroup);

	@Delete("DELETE FROM hijr_account_group WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_account_group WHERE id_account_group=#{id}")
	void delete(AccountGroup accountGroup);

	List<AccountGroup> getList(QueryParameter param);

	AccountGroup getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
