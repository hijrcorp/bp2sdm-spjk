package id.go.menlhk.bp2sdm.spjk.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.go.menlhk.bp2sdm.spjk.core.JsonDateSerializer;
import id.go.menlhk.bp2sdm.spjk.model.Account;


public class Sesi {
	public static final String ID = "id_sesi";
	public static final String KODE = "kode_sesi";
	public static final String TAHUN = "tahun_sesi";
	public static final String PERIODE_AWAL = "periode_awal_sesi";
	public static final String PERIODE_AKHIR = "periode_akhir_sesi";
	public static final String KETERANGAN = "keterangan_sesi";
	//public static final String JUMLAH_RESPONDEN = "jumlah_responden_sesi";
	public static final String SUMBER = "sumber_sesi";
	public static final String TIMESTAMPADDED = "timestamp_added_sesi";
	public static final String TIMESTAMPMODIFIED = "timestamp_modified_sesi";

	private String id;
	private String kode;
	private Integer tahun;
	private Date periodeAwal;
	private Date periodeAkhir;
	private String keterangan;
	//private Integer jumlahResponden;
	private String sumber;
	private Date timestampAdded;
	private Date timestampModified;

	public Sesi() {

	}

	public Sesi(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("kode")
	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	@JsonProperty("tahun")
	public Integer getTahun() {
		return tahun;
	}

	public void setTahun(Integer tahun) {
		this.tahun = tahun;
	}


	@JsonSerialize(using=JsonDateSerializer.class)
	@JsonProperty("periode_awal_serialize")
	public Date getPeriodeAwalSerialize() {
		return periodeAwal;
	}
	@JsonProperty("periode_awal")
	public Date getPeriodeAwal() {
		return periodeAwal;
	}

	public void setPeriodeAwal(Date periodeAwal) {
		this.periodeAwal = periodeAwal;
	}

	@JsonSerialize(using=JsonDateSerializer.class)
	@JsonProperty("periode_akhir_serialize")
	public Date getPeriodeAkhirSerialize() {
		return periodeAkhir;
	}
	
	@JsonProperty("periode_akhir")
	public Date getPeriodeAkhir() {
		return periodeAkhir;
	}

	public void setPeriodeAkhir(Date periodeAkhir) {
		this.periodeAkhir = periodeAkhir;
	}

	@JsonProperty("keterangan")
	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@JsonProperty("sumber")
	public String getSumber() {
		return sumber;
	}

	public void setSumber(String sumber) {
		this.sumber = sumber;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}
	
	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}

	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}


	/**************************************************************/

	private Integer expiredSesi;
	private Integer jumlahHari;
	private Integer active;
	private List<User> account = new ArrayList<User>();
	private Integer intervalTime;

	@JsonProperty("expired")
	public Integer getExpiredSesi() {
		return expiredSesi;
	}

	public void setExpiredSesi(Integer expiredSesi) {
		this.expiredSesi = expiredSesi;
	}
	
	@JsonProperty("jumlah_hari")
	public Integer getJumlahHari() {
		return jumlahHari;
	}

	public void setJumlahHari(Integer jumlahHari) {
		this.jumlahHari = jumlahHari;
	}

	@JsonProperty("active")
	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public List<User> getAccount() {
		return account;
	}

	public void setAccount(List<User> account) {
		this.account = account;
	}

	//added this in 2022 for handle generate time
	public Integer getIntervalTime() {
		return intervalTime;
	}

	public void setIntervalTime(Integer intervalTime) {
		this.intervalTime = intervalTime;
	}
}
