package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import id.go.menlhk.bp2sdm.spjk.domain.StatusChanges;

@Mapper
public interface StatusMapper {

	@Insert("insert into tbl_status_changes (id, status_name, created_time, reference_id, reference_name) values "
			+ "(#{id}, #{statusName}, #{createdTime}, #{referenceId}, #{referenceName})")
	void insert(StatusChanges statusChanges);

	@Select("select * from tbl_status_changes where reference_id=#{referenceId} and reference_name=#{referenceName} order by created_time desc")
	List<StatusChanges> findListStatus(@Param("referenceId") String referenceId,
			@Param("referenceName") String referenceName);

}
