package id.go.menlhk.bp2sdm.spjk.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.go.menlhk.bp2sdm.spjk.domain.bc.OauthUser;

@RestController
public class SampleController {

	@RequestMapping(value = "/test/get", method = RequestMethod.GET)
	public OauthUser test() {
		return new OauthUser();
	}

	@RequestMapping(value = "/rest/get", method = RequestMethod.GET)
	public Authentication sampleGet() {
		OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();

		Authentication userAuthentication = oAuth2Authentication.getUserAuthentication();

		return userAuthentication;
	}
}
