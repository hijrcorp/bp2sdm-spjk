package id.go.menlhk.bp2sdm.spjk.core;

public class NewMessageResult extends Message {
	private long count;
	private boolean nextMore = false;
	private int nextPage = 1;
	private String nextPageUrl = "";
	private int limit;
	private int activePage;
	private int pageCount;

	public NewMessageResult(String message) {
		super(message);
	}

	public NewMessageResult(String message, int code) {
		super(message, code);
	}

	public NewMessageResult(int code) {
		super(code);
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public boolean isNextMore() {
		return nextMore;
	}

	public void setNextMore(boolean nextMore) {
		this.nextMore = nextMore;
	}

	public int getNextPage() {
		return nextPage;
	}

	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	public String getNextPageUrl() {
		return nextPageUrl;
	}

	public void setNextPageUrl(String nextPageUrl) {
		this.nextPageUrl = nextPageUrl;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getActivePage() {
		return activePage;
	}

	public void setActivePage(int activePage) {
		this.activePage = activePage;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
}
