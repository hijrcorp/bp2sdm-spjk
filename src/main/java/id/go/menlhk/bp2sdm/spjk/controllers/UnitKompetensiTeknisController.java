package id.go.menlhk.bp2sdm.spjk.controllers;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import id.go.menlhk.bp2sdm.spjk.core.Clause;
import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.core.StyleExcel;
import id.go.menlhk.bp2sdm.spjk.domain.MappingBidangUnitKompetensi;
import id.go.menlhk.bp2sdm.spjk.domain.MappingUnitKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.Pertanyaan;
import id.go.menlhk.bp2sdm.spjk.domain.PertanyaanJawaban;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensi;
import id.go.menlhk.bp2sdm.spjk.domain.UnitKompetensiTeknis;
import id.go.menlhk.bp2sdm.spjk.domain.bc.Pernyataan;
import id.go.menlhk.bp2sdm.spjk.domain.bc.PernyataanJawaban;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.helper.Utils;
import id.go.menlhk.bp2sdm.spjk.mapper.ConfigMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingBidangUnitKompetensiMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.MappingUnitKompetensiTeknisMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PernyataanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PernyataanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanJawabanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.PertanyaanMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.ReferenceMapper;
import id.go.menlhk.bp2sdm.spjk.mapper.UnitKompetensiTeknisMapper;

@RestController
@RequestMapping("/unit-kompetensi-teknis")
public class UnitKompetensiTeknisController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	private UnitKompetensiTeknisMapper unitKompetensiTeknisMapper;
	
	@Autowired
	private MappingBidangUnitKompetensiMapper mappingBidangUnitKompetensiMapper;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper> save(
			RestQLController controller,
			@RequestParam("id") Optional<String> id,
			@RequestParam("id_mapping") Optional<String> idMapping,
			@RequestParam("id_kelompok_jabatan") Optional<String> idKelompokJabatan,
			@RequestParam("id_bidang_teknis") Optional<String> idBidangTeknis,
			@RequestParam("kode") Optional<String> kode,
			@RequestParam("judul") Optional<String> judul,HttpServletRequest request

	) throws Exception {

		ResponseWrapper resp = new ResponseWrapper();

		UnitKompetensiTeknis data = new UnitKompetensiTeknis(Utils.getLongNumberID());
		if (id.isPresent()) {
			data.setId(id.get());
			data = unitKompetensiTeknisMapper.getEntity(id.get());
		}
		if (data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		if (kode.isPresent()) data.setKode(kode.get());
		if (judul.isPresent()) data.setJudul(judul.get());
		if (idKelompokJabatan.isPresent()) data.setIdKelompokJabatan(idKelompokJabatan.get());
		
		boolean pass = true;
		if (data.getKode() == null && data.getJudul() == null && data.getIdKelompokJabatan() == null) {
			pass = false;
		} else {
			if (data.getKode().equals("") && data.getJudul().equals("") && data.getIdKelompokJabatan().equals("")) {
				pass = false;
			}
		}

		if (!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if (id.isPresent()) {
			unitKompetensiTeknisMapper.update(data);
		} else {
			unitKompetensiTeknisMapper.insert(data);
		}
		//run everytime
		/*QueryParameter param = new QueryParameter();
		UnitKompetensiTeknis prevData = unitKompetensiTeknisMapper.getEntity(id.get());
		param.setClause(param.getClause()+" AND "+MappingBidangUnitKompetensi.ID_KELOMPOK_JABATAN+" ='"+prevData.getIdKelompokJabatan()+"'");
		param.setClause(param.getClause()+" AND "+MappingBidangUnitKompetensi.ID_BIDANG_TEKNIS+" ='"+idBidangTeknis.get()+"'");
		param.setClause(param.getClause()+" AND "+MappingBidangUnitKompetensi.ID_UNIT_KOMPETENSI+" ='"+data.getId()+"'");
		mappingBidangUnitKompetensiMapper.deleteBatch(param);
		*/
		MappingBidangUnitKompetensi o = new MappingBidangUnitKompetensi(Utils.getLongNumberID());
		if(idMapping.isPresent() && !idMapping.get().equals("")) {
			o.setId(idMapping.get());
			o = mappingBidangUnitKompetensiMapper.getEntity(idMapping.get());
		}
		if (o == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		o.setIdKelompokJabatan(data.getIdKelompokJabatan());
		o.setIdUnitKompetensi(data.getId());
		o.setIdBidangTeknis(idBidangTeknis.get());
		if(idMapping.isPresent() && !idMapping.get().equals("")) {
			mappingBidangUnitKompetensiMapper.update(o);
		}else {
			mappingBidangUnitKompetensiMapper.insert(o);
		}
		resp.setData(data);
		return new ResponseEntity<id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

}
