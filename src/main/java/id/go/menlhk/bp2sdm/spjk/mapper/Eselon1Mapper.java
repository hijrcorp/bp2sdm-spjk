package id.go.menlhk.bp2sdm.spjk.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.Eselon1;

public interface Eselon1Mapper {

	@Insert("INSERT INTO tbl_eselon1 (eselon1_id, kode_eselon1, nama_eselon1, kode_simpeg_eselon1) VALUES (#{id:VARCHAR}, #{kode:VARCHAR}, #{nama:VARCHAR}, #{kodeSimpeg:VARCHAR})")
	void insert(Eselon1 eselon1);

	@Update("UPDATE tbl_eselon1 SET kode_eselon1=#{kode:VARCHAR}, nama_eselon1=#{nama:VARCHAR}, kode_simpeg_eselon1=#{kodeSimpeg:VARCHAR} WHERE eselon1_id=#{id}")
	void update(Eselon1 eselon1);

	@Delete("DELETE FROM tbl_eselon1 WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_eselon1 WHERE eselon1_id=#{id}")
	void delete(Eselon1 eselon1);

	List<Eselon1> getList(QueryParameter param);

	Eselon1 getEntity(String id);

	long getCount(QueryParameter param);

	int getNewId();

	//costume
	@Update("UPDATE spjk_eselon1 SET id_account_deleted_eselon1=#{userId}, timestamp_deleted_eselon1=now() WHERE id_eselon1=#{idEselon1}")
	void deleteTemp(@Param("idEselon1") String idEselon1, @Param("userId") String userId);
}
