package id.go.menlhk.bp2sdm.spjk.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.go.menlhk.bp2sdm.spjk.core.QueryParameter;
import id.go.menlhk.bp2sdm.spjk.domain.SimpegJabatan;


public interface SimpegJabatanMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO simpeg_jabatan (kode, jabatan, eselon, kelompok_jabatan) VALUES (#{kode:VARCHAR}, #{jabatan:VARCHAR}, #{eselon:VARCHAR}, #{kelompokJabatan:VARCHAR})")
	void insert(SimpegJabatan jabatan);
	
	@Update("UPDATE simpeg_jabatan SET kode=#{kode:VARCHAR}, jabatan=#{jabatan:VARCHAR}, eselon=#{eselon:VARCHAR}, kelompok_jabatan=#{kelompokJabatan:VARCHAR} WHERE id_jabatan=#{idJabatan}")
	void update(SimpegJabatan jabatan);
	
	@Delete("DELETE FROM simpeg_jabatan WHERE ${clause}")
	void deleteBatch(QueryParameter param);
	
	@Delete("DELETE FROM simpeg_jabatan WHERE id_jabatan=#{idJabatan}")
	void delete(SimpegJabatan jabatan);
	
	@Delete("INSERT INTO simpeg_jabatan SELECT * FROM simpeg_jabatan_test")
	void copyTest();
	
	List<SimpegJabatan> getList(QueryParameter param);
	
	SimpegJabatan getEntity(String id);
	
	long getCount(QueryParameter param);
	
	int getNewId();
	
	/********************************** - End Generate - ************************************/
	
	@Select("SELECT kode FROM simpeg_eselon WHERE eselon=#{namaEselon}")
	String getKodeEselonSimpeg(String namaEselon);

}
