package id.go.menlhk.bp2sdm.spjk.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.go.menlhk.bp2sdm.spjk.domain.RataRataIndividuExisting;
import id.go.menlhk.bp2sdm.spjk.domain.RataRataKompetensiIndividu;
import id.go.menlhk.bp2sdm.spjk.domain.UnitAuditi;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapper;
import id.go.menlhk.bp2sdm.spjk.helper.ResponseWrapperList;
import id.go.menlhk.bp2sdm.spjk.mapper.SqlMapper;
import id.go.menlhk.bp2sdm.spjk.ref.AccountLoginInfo;
import id.go.menlhk.bp2sdm.spjk.services.RestQLTriggerService;


@Controller
@CrossOrigin
@RequestMapping(value = {"/restql/","/restql/me","/restql/me_costume","/restql/header"})
public class RestQLController extends BaseController {
	
	private String prefix_entity = "spjk_";
	
	@Autowired
	private SqlMapper sqlMapper;
	
	@Autowired
	private RestQLTriggerService triggerService;
	
	private Map<String, Object> data;
	
	private Map<String, String[]> dataMain;
	
	@RequestMapping(value="/{entity}", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@PathVariable String entity,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter") Optional<String> filter,
    		@RequestParam("filter_funct") Optional<String> filterFunct,
    		@RequestParam("select") Optional<String> select,
    		@RequestParam("select_funct") Optional<String> selectFunct,
    		@RequestParam("join") Optional<String> join,
    		@RequestParam("order") Optional<String> order,
    		HttpServletRequest request
    		)throws Exception {
		
		ResponseWrapperList resp = new ResponseWrapperList();
		
		List<String> lstFld = new ArrayList<String>();
		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
			
			String sqlCheck = "SELECT entity_permission, join_entity(entity_permission, fields_permission) entity_fields_permission, role_permission FROM hijr_permission WHERE mode_permission='HIDDEN'";
			
			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
			
			for(Map<String, Object> rsCheck: lstRsCheck) {
				if(request.isUserInRole(rsCheck.get("role_permission").toString()) || rsCheck.get("role_permission").toString().equals("*")){
					String[] flds = rsCheck.get("entity_fields_permission").toString().split(",");
//					System.out.println(rsCheck.get("entity_fields_permission").toString());
					for(String f: flds) {
						lstFld.add(f);
					}
					
				}
			}
		}
		
		StringBuilder sql = construct(entity, limit, page, filter, select, join, selectFunct, filterFunct);
		
		if(!request.isUserInRole("ROLE_SPJK_RESPONDEN") && !request.isUserInRole("SUPER_ADMIN") && !request.isUserInRole("ROLE_SPJK_SATKER") && !request.isUserInRole("ROLE_SPJK_ESELON1") && !request.isUserInRole("ROLE_SPJK_BDLHK") && !request.isUserInRole("ROLE_SPJK_PUSDIKLAT") && !request.isUserInRole("ROLE_SPJK_UNIT_PEMBINA")) {
			//if(!request.isUserInRole("ROLE_SPJK_RESPONDEN")){
			//}else {
				sql.append(" AND id_source_").append(entity);
				sql.append(" IN ('").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("', '*')");
			//}
		}
		if(request.getRequestURI().contains("/me/")) {
			sql.append(" AND id_account_").append(entity);
			sql.append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");	
		}
		
		if(request.getRequestURI().contains("/header/")) {
			sql.append(" AND id_header_").append(entity);
			sql.append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");	
		}
		
		if(request.isUserInRole("ROLE_SPJK_SATKER")) {
			//check entity name if same in this function, so give return;
			String idAccount = extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			triggerService.onGetUnitAuditiByUser(entity, idAccount, sql);
		}
		
		if(request.isUserInRole("ROLE_SPJK_ESELON1")) {
			//check entity name if same in this function, so give return;
			String idAccount = extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			triggerService.onGetEselon1ByUser(entity, idAccount, sql);
		}
		
		if(request.isUserInRole("ROLE_SPJK_BDLHK")) {
			//check entity name if same in this function, so give return;
			String idAccount = extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			triggerService.onGetOrganisasiByUser(entity, idAccount, sql, filter);
		}
		
		if(request.isUserInRole("ROLE_SPJK_UNIT_PEMBINA")) {
			String idAccount = extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			triggerService.onGetUnitPembinaByUser(entity, idAccount, sql, filter);
		}
		
		if(request.isUserInRole("ROLE_SPJK_RESPONDEN")) {
			String idAccount = extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			triggerService.onGetRespondenByUser(entity, idAccount, sql, filter);
		}
		
		if(order.isPresent()) {
			sql.append(" ORDER BY ");
			StringBuilder orderstr = new StringBuilder();
			
			String[] ordercol = order.get().split(",");
			for(String c: ordercol) {
				String[] ca =  c.split("\\.");
				
				if(ca.length > 1) {
					orderstr.append(",").append(ca[0].toLowerCase()).append("_").append(entity).append(" ").append(ca[1].toUpperCase());
				}else {
					orderstr.append(",").append(c.toLowerCase()).append("_").append(entity).append(" ASC");	
				}
				
			}
			sql.append(orderstr.substring(1));
		}
		
		int pLimit = 50;
		int pOffset = 0;
		
		if(limit.isPresent()) {
			if(limit.get() > 100) {
				pLimit = limit.get(); //100?
			}else {
				pLimit = limit.get();
			}
		}
		
		int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		pOffset = (pPage-1)*pLimit;
	    	}
	    	
	    	StringBuilder qry = new StringBuilder();
	    	qry.append(sql).append(" LIMIT ").append(pLimit);
	    	qry.append(" OFFSET ").append(pOffset);
	    	
//	    	String qry = sql + " LIMIT " + pLimit;
//	    	qry += " OFFSET " + pOffset;

	    	
		resp.setNextPageNumber(pPage+1);
		
		String qryString =	request.getQueryString()==null?"":request.getQueryString();
	    	if(!qryString.contains("page=")) {
	    		qryString += "&page=2";
	    	}else {
	    		qryString = qryString.replace("page="+pPage, "page="+resp.getNextPageNumber());	    		
	    	}
		resp.setNextPage(request.getRequestURL().append("?").append(qryString).toString());
		
		
		//System.out.println(sql);
		
		List<Map<String, Object>> listrs = sqlMapper.select(qry.toString());
		
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		
		
		for(Map<String, Object> rs: listrs) {
			String[] joinent = {};
			
			if(join.isPresent()) {
				joinent = join.get().split(",(?=[^\\)]*(\\(|$))");
			}
			data.add(remap(rs, entity, joinent, lstFld));
		}
		
		resp.setData(data);
		resp.setCount(sqlMapper.count(sql.toString()));
		
		resp.setNextMore(data.size()+((pPage-1)*pLimit) < resp.getCount());
		
		if(!resp.isNextMore()) {
			resp.setNextPage("#");
			resp.setNextPageNumber(-1);
		}
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{entity}/{_id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getOne(
			@PathVariable String entity,
			@PathVariable Optional<String> _id,
			@RequestParam("select") Optional<String> select,
    			@RequestParam("join") Optional<String> join,
    			@RequestParam("fetch") Optional<String> fetch,
    			@RequestParam("action") Optional<String> action,
			HttpServletRequest request
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		List<String> lstFld = new ArrayList<String>();
		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
			
			String sqlCheck = "SELECT entity_permission, join_entity(entity_permission, fields_permission) entity_fields_permission, role_permission FROM hijr_permission WHERE mode_permission='HIDDEN'";
			
			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
			
			for(Map<String, Object> rsCheck: lstRsCheck) {
				if(request.isUserInRole(rsCheck.get("role_permission").toString()) || rsCheck.get("role_permission").toString().equals("*")){
					String[] flds = rsCheck.get("entity_fields_permission").toString().split(",");
//					System.out.println(rsCheck.get("entity_fields_permission").toString());
					for(String f: flds) {
						lstFld.add(f);
					}
					
				}
			}
		}

		if(action.isPresent() && action.get().equals("delete")) resp.setMessage("Apakah anda yakin akan mengapus record ini?");
		resp.setData(selectOne(entity, _id, select, join, fetch, lstFld, request));
		
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRES_NEW)
	private HashMap<String, Object> selectOne(
			String entity,
			Optional<String> _id,
			Optional<String> select,
    			Optional<String> join,
			Optional<String> fetch,
			List<String> lstFld,
			HttpServletRequest request
			) throws Exception {
		
		//System.out.println(join);
		StringBuilder sql = construct(entity, Optional.empty(), Optional.empty(), Optional.empty(), select, join);
		
		if(request.getRequestURI().contains("/me/")) {
			
			sql.append(" AND id_account_").append(entity);
			sql.append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");	
		}else if(request.getRequestURI().contains("/me_costume/")) {
			String idAccount = extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			
			Optional<String> filter=Optional.ofNullable(request.getParameter("filter"));
			triggerService.onGetIdRespondenByUser(entity, sql, idAccount, filter);
			System.out.println("sql:"+sql);
		}else {
			if(_id.isPresent()) {
				sql.append(" AND id_").append(entity).append("='").append(_id.get()).append("'");
			}else {
				sql.append(" AND id_").append(entity).append("=''");
			}
		}
		if(request.getRequestURI().contains("/me_costume/")) {
			
		}else {
			if(!request.isUserInRole("SUPER_ADMIN")) {
				sql.append(" AND id_source_").append(entity);
				sql.append(" IN ('").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("', '*')");
			}
		}
		
		String id = "";
		
		List<Map<String, Object>> lst = sqlMapper.select(sql.toString());
		
		if(!lst.isEmpty()) {
		
			Map<String, Object> rs = lst.get(0);
			
			id = rs.get("id_"+entity)==null?"":rs.get("id_"+entity).toString();
			
			String[] joinent = {};
			
			if(join.isPresent() && !join.get().equals("")) {
				joinent = join.get().split(",(?=[^\\)]*(\\(|$))");
			}
			HashMap<String, Object> data = remap(rs, entity, joinent, lstFld);
			
			
			
			if(fetch.isPresent() && !fetch.get().equals("")) {
				String[] ent = fetch.get().split(",(?=[^\\)]*(\\(|$))");
				for(String e: ent) {
//					String[] fe =  e.split("\\.");
					String[] fe =  e.split("\\.(?=[^\\)]*(\\(|$))");
					boolean isOrder = false;
					
					Optional<String> fetchSelect = Optional.empty();
					String limit = "100";
					
					StringBuilder orderstr = new StringBuilder();
					
					if(fe.length > 1) {
						for(int i=1; i < fe.length; i++) {
							if(fe[i].indexOf("select") >= 0) {
								fetchSelect = Optional.of(fe[i].substring(7, fe[i].length()-1));		
							}
							
							if(fe[i].indexOf("limit") >= 0) {
								limit = fe[i].substring(6, fe[i].length()-1);		
							}
							
							if(fe[i].indexOf("order") >= 0) {
								isOrder = true;
								System.out.println(fe[i]);
								String[] ordercol = fe[i].substring(6, fe[i].length()-1).split(",");
								for(String c: ordercol) {
									String[] ca =  c.split("\\.");
									
									if(ca.length > 1) {
										orderstr.append(",").append(ca[0].toLowerCase()).append("_").append(fe[0]).append(" ").append(ca[1].toUpperCase());
									}else {
										orderstr.append(",").append(c.toLowerCase()).append("_").append(fe[0]).append(" ASC");	
									}
									
								}
								
							}
						}
					}
				
					StringBuilder sqlFetch = construct(fe[0], Optional.empty(), Optional.empty(), Optional.empty(), fetchSelect, Optional.empty());
					sqlFetch.append(" AND id_").append(entity).append("_").append(fe[0]).append(" = '").append(id).append("'");
					if(isOrder) sqlFetch.append(" ORDER BY ").append(orderstr.substring(1));
					sqlFetch.append(" LIMIT ").append(limit);
					
					
//					sqlFetch += " AND id_"+entity+"_"+fe[0]+" = '"+id+"'";
//					sqlFetch += " LIMIT " + limit;
					
//					System.out.println(sqlFetch);
					List<Map<String, Object>> listrs = sqlMapper.select(sqlFetch.toString());
					
					List<Map<String, Object>> newlistrs = new ArrayList<Map<String, Object>>();
					
					
					for(Map<String, Object> rsFetch: listrs) {
						
						newlistrs.add(remap(rsFetch, fe[0], new String[]{}, lstFld));
					}
					
					
					data.put(fe[0], newlistrs);
				}
					
			}
			
			return data;
			
		}
		
		return new HashMap<>();
	}
	
    @RequestMapping(value="/{entity}/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@PathVariable String entity,
    		@RequestBody RestQLController controller,
    		@RequestParam("append") Optional<String> append,
    		@RequestParam("mode") Optional<String> mode,
    		@RequestParam("join") Optional<String> join,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		//List<String> lstFld = new ArrayList<String>();
    		List<String> lstEnt = new ArrayList<String>();
    		List<String> lstDel = new ArrayList<String>();
    		
    		Map<String, List<String>> mapFld = new HashMap<String, List<String>>();
    		
    		boolean isAdmin = true;
    		
    		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
    			
    			isAdmin = false;
    			
    			//String sqlCheck = "SELECT entity_permission, fields_permission, role_permission FROM hijr_permission WHERE mode_permission='WRITE'";
    			String sqlCheck = "SELECT entity_permission, join_entity(entity_permission, fields_permission) entity_fields_permission, role_permission FROM hijr_permission WHERE mode_permission='WRITE'";
    			
    			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
    			
    			for(Map<String, Object> rsCheck: lstRsCheck) {
    				if(request.isUserInRole(rsCheck.get("role_permission").toString())){
    					/*
    					String[] flds = rsCheck.get("fields_permission").toString().split(",");
    					lstFld.addAll(Arrays.asList(flds));
    					lstEnt.add(rsCheck.get("entity_permission").toString());
    					*/
    					String[] flds = rsCheck.get("entity_fields_permission").toString().split(",");
    					for(String f: flds) {
    						if(f.indexOf("=") >= 0) {
    							String[] fs = f.split("=");
    							
    							if(mapFld.containsKey(fs[0])) {
    								mapFld.get(fs[0]).add(fs[1]);
    							}else {
    								List<String> lv = new ArrayList<String>();
    								lv.add(fs[1]);
    								mapFld.put(fs[0], lv);
    							}
    							
    						}else {
    							mapFld.put(f, new ArrayList<String>());
    						}
    					}
    					lstEnt.add(rsCheck.get("entity_permission").toString());
    				}
    			}
    			
    			if(lstEnt.size() <= 0) {
	    			resp.setCode(HttpStatus.UNAUTHORIZED.value());
	    			resp.setMessage("Anda bukan admin, jangan coba-coba...!");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    			}
    			
    			if(append.isPresent() && append.get().equals("false")) {
    				String sqlDel = "SELECT entity_permission, role_permission FROM hijr_permission WHERE mode_permission='DELETE'";
    				
    				List<Map<String, Object>> lstRsDel = sqlMapper.select(sqlDel);
    				
    				for(Map<String, Object> rsDel: lstRsDel) {
    					if(request.isUserInRole(rsDel.get("role_permission").toString())){
    						lstDel.add(rsDel.get("entity_permission").toString());
    					}
    				}
    			}
    		}
    		
    		for (Map.Entry<String,List<String>> entry : mapFld.entrySet()) {
    			System.out.println(entry.getKey() + ": " + entry.getValue().size());
    		}
    		
    		StringBuilder sql = new StringBuilder();
    		
    		sql.append("INSERT INTO ").append(prefix_entity).append(entity);
    		
    		List<StringBuilder> listsql = new ArrayList<StringBuilder>();
    		
    		Map<String, Object> rs = controller.getData();
    		
    		StringBuilder fields = new StringBuilder();
    		StringBuilder values = new StringBuilder();
    		StringBuilder combines = new StringBuilder();
    		StringBuilder joins = new StringBuilder();
    		StringBuilder select = new StringBuilder("id");
    		
    		String id = "";
    		int countFlds = 0;
    		
    		for (Map.Entry<String,Object> entry : rs.entrySet()) {
    			// entry sebagai join object
    			if(entry.getValue() instanceof Map) {
    				
    				joins.append(",").append(entry.getKey()).append(".select(id");
    				
    				Map<String, Object> subrs = (Map<String, Object>)entry.getValue();
    				for (Map.Entry<String,Object> sub : subrs.entrySet()) {
    					if(sub.getKey().equals("id")) {
    						
    						if(mapFld.containsKey(entity+"."+entry.getKey())) {
    							
    							if(mapFld.get(entity+"."+entry.getKey()).size() > 0 && mapFld.get(entity+"."+entry.getKey()).contains(sub.getValue())) {
    								countFlds++;
	    	    					
		    	        				fields.append(",id_").append(entry.getKey()).append("_").append(entity);
		    	        				values.append(",'").append(sub.getValue()).append("'");
		    	        				
		    	        				combines.append(",id_").append(entry.getKey()).append("_").append(entity);
		        					combines.append("='").append(sub.getValue()).append("'");
    							}else {
    								countFlds++;
	    	    					
		    	        				fields.append(",id_").append(entry.getKey()).append("_").append(entity);
		    	        				values.append(",'").append(sub.getValue()).append("'");
		    	        				
		    	        				combines.append(",id_").append(entry.getKey()).append("_").append(entity);
		        					combines.append("='").append(sub.getValue()).append("'");
    							}
    	    					
	    	    					
	    	    				}else if(mapFld.containsKey(entity+".*") || isAdmin) {
	    	    					countFlds++;
	    	    					
	    	        				fields.append(",id_").append(entry.getKey()).append("_").append(entity);
	    	        				values.append(",'").append(sub.getValue()).append("'");
	    	        				
	    	        				combines.append(",id_").append(entry.getKey()).append("_").append(entity);
	        					combines.append("='").append(sub.getValue()).append("'");
	    	    				}
    						
    					}else {
    						joins.append(",").append(sub.getKey());
    					}
    				}
    				
    				joins.append(")");
    				
    			}else {
    				if(entry.getValue() instanceof List) {
    					// GO TO SECTION: LOOP LIST ENTRY
    				}else {
    					
    					select.append(",").append(entry.getKey());
    					
    					if(entry.getKey().equals("id")) {
    	    					id = entry.getValue().toString();
    	            			
    	        				fields.append(",").append(entry.getKey()).append("_").append(entity);
    	            			values.append(",'").append(entry.getValue()).append("'");
    	            			
    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
    	    					combines.append("='").append(entry.getValue()).append("'");
        				}else {
        					if(mapFld.containsKey(entity+"."+entry.getKey())) {
        					
        						if(mapFld.get(entity+"."+entry.getKey()).size() > 0) {
        							
        							if(mapFld.get(entity+"."+entry.getKey()).contains(entry.getValue())) {
        								countFlds++;
            	    					
        								fields.append(",").append(entry.getKey()).append("_").append(entity);
        		    	            			values.append(",'").append(entry.getValue()).append("'");
        		    	            			
        		    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
        		    	    					combines.append("='").append(entry.getValue()).append("'");	
        							}
								
							}else {
								countFlds++;
	    	    					
								fields.append(",").append(entry.getKey()).append("_").append(entity);
		    	            			values.append(",'").append(entry.getValue()).append("'");
		    	            			
		    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
		    	    					combines.append("='").append(entry.getValue()).append("'");
							}
		    					
	    	    					
	    	    				}else if(mapFld.containsKey(entity+".*") || isAdmin) {
	    	    					countFlds++;
	    	    					
	    	    					fields.append(",").append(entry.getKey()).append("_").append(entity);
	    	            			values.append(",'").append(entry.getValue()).append("'");
	    	            			
	    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
	    	    					combines.append("='").append(entry.getValue()).append("'");
	    	    				}
        				}
    					
    				}
    				
    			}
    			
    		}
    		
    		sql.append(" (");
    		sql.append(fields.toString().substring(1));
    		if(!mode.isPresent()) {
	    		sql.append(",id_source_").append(entity);
	    		sql.append(",id_account_added_").append(entity);
	    		sql.append(",timestamp_added_").append(entity);
    		}
    		if(request.getRequestURI().contains("/me/")) {
    			sql.append(",id_account_").append(entity);
    		}
    		sql.append(")");
    		sql.append(" VALUES (");
    		sql.append(values.toString().substring(1));

    		if(!mode.isPresent()) {
	    		sql.append(",'").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("'");
	    		sql.append(",'").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
	    		sql.append(",NOW()");
    		}
    		if(request.getRequestURI().contains("/me/")) {
    			sql.append(",'").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");	
    		}
    		sql.append(")");
    		
		sql.append(" ON DUPLICATE KEY UPDATE ");
		sql.append(combines.toString().substring(1));

		if(!mode.isPresent()) {
		sql.append(",id_account_modified_").append(entity).append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
	    	sql.append(",timestamp_modified_").append(entity).append("=NOW()");
		}
	    	
	    	HashMap<String, Object> prev = selectOne(entity, Optional.of(id), Optional.of("id, "+ select.toString()), joins.length()>1?Optional.of(joins.toString().substring(1)):Optional.empty(), Optional.empty(), new ArrayList<>(), request);
	    	//HashMap<String, Object> prev = new HashMap<>();
    		//System.out.println(sql.toString());
	    	
	    	try {
	    		if(!triggerService.onNewCheck(controller.getDataMain().get("column"))) {
	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
	    		}
	    	}catch (Exception e) {
				// TODO: handle exception
	    		e.printStackTrace();
			}
    		if(!triggerService.onCheckDuplicate(entity, controller.getData())) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Duplicated :"+controller.getData().get("nama"));
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
	    	// selain kolom id_xxxxxxxxx
	    	if(countFlds > 0) sqlMapper.execute(sql.toString());
    		
	    	boolean updateFlag = true;    		
    		if(id.equals("")) {
    			Map<String,Object> obj = sqlMapper.selectOne("SELECT LAST_INSERT_ID()");
    			id = obj.values().iterator().next().toString();
    			
    			updateFlag = false;
    		}
    		
    		// SECTION: LOOP FOR LIST ENTRY
    		for (Map.Entry<String,Object> entry : rs.entrySet()) {
    			if(entry.getValue() instanceof List) {
    				
    				if(append.isPresent() && append.get().equals("false")) {
	    				StringBuilder del = new StringBuilder("DELETE FROM  ");
	    				del.append(prefix_entity).append(entry.getKey());
	    				del.append(" WHERE id_").append(entity).append("_").append(entry.getKey());
	    				del.append("='").append(id).append("'");
	    				
	    				if(lstDel.contains(entry.getKey()) || isAdmin) {
	    					listsql.add(del);	
	    				}
	    				
	    			}
    				
    				
    				for(Map<String, Object> map: (List<Map<String, Object>>)entry.getValue()) {
    					
    					StringBuilder ins = new StringBuilder("INSERT INTO  ");
        				ins.append(prefix_entity).append(entry.getKey());
    					
    					StringBuilder subfields = new StringBuilder();
    		    			StringBuilder subvalues = new StringBuilder();
    		    			
    		    			subfields.append(",id_").append(entity).append("_").append(entry.getKey());
    		    			subvalues.append(",'").append(id).append("'");
    					
        				for (Map.Entry<String,Object> sub : map.entrySet()) {
        					if(!sub.getKey().equals("id")) {
//        					if((lstFld.contains(sub.getKey()) && lstEnt.contains(entry.getKey())) || isAdmin) {
        						if(mapFld.containsKey(entry.getKey()+"."+sub.getKey())) {	
        							
        							if(mapFld.get(entry.getKey()+"."+sub.getKey()).size() > 0 ) {
        								if(mapFld.get(entry.getKey()+"."+sub.getKey()).contains(sub.getValue())) {
	        								subfields.append(",").append(sub.getKey()).append("_").append(entry.getKey());
		        	        					subvalues.append(",'").append(sub.getValue()).append("'");
        								}
        							}else {
        								subfields.append(",").append(sub.getKey()).append("_").append(entry.getKey());
	        	        					subvalues.append(",'").append(sub.getValue()).append("'");
        							}
	        						
        						}else if(mapFld.containsKey(entry.getKey()+".*") || isAdmin) {
        							subfields.append(",").append(sub.getKey()).append("_").append(entry.getKey());
        	        					subvalues.append(",'").append(sub.getValue()).append("'");
        						}
        					}
        				}
        				
//        				ins.append(" (").append(subfields.toString().substring(1)).append(")");
//        				ins.append(" VALUES (").append(subvalues.toString().substring(1)).append(")");
        				
        				ins.append(" (");
	        	    		ins.append(subfields.toString().substring(1));
	        	    		ins.append(",id_source_").append(entry.getKey());
	        	    		ins.append(",id_account_added_").append(entry.getKey());
	        	    		ins.append(",timestamp_added_").append(entry.getKey());
	        	    		ins.append(")");
	        	    		ins.append(" VALUES (");
	        	    		ins.append(subvalues.toString().substring(1));
	        	    		ins.append(",'").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("'");
	        	    		ins.append(",'").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
	        	    		ins.append(",NOW()");
	        	    		ins.append(")");
	        	    		
	        	    		if(lstEnt.contains(entry.getKey()) || isAdmin) {
	        	    			listsql.add(ins);
	        	    		}
    				}
    				
    			}
    			
    		}
    		
    		String strsql = "";
    		for (StringBuilder sb: listsql) {
    			strsql += sb.append(";\n").toString();
    		}
			
    		//System.out.println(strsql);
    		if(!id.equals("0") && !strsql.equals("")) sqlMapper.execute(strsql);
    		System.out.println(">>>>");
    		HashMap<String, Object> data = selectOne(entity, Optional.of(id), Optional.of(select.toString()), joins.length()>1?Optional.of(joins.toString().substring(1)):join.isPresent()?join:Optional.empty(), Optional.empty(), new ArrayList<>(), request);
    		
    		if(updateFlag) {
			resp.setMessage("Perubahan data telah berhasil disimpan");
			
			triggerService.onAfterUpdate(entity, data, prev);
    		}else {
    			resp.setMessage("Penambahan data ke dalam sistem telah berhasil");
    			
    			triggerService.onAfterInsert(entity, data);
    		}
    		
    		resp.setData(data);
    		/*
    		System.out.println("RUN");
    		System.out.println(joins.length()>1?Optional.of(joins.toString().substring(1)):join);
    		*/
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{entity}/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> delete(
    			@PathVariable String entity,
			@PathVariable String id,
			@RequestParam("cascade") Optional<String> cascade,
			HttpServletRequest request
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		
		String[] casent = new String[] {};
		if(cascade.isPresent()) {
			casent = cascade.get().split(",");
		}
		
		List<String> lstEnt = new ArrayList<String>();
		
		boolean isAdmin = true;
		
		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
			
			isAdmin = false;
			
			String sqlCheck = "SELECT entity_permission, role_permission FROM hijr_permission WHERE mode_permission='DELETE'";
			
			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
			
			for(Map<String, Object> rsCheck: lstRsCheck) {
				if(request.isUserInRole(rsCheck.get("role_permission").toString())){
					lstEnt.add(rsCheck.get("entity_permission").toString());
				}
			}
			
			if(lstEnt.size() <= 0) {
	    			resp.setCode(HttpStatus.UNAUTHORIZED.value());
	    			resp.setMessage("Anda bukan admin, jangan coba-coba...!");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
		}
		
		StringBuilder sql = new StringBuilder("DELETE FROM  ");
		sql.append(prefix_entity).append(entity);
		sql.append(" WHERE id_").append(entity);
		sql.append("='").append(id).append("'");
		
		
		if(!request.isUserInRole("SUPER_ADMIN")) {
			sql.append(" AND id_source_").append(entity);
			sql.append(" IN ('").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("', '*')");
		}
		
		if(request.getRequestURI().contains("/me/")) {
			sql.append(" AND id_account_").append(entity);
			sql.append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
		}
		
		List<StringBuilder> listsql = new ArrayList<StringBuilder>();
		
		for(String e: casent) {
			
			StringBuilder del = new StringBuilder("DELETE FROM  ");
			del.append(prefix_entity).append(e);
			del.append(" WHERE id_").append(entity).append("_").append(e);
			del.append("='").append(id).append("'");
			
			if(lstEnt.contains(e) || isAdmin) {
				listsql.add(del);
			}
		}

		HashMap<String, Object> data = selectOne(entity, Optional.of(id), Optional.empty(), Optional.empty(), Optional.empty(), new ArrayList<>(), request);
		//System.out.println(sql.toString());
		
		if(lstEnt.contains(entity) || isAdmin) {
			sqlMapper.execute(sql.toString());
		}
		
		Map<String,Object> obj = sqlMapper.selectOne("SELECT ROW_COUNT()");
		int rows = Integer.valueOf(obj.values().iterator().next().toString());
		
		String strsql = "";
		for (StringBuilder sb: listsql) {
			strsql += sb.append(";\n").toString();
		}

		
		resp.setData(data);
		
		
		//System.out.println(strsql);
		if(rows > 0 && !strsql.equals("")) sqlMapper.execute(strsql);
		resp.setMessage("Data telah dihapus.");
		
		triggerService.onAfterDelete(entity, data);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    //experiment

	
	@RequestMapping(value="/multi/entity", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getMultiList(
    		@RequestParam("entity") Optional<String> entity,
    		@RequestParam("filter") Optional<String> filter,
    		@RequestParam("filter_funct") Optional<String> filterFunct,
    		@RequestParam("limit") Optional<String> limit,
    		@RequestParam("join") Optional<String> join,
    		HttpServletRequest request
    		)throws Exception {
		
		ResponseWrapperList resp = new ResponseWrapperList();
		Map<String, Object> map = new HashMap<>();
		if(entity.isPresent()) {
			int i=0;
			Optional<String> newfilter=Optional.ofNullable(null);
			Optional<String> newfilterFunct=Optional.ofNullable(null);
			Optional<Integer> newLimit=Optional.ofNullable(null);
			Optional<String> newJoin=Optional.ofNullable(null);
			


			ArrayList<String> arrAllParam = new ArrayList<String>();
			ArrayList<Map<String, Object>> mapAllParam = new ArrayList<Map<String, Object>>();
			
			for(String e : entity.get().split(";")) {

				//ArrayList<Optional<String>> arrFilter = new ArrayList<Optional<String>>();
				ArrayList<String> arrFilter = new ArrayList<String>();
				//ArrayList<Optional<String>> arrFilterFunct = new ArrayList<Optional<String>>();
				ArrayList<String> arrFilterFunct = new ArrayList<String>();
				//ArrayList<Optional<Integer>> arrLimit = new ArrayList<Optional<Integer>>();
				ArrayList<Integer> arrLimit = new ArrayList<Integer>();
				//ArrayList<Optional<String>> arrJoin = new ArrayList<Optional<String>>();
				ArrayList<String> arrJoin = new ArrayList<String>();
				
				ArrayList<String> arrEntity = new ArrayList<String>();
				if(filter.isPresent()) {
					for(String f : filter.get().split(";")) {
						System.out.println("f " +f.split("->")[0]);
						System.out.println("f " +f.split("->")[1]);
						if(e.equals(f.split("->")[0])) {
							newfilter = Optional.ofNullable(f.split("->")[1]);
							arrFilter.add(f.split("->")[1]);
						}else {
							newfilter=Optional.ofNullable(null);
						}
					}
				}

				if(filterFunct.isPresent()) {
					for(String f : filterFunct.get().split(";")) {
						System.out.println("f " +f.split("->")[0]);
						System.out.println("f " +f.split("->")[1]);
						if(e.equals(f.split("->")[0])) {
							newfilterFunct = Optional.ofNullable(f.split("->")[1]);
							arrFilterFunct.add(f.split("->")[1]);
						}else {
							newfilterFunct=Optional.ofNullable(null);
						}
					}
				}
				if(limit.isPresent()) {
					for(String l : limit.get().split(";")) {
						//System.out.println("l " +l.split("->")[0]);
						//System.out.println("l " +l.split("->")[1]);
						if(e.equals(l.split("->")[0])) {
							newLimit = Optional.ofNullable(Integer.valueOf(l.split("->")[1]));
							arrLimit.add(Integer.valueOf(l.split("->")[1]));
						}else {
							newLimit=Optional.ofNullable(null);
						}
					}
				}
				if(join.isPresent()) {
					for(String j : join.get().split(";")) {
						//System.out.println("l " +l.split("->")[0]);
						//System.out.println("l " +l.split("->")[1]);
						if(e.equals(j.split("->")[0])) {
							newJoin = Optional.ofNullable(j.split("->")[1]);
							arrJoin.add(j.split("->")[1]);
						}else {
							newJoin=Optional.ofNullable(null);
						}
					}
				}
				
				//arrAllParam.add("{\"entity\" : \""+e+"\", \"filter\" : '"+arrFilter+"', \"filterFunct\" : '"+arrFilterFunct+"', \"limit\" : '"+arrLimit+"', \"join\" : '"+arrJoin+"'}");
				arrEntity.add(e);
				Map<String, Object> obj = new HashMap<>();
				obj.put("entity", arrEntity);
				obj.put("filter", arrFilter);
				obj.put("filterFunct", arrFilterFunct);
				obj.put("limit", arrLimit);
				obj.put("join", arrJoin);
				
				mapAllParam.add(obj);
				//param : entity?, limit?, page(null), filter?, costume_filter?, select(null), select_funct(null), join?, order(null), request?
				/*
				String cry="";
				if(e.equals(arrFilter.get(i).get())) {
					
				}
				
				if(e.equals(arrFilterFunct.get(i).get())) {
					
				}
				
				if(e.equals(String.valueOf(arrLimit.get(i).get()))) {
					
				}
				
				if(e.equals(arrJoin.get(i).get())) {
					
				}
				*/
				//limit=100
				//limt=200
				
				/*map.put(e, 
						getList(e, 
								forceIntArrayReturn(arrLimit, i) , 
								Optional.ofNullable(null), 
								forceStrArrayReturn(arrFilter, i), 
								forceStrArrayReturn(arrFilterFunct, i), 
								Optional.ofNullable(null), Optional.ofNullable(null), 
								forceStrArrayReturn(arrJoin, i), 
								Optional.ofNullable(null), 
								request
						).getBody().getData()
				);*/
				
				//arrFilter, arrFilterFunct, arrLimit, arrJoin
				
				/*if(arrFilter.size() > 1) {
					for(Optional<String> afilter : arrFilter) {
						map.put(e, getList(e, forceIntArrayReturn(arrLimit, i) , Optional.ofNullable(null), afilter, forceStrArrayReturn(arrFilterFunct, i), Optional.ofNullable(null), Optional.ofNullable(null), forceStrArrayReturn(arrJoin, i), Optional.ofNullable(null), request).getBody().getData());
					}
				}else if(arrJoin.size() > 1) {
					for(Optional<String> aJoin : arrJoin) {
						map.put(e, getList(e, forceIntArrayReturn(arrLimit, i) , Optional.ofNullable(null), forceStrArrayReturn(arrFilter, i), forceStrArrayReturn(arrFilterFunct, i), Optional.ofNullable(null), Optional.ofNullable(null), aJoin, Optional.ofNullable(null), request).getBody().getData());
					}
				}else {
					
				}*/
				i++;
			}
			System.out.println("mapAllParam"+mapAllParam);
			for(Map o: mapAllParam) {
				//System.out.println("obj_all_param"+ obj_all_param);

				System.out.println("o:"+o.get("entity"));
				JSONObject obj_value = new JSONObject(o);

				System.out.println("entity:"+obj_value.getString("entity")+"&"+"limit:"+obj_value.get("limit")+"&"+"filter:"+obj_value.get("filter")+"&"+"filterFunct:"+"obj_value.get(\"filterFunct\")"+"&"+"join:"+obj_value.get("join"));
		    	
				String param="entity";
				if(obj_value.getJSONArray("filter").length() > obj_value.getJSONArray("join").length()) {
					param="filter";
				}else if(obj_value.getJSONArray("join").length() > obj_value.getJSONArray("filter").length()) {
					param="join";
				}else if(obj_value.getJSONArray("limit").length() > obj_value.getJSONArray("filter").length()) {
					param="limit";
				}else if(obj_value.getJSONArray("limit").length() > obj_value.getJSONArray("join").length()) {
					param="limit";
				}
				System.out.println("parama:"+param);

		        /*ArrayList<Object> listdata = new ArrayList<Object>();  
		        if (obj_value.getJSONArray(param) != null) {
		            //Iterating JSON array  
		            for (int k=0;i<obj_value.getJSONArray(param).length();k++){   
		                //Adding each element of JSON array into ArrayList  
		                listdata.add(obj_value.getJSONArray(param).get(k));  
		            }   
		        }*/
		        
					try {
							if(obj_value.getJSONArray("filter").length() > 0 && obj_value.getJSONArray("join").length() > 0 && obj_value.getJSONArray("limit").length() > 0) {
								System.out.println("para-para:"+param);
								for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
									System.out.println("1:"+k);
									map.put(obj_value.getJSONArray("entity").get(k).toString(), 
										getList(obj_value.getJSONArray("entity").get(k).toString(), 
												Optional.ofNullable(Integer.valueOf(obj_value.getJSONArray("limit").get(k).toString())), 
												Optional.ofNullable(null), 
												Optional.ofNullable(obj_value.getJSONArray("filter").get(k).toString()), 
												Optional.ofNullable(null), 
												Optional.ofNullable(null), Optional.ofNullable(null), 
												Optional.ofNullable(obj_value.getJSONArray("join").get(k).toString()), 
												Optional.ofNullable(null), 
												request
										).getBody().getData()
									);

						    	}
							}else if(obj_value.getJSONArray("filter").length() > 0 && obj_value.getJSONArray("join").length() > 0) {
								for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
									map.put(obj_value.getJSONArray("entity").get(k).toString(), 
											getList(obj_value.getJSONArray("entity").get(k).toString(), 
													Optional.ofNullable(null), 
													Optional.ofNullable(null), 
													Optional.ofNullable(obj_value.getJSONArray("filter").get(k).toString()), 
													Optional.ofNullable(null), 
													Optional.ofNullable(null), Optional.ofNullable(null), 
													Optional.ofNullable(obj_value.getJSONArray("join").get(k).toString()), 
													Optional.ofNullable(null), 
													request
											).getBody().getData()
									);
								}
							}else if(obj_value.getJSONArray("limit").length() > 0 && obj_value.getJSONArray("join").length() > 0) {
								for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
									map.put(obj_value.getJSONArray("entity").get(k).toString(), 
											getList(obj_value.getJSONArray("entity").get(k).toString(), 
													Optional.ofNullable(Integer.valueOf(obj_value.getJSONArray("limit").get(k).toString())), 
													Optional.ofNullable(null), 
													Optional.ofNullable(null), 
													Optional.ofNullable(null), 
													Optional.ofNullable(null), Optional.ofNullable(null), 
													Optional.ofNullable(obj_value.getJSONArray("join").get(k).toString()), 
													Optional.ofNullable(null), 
													request
											).getBody().getData()
									);
								}
							}else if(obj_value.getJSONArray("limit").length() > 0 && obj_value.getJSONArray("filter").length() > 0) {
								for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
									map.put(obj_value.getJSONArray("entity").get(k).toString(), 
											getList(obj_value.getJSONArray("entity").get(k).toString(), 
													Optional.ofNullable(Integer.valueOf(obj_value.getJSONArray("limit").get(k).toString())), 
													Optional.ofNullable(null), 
													Optional.ofNullable(obj_value.getJSONArray("filter").get(k).toString()), 
													Optional.ofNullable(null), 
													Optional.ofNullable(null), Optional.ofNullable(null), 
													Optional.ofNullable(null), 
													Optional.ofNullable(null), 
													request
											).getBody().getData()
									);
								}
							}else {
								if(obj_value.getJSONArray("filter").length() > 0) {
									for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
										map.put(obj_value.getJSONArray("entity").get(k).toString(), 
												getList(obj_value.getJSONArray("entity").get(k).toString(), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(obj_value.getJSONArray("filter").get(k).toString()), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														request
												).getBody().getData()
										);
									}
								}else if(obj_value.getJSONArray("join").length() > 0) {
									for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
										map.put(obj_value.getJSONArray("entity").get(k).toString(), 
												getList(obj_value.getJSONArray("entity").get(k).toString(), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), Optional.ofNullable(null), 
														Optional.ofNullable(obj_value.getJSONArray("join").get(k).toString()), 
														Optional.ofNullable(null), 
														request
												).getBody().getData()
										);
									}
								}else if(obj_value.getJSONArray("limit").length() > 0) {
									for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
										map.put(obj_value.getJSONArray("entity").get(k).toString(), 
												getList(obj_value.getJSONArray("entity").get(k).toString(), 
														Optional.ofNullable(Integer.valueOf(obj_value.getJSONArray("limit").get(k).toString())), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														request
												).getBody().getData()
										);
									}
								}else {
									for(int k=0;k<obj_value.getJSONArray(param).length();k++) {
										map.put(obj_value.getJSONArray("entity").get(k).toString(), 
												getList(obj_value.getJSONArray("entity").get(k).toString(), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														Optional.ofNullable(null), 
														request
												).getBody().getData()
										);
									}
								}
							}
					} catch (Throwable e) {
						e.printStackTrace();
					}
				
				
			}
			//System.out.println("arrAllParam: "+arrAllParam);
		}
		resp.setData(map);
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    //
    
    private HashMap<String, Object> remap(Map<String, Object> rs, String entity, String[] joinent, List<String> hidfld) {
    		HashMap<String, Object> newrs = new HashMap<String, Object>();
		
		for (Map.Entry<String,Object> entry : rs.entrySet()) {
			String k = entry.getKey().replace("_"+entity, "");
			if(entry.getKey().indexOf(entity) >= 0 && !hidfld.contains(entity+"."+k))
				newrs.put(k, entry.getValue());	
		}
		
		
		for(String e: joinent) {
			String[] je =  e.split("\\.");
			
			Map<String, Object> newrsjoin = new HashMap<String, Object>();
			
			for (Map.Entry<String,Object> entry : rs.entrySet()) {
				String k = entry.getKey().replace("_"+je[0], "");
				if(entry.getKey().indexOf(je[0]) >= 0 && !hidfld.contains(je[0]+"."+k))
					newrsjoin.put(k, entry.getValue());	
			}
			
			newrs.put(je[0], newrsjoin);
		}
		
		return newrs;
    }
    
    

    private StringBuilder construct(
    		String entity,
    		Optional<Integer> limit,
    		Optional<Integer> page,
    		Optional<String> filter,
    		Optional<String> select,
    		Optional<String> join,
    		Optional<String> selectFunct,
    		Optional<String> filterFunct
    		) {
    		String tblname = prefix_entity+entity.toLowerCase();
		
		StringBuilder sb = new StringBuilder();
		
//		sb.append("SELECT id_").append(entity);
		sb.append("SELECT 1");
		
		String[] joinent = {};
		if(join.isPresent() && !join.get().equals("")) {
			joinent = join.get().split(",(?=[^\\)]*(\\(|$))");
		}
		
		
		
		if(select.isPresent()) {
			String[] cols = select.get().split(",");
			for(String c: cols) {
				
				sb.append(", ").append(c).append("_").append(entity.toLowerCase());
				//sb.append(" AS ").append(c);	
			}
			
			for(String e: joinent) {
				String[] je =  e.split("\\.");
				
				if(je.length > 1) {
					String[] joinsc = je[1].substring(7, je[1].length()-1).split(",");
					
					for(String sc: joinsc) {
						sb.append(", ").append(sc).append("_").append(je[0]);
						//sb.append(" AS ").append(sc);	
					}
				}else {
					sb.append(", ").append(prefix_entity).append(e).append(".*");
				}
			}
					
		}else {
			sb.append(", ").append(tblname).append(".*");
			
			for(String e: joinent) {
				String[] je =  e.split("\\.");
				
				if(je.length > 1) {
					String[] joinsc = je[1].substring(7, je[1].length()-1).split(",");
					
					for(String sc: joinsc) {
						sb.append(", ").append(sc).append("_").append(je[0]);
						//sb.append(" AS ").append(sc);	
					}
				}else {
					sb.append(", ").append(prefix_entity).append(e).append(".*");
				}
			}
					
		}
		
		if(selectFunct.isPresent()) {
			sb.append(", ").append(selectFunct.get());
		}
		
		sb.append(" FROM ").append(tblname);
		
		for(String e: joinent) {
			String[] je =  e.split("\\.");
			sb.append(" LEFT JOIN ").append(prefix_entity).append(je[0]);
			sb.append(" ON ").append("id_").append(je[0]).append("_").append(entity);
			sb.append(" = ").append("id_").append(je[0]);
		}
				
		
		sb.append(" WHERE 1");

		
		if(filter.isPresent()) {
			String[] cols = filter.get().split(",(?=[^\\)]*(\\(|$))");
			for(String c: cols) {

				String[] f = c.split("\\.");
				
				sb.append(" AND ").append(f[0]).append("_").append(entity.toLowerCase());
				
				if(f[1].toLowerCase().startsWith("eq")) {
					
					sb.append(" = ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("neq")) {
					
					sb.append(" != ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("like") || f[1].toLowerCase().startsWith("slike")) {
					System.out.println("f[1]:"+f[1].substring(5, f[1].length()-1));
					sb.append(" LIKE '%").append(f[1].substring(5, f[1].length()-1).replaceAll("'", "")/*.replaceFirst(" ", "")*/);
					sb.append("%'");
				}
				
				if(f[1].toLowerCase().startsWith("more")) {
					
					sb.append(" > ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("less")) {
					
					sb.append(" < ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("between")) {
					
					sb.append(" BETWEEN ").append(f[1].substring(8, f[1].length()-1).replace(",", " AND "));
				}
				
				if(f[1].toLowerCase().startsWith("is")) {
					
					sb.append(" IS ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("not")) {
					
					sb.append(" IS NOT ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("in")) {
					
					sb.append(" IN (");
					
					String[] vals = f[1].substring(3, f[1].length()-1).split(",");
					
					StringBuilder strv = new StringBuilder();
					for(String v: vals) {
						strv.append(",").append(v);
					}
					
					sb.append(strv.toString().substring(1));
					
					sb.append(")");
				}
			}
		}
		
		if(filterFunct.isPresent()) {
			String[] cols = filterFunct.get().split(",(?=[^\\)]*(\\(|$))");
			for(String c: cols) {

				String[] f = c.split("\\.");
				
				sb.append(" AND ").append(f[0]);
				
				if(f[1].toLowerCase().startsWith("eq")) {
					
					sb.append(" = ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("neq")) {
					
					sb.append(" != ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("like") || f[1].toLowerCase().startsWith("slike")) {
					System.out.println("f[1]:"+f[1].substring(5, f[1].length()-1));
					sb.append(" LIKE '%").append(f[1].substring(5, f[1].length()-1).replaceAll("'", "")/*.replaceFirst(" ", "")*/);
					sb.append("%'");
				}
				
				if(f[1].toLowerCase().startsWith("more")) {
					
					sb.append(" > ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("less")) {
					
					sb.append(" < ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("between")) {
					
					sb.append(" BETWEEN ").append(f[1].substring(8, f[1].length()-1).replace(",", " AND "));
				}
				
				if(f[1].toLowerCase().startsWith("is")) {
					
					sb.append(" IS ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("not")) {
					
					sb.append(" IS NOT ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("in")) {
					
					sb.append(" IN (");
					
					String[] vals = f[1].substring(3, f[1].length()-1).split(",");
					
					StringBuilder strv = new StringBuilder();
					for(String v: vals) {
						strv.append(",").append(v);
					}
					
					sb.append(strv.toString().substring(1));
					
					sb.append(")");
				}
			}
		}
		
		return sb;
    }
    
    private StringBuilder construct(
    		String entity,
    		Optional<Integer> limit,
    		Optional<Integer> page,
    		Optional<String> filter,
    		Optional<String> select,
    		Optional<String> join
    		) {
    		String tblname = prefix_entity+entity.toLowerCase();
		
		StringBuilder sb = new StringBuilder();
		
//		sb.append("SELECT id_").append(entity);
		sb.append("SELECT 1");
		
		String[] joinent = {};
		if(join.isPresent() && !join.get().equals("")) {
			joinent = join.get().split(",(?=[^\\)]*(\\(|$))");
		}
		
		
		
		if(select.isPresent()) {
			String[] cols = select.get().split(",");
			for(String c: cols) {
				
				sb.append(", ").append(c).append("_").append(entity.toLowerCase());
				//sb.append(" AS ").append(c);	
			}
			
			for(String e: joinent) {
				String[] je =  e.split("\\.");
				
				if(je.length > 1) {
					String[] joinsc = je[1].substring(7, je[1].length()-1).split(",");
					
					for(String sc: joinsc) {
						sb.append(", ").append(sc).append("_").append(je[0]);
						//sb.append(" AS ").append(sc);	
					}
				}else {
					sb.append(", ").append(prefix_entity).append(e).append(".*");
				}
			}
					
		}else {
			sb.append(", ").append(tblname).append(".*");
			
			for(String e: joinent) {
				String[] je =  e.split("\\.");
				
				if(je.length > 1) {
					String[] joinsc = je[1].substring(7, je[1].length()-1).split(",");
					
					for(String sc: joinsc) {
						sb.append(", ").append(sc).append("_").append(je[0]);
						//sb.append(" AS ").append(sc);	
					}
				}else {
					sb.append(", ").append(prefix_entity).append(e).append(".*");
				}
			}
					
		}
		
		sb.append(" FROM ").append(tblname);
		
		for(String e: joinent) {
			String[] je =  e.split("\\.");
			sb.append(" LEFT JOIN ").append(prefix_entity).append(je[0]);
			sb.append(" ON ").append("id_").append(je[0]).append("_").append(entity);
			sb.append(" = ").append("id_").append(je[0]);
		}
				
		
		sb.append(" WHERE 1");
		
		
		if(filter.isPresent()) {
			String[] cols = filter.get().split(",(?=[^\\)]*(\\(|$))");
			for(String c: cols) {

				String[] f = c.split("\\.");
				
				sb.append(" AND ").append(f[0]).append("_").append(entity.toLowerCase());
				
				if(f[1].toLowerCase().startsWith("eq")) {
					
					sb.append(" = ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("neq")) {
					
					sb.append(" != ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("like") || f[1].toLowerCase().startsWith("slike")) {
					System.out.println("f[1]:"+f[1].substring(5, f[1].length()-1));
					sb.append(" LIKE '%").append(f[1].substring(5, f[1].length()-1).replaceAll("'", "")/*.replaceFirst(" ", "")*/);
					sb.append("%'");
				}
				
				if(f[1].toLowerCase().startsWith("more")) {
					
					sb.append(" > ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("less")) {
					
					sb.append(" < ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("between")) {
					
					sb.append(" BETWEEN ").append(f[1].substring(8, f[1].length()-1).replace(",", " AND "));
				}
				
				if(f[1].toLowerCase().startsWith("is")) {
					
					sb.append(" IS ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("not")) {
					
					sb.append(" IS NOT ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("in")) {
					
					sb.append(" IN (");
					
					String[] vals = f[1].substring(3, f[1].length()-1).split(",");
					
					StringBuilder strv = new StringBuilder();
					for(String v: vals) {
						strv.append(",").append(v);
					}
					
					sb.append(strv.toString().substring(1));
					
					sb.append(")");
				}
			}
		}
		
		return sb;
    }

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public Map<String, String[]> getDataMain() {
		return dataMain;
	}

	public void setDataMain(Map<String, String[]> dataMain) {
		this.dataMain = dataMain;
	}

}


