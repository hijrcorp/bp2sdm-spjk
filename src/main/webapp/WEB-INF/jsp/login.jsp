<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">

<head>
	<title>Survey Penilaian Lingpeng dan Kegiatan Pengawasan - SIM Pengawasan ITJEN</title>
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
  	
  	
  	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css">

  	
  	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  	
</head>

<body>
	 <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">                    
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h4>Silahkan Masuk</h4>
                        			<c:choose>
						  <c:when test="${not empty param.error}">
						    <p style="font-size: 20; color: #FF1C19;">Data yang anda masukkan salah</p>
						  </c:when>
						  <c:otherwise>
						    <p>Survey Penilaian Lingpeng dan Kegiatan Pengawasan</p>
						  </c:otherwise>
						</c:choose>
                        		</div>
                        		<div class="form-top-right">
                        			<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
                        		</div>
                            </div>
                          
                            <div class="form-bottom">
			                    <form role="form" action="${loginPerform}" method="post" class="login-form" enctype="multipart/form-data">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input autocomplete="off" type="text" name="username_or_email" placeholder="Username atau Email" class="form-username form-control" id="username_or_email">
			                        </div>
			                      
									  <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
									    <div class="input-group" id="show_hide_password">
									      <input class="form-password form-control" type="password" name="password" placeholder="Password" id="password">
									      <div class="input-group-addon">
									        <a href="javascript:void(0)" id="show_password"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
									      </div>
									    </div>
									  </div>
			                        <button id="btnLogin" type="submit" class="btn">Masuk</button>
			                        
			                    </form>
		                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>

</body>
<script src="${pageContext.request.contextPath}/js/login-script.js"></script>

</html>