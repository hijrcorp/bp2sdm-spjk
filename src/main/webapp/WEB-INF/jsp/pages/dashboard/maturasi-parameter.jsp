<%@ include file = "inc/header.jsp" %>

<div id="wrapper" class="main-content">

	<div id="content-wrapper" class="section__content section__content--p20">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Template Bukti</a></li>
				<li class="breadcrumb-item active" id="menu"></li>
			</ol>
			
			<div class="card">
				<div class="card-header">
					<h4><span id="menu"></span></h4>
				</div>
				<div class="card-body">
					<div class="default-tab">
						<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
						<div class="tab-content pl-3 pt-3" id="nav-tab-content"></div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.container-fluid -->

		<%@ include file = "inc/trademark.jsp" %>	

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->



<div id="template-content" style="display: none">
	<div class="form-row">
    <div class="form-group col-md-12">
    	<button onclick="add()" data-toggle="modal" data-target="#modal-form" class="btn btn-dark float-right mx-1" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
    </div>
	</div>
	<div class="card" id="card-data-index">
	</div>
</div>


<!-- Form Modal-->
<div class="modal fade" id="modal-form" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="entry-form">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
					<input type="hidden" name="jenis" />
					<!-- <input type="hidden" name="id_unsur" /> -->
				
					<div class="form-group row">
						<div class="col-md-3">
							<label>Unsur</label>
						</div>
						<div class="col-md-9">
							<select name="id_unsur" class="form-control">
							</select>
						</div>
					</div> 
				
					<div class="form-group row">
						<div class="col-md-3">
							<label>Sub Unsur</label>
						</div>
						<div class="col-md-9">
							<select name="id_sub_unsur" class="form-control">
							</select>
						</div>
					</div> 
				
					<div class="form-group row">
						<div class="col-md-3">
							<label>Tingkat</label>
						</div>
						<div class="col-md-9">
							<select name="id_maturitas_tingkat" class="form-control">
							</select>
						</div>
					</div>
				
					<div class="form-group row">
						<div class="col-md-3">
							<label>Kode</label>
						</div>
						<div class="col-md-9">
							<input type="text" name="kode_pernyataan" class="form-control" />
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-sm-3">
							<label>Pernyataan</label>
						</div>
						<div class="col-sm-9">
							<textarea name="judul_pernyataan" class="form-control" rows="4"></textarea>
						</div>
					</div>
				
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
					<button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="${assetPath}/js/maturasi-parameter.js"></script>
<%@ include file = "inc/footer.jsp" %>