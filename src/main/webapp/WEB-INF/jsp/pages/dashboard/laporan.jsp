<%@ include file = "inc/header.jsp" %>

<style>
.table-striped thead, .table-striped tfoot {
  background: #84ACB6 !important;
  color: white;
}
/* html {zoom: 80%;}  */
.form-control,.force {
      width: 350px!important;
      height: 43.98px;
    }
    [class^='select2'] {
  border-radius: 0px !important;
}


	
	::-webkit-scrollbar {
	    -webkit-appearance: none;
	    width: 7px;
	}
	::-webkit-scrollbar-thumb {
	    border-radius: 4px;
	    background-color: rgba(0,0,0,.5);
	    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
	}
	
	
		
		.header-fixed {
		    position: fixed;
		    top: 100px; display:none;
		    background-color:white;
		}
		
</style>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    	<%@ include file = "inc/hasil-keseluruhan.jsp" %>
		
				<div class="row mb-3">
					<div class="col-md-12">
					<form id="filter-kompetensi-individu" class="form-inline">
						
				      <div class="input-group"> <!-- pl-3 pr-3 -->
				       <!--  <div class="input-group-prepend d-none">
				          <span class="input-group-text">Prepend</span>
				        </div> -->
						<div class="form-group mb-0" style="width: 100px">
							<select name="filter_id_eselon1" class="form-control form-control-sm select2">
								<option value="">Eselon1</option>
							</select>
						</div>
						
					    <c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_ESELON1')}">
							<div class="form-group mb-0" style="width: 115px">
								<select name="filter_id_organisasi" class="form-control select2">
									<option value="">BDLHK</option>
								</select>
							</div>
					    </c:if>
    
						<div class="form-group mb-0" style="width: 140px">
							<select name="filter_id_propinsi" class="form-control select2 form-control-inline">
								<option value="">Propinsi</option>
							</select>
						</div>
						<div class="form-group mb-0" style="width: 200px">
							<select name="filter_id_unit_auditi" class="form-control select2">
								<option value="">Satker</option>
							</select>
						</div>
						<div class="form-group mb-0 d-none" style="width: 0px">
							<select name="filter_id_kelompok_jabatan" class="form-control  form-control-inline">
								<option value="">Jabatan</option>
							</select>
						</div>
						<div class="form-group mb-0" style="width: 100px">
							<select name="filter_id_jenjang_jabatan" class="form-control select2 form-control-inline">
								<option value="">Jenjang</option>
							</select>
						</div>
						<div class="form-group mb-0" style="width: 80px">
							<select name="filter_status" class="form-control select2 form-control-inline">
								<option value="">Status</option>
								<option value="SELESAI">Selesai</option>
								<option value="PROSES">Proses</option>
							</select>
						</div>
						<div class="form-group mb-0" style="width: 228px">
							<input type="text" placeholder="NIP" name="filter_nip" class="form-control force rounded-0 shadow-none">
						</div>
				        <div class="input-group-append ">
						<button id="btn-search" type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
						<button type="button" onclick="clearIndividu()" class="btn btn-danger ">X</button>
						
				        </div>
					</div>
					</form>
					</div>
				</div>
		<div class="card">
			<div class="card-header">
				<div class="row justify-content-between">
					<div class="col-md-6 d-flex align-items-center">
						<h4 class="card-title">Daftar Responden</h4>
					</div>
					<div class="col-md-6 ">
						<div class="float-right">
						<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark d-none" type="button"><i class="fas fa-fw fa-print"></i></button>
						</div>
					</div>
				</div>
			</div>
			
			<div id="card-hidar-table" class="card-body table-responsive p-0 m-0">
				<!-- <table class="table table-hover table-hovered table-bordered table-sm p-0 m-0"> -->
				<table id="tbl-hidar-table" class="table table-bordered table-striped table-hover small">
					<thead id="hidar-table" ></thead>
				</table>
			</div>
			
			<div id="card-tbl-kompetensi-individu" class="card-body table-responsive p-0" style="height:600px" onscroll="scrollPhan()">
				<table id="tbl-kompetensi-individu" class="table table-bordered table-striped table-hover small">
					<thead>
						<tr>
							<th width="50"  class="text-center align-middle">No</th>
							<th width="150" class="text-center align-middle">Nama</th>
							<th width="100" class="text-center align-middle">NIP</th>
							<th width="100" class="text-center align-middle">Gol</th>
							<th width="100" class="text-center align-middle">Jabatan</th>
							<th width="100" class="text-center align-middle">Jenjang</th>
							<th width="100" class="text-center align-middle">Eselon 1</th>
							<th width="100" class="text-center align-middle">Satuan Kerja</th>
							<th width="100" class="text-center align-middle">Propinsi</th>
							<th width="100" class="text-center align-middle">Nilai Manajerial</th>
							<th width="100" class="text-center align-middle">Nilai Sosiokultural</th>
							<th width="100" class="text-center align-middle">Nilai Teknis</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<th width="50" colspan="9" class="text-center align-middle">NILAI RATA-RATA</th>
							<th id="nilai-komp-manajerial" width="100" class="text-center">-</th>
							<th id="nilai-komp-sosiokultural" width="100" class="text-center">-</th>
							<th id="nilai-komp-teknis" width="100" class="text-center">-</th>
						</tr>
					</tfoot>
				</table>
			
				<div class="default-tab">
					<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
					<div class="tab-content pl-0 pt-0 table-responsive" id="nav-tab-content">
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="card">
			<div class="card-header">
				<h4>Table Progress Peserta Ujian</h4>
			</div>
			<div class="card-body">
				<table id="tbl-progress-peserta-ujian" class="table table-bordered table-striped table-hover small">
					<thead>
						<tr>
							<th width="10"  class="text-center">No</th>
							<th width="100" class="text-center">Kelompok Jabatan</th>
							<th width="100" class="text-center">Jumlah Peserta Berjalan</th>
							<th width="100" class="text-center">Total Peserta</th>
							<th width="100" class="text-center">Kode Sesi</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<div class="default-tab">
					<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
					<div class="tab-content pl-0 pt-0 table-responsive" id="nav-tab-content">
					</div>
				</div>
			</div>
		</div>
		
		<div class="card">
			<div class="card-header">
				<h4>Tabel Rata-rata Kompetensi Teknis</h4>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
			           	<div class="pull-right">
							<form class="form-inline">
			                <div class="form-row">
	    						<div class="form-group col-md-6">
				                <select id="filter_tingkat_jabatan" data-chart="teknisChart" class="form-control mr-3">
				                	<option value="">Pilih Tingkatan</option>
				                </select>
				                </div>
				                <div class="form-group col-md-6">
				                <select id="filter_jenjang_jabatan" data-chart="teknisChart" class="form-control">
				                	<option value="">Pilih Jenjang</option>
				                </select>
				                </div>
			                </div>
			             	</form>
			            </div>
						<!-- <div class="mt-4" style="">zoom: 1.1764705882352942; -->
						<div id="ContainerTeknisChart" style="">
							<canvas id="teknisChart"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<table id="tbl-teknis" class="table table-bordered table-striped table-hover small">
							<thead>
								<tr>
									<th width="10"  class="text-center">No</th>
									<th width="100" class="text-center">Kelompok Jabatan</th>
									<th width="100" class="text-center">Jenjang</th>
									<th width="100" class="text-center">Tingkatan</th>
									<th width="200" class="text-center">Jabatan</th>
									<th width="100" class="text-center">Nilai Rata-Rata</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class="default-tab">
							<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
							<div class="tab-content pl-0 pt-0 table-responsive" id="nav-tab-content">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="card">
			<div class="card-header">
				<h4>Tabel Rata-rata Kompetensi Sosiokultural</h4>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
			           	<div class="pull-right">
							<form class="form-inline">
			                <div class="form-row">
	    						<div class="form-group col-md-6">
				                <select id="filter_tingkat_jabatan" data-chart="sosiokulturalChart" class="form-control mr-3">
				                	<option value="">Pilih Tingkatan</option>
				                </select>
				                </div>
				                <div class="form-group col-md-6">
				                <select id="filter_jenjang_jabatan" data-chart="sosiokulturalChart" class="form-control">
				                	<option value="">Pilih Jenjang</option>
				                </select>
				                </div>
			                </div>
			             	</form>
			            </div>
						<div class="mt-4" style=""><!-- zoom: 1.1764705882352942; -->
							<canvas id="sosiokulturalChart"></canvas>
						</div>
					</div>
					<div class="col-md-6 table-responsive">
						<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover small">
							<thead>
								<tr>
									<th width="10"  class="text-center">No</th>
									<th width="100" class="text-center">Kelompok Jabatan</th>
									<th width="100" class="text-center">Jenjang</th>
									<th width="100" class="text-center">Tingkatan</th>
									<th width="300" class="text-center">Jabatan</th>
									<th width="100" class="text-center">Nilai Rata-Rata</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class="default-tab">
							<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
							<div class="tab-content pl-0 pt-0 table-responsive" id="nav-tab-content">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="card">
			<div class="card-header">
				<h4>Tabel Rata-rata Kompetensi Manajerial</h4>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
			           	<div class="pull-right mb-3">
							<form class="form-inline">
			                <div class="form-row">
	    						<div class="form-group col-md-6">
				                <select id="filter_tingkat_jabatan" data-chart="manajerialChart" class="form-control mr-3">
				                	<option value="">Pilih Tingkatan</option>
				                </select>
				                </div>
				                <div class="form-group col-md-6">
				                <select id="filter_jenjang_jabatan" data-chart="manajerialChart" class="form-control">
				                	<option value="">Pilih Jenjang</option>
				                </select>
				                </div>
			                </div>
			             	</form>
			            </div>
						<!-- <div class="mt-5" style="">zoom: 1.1764705882352942; -->
						<div id="ContainerManajerialChart" style="">
							<canvas id="manajerialChart"></canvas>
						</div>
           				<div class="text-center mt-4" id="div-btn-kompetensi-manajerial"></div>
					</div>
					<div class="col-md-6 table-responsive">
						<table id="tbl-manajerial" class="table table-bordered table-striped table-hover small">
							<thead>
								<tr>
									<th width="10"  class="text-center">No</th>
									<th width="100" class="text-center">Kelompok Jabatan</th>
									<th width="100" class="text-center">Jenjang</th>
									<th width="100" class="text-center">Tingkatan</th>
									<th width="100" class="text-center">Kategori</th>
									<th width="200" class="text-center">Jabatan</th>
									<th width="100" class="text-center">Nilai Standard</th>
									<th width="100" class="text-center">Nilai Pemetaan</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class="default-tab">
							<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
							<div class="tab-content pl-0 pt-0 table-responsive" id="nav-tab-content">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/laporan.js"></script>
<script>
//card-hidar-table
function scrollPhan(){
	var scrollPos = document.getElementById("card-tbl-kompetensi-individu").scrollLeft;
	document.getElementById("card-hidar-table").scrollLeft=scrollPos;
}
</script>