<%@ include file = "inc/header.jsp" %>

<style>
</style>

<!-- MAIN CONTENT-->
<div class="main-content" style="padding-top: 110px">
  <div class="section__content section__content--p30">
    <div class="container-fluid">

		<!-- Breadcrumbs <%= (request.getParameter("jenis") != null && request.getParameter("jenis").equals("Rekomendasi Pengembangan")?"active":"") %>-->
		<!-- <ol class="breadcrumb">
			<li class="breadcrumb-item active"><a href="#">Profile</a></li>
			<li class="breadcrumb-item"><a href="per-individu-nilai-kompetensi">Nilai Kompetensi</a></li>
			<li class="breadcrumb-item"><a href="per-individu-rekomendasi-pengembangan">Rekomendasi Pengembangan</a></li>
		</ol> -->
		
		<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
		    <li class="nav-item"><a onclick="alert('Halaman tidak tersedia.')" class="nav-link" href="javascript:void(0)">Profile</a></li>
			<li class="nav-item"><a class="nav-link" href="per-individu?mode=nilai-kompetensi">Nilai Kompetensi</a></li>
			<li class="nav-item"><a class="nav-link active" href="#">Rekomendasi Pengembangan</a></li>
		</ul>
		
		<div class="card">
			<div class="card-body">
				
				<div class="row">
					<div class="col-md-2">
						<img src="${assetPath}/images/icon/picture.png" data-src="${assetPath}/images/icon/picture.png" alt="${requestScope.realName}" style="width: 240px" />
					</div>
					<div class="col-md-4 pt-2" style="font-size: 20px">
						<h2 class="mb-1">Acong Kadal, S.Hut</h2>
						<p>KSDAE-Balai Taman Nasional Anu</p>
						<p>Pengendalian Ekosisetem Hutan - Keterampilan - Terampil</p>
						<p style="color: red; font-style: italic">Bidang Konservasi Sumber Daya Hutan</p>
						<div class="mt-3 text-center" style="border: 1px solid black; font-size: 30px; padding: 4px; width: 170px; font-weight: bold;">GOOD</div>
					</div>
					<div class="col-md-4 pt-2 text-right" style="font-size: 20px">
						<span style="font-size: 20px; font-weight: 500">
							Rekomendasi Pengembangan Kompetensi<br/>
							SDM Aparatur Jabatan Fungsional KLHK<br/>
							Pusat Perencanaan dan Pengembangan SDM
						</span>
					</div>
					<div class="col-md-2">
						<img src="${assetPath}/images/logo1.png" data-src="${assetPath}/images/logo1.png" alt="${requestScope.realName}" style="width: 200px" />
					</div>
				</div>
				
				<br/>
				<div style="margin-bottom: 10px">
					<div class="float-left">
						<h3 class="primary-color mt-3">Ringkasan Hasil Pemetaan</h3>
					</div>
					<div class="float-right">
						<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
					</div>
				</div>
				<hr class="mb-5" style="margin-top: 60px"/>
				
				<div class="row">
					<div class="col-md-4">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="bar"></canvas>
						</div>
					</div>
					<div class="col-md-4">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="radar"></canvas>
						</div>
					</div>
					<div class="col-md-4">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Manajerial</h5>
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td>a.</td>
								   <td>Integritas</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="row mt-3 mb-3">
					<div class="col-md-4">
						<h3 class="primary-color text-center">Kompetensi Teknis</h3>
					</div>
					<div class="col-md-4">
						<h3 class="primary-color text-center">Kompetensi Manajerial</h3>
					</div>
					<div class="col-md-4">
						<h3 class="primary-color text-center">Kompetensi Sosiokultural</h3>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="primary-color mt-3">Rekomendasi Pengembangan</h3>
				</div>
				<hr class="mb-3" style="margin-top: 60px"/>
				<div class="row">
					<div class="col-md-12">
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="1" class="text-center">No</th>
									<th width="50" class="text-center">Kode</th>
									<th width="200" class="text-center">Unit Kompetensi Teknis</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
									<th width="150" class="text-center">Bentuk Pengembangan</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td class="text-center">a.</td>
								   <td>KHT.PEH.001.01</td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">50%</td>
								   <td class="text-center">A</td>
								</tr>
								<tr>
								   <td class="text-center">b.</td>
								   <td></td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">80%</td>
								   <td class="text-center">B</td>
								</tr>
								<tr>
								   <td class="text-center">c.</td>
								   <td></td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">80%</td>
								   <td class="text-center">C</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-12 mt-3">
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="1" class="text-center">No</th>
									<th width="250" class="text-center">Kompetensi Manajerial</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
									<th width="150" class="text-center">Bentuk Pengembangan</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td class="text-center">a.</td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">50%</td>
								   <td class="text-center">A</td>
								</tr>
								<tr>
								   <td class="text-center">b.</td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">80%</td>
								   <td class="text-center">B</td>
								</tr>
								<tr>
								   <td class="text-center">c.</td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">80%</td>
								   <td class="text-center">C</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-12 mt-3">
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="1" class="text-center">No</th>
									<th width="250" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
									<th width="150" class="text-center">Bentuk Pengembangan</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td class="text-center">a.</td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">50%</td>
								   <td class="text-center">A</td>
								</tr>
								<tr>
								   <td class="text-center">b.</td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">80%</td>
								   <td class="text-center">B</td>
								</tr>
								<tr>
								   <td class="text-center">c.</td>
								   <td></td>
								   <td class="text-center">70%</td>
								   <td class="text-center">80%</td>
								   <td class="text-center">C</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<p class="mt-3" style="font-size: 24px">
					Pusat Perencanaan dan Pengembangan SDM merekomendasikan program pengembangan kompetensi teknis, 
					manajerial, dan sosokultural, sebagaimana pada tabel di atas berdasarkan hasil pemetaan kompetensi tahun 2021. 
				</p>
				
				<div class="row" style="margin-top: 50px; margin-bottom: 100px">
					<div class="col-md-8"></div>
					<div class="col-md-4">
						<p class="mt-3" style="font-size: 24px">
							Mengetahui, 
						</p>
						<p style="font-size: 24px; margin-top: 120px">
							Nama Lengkap dan Gelar<br/>
							NIP. 123456 788 3345
						</p>
					</div>
				</div>
				
			</div>
		</div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<script src="${assetPath}/js/dashboard.js"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/per-individu.js"></script>