<%@ include file = "inc/header.jsp" %>

<style>
	.form-control,.force {
      width: 350px!important;
    }
    .chart-legend li span{
	    display: inline-block;
	    width: 12px;
	    height: 12px;
	    margin-right: 5px;
	}
	/* canvas#barHorizontalEx{position:relative; z-index:1000;} */
</style>

<!-- MAIN CONTENT-->
<div class="main-content" > <!-- style="padding-top: 110px" -->
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    
		<form id="form-search" >
    	<% if(request.getParameter("mode") !=null) { %>
			<% if(request.getParameter("mode").equals("satker") || request.getParameter("mode").equals("jabatan")) { %>
			<div class="row <%if(request.getParameter("mode").equals("jabatan")) out.print("d-none"); else out.print("");%>">
				<div class="row col-md-10">
						<div class="form-group mb-0 ml-3" style="width: 115px">
							<select name="filter_id_eselon1" class="form-control select2">
								<option value="">Eselon1</option>
							</select>
						</div>
						
					    <c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_ESELON1')}">
							<div class="form-group ml-3 mb-0" style="width: 115px">
								<select name="filter_id_organisasi" class="form-control select2">
									<option value="">BDLHK</option>
								</select>
							</div>
					    </c:if>
					    
						<div class="form-group mb-0 ml-3" style="width: 115px">
							<select name="filter_id_propinsi" class="form-control select2">
								<option value="">Propinsi</option>
							</select>
						</div>
						<div class="form-group mb-0 ml-3" style="width: 230px">
							<select name="filter_id_unit_auditi" class="form-control select2">
								<option value="">Pilih Satker</option>
							</select>
						</div>
						<button type="button" onclick="display()" class="btn btn-primary ml-3"><i class="fa fa-search"></i> CARI</button>
						<button id="btn-reset" type="button" class="btn btn-danger ml-3">CLEAR</button>
				</div>
				<div class="col-md-2">
					<div class="float-right">
						<button id="btn-download" class="btn btn-dark d-none" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
					</div>
				</div>
			</div>
			<% } %>
		<% } %>
    	<%@ include file = "inc/hasil-keseluruhan.jsp" %>
		
		<ul class="breadcrumb nav nav-pills mb-3 mt-2" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link active" onclick="window.location.reload()" href="javascript:void(0);">Demografi</a></li>
			<li class="nav-item"><a class="nav-link " href="../dashboard/per-jabatan-nilai?<%out.print(mode+"&"+kode);%>">Nilai Pemetaan</a></li>
			<li class="nav-item"><a class="nav-link " href="../dashboard/per-jabatan-analisis?<%out.print(mode+"&"+kode);%>">Analisis</a></li>
			<li class="nav-item"><a class="nav-link" href="javascript:void(0);" onclick="goToJabatanRekomendasi();">Rekomendasi Pengembangan</a></li>
		</ul>
		
		
		<div class="card mb-3 d-none">
			<div class="card-body">
		        <div class="row form-inline">
		            <div class="col-md-12">
		                <select id="filter_bidang_teknis" name="filter_id_bidang_teknis" class="form-control form-control-sm">
		                    <option value="">Pilih Bidang</option>
		                </select>
		                <!-- <select id="filter_tingkat_jabatan" name="filter_id_tingkat_jabatan" class="form-control form-control-sm">
		                    <option value="">Pilih Tingkatan</option>
		                </select>
		                <select id="filter_jenjang_jabatan" name="filter_id_jenjang_jabatan" class="form-control form-control-sm">
		                    <option value="">Pilih Jenjang</option>
		                </select> -->
		            </div>
		        </div>
			</div>
		</div>
		
		<div class="card">
			<div id="main-content" class="card-body">
			
				<div style="margin-bottom: 10px">
					<div class="float-left">
						<!-- This mean, all data of responden -->
						<h3 class="primary-color mt-3" title="This mean, all data of responden">Demografi SDM Existing</h3>
					</div>
				</div>
				
				<hr class="" style="margin-top: 60px"/>
				
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="card">
						<div class="card-header bg-white">
							<h4 class="card-title primary-color m-0">% Gender dan Usia</h4>
						</div>
						<div class="card-body">
						<div class="row">
						<div class="col-md-12">
						<div id="ContainerBarHorizontalEx" style=" margin-top: 0px"> <!-- zoom: 1.1764705882352942; -->
							<canvas class="mw-100" id="barHorizontalEx" width="350" height="350"></canvas>
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="card">
						<div class="card-header bg-white">
							<h4 class="card-title primary-color m-0">% Jenjang Jabatan</h4>
						</div>
						<div class="card-body">
						<div class="row">
						<div class="col-md-12">
						<div id="ContainerBarJenjangJabEx" style=" margin-top: 0px"> <!-- zoom: 1.1764705882352942; -->
							<canvas class="mw-100" id="barJenjangJabEx" width="350" height="350"></canvas>
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
				</div>
				
				
				<div style="margin-bottom: 10px">
					<div class="float-left">
						<!-- This mean, only data responden who do login -->
						<h3 class="primary-color mt-3" title="This mean, only data responden who do login">Demografi SDM Hasil Pemetaan</h3>
					</div>
				</div>
				
				<hr class="" style="margin-top: 60px"/>
				
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="card">
						<div class="card-header bg-white">
							<h4 class="card-title primary-color m-0">% Gender dan Usia</h4>
						</div>
						<div class="card-body">
						<div class="row">
						<div class="col-md-12">
						<div id="ContainerBarHorizontal" style=" margin-top: 0px"> <!-- zoom: 1.1764705882352942; -->
							<canvas class="mw-100" id="barHorizontal" width="350" height="350"></canvas>
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="card">
						<div class="card-header bg-white">
							<h4 class="card-title primary-color m-0">% Jenjang Jabatan</h4>
						</div>
						<div class="card-body">
						<div class="row">
						<div class="col-md-12">
						<div id="ContainerBarJenjangJab" style=" margin-top: 0px"> <!-- zoom: 1.1764705882352942; -->
							<canvas class="mw-100" id="barJenjangJab" width="350" height="350"></canvas>
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
				</div>
				
				<br/>
				
			</div>
		</div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/per-jabatan.js"></script>
<script type="text/javascript">
     /*$("#btn-print").on("click", function () {
         $('#main-body').addClass("d-none");
         var divContents = $("#main-content").html();
         var printWindow = window.open('', '', 'height=400,width=800');
         printWindow.document.write('<html><head>'+document.getElementsByTagName('head')[0].innerHTML+'</head>');
         printWindow.document.write('<body>');
         printWindow.document.write(divContents);
         printWindow.document.write('</body></html>');
         printWindow.document.close();
         printWindow.print();
     });*/
 	$('#btn-print').on('click', function (e) {
 		$('aside').remove();
 		$('header').remove();
 		$('.page-container.pl-lg-default').attr("class", "");
 		document.getElementsByClassName("copyright")[0].parentElement.parentElement.remove();
 		document.getElementsByClassName("main-content")[0].className="";
 		$('#pills-tab').hide();
 		$(document.getElementById("form-search").children[0]).hide();
 		$(document.getElementById("form-search").children[2]).hide();
 		$(document.getElementById("form-search").children[3]).hide();
 		$(document.getElementById("form-search").children[4]).hide();
 	});
 </script>