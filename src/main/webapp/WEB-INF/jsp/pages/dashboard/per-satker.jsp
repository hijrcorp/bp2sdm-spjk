<%@ include file = "inc/header.jsp" %>

<style>
</style>

<!-- MAIN CONTENT-->
<div class="main-content" style="padding-top: 110px">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    
		<div class="row">
			<div class="row col-md-10">
				<form class="form-inline">
					<div class="form-group mb-0 ml-3">
						<select name="filter_id_eselon1" class="form-control">
							<option value="">-- Pilih Eselon1 --</option>
						</select>
					</div>
					<div class="form-group mb-0 ml-3">
						<select name="filter_id_eselon1" class="form-control form-control-inline" style="width: auto">
							<option value="">-- Pilih Jabatan --</option>
						</select>
					</div>
					<div class="form-group mb-0 ml-3">
						<select name="filter_id_eselon1" class="form-control form-control-inline" style="width: auto">
							<option value="">-- Pilih Jenjang --</option>
						</select>
					</div>
					<div class="form-group mb-0 ml-3">
						<select name="filter_id_eselon1" class="form-control form-control-inline" style="width: auto">
							<option value="">-- Pilih Nama / NIP --</option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary ml-3"><i class="fa fa-search"></i> CARI</button>
					<button type="button" class="btn btn-primary ml-3">CLEAR</button>
				</form>
			</div>
			<div class="col-md-2">
				<div class="float-right">
					<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
					<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
				</div>
			</div>
		</div>
		
		<br/>

		<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link active" href="per-jabatan-nilai-kompetensi">Hasil Pemetaan Kompetensi PEH</a></li>
			<li class="nav-item"><a class="nav-link" href="per-jabatan-rekomendasi-pengembangan">Rekomendasi Pengembangan</a></li>
		</ul>
		
		<div class="card">
			<div class="card-body">
				
				<div class="row">
					<div class="col-md-2">
						<img src="${assetPath}/images/icon/picture.png" data-src="${assetPath}/images/icon/picture.png" alt="${requestScope.realName}" style="width: 240px" />
					</div>
					<div class="col-md-4 pt-2" style="font-size: 20px">
						<h2 class="mb-1">Acong Kadal, S.Hut</h2>
						<p>KSDAE-Balai Taman Nasional Anu</p>
						<p>Pengendalian Ekosisetem Hutan - Keterampilan - Terampil</p>
						<p style="color: red; font-style: italic">Bidang Konservasi Sumber Daya Hutan</p>
						<div class="mt-3 text-center" style="border: 1px solid black; font-size: 30px; padding: 4px; width: 170px; font-weight: bold;">GOOD</div>
					</div>
					<div class="col-md-4 pt-2 text-right" style="font-size: 20px">
						<span style="font-size: 20px; font-weight: 500">
							Rekomendasi Pengembangan Kompetensi<br/>
							SDM Aparatur Jabatan Fungsional KLHK<br/>
							Pusat Perencanaan dan Pengembangan SDM
						</span>
					</div>
					<div class="col-md-2">
						<img src="${assetPath}/images/logo1.png" data-src="${assetPath}/images/logo1.png" alt="${requestScope.realName}" style="width: 200px" />
					</div>
				</div>
				
				<br/>
				<div style="margin-bottom: 10px">
					<div class="float-left">
						<h3 class="primary-color mt-3">Ringkasan Hasil Pemetaan</h3>
					</div>
					<div class="float-right">
						<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
					</div>
				</div>
				<hr class="mb-5" style="margin-top: 60px"/>
				
				<div class="row">
					<div class="col-md-4">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="barHorizontal"></canvas>
						</div>
					</div>
					<div class="col-md-4">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="bar1"></canvas>
						</div>
					</div>
					<div class="col-md-4">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="bar2"></canvas>
						</div>
					</div>
				</div>
				
				<div class="row mt-3 mb-3">
					<div class="col-md-4">
						<h3 class="primary-color text-center">Kompetensi Teknis</h3>
					</div>
					<div class="col-md-4">
						<h3 class="primary-color text-center">Kompetensi Manajerial</h3>
					</div>
					<div class="col-md-4">
						<h3 class="primary-color text-center">Kompetensi Sosiokultural</h3>
					</div>
				</div>
				
				<br/>
		
				<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
					<li class="nav-item"><a class="nav-link active" href="#">Ahli/Terampil</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Jenjang</a></li>
				</ul>
				
				<div class="float-left">
					<h3 class="primary-color mt-3 mb-3">Kompetensi Teknis</h3>
				</div>
				<hr class="mb-3" style="margin-top: 70px"/>
				<div class="row">
					<div class="col-md-6">
					  <div style="zoom: 1.1764705882352942;">
					    <canvas id="bar"></canvas>
					  </div>
					</div>
					<div class="col-md-6">
						<table id="tbl" class="table table-bordered table-striped-not table-hover">
						   <tbody>
						      <tr>
						         <td rowspan="5" width="62"><p>Kompetensi Inti</p></td>
						         <td width="32"><p>No</p></td>
						         <td width="75"><p>Kode</p></td>
						         <td width="273"><p>Nama Kompetensi</p></td>
						         <td width="66"><p>Skor</p></td>
						      </tr>
						      <tr>
						         <td width="32"><p>1</p></td>
						         <td width="75"><p>KHT.PEH.001.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>2</p></td>
						         <td width="75"><p>KHT.PEH.002.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>3</p></td>
						         <td width="75"><p>KHT.PEH.244.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32">&nbsp;</td>
						         <td width="75">&nbsp;</td>
						         <td width="273"><p>Rata-rata</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="62">&nbsp;</td>
						         <td width="32">&nbsp;</td>
						         <td width="75">&nbsp;</td>
						         <td width="273">&nbsp;</td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="62"><p>Pilihan</p></td>
						         <td width="32"><p>No</p></td>
						         <td width="75"><p>Kode</p></td>
						         <td width="273"><p>Nama Kompetensi</p></td>
						         <td width="66"><p>Skor</p></td>
						      </tr>
						      <tr>
						         <td rowspan="7" width="62"><p>Wisata alam</p></td>
						         <td width="32"><p>1</p></td>
						         <td width="75"><p>KHT.PEH.134.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>2</p></td>
						         <td width="75"><p>KHT.PEH.135.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>3</p></td>
						         <td width="75"><p>KHT.PEH.136.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>4</p></td>
						         <td width="75"><p>KHT.PEH.137.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>5</p></td>
						         <td width="75"><p>KHT.PEH.138.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>6</p></td>
						         <td width="75"><p>KHT.PEH.272.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>7</p></td>
						         <td width="75"><p>KHT.PEH.275.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="62">&nbsp;</td>
						         <td width="32">&nbsp;</td>
						         <td width="75">&nbsp;</td>
						         <td width="273"><p>Rata-rata</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="62">&nbsp;</td>
						         <td width="32">&nbsp;</td>
						         <td width="75">&nbsp;</td>
						         <td width="273">&nbsp;</td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td rowspan="11" width="62"><p>Konservasi jenis</p></td>
						         <td width="32"><p>1</p></td>
						         <td width="75"><p>KHT.PEH.008.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>2</p></td>
						         <td width="75"><p>KHT.PEH.009.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>3</p></td>
						         <td width="75"><p>KHT.PEH.010.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>4</p></td>
						         <td width="75"><p>KHT.PEH.011.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>5</p></td>
						         <td width="75"><p>KHT.PEH.129.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>6</p></td>
						         <td width="75"><p>KHT.PEH.180.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>7</p></td>
						         <td width="75"><p>KHT.PEH.189.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>8</p></td>
						         <td width="75"><p>KHT.PEH.190.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>9</p></td>
						         <td width="75"><p>KHT.PEH.191.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>10</p></td>
						         <td width="75"><p>KHT.PEH.194.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>11</p></td>
						         <td width="75"><p>KHT.PEH.195.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="62">&nbsp;</td>
						         <td width="32">&nbsp;</td>
						         <td width="75">&nbsp;</td>
						         <td width="273"><p>Rata-rata</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="62">&nbsp;</td>
						         <td width="32">&nbsp;</td>
						         <td width="75">&nbsp;</td>
						         <td width="273">&nbsp;</td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td rowspan="7" width="62"><p>Konservasi kawasan</p></td>
						         <td width="32"><p>1</p></td>
						         <td width="75"><p>KHT.PEH.013.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>2</p></td>
						         <td width="75"><p>KHT.PEH.027.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>3</p></td>
						         <td width="75"><p>KHT.PEH.038.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>4</p></td>
						         <td width="75"><p>KHT.PEH.039.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>5</p></td>
						         <td width="75"><p>KHT.PEH.044.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>6</p></td>
						         <td width="75"><p>KHT.PEH.182.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="32"><p>7</p></td>
						         <td width="75"><p>KHT.PEH.183.01</p></td>
						         <td width="273"><p>&nbsp;</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						      <tr>
						         <td width="62">&nbsp;</td>
						         <td width="32">&nbsp;</td>
						         <td width="75">&nbsp;</td>
						         <td width="273"><p>Rata-rata</p></td>
						         <td width="66">&nbsp;</td>
						      </tr>
						   </tbody>
						</table>
					</div>
				</div>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Manajerial</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="radar"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Manajerial</h5>
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Manajerial</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td>a.</td>
								   <td>Integritas</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>b.</td>
								   <td>Kemampuan menghadapi perubahan (Ability to change)</td>
								   <td class="text-center">4</td>
								   <td class="text-center">3</td>
								</tr>
								<tr>
								   <td>c.</td>
								   <td>Perencanaan yang terorganisasi (Planning Organizing)</td>
								   <td class="text-center">3</td>
								   <td class="text-center">2</td>
								</tr>
								<tr>
								   <td>d.</td>
								   <td>Kemampuan berkomunikasi (Communication Skills)</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>e.</td>
								   <td>Membangun relasi (Relationship Building)</td>
								   <td class="text-center">3</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>f.</td>
								   <td>Berpikir analitis</td>
								   <td class="text-center">4</td>
								   <td class="text-center">3</td>
								</tr>
								<tr>
								   <td>g.</td>
								   <td>Kepemimpinan (Leadership)</td>
								   <td class="text-center">3</td>
								   <td class="text-center">3</td>
								</tr>
								<tr>
								   <td>h.</td>
								   <td>Kerjasama (Teamwork)</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>i.</td>
								   <td>Tanggapan terhadap pengaruh budaya</td>
								   <td class="text-center">3</td>
								   <td class="text-center">3</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Sosiokultural</h5>
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td>a.</td>
								   <td>Perekat Bangsa</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/><br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
				</div>
				<hr class="mt-5"/>
				<p class="mt-3" style="font-size: 24px; padding: 0px 20px 0px 20px">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis finibus eros. Donec ut venenatis quam, convallis lobortis nunc. Suspendisse tincidunt auctor nunc fermentum elementum. Morbi quis lacus quis turpis bibendum scelerisque. Phasellus imperdiet dolor a magna semper euismod. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut auctor ante tellus, ac mattis quam venenatis imperdiet. Donec in pulvinar enim. Suspendisse id pharetra massa. Maecenas eros nibh, aliquam ac pellentesque a, vulputate id ex.
				</p>
				
				<br/><br/><br/><br/><br/><br/>
				
			</div>
		</div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<script src="${assetPath}/js/dashboard.js"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/per-satker.js"></script>