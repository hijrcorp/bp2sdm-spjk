<%@ include file = "inc/header.jsp" %>

<div id="wrapper" class="main-content">

	<div id="content-wrapper" class="section__content section__content--p20">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Daftar Tabulasi</a></li>
				<li class="breadcrumb-item active">Tabulasi 1</li>
			</ol>
			
			<div class="card">
				<div class="card-header">
					<h4>Kuesioner Survei Persepsi</h4>
				</div>
				<div class="card-body">
					<div class="default-tab">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true"  class="nav-item nav-link">Lingkungan Pengendalian</a>
								<a id="nav-2-tab" data-toggle="tab" href="#nav-2" role="tab" aria-controls="nav-2" aria-selected="false" class="nav-item nav-link">Penilaian Risiko</a>
								<a id="nav-3-tab" data-toggle="tab" href="#nav-3" role="tab" aria-controls="nav-3" aria-selected="false" class="nav-item nav-link">Kegiatan Pengendalian</a>
								<a id="nav-4-tab" data-toggle="tab" href="#nav-4" role="tab" aria-controls="nav-4" aria-selected="false" class="nav-item nav-link">Informasi &nbsp; Komunikasi</a>
								<a id="nav-5-tab" data-toggle="tab" href="#nav-5" role="tab" aria-controls="nav-5" aria-selected="false" class="nav-item nav-link active">Pemantauan</a>
							</div>
						</nav>
						<div class="tab-content pl-3 pt-3">
							<div id="nav-1" aria-labelledby="nav-1-tab" role="tabpanel" class="tab-pane fade">
								<p>1. Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth
								master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh
								dreamcatcher synth. Cosby sweater eu banh mi, irure terry richardson ex sd. Alip placeat salvia cillum iphone.
								Seitan alip s cardigan american apparel, butcher voluptate nisi .</p>
							</div>
							<div id="nav-2" aria-labelledby="nav-2-tab" role="tabpanel" class="tab-pane fade">
								<p>2. Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth
								master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh
								dreamcatcher synth. Cosby sweater eu banh mi, irure terry richardson ex sd. Alip placeat salvia cillum iphone.
								Seitan alip s cardigan american apparel, butcher voluptate nisi .</p>
							</div>
							<div id="nav-3" aria-labelledby="nav-3-tab" role="tabpanel" class="tab-pane fade">
								<p>3. Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth
								master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh
								dreamcatcher synth. Cosby sweater eu banh mi, irure terry richardson ex sd. Alip placeat salvia cillum iphone.
								Seitan alip s cardigan american apparel, butcher voluptate nisi .</p>
							</div>
							<div id="nav-4" aria-labelledby="nav-4-tab" role="tabpanel" class="tab-pane fade">
								<p>4. Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth
								master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh
								dreamcatcher synth. Cosby sweater eu banh mi, irure terry richardson ex sd. Alip placeat salvia cillum iphone.
								Seitan alip s cardigan american apparel, butcher voluptate nisi .</p>
							</div>
							<div id="nav-5" aria-labelledby="nav-5-tab" role="tabpanel" class="tab-pane fade show active">
							
								<div class="card mb-1">
									<div class="card-body">
										<h3 class="card-text"><span class="custom-title">Pemantauan Berkelanjutan (5.1)</span> <i class="fa fa-caret-down"></i></h3>
									</div>
								</div>
								<div class="card">
									<div class="card-body" style="border-bottom: 1px solid #ccc;">
										<h4 class="card-text"><span class="custom-title">Pemantauan Berkelanjutan (5.1)</span> <i class="fa fa-caret-down"></i></h4>
									</div>
									<div class="card-body pl-5 px-5">
										<table id="tbl-data-2" class="table">
											<tbody>
												<tr>
													<td width="50">1</td>
													<td width="500">Apakah daftar risiko telah dimutakhirkan secara terus menerus sesuai dengan perubahan kebutuhan atau harapan stakeholders dan telah dilakukan pemantauan otomatis/online oleh pimpinan organisasi?</td>
													<td width="250"><select class="form-control"><option>Ya</option><option>Tidak</option></select></td>
												</tr>
												<tr>
													<td>2</td>
													<td>Apakah satker telah mengidentifikasi dan menetapkan kegiatan yang dibutuhkan untuk menyelesaikan tugas dan fungsi pada masing-masing posisi/jabatan.</td>
													<td><select class="form-control"><option>Ya</option><option>Tidak</option></select></td>
												</tr>
												<tr>
													<td>3</td>
													<td>Apakah unsur pimpinan satker melakukan interaksi yang cukup intensif dengan level di bawahnya.</td>
													<td><select class="form-control"><option>Ya</option><option>Tidak</option></select></td>
												</tr>
												<tr>
													<td>4</td>
													<td>Apakah IKU telah digunakan untuk mengukur kinerja organisasi/ unit organisasi/ unit kerja Saudara?</td>
													<td><select class="form-control"><option>Ya</option><option>Tidak</option></select></td>
												</tr>
												<tr>
													<td>5</td>
													<td>Apakah unsur pimpinan satker telah menerapkan manajemen berbasis kinerja.</td>
													<td><select class="form-control"><option>Ya</option><option>Tidak</option></select></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.container-fluid -->

		<%@ include file = "inc/trademark.jsp" %>	

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Form Modal-->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="entry-form">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
						<div class="form-row">
							<div class="form-group col-md-3">
								<label>Nama/Kode Kandang</label>
							</div>
							<div class="form-group col-md-9">
								<input type="text" class="form-control" name="nama" placeholder="Buaya01 / Burung-P1" autocomplete="off">
							</div>
						</div>
									
						<div class="form-group row">
							<div class="col-sm-3">
								<label>Jenis Kandang</label>
							</div>
							<div class="col-sm-9">
								<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="customRadioInline1" name="jenis" class="custom-control-input" value="INDIVIDUAL">
								<label class="custom-control-label" for="customRadioInline1">Individual</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="customRadioInline2" name="jenis" class="custom-control-input" value="KOLONI">
								<label class="custom-control-label" for="customRadioInline2">Koloni</label>
							</div>
							</div>
						</div>
					
						<div class="form-group row">
							<div class="col-md-3">
								<label>Taksa Satwa</label>
							</div>
							<div class="col-md-9">
								<select name="id_taksa" class="form-control">
								</select>
							</div>
						</div> 
						
						<div class="form-group row">
							<div class="col-sm-3">
					 			<label>Kapasitas Kandang</label>
							</div>
							<div class="col-sm-9">
								<div class="input-group">
									<input type="number" class="form-control" name="kapasitas" aria-label="Kapasitas Kandang">
									<div class="input-group-append">
										<span class="input-group-text">Ekor</span>
									</div>
								</div>
							</div>
						</div>
					
					<div class="form-group row">
						<div class="col-sm-3">
							<label>Kondisi Kandang</label>
						</div>
						<div class="col-sm-9">
							<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" id="customRadioInline3" name="status_kondisi" class="custom-control-input" value="BAIK">
							<label class="custom-control-label" for="customRadioInline3">Kondisi Baik</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" id="customRadioInline5" name="status_kondisi" class="custom-control-input" value="PERLU_PERBAIKAN">
							<label class="custom-control-label" for="customRadioInline5">Perlu Perbaikan</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" id="customRadioInline6" name="status_kondisi" class="custom-control-input" value="TIDAK_DAPAT_DIGUNAKAN">
							<label class="custom-control-label" for="customRadioInline6">Tidak Dapat Digunakan</label>
						</div>
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-sm-3">
							<label>Keterangan Kondisi</label>
						</div>
						<div class="col-sm-9">
							<textarea class="form-control" name="keterangan_kondisi" rows="3"></textarea>
						</div>
					</div>
				
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
					<button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="${assetPath}/js/tab1.js"></script>
<%@ include file = "inc/footer.jsp" %>