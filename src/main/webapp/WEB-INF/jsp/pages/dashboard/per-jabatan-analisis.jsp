<%@ include file = "inc/header.jsp" %>

<style>
	.form-control-force {
      width: 350px!important;
    }
    .chart-legend li span{
	    display: inline-block;
	    width: 12px;
	    height: 12px;
	    margin-right: 5px;
	}
	
	.chartWrapper {
	  position: relative;
	  overflow-x: scroll;
	}
	
	.chartWrapper > canvas {
	  position: absolute;
	  left: 0;
	  top: 0;
	  pointer-events: none;
	}
	
	.chartAreaWrapper {
	  width: 1000px;
	}
	
	.chartAreaWrapper-50 {
	  width: 500px;
	}
	
	::-webkit-scrollbar {
	    -webkit-appearance: none;
	    width: 7px;
	}
	::-webkit-scrollbar-thumb {
	    border-radius: 4px;
	    background-color: rgba(0,0,0,.5);
	    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
	}
		
</style>

<!-- MAIN CONTENT-->
<div class="main-content" > <!-- style="padding-top: 110px" -->
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    
		<form id="form-search" >
    	<% if(request.getParameter("mode") !=null) { %>
			<% if(request.getParameter("mode").equals("satker") || request.getParameter("mode").equals("jabatan")) { %>
			<div class="row <%if(request.getParameter("mode").equals("jabatan")) out.print("d-none"); else out.print("");%>">
				<div class="row col-md-10">
						<!-- <div class="form-group mb-0 ml-3" style="width: 350px">
							<select name="filter_id_eselon1" class="form-control select2">
								<option value="">Pilih Eselon1</option>
							</select>
						</div>
							<div class="form-group mb-0 ml-3" style="width: 350px">
							<select name="filter_id_unit_auditi" class="form-control select2">
								<option value="">Pilih Satker</option>
							</select>
						</div>
						<button type="button" onclick="display(1)" class="btn btn-primary ml-3"><i class="fa fa-search"></i> CARI</button>
						<button id="btn-reset" type="button" class="btn btn-danger ml-3">CLEAR</button> -->
						
						
						
						<div class="form-group mb-0 ml-3" style="width: 115px">
							<select name="filter_id_eselon1" class="form-control select2">
								<option value="">Eselon1</option>
							</select>
						</div>
						
					    <c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_ESELON1')}">
							<div class="form-group mb-0 ml-3" style="width: 115px">
								<select name="filter_id_organisasi" class="form-control select2">
									<option value="">BDLHK</option>
								</select>
							</div>
					    </c:if>
					    
						<div class="form-group mb-0 ml-3" style="width: 115px">
							<select name="filter_id_propinsi" class="form-control select2">
								<option value="">Propinsi</option>
							</select>
						</div>
						<div class="form-group mb-0 ml-3" style="width: 230px">
							<select name="filter_id_unit_auditi" class="form-control select2">
								<option value="">Pilih Satker</option>
							</select>
						</div>
						<button type="button" onclick="display(1)" class="btn btn-primary ml-3"><i class="fa fa-search"></i> CARI</button>
						<button id="btn-reset" type="button" class="btn btn-danger ml-3">CLEAR</button>
					
				</div>
				<div class="col-md-2">
					<div class="float-right">
						<button id="btn-download" class="btn btn-dark d-none" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
					</div>
				</div>
			</div>
			<% } %>
		<% } %>
    	<%@ include file = "inc/hasil-keseluruhan.jsp" %>
		
		<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link" href="../dashboard/per-jabatan?<%out.print(mode+"&"+kode);%>">Demografi</a></li>
			<li class="nav-item"><a class="nav-link" href="../dashboard/per-jabatan-nilai?<%out.print(mode+"&"+kode);%>">Nilai Pemetaan</a></li>
			<li class="nav-item"><a class="nav-link active" onclick="window.location.reload();" href="javascript:void(0);">Analisis</a></li>
			<%-- <li class="nav-item"><a class="nav-link" href="../dashboard/per-jabatan-rekomendasi?<%out.print(mode+"&"+kode);%>">Rekomendasi Pengembangan</a></li> --%>
			<li class="nav-item"><a class="nav-link" href="javascript:void(0);" onclick="goToJabatanRekomendasi();">Rekomendasi Pengembangan</a></li>
			<!-- goToJabatanRekomendasi(); -->
		</ul>
		
		
		<div class="card mb-3">
			<div class="card-body">
		        <div class="row form-inline">
		            <div class="col-md-12">
		                <select id="filter_bidang_teknis" name="filter_id_bidang_teknis" class="form-control form-control-sm">
		                    <option value="">Pilih Bidang</option>
		                </select>
		                <select id="filter_tingkat_jabatan" name="filter_id_tingkat_jabatan" class="form-control form-control-sm">
		                    <option value="">Pilih Tingkatan</option>
		                </select>
		                <select id="filter_jenjang_jabatan" name="filter_id_jenjang_jabatan" class="form-control form-control-sm">
		                    <option value="">Pilih Jenjang</option>
		                </select>
		            </div>
		        </div>
			</div>
		</div>
		
		<div class="card">
			<div class="card-body">
				<h3 class="secondary-color " style="font-size: 18px;">Grafik Kompetensi Teknis</h3>
				<hr class=""/>
				<div class="row justify-content-center">
					<div class="col-md-12">
					<div class="chartWrapper">
					  <div class="chartAreaWrapper">
					  <div id="container-bar-teknis" class="chartAreaWrapper2">
					      <!-- <canvas id="chart-Test" height="300" width="1200"></canvas> -->
										
							<!-- <div id="container-bar-inti" class="chart-container" style="position: relative; overflow-x: scroll"> --> <!-- zoom: 1.1764705882352942;  -->
								<canvas id="bar-teknis" height="400" width="400"></canvas>
							<!-- </div> -->
							<!-- <div id="container-legend1" class="chart-legend d-flex justify-content-center"></div> -->
					  </div>
					  </div>
					</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="card">
			<div class="card-body">
				<h3 class="secondary-color " style="font-size: 18px;">Penerapan dan Penguasaan Kompetensi Teknis</h3>
				<hr class=""/>
				<div class="row justify-content-center">
					<div class="col-md-5">
						<div class="card">
							<div class="card-header">Penerapan Standar Kompetensi</div>
							<div class="card-body">
								<canvas id="pie-chart"></canvas>
							</div>
						</div>
						
						<div class="card">
							<div class="card-header">Penguasaan Standard Kompetensi <!-- POLHUT  --></div>
							<div class="card-body">
								<canvas id="pie-chart-penguasaan"></canvas>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="card">
							<div class="card-header">Performa Fungsi Utama Standard Kompetensi</div>
							<div class="card-body">
							
							<div class="chartWrapper">
							  <div class="chartAreaWrapper-50">
							  <div id="container-bar-fungsi-utama" class="chartAreaWrapper2">
							      <!-- <canvas id="chart-Test" height="300" width="1200"></canvas> -->
												
									<!-- <div id="container-bar-inti" class="chart-container" style="position: relative; overflow-x: scroll"> --> <!-- zoom: 1.1764705882352942;  -->
										<canvas id="bar-fungsi-utama" height="400" width="400"></canvas>
									<!-- </div> -->
									<!-- <div id="container-legend1" class="chart-legend d-flex justify-content-center"></div> -->
							  </div>
							  </div>
							</div>
							
					  			<!-- <div id="container-bar-fungsi-utama" class="chartAreaWrapper2">
									<canvas id="bar-fungsi-utama"></canvas>
								</div> -->
								<div id="container-legend-bar-fungsi-utama" class="chart-legend d-flex justify-content-center"></div>
							</div>
						</div>
						
						<div class="card table-responsive">
						<table id="tbl-fungsi-utama" class="table table-sm" style="width:max-content;!important">
						  <tbody>
						  </tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ... content in page 1 ... -->
		<p style="page-break-after: always;">&nbsp;</p>
		<!-- <p style="page-break-before: always;">&nbsp;</p>
		... content in page 2 ... -->
		<div class="card">
			<div class="card-body">
				<!-- <div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Manajerial</h3>
				</div>
				<hr class="mt-5"/> -->
				<h3 class="secondary-color " style="font-size: 18px;">Nilai Kompetensi Manajerial</h3>
				<hr class=""/>
				<div class="row d-none">
					<div class="col-md-12 table-responsive">
						<!-- <h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Nilai Kompetensi Manajerial</h5> -->
						
						<table id="tbl-komp-manaj" class="table table-bordered">
							<thead>
								<tr id="tr-head-manaj" class="bg-secondary text-white">
									<!-- <th class="align-middle">NO</th> -->
									<th width="30%" class="align-middle text-center">Hasil Pemetaan Kompetensi Manajerial / Nilai Standard</th>
									<!-- <th class="align-middle text-center" style="writing-mode: tb-rl;transform: rotateZ(180deg);">PEMULA</th>
									<th class="align-middle text-center" style="writing-mode: tb-rl;transform: rotateZ(180deg);">MAHIR</th>
									<th class="align-middle text-center" style="writing-mode: tb-rl;transform: rotateZ(180deg);">PENYELIA</th>
									<th class="align-middle text-center" style="writing-mode: tb-rl;transform: rotateZ(180deg);">PERTAMA</th>
									<th class="align-middle text-center" style="writing-mode: tb-rl;transform: rotateZ(180deg);">MUDA</th>
									<th class="align-middle text-center" style="writing-mode: tb-rl;transform: rotateZ(180deg);">MADYA</th> -->
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>

				<div id="container-manajerial" class="row row-cols-1 row-cols-md-3 mt-3">
				  
				</div>
			</div>
		</div>
		
		
		<style>
			.wrapper-new {
			  width:60%;
			  display:block;
			  overflow:hidden;
			  margin:0 auto;
			  padding: 60px 50px;
			  background:#fff;
			  border-radius:4px;
			}
			
			.wrapper-canvas{ 
			  background:#fff;
			  height:400px;
			}

		</style>
		<p style="page-break-before: always;">&nbsp;</p>
		<div class="card">
			<div class="card-body">
				<!-- <div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
				</div>
				<hr class="mt-5"/> -->
				<h3 class="secondary-color " style="font-size: 18px;">Nilai Kompetensi Sosiokultural</h3>
				<hr class=""/>
				<div class="row">
					<div class="col-md-12">
						<div class="wrapper-new">
							<canvas class="wrapper-canvas" id="chart-sosiokultural" width="400" height="400"></canvas>
						</div>
						<!-- <h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Nilai Kompetensi Sosiokultural</h5> -->
						<table id="tbl-komp-sosio" class="table table-bordered d-none">
							<thead>
								<tr class="bg-secondary text-white">
									<th width="35%" class="align-middle text-center">JENJANG</th>
									<th class="align-middle text-center">HASIL PEMETAAN</th>
									<th class="align-middle text-center">NILAI STANDAR</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
		
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
<%@ include file = "inc/footer.jsp" %>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
<script src="${assetPath}/js/per-jabatan-analisis.js"></script>
<script>
	$('#btn-print').on('click', function (e) {
		$('aside').remove();
		$('header').remove();
		$('.page-container.pl-lg-default').attr("class", "");
		document.getElementsByClassName("copyright")[0].parentElement.parentElement.remove();
		document.getElementsByClassName("main-content")[0].className="";
		$('#pills-tab').hide();
		$(document.getElementById("form-search").children[0]).hide();
		$(document.getElementById("form-search").children[2]).hide();
		$(document.getElementById("form-search").children[3]).hide();
		$(document.getElementById("form-search").children[4]).hide();
	});
</script>
