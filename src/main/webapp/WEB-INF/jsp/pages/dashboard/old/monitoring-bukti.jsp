<%@ include file = "inc/header.jsp" %>

<div id="wrapper" class="main-content">

	<div id="content-wrapper" class="section__content section__content--p20">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Pengumpulan Bukti</a></li>
				<li class="breadcrumb-item active" id="menu"></li>
			</ol>
			
			<div class="card">
				<div class="card-header">
					<h4>
						<span id="menu"></span>
			      <span class="float-right">
			        <button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i> Unduh</button>
			      </span>
				  </h4>
				</div>
				<div class="card-body">
				
					<div class="card">
						<div class="card-body">
							<div class="form-group row">
								<div class="col-md-2">
									<label>Tahun</label>
								</div>
								<div class="col-md-4">
									<select name="filter_tahun" class="form-control">
										<option value="">-- Pilih Tahun --</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label>Eselon1</label>
								</div>
								<div class="col-md-10">
									<select name="filter_id_eselon1" class="form-control">
										<option value="">-- Pilih Eselon1 --</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				
					<div class="default-tab">
						<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
						<div class="tab-content pl-0 pt-0 table-responsive" id="nav-tab-content">
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.container-fluid -->

		<%@ include file = "inc/trademark.jsp" %>	

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->



<div id="template-content" style="display: none">
	<div class="card pt-2 table-responsive" id="card-data-index" style="border-top: 0px">
	</div>
</div>


<script src="${assetPath}/js/monitoring-bukti.js"></script>
<%@ include file = "inc/footer.jsp" %>