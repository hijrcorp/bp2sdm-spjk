<%@ include file = "inc/header.jsp" %>

<style>
</style>

<!-- MAIN CONTENT-->
<div class="main-content" style="padding-top: 110px">
  <div class="section__content section__content--p30">
    <div class="container-fluid">

		<!-- Breadcrumbs <%= (request.getParameter("jenis") != null && request.getParameter("jenis").equals("Rekomendasi Pengembangan")?"active":"") %>-->
		<!-- <ol class="breadcrumb">
			<li class="breadcrumb-item active"><a href="#">Profile</a></li>
			<li class="breadcrumb-item"><a href="per-individu-nilai-kompetensi">Nilai Kompetensi</a></li>
			<li class="breadcrumb-item"><a href="per-individu-rekomendasi-pengembangan">Rekomendasi Pengembangan</a></li>
		</ol> -->
		
		<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link active" href="#">Profile</a></li>
			<li class="nav-item"><a class="nav-link" href="per-individu-nilai-kompetensi">Nilai Kompetensi</a></li>
			<li class="nav-item"><a class="nav-link" href="per-individu-rekomendasi-pengembangan">Rekomendasi Pengembangan</a></li>
		</ul>
		
		<div class="card">
			<div class="card-body">
				
				<div class="row">
					<div class="col-md-2">
						<img src="${assetPath}/images/icon/picture.png" data-src="${assetPath}/images/icon/picture.png" alt="${requestScope.realName}" style="width: 240px" />
					</div>
					<div class="col-md-10 pt-2" style="font-size: 20px">
						<h2 class="mb-1">Acong Kadal, S.Hut</h2>
						<p>KSDAE-Balai Taman Nasional Anu</p>
						<p>Pengendalian Ekosisetem Hutan - Keterampilan - Terampil</p>
						<p style="color: red; font-style: italic">Bidang Konservasi Sumber Daya Hutan</p>
						<br/>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-3">
								<div class="float-left text-right" style="border: 0px solid black;">
									Kompetensi<br/>Manajerial
								</div>
								<div class="" style="border: 0px solid black;">
									<button class="btn btn-success mt-1 ml-4 " style="font-size: 22px; padding: 9px; width: 170px; font-weight: bold;">67%</button>
								</div>
							</div>
							<div class="col-md-3">
								<div class="float-left text-right">
									Kompetensi<br/>Manajerial
								</div>
								<div class="">
									<button class="btn btn-success mt-1 ml-4 " style="font-size: 22px; padding: 9px; width: 170px; font-weight: bold;">70%</button>
								</div>
							</div>
							<div class="col-md-3">
								<div class="float-left text-right">
									Kompetensi<br/>Manajerial
								</div>
								<div class="">
									<button class="btn btn-success mt-1 ml-4 " style="font-size: 22px; padding: 9px; width: 170px; font-weight: bold;">55%</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<br/>
				<div class="float-left">
					<h3 class="primary-color mt-3">Nilai Kompetensi</h3>
				</div>
				<div class="float-right" style="margin-top: -0px">
					<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
					<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
				</div>
				<hr class="mt-5"/>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Manajerial</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="radar"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Manajerial</h5>
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Manajerial</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td>a.</td>
								   <td>Integritas</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>b.</td>
								   <td>Kemampuan menghadapi perubahan (Ability to change)</td>
								   <td class="text-center">4</td>
								   <td class="text-center">3</td>
								</tr>
								<tr>
								   <td>c.</td>
								   <td>Perencanaan yang terorganisasi (Planning Organizing)</td>
								   <td class="text-center">3</td>
								   <td class="text-center">2</td>
								</tr>
								<tr>
								   <td>d.</td>
								   <td>Kemampuan berkomunikasi (Communication Skills)</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>e.</td>
								   <td>Membangun relasi (Relationship Building)</td>
								   <td class="text-center">3</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>f.</td>
								   <td>Berpikir analitis</td>
								   <td class="text-center">4</td>
								   <td class="text-center">3</td>
								</tr>
								<tr>
								   <td>g.</td>
								   <td>Kepemimpinan (Leadership)</td>
								   <td class="text-center">3</td>
								   <td class="text-center">3</td>
								</tr>
								<tr>
								   <td>h.</td>
								   <td>Kerjasama (Teamwork)</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
								<tr>
								   <td>i.</td>
								   <td>Tanggapan terhadap pengaruh budaya</td>
								   <td class="text-center">3</td>
								   <td class="text-center">3</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Sosiokultural</h5>
						<table id="tbl" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								   <td>a.</td>
								   <td>Perekat Bangsa</td>
								   <td class="text-center">4</td>
								   <td class="text-center">4</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Teknis</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<div style="zoom: 1.1764705882352942; margin-top: 0px">
							<canvas id="bar"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Peta Kompetensi PEH-Keterampilan-Terampil-Bidang KSDH</h5>
						<table id="tbl" class="table table-bordered">
							<tbody>
								<tr>
								   <td rowspan="5" width="62"><p>Kompetensi Inti</p></td>
								   <td width="32"><p>No</p></td>
								   <td width="75"><p>Kode</p></td>
								   <td width="273"><p>Nama Kompetensi</p></td>
								   <td width="66"><p>Skor</p></td>
								</tr>
								<tr>
								   <td width="32"><p>1</p></td>
								   <td width="75"><p>KHT.PEH.001.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>2</p></td>
								   <td width="75"><p>KHT.PEH.002.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>3</p></td>
								   <td width="75"><p>KHT.PEH.244.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32">&nbsp;</td>
								   <td width="75">&nbsp;</td>
								   <td width="273"><p>Rata-rata</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="62">&nbsp;</td>
								   <td width="32">&nbsp;</td>
								   <td width="75">&nbsp;</td>
								   <td width="273">&nbsp;</td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="62"><p>Pilihan</p>
								   <td width="32"><p>No</p></td>
								   <td width="75"><p>Kode</p></td>
								   <td width="273"><p>Nama Kompetensi</p></td>
								   <td width="66"><p>Skor</p></td>
								</tr>
								<tr>
								   <td rowspan="7" width="62"><p>Wisata alam</p></td>
								   <td width="32"><p>1</p></td>
								   <td width="75"><p>KHT.PEH.134.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>2</p></td>
								   <td width="75"><p>KHT.PEH.135.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>3</p></td>
								   <td width="75"><p>KHT.PEH.136.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>4</p></td>
								   <td width="75"><p>KHT.PEH.137.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>5</p></td>
								   <td width="75"><p>KHT.PEH.138.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>6</p></td>
								   <td width="75"><p>KHT.PEH.272.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>7</p></td>
								   <td width="75"><p>KHT.PEH.275.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="62">&nbsp;</td>
								   <td width="32">&nbsp;</td>
								   <td width="75">&nbsp;</td>
								   <td width="273"><p>Rata-rata</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="62">&nbsp;</td>
								   <td width="32">&nbsp;</td>
								   <td width="75">&nbsp;</td>
								   <td width="273">&nbsp;</td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td rowspan="11" width="62"><p>Konservasi jenis</p></td>
								   <td width="32"><p>1</p></td>
								   <td width="75"><p>KHT.PEH.008.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>2</p></td>
								   <td width="75"><p>KHT.PEH.009.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>3</p></td>
								   <td width="75"><p>KHT.PEH.010.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>4</p></td>
								   <td width="75"><p>KHT.PEH.011.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>5</p></td>
								   <td width="75"><p>KHT.PEH.129.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>6</p></td>
								   <td width="75"><p>KHT.PEH.180.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>7</p></td>
								   <td width="75"><p>KHT.PEH.189.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>8</p></td>
								   <td width="75"><p>KHT.PEH.190.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>9</p></td>
								   <td width="75"><p>KHT.PEH.191.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>10</p></td>
								   <td width="75"><p>KHT.PEH.194.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>11</p></td>
								   <td width="75"><p>KHT.PEH.195.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="62">&nbsp;</td>
								   <td width="32">&nbsp;</td>
								   <td width="75">&nbsp;</td>
								   <td width="273"><p>Rata-rata</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="62">&nbsp;</td>
								   <td width="32">&nbsp;</td>
								   <td width="75">&nbsp;</td>
								   <td width="273">&nbsp;</td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td rowspan="7" width="62"><p>Konservasi kawasan</p></td>
								   <td width="32"><p>1</p></td>
								   <td width="75"><p>KHT.PEH.013.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>2</p></td>
								   <td width="75"><p>KHT.PEH.027.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>3</p></td>
								   <td width="75"><p>KHT.PEH.038.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>4</p></td>
								   <td width="75"><p>KHT.PEH.039.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>5</p></td>
								   <td width="75"><p>KHT.PEH.044.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>6</p></td>
								   <td width="75"><p>KHT.PEH.182.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="32"><p>7</p></td>
								   <td width="75"><p>KHT.PEH.183.01</p></td>
								   <td width="273"><p>&nbsp;</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
								<tr>
								   <td width="62">&nbsp;</td>
								   <td width="32">&nbsp;</td>
								   <td width="75">&nbsp;</td>
								   <td width="273"><p>Rata-rata</p></td>
								   <td width="66">&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			
			</div>
		</div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<script src="${assetPath}/js/dashboard.js"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/per-individu.js"></script>