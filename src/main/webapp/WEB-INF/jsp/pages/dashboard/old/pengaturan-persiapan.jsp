<%@ include file = "inc/header.jsp" %>

<div id="wrapper" class="main-content">

	<div id="content-wrapper" class="section__content section__content--p20">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
				<li class="breadcrumb-item active" id="menu"></li>
			</ol>
			
			<div class="card">
				<div class="card-header">
					<h4><span id="menu"></span></h4>
				</div>
				<div class="card-body">
					<table id="tbl" class="table table-bordered table-striped table-hover" style="table-layout: fixed">
						<thead>
							<tr>
								<th width="10"  class="text-center">No</th>
								<th width="400" class="text-center">Judul</th>
								<th width="100" class="text-center">Bobot</th>
								<th width="100" class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>

		</div>
		<!-- /.container-fluid -->

		<%@ include file = "inc/trademark.jsp" %>	

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->


<script src="${assetPath}/js/pengaturan-persiapan.js"></script>
<%@ include file = "inc/footer.jsp" %>