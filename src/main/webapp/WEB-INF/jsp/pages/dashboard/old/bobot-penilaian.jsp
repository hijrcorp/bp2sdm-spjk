<%@ include file = "inc/header.jsp" %>

<div id="wrapper" class="main-content">

	<div id="content-wrapper" class="section__content section__content--p20">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Bobot Penilaian</a></li>
				<li class="breadcrumb-item active" id="menu"></li>
			</ol>
			
			<div class="card">
				<div class="card-header">
					<h4>
						<span id="menu"></span>
			      <span class="float-right">
			        <button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i> Unduh</button>
			      </span>
				  </h4>
				</div>
				<div class="card-body">
				
					<div class="card">
						<div class="card-body">
							<div class="form-group row">
								<div class="col-md-2">
									<label>Tahun</label>
								</div>
								<div class="col-md-4">
									<select name="filter_tahun" class="form-control">
										<option value="">-- Pilih Tahun --</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label>Unit Auditi</label>
								</div>
								<div class="col-md-10">
									<select name="filter_id_unit_auditi" class="form-control">
										<option value="">-- Pilih Unit Auditi --</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				
					<table id="tbl" class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th rowspan="2" colspan="2" width="70" class="text-center">No</th>
								<th rowspan="2" width="900" class="text-center">Unsur</th>
								<th colspan="4" width="100" class="text-center">Hasil Penilaian</th>
							</tr>
							<tr>
								<th class="text-center" nowrap>Jumlah<br/>Bobot</th>
								<th class="text-center" nowrap>Bobot<br/>Sub Unsur</th>
								<th class="text-center" nowrap>Tingkat Maturitas<br/>(1/2/3/4/5)*</th>
								<th class="text-center" width="130">Skor</th>
							</tr>
						</thead>
						<tbody>
							<tr><td colspan="10"><p class="text-center pt-3 pb-3">Silahkan pilih <b>Tahun</b> dan <b>Unit Auditi</b> terlebih dahulu.</p></td></tr>
						</tbody>
					</table>
					
				</div>
			</div>

		</div>
		<!-- /.container-fluid -->

		<%@ include file = "inc/trademark.jsp" %>	

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->



<div id="template-content" style="display: none">
	
</div>


<script src="${assetPath}/js/bobot-penilaian.js"></script>
<%@ include file = "inc/footer.jsp" %>