  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <head>

    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Pusat Penyelamatan Satwa">
    <meta name="author" content="PT. Hijr Global Solution">
    
    <c:set var="contextPath" scope="session" value="${pageContext.request.contextPath}" />
    <c:set var="userPicture" scope="session" value="https://menlhk.hijr.co.id/sso/images/user.png?1543544826379" />

    <title>Pemetaan Kompetensi Jabatan Fungsional - SIM Pengawasan Itjen</title>
    
    <link rel="icon" type="image/png" sizes="32x32" href="${assetPath}/images/icon/favicon.ico">
    <%-- <link href="${assetPath}/images/logo.png" rel="shortcut icon" type="image/png" /> --%>
    <%-- <link rel="icon" type="image/png" sizes="32x32" href="${assetPath}/images/logo.png"> --%>

    <!-- Fontfaces CSS-->
    <link href="${assetPath}/css/font-face.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap core CSS-->
    <link href="${contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="${assetPath}/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="${assetPath}/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <%-- <link href="${contextPath}/vendor/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet" media="screen"> --%>
    <link href="${assetPath}/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.css" integrity="sha512-C7hOmCgGzihKXzyPU/z4nv97W0d9bv4ALuuEbSf6hm93myico9qa0hv4dODThvCsqQUmKmLcJmlpRmCaApr83g==" crossorigin="anonymous" />
    <!-- Main CSS-->
    <link href="${assetPath}/css/theme.css" rel="stylesheet" media="all">
    <link href="${assetPath}/css/styles.css" rel="stylesheet" media="all">

    <!-- Custom fonts for this template-->
    <%-- <link href="${contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"> --%>

    <link href="${contextPath}/vendor/select2.min.css" rel="stylesheet">
		<link href="${contextPath}/vendor/daterangepicker.css" rel="stylesheet"> 
		<link href="${contextPath}/vendor/ekko-lightbox.css" rel="stylesheet">
		
		<script>var ctx = "${contextPath}"</script>
		<script>var ctx2 = "${contextPath}"</script>
		<script>var accountId = "${accountId}"</script>
		<script>var accountName = "${realName}"</script>
    <script>var roleList = "${roleList}"</script>
    <script>var position = "${roleList eq '[ROLE_PPS_KEEPER, ROLE_PPS_USER]'?'KEEPER':roleList eq '[ROLE_PPS_USER, ROLE_PPS_DOKTER]'?'DOKTER':'OTHER'}"</script>
    <script>var tokenCookieName = "${cookieName}"</script>
    <script>var satkerId = "${satkerId}"</script>
    <script>var assetPath = "${assetPath}"</script>
		<script>var localeCookieName = "${localeCookieName}"</script>
		<script>var skipPersetujuanPengeluaran = "${skipPersetujuanPengeluaran}"</script>
  </head>
