<%@page import="id.go.menlhk.bp2sdm.spjk.domain.Sesi"%>
<%@page import="id.go.menlhk.bp2sdm.spjk.core.QueryParameter"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%

ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
SesiMapper sesiMapper = appCtx.getBean(SesiMapper.class);
QueryParameter param = new QueryParameter();
param.setClause(param.getClause()+" AND active_sesi='"+1+"'");
String kode="";
if(sesiMapper.getCount(param)>0){
	kode = "kode-sesi="+sesiMapper.getList(param).get(0).getKode();
}
if(request.getParameter("kode-sesi")!=null) kode="kode-sesi="+request.getParameter("kode-sesi");
%>

<style>
.bordered-icon {
  text-shadow: -1px 0 #5E5E5E, 0 1px #5E5E5E, 1px 0 #5E5E5E, 0 -1px #5E5E5E;
}
.bordered-icon2 {
  text-shadow: 0 0 3px #5E5E5E;
}
</style>
<script> var roleList = "<c:out value='${roleList}'/>"; </script>

<% boolean isDropdown = false; %>

<c:set var="menu" scope="session">
  <nav class="navbar-sidebar">
    <ul class="list-unstyled navbar__list">
      <li class="has-sub">
        <a class="font-weight-bold" href="../dashboard/?<%out.print(kode);%>"><i class="fas fa-fw fa-tachometer-alt"></i>Dashboard</a>
      </li>
      <!-- <li class="has-sub">
        <a class="open font-weight-bold" href="#" onclick="return false;"><i class="fas fa-fw fa-list"></i>Master Data</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: block;">
           <li><a href="#" class="pegawai?menu=Pegawai"><i class="fas fa-fw fa-folder"></i> Pegawai</a></li>
           <li><a href="#" class="unit-kompetensi?menu=Unit Kompetensi"><i class="fas fa-fw fa-folder"></i> Unit Kompetensi</a></li>
           <li><a href="#" class="bank-soal?menu=Bank Soal"><i class="fas fa-fw fa-folder"></i> Bank Soal</a></li>
        </ul>
      </li> -->
      <li class="has-sub">
        <a class="open font-weight-bold" href="laporan?<%out.print(kode);%>"><i class="fas fa-fw fa-file-text"></i>Laporan</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: block;">
        	<!-- per-individu?menu=Per Individu&jenis=Nilai Kompetensi -->
           <li><a href="../dashboard/per-individu?mode=nilai-kompetensi&<%out.print(kode);%>"><i class="fas fa-fw fa-folder"></i> Per Individu</a></li>
           <li><a href="../dashboard/per-jabatan?mode=jabatan&<%out.print(kode);%>"><i class="fas fa-fw fa-folder"></i> Per Jabatan</a></li>
           <li><a href="../dashboard/per-jabatan?mode=satker&<%out.print(kode);%>"><i class="fas fa-fw fa-folder"></i> Per Organisasi</a></li>
        </ul>
      </li>
    </ul>
  </nav>
</c:set>


<c:set var="menuDesktop" scope="session" value="${menu}" />

<!-- MENU SIDEBAR-->
  <aside class="menu-sidebar d-none d-lg-block" style="width:265px">
    <div class="logo pl-0 pr-0" style="height: 117px">
      <%-- <a href="./">
        <img src="${assetPath}/images/icon/spip.png" alt="Logo" style="height: 55px; border-radius: 100%" />
      </a> --%>
      <div class="h5" style="float: right; margin-top: 4px;">
        <a href="?<%out.print(kode);%>" style="color: black; padding-left: 14px;">
          <span style="font-weight: bold; color: #516b73">PEMETAAN<br/>KOMPETENSI JABATAN<br/>FUNGSIONAL</span> <span style="margin-left: -2px;"></span>
        </a>
      </div>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">${menuDesktop}</div>
  </aside>
<!-- END MENU SIDEBAR-->

<c:set var="menuMobile" scope="session" value="${menu}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'navbar-sidebar', 'navbar-mobile')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__list', 'navbar-mobile__list list-unstyled')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__sub-list js-sub-list', 'navbar-mobile-sub__list list-unstyled js-sub-list')}" />
<!-- HEADER MOBILE-->
  <header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
      <div class="container-fluid">
        <div class="header-mobile-inner">
          <%-- <a class="logo" href="index"><img src="${assetPath}/images/icon/spip.png" style="height: 60px; border-radius: 100%" alt="Logo" /></a> --%>
          <button class="hamburger hamburger--slider" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
        </div>
      </div>
    </div>
    ${menuMobile}
  </header>
<!-- END HEADER MOBILE-->

<style>
@media (max-width: 991px) {
    .header-desktop {
        height: 75px;
    }
}
</style>
