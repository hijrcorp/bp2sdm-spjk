<%@ include file = "inc/header.jsp" %>


<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    	<%@ include file = "inc/hasil-keseluruhan.jsp" %>
      <div class="row">
        <div class="col-lg-12">
          <div class="au-card recent-report">
            <div class="au-card-inner">
              <!-- new -->
              <div class="row">
				    <div class="col-md-8">
				        <h3 class="title-2 text-right">Nilai Rata-rata Kompetensi Teknis</h3>
				    </div>
				    <div class="col-md-4 d-flex">
				        <select id="filter_tingkat_jabatan" data-chart="teknisChart" class="form-control form-control-sm mr-2">
				            <option value="">Pilih Tingkatan</option>
				        </select>
				        <select id="filter_jenjang_jabatan" data-chart="teknisChart" class="form-control form-control-sm">
				            <option value="">Pilih Jenjang</option>
				        </select>
				    </div>
				</div>       
              <!-- end new -->
              <hr/>
			  <div id="ContainerTeknisChart" style=""><!-- zoom: 1.1764705882352942; -->
			    <canvas id="teknisChart"></canvas>
			  </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="au-card recent-report p-3">
            <div class="au-card-inner">
              <!-- new -->
              <div class="chart-info">
				    <div class="chart-info__left"><h2 class="title-3">Rata-Rata Kompetensi Sosiokultural</h2></div>
				    <div class="">
				        <div class="row">
				            <div class="col-md-6">
				                <select id="filter_tingkat_jabatan" data-chart="sosiokulturalChart" class="form-control form-control-sm">
				                    <option value="">Pilih Tingkatan</option>
				                </select>
				            </div>
				            <div class="col-md-6">
				                <select id="filter_jenjang_jabatan" data-chart="sosiokulturalChart" class="form-control form-control-sm">
				                    <option value="">Pilih Jenjang</option>
				                </select>
				            </div>
				        </div>
				    </div>
				</div>
				              
              <!-- end new -->
			  <div style=""><!-- zoom: 1.1764705882352942; -->
			    <canvas id="sosiokulturalChart"></canvas>
			  </div>
			  <div class="row" style="padding-top: 20px;">
			  	 <div class="col-lg-12">
			  		<h3 class="title-3 text-center">Perekat Bangsa</h3>
			  	 </div>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="au-card recent-report p-3">
            <div class="au-card-inner">
              <!-- new -->
              <div class="chart-info">
				    <div class="chart-info__left"><h2 class="title-3">Rata-rata Kompetensi Manajerial</h2></div>
				    <div class="">
				        <div class="row">
				            <div class="col-md-6">
				                <select id="filter_tingkat_jabatan" data-chart="manajerialChart" class="form-control form-control-sm">
				                    <option value="">Pilih Tingkatan</option>
				                </select>
				            </div>
				            <div class="col-md-6">
				                <select id="filter_jenjang_jabatan" data-chart="manajerialChart" class="form-control form-control-sm">
				                    <option value="">Pilih Jenjang</option>
				                </select>
				            </div>
				        </div>
				    </div>
				</div>
				              
              <!-- end new -->
			  <div style="padding-top: 3px"><!-- zoom: 1.1764705882352942;  -->
			    <canvas id="manajerialChart"></canvas>
			  </div>
			  <div class="row" style="padding-top: 20px;">
			  	<div class="col-lg-12 text-center" id="div-btn-kompetensi-manajerial"></div>
			  </div>
            </div>
          </div>
        </div>
      </div>
      
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<%-- <%@ include file = "inc/trademark.jsp" %>   --%>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/dashboard.js"></script>
<%-- <script src="${assetPath}/js/main.js"></script> --%>