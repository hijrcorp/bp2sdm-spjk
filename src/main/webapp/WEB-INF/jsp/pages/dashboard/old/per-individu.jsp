<%@ include file = "inc/header.jsp" %>

<% String kodes="";
if(request.getParameter("kode-sesi")!=null) {
	kodes="kode-sesi="+request.getParameter("kode-sesi");
 } %>  
<style>

    .chart-legend li span{
	    display: inline-block;
	    width: 12px;
	    height: 12px;
	    margin-right: 5px;
	}
</style>

<!-- MAIN CONTENT-->
<div class="main-content" style="padding-top: 110px">
  <div class="section__content section__content--p30">
    <div class="container-fluid">

		<!-- Breadcrumbs <%= (request.getParameter("jenis") != null && request.getParameter("jenis").equals("Rekomendasi Pengembangan")?"active":"") %>-->
		<nav class="navbar breadcrumb navbar-expand-lg navbar-dark mb-3">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="nav nav-pills mr-auto">
		        <li class="nav-item"><a onclick="alert('Halaman tidak tersedia.')" class="nav-link" href="javascript:void(0)">Profile</a></li>
		        <!-- per-individu -->
		        <li class="nav-item"><a class="nav-link <% if(request.getParameter("mode").equals("nilai-kompetensi")) out.print("active"); %>" href="per-individu?mode=nilai-kompetensi&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kodes);else out.print(kodes);%>">Nilai Kompetensi</a></li>
		        <li class="nav-item"><a class="nav-link <% if(request.getParameter("mode").equals("rekomendasi-pengembangan")) out.print("active"); %>" href="per-individu?mode=rekomendasi-pengembangan&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kodes);else out.print(kodes);%>">Rekomendasi Pengembangan</a></li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		    <%-- <% if(request.getParameter("mode").equals("nilai_kompetensi")) {%> --%>
		    	<input value="<% out.print(request.getParameter("mode")); %>" name="mode" class="form-control mr-sm-2 d-none" type="text"/>
		    <%-- <% } %> --%>
		        <input value="<% out.print(request.getParameter("nip")!=null?request.getParameter("nip"):""); %>" name="nip" class="form-control mr-sm-2" type="number" placeholder="NIP" aria-label="Search" style="width: 250px;"/>
		        <!-- just for view 
		        <input id="nip" class="form-control mr-sm-2" type="search" placeholder="Nama/NIP" aria-label="Search" style="width: 250px;"/>
		        -->
		        <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0" type="button">Cari</button>
		    </form>
		</div>
		</nav>
		
		<div class="card">
			<div class="card-body">
				<% if(request.getParameter("mode").equals("nilai-kompetensi")) {%>
				<div class="row">
					<div class="col-md-2">
						<img src="${assetPath}/images/icon/picture.png" data-src="${assetPath}/images/icon/picture.png" alt="${requestScope.realName}" style="width: 240px" />
					</div>
					<div class="col-md-10 pt-2" style="font-size: 18px">
						<h2 id="nama_profile" class="mb-1">-</h2>
						<p id="satker_profle">-</p>
						<p id="jabatan_profile">-</p>
						<p id="bidang_teknis" style="color: red; font-style: italic">-</p>
						<br/>
						<div class="row">
							<div class="col-md-3 ml-4">
								<div class="float-left text-right" style="border: 0px solid black;">
									Kompetensi<br/>Manajerial
								</div>
								<div class="" style="border: 0px solid black;">
										<button id="progress_manajerial<% if(request.getAttribute("introduceSoal").equals("1")) out.print(request.getAttribute("introduceSoal")); %>" class="btn btn-success mt-1 ml-4 " style="font-size: 22px; padding: 9px; width: 127px; font-weight: bold;">-</button>
								</div>
							</div>
							<div class="col-md-3">
								<div class="float-left text-right">
									Kompetensi<br/>Sosiokultural
								</div>
								<div class="">
									<button id="progress_sosiokultural<% if(request.getAttribute("introduceSoal").equals("1")) out.print(request.getAttribute("introduceSoal")); %>" class="btn btn-success mt-1 ml-4 " style="font-size: 22px; padding: 9px; width: 124px; font-weight: bold;">-</button>
								</div>
							</div>
							<div class="col-md-3">
								<div class="float-left text-right">
									Kompetensi<br/>Teknis
								</div>
								<div class="">
									<button id="progress_teknis<% if(request.getAttribute("introduceSoal").equals("1")) out.print(request.getAttribute("introduceSoal")); %>" class="btn btn-success mt-1 ml-4 " style="font-size: 22px; padding: 9px; width: 127px; font-weight: bold;">-</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<br/>
				<div class="float-left">
					<h3 class="primary-color mt-3">Nilai Kompetensi</h3>
				</div>
				<div class="float-right" style="margin-top: -0px">
					<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
					<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
				</div>
				<hr class="mt-5"/>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Manajerial</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<div style="margin-top: 0px"> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="radar"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Manajerial</h5>
						<table id="tbl-manajerial" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Manajerial</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Sosiokultural</h5>
						<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Teknis</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<h3 class="secondary-color " style="font-size: 22px">INTI</h3>
						<div id="container-bar-inti" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-inti"></canvas>
						</div>
						<div id="container-legend1" class="chart-legend d-flex justify-content-center"></div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Peta Kompetensi INTI </h5>
						<table id="tbl-teknis-inti" class="table table-bordered">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<hr class="mt-5"/>
				
				<div class="row">
					<div class="col-md-6">
						<h3 class="secondary-color " style="font-size: 22px">PILIHAN</h3>
						<div id="container-bar-pilihan" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-pilihan"></canvas>
						</div>
						<div id="container-legend2" class="chart-legend d-flex justify-content-center"></div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Peta Kompetensi PILIHAN</h5>
						<table id="tbl-teknis-pilihan" class="table table-bordered">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			<% } %>
			
			<% if(request.getParameter("mode").equals("rekomendasi-pengembangan")) {%>
				<%@ include file = "per-individu-rekomendasi-pengembangan.jsp" %>
			<% } %>
			</div>
		</div>
		
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%-- <script src="${assetPath}/js/dashboard.js"></script> --%>
<%@ include file = "inc/footer.jsp" %>

<% if(request.getParameter("mode").equals("nilai-kompetensi")) {%>
<script src="${assetPath}/js/per-individu.js"></script>
<% } else if(request.getParameter("mode").equals("rekomendasi-pengembangan")) {%>
<script src="${assetPath}/js/per-individu-rekomendasi-pengembangan.js"></script>
<% } %>