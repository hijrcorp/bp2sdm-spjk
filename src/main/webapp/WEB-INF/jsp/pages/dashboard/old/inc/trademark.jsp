<div class="row">
  <div class="col-md-12">
    <div class="copyright">
      <p class="text-dark">
        Copyright � 2020 -  <%= new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %> <a href="http://www.hijr.co.id/" target="_blank" class="text-primary">PT. Hijr Global Solution</a>. <br/>All rights reserved.
      </p>
    </div>
  </div>
</div>