<%@ include file = "inc/header.jsp" %>

<style>
	.form-control,.force {
      width: 350px!important;
    }
    .chart-legend li span{
	    display: inline-block;
	    width: 12px;
	    height: 12px;
	    margin-right: 5px;
	}
</style>

<!-- MAIN CONTENT-->
<div class="main-content" style="padding-top: 110px">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
    
		<form id="form-search" >
    	<% if(request.getParameter("mode") !=null) { %>
			<% if(request.getParameter("mode").equals("satker") || request.getParameter("mode").equals("jabatan")) { %>
			<div class="row <%if(request.getParameter("mode").equals("jabatan")) out.print("d-none"); else out.print("");%>">
				<div class="row col-md-10">
						<div class="form-group mb-0 ml-3">
							<select name="filter_id_eselon1" class="form-control">
								<option value="">Pilih Eselon1</option>
							</select>
						</div>
							<div class="form-group mb-0 ml-3" style="width: 350px">
							<select name="filter_id_unit_auditi" class="form-control select2">
								<option value="">Pilih Satker</option>
							</select>
						</div>
						<button type="button" onclick="display()" class="btn btn-primary ml-3"><i class="fa fa-search"></i> CARI</button>
						<button type="button" onclick="$('#form-search').trigger('reset');$('[name=filter_id_unit_auditi]').val('').trigger('change')" class="btn btn-primary ml-3">CLEAR</button>
					
				</div>
				<div class="col-md-2">
					<div class="float-right">
						<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
					</div>
				</div>
			</div>
			<% } %>
		<% } %>
    	<%@ include file = "inc/hasil-keseluruhan.jsp" %>
		
		<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link active" onclick="console.log('per-jabatan?mode=jabatan')" href="javascript:void(0);">Hasil Pemetaan Kompetensi</a></li>
			<li class="nav-item"><a class="nav-link" href="javascript:void(0);" onclick="goToJabatanRekomendasi();">Rekomendasi Pengembangan</a></li>
		</ul>
		
		<div class="card">
			<div class="card-body">
				<div style="margin-bottom: 10px">
					<div class="float-left">
						<h3 class="primary-color mt-3">Demografi SDM</h3>
					</div>
				</div>
				<hr class="" style="margin-top: 60px"/>
				
				<div class="row">
					<div class="col-md-4">
						<div class="card">
						<div class="card-header bg-white">
							<h4 class="card-title primary-color m-0">% Gender dan Usia</h4>
						</div>
						<div class="card-body">
						<div class="row">
						<div class="col-md-12">
						<div id="ContainerBarHorizontal" style=" margin-top: 0px"> <!-- zoom: 1.1764705882352942; -->
							<canvas class="mw-100" id="barHorizontal" width="350" height="350"></canvas>
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
						<div class="card-header bg-white">
							<h4 class="card-title primary-color m-0">% Keterampilan berdasarkan Jenjang</h4>
						</div>
						<div class="card-body">
						<div class="row">
						<div class="col-md-12">
						<div id="ContainerBar1" style=" margin-top: 0px"> <!-- zoom: 1.1764705882352942; -->
							<canvas class="mw-100" id="bar1" width="350" height="350"></canvas>
						</div>
						</div>
						</div>
						</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
						<div class="card-header bg-white">
							<h4 class="card-title primary-color m-0">% Keahlian berdasarkan Jenjang</h4>
						</div>
						<div class="card-body">
						<div id="ContainerBar2" style=" margin-top: 0px"> <!-- zoom: 1.1764705882352942; -->
							<canvas class="mw-100" id="bar2" width="350" height="350"></canvas>
						</div>
						</div>
						</div>
					</div>
				</div>
				
				<br/>
		

				<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
	                <div class="row">
	                	<div class="col-auto">
		                  <select id="filter_tingkat_jabatan" name="filter_tingkat_jabatan" data-chart="teknisChart" class="form-control mr-3">
			                	<option value="">-- Pilih Tingkatan --</option>
			                </select>
	                  </div>
	                  <div class="col-auto">
			                <select id="filter_jenjang_jabatan" name="filter_jenjang_jabatan"  data-chart="teknisChart" class="form-control">
			                	<option value="">-- Pilih Jenjang --</option>
			                </select>
	                  </div>
	                </div>
				</ul>
	            </form>
				
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompetensi Teknis</h3>
				</div>
				
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<h3 class="secondary-color " style="font-size: 22px">INTI</h3>
						<div id="container-bar-inti" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-inti"></canvas>
						</div>
						<div id="container-legend1" class="chart-legend d-flex justify-content-center"></div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Peta Kompetensi INTI </h5>
						<table id="tbl-teknis-inti" class="table table-bordered">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<hr class="mt-5"/>
				
				<div class="row">
					<div class="col-md-6">
						<h3 class="secondary-color " style="font-size: 22px">PILIHAN</h3>
						<div id="container-bar-pilihan" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-pilihan"></canvas>
						</div>
						<div id="container-legend2" class="chart-legend d-flex justify-content-center"></div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Peta Kompetensi PILIHAN</h5>
						<table id="tbl-teknis-pilihan" class="table table-bordered">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Manajerial</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<div style="margin-top: 0px"> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="radar"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Niliai Kompetensi Manajerial</h5>
						<table id="tbl-manajerial" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Manajerial</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/><br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Justification</h3>
				</div>
				<hr class="mt-5"/>
				<p class="mt-3" style="font-size: 24px; padding: 0px 20px 0px 20px">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis finibus eros. Donec ut venenatis quam, convallis lobortis nunc. Suspendisse tincidunt auctor nunc fermentum elementum. Morbi quis lacus quis turpis bibendum scelerisque. Phasellus imperdiet dolor a magna semper euismod. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut auctor ante tellus, ac mattis quam venenatis imperdiet. Donec in pulvinar enim. Suspendisse id pharetra massa. Maecenas eros nibh, aliquam ac pellentesque a, vulputate id ex.
				</p>
				
				<br/><br/><br/><br/><br/><br/>
				
			</div>
		</div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/per-jabatan.js"></script>