<!-- <div class="card-body"> -->
	<style>
	    .chart-legend li span{
		    display: inline-block;
		    width: 12px;
		    height: 12px;
		    margin-right: 5px;
		}
	</style>
	<div class="row">
		<div class="col-md-2">
			<img src="${assetPath}/images/icon/picture.png" data-src="${assetPath}/images/icon/picture.png" alt="${requestScope.realName}" style="width: 240px" />
		</div>
		<div class="col-md-4 pt-2" style="font-size: 18px">
			<h2 id="nama_profile" class="mb-1">-</h2>
			<p id="satker_profle">-</p>
			<p id="jabatan_profile">-</p>
			<p id="bidang_teknis" style="color: red; font-style: italic">-</p>
			<div id="status_profile" class="mt-3 text-center" style="border: 1px solid black; font-size: 30px; padding: 4px; width: 170px; font-weight: bold;">-</div>
		</div>
		<div class="col-md-4 pt-2 text-right" style="font-size: 20px">
			<span style="font-size: 20px; font-weight: 500">
				Rekomendasi Pengembangan Kompetensi<br/>
				SDM Aparatur Jabatan Fungsional KLHK<br/>
				Pusat Perencanaan dan Pengembangan SDM
			</span>
		</div>
		<div class="col-md-2">
			<img src="${assetPath}/images/logo1.png" data-src="${assetPath}/images/logo1.png" alt="${requestScope.realName}" style="width: 200px" />
		</div>
	</div>
	
	<br/>
	<div style="margin-bottom: 10px">
		<div class="float-left">
			<h3 class="primary-color mt-3">Ringkasan Hasil Pemetaan</h3>
		</div>
		<div class="float-right">
			<button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
			<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
		</div>
	</div>
	<hr class="mb-5" style="margin-top: 60px"/>
	
	
	<div class="row mt-3 mb-3">
		<div class="col-md-4">
			<h4 class="primary-color text-center">Kompetensi Teknis</h4>
		</div>
		<div class="col-md-4">
			<h4 class="primary-color text-center">Kompetensi Manajerial</h4>
		</div>
		<div class="col-md-4">
			<h4 class="primary-color text-center">Kompetensi Sosiokultural</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<h4 class="card-title">INTI</h4>
						<div id="container-bar-inti" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942; height: 500px !important;  -->
							<canvas id="bar-inti" ></canvas><!-- width="600" height="250" -->
						</div>
						<h4 class="card-title">PILIHAN</h4>
						<div id="container-bar-pilihan" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942; height: 500px !important;  -->
							<canvas id="bar-pilihan" ></canvas><!-- width="600" height="250" -->
						</div>
					    <div id="container-legend2" class="chart-legend d-flex justify-content-center"></div>
					</div>
				</div>
			</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
			<div class="card-body pl-2 pr-2">
				<canvas id="radar"></canvas>
			</div>
			</div>
		</div>
		<div class="col-md-4">
			<!-- <h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Nilai Kompetensi Sosiokultural</h5> -->
			<div class="card">
			<div class="card-body p-0">
			<table id="tbl-sosiokultural2" class="table table-bordered table-striped table-hover small">
				<thead>
					<tr>
						<th width="50"  class="text-center">No</th>
						<th width="200">Kompetensi Sosiokultural</th>
						<th width="100" class="text-center">Level Standar</th>
						<th width="100" class="text-center">Hasil Pengukuran</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
			</div>
		</div>
	</div>
	
	<br/>
	<div class="float-left">
		<h3 class="primary-color mt-3">Rekomendasi Pengembangan</h3>
	</div>
	<hr class="mb-3" style="margin-top: 60px"/>
	<div class="row">
		<div class="col-md-12">
			<table id="tbl-teknis" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="1" class="text-center">No</th>
						<th width="50" class="text-center">Kode</th>
						<th width="200" class="text-center">Unit Kompetensi Teknis</th>
						<th width="100" class="text-center">Level Standar</th>
						<th width="100" class="text-center">Hasil Pengukuran</th>
						<th width="150" class="text-center">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="col-md-12 mt-3">
			<table id="tbl-manajerial" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="1" class="text-center">No</th>
						<th width="250" class="text-center">Kompetensi Manajerial</th>
						<th width="100" class="text-center">Level Standar</th>
						<th width="100" class="text-center">Hasil Pengukuran</th>
						<th width="150" class="text-center">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="col-md-12 mt-3">
			<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="1" class="text-center">No</th>
						<th width="250" class="text-center">Kompetensi Sosiokultural</th>
						<th width="100" class="text-center">Level Standar</th>
						<th width="100" class="text-center">Hasil Pengukuran</th>
						<th width="150" class="text-center">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
	
	<p class="mt-3" style="font-size: 24px">
		Pusat Perencanaan dan Pengembangan SDM merekomendasikan program pengembangan kompetensi teknis, 
		manajerial, dan sosokultural, sebagaimana pada tabel di atas berdasarkan hasil pemetaan kompetensi tahun 2021. 
	</p>
	
	<div class="row" style="margin-top: 50px; margin-bottom: 100px">
		<div class="col-md-8"></div>
		<div class="col-md-4">
			<p class="mt-3" style="font-size: 24px">
				Mengetahui, 
			</p>
			<p style="font-size: 24px; margin-top: 120px">
				Nama Lengkap dan Gelar<br/>
				NIP. 123456 788 3345
			</p>
		</div>
	</div>
	
<!-- </div> -->