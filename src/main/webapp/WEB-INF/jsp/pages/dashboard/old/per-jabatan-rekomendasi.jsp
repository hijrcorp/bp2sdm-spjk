<%@ include file = "inc/header.jsp" %>

<style>
	.form-control,.force {
      width: 350px!important;
    }
    .chart-legend li span{
	    display: inline-block;
	    width: 12px;
	    height: 12px;
	    margin-right: 5px;
	}
</style>

<!-- MAIN CONTENT-->
<div class="main-content" style="padding-top: 110px">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
		
		<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link" href="per-jabatan?mode=jabatan&<%out.print(kode);%>">Hasil Pemetaan Kompetensi</a></li>
			<!-- id_kelompok=&kode-sesi=&id_tingkatan=&id_jenjang -->
			<li class="nav-item"><a class="nav-link active">Rekomendasi Pengembangan</a></li>
		</ul>
		
		<div class="card">
			<div class="card-body">
				<div class="float-left">
					<h3 class="primary-color mt-3 mb-3">Ringkasan Hasil Pemetaan</h3>
				</div>
				<hr class="mb-3" style="margin-top: 70px"/>
				<div class="row">
					<div class="col-md-6">
					<div class="card">
					<div class="card-body">
					  	<h3 class="secondary-color " style="font-size: 22px">INTI</h3>
						<div id="container-bar-inti" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-inti"></canvas>
						</div>
						<div id="container-legend1" class="chart-legend d-flex justify-content-center"></div>
						
						<h3 class="secondary-color " style="font-size: 22px">PILIHAN</h3>
						<div id="container-bar-pilihan" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-pilihan"></canvas>
						</div>
						<div id="container-legend2" class="chart-legend d-flex justify-content-center"></div>
					</div>
					</div>
					</div>
					<div class="col-md-6">
					<div class="card">
					<div class="card-body">
						<h3 class="secondary-color " style="font-size: 22px">MANAJERIAL</h3>
						<div style="margin-top: 0px"> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="radar"></canvas>
						</div>
					</div>
					</div>
					</div>
				</div>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6 offset-3">
						<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<br/><br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Justification</h3>
				</div>
				<hr class="mt-5"/>
				<p class="mt-3" style="font-size: 24px; padding: 0px 20px 0px 20px">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis finibus eros. Donec ut venenatis quam, convallis lobortis nunc. Suspendisse tincidunt auctor nunc fermentum elementum. Morbi quis lacus quis turpis bibendum scelerisque. Phasellus imperdiet dolor a magna semper euismod. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut auctor ante tellus, ac mattis quam venenatis imperdiet. Donec in pulvinar enim. Suspendisse id pharetra massa. Maecenas eros nibh, aliquam ac pellentesque a, vulputate id ex.
				</p>
				
				<br/><br/><br/><br/><br/><br/>
				
			</div>
		</div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/per-jabatan-rekomendasi.js"></script>