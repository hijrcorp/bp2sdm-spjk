<%@ include file = "inc/header.jsp" %>

<div id="wrapper" class="main-content">

	<div id="content-wrapper" class="section__content section__content--p20">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Pengumpulan Bukti</a></li>
				<li class="breadcrumb-item active" id="menu"></li>
			</ol>
			
			<div class="card">
				<div class="card-header">
					<h4><span id="menu"></span></h4>
				</div>
				<div class="card-body">
				
					<div class="card">
						<div class="card-body">
							<div class="form-group row">
								<div class="col-md-2">
									<label>Tahun</label>
								</div>
								<div class="col-md-4">
									<select name="filter_tahun" class="form-control">
										<option value="">-- Pilih Tahun --</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label>Unit Auditi</label>
								</div>
								<div class="col-md-10">
									<select name="filter_id_unit_auditi" class="form-control">
										<option value="">-- Pilih Unit Auditi --</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				
					<table id="tbl" class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th width="10"  rowspan="2" class="text-center">No</th>
								<th width="600" rowspan="2" class="text-center">Judul</th>
								<th width="300" colspan="3" class="text-center">Hasil Pengujian Bukti</th>
								<th width="100" rowspan="2" class="text-center">Action</th>
							</tr>
							<tr>
								<th class="text-center" width="150">Dokumen</th>
								<th class="text-center" width="150">Substansi</th>
								<th class="text-center" width="120" nowrap>Hasil (Y/T)</th>
							</tr>
						</thead>
						<tbody>
							<tr><td colspan="10"><p class="text-center pt-3 pb-3">Silahkan pilih <b>Tahun</b> dan <b>Unit Auditi</b> terlebih dahulu.</p></td></tr>
						</tbody>
					</table>
					
				</div>
			</div>

		</div>
		<!-- /.container-fluid -->

		<%@ include file = "inc/trademark.jsp" %>	

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->


<script src="${assetPath}/js/pengumpulan-bukti-persiapan.js"></script>
<%@ include file = "inc/footer.jsp" %>