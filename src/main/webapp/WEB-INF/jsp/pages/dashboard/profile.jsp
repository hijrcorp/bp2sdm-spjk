<script>var vargolongan="";</script>
<!-- <div class="card">
<div class="card-body"> -->
   <style>
      .chart-legend li span{
      display: inline-block;
      width: 12px;
      height: 12px;
      margin-right: 5px;
      }
   </style>
   <div class="float-right d-none" style="margin-top: -0px">
      <button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
      <button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
   </div>
   <div class="row justify-content-center">
      <div class="col-md-2 col-6">
         <img src="/spektra/assets-dashboard/images/icon/picture.png" data-src="https://drive.google.com/uc?export=view&id=14LM6WzR8AaAsYe96oDdb4rkQelRRCqIq" alt="Lord " style="width: 240px">
      </div>
      <!-- <div class="col-md-10 pt-2" style="font-size: 18px">
         <h2 id="nama_profile" class="mb-1">Abdul Ahmad</h2>
         <p id="satker_profle">Dinas Lingkungan Hidup Kabupaten Agam</p>
         <p id="jabatan_profile">Pengendali Ekosistem Hutan PEMULA
         </p>
         <p id="bidang_teknis" style="color: red; font-style: italic">PEH-Bidang Perencanaan Hutan</p>
         <br>
      </div> -->
   </div>

   <div class="float-left">
      <h3 class="primary-color mt-3">Biodata <i class="fa fa-pencil d-none"></i></h3>
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12">
	        
         <form id="entry-form-biodata-sinkorn">
         	<input name="id" type="hidden" value="${responden.id}">
            <div class="row form-group">
               <div class="col-lg-6 col-12">
                  <label for="">Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama_lengkap" value="${responden.nama}" disabled>
               </div>
               <div class="col-lg-6 col-12">
                  <label for="">NIP</label>
                  <input type="number" class="form-control" name="nip" value="${responden.nip}" disabled>
               </div>
            </div>
            <div class="row form-group">
               <div class="col-lg-6 col-12">
                  <label for="">Jenis Kelamin</label>
                  <select name="jenis_kelamin" class="form-control" disabled>
                  		<option>JENIS KELAMIN</option>
			         	<option value="LAKI-LAKI">LAKI-LAKI</option>
                  		<option value="PEREMPUAN">PEREMPUAN</option>
                  </select>
               </div>
               <div class="col-lg-6 col-12">
                  	<label for="">Tanggal Lahir(DD-MM-YYY)</label>
					<input name="tanggal_lahir" type="date" class="form-control" value="${responden_tglLahir}" disabled>
               </div>
            </div>
            <div class="row form-group ">
               <div class="col-lg-6 col-12">
                  <label for="">Jabatan</label>
                  <input type="text" class="form-control" name="jabatan" value="${jabatan}" disabled>
               </div>
               <div class="col-lg-6 col-12">
	               <label for="">Satker/Unit Kerja</label>
	               <input type="text" class="form-control" name="unit_auditi" value="${namaSatker}" disabled>
               </div>
            </div>
            <div class="row form-group">
               <div class="col-lg-6 col-12">
                  <label for="">Golongan</label>
                  <select name="golongan" class="form-control" disabled>
                  	<option>GOLONGAN</option>
                  </select>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
</div>

<div class="card">
<div class="card-body">
   <div class="float-left">
      <h3 class="primary-color mt-3" style="font-size: 22px">Riwayat Jabatan 
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12 table-responsive">
         <table id="tbl-riw-jabatan" class="table table-hover table-striped">
            <thead>
               <tr>
                  <th class="align-middle">No.</th>
                  <th class="align-middle">Jenis Jabatan</th>
                  <th class="align-middle">Instansi</th>
                  <th class="align-middle">Satker</th>
                  <th class="align-middle">Unor</th>
                  <th class="align-middle">Nama Jabatan</th>
                  <th class="align-middle">TMT Jabatan ~ Pelantikan</th>
                  <th class="align-middle">Surat Keptusan</th>
                  <!-- <th class="align-middle">Sertifikat/Ijazah</th> -->
                  <th class="align-middle">Action</th>
               </tr>
            </thead>
            <tbody>
            	<!-- will be render by function -->
            </tbody>
            <tfoot class="d-none">
               <tr>
                  <td colspan="6" class="text-center"><button onclick="add('jabatan')" class="btn btn-secondary btn-sm">Tambahkan </button></td>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
   <br>
   <div class="float-left">
      <h3 class="primary-color mt-3" style="font-size: 22px">Riwayat Pendidikan</h3>
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12 table-responsive">
         <table id="tbl-riw-pendidikan" class="table table-hover table-striped">
            <thead>
               <tr>
                  <th class="align-middle">No.</th>
                  <th class="align-middle">Tingkat Pendidikan</th>
                  <th class="align-middle">Jurusan</th>
                  <th class="align-middle">Nama Sekolah/Universitas/Instansi</th>
                  <th class="align-middle">Tahun & Tgl Lulus</th>
                  <th class="align-middle">Nomor Ijazah</th>
                  <th class="align-middle">Gelar</th>
                  <th class="align-middle">Action</th>
               </tr>
            </thead>
            <tbody>
               <!-- will render by function -->
            </tbody>
            <tfoot class="d-none">
               <tr>
                  <td colspan="6" class="text-center"><button onclick="add('pendidikan')" class="btn btn-secondary btn-sm">Tambahkan </button></td>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
   <br>
   <div class="float-left">
      <h3 class="primary-color mt-3" style="font-size: 22px">Riwayat Pelatihan</h3>
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12 table-responsive">
         <table id="tbl-riw-pelatihan" class="table table-hover table-striped">
            <thead>
               <tr>
                  <th class="align-middle">No.</th>
                  <th class="align-middle">Jenis Diklat</th>
                  <th class="align-middle">Nama Diklat</th>
                  <th class="align-middle">Instisui Penyelenggara</th>
                  <th class="align-middle">Nomor Sertifikat</th>
                  <th class="align-middle">Tahun Diklat</th>
                  <th class="align-middle">Tanggal</th>
                  <th class="align-middle">Durasi</th>
                  <th class="align-middle">Action</th>
               </tr>
            </thead>
            <tbody>
	               <!-- will render by function -->
            </tbody>
            <tfoot class="d-none">
               <tr>
                  <td colspan="6" class="text-center"><button onclick="add('pelatihan')" class="btn btn-secondary btn-sm">Tambahkan </button></td>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
   <hr class="mt-5">
<!-- </div>
</div> -->
          
          