<%@ include file = "inc/header.jsp" %>

<style>
	.form-control,.force {
      width: 350px!important;
    }
    .chart-legend li span{
	    display: inline-block;
	    width: 12px;
	    height: 12px;
	    margin-right: 5px;
	}
</style>

<!-- MAIN CONTENT-->
<div class="main-content" style="">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
		<form id="form-search" class="" >
    	<% if(request.getParameter("mode") !=null) { %>
			<% if(request.getParameter("mode").equals("satker") || request.getParameter("mode").equals("jabatan")) { %>
			<div class="row pb-3 d-flex justify-content-end <%if(request.getParameter("mode").equals("jabatan")) out.print("d-none"); else out.print("");%>">
				<div class="row col-md-10 d-none">
						<div class="form-group mb-0 ml-3" style="width: 115px">
							<select name="filter_id_eselon1" class="form-control select2">
								<option value="">Eselon1</option>
							</select>
						</div>
						<div class="form-group mb-0 ml-3" style="width: 115px">
							<select name="filter_id_organisasi" class="form-control select2">
								<option value="">BDLHK</option>
							</select>
						</div>
						<div class="form-group mb-0 ml-3" style="width: 115px">
							<select name="filter_id_propinsi" class="form-control select2">
								<option value="">Propinsi</option>
							</select>
						</div>
						<div class="form-group mb-0 ml-3" style="width: 230px">
							<select name="filter_id_unit_auditi" class="form-control select2">
								<option value="">Pilih Satker</option>
							</select>
						</div>
						<button type="button" onclick="display(1)" class="btn btn-primary ml-3"><i class="fa fa-search"></i> CARI</button>
						<button id="btn-reset" type="button" class="btn btn-danger ml-3">CLEAR</button>
					
				</div>
				<div class="col-md-2">
					<div class="float-right">
						<button id="btn-download" class="btn btn-dark d-none" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
					</div>
				</div>
			</div>
			<% } %>
		<% } %>
		<div class="d-none">
    	<%@ include file = "inc/hasil-keseluruhan.jsp" %>
		</div>
		</form>
		
		<ul class="breadcrumb nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item"><a class="nav-link" href="../dashboard/per-jabatan?<%out.print(mode+"&"+kode);%>">Demografi</a></li>
			<li class="nav-item"><a class="nav-link" href="../dashboard/per-jabatan-nilai?<%out.print(mode+"&"+kode);%>">Nilai Pemetaan</a></li>
			<li class="nav-item"><a class="nav-link" href="../dashboard/per-jabatan-analisis?<%out.print(mode+"&"+kode);%>">Analisis</a></li>
			<li class="nav-item"><a class="nav-link active" onclick="window.location.reload();" href="javascript:void(0);" onclick="goToJabatanRekomendasi();">Rekomendasi Pengembangan</a></li>
		</ul>
		
		<div class="card mb-3 d-none">
			<div class="card-body">
		        <div class="row form-inline">
		            <div class="col-md-12">
		                <select id="filter_bidang_teknis" name="filter_id_bidang_teknis" class="form-control form-control-sm">
		                    <option value="">Pilih Bidang</option>
		                </select>
		                <select id="filter_tingkat_jabatan" name="filter_id_tingkat_jabatan" class="form-control form-control-sm">
		                    <option value="">Pilih Tingkatan</option>
		                </select>
		                <select id="filter_jenjang_jabatan" name="filter_id_jenjang_jabatan" class="form-control form-control-sm">
		                    <option value="">Pilih Jenjang</option>
		                </select>
		            </div>
		        </div>
			</div>
		</div>
		
		<div id="rekomendasi-body" class="card">
			<div class="card-body">
				<div class="float-left">
					<h3 class="primary-color mt-3 mb-3">Ringkasan Hasil Pemetaan</h3>
				</div>
				<hr class="mb-3" style="margin-top: 70px"/>
				<div class="row">
					<div class="col-md-6">
					<div class="card">
					<div class="card-body">
					  	<h3 class="secondary-color " style="font-size: 22px">INTI</h3>
						<div id="container-bar-inti" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-inti"></canvas>
						</div>
						<div id="container-legend1" class="chart-legend d-flex justify-content-center"></div>
						
					</div>
					</div>
					<div class="card">
					<div class="card-body">
						<h3 class="secondary-color " style="font-size: 22px">PILIHAN</h3>
						<div id="container-bar-pilihan" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-pilihan"></canvas>
						</div>
						<div id="container-legend2" class="chart-legend d-flex justify-content-center"></div>
					</div>
					</div>
					</div>
					<div class="col-md-6">
					<div class="card">
					<div class="card-body">
						<h3 class="secondary-color " style="font-size: 22px">MANAJERIAL</h3>
						<div style="margin-top: 0px"> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="radar"></canvas>
						</div>
					</div>
					</div>
					<div class="card">
					<div class="card-body">
						<h3 class="secondary-color " style="font-size: 22px">SOSIOKULTURAL</h3>
						
						<table id="tbl-sosiokultural2" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					</div>
					</div>
				</div>
				
	
	<br/>
	<div class="float-left">
		<h3 class="primary-color mt-3">Rekomendasi Pengembangan</h3>
	</div>
	<hr class="mb-3" style="margin-top: 60px"/>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
			<table id="tbl-teknis" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="" class="text-center align-middle">No</th>
						<th width="" class="text-center align-middle">Kode</th>
						<th width="" class="text-center align-middle">Unit Kompetensi Teknis</th>
						<th width="" class="text-center align-middle">Level Standar</th>
						<th width="" class="text-center align-middle">Hasil Pengukuran</th>
						<th width="" class="text-center align-middle">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-md-12 mt-3">
			<table id="tbl-manajerial" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="" class="text-center align-middle">No</th>
						<th width="" class="text-center align-middle">Kompetensi Manajerial</th>
						<th width="" class="text-center align-middle">Level Standar</th>
						<th width="" class="text-center align-middle">Hasil Pengukuran</th>
						<th width="" class="text-center align-middle">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="col-md-12 mt-3">
			<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="" class="text-center align-middle">No</th>
						<th width="" class="text-center align-middle">Kompetensi Sosiokultural</th>
						<th width="" class="text-center align-middle">Level Standar</th>
						<th width="" class="text-center align-middle">Hasil Pengukuran</th>
						<th width="" class="text-center align-middle">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
				<br/><br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Justification</h3>
				</div>
				<hr class="mt-5"/>
				<p class="mt-3" style="font-size: 24px; padding: 0px 20px 0px 20px">
					-
				</p>
				
				<br/><br/><br/><br/><br/><br/>
				
			</div>
		</div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%@ include file = "inc/footer.jsp" %>
<script src="${assetPath}/js/per-jabatan-rekomendasi.js"></script>
<script>
	$('#btn-print').on('click', function (e) {
		$('aside').remove();
		$('header').remove();
		$('.page-container.pl-lg-default').attr("class", "");
		$('#rekomendasi-body').attr("class", "border-0");
		$('#form-search').attr("class", "d-none");
		document.getElementsByClassName("copyright")[0].parentElement.parentElement.remove();
		document.getElementsByClassName("main-content")[0].className="";
		$('#pills-tab').hide();
		$(document.getElementById("form-search").children[0]).hide();
		$(document.getElementById("form-search").children[2]).hide();
		$(document.getElementById("form-search").children[3]).hide();
		$(document.getElementById("form-search").children[4]).hide();
	});
</script>