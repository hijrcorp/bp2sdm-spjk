<!DOCTYPE html>
<html lang="en">

<%@ include file = "./head.jsp" %>
<style>
.menu-sidebar__content {
  height: 90% !important;
}
.section__content--p30 {
    padding: 0 0px!important;
}
body{
	/* zoom: 85%; */
}
@page {
	size : auto;
}

@media (min-width: 992px){
	.pl-lg-default, .px-lg-default {
	    padding-left: 265px!important;
	}
	.left-lg-default {
	    left: 265px!important;
	}
	.width-lg-default {
	    width: 265px!important;
	}
}
</style>

<body class="animsition-not" >
  <div class="page-wrapper">
  
    <%@ include file = "./sidebar.jsp" %>

    <div class="page-container pl-lg-default">
    
      <%@ include file = "./navbar.jsp" %>
      