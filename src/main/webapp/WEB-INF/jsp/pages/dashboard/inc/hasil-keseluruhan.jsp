<%@page import="id.go.menlhk.bp2sdm.spjk.domain.KelompokJabatan"%>
<%@page import="java.util.ArrayList"%>
<style>

	 ::-webkit-scrollbar {
	    -webkit-appearance: none;
	    width: 7px;
	    height: 5.5px;
	}
	::-webkit-scrollbar-thumb {
	    border-radius: 4px;
	    background-color: rgba(0,0,0,.5);
	    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
	}
		
</style>
<% if(request.getParameter("mode") ==null) { %>
      <div class="row">
        <div class="col-md-12">
          <div class="overview-wrap">
            <h3 class="" style="text-transform: capitalize;font-weight: 400;">Hasil Keseluruhan</h3>
          </div>
        </div>
      </div>
<% } %>

<% 
ArrayList<KelompokJabatan> kelompokJab =  (ArrayList<KelompokJabatan>)request.getAttribute("listKelompokJabatan");
%>
			
      	<% 
      	String style1="";
      	String style2="";
      	if(request.getAttribute("unitPembinaId")==null) { 	
			style2="style='width: 1700px;'";
			style1="style='overflow-x: scroll;'";
       	} 
       	%>
<div class="" <% out.print(style1); %>>
<div id="list-kelompok" class="row mt-3" <% out.print(style2); %>>
        <!-- start -->
      	<% //for(int i = 0 ; i < 5 ; i++) {
      		int i=0;
      	for(KelompokJabatan o:kelompokJab){
      	String dhide="d-none";
      	if(request.getAttribute("unitPembinaId")!=null && request.getAttribute("unitPembinaId").equals(String.valueOf(i+1))) {
      		dhide="col-3 mb-3";
        }
      	if(request.getAttribute("unitPembinaId")==null) {
      		dhide="";
        }
        
        %>
        
        <div class="col-2 <% out.print(dhide); %>"> 
		<% if(request.getParameter("mode") !=null) {
				if(request.getParameter("mode").equals("jabatan") || request.getParameter("mode").equals("satker")) {
		        	if(request.getParameter("id_kelompok")!=null) { %>
		          		<div id="progress-id-kelompok-<%=i%>" data-id="" class="overview-item overview-item--c1" onclick="window.location.href=window.location.search.split('&')[0]+'&id_kelompok='+$('#progress-id-kelompok-<%=i%>').data('id')+'&kode-sesi='+'<% out.print(request.getParameter("kode-sesi")); %>'" style="background: white; padding: 15px;margin-bottom:0px!important">
		        <%	}else{ %>
		        		<div id="progress-id-kelompok-<%=i%>" data-id="" class="overview-item overview-item--c1" onclick="setKelompokJabatan(this)" style="background: white; padding: 15px;margin-bottom:0px!important">
		        	<%}
		        } 
	       }else{ %>
        	<div onclick="setKelompokJabatan(this)" id="progress-id-kelompok-<%=i%>" data-id="" class="overview-item overview-item--c1"  style="background: white; padding: 15px;margin-bottom:0px!important">
        <% } %>
          	<div class="row">
	            <div class="col-6">
	                <div class="text">
	                  <h4 class="text-secondary" id="progress-nama-kode-kelompok-berjalan-<%=i%>"></h4>
	                  <span class="text-dark">Responden</span>
	                  <h4 class="text-dark"><span style="color: red" id="julah-peserta-berjalan-<%=i%>"></span>/<span id="total-peserta-<%=i%>"></span></h4>
	                </div>
	            </div>
	            <div class="col-6">
	              <div class="overview-boxs clearfix">
	                <div style="height: 100px !important;" class="progress-chart-<%=i%>" style=""> <!-- zoom: 1.1764705882352942 -->
	                  <canvas id="progress-chart-<%=i%>" width="100" height="50" class="progress-chart-<%=i%>"></canvas>
	                </div>
	              </div>
	            </div>
	        </div>
            <div class="overview-boxs clearfix d-none">
            <div class="text text-center" style="">
             	<h5 class="text-dark"><a href="#">Lihat Hasil</a></h5>
              </div>
            </div>
          </div>
        </div>
        
        <% i++;
        } %>
        <!-- end -->
      
 </div>
 </div>