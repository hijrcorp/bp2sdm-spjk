<% if(request.getParameter("mode") ==null) { %>
      <div class="row">
        <div class="col-md-12">
          <div class="overview-wrap">
            <h3 class="" style="text-transform: capitalize;font-weight: 400;">Hasil Keseluruhan</h3>
          </div>
        </div>
      </div>
<% } %>
<div class="" style="
   /*  width: 1000px;
    max-width: 900px; */
    overflow-x: scroll;
">
      <div id="list-kelompok" class="card-deck mt-3 mb-3 d-inline-flex">
      	<% for(int i = 0 ; i < 5 ; i++) { %>
        <div class="card " style="border-radius:10px">  
		<% if(request.getParameter("mode") !=null) {
				if(request.getParameter("mode").equals("jabatan") || request.getParameter("mode").equals("satker")) {
		        	if(request.getParameter("id_kelompok")!=null) { %>
		          		<div id="progress-id-kelompok-<%=i%>" data-id="" class="card-body " onclick="window.location.href=window.location.search.split('&')[0]+'&id_kelompok='+$('#progress-id-kelompok-<%=i%>').data('id')+'&kode-sesi='+'<% out.print(request.getParameter("kode-sesi")); %>'" style="background: white; padding: 15px;border-radius:10px">
		        <%	}else{ %>
		        		<div id="progress-id-kelompok-<%=i%>" data-id="" class="card-body " onclick="setKelompokJabatan(this)" style="background: white; padding: 15px;border-radius:10px">
		        	<%}
		        } 
	       }else{ %>
        	<div onclick="setKelompokJabatan(this)" id="progress-id-kelompok-<%=i%>" data-id="" class="card-body "  style="background: white; padding: 15px;border-radius:10px">
        <% } %>
	          	<div class="row no-gutters">
		            <div class="col-6">
		                <div class="text">
		                  <h5 class="text-secondary text-wrap" id="progress-nama-kode-kelompok-berjalan-<%=i%>"></h5>
		                  <span class="text-dark small">Responden</span>
		                  <h5 class="text-dark"><span style="color: red" id="julah-peserta-berjalan-<%=i%>"></span>/<span id="total-peserta-<%=i%>"></span></h5>
		                </div>
		            </div>
		            <div class="col-6">
		              <div class="overview-boxs clearfix">
		                <div style="height: 100px !important;" class="progress-chart-<%=i%>" style=""> <!-- zoom: 1.1764705882352942 -->
		                  <canvas id="progress-chart-<%=i%>" width="100" height="50" class="progress-chart-<%=i%>"></canvas>
		                </div>
		              </div>
		            </div>
		        </div>
	            <div class="overview-boxs clearfix">
	            <div class="text text-center" style="">
	             	<h5 class="text-dark"><a href="#">Lihat Hasil</a></h5>
	              </div>
	            </div>
          	</div>
        </div>
        <% } %>
      </div>
 </div>