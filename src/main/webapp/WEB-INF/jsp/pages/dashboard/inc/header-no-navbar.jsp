<!DOCTYPE html>
<html lang="en">

<%@ include file = "./head.jsp" %>
<style>
.menu-sidebar__content {
  height: 90% !important;
}
.section__content--p30 {
    padding: 0 0px!important;
}
body{
	/* zoom: 85%; */
}
@page {
	size : auto;
}

@media (min-width: 992px){
	.pl-lg-default, .px-lg-default {
	    padding-left: 265px!important;
	}
	.left-lg-default {
	    left: 265px!important;
	}
	.width-lg-default {
	    width: 265px!important;
	}
}

@media print {.bg-danger tr td  {
    color: white !important;
    background-color: #dc3545!important;
}}

@media print {tr.bg-secondary td {
    color: white !important;
    background-color: #6c757d!important;
}}
@media print {tr.bg-danger td {
    color: white !important;
    background-color: #dc3545!important;
}}

@media print {.bg-danger tr  {
    color: white !important;
    background-color: #dc3545!important;
}}
@media print {tr td.bg-danger  {
    color: white !important;
    background-color: #dc3545!important;
}}
@media print {.bg-secondary th  {
    color: white !important;
    background-color: #6c757d!important;
}}
@media print {td.bg-secondary  {
    color: white !important;
    background-color: #6c757d!important;
}}
@media print {th.bg-secondary  {
    color: white !important;
    background-color: #6c757d!important;
}}
.text-verti  {
	writing-mode: tb-rl;
   	transform: rotateZ(180deg);
   	height: min-content;
}

@media print {.text-verti  {
	writing-mode: none;
   	transform: none;
   	height: min-content;
}}
@media print {h5.secondary-color  {
    color: #91a9b3 !important;
}}
@media print {.table-striped thead tr th, .table-striped tfoot tr th {
    background: #84ACB6 !important;
    color: white;
}}
</style>

<body class="animsition-not" >
  <div class="page-wrapper">
  
    <%@ include file = "./sidebar-with-navbar.jsp" %>

    <div class="page-container pl-lg-default">

	<%@ include file = "./new-header.jsp" %>
      