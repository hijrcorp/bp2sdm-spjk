<!-- HEADER DESKTOP-->
  <header class="header-desktop left-lg-default" style="background-color: #597D88;">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="header-wrap">
          <div class="account-wrap">
            <div class="account-item clearfix js-item-menu">
              <div class="image">
                <%-- <img src="${userPicture}" data-src="${userPicture}" alt="${requestScope.realName}" /> --%>
                <img src="${contextPath}/images/logo-menlhk.png" data-src="${assetPath}/images/icon/user.png" alt="${requestScope.realName}" />
              </div>
              <div class="content">
                <a class="js-acc-btn" href="#" style="color:white">${requestScope.realName}</a>
              </div>
              <div class="account-dropdown js-dropdown">
                <div class="info clearfix">
                  <div class="image">
                    <a href="#">
                    	<%-- <img src="${userPicture}" data-src="${userPicture}" alt="${requestScope.realName}" /> --%>
                      <img src="${assetPath}/images/icon/user.png" data-src="${assetPath}/images/icon/user.png" alt="${requestScope.realName}" />
                    </a>
                  </div>
                  <div class="content">
                    <h5 class="name"><a href="#">${requestScope.realName}</a></h5>
                    <span class="email">${requestScope.organization}</span>
                  </div>
                </div>
                <div class="account-dropdown__body">
                  <div class="account-dropdown__item">
                    <a href="${contextPath}/login-password"><i class="zmdi zmdi-hc-fw zmdi-key"></i>Ubah Password</a>
                  </div>
                  <div class="account-dropdown__item">
                    <a onclick="showListSesi()" href="javascript:void(0)"><i class="zmdi zmdi-settings"></i>Sesi</a>
                  </div>
                </div>
                <div class="account-dropdown__footer">
                  <a href="${contextPath}/login-logout"><i class="zmdi zmdi-hc-fw zmdi-power"></i>Logout</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
<!-- HEADER DESKTOP-->
