<div class="row">
  <div class="col-md-12">
    <div class="copyright">
      <p class="text-dark">
        Copyright � 2020 -  <%= new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %> <a href="#" class="text-primary">KLHK BP2SDM</a>. <br/>All rights reserved.
      </p>
    </div>
  </div>
</div>