<%@ include file = "inc/header.jsp" %>

<div id="wrapper" class="main-content">

	<div id="content-wrapper" class="section__content section__content--p20">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Penentuan Bukti</a></li>
				<li class="breadcrumb-item active" id="menu"></li>
			</ol>
			
			<div class="card">
				<div class="card-header">
					<h4><span id="menu"></span></h4>
				</div>
				<div class="card-body">
					<div class="default-tab">
						<nav><div class="nav nav-tabs" id="nav-tab" role="tablist"></div></nav>
						<div class="tab-content pl-0 pt-0" id="nav-tab-content"></div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.container-fluid -->

		<%@ include file = "inc/trademark.jsp" %>	

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->



<div id="template-content" style="display: none">
	<div class="card pt-2" id="card-data-index" style="border-top: 0px">
		<div class="card-body mb-0 pb-0"><h3 class="card-text"><span class="custom-title">1. Penegakan Integritas dan Nilai Etika (1.1)</span></h3></div> <!-- Sub Unsur -->
		<div class="card-body">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th rowspan="2" width="70" class="text-center">Tingkat</th>
						<th rowspan="2" width="100" class="text-center">No. Bukti</th>
						<th rowspan="2" width="100" class="text-center">Kode Bukti</th>
						<th colspan="4" width="100" class="text-center">Bukti Pengujian</th>
					</tr>
					<tr>
						<th class="text-center" width="100">Dokumen</th>
						<th class="text-center" width="100">Kuesioner Lanjutan</th>
						<th class="text-center" width="100">Wawancara</th>
						<th class="text-center" width="100">Observasi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">1</td>
						<td class="text-center">1.1.1</td>
						<td class="text-center">D.1.1.1.1</td>
						<td class="text-center">D.1.1.1.1</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center">2</td>
						<td class="text-center">1.1.2</td>
						<td class="text-center">D.1.1.2.2</td>
						<td class="text-center">D.1.1.2.2</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.2</td>
						<td class="text-center">D.1.1.2.2</td>
						<td class="text-center">D.1.1.2.2</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.2</td>
						<td class="text-center">KL.1.1.2.1</td>
						<td class="text-center"></td>
						<td class="text-center">KL.1.1.2.1</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.2</td>
						<td class="text-center">W.1.1.2.1</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center">W.1.1.2.1</td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center">3</td>
						<td class="text-center">1.1.3</td>
						<td class="text-center">D.1.1.3.2</td>
						<td class="text-center">D.1.1.3.2</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.3</td>
						<td class="text-center">KL.1.1.3.1</td>
						<td class="text-center"></td>
						<td class="text-center">KL.1.1.3.1</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.3</td>
						<td class="text-center">KL.1.1.3.2</td>
						<td class="text-center"></td>
						<td class="text-center">KL.1.1.3.2</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.3</td>
						<td class="text-center">KL.1.1.3.3</td>
						<td class="text-center"></td>
						<td class="text-center">KL.1.1.3.3</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.3</td>
						<td class="text-center">W.1.1.3.2</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center">W.1.1.3.2</td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.3</td>
						<td class="text-center">W.1.1.3.3</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center">W.1.1.3.3</td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center">4</td>
						<td class="text-center">1.1.4</td>
						<td class="text-center">D.1.1.4.2</td>
						<td class="text-center">D.1.1.4.2</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">1.1.4</td>
						<td class="text-center">D.1.1.4.2</td>
						<td class="text-center">D.1.1.4.2</td>
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>


<!-- Form Modal-->
<div class="modal fade" id="modal-form" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="entry-form">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
					<input type="hidden" name="jenis" />
					<input type="hidden" name="id_unsur" />
				
					<div class="form-group row">
						<div class="col-md-3">
							<label>Sub Unsur</label>
						</div>
						<div class="col-md-9">
							<select name="id_sub_unsur" class="form-control select2">
							</select>
						</div>
					</div> 
				
					<div class="form-group row">
						<div class="col-md-3">
							<label>Tingkat</label>
						</div>
						<div class="col-md-9">
							<select name="id_maturitas_tingkat" class="form-control select2">
							</select>
						</div>
					</div>
				
					<div class="form-group row">
						<div class="col-md-3">
							<label>Kode</label>
						</div>
						<div class="col-md-9">
							<input type="text" name="kode_pernyataan" class="form-control" />
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-sm-3">
							<label>Pernyataan</label>
						</div>
						<div class="col-sm-9">
							<textarea name="judul_pernyataan" class="form-control" rows="4"></textarea>
						</div>
					</div>
				
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
					<button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="${assetPath}/js/penentuan-bukti-pengujian.js"></script>
<%@ include file = "inc/footer.jsp" %>