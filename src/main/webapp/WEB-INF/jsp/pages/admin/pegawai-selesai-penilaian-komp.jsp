<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: PEGAWAI SELESAI PENILAIAN KOMPETENSI</li>
                            </ol>

                            <div class="card mb-3">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging  -->
                                        <input type="hidden" name="limit" value="2">
                                        
								        <div class="form-group row">
							                <label class="col-sm-2 col-form-label">Unit Kerja</label>
								            <div class="col-md-4">
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
							                <label class="col-sm-2 col-form-label">Tahun</label>
								            <div class="col-md-4">
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-group row">
							                <label class="col-sm-2 col-form-label">Unit Eselon I</label>
								            <div class="col-md-4">
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
							                <label class="col-sm-2 col-form-label">Jabatan</label>
								            <div class="col-md-4">
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-group row">
							                <label class="col-sm-2 col-form-label">Nama</label>
								            <div class="col-md-4">
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
							                <label class="col-sm-2 col-form-label">NIP</label>
								            <div class="col-md-4">
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-group row">
							                <label class="col-md-2">Nilai</label>
								            <div class="col-md-4">
								                <div class="form-check form-check-inline">
												  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
												  <label class="form-check-label" for="inlineRadio1">0-25</label>
												</div>
												<div class="form-check form-check-inline">
												  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
												  <label class="form-check-label" for="inlineRadio2">26-50</label>
												</div>
												<div class="form-check form-check-inline">
												  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
												  <label class="form-check-label" for="inlineRadio3">51-75</label>
												</div>
												<div class="form-check form-check-inline">
												  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option3">
												  <label class="form-check-label" for="inlineRadio4">76-100</label>
												</div>
								            </div>
								        </div>

                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <i class="fas fa-table"></i> Tabel Data
                                    <div class="float-right">
                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tampilkan</button>
                                        <button class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i> Download</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th rowspan="2">No</th>
                                                    <th rowspan="2">Nama/NIP</th>
                                                    <th rowspan="2">Jabatan</th>
                                                    <th colspan="2">Hasil Penilaian</th>
                                                    <th rowspan="2">Keterangan</th>
                                                    <th rowspan="2" class="text-center">Aksi</th>
                                                </tr>
                                                <tr>
                                                    <th>Manajerial</th>
                                                    <th>Teknis</th>
                                                </tr>
                                            </thead>
                                            <tbody>
												<tr>
													<td>1</td>
													<td>Xxxxxxx/999</td>
													<td>Xxxxxxx xxxxxx</td>
													<td>99</td>
													<td>999</td>
													<td></td>
													<td>Detil</td>
												</tr>
												<tr>
													<td>2</td>
													<td>Xxxxxxx/999</td>
													<td>Xxxxxxx xxxxxx</td>
													<td>99</td>
													<td>999</td>
													<td></td>
													<td>Detil</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Xxxxxxx/999</td>
													<td>Xxxxxxx xxxxxx</td>
													<td>99</td>
													<td>999</td>
													<td></td>
													<td>Detil</td>
												</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" style="display: none;"></div>
							        <input type="text" class="form-control d-none" name="id" autocomplete="off">
							
							        <div class="form-row">
							            <div class="form-group col-md-12">
							                <label>Kode Kelompok Jabatan</label>
							                <input type="text" name="kode" class="form-control">
							            </div>
							        </div>
							        <div class="form-row">
							            <div class="form-group col-md-12">
							                <label>Nama Kelompok Jabatan</label>
							                <input type="text" name="nama" class="form-control">
							            </div>
							        </div>
							
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>

                <%-- <script src="${pageContext.request.contextPath}/js/kelompok-jabatan.js"></script> --%>

    </body>

</html>