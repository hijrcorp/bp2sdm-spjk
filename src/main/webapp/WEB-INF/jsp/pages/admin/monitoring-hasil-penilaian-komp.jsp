<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: Penilaian Kompetensi</li>
                            </ol>

                            <div class="card mb-3">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging  -->
                                        <input type="hidden" name="limit" value="2">
                                        
								        <div class="form-row">
								            <div class="form-group col-md-6">
								                <label>Unit Kerja</label>
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6">
								                <label>Tahun</label>
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>
								
                                        
								        <div class="form-row">
								            <div class="form-group col-md-6">
								                <label>Unit Eselon I</label>
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6">
								                <label>Jabatan</label>
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>

                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <i class="fas fa-table"></i> Tabel Data
                                    <div class="float-right">
                                        <button id="btn-add" data-toggle="modal" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tampilkan</button>
                                    	<button id="btn-add" data-toggle="modal" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Download</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th rowspan="3">No</th>
                                                    <th rowspan="3" width="15%">Jabatan Fungsional</th>
                                                    <th rowspan="3">Jumlah</th>
                                                    <th rowspan="3">Selesai</th>
                                                    <th rowspan="3">%</th>
                                                    <th colspan="8" class="text-center">Hasil Penilaian Kompetensi</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center" colspan="2">0-25</th>
                                                    <th class="text-center" colspan="2">26-50</th>
                                                    <th class="text-center" colspan="2">51-75</th>
                                                    <th class="text-center" colspan="2">76-100</th>
                                                </tr>
                                                <tr>
                                                    <th>Jumlah</th>
                                                    <th>%</th>
                                                    <th>Jumlah</th>
                                                    <th>%</th>
                                                    <th>Jumlah</th>
                                                    <th>%</th>
                                                    <th>Jumlah</th>
                                                    <th>%</th>
                                                </tr>
                                            </thead>
                                            <tbody>
												<tr>
													<td>1</td>
													<td>PEH Ahli</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
												</tr>
												<tr>
													<td>2</td>
													<td>PEH Terampil</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Penyuluh</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
												</tr>
                                            </tbody>
                                            <tfoot>
												<tr>
													<td></td>
													<td></td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
													<td>99</td>
												</tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" style="display: none;"></div>
							        <input type="text" class="form-control d-none" name="id" autocomplete="off">
							
							        <div class="form-row">
							            <div class="form-group col-md-12">
							                <label>Kode Kelompok Jabatan</label>
							                <input type="text" name="kode" class="form-control">
							            </div>
							        </div>
							        <div class="form-row">
							            <div class="form-group col-md-12">
							                <label>Nama Kelompok Jabatan</label>
							                <input type="text" name="nama" class="form-control">
							            </div>
							        </div>
							
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>

                <%-- <script src="${pageContext.request.contextPath}/js/kelompok-jabatan.js"></script> --%>

    </body>

</html>