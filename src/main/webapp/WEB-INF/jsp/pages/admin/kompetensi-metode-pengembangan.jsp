<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
		
		.header-fixed {
		    position: fixed;
		    top: 0px; display:none;
		    background-color:white;
		}
		
		/* table, tr td {
		    border: 1px solid red
		}
		tbody {
		    display: block;
		    height: 50px;
		    overflow: auto;
		}
		thead, tbody tr {
		    display: table;
		    width: 100%;
		    table-layout: fixed;

		}
		thead {
		    width: calc( 100% - 1em )
		}
		table {
		    width: 400px;
		} */
		/* even columns width , fix width of table too*/
		/* scrollbar is average 1em/16px width, remove it from thead width */
		/* 
		thead, tbody { display: block; }
		
		tbody {
		    height: 100px;      
		    overflow-y: auto;   
		    overflow-x: hidden;  
		} */
		
		#fixedContainer {
		  position: fixed;
		  width: 600px;
		  height: 200px;
		  left: 50%;
		  top: 0%;
		  margin-left: -300px; /*half the width*/
		}
	</style>
	
	<link href="${contextPath}/vendor/stickytable/jquery.stickytable.css" rel="stylesheet"> 
	<style>
	.sticky-table table tr.sticky-header th, .sticky-table table tr.sticky-header td, .sticky-table table tr.sticky-footer th, .sticky-table table tr.sticky-footer td {
		background-color:#343a40!important;
	}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: KOMPETENSI METODE PENGEMBANGAN</li>
                            </ol>

                            <div class="card mb-3 d-none">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging  -->
                                        <input type="hidden" name="limit" value="2">
                                        
								        <div class="form-row">
								            <div class="form-group col-md-6">
								                <label>Pegawai</label>
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6">
								                <label>Akun Bank</label>
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-row d-none">
								            <div class="form-group col-md-6">
								                <label>Tgl.Penggajian</label>
								                <input type="date" class="form-control" name="filter_tanggal_dari" autocomplete="off">
								            </div>
								            <div class="form-group col-md-6">
								                <label>Hingga</label>
								                <input type="date" class="form-control" name="filter_tanggal_hingga" autocomplete="off">
								            </div>
								        </div>

                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <!-- <i class="fas fa-table"></i> Tabel Data
                                    <div class="float-right">
                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                    </div> -->
                                    <nav>
									  <div class="nav nav-tabs" id="nav-tab" role="tablist">
									    <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PDL</a> -->
							    	  </div>
									</nav>
                                </div>
                                <div class="card-body">
                                	<div class="row mb-2">
                               		<div class="col-md-3">
								        <select class="form-control" name="filter_id_bidang_teknis">
								            <option value="">-- Pilih --</option>
								        </select>
								    </div>
                               		<div class="col-md-3">
								        <!-- <select class="form-control" name="filter_id_master_jabatan">
								            <option value="">-- Pilih --</option>
								        </select> -->
								    </div>
									    
                                	<div class="col-md-6 text-right mb-1 d-none">
                                        <button  id="btn-adds" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                    </div>
                                    
                                    </div>
                                	<div class="row">
                               		<div class="col-md-12" > 
                                    <div class="sticky-table sticky-ltr-cells">
                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" cellspacing="0" >
                                            <thead id="head" class="thead-dark small">
                                                <tr>
                                                    <th width="5%" rowspan="4">No</th>
                                                    <th rowspan="4">FUNGSI DASAR/UNIT KOMPETENSI</th>
                                                    <th rowspan="4">KODE UNIT</th>
                                                    <th class="text-center" colspan="8">Pemula</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center" colspan="4">Klasikal(<=30)</th>
                                                    <th class="text-center" colspan="4">Non-Klasikal(31-60)</th>
                                                </tr>
                                                <tr>
                                                		<th width="5%">20</th>
                                                		<th width="5%">30</th>
                                                		<th width="5%">30</th>
                                                		<th width="5%">30</th>
                                                		<th width="5%">50</th>
                                                		<th width="5%">40</th>
                                                		<th width="5%">60</th>
                                                		<th width="5%">30</th>
                                                </tr>
                                                <tr >
                                                	<!-- KLASIKAL -->
                                                    <th class="text-right" style="writing-mode: tb-rl;height:min-content" >Pelatihan</th>
                                                    <th class=text-right style="writing-mode: tb-rl;height:min-content" >Seminar/Konfrensi/Sarasehan</th>
                                                    <th class="text-right" style="writing-mode: tb-rl;height:min-content" >Workshop/Lokakarya</th>
                                                    <th class="text-right" style="writing-mode: tb-rl;height:min-content" >Kursus</th>
                                                    <!-- NON KLASIKAL -->
                                                    <th class="text-right" style="writing-mode: tb-rl;height:min-content" >Mentoring</th>
                                                    <th class=text-right style="writing-mode: tb-rl;height:min-content" >Choaching</th>
                                                    <th class="text-right" style="writing-mode: tb-rl;height:min-content" >E-Lerarning</th>
                                                    <th class="text-right" style="writing-mode: tb-rl;height:min-content" >Pelatihan Jarak Jauh</th>
                                                </tr>
                                            </thead>
                                            <tbody class="small">
												<tr>
                                                		<td>1</td>
                                                		<td>Loem dolor isp</td>
                                                		<td>KHT.PEH.001.01</td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td></td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td></td>
                                                </tr>
												<tr>
                                                		<td>2</td>
                                                		<td>Ipsum Loem dolor lor</td>
                                                		<td>KHT.PEH.002.01</td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td></td>
                                                		<td>V</td>
                                                		<td>V</td>
                                                		<td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        
                                    	
										<!-- <table class="header-fixed table table-bordered table-striped table-hover" cellspacing="0" ></table> -->
                                        
                                    </div>
                                    </div>
                                    </div>
                                    <!-- <div class="table-responsive" style="height: auto; overflow-y: scroll;">
                                    <table class="header-fixed table table-bordered table-striped table-hover" cellspacing="0" ></table>
                                    </div> -->
                                </div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert"></div>
							        <input type="text" class="form-control d-none" name="id" autocomplete="off">
								
							        <div class="form-row d-none">
									    <div class="form-group col-md-12">
									        <label>Kelompok Jabatan</label>
									        <select class="form-control" name="id_kelompok_jabatan">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Jabatan</label>
									        <select class="form-control select2" name="id_master_jabatan">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
									
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Bidang Teknis</label>
									        <select class="form-control d-none" name="id_bidang_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									        <input class="form-control" id="id_bidang_teknis" readonly>
									    </div>
									</div>
									
							        <div class="form-row">
								    <div class="form-group col-md-12">
								        <label>Jenis Kompetensi</label><br/>
								        <div class="custom-control custom-radio custom-control-inline">
										  <input value="INTI" type="radio" id="jenis_kompetensi1" name="jenis_kompetensi" class="custom-control-input">
										  <label class="custom-control-label" for="jenis_kompetensi1">INTI</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
										  <input value="PILIHAN" type="radio" id="jenis_kompetensi2" name="jenis_kompetensi" class="custom-control-input">
										  <label class="custom-control-label" for="jenis_kompetensi2">PILIHAN</label>
										</div>
								    </div>
									</div>
								    
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Kelompok Teknis</label>
									        <select class="form-control " name="id_kelompok_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
									
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Unit Kompetensi Teknis</label>
									        <select class="form-control select2" multiple="multiple" id="id_unit_kompetensi_teknis" name="id_unit_kompetensi_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
							
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>
                <script src="${pageContext.request.contextPath}/js/kompetensi-metode-pengembangan.js"></script>

    </body>
</html>