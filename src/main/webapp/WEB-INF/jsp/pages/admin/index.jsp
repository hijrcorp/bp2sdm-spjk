<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
<style>
	.g-0, .gy-0 {
    --bs-gutter-y: 0;
	}
	.g-0, .gx-0 {
	    --bs-gutter-x: 0;
	}
	.avatar-xl {
	    height: 2rem;
	    width: 2rem;
	}
	.avatar {
	    position: relative;
	    display: inline-block;
	}
	.pl-card, .px-card, .p-card {
	    padding-left: 1.25rem !important;
	}
	.avatar-xl .avatar-name {
	    font-size: .66667rem;
	}
	.avatar img, .avatar .avatar-name {
	    width: 100%;
	    height: 100%;
	}
	.avatar .avatar-name {
	    background-color: #344050;
	    position: absolute;
	    text-align: center;
	    color: #fff;
	    font-weight: bold;
	    text-transform: uppercase;
	    display: block;
	}
	.bg-soft-primary {
	    background-color: #e6effc !important;
	}
	.avatar .avatar-name>span {
	    position: absolute;
	    top: 50%;
	    left: 50%;
	    -webkit-transform: translate3d(-50%, -50%, 0);
	    transform: translate3d(-50%, -50%, 0);
	}
	.fs-0 {
	    font-size: 1rem !important;
	}
	.fs--1 {
	    font-size: .83333rem !important;
	}
	.font-weight-semi-bold {
	    font-weight: 600 !important;
	}
	.pr-card, .px-card, .p-card {
	    padding-right: 1.25rem !important;
	}
</style>
  <body id="page-top">

    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%@ include file = "inc/sidebar.jsp" %>  

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb bg-info text-white">
            <li class="breadcrumb-item ">Selamat Datang di Aplikasi SPeKtra</li>
          </ol>
		<% if(request.getParameter("debug")==null) { %>	
		<c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SUPER_ADMIN')}">
		<div id="container-dashboard" class="row ">
		    <!-- <div class="col col-xxl-3 mb-3 pr-md-2">
		        <div class="card border-primary h-md-100">
		            <div class="card-header bg-transparent border-bottom-0 pb-0">
		                <h6 class="mb-0 mt-2 d-flex align-items-center">
		                    PEH
		                </h6>
		            </div>
		            <div class="card-body align-items-end">
		                <div class="row flex-end-center d-flex align-items-center">
						<div class="col-auto pr-2">
						  <div class="fs--1 font-weight-semi-bold">12 %</div>
						</div>
						<div class="col pr-card pl-2">
						  <div class="progress mr-2" style="height: 8px;">
						    <div class="progress-bar rounded-capsule" role="progressbar" style="width: 12%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
						  </div>
						</div>
						</div>
		            </div>
		        </div>
		    </div> -->
		    <!-- <div class="col col-xxl-3 mb-3 pr-md-2">
		        <div class="card border-success h-md-100">
		            <div class="card-header bg-transparent border-bottom-0 pb-0">
		                <h6 class="mb-0 mt-2 d-flex align-items-center">
		                    POLHUT
		                </h6>
		            </div>
		            <div class="card-body align-items-end">
		                <div class="row flex-end-center d-flex align-items-center">
						<div class="col-auto pr-2">
						  <div class="fs-4 font-weight-normal text-sans-serif text-700 line-height-1 mb-1">19%</div>
						  <div class="fs--1 font-weight-semi-bold">50 %</div>
						</div>
						<div class="col pr-card pl-2">
						  <div class="progress mr-2" style="height: 8px;">
						    <div class="progress-bar rounded-capsule" role="progressbar" style="width: 19%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
						  </div>
						</div>
						</div>
		            </div>
		        </div>
		    </div>
		    
		    <div class="col col-xxl-3 mb-3 pr-md-2">
		        <div class="card border-info h-md-100">
		            <div class="card-header bg-transparent border-bottom-0 pb-0">
		                <h6 class="mb-0 mt-2 d-flex align-items-center">
		                    PLH
		                </h6>
		            </div>
		            <div class="card-body align-items-end">
		                <div class="row flex-end-center d-flex align-items-center">
						<div class="col-auto pr-2">
						  <div class="fs-4 font-weight-normal text-sans-serif text-700 line-height-1 mb-1">3%</div>
						  <div class="fs--1 font-weight-semi-bold">50 %</div>
						</div>
						<div class="col pr-card pl-2">
						  <div class="progress mr-2" style="height: 8px;">
						    <div class="progress-bar rounded-capsule" role="progressbar" style="width: 3%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
						  </div>
						</div>
						</div>
		            </div>
		        </div>
		    </div>
		    
		    <div class="col col-xxl-3 mb-3 pr-md-2">
		        <div class="card border-success h-md-100">
		            <div class="card-header bg-transparent border-bottom-0 pb-0">
		                <h6 class="mb-0 mt-2 d-flex align-items-center">
		                    PEDAL
		                </h6>
		            </div>
		            <div class="card-body align-items-end">
		                <div class="row flex-end-center d-flex align-items-center">
						<div class="col-auto pr-2">
						  <div class="fs-4 font-weight-normal text-sans-serif text-700 line-height-1 mb-1">5%</div>
						  <div class="fs--1 font-weight-semi-bold">50 %</div>
						</div>
						<div class="col pr-card pl-2">
						  <div class="progress mr-2" style="height: 8px;">
						    <div class="progress-bar rounded-capsule" role="progressbar" style="width: 5%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
						  </div>
						</div>
						</div>
		            </div>
		        </div>
		    </div>
		    
		    <div class="col col-xxl-3 mb-3 pr-md-2">
		        <div class="card border-primary h-md-100">
		            <div class="card-header bg-transparent border-bottom-0 pb-0">
		                <h6 class="mb-0 mt-2 d-flex align-items-center">
		                    PK
		                </h6>
		            </div>
		            <div class="card-body align-items-end">
		                <div class="row flex-end-center d-flex align-items-center">
						<div class="col-auto pr-2">
						  <div class="fs-4 font-weight-normal text-sans-serif text-700 line-height-1 mb-1">2%</div>
						  <div class="fs--1 font-weight-semi-bold">50 %</div>
						</div>
						<div class="col pr-card pl-2">
						  <div class="progress mr-2" style="height: 8px;">
						    <div class="progress-bar rounded-capsule" role="progressbar" style="width: 2%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
						  </div>
						</div>
						</div>
		            </div>
		        </div>
		    </div> -->
		</div>
		<div class="row ">
		    <div class="col-xl-8 col-xxl-4 mb-3 ">
		        <div class="card h-lg-100">
		            <div class="card-body position-relative">
		                <canvas id="myChart"></canvas>
		            </div>
		        </div>
		    </div>
		    <div class="col-lg-4 col-xxl-8 mb-3 ">
		        <div class="card h-lg-100">
		            <div class="card-header ">
		                <h6 class="mb-0 d-flex align-items-center text-muted">Progress Input Sub Bidang PEH</h6>
		            </div>
		            <div class="card-body d-flex align-items-center">
		                <div id="container-bidang" class="w-100">
		                   <!--
		                    <div class="mb-1 small">Bidang Pemanfaatan Hasil Hutan <span class="float-right">40%</span></div>
		                    <div class="progress mb-3 rounded-soft" style="height: 10px;">
		                        <div class="progress-bar bg-warning border-right border-white border-2x" role="progressbar" style="width: 40%;" aria-valuenow="43.72" aria-valuemin="0" aria-valuemax="100"></div>
		                    </div>
		                     
		                    <div class="mb-1 small">Bidang Rehabilitasi Hutan dan Pengelolaan DAS <span class="float-right">60%</span></div>
		                    <div class="progress mb-3 rounded-soft" style="height: 10px;">
		                        <div class="progress-bar bg-primary border-right border-white border-2x" role="progressbar" style="width: 60%;" aria-valuenow="43.72" aria-valuemin="0" aria-valuemax="100"></div>
		                    </div>
		                    
		                    <div class="mb-1 small">Bidang Konservasi Sumberdaya Hutan <span class="float-right">80%</span></div>
		                    <div class="progress mb-3 rounded-soft" style="height: 10px;">
		                        <div class="progress-bar bg-success border-right border-white border-2x" role="progressbar" style="width: 80%;" aria-valuenow="43.72" aria-valuemin="0" aria-valuemax="100"></div>
		                    </div> -->
		                    <!-- pake ini kalo mau untuk keterangan warna -->
		                    <!-- <div class="row fs--1 font-weight-semi-bold text-500">
		                        <div class="col-auto d-flex align-items-center pr-2"><span class="dot bg-primary"></span><span>Regular</span><span class="d-none d-md-inline-block d-lg-none d-xxl-inline-block ml-1">(895MB)</span></div>
		                        <div class="col-auto d-flex align-items-center px-2"><span class="dot bg-info"></span><span>System</span><span class="d-none d-md-inline-block d-lg-none d-xxl-inline-block ml-1">(379MB)</span></div>
		                        <div class="col-auto d-flex align-items-center px-2"><span class="dot bg-success"></span><span>Shared</span><span class="d-none d-md-inline-block d-lg-none d-xxl-inline-block ml-1">(192MB)</span></div>
		                        <div class="col-auto d-flex align-items-center pl-2"><span class="dot bg-200"></span><span>Free</span><span class="d-none d-md-inline-block d-lg-none d-xxl-inline-block ml-1">(576MB)</span></div>
		                    </div> -->
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		</c:if>
		<c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_RESPONDEN')}">
		<div class="row ">
		    <div class="col-12 mb-3 ">
		        <div class="card h-lg-100">
		            <div class="card-body position-relative">
		                <div class="alert alert-danger">Mohon maaf anda semestinya tidak berada dipage ini, klik <a href="${ssoPath}/update-username">disini</a> untuk memperbaiki.</div>
		            </div>
		        </div>
		    </div>
		</div>
		</c:if>
		<% }else{ %>
		<!-- this just for programmer -->
		<div class="card mb-3 d-none" id="recentPurchaseTable" data-list="{&quot;valueNames&quot;:[&quot;name&quot;,&quot;email&quot;,&quot;product&quot;,&quot;payment&quot;,&quot;amount&quot;],&quot;page&quot;:8,&quot;pagination&quot;:true}">
            <div class="card-header">
              <div class="row flex-between-center">
                <div class="col-6 col-sm-auto d-flex align-items-center pr-0">
                  <h5 class="fs-0 mb-0 text-nowrap py-2 py-xl-0">Monitoring Status Responden</h5>
                </div>
                <div class="col-6 col-sm-auto ml-auto text-right pl-0">
                  <div class="d-none" id="table-purchases-actions">
                    <div class="d-flex"><select class="form-select form-select-sm" aria-label="Bulk actions">
                        <option selected="">Bulk actions</option>
                        <option value="Refund">Refund</option>
                        <option value="Delete">Delete</option>
                        <option value="Archive">Archive</option>
                      </select><button class="btn btn-falcon-default btn-sm ml-2" type="button">Apply</button></div>
                  </div>
                  <div id="table-purchases-replace-element"><button class="btn btn-falcon-default btn-sm" type="button"><span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span> <span class="d-none d-sm-inline-block ml-1">New</span></button><button class="btn btn-falcon-default btn-sm mx-2" type="button"><span class="fas fa-filter" data-fa-transform="shrink-3 down-2"></span> <span class="d-none d-sm-inline-block ml-1">Filter</span></button><button class="btn btn-falcon-default btn-sm" type="button"> <span class="fas fa-external-link-alt" data-fa-transform="shrink-3 down-2"></span> <span class="d-none d-sm-inline-block ml-1">Export</span></button></div>
                </div>
              </div>
            </div>
            <div class="card-body px-0 py-0">
              <div class="table-responsive scrollbar">
                <table class="table small fs--1 mb-0">
                  <thead class="bg-200 text-900">
                    <tr>
                      <th class="align-middle" style="width: 28px;">
                        <div class="form-check mb-0 d-flex align-items-center"><input class="form-check-input" id="checkbox-bulk-purchases-select" type="checkbox" data-bulk-select="{&quot;body&quot;:&quot;table-purchase-body&quot;,&quot;actions&quot;:&quot;table-purchases-actions&quot;,&quot;replacedElement&quot;:&quot;table-purchases-replace-element&quot;}"></div>
                      </th>
                      <th class="sort pr-1 align-middle white-space-nowrap" data-sort="name">Total Responden</th>
                      <th class="sort pr-1 align-middle white-space-nowrap" data-sort="email">Responden Selesai</th>
                      <th class="sort pr-1 align-middle white-space-nowrap" data-sort="product">Responden Proses</th>
                      <th class="sort pr-1 align-middle white-space-nowrap text-center" data-sort="payment">Responden Tidak Menjawab</th>
                      <th class="sort pr-1 align-middle white-space-nowrap text-center" data-sort="payment">Duplicate Responden</th>
                      <th class="sort pr-1 align-middle white-space-nowrap text-center" data-sort="payment">Bidang Teknis Invalid</th>
                      <th class="sort pr-1 align-middle white-space-nowrap text-center" data-sort="payment">Jabatan Invalid</th>
                      <th class="no-sort pr-1 align-middle data-table-row-action"></th>
                    </tr>
                  </thead>
                  <tbody class="list" id="table-monitoring-body">
                  	<!-- <tr class="btn-reveal-trigger">
                      <td class="align-middle" style="width: 28px;">
                        <div class="form-check mb-0 d-flex align-items-center"><input class="form-check-input" type="checkbox" id="checkbox-0" data-bulk-select-row="data-bulk-select-row"></div>
                      </td>
                      <th class="align-middle white-space-nowrap name"><a href="../pages/customer-details.html">Sylvia Plath</a></th>
                      <td class="align-middle white-space-nowrap email">john@gmail.com</td>
                      <td class="align-middle white-space-nowrap product">Slick - Drag &amp; Drop Bootstrap Generator</td>
                      <td class="align-middle text-center fs-0 white-space-nowrap payment"><span class="badge badge rounded-pill badge-soft-success">Success <span class="ml-1 fas fa-check" data-fa-transform="shrink-2"></span> </span></td>
                      <td class="align-middle text-right amount">$99</td>
                    </tr>
                    <tr class="btn-reveal-trigger">
                      <td class="align-middle" style="width: 28px;">
                        <div class="form-check mb-0 d-flex align-items-center"><input class="form-check-input" type="checkbox" id="checkbox-2" data-bulk-select-row="data-bulk-select-row"></div>
                      </td>
                      <th class="align-middle white-space-nowrap name"><a href="../pages/customer-details.html">Edgar Allan Poe</a></th>
                      <td class="align-middle white-space-nowrap email">edgar@yahoo.com</td>
                      <td class="align-middle white-space-nowrap product">All-New Fire HD 8 Kids Edition Tablet</td>
                      <td class="align-middle text-center fs-0 white-space-nowrap payment"><span class="badge badge rounded-pill badge-soft-secondary">Blocked <span class="ml-1 fas fa-ban" data-fa-transform="shrink-2"></span> </span></td>
                      <td class="align-middle text-right amount">$199</td>
                    </tr>
                    <tr class="btn-reveal-trigger">
                      <td class="align-middle" style="width: 28px;">
                        <div class="form-check mb-0 d-flex align-items-center"><input class="form-check-input" type="checkbox" id="checkbox-5" data-bulk-select-row="data-bulk-select-row"></div>
                      </td>
                      <th class="align-middle white-space-nowrap name"><a href="../pages/customer-details.html">Emily Dickinson</a></th>
                      <td class="align-middle white-space-nowrap email">emily@gmail.com</td>
                      <td class="align-middle white-space-nowrap product">Mirari OK to Wake! Alarm Clock &amp; Night-Light</td>
                      <td class="align-middle text-center fs-0 white-space-nowrap payment"><span class="badge badge rounded-pill badge-soft-warning">Pending <span class="ml-1 fas fa-stream" data-fa-transform="shrink-2"></span> </span></td>
                      <td class="align-middle text-right amount">$11</td>
                    </tr> -->
                    </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer">
              <div class="row align-items-center">
                <div class="pagination d-none"><li class="active"><a class="page" href="javascript:function Z(){Z=&quot;&quot;}Z()">1</a></li><li><a class="page" href="javascript:function Z(){Z=&quot;&quot;}Z()">2</a></li></div>
                <div class="col">
                  <p class="mb-0 fs--1"><span class="d-none d-sm-inline-block mr-2" data-list-info="data-list-info">1 to 8 of 11</span><span class="d-none d-sm-inline-block mr-2">—</span><a class="font-weight-semi-bold" href="#!" data-list-view="*">View all<span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span></a></p>
                </div>
                <div class="col-auto d-flex"><button class="btn btn-sm btn-light" type="button" data-list-pagination="prev" disabled=""><span>Previous</span></button><button class="btn btn-sm px-4 ml-2 btn-primary" type="button" data-list-pagination="next"><span>Next</span></button></div>
              </div>
            </div>
          </div>
          
          <div class="row g-0">
          	<div class="col-md-6 pr-lg-2 mb-3">
          		<div class="card h-lg-100 overflow-hidden">
                <div class="card-header bg-light">
                  <div class="row align-items-center">
                    <div class="col">
                      <h6 class="mb-0">Running Simulasi(Ujian Responden)</h6>
                    </div>
                    <div class="d-none col-auto text-center pr-card"><select class="form-select form-select-sm">
                        <option>Working Time</option>
                        <option>Estimated Time</option>
                        <option>Billable Time</option>
                      </select></div>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="row g-0 align-items-center py-2 position-relative border-bottom border-200 ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-primary text-dark"><span class="fs-0 text-primary">T</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Total</a><span id="total-responden-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="total-responden-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center py-2 position-relative border-bottom border-200 ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-success text-dark"><span class="fs-0 text-success">S</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Selesai</a><span id="selesai-responden-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="selesai-responden-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="79" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center py-2 position-relative border-bottom border-200 ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-info text-dark"><span class="fs-0 text-info">P</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Proses</a><span id="proses-responden-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="proses-responden-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center py-2 position-relative border-bottom border-200 ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-warning text-dark"><span class="fs-0 text-warning">TM</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Tidak Menjawab</a><span id="no-proses-responden-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="no-proses-responden-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer bg-light p-0"><a class="btn btn-sm btn-link btn-block py-2" href="#!">Show all status<span class="fas fa-chevron-right ml-1 fs--2"></span> </a></div>
              </div>
          	</div>
          	
          	<div class="col-md-6 pr-lg-2 mb-3">
          		<div class="card h-lg-100 overflow-hidden">
                <div class="card-header bg-light">
                  <div class="row align-items-center">
                    <div class="col">
                      <h6 class="mb-0">Status Responden(Issue)</h6>
                    </div>
                    <div class="d-none col-auto text-center pr-card"><select class="form-select form-select-sm">
                        <option>Working Time</option>
                        <option>Estimated Time</option>
                        <option>Billable Time</option>
                      </select></div>
                  </div>
                </div>
                <div class="card-body p-0">
                  <div class="row g-0 align-items-center py-2 position-relative border-bottom border-200 ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-primary text-dark"><span class="fs-0 text-primary">JI</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Jabatan Invalid</a><span id="jabatan-invalid-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="jabatan-invalid-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center py-2 position-relative border-bottom border-200 ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-success text-dark"><span class="fs-0 text-success">BTI</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Bidang Teknis Invalid</a><span id="bidang-teknis-invalid-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="bidang-teknis-invalid-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="79" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center py-2 position-relative ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-danger text-dark"><span class="fs-0 text-danger">DR</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Duplicate Responden</a><span id="duplicate-responden-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="duplicate-responden-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row g-0 align-items-center py-2 position-relative ml-0">
                    <div class="col pl-card py-1 position-static">
                      <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl mr-3">
                          <div class="avatar-name rounded-circle bg-soft-danger text-dark"><span class="fs-0 text-danger">DM</span></div>
                        </div>
                        <div class="flex-1">
                          <h6 class="mb-0 d-flex align-items-center"><a class="text-800 stretched-link" href="#!">Duplicate Menjawab</a><span id="duplicate-menjawab-percentage" class="badge rounded-pill ml-2 bg-light text-primary d-none">0%</span></h6>
                        </div>
                      </div>
                    </div>
                    <div class="col py-1">
                      <div class="row flex-end-center d-flex align-items-center g-0">
                        <div class="col-auto pr-2">
                          <div id="duplicate-menjawab-count" class="fs--1 font-weight-semi-bold">0</div>
                        </div>
                        <div class="col-5 pr-card pl-2">
                          <div class="progress mr-2" style="height: 5px;">
                            <div class="progress-bar rounded-pill" role="progressbar" style="width: 0%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer bg-light p-0"><a class="btn btn-sm btn-link btn-block py-2" href="#!">Show all status<span class="fas fa-chevron-right ml-1 fs--2"></span> </a></div>
              </div>
          	</div>
          </div>
		<!-- end this -->
		<% } %>
        <!-- /.container-fluid -->

        <%@ include file = "inc/trademark.jsp" %>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/dashboard.js"></script>
	
  </body>

</html>
