<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top">

    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%@ include file = "inc/sidebar.jsp" %>  

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">User Account</a>
            </li>
            <li class="breadcrumb-item active"><%-- ${requestScope.app} --%>SPeKtra</li>
          </ol>

          

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              User Account
              <div class="float-right">
    			<c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
				<div class="btn-group ">
				  <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Tambah
				  </button>
				  <div class="dropdown-menu">
				    <a class="dropdown-item" href="javascript:void(0)" onclick="importExcel()">IMPORT USER SATKER</a>
				    <a class="dropdown-item" href="javascript:void(0)" onclick="add()">SINGLE FORM</a>
				    <div class="dropdown-divider"></div>
				    <a class="dropdown-item" href="javascript:void(0)" onclick="add('multiform')">MULTI FORM</a>
				  </div>
				</div>
				</c:if>
    			<c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
    				<!-- <a href="?mode=multiform" class="btn btn-outline-secondary" type="button">Tambah</a> -->
				    <a class="btn btn-outline-secondary" type="button" href="javascript:void(0)" onclick="add('multiform')">Tambah</a>
    			</c:if>
              </div>
              </div>
            <div class="card-body">
            		<form id="search-form">
            			<input type="hidden" name="filter_application" value="SPJK">
            			<!-- Aktifin input di bawah untuk ngetes paging  -->
            			<!-- <input type="hidden" name="limit" value="2">  -->
            			<div class="form-group row">
			    			<div class="col-sm-6">
  							<div class="input-group">
							  <input type="text" class="form-control" name="filter_keyword" placeholder="Ketik kata kunci untuk mencari data.">
							  <div class="input-group-append">
							    <button class="btn btn-outline-secondary" type="submit">Cari</button>
							  </div>
							</div>
				  		</div>
    					<c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
				  		<div class="col-sm-3 offset-sm-3">
				    			<select name="filter_group" class="form-control"></select>
				  		</div>
				  		</c:if>
				  	</div>
				</form>
				<div class="table-responsive">
                <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    	  <th>No</th>
                      <th>Username</th>
                      <th width="20%"><spring:message code="account.kolom.label.firstname"/></th>
                      <th width="20%"><spring:message code="account.kolom.label.lastname"/></th>
                      <!-- <th width="15%">Job Role</th> -->
                      <th width="15%">Group</th>
                      <th>Status</th>
                      <th width="10%" class="d-none">Created</th>
                      <th width="10%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

        <%@ include file = "inc/trademark.jsp" %>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    
    <!-- Form Modal-->
    <div class="modal fade" id="modal-form" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        
        <form id="entry-form">
        <input type="hidden" name="application_id" value="${requestScope.app}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
       		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
     			<div class="form-row " >
			    <div class="form-group col-md-6">
			      <label>First Name</label>
			      <input type="text" class="form-control" name="first_name" autocomplete="off">
			    </div>
			    <div class="form-group col-md-6">
			      <label>Last Name</label>
			      <input type="text" class="form-control" name="last_name" autocomplete="off">
			    </div>
			  	</div>
			  
			  <div class="form-row">
		     	<div class="form-group col-md-6">
			      <label>Email (Optional)</label>
			      <input type="text" class="form-control" name="email" autocomplete="off">
			    </div>
			    
			    <div class="form-group col-md-6">
			      <label>Mobile (Optional)</label>
			      <input type="text" class="form-control" name="mobile" autocomplete="off">
			    </div>
			  </div> 
			  
		     <div class="form-row d-none">
		     	<div class="form-group col-md-6">
			      <label>Nama Unit/Organisasi</label>
			      <input type="text" class="form-control" id="unit_organisasi" autocomplete="off" readonly>
			    </div>
			    
			    <div class="form-group col-md-6">
			      <label>Jabatan/Posisi*</label>
			      <select id="position_id" class="form-control"></select>
			    </div>
			  </div>  
			  
		     <div class="form-row">
			    <div class="form-group col-md-6">
			      <label>User Group</label>
			      <select name="group_id" class="form-control"></select>
			    </div>
		     	<div id="form-unit-auditi" class="form-group col-md-6 d-none">
			      <label>Satker</label>
			      <select name="unit_auditi_id" class="form-control select select2">
			      <option value="">Pilih Satker</option>
			      </select>
			    </div>
		     	<div id="form-bdlhk" class="form-group col-md-6 d-none">
			      <label>BDLHK</label>
			      <select name="mapping_organisasi_id" class="form-control select select2">
			      <option value="">Pilih BDLHK</option>
			      </select>
			    </div>
		     	<div id="form-eselon1" class="form-group col-md-6 d-none">
			      <label>Eselon1</label>
			      <select name="eselon1_id" class="form-control select select2">
			      <option value="">Pilih Eselon1</option>
			      </select>
			    </div>
		     	<div id="form-propinsi" class="form-group col-md-6 d-none">
			      <label>Propinsi</label>
			      <select name="propinsi_id" class="form-control select select2">
			      <option value="">Pilih Propinsi</option>
			      </select>
			    </div>
			  </div>  
			  
		     <div class="form-row d-none" id="form-jabatan">
			    <div class="form-group col-md-6">
			      <label>Kelompok Jabatan</label>
			      <select name="id_kelompok_jabatan" class="form-control "></select>
			    </div>
			    <div id="form-jenjang" class="form-group col-md-6">
			      <label>Jabatan(Tingkat Jenjang)</label>
			      <select name="id_jabatan" class="form-control  select select2"></select>
			    </div>
			  </div>    
			   
       			<div class="form-row">
       				<div class="form-group col-md-6">
				      <label>User Name</label>
				      <input type="text" class="form-control" name="username" autocomplete="off">
				      <input type="date" class="form-control d-none" name="birth_date" id="username_date" autocomplete="off">
				      <select class="form-control d-none" name="male_status">
				      	<option value="">Select Gender</option>
				      	<option value="1">LAKI-LAKI</option>
				      	<option value="2">PEREMPUAN</option>
				      </select>
				    </div>
        			<div class="form-group col-sm-6">
	          			<label></label>
						<div class="form-check">
						  <input class="form-check-input" type="checkbox" value="true" name="enabled">
						  <label class="form-check-label">
						    Disabled Account
						  </label>
						</div>
			  		</div>
			  	</div>
			  	
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" type="submit">Save</button>
          </div>
          </form>
        </div>
      </div>
    </div>
    
    
    <!-- Form Modal Multiple-->
    <div class="modal fade" id="modal-form-multiple" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        
        <form id="entry-form-multiple">
        <input type="hidden" name="application_id" value="${requestScope.app}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabelMultiple">Entry Form</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
       		<div id="modal-form-multiple-msg" class="alert alert-danger" role="alert"></div>
   			
				<ul class="nav nav-tabs" id="myTab" role="tablist"></ul>
				<div class="form-row pt-3 d-none">
					<div class="form-group col-md-3">
					<select disabled id="filter_bidang_teknis" name="id_bidang_teknis" class="form-control"><option>Pilih Bidang Teknis</option></select>
					</div>
				</div>
				<div class="tab-content" id="myTabForm"></div>
				<div class="text-muted small">
					<div>*Apabila ingin melakukan <i>paste</i> pastikan dimulai dari row <strong>nomor dua.</strong></div>
				</div>
				
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <!-- <button onclick="save()" class="btn btn-primary" type="button">Save</button> -->
            <!-- real button in behinde -->
            <button id="btn-submit-form-multiple" class="btn btn-primary " type="submit">Save</button>
          </div>
          </form>
        </div>
      </div>
    </div>
    
      <!-- Form Modal-->
    <div class="modal fade" id="modal-form-table" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        <form id="entry-form-table">
        <input type="hidden" name="application_id" value="${requestScope.app}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Display Form</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
       		<div id="modal-form-table-msg" class="alert alert-danger" role="alert"></div>
			<div class="text-muted small d-none">
				<div>*Lorem <i>dolor</i> lorem dolor upsum lorem dolor upsu.</div>
			</div>
			
			<div class="form-row">
			    <div class="form-group col-md-6">
			        <label class="">Import Excel : </label>
			        <div class="input-group mb-3">
			            <input onchange="$('#btn-form-table').prop('disabled', true);" type="file" class="form-control" name="file_excel" />
			            <div class="input-group-append">
			                <button data-obj="" id="btn-create-form" onclick="importExcel(1)" class="btn btn-outline-success" type="button">Preview</button>
			                <button id="btn-form-table" class="btn btn-primary" onclick="importExcel(2)" type="button" disabled>Save</button>
			            </div>
			        </div>
			    </div>
			</div>
			
			<div class="table-responsive">
			    <table id="tbl-responden-modal-table" class="table table-sm table-bordered table-hover" width="100%" cellspacing="0">
			        <thead>
			            <tr>
							<th width="5%">No</th>		
							<th width="30%">Full Nama</th>	
							<th width="25%">Username</th>
							<th width="30%">Satker</th>
			            </tr>
			        </thead>
			        <tbody>
			        </tbody>
			    </table>
			</div>	
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <!-- <button id="btn-form-table" class="btn btn-primary" type="submit" disabled>Save</button> -->
          </div>
          </form>
        </div>
      </div>
    </div>

    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/c/account.js"></script>

  </body>

</html>
