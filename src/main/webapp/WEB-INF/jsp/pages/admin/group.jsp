<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top">

    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%@ include file = "inc/sidebar.jsp" %>  

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Group Authorities</a>
            </li>
            <li class="breadcrumb-item active">${requestScope.appname}</li>
          </ol>

          

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Group Authorities
              <div class="float-right">
              <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-outline-secondary" type="button">Add New</button>
              </div>
            </div>
            <div class="card-body">
            		<form id="search-form">
            			<input type="hidden" name="filter_application" value="${requestScope.app}">
            			<!-- Aktifin input di bawah untuk ngetes paging  -->
            			<!-- <input type="hidden" name="limit" value="1"> -->
            			<div class="form-group row">
			    			<div class="col-sm-6">
  							<div class="input-group">
							  <input type="text" class="form-control" name="filter_keyword" placeholder="Type any keyword to find spesific data">
							  <div class="input-group-append">
							    <button class="btn btn-outline-secondary" type="submit">Search</button>
							  </div>
							</div>
				  		</div>
				    		
				  	</div>
				</form>
				<div class="table-responsive">
                <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    	  <th>No</th>
                      <th width="20%">Group Name</th>
                      <th width="50%">Description</th>
                      <th>Sequence</th>
                      <th>Users</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

        <%@ include file = "inc/trademark.jsp" %>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
        <!-- Form Modal-->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content"><form id="entry-form"><input type="hidden" name="application_id" value="${requestScope.app}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          			<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
				          		
            			<div class="form-group row">
            				<div class="col-sm-9">
							<label>Group Name</label>
							<input type="text" class="form-control" name="name" autocomplete="off" placeholder="e.g. Operator or Administrator">
				  		</div>
				  		
			    			<div class="col-sm-3">
							<label>Sequence</label>
							<input type="text" class="form-control" name="sequence">
				  		</div>
				  	</div>
				  	
				  	<div class="form-group row">
            				<div class="col-sm-12">
							<label>Description</label>
							<textarea class="form-control" name="description" rows="3"></textarea>
				  		</div>
				  	</div>
          
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" type="submit">Save</button>
          </div>
          </form>
        </div>
      </div>
    </div>

    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/c/group.js"></script>

  </body>

</html>
