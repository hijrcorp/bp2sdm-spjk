<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA TRANSAKSI :: FORM EXPORT PERTANYAAN</li>
                            </ol>

                            <div class="card mb-3 d-none">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging  -->
                                        <input type="hidden" name="limit" value="1">
                                        
								        <div class="form-row">
								            <div class="form-group col-md-6">
								                <label>Pegawai</label>
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6">
								                <label>Akun Bank</label>
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-row d-none">
								            <div class="form-group col-md-6">
								                <label>Tgl.Penggajian</label>
								                <input type="date" class="form-control" name="filter_tanggal_dari" autocomplete="off">
								            </div>
								            <div class="form-group col-md-6">
								                <label>Hingga</label>
								                <input type="date" class="form-control" name="filter_tanggal_hingga" autocomplete="off">
								            </div>
								        </div>

                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="row">
                            	<div class="col-md-3">
                            		<!-- DataTables Example -->
		                            <div class="card mb-3">
		                                <div class="card-header">
		                                    <i class="fas fa-table"></i> Daftar Tab Kode
		                                    <div class="float-right">
		                                        <button id="btn-export" data-toggle="modal" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i> </button>
		                                    </div>
		                                </div>
		                                <form id="entry-form-tab">
										<div class="list-group list-group-flush" id="list-kode-tab" role="tablist">
									      <!-- <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Home</a>
									      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Profile</a>  -->
									    </div>
									    </form>
		                            </div>
                            	</div>
                            	<div class="col-md-9">
								<!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <i class="fas fa-table"></i> Daftar Preview Pertanyaan
                                    <!-- <div class="float-right">
                                        <button id="btn-download" data-toggle="modal" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i> DOWNLOAD</button>
                                    </div> -->
                                </div>
                                <div class="card-body">
                                <!-- s -->
								    <div class="tab-content" id="nav-tabContent">
							      	</div>
                                <!-- e -->
                                </div>
                            </div>
								</div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

            <!-- Form Modal
            <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
					<form id="entry-form-pertanyaan">
					    <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
					        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					            <span aria-hidden="true">�</span>
					        </button>
					    </div>
					    <div class="modal-body">
					        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" ></div>
					        <input type="text" class="form-control d-none" value="" name="id" autocomplete="off" >
												
							<div class="form-row ">
					            <div class="form-group col-md-12">
					                <label>Pilih file Excel</label>
					                <input type="file" name="file_excel" class="form-control">
					            </div>
							</div>
					
					    </div>
					    <div class="modal-footer">
					        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
					        <button id="btn-save" class="btn btn-primary" type="submit">Submit</button>
					    </div>
					</form>
                    </div>
                </div>
            </div>
			-->
            <script src="${pageContext.request.contextPath}/js/export-pertanyaan2.js"></script>

    </body>
</html>