<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA :: HASIL UJIAN</li>
                            </ol>

                            <% if(request.getParameter("filter_id")==null) {%>
                            <div class="card mb-3 ">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging 
                                        <input type="hidden" name="limit" value="2"> -->
                                        
								        <div class="form-group row">
							                <label class="col-md-2">Kode</label>
								            <div class="col-md-4">
								                <input type="text" name="filter_kode" class="form-control">
                                        	</div>
							                <label class="col-md-2">Tahun</label>
								            <div class="col-md-4">
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_tahun" class="form-control">
								                </select>
								            </div>
								        </div>
								        <!-- <div class="form-group row">
							                <label class="col-md-2">Kelompok Jabatan</label>
								            <div class="col-md-4">
								                <select name="filter_kelompok_jabatan_id" class="form-control ">
								                </select>
								            </div>
							                <label class="col-md-2">Bidang Teknis</label>
								            <div class="col-md-4">
								                <select name="filter_bidang_teknis_id" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-group row">
							                <label class="col-md-2">Periode</label>
								            <div class="col-md-4">
								                <input type="date" class="form-control" name="filter_awal" autocomplete="off">
								            </div>
							                <label class="col-md-2">Hingga</label>
								            <div class="col-md-4">
								                <input type="date" class="form-control" name="filter_akhir" autocomplete="off">
								            </div>
								        </div> -->

                                        <div class="float-right">
                                          <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <% } %>
							<div class="row">
								<div class="col-md-3">
							        <div class="card">
									  <div class="card-header">
									   <i class="fas fa-users"></i> User
	                                	<div class="float-right">
											<button id="btn-delete-pertanyaan" class="btn btn-outline-danger btn-sm" type="button">Delete</button>
										</div>
									  </div>
									  <div id="list-user" class="list-group list-group-flush">
									    <!-- <a href="#" class="list-group-item list-group-item-action">Cras justo odio</a> -->
									  </div>
									</div>
							    </div>
								<div class="col-md-9">
								
	                            <!-- DataTables Example -->
	                            <div class="card mb-3">
	                                <div class="card-header ">
	                                    <i class="fas fa-table"></i> Pertanyaan & Jawaban
	                                	<div class="float-right">
											<button id="btn-reset-pertanyaan" class="btn btn-outline-secondary" type="button">Reset</button>
										</div>
	                                </div>
	                                <div  id="list-pertanyaan" class="card-body tab-content">
	                                    
	                                </div>
	                            </div>
								</div>
							</div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Hasil Pencarian</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" style="display: none;"></div>
							       	<div id="list-modal" class="list-group list-group-flush">
									  <!-- render form js -->
									</div>
							    </div>
							    
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary d-none" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>

                <script src="${pageContext.request.contextPath}/js/hasil-ujian.js"></script>

    </body>

</html>