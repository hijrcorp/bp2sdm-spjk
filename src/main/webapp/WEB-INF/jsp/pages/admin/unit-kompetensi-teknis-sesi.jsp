<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: MAPPING TEKNIS & SESI</li>
                            </ol>
                            <div class="row">
					          	<div class="col-md-3">
					          	
					          		<div class="card mb-3">
						            <div class="card-header">Daftar Sesi</div>
						          		<div id="container-sesi" class="list-group list-group-flush">
										  <!-- <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a> -->
										</div>
									</div>
					          	</div>
                            	<div class="col-md-9">
                            		
	                            <!-- DataTables Example -->
	                            <div class="card mb-3">
	                                <div class="card-header">
	                                    <!-- <i class="fas fa-table"></i> Tabel Data
	                                    <div class="float-right">
	                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
	                                    </div> -->
	                                    <nav>
										  <div class="nav nav-tabs" id="nav-tab" role="tablist">
										    <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PDL</a> -->
								    	  </div>
										</nav>
	                                </div>
	                                <div class="card-body">
	                                	<div class="row mb-2">
	                               		<div class="col-md-6"></div>
	                                	<div class="col-md-6 text-right mb-1">
	                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
	                                    </div>
	                                    </div>
	                                    <div class="table-responsive">
	                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
	                                            <thead class="thead-dark">
	                                                <tr>
	                                                    <th>No</th>
	                                                    <!-- <th width="15%">Bidang Teknis</th>
	                                                    <th width="15%">Kelompok Teknis</th> -->
	                                                    <th width="20%">Kode</th>
	                                                    <th width="35%">Judul</th>
	                                                    <th width="35%">Fungsi Utama</th>
	                                                    <th class="text-center">Aksi</th>
	                                                </tr>
	                                            </thead>
	                                            <tbody>
	
	                                            </tbody>
	                                        </table>
	                                    </div>
	                                </div>
	                            </div>
                            	</div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

            <!-- Form Modal-->
		    <div class="modal fade" id="modal-form" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		      <div class="modal-dialog modal-xl" role="document">
		        <div class="modal-content">
		        
		        <form id="entry-form-modal">
		        <input type="hidden" name="application_id" value="${requestScope.app}">
		          <div class="modal-header">
		            <h5 class="modal-title" id="exampleModalLabel">Form</h5>
		            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
		              <span aria-hidden="true">�</span>
		            </button>
		          </div>
		          <div class="modal-body">
		       			<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		       			<!-- <div class="form-row " >
						    <div class="form-group col-md-6">
						      <label>Copy dari Sesi</label>
						      <select name="from_sesi_id" class="form-control" onchange="displayGeneralUser(1);">
						      	<option value="">Pilih Sesi Sumber</option>
						      </select>
						    </div>
						    <div class="form-group col-md-6 d-none">
						      <label>Ke Sesi</label>
						      <select id="sesi_id" name="sesi_id" class="form-control">
						      	<option value="">Pilih Sesi Tujuan</option>
						      </select>
						    </div>
						    <div class="form-group col-md-6">
						      <label>Mode Copy</label>
						      <select name="mode_copy" class="form-control">
						      	<option value="1">Manual Memilih User</option>
						      	<option value="2">Otomatis Semua User</option>
						      </select>
						    </div>
					  	</div>
					  	
		       			<hr/> -->
						
						<ul class="nav nav-tabs" id="myTabModal1" role="tablist"></ul>
		       			<!-- <form id="search-form-modal-created" class="pt-3"> -->
		          		<input type="hidden" name="filter_application" value="SPJK">
		   				<div class="form-group row">
			    			<div class="col-sm-6">
								<div class="input-group">
								  <input type="text" class="form-control" name="filter_username" placeholder="Ketik kata kunci untuk mencari data.">
								  <div class="input-group-append">
								    <button onclick="filterModalUser();" class="btn btn-outline-secondary" type="button">Cari</button>
								  </div>
								</div>
					  		</div>
					  		<div class="col-sm-3 offset-sm-3 d-none">
					    		<select name="" class="form-control">
					    			<option>Action</option>
					    			<option value="NO_LIMIT">Tampilkan Semua Baris</option>
					    		</select>
					  		</div>
					  	</div>
						<!-- </form> -->
						<div class="table-responsive">
		                <table id="tbl-data-modal1" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
		                 
                          <thead class="thead-dark">
                              <tr>
                                  <th><input type="checkbox" name="cek_all_account_modal"></th>
                                  <th>No</th>
                                  <!-- <th width="15%">Bidang Teknis</th>
                                  <th width="15%">Kelompok Teknis</th> -->
                                  <th width="20%">Kode</th>
                                  <th width="35%">Judul</th>
                                  <th width="35%">Fungsi Utama</th>
                                  <th class="text-center">Aksi</th>
                              </tr>
                          </thead>
                          <tbody>

                          </tbody>
		                </table>
		                </div>
		          </div>
		          <div class="modal-footer">
		            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
		            <button class="btn btn-primary" type="submit">Submit</button>
		          </div>
		          </form>
		        </div>
		      </div>
		    </div>
			
            <script src="${pageContext.request.contextPath}/js/unit-kompetensi-teknis-sesi.js"></script>

    </body>
</html>