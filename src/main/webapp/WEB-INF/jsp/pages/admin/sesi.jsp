<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: SESI</li>
                            </ol>

                            <div class="card mb-3 ">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging  -->
                                       <!--  <input type="hidden" name="limit" value="2"> -->
                                        
								        <div class="form-group row">
							                <label class="col-md-2">Kode</label>
								            <div class="col-md-4">
								                <input type="text" name="filter_kode" class="form-control">
                                        	</div>
							                <label class="col-md-2">Tahun</label>
								            <div class="col-md-4">
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_tahun" class="form-control">
								                </select>
								            </div>
								        </div>
								        <div class="form-group row">
							                <label class="col-md-2">Kelompok Jabatan</label>
								            <div class="col-md-4">
								                <select name="filter_kelompok_jabatan_id" class="form-control ">
								                </select>
								            </div>
							                <label class="col-md-2">Bidang Teknis</label>
								            <div class="col-md-4">
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_bidang_teknis_id" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-group row">
							                <label class="col-md-2">Periode</label>
								            <div class="col-md-4">
								                <input type="date" class="form-control" name="filter_awal" autocomplete="off">
								            </div>
							                <label class="col-md-2">Hingga</label>
								            <div class="col-md-4">
								                <input type="date" class="form-control" name="filter_akhir" autocomplete="off">
								            </div>
								        </div>

                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <i class="fas fa-table"></i> Tabel Data
                                    <div class="float-right">
                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>No</th>
                                                    <th width="10%">Kode</th>
                                                    <th width="3%">Tahun</th>
                                                    <th width="25%">Periode</th>
                                                    <th width="35%">Judul Kegiatan</th>
                                                   <th class="" width="10%">Laporan</th>
                                                   <th class="" width="17%">Tanggal Update</th>
                                                    <th class="text-center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" style="display: none;"></div>
							        <input type="text" class="form-control d-none" name="id" autocomplete="off">
							 
									<div class="form-row">
								      <div class="form-group col-md-6">
								      		<label>Kode *</label>
											<input type="text" class="form-control" name="kode" placeholder="Ketik Kode Sesi">
								      </div>
								      <div class="form-group col-md-6">
								      		<label>Tahun *</label>
											<select class="form-control" name="tahun">
												<option value="0">-- Pilih Tahun --</option>
											</select>
								      </div>
								    </div>
									
									<div class="form-row">
								      <div class="form-group col-md-12">
								      	<label>Judul *</label>
											<input type="text" class="form-control" name="keterangan" placeholder="Ketik Judul Sesi">
								      </div>
								    </div>
									
									<div class="form-row">
								      <div class="form-group col-md-12">
								      	<label>Periode * </label>
								      	<div class="input-group">
										  <input name="periode_awal" type="text" class="form-control single-date-picker" aria-label="Awal" autocomplete="off">
										  <div class="input-group-append">
										    <span class="input-group-text">Hingga</span>
										  </div>
										   <input name="periode_akhir" type="text" class="form-control single-date-picker" aria-label="Akhir" autocomplete="off">
									  	</div>
									  	
								        <div class="custom-control custom-switch">
										  <input onchange="cekInputPeriode(this)" type="checkbox" class="custom-control-input" id="customSwitch1" name="periode_aktif">
										  <label class="custom-control-label" for="customSwitch1">Switch <strong>ON</strong> to edit the periode</label>
										</div>
								      </div>
								    </div>
									
							    </div>
							    
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>

                <script src="${pageContext.request.contextPath}/js/sesi.js"></script>

    </body>

</html>