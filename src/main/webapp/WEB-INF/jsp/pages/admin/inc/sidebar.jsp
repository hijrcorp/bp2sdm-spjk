<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Sidebar -->
<ul id="myGroup" class="sidebar navbar-nav">
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_ADMIN') || fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
    	<%-- <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapseDashboard" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> DASHBOARD</a>
        </li>
        <div class="collapse" id="collapseDashboard">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/profile-jabatan-fungsi">
            <i class="fas fa-fw fa-weight"></i>
            Profile Jabatan Fungsional</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/monitoring-pelaksana-penilaian-komp">
            <i class="fas fa-fw fa-weight"></i>
            Monitoring Pelaksana Penilaian Kompetensi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/monitoring-hasil-penilaian-komp">
            <i class="fas fa-fw fa-weight"></i>
            Monitoring Hasil Penilaian Kompetensi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/pegawai-selesai-penilaian-komp">
            <i class="fas fa-fw fa-weight"></i>
            Pegawai Selesai Penilaian Kompetensi(F1)</a>
        </li>
        </div> --%>
        
    <c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER') && !fn:containsIgnoreCase(roleList, 'ROLE_SPJK_ESELON1') && !fn:containsIgnoreCase(roleList, 'ROLE_SPJK_PROPINSI') && !fn:containsIgnoreCase(roleList, 'ROLE_SPJK_BDLHK') && !fn:containsIgnoreCase(roleList, 'ROLE_SPJK_PUSDIKLAT') && !fn:containsIgnoreCase(roleList, 'ROLE_SPJK_UNIT_PEMBINA')}">
        <li class="nav-header text-muted">
            <a href="${pageContext.request.contextPath}/" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right "></i> Dashboard</a>
        </li>
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapseReferensi" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> DATA REFERENSI</a>
        </li>
        <div class="collapse small" id="collapseReferensi" data-parent="#myGroup">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kelompok-jabatan">
            <i class="fas fa-fw fa-weight"></i>
            Jabatan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/tingkat-jenjang">
            <i class="fas fa-fw fa-weight"></i>
            Tingkat</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/jenjang-jabatan">
            <i class="fas fa-fw fa-weight"></i>
            Jenjang</a>
        </li>
         <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kelompok-manajerial">
            <i class="fas fa-fw fa-weight"></i>
            Manajerial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kelompok-sosiokultural">
            <i class="fas fa-fw fa-weight"></i>
            Sosiokultural</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="${pageContext.request.contextPath}/admin/metode-pengembangan-jenis">
            <i class="fas fa-fw fa-weight"></i>
            Jenis Metode Pengembangan</a>
        </li>
        <hr/>
        <li class="nav-item" title="Mapping Bidang Teknis">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/bidang-teknis">
            <i class="fas fa-fw fa-weight"></i>
            Bidang Teknis</a>
        </li>
        <li class="nav-item" title="Mapping Kelompok Teknis">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kelompok-teknis">
            <i class="fas fa-fw fa-weight"></i>
            Kelompok Teknis</a>
        </li>
        
        <li class="nav-item">
            <a data-toggle="collapse" href="#collapsePemetaMethod" class="nav-link collapsed" aria-expanded="false"> <i class="fas fa-fw fa-weight"></i> Kelompok Nilai Metode <i class="fas fa-chevron-down"></i></a>
        </li>
        <div class="collapse" id="collapsePemetaMethod">
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/metode-pengembangan-kelompok-nilai">
	            <i class="fas fa-fw fa-weight ml-3"></i>
	            TEKNIS</a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/metode-pengembangan-kelompok-nilai?mode=NON_TEKNIS">
	            <i class="fas fa-fw fa-weight ml-3"></i>
	            NON-TEKNIS</a>
	        </li>
        </div>
        
        <li class="nav-item" title="Mapping Fungsi Utama">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/fungsi-utama">
            <i class="fas fa-fw fa-weight"></i>
            Fungsi Utama</a>
        </li>
        </div>
        
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapseOrganisasi" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> Organisasi</a>
        </li>
        <div class="collapse small" id="collapseOrganisasi" data-parent="#myGroup">
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/eselon1">
	            <i class="fas fa-fw fa-weight"></i>
	            Eselon1</a>
	        </li>
	        <li class="nav-item d-none">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kelompok-unit-auditi">
	            <i class="fas fa-fw fa-weight"></i>
	            Kelompok Satker</a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/unit-auditi">
	            <i class="fas fa-fw fa-weight"></i>
	            Satker</a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/mapping-organisasi-detil">
	            <i class="fas fa-fw fa-weight"></i>
	            Mapping Organisasi</a>
	        </li>
        </div>
        
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapsePertanyaan" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> Pertanyaan</a>
        </li>
        <div data-parent="#myGroup" class="collapse small" id="collapsePertanyaan">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/form-pertanyaan?jenis=teknis">
            <i class="fas fa-fw fa-weight"></i>
            Teknis</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/form-pertanyaan?jenis=manajerial">
            <i class="fas fa-fw fa-weight"></i>
            Manajerial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/form-pertanyaan?jenis=sosiokultural">
            <i class="fas fa-fw fa-weight"></i>
            Sosiokultural</a>
        </li>
        </div>
        
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapseMaster" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> Master</a>
        </li>
        <div data-parent="#myGroup" class="collapse small" id="collapseMaster">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/sesi">
            <i class="fas fa-fw fa-weight"></i>
            Sesi</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/account-sesi-mapping">
            <i class="fas fa-fw fa-weight"></i>
            Sesi User Mapping </a>
        </li>
        </div>
        
        
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapseImportExport" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> Import/Export</a>
        </li>
        <div data-parent="#myGroup" class="collapse small" id="collapseImportExport">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/import-pertanyaan">
            <i class="fas fa-fw fa-weight"></i>
            Import Pertanyaan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/export-pertanyaan">
            <i class="fas fa-fw fa-weight"></i>
            Export Pertanyaan</a>
        </li>
        </div>
        
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapseUnitKomp" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> UNIT KOMPETENSI</a>
        </li>
        <div data-parent="#myGroup" class="collapse small" id="collapseUnitKomp">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/unit-kompetensi-manajerial">
            <i class="fas fa-fw fa-weight"></i>
            Manajerial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/unit-kompetensi-sosiokultural">
            <i class="fas fa-fw fa-weight"></i>
            Sosiokultural</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/unit-kompetensi-teknis">
            <i class="fas fa-fw fa-weight"></i>
            Teknis</a>
        </li>
        <li class="nav-item d-none">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/mapping-unit-kompetensi-teknis">
            <i class="fas fa-fw fa-weight"></i>
            Mapping Teknis & Bidang</a>
        </li>
        <li class="nav-item d-none">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/unit-kompetensi-teknis-sesi">
            <i class="fas fa-fw fa-weight"></i>
            Mapping Teknis & Sesi</a>
        </li>
        </div>
                
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapsePemeta" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> PEMETAAN</a>
        </li>
        <div data-parent="#myGroup" class="collapse small" id="collapsePemeta">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/jabatan">
            <i class="fas fa-fw fa-weight"></i>
            Kelompok Jabatan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kompetensi-manajerial?mode=MANAJERIAL">
            <i class="fas fa-fw fa-weight"></i>
            Komp Manajerial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kompetensi-manajerial?mode=SOSIOKULTURAL">
            <i class="fas fa-fw fa-weight"></i>
            Komp Sosiokultural</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kompetensi-teknis">
            <i class="fas fa-fw fa-weight"></i>
            Kompetensi Teknis</a>
        </li>
        <hr/>
       <%--  <li class="nav-item">
            <a class="nav-link text-truncate " href="${pageContext.request.contextPath}/admin/kompetensi-metode-pengembangan">
            <i class="fas fa-fw fa-weight"></i>
            Kompetensi Metode</a>
        </li> --%>
        <%-- <li class="nav-item">
            <a class="nav-link text-truncate " href="${pageContext.request.contextPath}/admin/kompetensi-metode-pengembangan-non-teknis">
            <i class="fas fa-fw fa-weight"></i>
            Kompetensi Metode Non-Teknis</a>
        </li> --%>
        <!--  -->
        <!-- <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapsePemetaMethod" class="text-decoration-none text-muted"> Kompetensi Metode Non-Teknis <i class="fas fa-chevron-down"></i></a>
        </li> -->
        <li class="nav-item">
            <a data-toggle="collapse" href="#collapsePemetaMethod" class="nav-link collapsed" aria-expanded="false"> <i class="fas fa-fw fa-weight"></i> Kompetensi Metode <i class="fas fa-chevron-down"></i></a>
        </li>
        <div class="collapse" id="collapsePemetaMethod">
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kompetensi-metode-pengembangan">
	            <i class="fas fa-fw fa-weight ml-3"></i>
	            TEKNIS</a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kompetensi-metode-pengembangan-non-teknis">
	            <i class="fas fa-fw fa-weight ml-3"></i>
	            NON-TEKNIS</a>
	        </li>
	        <%-- <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kompetensi-metode-pengembangan-non-teknis?mode=MANAJERIAL">
	            <i class="fas fa-fw fa-weight"></i>
	            MANAJERIAL</a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="${pageContext.request.contextPath}/admin/kompetensi-metode-pengembangan-non-teknis?mode=SOSIOKULTURAL">
	            <i class="fas fa-fw fa-weight"></i>
	            SOSIOKULTURAL</a>
	        </li> --%>
        </div>
        <!--  -->
        </div>
        
        <li class="nav-header text-muted">
            <a data-toggle="collapse" href="#collapseAkun" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> Settings</a>
        </li>
        <div data-parent="#myGroup" class="collapse small" id="collapseAkun">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/account">
            <i class="fas fa-fw fa-weight"></i>
            Pengguna</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/user-pertanyaan">
            <i class="fas fa-fw fa-weight"></i>
            Reset Soal/kompetensi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/konfigurasi">
            <i class="fas fa-fw fa-weight"></i>
            Parameter</a>
        </li>
        </div>
    </c:if>
    
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
        <li class="nav-header text-muted">
            <a href="${pageContext.request.contextPath}/dashboard/laporan" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right "></i> Dashboard</a>
            <!-- <a href="javascript:void(0)" onclick="alert('Fitur belum dapat diakses.')" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right "></i> Dashboard</a> -->
        </li>
        
        
        <li class="nav-header text-muted">
            <a href="${pageContext.request.contextPath}/admin/account-sesi-mapping" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right"></i> Monitoring User </a>
        </li>
        <li class="nav-header text-muted">
            <a href="${pageContext.request.contextPath}/admin/account" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right "></i> Pengguna</a>
        </li>
    </c:if>
    
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_ESELON1')}">
        <c:redirect url="/dashboard/laporan"/>
    </c:if>
    
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_PROPINSI')}">
        <li class="nav-header text-muted">
            <a href="${pageContext.request.contextPath}/dashboard" target="_blank" class="d-none text-decoration-none text-muted"> <i class="fas fa-chevron-right "></i> Dashboard</a>
            <a href="javascript:void(0)" onclick="alert('Mohon maaf, dashboard anda belum dapat akses.')" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right "></i> Dashboard</a>
        </li>
        
        <li class="nav-header text-muted">
            <a href="${pageContext.request.contextPath}/admin/account" class="text-decoration-none text-muted"> <i class="fas fa-chevron-right "></i> Pengguna</a>
        </li>
    </c:if>
    
    <!-- BDLHK -->    
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_BDLHK')}">
        <c:redirect url="/dashboard/laporan"/>
    </c:if>
    
    <!-- PUSDIK -->    
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_PUSDIKLAT')}">
        <c:redirect url="/dashboard/laporan"/>
    </c:if>
    
    
    <!-- PUSDIK -->    
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_UNIT_PEMBINA')}">
       <c:redirect url="/dashboard/laporan"/>
    </c:if>
    
    <!-- end -->
    
    </c:if>
</ul>