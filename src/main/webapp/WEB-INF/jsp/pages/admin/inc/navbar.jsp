<nav class="navbar navbar-expand navbar-dark bg-success static-top">

      <a class="navbar-brand mr-1" href="${pageContext.request.contextPath}/page/">Beranda</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto">
        
        <li class="nav-item dropdown arrow-down">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <img src="${requestScope.avatar}" width="25px" /> ${requestScope.realName}  
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          	<a class="dropdown-item" href="${pageContext.request.contextPath}/login-password" target="_blank">Change Password</a>
            <a class="dropdown-item" href="${pageContext.request.contextPath}/login-logout">Logout</a>
          </div>
        </li>
      </ul>

    </nav>