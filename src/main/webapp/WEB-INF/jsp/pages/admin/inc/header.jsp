  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

	<title>Sistem Pemetaan Jabatan Kompetensi - SPeKtra</title>

    <!-- Bootstrap core CSS-->
   <%--  <link href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --%>
   	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Custom fonts for this template-->
    <link href="${contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="${contextPath}/vendor/select2.min.css" rel="stylesheet">
	<link href="${contextPath}/vendor/daterangepicker.css" rel="stylesheet"> 
	<link href="${contextPath}/vendor/ekko-lightbox.css" rel="stylesheet"> 
    <%-- <link href="${pageContext.request.contextPath}/vendor/bootstrap-year-calendar-bs4-master/css/bootstrap-year-calendar.css" rel="stylesheet"> --%>
	
	<!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/css/sb-admin.css" rel="stylesheet">
	
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<script>var localeCookieName = "${localeCookieName}"</script>
	<script>var roleList = "${roleList}"
		roleList = roleList.substring(1, roleList.length-1);
	</script>
	
	<!-- Favicon and touch icons -->
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	
	<style>
		.colored-toast.swal2-icon-success {
		  background-color: #a5dc86 !important;
		}
		 
		.colored-toast.swal2-icon-error {
		  background-color: #f27474 !important;
		}
		 
		.colored-toast.swal2-icon-warning {
		  background-color: #f8bb86 !important;
		}
		 
		.colored-toast.swal2-icon-info {
		  background-color: #3fc3ee !important;
		}
		 
		.colored-toast.swal2-icon-question {
		  background-color: #87adbd !important;
		}
		 
		.colored-toast .swal2-title {
		  color: white;
		}
		 
		.colored-toast .swal2-close {
		  color: white;
		}
		 
		.colored-toast .swal2-html-container {
		  color: white;
		}
	</style>
  </head>
