<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Confirmation Modal-->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-confirm-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
            <button class="btn btn-primary" id="btn-modal-confirm-yes" type="button">Yes</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Alert Modal-->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-success">
            <h5 class="modal-title" id="exampleModalLabel">Alert</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-alert-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Error Modal-->
    <div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Error</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-error-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="${contextPath}/vendor/jquery/jquery.min.js"></script>
    <script src="${contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="${contextPath}/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="${contextPath}/vendor/jquery.disableAutoFill.min.js"></script>
    <script src="${contextPath}/vendor/js.cookie.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="${contextPath}/vendor/chart.js/Chart.min.js"></script>
    <script src="${contextPath}/vendor/moment.min.js"></script>
    <script src="${contextPath}/vendor/daterangepicker.js"></script>
	<script src="${contextPath}/vendor/select2.min.js"></script>
	<script src="${contextPath}/vendor/ekko-lightbox.min.js"></script>
	<%-- <script src="${contextPath}/vendor/loadingoverlay.min.js"></script> --%>
	<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
	<script src="${contextPath}/vendor/bootstrap-year-calendar-bs4-master/js/bootstrap-year-calendar.js"></script>
	<!-- Tiny Mce -->
	<script src="${contextPath}/vendor/tinymce.min.js" referrerpolicy="origin"></script>
	<script src="${contextPath}/vendor/stickytable/jquery.stickytable.js"></script>
	
	<!-- Sweetalert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	
	<!-- Custom scripts for all pages-->
    <script src="${pageContext.request.contextPath}/js/sb-admin.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/common.js"></script>
	<script>
		$(document).ready(function(){
			if($('#myGroup').find("div li.active")[0]!=undefined){
				console.log($('#myGroup').find("div li.active")[0].parentElement.id);
				$('#'+$('#myGroup').find('li.active').parent()[0].id).addClass('show');
			}
		});
	</script>
	
    <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_BDLHK')}">
	<style>
		/* #content-wrapper a{
			display:none!important;
		} */
		/* #content-wrapper button{
			display:none!important;
		} */
	</style>
	</c:if>