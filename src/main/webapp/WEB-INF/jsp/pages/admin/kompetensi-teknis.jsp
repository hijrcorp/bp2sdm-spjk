<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: PEMETAAN KOMPETENSI TEKNIS</li>
                            </ol>

                            <div class="card mb-3 d-none">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging  -->
                                        <input type="hidden" name="limit" value="2">
                                        
								        <div class="form-row">
								            <div class="form-group col-md-6">
								                <label>Pegawai</label>
								                <select name="filter_id_pegawai" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6">
								                <label>Akun Bank</label>
								                <!-- <input type="text" name="filter_id_akun_bank" class="form-control" placeholder="Ketik Nama/No.Rek Bank"> -->
								                <select name="filter_id_akun_bank" class="form-control">
								                </select>
								            </div>
								        </div>
								
								        <div class="form-row d-none">
								            <div class="form-group col-md-6">
								                <label>Tgl.Penggajian</label>
								                <input type="date" class="form-control" name="filter_tanggal_dari" autocomplete="off">
								            </div>
								            <div class="form-group col-md-6">
								                <label>Hingga</label>
								                <input type="date" class="form-control" name="filter_tanggal_hingga" autocomplete="off">
								            </div>
								        </div>

                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <!-- <i class="fas fa-table"></i> Tabel Data
                                    <div class="float-right">
                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                    </div> -->
                                    <nav>
									  <div class="nav nav-tabs" id="nav-tab" role="tablist">
									    <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PDL</a> -->
							    	  </div>
									</nav>
                                </div>
                                <div class="card-body">
                                	<div class="row mb-2">
                               		<div class="col-md-3">
								        <select class="form-control" name="filter_id_bidang_teknis">
								            <option value="">-- Pilih --</option>
								        </select>
								    </div>
                               		<div class="col-md-3">
								        <!-- <select class="form-control" name="filter_id_master_jabatan">
								            <option value="">-- Pilih --</option>
								        </select> -->
								    </div>
									    
                                	<div class="col-md-6 text-right mb-1">
                                        <button  id="btn-adds" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                    </div></div>
                                    <div class="table-responsive" style="height: auto; overflow-y: scroll;">
                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" cellspacing="0" >
                                            <thead id="head" class="thead-dark small">
                                                <tr>
                                                    <th width="5%" rowspan="3">No</th>
                                                    <th rowspan="3">Jabatan</th>
                                                    <th class="text-center" colspan="2" width="15%">Kompetensi Inti</th>
                                                    <th class="text-center" colspan="4">Kompetensi Pilihan</th>
                                                </tr>
                                                <tr >
                                               		<th rowspan="2" width="5%">No</th>
                                               		<th rowspan="2" width="15%">Kode Unit</th>
                                                    <th class="text-center" colspan="2" width="15%">Inventarisasi</th>
                                                    <th class="text-center" colspan="2" width="15%">Pemolaan</th>
                                                </tr>
                                                <tr>
                                                		<th width="5%">No</th>
                                                		<th width="15%">Kode Unit</th>
                                                		<th width="5%">No</th>
                                                		<th width="15%">Kode Unit</th>
                                                </tr>
                                            </thead>
                                            <tbody class="small">
												<!-- <tr>
                                                		<td>1</td>
                                                		<td>PEH KETERAMPILAN PEMULA</td>
                                                		<td>1<br>2</td>
                                                		<td>KHT.PEH.001.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.255.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a></td>
                                                		<td>1<br>2<br>3</td>
                                                		<td>KHT.PEH.001.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.008.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.255.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a></td>
                                                		<td>1<br>2</td>
                                                		<td>KHT.PEH.001.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.255.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a></td>
                                                </tr>
                                                <tr>
                                                		<td>2</td>
                                                		<td>PEH KETERAMPILAN TERAMPIL</td>
                                                		<td>1<br>2</td>
                                                		<td>KHT.PEH.001.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.255.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a></td>
                                                		<td>1<br>2<br>3</td>
                                                		<td>KHT.PEH.001.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.008.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.255.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a></td>
                                                		<td>1<br>2</td>
                                                		<td>KHT.PEH.001.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><br>KHT.PEH.255.01 <a href="#"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a></td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                    <div>
										<div class="col-md-6  mb-1">
	                                        <button  id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
	                                    </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert"></div>
							        <input type="text" class="form-control d-none" name="id" autocomplete="off">
								
							        <div class="form-row d-none">
									    <div class="form-group col-md-12">
									        <label>Kelompok Jabatan</label>
									        <select class="form-control" name="id_kelompok_jabatan">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Jabatan</label>
									        <select class="form-control select2" name="id_master_jabatan">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
									
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Bidang Teknis</label>
									        <select class="form-control d-none" name="id_bidang_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									        <input class="form-control" id="id_bidang_teknis" readonly>
									    </div>
									</div>
									
							        <div class="form-row">
								    <div class="form-group col-md-12">
								        <label>Jenis Kompetensi</label><br/>
								        <div class="custom-control custom-radio custom-control-inline">
										  <input value="INTI" type="radio" id="jenis_kompetensi1" name="jenis_kompetensi" class="custom-control-input">
										  <label class="custom-control-label" for="jenis_kompetensi1">INTI</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
										  <input value="PILIHAN" type="radio" id="jenis_kompetensi2" name="jenis_kompetensi" class="custom-control-input">
										  <label class="custom-control-label" for="jenis_kompetensi2">PILIHAN</label>
										</div>
								    </div>
									</div>
								    
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Kelompok Teknis</label>
									        <select class="form-control " name="id_kelompok_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
									
							        <div class="form-row">
									    <div class="form-group col-md-12">
									        <label>Unit Kompetensi Teknis</label>
									        <select class="form-control select2" multiple="multiple" id="id_unit_kompetensi_teknis" name="id_unit_kompetensi_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
							
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>

                <script src="${pageContext.request.contextPath}/js/kompetensi-teknis.js"></script>

    </body>
</html>