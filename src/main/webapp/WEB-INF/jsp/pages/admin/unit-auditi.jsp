<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: SATKER</li>
                            </ol>

                            <div class="card mb-3">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <!-- <input type="hidden" name="filter_application" value="pps"> -->
                                        <!-- Aktifin input di bawah untuk ngetes paging  -->
                                        <input type="hidden" name="limit" value="2">
                                        
								        <div class="form-row">
								            <div class="form-group col-md-6">
								                <label>Kode Satker</label>
								                <input type="text" name="filter_kode" class="form-control" placeholder="Ketik Kode/Nama Satker">
								            </div>
								            <div class="form-group col-md-6">
								                <label>Nama Satker</label>
								                <input type="text" name="filter_nama" class="form-control" placeholder="Ketik Kode/Nama Satker">
								            </div>
								        </div>
								        
								        <div class="form-row ">
								            <div class="form-group col-md-6">
								                <label>Eselon1</label>
								                <select name="filter_id_eselon1" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6 ">
								                <label>Status Auditi</label>
								                <select name="filter_id_status_auditi" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6 d-none">
								                <label>Kode Simpeg</label>
								                <input type="text" class="form-control" name="filter_kode_simpeg" autocomplete="off">
								            </div>
								        </div>
								
								        <div class="form-row ">
								            <div class="form-group col-md-6">
								                <label>Propinsi</label>
								                <select name="filter_id_propinsi" class="form-control select select2">
								                </select>
								            </div>
								            <div class="form-group col-md-6">
								                <label>Kabupaten/Kota</label>
								                <select name="filter_id_distrik" class="form-control select select2">
								                </select>
								            </div>
								        </div>

                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <!-- <i class="fas fa-fw fa-search"></i> -->Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <i class="fas fa-table"></i> Tabel Data
                                    <div class="float-right ">
                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                        <button onclick="importExcel()" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Import Excel</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>No</th>
                                                    <th width="5%">Kode</th>
                                                    <th width="30%">Nama Satker</th>
                                                    <th width="20%">Propinsi/Kabupaten</th>
                                                    <th width="25%">Eselon1</th>
                                                    <th width="10%">Status Unit</th>
                                                    <th width="10%" class="text-center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

               <!-- Form Modal-->
               <div class="modal fade" id="modal-form"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                   <div class="modal-dialog" role="document">
                       <div class="modal-content">
						<form id="entry-form">
						    <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
						        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
						            <span aria-hidden="true">�</span>
						        </button>
						    </div>
						    <div class="modal-body">
						        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert"></div>
						        <input type="text" class="form-control d-none" name="id" autocomplete="off">
						
						        <div class="form-row">
						            <div class="form-group col-md-12">
						                <label>Kode(Opsional)</label>
						                <input type="text" name="kode" class="form-control" placeholder="Ketik kode Satker">
						            </div>
						        </div>
						        <div class="form-row">
						            <div class="form-group col-md-12">
						                <label>Nama Satker</label>
						                <input type="text" name="nama" class="form-control" placeholder="Ketik nama Satker">
						            </div>
						        </div>
						        <div class="form-row">
						            <div class="form-group col-md-12">
						                <label>Eselon1</label>
						                <select name="id_eselon1" class="form-control select select2">
						                </select>
						            </div>
						        </div>
						        <div class="form-row">
						            <div class="form-group col-md-12">
						                <label>Propinsi</label>
						                <select name="id_propinsi" class="form-control select select2">
						                </select>
						            </div>
						        </div>
						        <div class="form-row">
						            <div class="form-group col-md-12">
						                <label>Kabupaten/Kota</label>
						                <select data-id="" name="id_distrik" class="form-control select select2">
						                </select>
						            </div>
						        </div>
						        <div class="form-row">
						            <div class="form-group col-md-12">
						                <label>Status Auditi</label>
						                <select name="id_status_auditi" class="form-control select select2">
						                </select>
						            </div>
						        </div>
						        <div class="form-row">
						            <div class="form-group col-md-12">
						                <label>Kode Simpeg(Opsional)</label>
						                <select name="kode_simpeg" class="form-control select select2">
						                </select>
						            </div>
						        </div>
						
						    </div>
						    <div class="modal-footer">
						        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
						        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
						    </div>
						</form>
                       </div>
                   </div>
               </div>
               
		      	<!-- Form Modal-->
			    <div class="modal fade" id="modal-form-table" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			      <div class="modal-dialog modal-xl" role="document">
			        <div class="modal-content">
			        <form id="entry-form-table">
			        <input type="hidden" name="application_id" value="${requestScope.app}">
			          <div class="modal-header">
			            <h5 class="modal-title" id="exampleModalLabel">Display Form</h5>
			            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			              <span aria-hidden="true">�</span>
			            </button>
			          </div>
			          <div class="modal-body">
			       		<div id="modal-form-table-msg" class="alert alert-danger" role="alert"></div>
						<div class="text-muted small d-none">
							<div>*Lorem <i>dolor</i> lorem dolor upsum lorem dolor upsu.</div>
						</div>
						
						<div class="form-row">
						    <div class="form-group col-md-6">
						        <label class="">Import Excel : </label>
						        <div class="input-group mb-3">
						            <input onchange="$('#btn-form-table').prop('disabled', true);" type="file" class="form-control" name="file_excel" />
						            <div class="input-group-append">
						                <button data-obj="" id="btn-create-form" onclick="importExcel(1)" class="btn btn-outline-success" type="button">Preview</button>
						                <button id="btn-form-table" class="btn btn-primary" onclick="importExcel(2)" type="button" disabled>Save</button>
						            </div>
						        </div>
						    </div>
						</div>
						
						<div class="table-responsive">
						    <table id="tbl-responden-modal-table" class="table table-sm table-bordered table-hover" width="100%" cellspacing="0">
						        <thead>
						            <tr>
										<th width="3%">No</th>			
										<th width="25%">Nama Satker</th>
										<th width="10%">Propinsi</th>
										<th width="15%">Kota/Kabupaten</th>
										<th width="15%">Eselon</th>
										<th width="3%">Status Unit</th>
										<th width="5%">Kode Simpeg</th>
						            </tr>
						        </thead>
						        <tbody>
						        </tbody>
						    </table>
						</div>	
			          </div>
			          <div class="modal-footer">
			            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
			            <!-- <button id="btn-form-table" class="btn btn-primary" type="submit" disabled>Save</button> -->
			          </div>
			          </form>
			        </div>
			      </div>
			    </div>

               <script src="${pageContext.request.contextPath}/js/unit-auditi.js"></script>

    </body>

</html>