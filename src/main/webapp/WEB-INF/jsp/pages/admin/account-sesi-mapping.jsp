<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top">

    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%@ include file = "inc/sidebar.jsp" %>  

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Master</a>
            </li>
            <li class="breadcrumb-item active"><%-- ${requestScope.app} --%>Mapping Sesi Account </li>
          </ol>

          <div class="row">
          	<div class="col-md-3">
          	
          		<div class="card mb-3">
	            <div class="card-header">Daftar Sesi</div>
	          		<div id="container-sesi" class="list-group list-group-flush">
					  <!-- <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a> -->
					</div>
				</div>
          	</div>
          	<div class="col-md-9">
          		<div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Mapping Sesi Account 
			  <c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
              <div class="float-right">
    			<button type="button" class="btn btn-outline-primary" onclick="add('CREATED')">
				    Tambah Account
				</button>
    			<button type="button" class="btn btn-outline-secondary d-none" onclick="add('ADDEDD')">
				    Copy Account
				</button>
              </div>
              </c:if>
              </div>
            <div class="card-body">
            		
				<ul class="nav nav-tabs" id="myTab" role="tablist"></ul>
				<div class="tab-content" id="myTabForm"></div>
				
            		<form id="search-form" class="pt-3">
            			<input type="hidden" name="filter_application" value="SPJK">
            			<!-- Aktifin input di bawah untuk ngetes paging  -->
            			<!-- <input type="hidden" name="limit" value="2">  -->
            			<div class="form-group row justify-content-between">
			    			<div class="col-sm-7">
  							<div class="input-group">
  							 <div class="input-group-prepend">
							    <div class="btn-group">
								  <button id="btn-label" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    Username
								  </button>
								  <div class="dropdown-menu">
								    <a class="dropdown-item filter-by-kriteria" data-column="username_account" href="javascript:void(0)">Username</a>
								    <a class="dropdown-item filter-by-kriteria" data-column="full_name_account" href="javascript:void(0)">Nama Lengkap</a>
								    <a class="dropdown-item filter-by-kriteria" data-column="nama_unit_auditi" href="javascript:void(0)">Nama Satker</a>
								    <a class="dropdown-item filter-by-kriteria" data-column="nama_master_jabatan" href="javascript:void(0)">Nama Jabatan</a>
								    <a class="dropdown-item filter-by-kriteria" data-column="status" href="javascript:void(0)">Status</a>
								    <div class="dropdown-divider"></div>
								    <a class="dropdown-item filter-by-kriteria" href="javascript:void(0)">Reset</a>
								  </div>
								</div>
							  </div>
							  <input type="text" class="form-control" data-column="username_account" name="filter_keyword" placeholder="Ketik kata kunci untuk mencari data.">
							  <div class="input-group-append">
							    <button class="btn btn-outline-secondary" type="submit">Cari</button>
							  </div>
							</div>
				  		</div>
				  		<div class="col-sm-3 ">
  						<c:if test="${!fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
				    		<select id="filter_action" class="form-control">
				    			<option value="0">Action</option>
			    				<option value="deleteall">Delete record yang dipilih</option>
				    		</select>
				  		</c:if>
				  		<div class="text-muted">Total Responden: <span id="total-responden">-</span></div>
				  		</div>
				  	</div>
				</form>
				<div class="table-responsive">
                <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                  <!-- <thead>
                    <tr>
                      <th class="_not_satker"><input type="checkbox" name="cek_all_account"></th>
                      <th></th>
                      <th><input type="text" class="form-control" data-column="username_account" name="" placeholder="Username"></th>
                      <th><input type="text" class="form-control" data-column="fullname_account" name="" placeholder="Nama Lengkap"></th>
                      <th><input type="text" class="form-control" data-column="satker_jabatan_account" name="" placeholder="Satker/Jabatan"></th>
                      <th class="_not_satker"></th>
                    </tr>
                  </thead> -->
                  <thead>
                    <tr>
                      <th class="_not_satker"><input type="checkbox" name="cek_all_account"></th>
                      <th>No</th>
                      <th>Username</th>
                      <th class="d-none">Nama Lengkap</th>
                      <th>Satker/Jabatan</th>
                      <th class="_not_satker">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                </div>
            </div>
          </div>
          	</div>
          </div>

        </div>
        <!-- /.container-fluid -->

        <%@ include file = "inc/trademark.jsp" %>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
      <!-- Form Modal-->
    <div class="modal fade" id="modal-form" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        
        <form id="entry-form">
        <input type="hidden" name="application_id" value="${requestScope.app}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
       		<div id="modal-form-msg2" class="alert alert-danger" role="alert"></div>
   				<div class="form-row " >
				    <div class="form-group col-md-6">
				      <label>Copy dari Sesi</label>
				      <select name="sesi_id" class="form-control">
				      	<option value="">Pilih Sesi Sumber</option>
				      </select>
				    </div>
				    <div class="form-group col-md-6">
				      <label>Ke Sesi</label>
				      <select name="sesi_id" class="form-control">
				      	<option value="">Pilih Sesi Tujuan</option>
				      </select>
				    </div>
			  	</div>
			  	
   				<div class="form-row " >
				    <!-- <div class="form-group col-md-6">
				      <label>Jabatan</label>
				      <select name="jabatan_id" class="form-control">
				      	<option value="">Pilih Jabatan</option>
				      </select>
				    </div> -->
				    <div class="form-group col-md-6">
				      <label>Kelompok Jabatan</label>
				      <select name="sesi_id" class="form-control">
				      	<option value="">Pilih Kelompok Jabatan</option>
				      </select>
				    </div>
			  	</div>
				   
       			<hr/>
				<ul class="nav nav-tabs" id="myTabModal" role="tablist"></ul>
				<div class="tab-content" id="myTabFormModal"></div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" type="submit">Submit</button>
          </div>
          </form>
        </div>
      </div>
    </div>
    
      <!-- Form Modal User-->
    <div class="modal fade" id="modal-form-create-user" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        
        <form id="entry-form-modal">
        <input type="hidden" name="application_id" value="${requestScope.app}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
       			<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
       			<div class="form-row " >
				    <div class="form-group col-md-6">
				      <label>Copy dari Sesi</label>
				      <select name="from_sesi_id" class="form-control" onchange="displayGeneralUser(1);">
				      	<option value="">Pilih Sesi Sumber</option>
				      </select>
				    </div>
				    <div class="form-group col-md-6 d-none">
				      <label>Ke Sesi</label>
				      <select id="sesi_id" name="sesi_id" class="form-control">
				      	<option value="">Pilih Sesi Tujuan</option>
				      </select>
				    </div>
				    <div class="form-group col-md-6">
				      <label>Mode Copy</label>
				      <select name="mode_copy" class="form-control">
				      	<option value="1">Manual Memilih User</option>
				      	<option value="2">Otomatis Semua User</option>
				      </select>
				    </div>
			  	</div>
			  	
       			<hr/>
				
				<ul class="nav nav-tabs" id="myTabModal1" role="tablist"></ul>
       			<!-- <form id="search-form-modal-created" class="pt-3"> -->
          		<input type="hidden" name="filter_application" value="SPJK">
   				<div class="form-group row">
	    			<div class="col-sm-6">
						<div class="input-group">
						  <input type="text" class="form-control" name="filter_username" placeholder="Ketik kata kunci untuk mencari data.">
						  <div class="input-group-append">
						    <button onclick="filterModalUser();" class="btn btn-outline-secondary" type="button">Cari</button>
						  </div>
						</div>
			  		</div>
			  		<div class="col-sm-3 offset-sm-3 d-none">
			    		<select name="" class="form-control">
			    			<option>Action</option>
			    			<option value="NO_LIMIT">Tampilkan Semua Baris</option>
			    		</select>
			  		</div>
			  	</div>
				<!-- </form> -->
				<div class="table-responsive">
                <table id="tbl-data-modal1" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th><input type="checkbox" name="cek_all_account_modal"></th>
                      <th>No</th>
                      <th >NIP</th>
                      <th >Nama Pegawai</th>
                      <th>Satker/Jabatan</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" type="submit">Submit</button>
          </div>
          </form>
        </div>
      </div>
    </div>

    <%@ include file = "inc/footer.jsp" %>
    
  	<c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_SPJK_SATKER')}">
    	<script>
    		function removeHtmlData(){
    			$('._not_satker').remove();
    		}

    		/*function countJabatan(){
    			if(params!="") {
    				var param=","+params;
    				params="filter=id_sesi.eq('"+selected_sesi+"')";
    				params+=param;
    			}else{
    				params+="filter=id_sesi.eq('"+selected_sesi+"')";
    				params+=",id_kelompok_jabatan.eq('"+1+"')";
    			}	
    			if($('[name=filter_keyword]').val()!='') {
    				var kolom=$('[name=filter_keyword]').data('column');
    				var operator = "eq";
    				if(kolom!="username_account") operator="like";
    				params+=","+$('[name=filter_keyword]').data('column')+"."+operator+"('"+$('[name=filter_keyword]').val()+"')";
    			}
    			setTimeout(() => {
    				ajaxGET(pathsql + '/view_mapping_jabatan_unit_auditi?page='+page+'&'+params+'&limit=10000&join=account,unit_auditi','onGetListSuccess','onGetListError');
    			}, 1000);
        	}*/
    	</script>
    </c:if>
    <script src="${pageContext.request.contextPath}/js/c/account-sesi-mapping.js"></script>
	
  </body>

</html>
