<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA MASTER :: FUNGSI UTAMA</li>
                            </ol>

                            <!-- DataTables Example -->
                            <div class="card mb-3">
                                <div class="card-header">
                                    <!-- <i class="fas fa-table"></i> Tabel Data
                                    <div class="float-right">
                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                    </div> -->
                                    <nav>
									  <div class="nav nav-tabs" id="nav-tab" role="tablist">
									    <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PDL</a> -->
							    	  </div>
									</nav>
                                </div>
                                <div class="card-body">
                                	<div class="text-right mb-1">
                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>No</th>
                                                    <th width="15%">Kode</th>
                                                    <th width="65%">Judul</th>
                                                    <th class="text-center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert"></div>
							        <input type="text" class="form-control d-none" name="id" autocomplete="off">
							
									<div class="form-row d-none">
									    <div class="form-group col-md-12">
									        <label>Kelompok Jabatan</label>
									        <select class="form-control" name="id_kelompok_jabatan">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									</div>
									
							        <div class="form-row">
							            <div class="form-group col-md-12">
							                <label>Kode</label>
							                <input type="text" name="kode" class="form-control">
							            </div>
							        </div>
							        <div class="form-row">
							            <div class="form-group col-md-12">
							                <label>Judul</label>
							                <input type="text" name="judul" class="form-control">
							            </div>
							        </div>
							
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>

               <script src="${pageContext.request.contextPath}/js/fungsi-utama.js"></script>

    </body>
</html>