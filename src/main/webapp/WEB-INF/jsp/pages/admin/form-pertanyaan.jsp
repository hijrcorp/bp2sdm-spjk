<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
		.popover{
		    /* max-width:1000px !important; */
		}
	</style>
    <body id="page-top">

        <%@ include file = "inc/navbar.jsp" %>

            <div id="wrapper">

                <%@ include file = "inc/sidebar.jsp" %>

                    <div id="content-wrapper">

                        <div class="container-fluid">

                            <!-- Breadcrumbs-->
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">DATA TRANSAKSI :: FORM PERTANYAAN</li>
                            </ol>
							<% if(!request.getParameter("jenis").equals("teknis")) { %>
                            <div class="card mb-3 ">
                                <div class="card-header">
                                    Parameter Pencarian
                                </div>
                                <div class="card-body">
                                    <form id="search-form" autocomplete="off">
                                        <input type="hidden" name="limit" value="1">
							            <div class="form-group row">
							                <label class="col-md-2">Unit Kompetensi</label>
							                <div class="col-md-10">
							                <select name="filter_id_unit_kompetensi" class="form-control select select2">
							                </select>
							                </div>
							            </div>
							
                                        <div class="float-right">
                                            <button id="btn-reset" class="btn btn-danger" type="button">
                                                <i class="fas fa-fw fa-search"></i>Reset</button>
                                            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
                                        </div>
                                    </form>
                                </div>
                            </div> 
							<% } %>
                            <div class="row">
                            	<div class="col-md-12">
	                            <div class="card mb-3">
	                                <div class="card-header">
	                                   <% if(!request.getParameter("jenis").equals("teknis")) { %>
	                                   <i class="fas fa-table"></i> Tabel Data
	                                    <div class="float-right">
	                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
	                                        <button id="btn-update" data-toggle="modal" data-target="#modal-form-update" class="btn btn-info" type="button"><i class="fas fa-clock"></i> Update Durasi</button>
	                                        <button id="btn-deleteall" data-toggle="modal" data-target="#modal-form-delete" class="btn btn-danger" type="button"><i class="fas fa-trash"></i> Hapus Soal</button>
	                                    </div>
	                                   <% }else{ %>
	                                    <nav>
										  <div class="nav nav-tabs" id="nav-tab" role="tablist">
										    <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PDL</a> -->
								    	  </div>
										</nav>
										<% } %>
	                                </div>
	                                
	                                <div class="card-body">
	                                	<% if(request.getParameter("jenis").equals("teknis")) { %>
	                                	<div class="row mb-2">
	                               		<div class="col-md-3">
									        <select class="form-control" name="filter_id_bidang_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
	                               		<div class="col-md-3">
								            <!-- <div class="form-group row">
								                <div class="col-md-12">
								                <input type="text" class="form-control" name="filter_keyword" placeholder="Ketik kata kunci data yang ingin dicari" autocomplete="off">
								                </div>
								            </div> -->
	                               		</div>
	                                	<div class="col-md-6 text-right mb-1">
	                                    <div class="float-right">
	                                        <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-dark" type="button"><i class="fas fa-fw fa-plus"></i> Tambah</button>
	                                        <button id="btn-update" data-toggle="modal" data-target="#modal-form-update" class="btn btn-info" type="button"><i class="fas fa-clock"></i> Update Durasi</button>
	                                        <button id="btn-deleteall" data-toggle="modal" data-target="#modal-form-delete" class="btn btn-danger" type="button"><i class="fas fa-trash"></i> Hapus Soal</button>
	                                    </div>
	                                    </div>
	                                    </div>
	                                    <% } %>
	                                    <div class="table-responsive">
	                                        <table id="tbl-data" class="table table-sm table-bordered table-striped table-hover" width="100%" cellspacing="0">
	                                            <thead class="thead-dark">
	                                                <tr>
	                                                    <th width="1%"><input type="checkbox" name="cek_all_pertanyaan"></th>
	                                                    <th width="1%">No</th>
	                                                    <th width="10%">Kode</th>
	                                                    <th width="67%">Isi</th>
	                                                    <th width="6%">Bobot/Level</th>
														<% if(request.getParameter("jenis").equals("teknis")) { %>
	                                                    <th width="3%">Kunci Jawaban</th>
	                                                    <th width="3%">Tipe Soal</th>
	                                                    <% } %>
	                                                    <th width="3" class="text-center">Aksi</th>
	                                                </tr>
	                                            </thead>
	                                            <tbody>
	
	                                            </tbody>
	                                        </table>
	                                    </div>
	                                </div>
	                            </div>
								</div>
                            </div>

                        </div>
                        <!-- /.container-fluid -->

                        <%@ include file = "inc/trademark.jsp" %>

                    </div>
                    <!-- /.content-wrapper -->

            </div>
            <!-- /#wrapper -->

            <%@ include file = "inc/footer.jsp" %>

                <!-- Form Modal-->
                <div class="modal fade" id="modal-form" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
							<form id="entry-form">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" ></div>
							        <input type="text" class="form-control d-none" name="id" autocomplete="off" disabled>
									
									<div class="form-row ">
									    <div class="form-group col">
									        <label>Unit Kompetensi </label>
									        <select id="id_unit_kompetensi" class="form-control select select2" name="id_unit_kompetensi">
									        </select>
									    </div>
									    <% if(request.getParameter("jenis").equals("teknis")) { %>
									    <div class="form-group col-md-6">
									        <label>Bidang Teknis </label>
									        <select class="form-control" name="id_bidang_teknis">
									            <option value="">-- Pilih --</option>
									        </select>
									    </div>
									     <% } %>
									       
									</div>
									
									<div class="form-row ">
							            <div class="form-group col-md-6">
							                <label>Kode Pertanyaan</label>
							                <input type="text" name="kode" class="form-control" placeholder="Opsional value">
							            </div>
									    <div class="form-group col-md-6 d-none">
									        <label>
									        <% if(request.getParameter("jenis").equals("teknis")) {out.print("Bobot Pertanyaan");}else out.print("Level Pertanyaan"); %> *</label>
									        <input type="number" class="form-control" name="bobot" placeholder="Tentukan Bobot/Level Pertanyaan" value="1">
									    </div>
							            <div class="form-group col-md-6">
							                <label>Tipe Soal</label>
									        <select class="form-control" name="tipe_soal">
									        	<option value="">Pilih</option>
									        	<option value="MUDAH">Mudah</option>
									        	<option value="SEDANG">Sedang</option>
									        	<option value="SUKAR">Sukar</option>
									        </select>
							            </div>
									</div>
									
									<div class="form-row ">
									    <div class="form-group col-md-12">
									        <% if(request.getParameter("jenis").equals("teknis")) {out.print("<label>Isi Pertanyaan *</label>");}else out.print("<label>Isi Pernyataan *</label>"); %>
									        <textarea class="form-control" name="isi" rows="3"></textarea>
									        <div>
											    <a target="_blank" href="#" class="float-right pull-right text-truncate col-6" id="file-name">tidak ada lampiran</a>
												<input type="file" accept="image/jpeg,image/gif,image/png" name="file">
											</div>
									    </div>
									</div>
									
									<div class="form-row d-none">
									    <div class="form-group col-md-12">
									        <label>Keterangan Pernyataan </label>
									        <textarea class="form-control" name="keterangan" rows="3"></textarea>
									    </div>
									</div>
									
									<div id="form-jumlah-jawaban" class="form-row">
									    <div class="form-group col-md-3">
									        <label class="">Jumlah Jawaban : </label>
									        <div class="input-group mb-3">
											  <div class="input-group-prepend">
											    <button class="btn btn-outline-secondary button-minus" type="button" id="button-minus">-</button>
											  </div>
											  <input min="1" type="number" class="form-control" value="1" name="jumlah_jawaban" placeholder="Jumlah pilihan jawaban" readonly="readonly">
											  <div class="input-group-append">
											    <button class="btn btn-outline-secondary button-plus" type="button" id="button-plus">+</button>
											  </div>
											  <div class="input-group-append">
											    <button data-obj="" id="btn-create-form" onclick="createFormJawaban()" class="btn btn-outline-success" type="button" >Create</button>
											  </div>
											</div>
									    </div>
									</div>
									<div class="form-row">
									    <div class="form-group col-md-12"><strong class="control-label">Pilihan Jawaban : </strong>
									    </div>
									</div>
									<div class="form-row">
								  	<div id="form-pilihan-jawaban" class="form-group col-md-12"></div>
									</div>
									
							        <input type="text" class="form-control d-none" name="kunci_jawaban_work" autocomplete="off">
									
									<div class="form-row ">
							            <div class="form-group col-md-3">
							                <label>Durasi Menjawab/Pertanyaan *</label>
							                <div class="input-group mb-3">
							                	<input type="number" name="durasi" class="form-control" placeholder="Tentukan">
							            		<div class="input-group-append">
											    	<span class="input-group-text">Detik</span>
											  	</div>
							            	</div>
							            </div>
									</div>
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>


                <!-- Form Modal-->
                <div class="modal fade" id="modal-form-update" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
							<form id="entry-form-update">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Update/Reset Durasi</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" ></div>
							        <input type="text" class="form-control" name="id" autocomplete="off" readonly>
									
									<div class="form-row ">
									    <div class="form-group col-md-6">
									        <label>Action</label>
									        <select id="id_action" class="form-control" name="id_action">
									        	<option value="UPDATE">UPDATE</option>
									        	<option value="RESET">RESET</option>
									        </select>
									    </div>
							            <div class="form-group col-md-6">
							                <label>Durasi Pertanyaan *</label>
							                <div class="input-group mb-3">
							                	<input type="number" name="durasi" class="form-control" placeholder="Tentukan">
							            		<div class="input-group-append">
											    	<span class="input-group-text">Detik</span>
											  	</div>
							            	</div>
							            </div>
									</div>
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button class="btn btn-primary" type="submit">Simpan</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="modal-form-delete" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
							<form id="entry-form-delete">
							    <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Form Update/Reset Durasi</h5>
							        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
							            <span aria-hidden="true">�</span>
							        </button>
							    </div>
							    <div class="modal-body">
							        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" ></div>
							        <input type="text" class="form-control" name="id" autocomplete="off" readonly>
									
									<div class="form-row ">
									    <div class="form-group col-md-12">
									        <label>Action</label>
									        <select id="id_action" class="form-control" name="id_action">
									        	<option value="DELETE">DELETE</option>
									        </select>
									    </div>
									</div>
							    </div>
							    <div class="modal-footer">
							        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
							        <button class="btn btn-primary" type="submit">Submit</button>
							    </div>
							</form>
                        </div>
                    </div>
                </div>
                
                <script src="${pageContext.request.contextPath}/js/form-pertanyaan.js"></script>

    </body>
</html>