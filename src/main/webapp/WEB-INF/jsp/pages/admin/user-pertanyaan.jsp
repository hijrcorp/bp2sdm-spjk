<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
	<style>
		table,thead,th {
    		vertical-align: middle!important;
		}
		.popover{
		    /* max-width:1000px !important; */
		}
	</style>
    <body id="page-top">

<%@ include file = "inc/navbar.jsp" %>

<div id="wrapper">

    <%@ include file = "inc/sidebar.jsp" %>

       <div id="content-wrapper">

           <div class="container-fluid">

               <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">DATA TRANSAKSI :: FORM RESET USER PERTANYAAN</li>
                </ol>
                
                <div class="card mb-3 ">
                    <div class="card-header">
                        Parameter Pencarian
                    </div>
                    <div class="card-body">
					   <form id="search-form" autocomplete="off">
					      <input type="hidden" name="limit" value="1">
					      <div class="form-group row">
					         <label class="col-md-2">NIP Responden</label>
					         <div class="col-md-8">
					            <input name="filter_nip_responden" class="form-control" placeholder="Ketik NIP Responden">
					         </div>
					         <div class="col-md-2">
					            <!-- <button id="btn-reset" class="btn btn-danger" type="button">
					            <i class="fa fa-fw fa-times-circle"></i>Reset</button> -->
					            <button id="btn-search" class="btn btn-primary" type="submit"><i class="fas fa-fw fa-search"></i> Cari</button>
					         </div>
					      </div>
					   </form>
					   <hr>
					   <h4 class="card-title">Info Responden</h4>
					   <div class="form-row">
					      <div class="col-md-6 form-group">
					         <label>Nama Lengkap</label>
					         <input type="text" class="form-control" disabled name="nama_lengkap">
					      </div>
					      <div class="col-md-6 form-group">
					         <label>ID Account/Status</label>
					         <input type="text" class="form-control" disabled name="id_account">
					      </div>
					   </div>
					   
					   <hr/>
					   <div class="form-row justify-content-end">
	                    	<div class="col-md-12">
	                        <div class="float-right">
							    <button data-toggle="tooltip" data-placement="top" title="Gunakan fungsi ini, apabila responden mengalami kesalahan input satker atau jabatannya." id="btn-delete-kompetensi" class="btn btn-danger" type="button" disabled><i class="fas fa-trash"></i> Reset Responden</button>
							    <button data-toggle="tooltip" data-placement="top" title="Gunakan fungsi ini, apabila responden stuck pada menu konfirmasi profil pegawai." id="btn-sinkorn-kompetensi" class="btn btn-success" type="button" disabled><i class="fas fa-check"></i> Perbaiki Responden</button>
	                        </div>
	                        </div>
					   </div>
					</div>
                </div> 

                <div class="row">
                	<div class="col-md-12">
                 <div class="card mb-3">
                     <div class="card-header">
                        <nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
						  <!-- <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PDL</a> -->
						  	  </div>
						</nav>
                       </div>
                       
                       <div class="card-body">
                    	<div class="row mb-2">
                   		<div class="col-md-3">
                   			<button id="btn-deleteall" class="btn btn-info" type="button"><i class="fas fa-clock"></i> Reset Soal</button>
                        </div>
                        </div>
                        <div class="table-responsive">
                            <table id="tbl-data" class="table table-sm table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                    <tr>
                                        <th width="1%"><input type="checkbox" name="cek_all_pertanyaan"></th>
                                        <th width="1%">No</th>
                                        <th id="th-kode" width="10%">Kode</th>
                                        <th width="67%">Isi Soal & Jawaban</th>
										<th id="th-kunci-jawaban" width="3%">Kunci Jawaban</th>
                                         </tr>
                                     </thead>
                                     <tbody>

                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
</div>
                    </div>

                </div>
                <!-- /.container-fluid -->

           <%@ include file = "inc/trademark.jsp" %>

       </div>
       <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<%@ include file = "inc/footer.jsp" %>

   <!-- Form Modal-->
         <div class="modal fade" id="modal-form-update" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
					<form id="entry-form-update">
					    <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Form Update/Reset Durasi</h5>
					        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					            <span aria-hidden="true">�</span>
					        </button>
					    </div>
					    <div class="modal-body">
					        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" ></div>
					        <input type="text" class="form-control" name="id" autocomplete="off" readonly>
							
							<div class="form-row ">
							    <div class="form-group col-md-6">
							        <label>Action</label>
							        <select id="id_action" class="form-control" name="id_action">
							        	<option value="UPDATE">UPDATE</option>
							        	<option value="RESET">RESET</option>
							        </select>
							    </div>
					            <div class="form-group col-md-6">
					                <label>Durasi Pertanyaan *</label>
					                <div class="input-group mb-3">
					                	<input type="number" name="durasi" class="form-control" placeholder="Tentukan">
					            		<div class="input-group-append">
									    	<span class="input-group-text">Detik</span>
									  	</div>
					            	</div>
					            </div>
							</div>
					    </div>
					    <div class="modal-footer">
					        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
					        <button class="btn btn-primary" type="submit">Simpan</button>
					    </div>
					</form>
                 </div>
             </div>
         </div>
         
         <div class="modal fade" id="modal-form-delete" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
					<form id="entry-form-delete">
					    <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Form Update/Reset Durasi</h5>
					        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					            <span aria-hidden="true">�</span>
					        </button>
					    </div>
					    <div class="modal-body">
					        <div id="modal-form-msg" class="alert alert-danger d-none" role="alert" ></div>
					        <input type="text" class="form-control" name="id" autocomplete="off" readonly>
							
							<div class="form-row ">
							    <div class="form-group col-md-12">
							        <label>Action</label>
							        <select id="id_action" class="form-control" name="id_action">
							        	<option value="DELETE">DELETE</option>
							        </select>
							    </div>
							</div>
					    </div>
					    <div class="modal-footer">
					        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
					        <button class="btn btn-primary" type="submit">Submit</button>
					    </div>
					</form>
                 </div>
             </div>
         </div>
         
         <script src="${pageContext.request.contextPath}/js/user-pertanyaan.js"></script>

    </body>
</html>