<script>var vargolongan="";</script>
<div class="card">
<div class="card-body">
   <style>
      .chart-legend li span{
      display: inline-block;
      width: 12px;
      height: 12px;
      margin-right: 5px;
      }
   </style>
   <div class="float-right d-none" style="margin-top: -0px">
      <button id="btn-download" class="btn btn-dark" type="button"><i class="fas fa-fw fa-download"></i></button>
      <button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
   </div>
   <div class="row justify-content-center">
      <div class="col-md-2 col-6">
         <img src="/spektra/assets-dashboard/images/icon/picture.png" data-src="https://drive.google.com/uc?export=view&id=14LM6WzR8AaAsYe96oDdb4rkQelRRCqIq" alt="Lord " style="width: 240px">
      </div>
      <!-- <div class="col-md-10 pt-2" style="font-size: 18px">
         <h2 id="nama_profile" class="mb-1">Abdul Ahmad</h2>
         <p id="satker_profle">Dinas Lingkungan Hidup Kabupaten Agam</p>
         <p id="jabatan_profile">Pengendali Ekosistem Hutan PEMULA
         </p>
         <p id="bidang_teknis" style="color: red; font-style: italic">PEH-Bidang Perencanaan Hutan</p>
         <br>
      </div> -->
   </div>

   <div class="float-left">
      <h3 class="primary-color mt-3">Biodata <i class="fa fa-pencil d-none"></i></h3>
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12">
	        <c:choose>
			<c:when test = "${responden == null}">
				<div class="card-title">
					<div class="alert alert-info">Anda belum dapat melihat form ini, sebelum melakukan konfirmasi jabatan anda <strong>atau</strong> admin belum mengupload data anda ke dashboard.</div>
					<form id="entry-form-biodata-sinkorn">
				         <input name="id" type="hidden" value="">
		         	</form>
				</div>
			</c:when>
			<c:otherwise>
         <form id="entry-form-biodata-sinkorn">
	         	<input name="id" type="hidden" value="${responden.id}">
	            <div class="row form-group">
	               <div class="col-lg-6 col-12">
	                  <label for="">Nama Lengkap</label>
	                  <input type="text" class="form-control" value="${responden.nama}" disabled>
	               </div>
	               <div class="col-lg-6 col-12">
	                  <label for="">NIP</label>
	                  <input type="number" class="form-control" value="${responden.nip}" disabled>
	               </div>
	            </div>
	            <div class="row form-group">
	               <div class="col-lg-6 col-12">
	                  <label for="">Jenis Kelamin</label>
	                  <select name="jenis_kelamin" class="form-control" disabled>
	                  	<option>PILIH JENIS KELAMIN</option>
	                  	<c:choose>
				         <c:when test = "${responden.jenisKelamin == 'LAKI-LAKI'}">
				         	<option value="LAKI-LAKI" selected>LAKI-LAKI</option>
				         	<option value="PEREMPUAN">PEREMPUAN</option>
				         </c:when>
				         <c:when test = "${responden.jenisKelamin == 'PEREMPUAN'}">
				         	<option value="LAKI-LAKI" selected>LAKI-LAKI</option>
				         	<option value="PEREMPUAN" selected>PEREMPUAN</option>
				         </c:when>
					       <c:otherwise>
				         	<option value="LAKI-LAKI" selected>LAKI-LAKI</option>
	                  		<option value="PEREMPUAN">PEREMPUAN</option>
					       </c:otherwise>
			         	</c:choose>
	                  </select>
	               </div>
	               <div class="col-lg-6 col-12">
	                  <!-- <label for="">Tempat, Tanggal Lahir</label> -->
	                  	<label for="">Tanggal Lahir(DD-MM-YYY)</label>
				      	<!-- <div class="input-group">
						  <input type="text" class="form-control" value="" disabled>
						  <div class="input-group-append">
						    <span class="input-group-text">,</span>
						  </div>
						  <input type="date" class="form-control" value="" disabled>
					  	</div> -->
						<input name="tanggal_lahir" type="date" class="form-control" value="${responden_tglLahir}" disabled>
	               </div>
	            </div>
	            <div class="row form-group ">
	               <div class="col-lg-6 col-12">
	                  <label for="">Jabatan</label>
	                  <input type="text" class="form-control" value="${jabatan}" disabled>
	               </div>
	               <div class="col-lg-6 col-12">
		               <label for="">Satker/Unit Kerja</label>
		               <input type="text" class="form-control" value="${namaSatker}" disabled>
	               </div>
	            </div>
	            <div class="row form-group">
	               <div class="col-lg-6 col-12">
	                  <label for="">Golongan</label>
	                  <select name="golongan" class="form-control" disabled>
	                  	<option>PILIH GOLONGAN</option>
	                  </select>
	                  	<script>vargolongan="${responden.golongan}";</script>
	               </div>
	            </div>
	            <hr/>
	            <div class="row justify-content-center ">
		           <div class="col-sm-12 col-md-6 ">
		           <button id="btn-sinkorn" type="submit" class="btn btn-primary btn-block-sm">SINKORN</button>
		        	</div>
		           <div class="col-sm-12 col-md-6 text-right">
		              <button id="btn-edit" onclick="$('#entry-form-biodata-sinkorn').find('[name=golongan]').prop('disabled', false);$('#entry-form-biodata').find('button').prop('disabled', false);" type="button" class="btn btn-primary ">EDIT</button>
		              <button id="btn-update" type="button" class="btn btn-success">UPDATE</button>
		        	</div>
		        </div>
         		<hr/>
	            <div class="row justify-content-center mt-3 ">
		           <div class="col">
		           	<small class="text-muted">Note:</small>
		        	<p class="text-muted small">*Klik tombol SINKORN apabila data : Jenis Kelamin, atau Tanggal lahir anda tidak sesuai.</p>
		        	<p class="text-muted small">*Dan juga form ini hanya dapat mengubah data : Golongan</p>
		           </div>
		        </div>
         </form>
         <!-- <form id="entry-form-biodata"> -->
	         	<%-- <input name="id" type="hidden" value="${responden.id}"> --%>
	            
	            <%-- <div class="row form-group">
	               <div class="col-lg-6 col-12">
	                  <label for="">Email</label>
	                  <input name="email" type="text" class="form-control" value="${email}" disabled>
	               </div>
	               <div class="col-lg-6 col-12">
	                  <label for="">No.Handphone</label>
	                  <input name="mobile_phone" type="number" class="form-control" value="${mobile}" disabled>
	               </div>
	            </div> --%>
	            <!-- <hr/>
	            <div class="row justify-content-center ">
		           <div class="col">
		        	<p class="text-mutted small">*Anda hanya dapat mengubah data : Golongan</p>
		           </div>
		           <div class="col text-right">
		              <button id="btn-edit" onclick="$('#entry-form-biodata').find('[name]').prop('disabled', false);$('#entry-form-biodata').find('button').prop('disabled', false);" type="button" class="btn btn-primary ">EDIT</button>
		              <button id="btn-submit" type="submit" class="btn btn-success">UPDATE</button>
		           </div>
		        </div> -->
         <!-- </form> -->
			</c:otherwise>
			</c:choose>
      </div>
   </div>
</div>
</div>

<div class="card">
<div class="card-body">
   <div class="float-left">
      <h3 class="primary-color mt-3" style="font-size: 22px">Riwayat Jabatan 
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12 table-responsive">
         <table id="tbl-riw-jabatan" class="table table-hover table-striped">
            <thead>
               <tr>
                  <th class="align-middle">No.</th>
                  <th class="align-middle">Jenis Jabatan</th>
                  <th class="align-middle">Instansi</th>
                  <th class="align-middle">Satker</th>
                  <th class="align-middle">Unor</th>
                  <th class="align-middle">Nama Jabatan</th>
                  <th class="align-middle">TMT Jabatan ~ Pelantikan</th>
                  <th class="align-middle">Surat Keptusan</th>
                  <!-- <th class="align-middle">Sertifikat/Ijazah</th> -->
                  <th class="align-middle">Action</th>
               </tr>
            </thead>
            <tbody>
            	<!-- will be render by function -->
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="11" class="text-center"><button onclick="add('jabatan')" class="btn btn-secondary btn-sm">Tambahkan </button></td>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
   <br>
   <div class="float-left">
      <h3 class="primary-color mt-3" style="font-size: 22px">Riwayat Pendidikan</h3>
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12 table-responsive">
         <table id="tbl-riw-pendidikan" class="table table-hover table-striped">
            <thead>
               <tr>
                  <th class="align-middle">No.</th>
                  <th class="align-middle">Tingkat Pendidikan</th>
                  <th class="align-middle">Jurusan</th>
                  <th class="align-middle">Nama Sekolah/Universitas/Instansi</th>
                  <th class="align-middle">Tahun & Tgl Lulus</th>
                  <th class="align-middle">Nomor Ijazah</th>
                  <th class="align-middle">Gelar</th>
                  <th class="align-middle">Action</th>
               </tr>
            </thead>
            <tbody>
               <!-- will render by function -->
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="8" class="text-center"><button onclick="add('pendidikan')" class="btn btn-secondary btn-sm">Tambahkan </button></td>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
   <br>
   <div class="float-left">
      <h3 class="primary-color mt-3" style="font-size: 22px">Riwayat Pelatihan</h3>
   </div>
   <hr class="mt-5">
   <div class="row">
      <div class="col-md-12 table-responsive">
         <table id="tbl-riw-pelatihan" class="table table-hover table-striped">
            <thead>
               <tr>
                  <th class="align-middle">No.</th>
                  <th class="align-middle">Jenis Diklat</th>
                  <th class="align-middle">Nama Diklat</th>
                  <th class="align-middle">Instisui Penyelenggara</th>
                  <th class="align-middle">Nomor Sertifikat</th>
                  <th class="align-middle">Tahun Diklat</th>
                  <th class="align-middle">Tanggal</th>
                  <th class="align-middle">Durasi</th>
                  <th class="align-middle">Action</th>
               </tr>
            </thead>
            <tbody>
	               <!-- will render by function -->
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="9" class="text-center"><button onclick="add('pelatihan')" class="btn btn-secondary btn-sm">Tambahkan </button></td>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
   <hr class="mt-5">
</div>
</div>
          
          