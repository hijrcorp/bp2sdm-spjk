<%@page import="id.go.menlhk.bp2sdm.spjk.model.Account"%>
<%@page import="id.go.menlhk.bp2sdm.spjk.mapper.AccountMapper"%>
<%@ include file = "inc-dashboard/header.jsp" %>


<% String kodes="";
if(request.getParameter("kode-sesi")!=null) {
	kodes="kode-sesi="+request.getParameter("kode-sesi");
} 

AccountMapper accountMapper = appCtx.getBean(AccountMapper.class);
Account account = accountMapper.getEntity(request.getAttribute("accountId").toString());
%>  
<style>

    .chart-legend li span{
	    display: inline-block;
	    width: 12px;
	    height: 12px;
	    margin-right: 5px;
	}
</style>

<!-- MAIN CONTENT-->
<div class="main-content" >
  <div class="section__content section__content--p30">
    <div class="container-fluid">

		<!-- Breadcrumbs <%= (request.getParameter("jenis") != null && request.getParameter("jenis").equals("Rekomendasi Pengembangan")?"active":"") %>-->
		<nav id="rekomendasi-nav-body" class="navbar d-none d-sm-block breadcrumb navbar-expand-lg navbar-dark mb-3">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="nav nav-pills mr-auto">
		        <li class="nav-item"><a class="nav-link <% if(request.getParameter("mode").equals("profile")) out.print("active"); %>" href="dashboard?mode=profile&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kodes);else out.print(kodes);%>">Profile</a></li>
		        <li class="nav-item"><a class="nav-link <% if(request.getParameter("mode").equals("nilai-kompetensi")) out.print("active"); %>" href="dashboard?mode=nilai-kompetensi&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kodes);else out.print(kodes);%>">Nilai Kompetensi</a></li>
		        <li class="nav-item"><a class="nav-link <% if(request.getParameter("mode").equals("rekomendasi-pengembangan")) out.print("active"); %>" href="dashboard?mode=rekomendasi-pengembangan&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kodes);else out.print(kodes);%>">Rekomendasi Pengembangan</a></li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		    <%-- <% if(request.getParameter("mode").equals("nilai_kompetensi")) {%> --%>
		    	<input value="<% out.print(request.getParameter("mode")); %>" name="mode" class="form-control mr-sm-2 d-none" type="text"/>
		    <%-- <% } %> --%>
		        <input value="<% out.print(request.getParameter("nip")!=null?request.getParameter("nip"):""); %>" name="nip" class="form-control mr-sm-2 d-none" type="number" placeholder="NIP" aria-label="Search" style="width: 250px;"/>
		        <!-- just for view 
		        <input id="nip" class="form-control mr-sm-2" type="search" placeholder="Nama/NIP" aria-label="Search" style="width: 250px;"/>
		        -->
		        <button id="btn-search" class="btn btn-outline-success my-2 my-sm-0 d-none" type="button">Cari</button>
		    </form>
		</div>
		</nav>
		
		<% if(request.getParameter("mode").equals("nilai-kompetensi")) {%>
		<div id="rekomendasi-body" class="card">
			<div class="card-body">
				<div class="row ">
					<div class="col-md-2 col-12 d-flex justify-content-center">
						<img src="${assetPath}/images/icon/picture.png" data-src="${assetPath}/images/icon/picture.png" alt="${requestScope.realName}" style="width: 240px" />
					</div>
					<div class="col-md-10 col-12 pt-2" style="font-size: 18px">
						<h2 id="nama_profile" class="mb-1">-</h2>
						<p id="satker_profle">-</p>
						<p id="jabatan_profile">-</p>
						<p id="bidang_teknis" style="color: red; font-style: italic">-</p>
						<br/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-12 ">
						
					</div>
					<div class="col-md-10 col-12 pt-2" style="font-size: 18px">
						<div class="row ">
							<div class="col-md-4 col-12 d-none d-sm-block">
								<div class=" " style="border: 0px solid black;">
									<strong>Kompetensi</strong><br/>Manajerial
								<div class="d-none d-sm-block" style="border: 0px solid black;">
									<button id="progress_manajerial<% if(request.getAttribute("introduceSoal").equals("1")) out.print(request.getAttribute("introduceSoal")); %>" class="btn btn-success  " style="font-size: 22px; padding: 9px; width: 127px; font-weight: bold;">-</button>
								</div>
								</div>
							</div>
							<div class="col-md-4 col-12 d-none d-sm-block">
								<div class=" ">
									<strong>Kompetensi</strong><br/>Sosiokultural
								</div>
								<div class="d-none d-sm-block">
									<button id="progress_sosiokultural<% if(request.getAttribute("introduceSoal").equals("1")) out.print(request.getAttribute("introduceSoal")); %>" class="btn btn-success  " style="font-size: 22px; padding: 9px; width: 124px; font-weight: bold;">-</button>
								</div>
							</div>
							<div class="col-md-4 col-12 d-none d-sm-block">
								<div class=" ">
									<strong>Kompetensi</strong><br/>Teknis
								</div>
								<div class="d-none d-sm-block">
									<button id="progress_teknis<% if(request.getAttribute("introduceSoal").equals("1")) out.print(request.getAttribute("introduceSoal")); %>" class="btn btn-success " style="font-size: 22px; padding: 9px; width: 127px; font-weight: bold;">-</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<br/>
				
				<div class="row no-gutters d-flex justify-content-between mt-3">
					<div class="col-10">
						<h3 class="primary-color ">Nilai Kompetensi</h3>
					</div>
					<div class="col-2 text-right">
						<button id="btn-download" class="btn btn-dark d-none" type="button"><i class="fas fa-fw fa-download"></i></button>
						<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
					</div>
				</div>
				
				<hr class=""/>
				
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Manajerial</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6">
						<div class="mb-3" style="margin-top: 0px"> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="radar"></canvas>
						</div>
					</div>
					<div class="col-md-6">
						<h4 class="secondary-color" style="margin-bottom: 10px;">Tabel Nilai Kompetensi Manajerial</h4>
						<div class="table-responsive">
						<table id="tbl-manajerial" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Manajerial</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				
				<br/>
				
				<div class="row">
					<div class="col-md-12">
						<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Sosiokultural</h3>
					</div>
				</div>
				
				<hr class=""/>
				<div class="row">
					<div class="col-md-6">
					</div>
					<div class="col-md-6">
						<h4 class="secondary-color" style="margin-bottom: 10px;">Tabel Nilai Kompetensi Sosiokultural</h4>
						<div class="table-responsive">
						<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th width="50"  class="text-center">No</th>
									<th width="200" class="text-center">Kompetensi Sosiokultural</th>
									<th width="100" class="text-center">Level Standar</th>
									<th width="100" class="text-center">Hasil Pengukuran</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				
				
		<!-- ... content in page 1 ... -->
		<p style="page-break-after: always;">&nbsp;</p>
		<!-- <p style="page-break-before: always;">&nbsp;</p>
		... content in page 2 ... -->
		
				<br/>
				<div class="float-left">
					<h3 class="secondary-color mt-3" style="font-size: 22px">Kompentensi Teknis</h3>
				</div>
				<hr class="mt-5"/>
				<div class="row">
					<div class="col-md-6 mb-3">
						<h3 class="secondary-color " style="font-size: 22px">INTI</h3>
						<div id="container-bar-inti" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-inti"></canvas>
						</div>
						<div id="container-legend1" class="chart-legend d-flex justify-content-center"></div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Peta Kompetensi INTI </h5>
						<div class="table-responsive">
						<table id="tbl-teknis-inti" class="table table-bordered">
							<tbody>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				
				<hr class="mt-5"/>
				
				<div class="row">
					<div class="col-md-6 mb-3">
						<h3 class="secondary-color " style="font-size: 22px">PILIHAN</h3>
						<div id="container-bar-pilihan" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942;  -->
							<canvas id="bar-pilihan"></canvas>
						</div>
						<div id="container-legend2" class="chart-legend d-flex justify-content-center"></div>
					</div>
					<div class="col-md-6">
						<h5 class="secondary-color" style="font-size: 18px; margin-bottom: 10px;">Tabel Peta Kompetensi PILIHAN</h5>
						<div class="table-responsive">
						<table id="tbl-teknis-pilihan" class="table table-bordered">
							<tbody>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<% } %>
			
		<% if(request.getParameter("mode").equals("rekomendasi-pengembangan")) {%>
		<div id="rekomendasi-body" class="card">
			<div class="card-body">
				<%@ include file = "per-individu-rekomendasi-pengembangan.jsp" %>
			</div>
		</div>
		<% } %>
		
		
		<% if(request.getParameter("mode").equals("profile")) {%>
			<%@ include file = "profile.jsp" %>
		<% } %>
		
      <%@ include file = "./inc-dashboard/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.3"></script>
<%-- <script src="${assetPath}/js/dashboard.js"></script> --%>
<%@ include file = "inc-dashboard/footer.jsp" %>

<!-- Form Modal Jabatan-->
<div class="modal  fade" id="modal-form-jabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <form id="entry-form-jabatan">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
               <button class="close" type="button" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">�</span>
               </button>
            </div>
            <div class="modal-body">
               <div id="modal-form-msg-jabatan" class="modal-form-msg alert alert-danger d-none" role="alert"></div>
               <input type="text" class="form-control d-none" name="id" autocomplete="off">
               <input type="text" class="form-control d-none" name="id_header" value="<%out.print(account.getId());%>">
              
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Jenis Jabatan</label>
                     <select name="jenis_jabatan" class="form-control">
                     	<option value="">Pilih</option>
                     	<option>Fungsional Umum</option>
                     	<option>Fungsional Tertentu</option>
                     	<option>Strukturual</option>
                     </select>
                  </div>
                  <div class="form-group col-md-6">
                     <label>Instansi</label>
                     <input type="text" name="nama_perusahaan" class="form-control">
                  </div>
               </div>
               
			   <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Satuan Kerja</label>
                     <input type="text" name="nama_unit_auditi" class="form-control">
                  </div>
                  <div class="form-group col-md-6">
                     <label>Unor</label>
                     <input type="text" name="unor" class="form-control">
                  </div>
               </div>
               
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label>Nama Jabatan</label>
                     <input type="text" name="jabatan" class="form-control">
                  </div>
               </div>
               					
				<div class="form-row">
			      <div class="form-group col-md-12">
			      	<label>Periode * </label>
			      	<div class="input-group">
					  <input type="date" name="periode_awal" class="form-control">
					  <div class="input-group-append">
					    <span class="input-group-text">Hingga</span>
					  </div>
					  <input type="date" name="periode_akhir" class="form-control">
				  	</div>
			      </div>
			    </div>
               
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Nomor Surat Keputusan</label>
                     <input type="text" name="no_surat_keputusan" class="form-control">
                  </div>
			      <div class="form-group col-md-6">
			      	<label>Tanggal Surat Keputusan </label>
			      	<input type="date" name="tanggal_surat_keputusan" class="form-control">
			      </div>
               </div>
               			
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label>Dokumen Surat Keputusan</label>
                     <input name="link" class="form-control" type="text" placeholder="Paste URL/LINK disini.">
                     <small class="text-muted">*pastikan url yang anda cantumkan telah aktif dan terhubung ke google drive anda, agar admin dapat memeriksa kevalidan data.</small>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
               <button onclick="save('jabatan');" class="btn btn-primary" type="button">Simpan</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- Form Modal Pendidikan-->
<div class="modal fade" id="modal-form-pendidikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <form id="entry-form-pendidikan">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
               <button class="close" type="button" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">�</span>
               </button>
            </div>
            <div class="modal-body">
               <div id="modal-form-msg-pendidikan" class="modal-form-msg alert alert-danger d-none" role="alert"></div>
               <input type="text" class="form-control d-none" name="id" autocomplete="off">
               <input type="text" class="form-control d-none" name="id_header" value="<%out.print(account.getId());%>">
               
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Tingkat Pendidikan</label>
                     <select name="tingkat_pendidikan" class="form-control">
                     	<option value="">Pilih</option>
                     	<option>SMA</option>
                     	<option>D1</option>
                     	<option>D2</option>
                     	<option>D3</option>
                     	<option>S1</option>
                     	<option>S2</option>
                     	<option>S3</option>
                     </select>
                  </div>
                  <div class="form-group col-md-6">
                     <label>Jurusan</label>
                     <input type="text" name="pendidikan" class="form-control">
                  </div>
               </div>
               
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label>Nama Sekolah/Universitas/Instansi</label>
                     <input type="text" name="nama_instansi" class="form-control">
                  </div>
               </div>
               
									
				<div class="form-row">
			      <div class="form-group col-md-12">
			      	<label>Tahun & Tanggal Lulus </label>
			      	<div class="input-group">
					  <select name="tahun_lulus" class="form-control">
					  	<option value="">--Pilih Tahun--</option>
					  </select>
					  <div class="input-group-append">
					    <span class="input-group-text">Tanggal Lulus :</span>
					  </div>
					  <input type="date" name="tanggal_lulus" class="form-control">
				  	</div>
				  	
			      </div>
			   </div>
			   
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label>Nomor Ijazah</label>
                     <input type="text" name="no_ijazah" class="form-control">
                  </div>
               </div>
			    
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Gelar Depan</label>
                     <input type="text" name="gelar_depan" class="form-control">
                  </div>
			      <div class="form-group col-md-6">
			      	<label>Gelar Belakang</label>
			      	<input type="text" name="gelar_belakang" class="form-control">
			      </div>
               </div>
			   
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label>Sertifikat/Ijazah</label>
                     <input name="link" class="form-control" type="text" placeholder="Paste URL/LINK disini.">
                     <small class="text-muted">*pastikan url yang anda cantumkan telah aktif dan terhubung ke google drive anda, agar admin dapat memeriksa kevalidan data.</small>
                  </div>
               </div>
			    
            </div>
            <div class="modal-footer">
               <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
               <button onclick="save('pendidikan');" class="btn btn-primary" type="button">Simpan</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- Form Modal Pelatihan-->
<div class="modal fade" id="modal-form-pelatihan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <form id="entry-form-pelatihan">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
               <button class="close" type="button" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">�</span>
               </button>
            </div>
            <div class="modal-body">
               <div id="modal-form-msg-pelatihan" class="modal-form-msg alert alert-danger d-none" role="alert"></div>
               <input type="text" class="form-control d-none" name="id" autocomplete="off">
               <input type="text" class="form-control d-none" name="id_header" value="<%out.print(account.getId());%>">
               
               
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Jenis Diklat</label>
                     <select name="jenis_diklat" class="form-control">
					  	<option value="">--Pilih Jenis Diklat--</option>
					  	<option value="Diklat Struktural">Diklat Struktural</option>
					  	<option value="Diklat Fungsional">Diklat Fungsional</option>
					  	<option value="Diklat Teknis">Diklat Teknis</option>
					  	<option value="Workshop/Kursus/Seminar/Magang">Workshop/Kursus/Seminar/Magang</option>
					  </select>
                  </div>
                  <div class="form-group col-md-6">
                     <label>Nama Diklat</label>
                     <input type="text" name="nama_diklat" class="form-control">
                  </div>
               </div>
               
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label>Institusi Penyelenggara</label>
                     <input type="text" name="institusi_penyelenggara" class="form-control">
                  </div>
               </div>
               
				
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Nomor Sertifikat</label>
                     <input type="text" name="no_sertifikat" class="form-control">
                  </div>
                  <div class="form-group col-md-6">
                     <label>Tahun Diklat</label>
					  <select name="tahun_diklat" class="form-control">
					  	<option value="">--Pilih Tahun--</option>
					  </select>
                  </div>
               </div>
               			
				<div class="form-row">
			      <div class="form-group col-md-12">
			      	<label>Tanggal Mulai/Selesai </label>
			      	<div class="input-group">
					  <input type="date" name="tanggal_awal" class="form-control">
					  <div class="input-group-append">
					    <span class="input-group-text">Hingga</span>
					  </div>
					  <input type="date" name="tanggal_akhir" class="form-control">
				  	</div>
				  	
			      </div>
			   </div>
			   			
				<div class="form-row">
			      <div class="form-group col-md-12">
			      	<label>Durasi(Jam) </label>
			      	<input type="number" name="durasi" class="form-control">
			      </div>
			    </div>
			    
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label>Sertifikat/Ijazah</label>
                     <input name="link" class="form-control" type="text" placeholder="Paste URL/LINK disini.">
                     <small class="text-muted">*pastikan url yang anda cantumkan telah aktif dan terhubung ke google drive anda, agar admin dapat memeriksa kevalidan data.</small>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
               <button onclick="save('pelatihan');" class="btn btn-primary" type="button">Simpan</button>
            </div>
         </form>
      </div>
   </div>
</div>

<% if(request.getParameter("mode").equals("nilai-kompetensi")) {%>
<script src="${assetPath}/js/user/per-individu.js"></script>
<% } else if(request.getParameter("mode").equals("profile")) {%>
<script src="${assetPath}/js/user/profile.js"></script>
<% } else if(request.getParameter("mode").equals("rekomendasi-pengembangan")) {%>
<script src="${assetPath}/js/user/per-individu-rekomendasi-pengembangan.js"></script>
<% } %>

<script>
	$('#btn-print').on('click', function (e) {
		$('aside').remove();
		$('header').remove();
		$('.page-container.pl-lg-default').attr("class", "");
		$('#rekomendasi-body').attr("class", "border-0");
		$("#rekomendasi-nav-body").addClass('d-md-none')
		$('#form-search').attr("class", "d-none");
		document.getElementsByClassName("copyright")[0].parentElement.parentElement.remove();
		document.getElementsByClassName("main-content")[0].className="";
		$('#pills-tab').hide();
		$(document.getElementById("form-search").children[0]).hide();
		$(document.getElementById("form-search").children[2]).hide();
		$(document.getElementById("form-search").children[3]).hide();
		$(document.getElementById("form-search").children[4]).hide();
	});
</script>
