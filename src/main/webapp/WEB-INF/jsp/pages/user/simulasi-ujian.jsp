<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
	<script>var idResponden = "${requestScope.idResponden}"</script>
	<style>
		.connection-notification {
		  position: fixed;
		  bottom: 0; /* Change to `bottom: 0;` if you want it at the bottom */
		  left: 0;
		  width: 100%;
		  background-color: #ff4d4d; /* Red for lost connection */
		  color: white;
		  text-align: center;
		  padding: 18px;
		  font-size: 16px;
		  z-index: 2000;
		}
		
		.connection-notification.connected {
		  background-color: #4caf50; /* Green for connection restored */
		}
		.blur-text {
    filter: blur(5px);
    transition: filter 0.3s ease; /* Optional for a smooth transition */
}
	</style>
		
</head>

<body onbeforeunload="return trysave()">
	<%@include file="inc/navbar.jsp"%>
	<c:choose>
         <c:when test = "${requestScope.statusResponden == null}">
         	<c:redirect url="/page/index"/>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'MANAJERIAL' && requestScope.statusResponden == ''}">
         	<c:redirect url="/page/main-unit-kompetensi"/>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'SOSIOKULTURAL' && requestScope.statusResponden == ''}">
         	<c:redirect url="/page/main-unit-kompetensi"/>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'TEKNIS_PILIHAN' && requestScope.statusResponden == ''}">
         	<c:redirect url="/page/main-unit-kompetensi"/>
         </c:when>
    </c:choose>

    <div class="container">
      <div class="row justify-content-center pt-5">
        <div class="col-md-9">
			<div id="table-card-body">
				<div class="loading-row list-group-item text-center"><img width="20" src="${pageContext.request.contextPath}/images/loading-spinner.gif"/> Mohon Tunggu..</div>
			</div>
          	<div id="info-answers" class="card-footer bg-light border fixed-bottom">
	    	<div class="container">
	    	<div class="row">
			    <div class="col-auto col-md-6" id="info-pertanyaan"></div><!-- Pertanyaan 1 dari 3 Pertanyaan   -->
			    <div class="col-auto col-md-6 text-right d-none" id="pernyataan-titles">Terjawab 0 dari 0 Pernyataan  </div>
		   </div>
			</div>
			</div>
			<div id="container-btn-end" class="row mb-3 d-none">
				<div class="col col-md-auto"><button class="btn btn-success btn-block">Lanjutkan</button></div>
			</div>
        </div>
        <div class="col-md-3">
        <div class="card">
        	<div class="card-header card-title">Nomor Soal</div>
        	<div class="card-body">
        	<div id="row-card-side" class="row">
        	<%-- <% for(int i=1; i <= 10; i++) { %>
        		<div class="border col-md-2 p-1 m-1 text-center"><% out.print(i); %></div>
       		<% } %> --%>
        	</div>
        	</div>
        </div>
        
        <div class="card mt-3">
        	<div class="card-header card-title">Keterangan</div>
        	<div class="card-body">
			<div><span class="border border-orange bg-warning pr-4 mr-1"></span> Sedang Dikerjakan</div>
			<div><span class="border bg-light pr-4 mr-1"></span> Belum Dijawab</div>
			<div><span class="border border-blue bg-primary pr-4 mr-1"></span> Sudah Dijawab</div>
			</div>
        </div>
        </div>
      </div>
    </div>
    
    
    
    
    
	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div id="modal-content" class="modal-body">
	        ...
	      </div>
	      <div id="modal-action" class="modal-footer">
	        
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Maaf Waktu Anda habis.</h5>
	        <button type="button" class="close d-none" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <p>Maaf waktu untuk menjawab pertanyaan pada Sesi Kompetensi <strong id="kode_pertanyaan">X</strong> Telah Habis.</p>
	        Silahkan Lanjut Apabila anda telah siap.
	      </div>
	      <div class="modal-footer">
	        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
	        <button id="btn-mulai" type="button" class="btn btn-primary">Lanjut</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Connection Notification -->
	<div id="connectionNotification" class="connection-notification" style="display: none;">
	  <span id="connectionMessage">Connection lost. Reconnecting...</span>
	</div>
	
	<script src="https://cdn.jsdelivr.net/npm/sockjs-client/dist/sockjs.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/general-simulasi-ujian.js"></script>
	<%@include file="inc/footer.jsp"%>
	
</body>

</html>