<!-- <div class="card-body"> -->
	<style>
	    .chart-legend li span{
		    display: inline-block;
		    width: 12px;
		    height: 12px;
		    margin-right: 5px;
		}
		
		
		@media (max-width: 479px){
			.card {
				font-family:none!important;
			}
		}
	</style>
	<div class="row">
		<div class="col-md-2">
			<img src="${assetPath}/images/icon/picture.png" data-src="${assetPath}/images/icon/picture.png" alt="${requestScope.realName}" style="width: 240px" />
		</div>
		<div class="col-md-4 pt-2" style="font-size: 18px">
			<h2 id="nama_profile" class="mb-1">-</h2>
			<p id="satker_profle">-</p>
			<p id="jabatan_profile">-</p>
			<p id="bidang_teknis" style="color: red; font-style: italic">-</p>
			<div id="status_profile" class="mt-3 text-center" style="border: 1px solid black; font-size: 30px; padding: 4px; width: 170px; font-weight: bold;">-</div>
		</div>
		<div class="col-md-4 pt-2 text-right" style="font-size: 20px">
			<span style="font-size: 20px; font-weight: 500">
				Rekomendasi Pengembangan Kompetensi<br/>
				SDM Aparatur Jabatan Fungsional KLHK<br/>
				Pusat Perencanaan dan Pengembangan SDM
			</span>
		</div>
		<div class="col-md-2 text-right">
			<img src="${assetPath}/images/logo1.png" data-src="${assetPath}/images/logo-menlhk.png" alt="${requestScope.realName}" style="width: 150px;height: 150px;" />
		</div>
	</div>
	
	<br/>
	<div style="margin-bottom: 10px">
		<div class="float-left">
			<h3 class="primary-color mt-3">Ringkasan Hasil Pemetaan</h3>
		</div>
		<div class="float-right">
			<button id="btn-download" class="btn btn-dark d-none" type="button"><i class="fas fa-fw fa-download"></i></button>
			<button id="btn-print" class="btn btn-dark" type="button"><i class="fas fa-fw fa-print"></i></button>
		</div>
	</div>
	<hr class="mb-5" style="margin-top: 60px"/>
	
	<div class="row">
		<div class="col-md-6">
			<h4 class="primary-color mb-3">Kompetensi Teknis</h4>
			<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<h4 class="card-title">INTI</h4>
						<div id="container-bar-inti" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942; height: 500px !important;  -->
							<canvas id="bar-inti" ></canvas><!-- width="600" height="250" -->
						</div>
						<h4 class="card-title">PILIHAN</h4>
						<div id="container-bar-pilihan" class="chart-container" style="position: relative; "> <!-- zoom: 1.1764705882352942; height: 500px !important;  -->
							<canvas id="bar-pilihan" ></canvas><!-- width="600" height="250" -->
						</div>
					    <div id="container-legend2" class="chart-legend d-flex justify-content-center"></div>
					</div>
				</div>
			</div>
			</div>
		</div>
		<div class="col-md-6">
			<h4 class="primary-color mb-3">Kompetensi Manajerial</h4>
			<div class="card">
			<div class="card-body pl-2 pr-2">
				<canvas id="radar"></canvas>
			</div>
			</div>
			
			<h4 class="primary-color mb-3">Kompetensi Sosiokultural</h4>	
			<div class="card">
			<div class="card-body p-0">
			
			<div class="table-responsive">
			<table id="tbl-sosiokultural2" class="table table-bordered table-striped table-hover small">
				<thead>
					<tr>
						<th width="50"  class="text-center">No</th>
						<th width="200">Kompetensi Sosiokultural</th>
						<th width="100" class="text-center">Level Standar</th>
						<th width="100" class="text-center">Hasil Pengukuran</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
			</div>
			</div>
		</div>
	</div>
	
	<br/>
	<div class="row">
		<div class="col-md-12">
			<h3 class="primary-color ">Rekomendasi Pengembangan</h3>
		</div>
	</div>
	<hr class="mb-3" />
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
			<table id="tbl-teknis" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="" class="text-center">No</th>
						<th width="" class="text-center">Kode</th>
						<th width="" class="text-center">Unit Kompetensi Teknis</th>
						<th width="" class="text-center">Level Standar</th>
						<th width="" class="text-center">Hasil Pengukuran</th>
						<th width="" class="text-center">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
		</div>
		
		<!-- ... content in page 1 ... -->
		<p style="page-break-after: always;">&nbsp;</p>
		<!-- <p style="page-break-before: always;">&nbsp;</p>
		... content in page 2 ... -->
		
		<div class="col-md-12 mt-3">
			<div class="table-responsive">
			<table id="tbl-manajerial" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="" class="text-center">No</th>
						<th width="" class="text-center">Kompetensi Manajerial</th>
						<th width="" class="text-center">Level Standar</th>
						<th width="" class="text-center">Hasil Pengukuran</th>
						<th width="" class="text-center">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
		</div>
		
		<!-- ... content in page 1 ... -->
		<p style="page-break-after: always;">&nbsp;</p>
		<!-- <p style="page-break-before: always;">&nbsp;</p>
		... content in page 2 ... -->
		
		<div class="col-md-12 mt-3">
			<div class="table-responsive">
			<table id="tbl-sosiokultural" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th width="" class="text-center">No</th>
						<th width="" class="text-center">Kompetensi Sosiokultural</th>
						<th width="" class="text-center">Level Standar</th>
						<th width="" class="text-center">Hasil Pengukuran</th>
						<th width="" class="text-center">Bentuk Pengembangan</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
			</div>
		</div>
	</div>

	<!-- ... content in page 1 ... -->
	<p style="page-break-after: always;">&nbsp;</p>
	<!-- <p style="page-break-before: always;">&nbsp;</p>
	... content in page 2 ... -->
	
	<!-- font-size: 24px -->
	<p class="mt-3 font-weight-normal text-justify" style="" contentEditable="true">
		Pusat Perencanaan dan Pengembangan SDM merekomendasikan program pengembangan kompetensi teknis, 
		manajerial, dan sosokultural, sebagaimana pada tabel di atas berdasarkan hasil pemetaan kompetensi tahun <span contentEditable="true"><%= new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %></span>. 
	</p>
	
	<div class="row" style="margin-top: 50px; margin-bottom: 100px">
		<div class="col-md-9"></div>
		<div class="col-md-3">
			<p class="mt-3 font-weight-normal"><!-- style="font-size: 24px" -->
				Mengetahui, 
			</p>
			<p class="font-weight-normal" style=" margin-top: 120px"><!-- font-size: 24px; -->
				<span contentEditable="true">Nama Lengkap dan Gelar</span><br/>
				NIP. <span contentEditable="true">123456 788 3345</span>
			</p>
		</div>
	</div>
	
<!-- </div> -->