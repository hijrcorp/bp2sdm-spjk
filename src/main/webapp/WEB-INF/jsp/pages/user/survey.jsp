<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
	<style>
		input[type="radio" i]:disabled {
		    background-color: #00ff00;
		}
	</style>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	
    <div class="container">
      <div class="row justify-content-center pt-5">
        <div class="col-md-9">
        <!-- <div class="card mt-3 mb-3"> -->
          <!-- <div class="card-header border mb-3 ">
	    	<div class="row">
			    <div class="col-6 col-md-6" id="pernyataan-title">Pernyataan : <strong>Aspek Keahlihan</strong></div>
			    <div id="head-pagination" class="col-6 col-md-6 text-right">
				    <button onclick="" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>
					<button onclick="" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>
				</div>
			</div>
			</div> -->
          <!-- <div class="card-body"> -->
			<div id="table-card-body">
				<div class="loading-row list-group-item text-center"><img width="20" src="${pageContext.request.contextPath}/img/loading-spinner.gif"/> Mohon Tunggu..</div>
			</div>
			
          <div id="info-answers" class="card-footer bg-light border fixed-bottom">
	    	<div class="row">
			    <div class="col-auto col-md-6" id="pernyataan-titles">Terjawab 0 dari 0 Pernyataan  </div>
			    <!-- <div class="col-6 text-right"><button onclick="" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>
				<button onclick="" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button></div> --></div>
			</div>
			<div id="container-btn-end" class="row mb-3 d-none">
				<div class="col col-md-auto"><button class="btn btn-success btn-block">Lanjutkan</button></div>
			</div>
          <!-- </div> -->
         <!--  <div id="table-card-footer" class="card-footer">
          </div> -->
        <!-- </div> -->
        </div>
      </div>

    </div>
	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div id="modal-content" class="modal-body">
	        ...
	      </div>
	      <div id="modal-action" class="modal-footer">
	        
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Maaf Waktu Anda habis.</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <p>Maaf waktu untuk menjawab pertanyaan dengan kode <strong id="kode_pertanyaan">X</strong> Telah Habis.</p>
	        Silahkan Lanjut Apabila anda telah siap.
	      </div>
	      <div class="modal-footer">
	        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
	        <button id="btn-mulai" type="button" class="btn btn-primary">Lanjut</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script src="${pageContext.request.contextPath}/js/general-survey-script.js?yes"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>