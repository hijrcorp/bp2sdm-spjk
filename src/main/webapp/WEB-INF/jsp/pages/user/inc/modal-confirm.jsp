<!-- Modal Feature-->
<div class="modal fade" data-backdrop="static" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-dialog">
		
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
				<div class="row">
					<div class="col-md-12">
						<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
					</div>
				</div>
			</div>
			
			<div class="modal-body">		
				<div  id="progressBar1"  class="row">
			      <div class="col-xs-12">
					<div class="progress">
					  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					    <span class="sr-only">45% Complete</span>
					  </div>
					</div>
			      </div>
			    </div>		
				<div class="form-group">
					<p>Apakah anda yakin akan menghapus data LHP ini?</p>
				</div>
				
				<div class="modal-footer">
					<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btnRemove" onclick="doRemove($(this).data('id'));" data-id="0" type="button" class="btn btn-primary">
					<span class="glyphicon glyphicon-trash"></span>  
					HAPUS</button>
				</div>
			</div>
		</div>
	</div>
</div>