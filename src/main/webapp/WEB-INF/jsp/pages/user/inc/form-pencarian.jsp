<!-- 
	<div class="form-group">
     <label class="col-md-2">Nomor LHP  </label>
     <div class="col-md-10">
		<input id="fNomorLhp" class="form-control input-sm" type="text" placeholder="">
     </div>
   	</div>
	   
	<div class="form-group">
     <label class="col-md-2">Tanggal LHP </label>
	  <div class="col-md-4">
		<input id="fTanggalLhp" class="form-control datepicker input-sm" type="text" placeholder="">
     </div>
     <label class="col-md-2">Tanggal Terima </label>
	  <div class="col-md-4">
    	<input id="fTanggalTerimaLhp" class="form-control datepicker input-sm" type="text" placeholder="">
     </div>
   	</div>
	   
	<div class="form-group">
     <label class="col-md-2">PKPT Tahun  </label>
     <div class="col-md-4">
		<select class="form-control input-sm" id="fPkpt">
			<option value="0">-- Pilih PKPT Tahun --</option>
		</select>
     </div>
     <label class="col-md-2">Satker/Auditi  </label>
     <div class="col-md-4">		      
		<select class="combobox form-control input-sm" id="fUnitAuditi">
			<option value="" selected="selected">-- Pilih Satker/Auditi --</option>
		</select>
   	</div>
   	</div>
	
	<div class="form-group">
	     <label class="col-md-2">Nama Kepala  </label>
	     <div class="col-md-4">
			<input id="fAuditiKepala" class="form-control input-sm" type="text" placeholder="">
	     </div>
	     <label class="col-md-2">NIP  </label>
	     <div class="col-md-4">
			<input id="fAuditiNip" class="form-control input-sm" type="text" placeholder="">
	     </div>
	</div>
	
	<div class="form-group">
	     <label class="col-md-2">Jenis Audit  </label>
	     <div class="col-md-4">
	     		<select class="form-control input-sm" id="fJenisAudit">
				<option value="0">-- Pilih Jenis Audit --</option>
			</select>
	     </div>
	     <label class="col-md-2">Instansi Pemeriksa  </label>
	     <div class="col-md-4">
			<select class="form-control input-sm" id="fInstansiPemeriksa">
				<option value="0">-- Pilih Instansi Pemeriksa --</option>
			</select>
	     </div>
	</div>
	
	<div class="form-group">
	     <label class="col-md-2">Eselon 1  </label>
	     <div class="col-md-4">		      
			<select class="hidden form-control input-sm" id="fFileBerkas">
				<option value="0">-- Pilih File Berkas --</option>
			</select>
			<select class="form-control input-sm" id="fEselon1">
				<option value="0">-- Pilih Eselon1 --</option>
			</select>
	     </div>
	     <label class="col-md-2">Nomor Surat Tugas  </label>
	     <div class="col-md-4">
			<input id="fNomorSurat" class="form-control input-sm" type="text" placeholder="">
	     </div>
	</div>
	
	<div class="form-group">
	     <label class="col-md-2">Tanggal  </label>
	     <div class="col-md-4">
     		<input id="fTanggalSurat" class="form-control datepicker input-sm" type="text" placeholder="" style="z-index:1151 !important;">
	     </div>
	     <label class="col-md-2">Lama Audit  </label>
	     <div class="col-md-4">
	     	<input id="fLamaAudit" class="form-control input-sm" type="text" placeholder="">
	     </div>
	</div>
	
	<div class="form-group">
	     <label class="col-md-2">Tanggal Pelaksanaan </label>
	     <div class="col-md-4">
	     	<input id="fTanggalAwal" type="text" class="datepicker form-control-lha-sm">
	     	<label class="control-label input-sm">s.d</label>
	     	<input id="fTanggalAkhir" type="text" class="datepicker form-control-lha-sm">
	     </div>
	     <label class="col-md-2">Periode Audit </label>
	     <div class="col-md-4">
	     	<select id="fPeriodeAwal" class="input-sm form-control-lha-sm">
				<option value="0">-- Pilih Periode Awal --</option>
	     	</select>
	     	<label class="control-label input-sm">s.d</label>
	     	<select id="fPeriodeAkhir" class="input-sm form-control-lha-sm">
				<option value="0">-- Pilih Periode Akhir --</option>
	     	</select>
	     </div>
	</div> -->
	
				
					<div class="form-group">
				      <label class="col-md-2">Nomor LHP  </label>
				      <div class="col-md-4">
							<input id="fNomorLhp" class="form-control input-sm" type="text" placeholder="">
				      </div>
				      <label class="col-md-2">Tanggal Terima </label>
					  <!-- <div class="col-md-4">
				      		<input id="fTanggalTerimaLhp" class="form-control datepicker input-sm" type="text" placeholder="">
				      </div> -->
				      
				      <div class="col-md-4">
				      	<input id="fTanggalTerimaLhp" type="text" class="datepicker form-control-lha-sm">
				      	<label class="control-label input-sm">s.d</label>
				      	<input id="fSdTanggalTerimaLhp" type="text" class="datepicker form-control-lha-sm">
				      </div>
				    </div>
				    
					<div class="form-group">
				      <label class="col-md-2">Tanggal LHP  </label>
					  <div class="col-md-4">
				      		<input id="fTanggalLhp" class="form-control datepicker input-sm" type="text" placeholder="">
				      </div>
				      <label class="col-md-2">PKPT Tahun  </label>
				      <div class="col-md-4">
							<select class="form-control input-sm" id="fPkpt">
								<option value="0">-- Pilih PKPT Tahun --</option>
							</select>
				      </div>
				    </div>
					
					<div class="form-group">
				      <label class="col-md-2">Satker/Auditi  </label>
				      <div class="col-md-4">		      
							<select class="combobox form-control input-sm" id="fUnitAuditi">
								<option value="" selected="selected">-- Pilih Satker/Auditi --</option>
							</select>
				      	</div>
				      <label class="col-md-2">Nama Kepala  </label>
				      <div class="col-md-4">
							<input id="fAuditiKepala" class="form-control input-sm" type="text" placeholder="">
				      </div>
					</div>
					
					<div class="form-group">
				      <label class="col-md-2">NIP  </label>
				      <div class="col-md-4">
							<input id="fAuditiNip" class="form-control input-sm" type="text" placeholder="">
				      </div>
				      <label class="col-md-2">Jenis Audit  </label>
				      <div class="col-md-4">
				      		<select class="form-control input-sm" id="fJenisAudit">
								<option value="0">-- Pilih Jenis Audit --</option>
							</select>
				      </div>
					</div>
					
					<div class="form-group">
				      <label class="col-md-2">Instansi Pemeriksa  </label>
				      <div class="col-md-4">
							<select class="form-control input-sm" id="fInstansiPemeriksa">
								<option value="0">-- Pilih Instansi Pemeriksa --</option>
							</select>
				      </div>
				      <label class="col-md-2">File Berkas  </label>
				      <div class="col-md-4">		      
							<select class="form-control input-sm" id="fFileBerkas">
								<option value="0">-- Pilih File Berkas --</option>
							</select>
				      </div>
					</div>
					
					<div class="form-group">
				      	<label class="col-md-2">Eselon1  </label>
				      	<div class="col-md-4">
							<select class="form-control input-sm" id="fEselon1">
								<option value="0">-- Pilih Eselon1 --</option>
							</select>
					 	</div>
				      <label class="col-md-2">Nomor Surat Tugas  </label>
				      <div class="col-md-4">
							<input id="fNomorSurat" class="form-control input-sm" type="text" placeholder="">
				      </div>
					</div>
					
					<div class="form-group">
				      <label class="col-md-2">Tanggal  </label>
				      <div class="col-md-4">
				      		<input id="fTanggalSurat" class="form-control datepicker input-sm" type="text" placeholder="" style="z-index:1151 !important;">
				      </div>
				      <label class="col-md-2">Lama Audit  </label>
				      <div class="col-md-4">
				      	<input id="fLamaAudit" class="form-control input-sm" type="text" placeholder="">
				      </div>
					</div>

					<div class="form-group">
				      <label class="col-md-2">Tanggal Pelaksanaan </label>
				      <div class="col-md-4">
				      	<input id="fTanggalAwal" type="text" class="datepicker form-control-lha-sm">
				      	<label class="control-label input-sm">s.d</label>
				      	<input id="fTanggalAkhir" type="text" class="datepicker form-control-lha-sm">
				      </div>
				      <label class="col-md-2">Periode Audit </label>
				      <div class="col-md-4">
				      	<select id="fPeriodeAwal" class="form-control-lha-sm input-sm">
								<option value="0">-- Tahun Awal --</option>
				      	</select>
				      	<label class="control-label input-sm">s.d</label>
				      	<select id="fPeriodeAkhir" class="form-control-lha-sm input-sm">
								<option value="0">-- Tahun Akhir --</option>
				      	</select>
				      </div>
					</div>