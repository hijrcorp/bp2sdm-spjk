    
  <!-- Modal Feature Begin -->
  <form id="frmVerifyThenDownload" class="form-horizontal">
    <div class="modal fade" data-backdrop="static" id="modalVerifyThenDownload" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Warning</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div class="col-md-12">
                <div id="verifyThenDownloadMsg"></div>
                <div>Silahkan lengkapi <a onclick="showUpdateProfile()" style="cursor: pointer;">disini</a>.</div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">CLOSE</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  
  <!-- Modal Feature Begin -->
  <form id="frmUpdateProfile" class="form-horizontal">
    <div class="modal fade" data-backdrop="static" id="modalUpdateProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Profile</h4>
          </div>
          <div class="modal-body">
            <div id="updateProfileProgress"  class="row">
              <div class="col-xs-12">
                <div class="progress">
                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    <span class="sr-only">45% Complete</span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-md-12">
                <div id="updateProfileMsg" class="alert alert-success" role="alert" hidden="hidden"></div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-md-6">
                <label>Nama Depan *</label>
                <input autocomplete="off" type="text" class="form-control" id="me_first_name" placeholder="Ketik nama depan anda">
              </div>
              <div class="col-md-6">
                <label>Nama Belakang *</label>
                <input autocomplete="off" type="text" class="form-control" id="me_last_name" placeholder="Ketik nama belakang anda">
              </div>
            </div>
  
            <div class="form-group">
              <div class="col-md-6">
                <label>ID Pengguna *</label>
                <input autocomplete="off" type="text" class="form-control" id="me_username" placeholder="Ketik ID login anda">
              </div>
              <div class="col-md-6">
                <label>Tipe *</label>
                <input autocomplete="off" type="text" class="form-control" id="me_position_name" readonly>
              </div>
            </div>
		    <div class="form-group">
		      <div class="col-md-12">
                <label>Email</label>
                <input autocomplete="off" type="text" class="form-control" id="me_email" placeholder="Ketik Alamat email anda">
		      </div>
		    </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">BATAL</button>
            <button id="btnUpdateProfile" type="submit" class="btn btn-primary">
              <span class="glyphicon glyphicon-floppy-disk"></span> SIMPAN
            </button>
          </div>
        </div>
      </div>
    </div>
  </form>
  
    <!-- Modal Feature Begin -->
	<form id="frmChangePassword" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modalChangePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Change your password</h4>
						
					</div>
					<div class="modal-body">
						<div  id="changePasswordProgress"  class="row">
					      <div class="col-xs-12">
							<div class="progress">
							  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							    <span class="sr-only">45% Complete</span>
							  </div>
							</div>
					      </div>
					    </div>
						<div class="form-group">
							<div class="col-md-12">
								<div id="changePasswordMsg" class="alert alert-success" role="alert" hidden="hidden"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
					      	<label>Current Password</label>
				    			<input type="password" class="form-control" name="oldPassword" id="changePasswordOld" placeholder="Type your current password">
					      </div>					     
						</div>		
						<div class="form-group">
							<div class="col-md-6">
					      	<label>Kata Kunci</label>
				    			<input type="password" class="form-control" name="newPassword" id="changePasswordNew" placeholder="New password">
					      </div>
					      
					      <div class="col-md-6">
					      	<label>Ulangi Kata Kunci</label>
				    			<input type="password" class="form-control" name="newPassword2" id="changePasswordNewRetype" placeholder="Retype new password">
					      </div>
						</div>
						
						
					</div>
					<div class="modal-footer">
							<button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btnChangePassword" type="submit" class="btn btn-primary">
						<span class="glyphicon glyphicon-floppy-disk"></span> 
						SIMPAN</button>
						</div>
				</div>
			</div>
		</div>
	</form>
	
	<div id="render-modal"></div>

	<footer class="footer d-none">
      <div class="container">
        <p class="text-muted text-center m-0 p-md-3 p-1">Copyright � 2019. Kementerian Lingkungan Hidup dan Kehutanan</p>
      </div>
    </footer>
    <c:choose>
         <c:when test = "${requestScope.infoPemberitahuan.status == 1}">
         	<footer class="sticky-footer fixed-bottom text-white marquee" style="opacity: 0.8;">
		    	<div class=" m-0 p-md-3 p-1" style="white-space:nowrap;overflow:initial"><span>${requestScope.infoPemberitahuan.value}</span></div>
		    </footer>
         </c:when>
   	</c:choose>
    
	

	<!-- Javascript -->
	<script src="${pageContext.request.contextPath}/js/libs/jquery-1.11.2.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>	
	<script src="${pageContext.request.contextPath}/js/libs/jquery.min.js"></script>	
	
	<!-- <script type="text/javascript" src="http://localhost:9910/kawasan/vendor/jquery/jquery.min.js"></script>
	 -->
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
	<!-- Sweetalert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    
    <!-- loading -->
	<script src="${pageContext.request.contextPath}/vendor/loadingoverlay.min.js"></script>
    
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap3-typeahead.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.cookie.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.number.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.md5.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-datetimepicker.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-combobox.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/select2.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.selectric.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/mprogress.min.js"></script>
	<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/base64.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
	<script src="${pageContext.request.contextPath}/js/common-script.js"></script>
	