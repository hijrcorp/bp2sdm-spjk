	<header class="navbar navbar-light bg-success bd-navbar fixed-top">
	<nav class="container flex-warp">
<!-- <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top "> -->
      <a class="navbar-brand text-white" href="${pageContext.request.contextPath}/">
        <img src="${pageContext.request.contextPath}/images/logo-menlhk.png" width="30" height="30" class="d-inline-block align-top" alt="">
        BP2SDM
      </a>
      <button style="display:block!important;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <!-- <a class="navbar-brand text-danger " role="button" data-toggle="popover" data-trigger="focus" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?"><i class="fas fa-power-off"></i></a>
 -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <strong>Selamat Datang</strong>,<br/> ${requestScope.realName}  
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
         <!--  <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div> -->
          <a class="dropdown-item" href="${pageContext.request.contextPath}/page/dashboard?mode=profile&"><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard</a>
          <a class="dropdown-item" href="${pageContext.request.contextPath}/login-logout"><i class="fas fa-power-off text-danger"></i> Logout/Keluar</a>
        </div>
      </li>
    </ul>
    <%-- <form class="form-inline my-2 my-lg-0 ">
      <a class="btn btn-outline-success my-2 my-sm-0" href="${pageContext.request.contextPath}/login-logout">Logout</a>
    </form> --%>
    </div>
    </nav>
    </header>