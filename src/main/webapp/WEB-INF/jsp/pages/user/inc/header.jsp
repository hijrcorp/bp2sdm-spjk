<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Sistem Pemetaan Jabatan Kompetensi - SPeKtra</title>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- CSS -->
<script>var ctx = "${pageContext.request.contextPath}"</script>
<script>var tokenCookieName = "${cookieName}"</script>
<script>
var position = "${position}";
//alert(position);

var disabled='';
if((position == 'Supervisior_View')){
	disabled='disabled';
}
</script>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link href="${pageContext.request.contextPath}/css/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-combobox.css">
<link href="${pageContext.request.contextPath}/css/sticky-footer-navbar.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/select2.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/common-style.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/mod-navbar.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/selectric.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/mprogress.min.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond./js/1.4.2/respond.min.js"></script>
        <![endif]-->

<!-- Favicon and touch icons -->
<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
<style>
	.font-weight-normal {
    		font-weight: 400!important;
	}
</style>
<link href="https://unpkg.com/nprogress@0.2.0/nprogress.css" rel="stylesheet">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/v4-shims.css">
<style>
.bg-warning {
    background-color: #ff9800!important;
}


	.example1 {
	 /* height: 50px;	 */
	 overflow: hidden;
	 /* position: relative; */
	}
	.example1 {
	 /* font-size: 3em; */
	 /* color: limegreen; */
	 /* position: absolute; */
	 width: 100%;
	 height: 100%;
	 margin: 0;
	 /* line-height: 50px; */
	 text-align: center;
	 /* Starting position */
	 -moz-transform:translateX(100%);
	 -webkit-transform:translateX(100%);	
	 transform:translateX(100%);
	 /* Apply animation to this element */	
	 -moz-animation: example1 15s linear infinite;
	 -webkit-animation: example1 15s linear infinite;
	 animation: example1 15s linear infinite;
	}
/* Move it (define the animation) */
@-moz-keyframes example1 {
 0%   { -moz-transform: translateX(50%); }
 50% { -moz-transform: translateX(-50%); }
}
@-webkit-keyframes example1 {
 0%   { -webkit-transform: translateX(50%); }
 50% { -webkit-transform: translateX(-50%); }
}
@keyframes example1 {
 0%   { 
 -moz-transform: translateX(100%); /* Firefox bug fix */
 -webkit-transform: translateX(100%); /* Firefox bug fix */
 transform: translateX(100%); 		
 }
 100% { 
 -moz-transform: translateX(-100%); /* Firefox bug fix */
 -webkit-transform: translateX(-100%); /* Firefox bug fix */
 transform: translateX(-100%); 
 }
}

.marquee {
  /* width: 450px; */
  margin: 0 auto;
  white-space: nowrap;
  overflow: hidden;
  box-sizing: border-box;
}

.marquee span {
  display: inline-block;
  padding-left: 100%;
  /* show the marquee just outside the paragraph */
  will-change: transform;
  animation: marquee 35s linear infinite;
}

.marquee span:hover {
  animation-play-state: paused
}


@keyframes marquee {
  0% { transform: translate(0, 0); }
  100% { transform: translate(-100%, 0); }
}


/* Respect user preferences about animations */

@media (prefers-reduced-motion: reduce) {
  .marquee { 
    white-space: normal 
  }
  .marquee span {
    animation-iteration-count: 1;
    animation-duration: 0.01; 
    /* instead of animation: none, so an animationend event is 
     * still available, if previously attached.
     */
    padding-left: 0;
  }
}
</style>