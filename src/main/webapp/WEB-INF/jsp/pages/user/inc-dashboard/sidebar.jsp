<%@page import="id.go.menlhk.bp2sdm.spjk.domain.Sesi"%>
<%@page import="id.go.menlhk.bp2sdm.spjk.core.QueryParameter"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="id.go.menlhk.bp2sdm.spjk.mapper.SesiMapper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%

ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
SesiMapper sesiMapper = appCtx.getBean(SesiMapper.class);
QueryParameter param = new QueryParameter();
param.setClause(param.getClause()+" AND active_sesi='"+1+"'");
String kode="";
if(sesiMapper.getCount(param)>0){
	kode = "kode-sesi="+sesiMapper.getList(param).get(0).getKode();
}
if(request.getParameter("kode-sesi")!=null) kode="kode-sesi="+request.getParameter("kode-sesi");
%>

<style>
.bordered-icon {
  text-shadow: -1px 0 #5E5E5E, 0 1px #5E5E5E, 1px 0 #5E5E5E, 0 -1px #5E5E5E;
}
.bordered-icon2 {
  text-shadow: 0 0 3px #5E5E5E;
}
</style>
<script> var roleList = "<c:out value='${roleList}'/>"; </script>

<% boolean isDropdown = false; %>

<c:set var="menu" scope="session">
  <nav class="navbar-sidebar">
    <ul class="list-unstyled navbar__list">
	<li class="has-sub">
	  	<a class="open font-weight-bold" href="${pageContext.request.contextPath}/page/dashboard?mode=profile&<%out.print(kode);%>"><i class="fas fa-fw fa-tachometer-alt"></i>Dashboard</a>
		<ul class="list-unstyled navbar__sub-list js-sub-list d-md-none d-block" style="">
			<li><a href="dashboard?mode=profile&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kode);else out.print(kode);%>"><i class="fas fa-fw fa-folder"></i> Profile</a></li>
			<li><a href="dashboard?mode=nilai-kompetensi&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kode);else out.print(kode);%>"><i class="fas fa-fw fa-folder"></i> Nilai Kompetensi</a></li>
			<li><a href="dashboard?mode=rekomendasi-pengembangan&<%if(request.getParameter("nip")!=null) out.print("nip="+request.getParameter("nip")+"&"+kode);else out.print(kode);%>"><i class="fas fa-fw fa-folder"></i> Rekomendasi Pengembangan</a></li>
	  	</ul>
	</li>
		
      <li class="has-sub">
        <a class="open font-weight-bold" href="${pageContext.request.contextPath}/"><i class="fas fa-fw fa-file-text"></i>Pemetaan</a>
      </li>
    </ul>
  </nav>
</c:set>


<c:set var="menuDesktop" scope="session" value="${menu}" />

<!-- MENU SIDEBAR-->
  <aside class="menu-sidebar d-none d-lg-block" style="width:265px">
    <div class="logo pl-0 pr-0" style="height: 117px">
      <%-- <a href="./">
        <img src="${assetPath}/images/icon/spip.png" alt="Logo" style="height: 55px; border-radius: 100%" />
      </a> --%>
      <div class="h5" style="float: right; margin-top: 4px;">
        <a href="?<%out.print(kode);%>" style="color: black; padding-left: 14px;">
          <span style="font-weight: bold; color: #516b73">PEMETAAN<br/>KOMPETENSI JABATAN<br/>FUNGSIONAL</span> <span style="margin-left: -2px;"></span>
        </a>
      </div>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">${menuDesktop}</div>
  </aside>
<!-- END MENU SIDEBAR-->

<c:set var="menuMobile" scope="session" value="${menu}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'navbar-sidebar', 'navbar-mobile')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__list', 'navbar-mobile__list list-unstyled')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__sub-list js-sub-list', 'navbar-mobile-sub__list list-unstyled js-sub-list')}" />

<!-- HEADER MOBILE-->
  <header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
      <div class="container-fluid">
        <div class="header-mobile-inner">
          <%-- <a class="logo" href="index"><img src="${assetPath}/images/icon/spip.png" style="height: 60px; border-radius: 100%" alt="Logo" /></a> --%>
          <button class="hamburger hamburger--slider" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
        </div>
      </div>
    </div>
    ${menuMobile}
  </header>
<!-- END HEADER MOBILE-->

<style>
@media (max-width: 991px) {
    .header-desktop {
        height: 75px;
    }
}
</style>
