<!DOCTYPE html>
<html lang="en">

<%@ include file = "./head.jsp" %>
<style>
.menu-sidebar__content {
  height: 90% !important;
}
.section__content--p30 {
    padding: 0 0px!important;
}
body{
	/* zoom: 85%; */
}
@page {
	size : auto;
}
@media (min-width: 992px){
	.pl-lg-default, .px-lg-default {
	    padding-left: 265px!important;
	}
	.left-lg-default {
	    left: 265px!important;
	}
	.width-lg-default {
	    width: 265px!important;
	}
}


@media print {.bg-danger tr td  {
    color: white !important;
    background-color: #dc3545!important;
}}

@media print {tr.bg-secondary td {
    color: white !important;
    background-color: #6c757d!important;
}}
@media print {tr.bg-danger td {
    color: white !important;
    background-color: #dc3545!important;
}}

@media print {.bg-danger tr  {
    color: white !important;
    background-color: #dc3545!important;
}}
@media print {tr td.bg-danger  {
    color: white !important;
    background-color: #dc3545!important;
}}
@media print {.bg-secondary th  {
    color: white !important;
    background-color: #6c757d!important;
}}
@media print {td.bg-secondary  {
    color: white !important;
    background-color: #6c757d!important;
}}
@media print {th.bg-secondary  {
    color: white !important;
    background-color: #6c757d!important;
}}
.text-verti  {
	writing-mode: tb-rl;
   	transform: rotateZ(180deg);
   	height: min-content;
}

@media print {.text-verti  {
	writing-mode: none;
   	transform: none;
   	height: min-content;
}}
@media print {h5.secondary-color  {
    color: #91a9b3 !important;
}}
@media print {.table-striped thead tr th, .table-striped tfoot tr th {
    background: #84ACB6 !important;
    color: white;
}}
</style>

<body class="animsition-not" >
  <div class="page-wrapper">
  
    <%-- <%@ include file = "./sidebar.jsp" % --%>
  
    <%@ include file = "./sidebar-with-navbar.jsp" %>

    <div class="page-container pl-lg-default">
    

    
      <!-- HEADER DESKTOP-->
  <header class="header-desktop d-none d-sm-block left-lg-default" style="background-color: #597D88;">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="header-wrap">
          <div class="account-wrap">
            <div class="account-item clearfix js-item-menu">
              <div class="image">
                <%-- <img src="${userPicture}" data-src="${userPicture}" alt="${requestScope.realName}" />
                <img src="${contextPath}/images/logo-menlhk.png" data-src="${assetPath}/images/icon/user.png" alt="${requestScope.realName}" /> --%>
              	<img src="${pageContext.request.contextPath}/images/logo-menlhk.png" width="30" height="30" class="d-inline-block align-top" alt="">
              </div>
              <div class="content">
                <a class="js-acc-btn" href="#" style="color:white">${requestScope.realName}</a>
              </div>
              <div class="account-dropdown js-dropdown">
                <div class="info clearfix">
                  <div class="image">
                    <a href="#">
                    	<%-- <img src="${userPicture}" data-src="${userPicture}" alt="${requestScope.realName}" /> --%>
                      	<img src="${assetPath}/images/icon/user.png" data-src="${assetPath}/images/icon/user.png" alt="${requestScope.realName}" />
                    </a>
                  </div>
                  <div class="content">
                    <h5 class="name"><a href="#">${requestScope.realName}</a></h5>
                    <span class="email">${requestScope.organization}</span>
                  </div>
                </div>
                <div class="account-dropdown__body">
                  <div class="account-dropdown__item">
                    <a href="${contextPath}/login-password"><i class="zmdi zmdi-hc-fw zmdi-key"></i>Ubah Password</a>
                  </div>
                  <div class="account-dropdown__item">
                    <a onclick="showListSesi()" href="javascript:void(0)"><i class="zmdi zmdi-settings"></i>Sesi</a>
                  </div>
                </div>
                <div class="account-dropdown__footer">
                  <a href="${contextPath}/login-logout"><i class="zmdi zmdi-hc-fw zmdi-power"></i>Logout</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header> 
<!-- HEADER DESKTOP-->
    <%--   <%@ include file = "./navbar.jsp" %> --%>
      