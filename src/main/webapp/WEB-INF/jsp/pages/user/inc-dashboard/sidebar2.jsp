<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script> var roleList = "<c:out value='${roleList}'/>"; </script>

<% boolean isDropdown = false; %>

<c:set var="menu" scope="session">
  <nav class="navbar-sidebar">
    <ul class="list-unstyled navbar__list">
      <!-- <li><a href="index"><i class="fas fa-fw fa-tachometer-alt"></i>Dashboard</a></li> -->
      <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
        <li class="has-sub">
          <a class="<%=(isDropdown?"js-arrow":"open")%> font-weight-bold" href="#" onclick="return false;"><i class="fas fa-fw fa-tachometer-alt"></i>Data Utama </a>
          <ul class="list-unstyled navbar__sub-list js-sub-list"<%=(isDropdown?"":" style=\"display: block;\"")%>>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN')}">
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}"> <!--  !fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN') -->
              <li><a href="kandang"><i class="fas fa-fw fa-cube"></i> Master Kandang</a></li>
              <li><a href="inventori?menu=Inventaris Satwa"><i class="fas fa-fw fa-paw"></i> Inventaris Satwa</a></li>
              <!-- <li><a href="cctv"><i class="fas fa-fw fa-video"></i> Kamera CCTV</a></li> -->
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN')}">
              <!-- <li><a href="inventori?menu=Inventaris Satwa"><i class="fas fa-fw fa-paw"></i> Inventaris Satwa</a></li> -->
              <li><a href="pengguna"><i class="fas fa-fw fa-user"></i> Akun Pengguna</a></li>
            </c:if>
          </ul>
        </li>
      </c:if>
      <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
        <li class="has-sub">
          <a class="<%=(isDropdown?"js-arrow":"open")%> font-weight-bold" href="#" onclick="return false;"><i class="fas fa-fw fa-star"></i>Menu Pelayanan</a>
          <ul class="list-unstyled navbar__sub-list js-sub-list"<%=(isDropdown?"":" style=\"display: block;\"")%>>
            <%-- <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') && !fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN')}"> --%>
            <!-- <li><a href="dokter"><i class="fas fa-fw fa-medkit"></i> Kesehatan Satwa</a></li> -->
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN')}">
              <li><a href="penerimaan?menu=Daftar Penerimaan"><i class="fas fa-fw fa-sign-in-alt"></i> Penerimaan Satwa</a></li>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
              <li><a href="pengeluaran?menu=Daftar Pengeluaran&type=PERMOHONAN"><i class="fas fa-fw fa-sign-out-alt"></i> Pengeluaran Satwa</a></li>
            </c:if>
            <%-- <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
              <li><a href="form-penerimaan?menu=Form Penerimaan"><i class="fas fa-fw fa-sign-in-alt rotate-90s"></i> Tambah Penerimaan</a></li>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
              <li><a href="form-pengeluaran?menu=Form Pengeluaran&type=PERMOHONAN"><i class="fas fa-fw fa-sign-out-alt rotate-270s"></i> Tambah Pengeluaran</a></li>
            </c:if> --%>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU')}">
              <li><a href="pengeluaran?proses=VERIFIKASI&menu=Verifikasi Pengeluaran&type=PERMOHONAN"><i class="fas fa-fw fa-calendar-check"></i> Verifikasi Pengeluaran</a></li>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
              <li><a href="pengeluaran?proses=PERSETUJUAN&menu=Persetujuan Pengeluaran&type=PERMOHONAN" style="font-size: 95%;"><i class="fas fa-fw fa-thumbs-up"></i> Persetujuan Pengeluaran</a></li>
            </c:if>
          </ul>
        </li>
      </c:if>
      <%-- <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
        <li class="has-sub">
          <a class="<%=(isDropdown?"js-arrow":"open")%> font-weight-bold" href="#" onclick="return false;" style="font-size: 95%;"><i class="fas fa-fw fa-tasks"></i>Menu Pengeluaran Satwa</a>
          <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: block;">
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
              <li><a href="pengeluaran?menu=Daftar Pengeluaran&type=PERMOHONAN"><i class="fas fa-fw fa-sign-out-alt"></i> Pengeluaran Satwa</a></li>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU')}">
              <li><a href="pengeluaran?proses=VERIFIKASI&menu=Verifikasi Pengeluaran&type=PERMOHONAN"><i class="fas fa-fw fa-calendar-check"></i> Verifikasi Pengeluaran</a></li>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
              <li><a href="pengeluaran?proses=PERSETUJUAN&menu=Persetujuan Pengeluaran&type=PERMOHONAN" style="font-size: 95%;"><i class="fas fa-fw fa-thumbs-up"></i> Persetujuan Pengeluaran</a></li>
            </c:if>
          </ul>
        </li>
      </c:if> --%>
      <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEEPER') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_DOKTER')}">
        <li class="has-sub">
          <a class="<%=(isDropdown?"js-arrow":"open")%> font-weight-bold" href="#" onclick="return false;"><i class="fas fa-fw fa-tasks"></i>Catatan Perawatan</a>
          <ul class="list-unstyled navbar__sub-list js-sub-list"<%=(isDropdown?"":" style=\"display: block;\"")%>>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEEPER')}">
              <li><a href="keeper"><i class="fas fa-fw fa-book"></i> Harian Keeper</a></li>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_DOKTER')}">
              <li><a href="dokter"><i class="fas fa-fw fa-medkit"></i> Harian Dokter</a></li>
            </c:if>
          </ul>
        </li>
      </c:if>
      <li class="has-sub">
        <a class="<%=(isDropdown?"js-arrow":"open")%> font-weight-bold" href="#" onclick="return false;"><i class="fas fa-fw fa-file-text"></i>Laporan</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list"<%=(isDropdown?"":" style=\"display: block;\"")%>>
          <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEEPER') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_DOKTER')}">
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEEPER')}">
              <li><a href="keeper?laporan&menu=Laporan Harian Keeper"><i class="fas fa-fw fa-file"></i> Harian Keeper</a></li>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_DOKTER')}">
              <li><a href="dokter?laporan&menu=Laporan Harian Dokter"><i class="fas fa-fw fa-file"></i> Harian Dokter</a></li>
            </c:if>
          </c:if>
          <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
            <li><a href="penerimaan?laporan&menu=Laporan Penerimaan Satwa"><i class="fas fa-fw fa-file"></i> Penerimaan Satwa</a></li>
            <li><a href="pengeluaran?laporan&menu=Laporan Pengeluaran Sastwa&type=PENYERAHAN"><i class="fas fa-fw fa-file"></i> Pengeluaran Satwa</a></li>
          </c:if>
        </ul>
      </li>
      <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN')}">
        <li class="has-sub">
          <a class="<%=(isDropdown?"js-arrow":"open")%> font-weight-bold" href="#" onclick="return false;"><i class="fas fa-fw fa-indent"></i>Data Pendukung</a>
          <ul class="list-unstyled navbar__sub-list js-sub-list"<%=(isDropdown?"":" style=\"display: block;\"")%>>
            <li><a href="lokasi"><i class="fas fa-fw fa-map-marker-alt"></i> Lokasi Pengeluaran</a></li>
            <li><a href="disposal"><i class="fas fa-fw fa-sign-in-alt"></i> Jenis Pengeluaran</a></li>
            <li><a href="pekerjaan"><i class="fas fa-fw fa-cog"></i> Daftar Pekerjaan</a></li>
            <li><a href="satuan"><i class="fas fa-fw fa-weight"></i> Satuan Ukuran</a></li>
            <li><a href="taksa"><i class="fas fa-fw fa-tags"></i> Taksa Satwa</a></li>
            <li><a href="sumber"><i class="fas fa-fw fa-info"></i> Sumber Satwa</a></li>
            <li><a href="satwa"><i class="fas fa-fw fa-bug"></i> Jenis Satwa</a></li>
          </ul>
        </li>
      </c:if>
      <!-- <li><a href="rating"><i class="fas fa-fw fa-star"></i>Rating</a></li> -->
      <!-- <hr/>
      <li><a href="z-table"><i class="fas fa-fw fa-table"></i>Tables</a></li>
      <li><a href="z-form"><i class="far fa-check-square"></i>Forms</a></li>
      <li><a href="#"><i class="fas fa-fw fa-calendar-alt"></i>Calendar</a></li>
      <li><a href="z-map"><i class="fas fa-fw fa-map-marker-alt"></i>Maps</a></li>
      <li class="has-sub">
        <a class="js-arrow" href="#"><i class="fas fa-fw fa-copy"></i>Pages</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list">
          <li><a href="z-login">Login</a></li>
          <li><a href="z-register">Register</a></li>
          <li><a href="z-forget-pass">Forget Password</a></li>
        </ul>
      </li>
      <li class="has-sub">
        <a class="js-arrow" href="#"><i class="fas fa-fw fa-desktop"></i>UI Elements</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: block;">
          <li><a href="z-button">Button</a></li>
          <li><a href="z-badge">Badges</a></li>
          <li><a href="z-tab">Tabs</a></li>
          <li><a href="z-card">Cards</a></li>
          <li><a href="z-alert">Alerts</a></li>
          <li><a href="z-progress-bar">Progress Bars</a></li>
          <li><a href="z-modal">Modals</a></li>
          <li><a href="z-switch">Switchs</a></li>
          <li><a href="z-grid">Grids</a></li>
          <li><a href="z-fontawesome">Fontawesome Icon</a></li>
          <li><a href="z-typo">Typography</a></li>
        </ul>
      </li> -->
    </ul>
  </nav>
</c:set>




<c:set var="menuDesktop" scope="session" value="${menu}" />

<!-- MENU SIDEBAR-->
  <aside class="menu-sidebar d-none d-lg-block">
    <div class="logo" style="padding-right: 0px; padding-left: 30px;">
      <a href="./">
        <%-- <img src="${assetPath}/images/icon/pps-indonesia-logo.png" alt="Logo" style="height: 60px" /> --%>
        <img src="${assetPath}/images/icon/tegal-alur.png" alt="Logo" style="height: 60px" />
      </a>
      <!-- <div style="float: right; font-size: 25px; margin-top: 4px;">
        <a href="index" style="color: black; padding-left: 9px;">
          <span style="font-weight: bold;">PPS</span> <span style="margin-left: -2px;">INDONESIA</span>
        </a>
      </div> -->
      <div style="float: right; font-size: 30px; margin-top: 4px;">
        <a href="./" style="color: black; padding-left: 14px;">
          <span style="font-weight: bold;">PPS</span> <span style="margin-left: -2px;">ONLINE</span>
        </a>
      </div>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">${menuDesktop}</div>
  </aside>
<!-- END MENU SIDEBAR-->

<c:set var="menuMobile" scope="session" value="${menu}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'navbar-sidebar', 'navbar-mobile')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__list', 'navbar-mobile__list list-unstyled')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__sub-list js-sub-list', 'navbar-mobile-sub__list list-unstyled js-sub-list')}" />
<!-- HEADER MOBILE-->
  <header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
      <div class="container-fluid">
        <div class="header-mobile-inner">
          <a class="logo" href="index"><img src="${assetPath}/images/icon/logo.png" alt="Logo" /></a>
          <button class="hamburger hamburger--slider" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
        </div>
      </div>
    </div>
    ${menuMobile}
  </header>
<!-- END HEADER MOBILE-->

<style>
@media (max-width: 991px) {
    .header-desktop {
        height: 75px;
    }
}
</style>

<%--@ include file = "./menu-mobile.jsp" --%>
<%--@ include file = "./menu-desktop.jsp" --%>