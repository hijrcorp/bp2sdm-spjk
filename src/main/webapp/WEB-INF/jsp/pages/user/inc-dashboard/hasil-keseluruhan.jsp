<% if(request.getParameter("mode") ==null) { %>
      <div class="row">
        <div class="col-md-12">
          <div class="overview-wrap">
            <h3 class="" style="text-transform: capitalize;font-weight: 400;">Hasil Keseluruhan</h3>
          </div>
        </div>
      </div>
<% } %>
      <div id="list-kelompok" class="row mt-3">
      	<% for(int i = 0 ; i < 5 ; i++) { %>
        <div class="col">  
		<% if(request.getParameter("mode") !=null) {
				if(request.getParameter("mode").equals("jabatan") || request.getParameter("mode").equals("satker")) {
		        	if(request.getParameter("id_kelompok")!=null) { %>
		          		<div id="progress-id-kelompok-<%=i%>" data-id="" class="overview-item overview-item--c1" onclick="window.location.href=window.location.search.split('&')[0]+'&id_kelompok='+$('#progress-id-kelompok-<%=i%>').data('id')+'&kode-sesi='+'<% out.print(request.getParameter("kode-sesi")); %>'" style="background: white; padding: 15px">
		        <%	}else{ %>
		        		<%-- <div id="progress-id-kelompok-<%=i%>" data-id="" class="overview-item overview-item--c1" onclick="window.location.href=window.location.search.split('&')[0]+'&id_kelompok='+$('#progress-id-kelompok-<%=i%>').data('id')+'&'+window.location.search.split('&')[1]" style="background: white; padding: 15px"> --%>
		        		<div id="progress-id-kelompok-<%=i%>" data-id="" class="overview-item overview-item--c1" onclick="setKelompokJabatan(this)" style="background: white; padding: 15px">
		        	<%}
		        } 
	       }else{ %>
        	<div id="progress-id-kelompok-<%=i%>" data-id="" class="overview-item overview-item--c1"  style="background: white; padding: 15px">
        <% } %>
          	<div class="row">
	            <div class="col-6">
	              <!-- <div class="overview-boxs clearfix"> -->
	                <div class="text">
	                  <h4 class="text-secondary" id="progress-nama-kode-kelompok-berjalan-<%=i%>"></h4>
	                  <span class="text-dark">Responden</span>
	                  <h4 class="text-dark"><span style="color: red" id="julah-peserta-berjalan-<%=i%>"></span>/<span id="total-peserta-<%=i%>"></span></h4>
	                </div>
	              <!-- </div> -->
	            </div>
	            <div class="col-6">
	              <div class="overview-boxs clearfix">
	                <div style="height: 100px !important;" class="progress-chart-<%=i%>" style=""> <!-- zoom: 1.1764705882352942 -->
	                  <canvas id="progress-chart-<%=i%>" width="100" height="50" class="progress-chart-<%=i%>"></canvas>
	                </div>
	              </div>
	            </div>
	          </div>
            <div class="overview-boxs clearfix">
            <div class="text text-center" style="">
              <!-- <div class="text text-center" style="padding: 0px 0 10px 0; padding-left: 20px !important; margin-top: 0px"> -->
                <h5 class="text-dark"><a href="#">Lihat Hasil</a></h5>
              </div>
            </div>
          </div>
        </div>
        <% } %>
      </div>