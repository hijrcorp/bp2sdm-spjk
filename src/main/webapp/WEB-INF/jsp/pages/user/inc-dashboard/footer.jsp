
</div>
  </div>
  
    <!-- Scroll to Top Button-->
    <!-- <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a> -->

    <!-- Confirmation Modal-->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-confirm-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
            <button class="btn btn-primary" id="btn-modal-confirm-yes" type="button">Yes</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Alert Modal-->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-success">
            <h5 class="modal-title" id="exampleModalLabel">Alert</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-alert-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Error Modal-->
    <div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Error</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-error-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="${contextPath}/vendor/jquery/jquery.min.js"></script>
    <script src="${contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="${contextPath}/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="${contextPath}/vendor/jquery.disableAutoFill.min.js"></script>
    <script src="${contextPath}/vendor/js.cookie.js"></script>
    <script src="${contextPath}/vendor/bootstrap-notify.min.js"></script>

    <!-- Page level plugin JavaScript-->
     <script src="${contextPath}/vendor/moment.min.js"></script>
    <%-- <script src="${contextPath}/vendor/chart.js/Chart.min.js"></script> --%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js" integrity="sha512-hZf9Qhp3rlDJBvAKvmiG+goaaKRZA6LKUO35oK6EsM0/kjPK32Yw7URqrq3Q+Nvbbt8Usss+IekL7CRn83dYmw==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw==" crossorigin="anonymous"></script>
   
    <script src="${contextPath}/vendor/daterangepicker.js"></script>
    <script src="${contextPath}/vendor/select2.min.js"></script>
    <script src="${contextPath}/vendor/ekko-lightbox.min.js"></script>
    <script src="${contextPath}/vendor/loadingoverlay.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
  
    <!-- DateTime Picker-->
    <%-- <script src="${assetPath}/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script> --%>
    <script src="${assetPath}/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    
    <!-- Kebutuhan dari common.js -->
    <%-- <script src="${contextPath}/vendor/remodal/remodal.js"></script>
    <script src="${contextPath}/vendor/bootstrap-input-spinner-master/bootstrap-input-spinner.js"></script> --%>
	
	<!-- Custom scripts for all pages-->
	<script src="${contextPathdashboard}/js/common.js"></script>
	<script src="${assetPath}/js/common.js"></script>
  
  
    <!-- Vendor JS -->
    <script src="${assetPath}/vendor/slick/slick.min.js">
    </script>
    <script src="${assetPath}/vendor/wow/wow.min.js"></script>
    <%-- <script src="${assetPath}/vendor/animsition/animsition.min.js"></script> --%>
    <script src="${assetPath}/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="${assetPath}/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="${assetPath}/vendor/counter-up/jquery.counterup.min.js"></script>
    <script src="${assetPath}/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="${assetPath}/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="${assetPath}/vendor/chartjs/Chart.bundle.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <!-- Main JS-->
    <script src="${assetPath}/js/main.js"></script>
    <%-- <script src="${assetPath}/js/admin-script.js"></script> --%>
  
	<script>
		function showListSesi(){
			$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
			ajaxGET("${contextPath}" + '/sesi/list'+"?limit=1000000&",function(response){
		    	var myvar = '<div id="list-sel-sesi" class="list-group">';
		    	$.each(response.data, function(key){
		    		if(window.location.search.split('&').length==1 && window.location.search.split('&')[0]!=""){
		    			myvar+='  <a href="'+"?"+'kode-sesi='+this.kode+'" class="list-group-item list-group-item-action text-left '+(this.active==1?"bg-success text-white":"")+'"><strong>'+this.kode+'('+this.tahun+')</strong><br/>'+this.periode_awal_serialize+' <strong>s.d</strong> '+this.periode_akhir_serialize+'</a>';
		    		}else if(window.location.search.split('&').length==1 && window.location.search.split('&')[0]==""){
		    			myvar+='  <a href="'+window.location.search.split('&')[0]+'?kode-sesi='+this.kode+'" class="list-group-item list-group-item-action text-left '+(this.active==1?"bg-success text-white":"")+'"><strong>'+this.kode+'('+this.tahun+')</strong><br/>'+this.periode_awal_serialize+' <strong>s.d</strong> '+this.periode_akhir_serialize+'</a>';
		    		}else if(window.location.search.split('&').length==3){
		    			/* if($.urlParam('nip')!=null){
		    				
		    			}else if($.urlParam('id_kelompok')!=null){
		    				
		    			} */
		    			myvar+='  <a href="'+window.location.search.split('&').splice(0, 2).toString().replaceAll(",","&")+'&kode-sesi='+this.kode+'" class="list-group-item list-group-item-action text-left '+(this.active==1?"bg-success text-white":"")+'"><strong>'+this.kode+'('+this.tahun+')</strong><br/>'+this.periode_awal_serialize+' <strong>s.d</strong> '+this.periode_akhir_serialize+'</a>';
		    		
		    		}else myvar+='  <a href="'+window.location.search.split('&')[0]+'&kode-sesi='+this.kode+'" class="list-group-item list-group-item-action text-left '+(this.active==1?"bg-success text-white":"")+'"><strong>'+this.kode+'('+this.tahun+')</strong><br/>'+this.periode_awal_serialize+' <strong>s.d</strong> '+this.periode_akhir_serialize+'</a>';
				});
		    	//
		    	//window.location.href=
		    	//window.location.search.split('&')[0]+'&kode-sesi='+$('#progress-id-kelompok-').data('id')
		    	myvar+='</div>';
		    	Swal.queue([{
				  title: 'Pilih Sesi',
				  showConfirmButton: false,
				  allowOutsideClick: false,
				  //confirmButtonText: 'Show my public IP',
				  html: myvar,
				  /* preConfirm: () => {
				    return fetch(ipAPI)
				      .then(response => response.json())
				      .then(data => Swal.insertQueueStep(data.ip))
				      .catch(() => {
				        Swal.insertQueueStep({
				          icon: 'error',
				          title: 'Unable to get your public IP'
				        })
				      }) 
				  } */
				}])
				
				$.LoadingOverlay("hide");
				
				if($.urlParam('kode-sesi')==null){
					window.location=$('#list-sel-sesi').find('.bg-success')[0].href;
				}
			});
		}
		//document.body.style.zoom = "80%";
	</script>
</body>

</html>
<!-- end document-->