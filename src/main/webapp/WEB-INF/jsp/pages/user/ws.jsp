<!DOCTYPE html>
<html>
   <head>
      <title>Real-Time Chat</title>
      <!-- Include SockJS library from CDN -->
      <script src="https://cdn.jsdelivr.net/npm/sockjs-client@1.5.1/dist/sockjs.min.js"></script>
      <!-- Include STOMP.js library from CDN -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
      <script>
         var stompClient = null;
         function connect() {
	         var socket = new SockJS('/spektra/exam-updates');
	         stompClient = Stomp.over(socket);
	         stompClient.connect({}, function (frame) {
		         console.log('Connected: ' + frame);
		         stompClient.subscribe('/topic/chat', function (message) {
		         	showMessage(JSON.parse(message.body).content);
		         });
	         });
         }
         
         function disconnect() {
	         if (stompClient !== null) {
	         	stompClient.disconnect();
	         }
	         console.log("Disconnected");
         }
         
         function sendMessage() {
	         var message = document.getElementById('message').value;
	         stompClient.send("/app/chat", {}, JSON.stringify({'content': message}));
         }
         function showMessage(message) {
	         var chatDiv = document.getElementById('chat');
	         var p = document.createElement('p');
	         p.appendChild(document.createTextNode(message));
	         chatDiv.appendChild(p);
         }
      </script>
   </head>
   <body onload="connect()">
      <h1>Real-Time Chat</h1>
      <div id="chat"></div>
      <input type="text" id="message" placeholder="Type your message" />
      <button onclick="sendMessage()">Send</button>
      <button onclick="disconnect()">Disconnect</button>
   </body>
</html>