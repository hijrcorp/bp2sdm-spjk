
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="java.util.Map"%>
<%@include file="inc/redirect.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
	<script>
	function init(){}
	var user_id = "<%= request.getAttribute("userId") %>";
	var sesi_id = "${requestScope.idSesi}";
	var responden_id = "${requestScope.idResponden}";
	</script>
	<style>
	.swal2-content{text-align:left!important}
	</style>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>

    <div class="container ">
      <div class="row justify-content-center pt-5">
        <div class="col-md-9">
       <c:choose>
         <c:when test = "${requestScope.statusUnitKompResponden == 'TEKNIS_INTI' || requestScope.statusUnitKompResponden == 'TEKNIS_PILIHAN'}">
         	<c:redirect url="/page/main-unit-kompetensi?mode=teknis"/>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'MANAJERIAL'}">
         	<c:redirect url="/page/main-unit-kompetensi?mode=manajerial"/>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'SOSIOKULTURAL'}">
         	<c:redirect url="/page/main-unit-kompetensi?mode=sosiokultural"/>
         </c:when>
         <c:when test = "${requestScope.statusResponden == 'PROSES'}">
         	<c:redirect url="/page/simulasi-ujian"/>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden==null && requestScope.statusResponden == 'SELESAI'}">
	        <div class="card mb-3">
			    <div id="msg-survey" class="card-body"><h4>Terimakasih Atas Partisipasi Anda.</h4></div>
			</div>
         </c:when>
         <c:when test = "${requestScope.statusResponden == 'NO_SESI'}">
	        <div class="card mb-3">
			    <div id="msg-survey" class="card-body"><h4>Tidak ada kegiatan <strong>ujian</strong>, atau mohon laporkan admin jika anda merasa sedang ada kegiatan ujian, </br> Terima Kasih</h4></div>
			</div>
			<p class="mt-5 h5">Quick Access</p>
			<div class="card border-0">
			    <!-- <button class=" btn btn-info btn-lg btn-block">PEMETAAAN</button> -->
			    <a class="btn btn-info btn-lg btn-block" href="${pageContext.request.contextPath}/page/dashboard?mode=profile&"><i class="fas fa-fw fa-tachometer-alt"></i> DASHBOARD PEMETAAN</a>
			</div>
         </c:when>
         <c:when test = "${requestScope.statusResponden == 'NO_SESI_JABATAN'}">
	        <div class="card mb-3">
			    <div id="msg-survey" class="card-body"><h5>Sistem tidak dapat menemukan user anda sebagai peserta ujian, <br/><br/> Terima Kasih</h5></div>
			</div>
			
			<p class="mt-5 h5">Quick Access</p>
			<div class="card ">
			    <!-- <button class=" btn btn-info btn-lg btn-block">PEMETAAAN</button> -->
			    <a class="btn btn-info btn-lg btn-block" href="${pageContext.request.contextPath}/page/dashboard?mode=profile&"><i class="fas fa-fw fa-tachometer-alt"></i> DASHBOARD PEMETAAN</a>
			</div>
         </c:when>
         <c:when test = "${requestScope.statusResponden == 'NO_SIMPEG'}">
	        <div class="card mb-3">
			    <div id="msg-survey" class="card-body"><h5>Mohon Maaf, server tidak mendeteksi NIP anda sebagai responden atau Server(SIMPEG) sedang maintenance mohon Menunggu beberapa saat, <br/><br/> Terima Kasih</h5></div>
			</div>
			
			<p class="mt-5 h5">Quick Access</p>
			<div class="card ">
			    <!-- <button class=" btn btn-info btn-lg btn-block">PEMETAAAN</button> -->
			    <a class="btn btn-info btn-lg btn-block" href="${pageContext.request.contextPath}/page/dashboard?mode=profile&"><i class="fas fa-fw fa-tachometer-alt"></i> DASHBOARD PEMETAAN</a>
			</div>
         </c:when>
         <c:when test = "${requestScope.browser.name != 'Chrome'}">
	        <div class="card mb-3">
			    <div class="card-body"><h4 class="text-justify">Sistem mendeteksi anda menggunakan browser selain <strong>chrome</strong>, Mohon pastikan anda telah menggunakan google chrome sebagai browser/peramban anda, untuk dapat mengakses <span class="text-success">SPeKtra</span>.<br/></br> <strong>Terima Kasih</strong></h4></div>
			</div>
         </c:when>
         
         <c:otherwise>
			<form id="form-entri">
		    <div class="card ">
			  <div class="card-header">
				<h6 class="card-title p-0 m-0">Informasi Profil Pegawai</h6>
			  </div>
			  <div class="card-body">
			    <!-- <h5 class="card-title">Special title treatment</h5>
			    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
				  <div class="form-group row">
				    <label for="" class="col-sm-2 col-form-label text-md-right">NIP</label>
				    <div class="col-sm-6">
				      <input name="nip" type="text" class="form-control" value="${requestScope.nip}" readonly>
				    </div>
				  </div>
				  <div class="form-group row">
				    <label for="" class="col-sm-2 col-form-label text-md-right">Nama</label>
				    <div class="col-sm-6">
				      <input name="nama" type="text" class="form-control" value="${requestScope.nama}" readonly>
				    </div>
				    <div class="col-sm-2">
			      		<c:choose>
				         <c:when test = "${requestScope.idEselon1 == 100 or requestScope.golongan eq null or requestScope.golongan eq ''}">
				         	<select name="golongan" class="form-control">
								<option value="">Pilih Gol</option>
				         	</select>
				         </c:when>
				         <c:otherwise>
				         	<input name="golongan" type="text" class="form-control" value="${requestScope.golongan}" readonly>
				         </c:otherwise>
				         </c:choose>
				    </div>
				  </div>
				  <div class="form-group row">
				    <label for="" class="col-sm-2 col-form-label text-md-right">Eselon1</label>
				    <div class="col-sm-8">
				      <select name="id_eselon1" class="form-control" onchange="getSatker()">
				      <c:choose>
				      	<c:when test="${requestScope.idEselon1 == 100}">
				      		<option value="${requestScope.idEselon1}">${requestScope.eselon1}</option>
				      	</c:when>
				      	<c:otherwise>
					      	<c:if test="${requestScope.eselon1New.size() > 1}">
								<option value="">Pilih Salah Satu</option>
							</c:if>
					     	<c:forEach items="${requestScope.eselon1New}" var="k" varStatus="loopStatus">
								<c:if test="${!fn:contains(k.nama, 'delete')}">
									<option value="${k.id}">${k.nama}</option>
								</c:if>
					      	</c:forEach>
				      	</c:otherwise>
				      </c:choose>
				      </select>
				    </div>
				  </div>
				  <div class="form-group row">
				    <label for="" class="col-sm-2 col-form-label text-md-right">Satuan Kerja</label>
				    <div class="col-sm-8">
				      <select name="id_unit_kerja" class="form-control">
				      	<%-- <option value="${requestScope.idUnitKerjaNew}">${requestScope.unitKerjaNew}</option> --%>
			      		<c:if test="${requestScope.idEselon1 == 100}">
			      			<option value="${requestScope.idSatker}">${requestScope.namaSatker}</option>
			      		</c:if>
				      	<c:if test="${requestScope.idEselon1 != 100}">
				      		<option value="">-</option>
					      	<%-- <c:if test="${requestScope.idSatkerNew.size() > 1}">
								<option value="">Pilih Salah Satu</option>
							</c:if> --%>
							<%-- <c:set value="${requestScope.namaSatker}" var="namaSatker" />
							<c:forEach items="${requestScope.idSatkerNew}" var="k" varStatus="loopStatus">
								<option value="${k.idUnitAuditi}">${k.namaUnitAuditi}</option>
							</c:forEach> --%>
					  	</c:if>
				      </select>
					  <select name="id_propinsi" class="d-none">
					    <c:if test="${requestScope.idPropinsi != null}">
				      		<option value="${requestScope.idPropinsi}">${requestScope.idPropinsi}</option>
				      	</c:if>
		      		  </select>
				    </div>
				  </div>
				  <div class="form-group row">
				    <label for="" class="col-sm-2 col-form-label text-md-right">Jabatan</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" value="${requestScope.jabatan}" readonly>
				    </div>
				  </div>
				  <div class="form-group row">
				    <label for="" class="col-sm-2 col-form-label text-md-right">Jenis Kelamin</label>
				    <div class="col-sm-4">
				      <input name="jenis_kelamin" type="text" class="form-control" value="${requestScope.jenisKelamin}" readonly>
				    </div>
				     <div class="col-sm-4">
				      <input value="${requestScope.tanggalLahir}" id="tanggal_lahir" name="tanggal_lahir" type="date" class="form-control" placeholder="Tentukan Tgl.Lahir Anda">
				    </div>
				   <!--  <script>
				    	document.getElementById("tanggal_lahir").value = "2014-02-09";
				    </script> -->
				  </div>
				  <small class="card-title text-primary">TIPS: Apabila Kolom <strong>Eselon1, Satker, dan Jabatan</strong> anda tidak sesuai, silahkan hubungi admin.</small>
				  
			  </div>
			</div>
	        <div class="card mt-3 mb-3">
				<div class="card-header "><h6 class="card-title p-0 m-0">Klarifikasi Bidang Teknis dan Jabatan Anda :</h6></div>
			 	<div class="card-body">
				  <div class="form-group row d-none">
				    <label for="" class="col-sm-2 col-form-label text-md-right">RESPONDEN</label>
				    <div class="col-sm-4">
				      <input type="text" class="form-control" name="id_responden" value="${requestScope.idResponden}" readonly>
				    </div>
				    <div class="col-sm-4">
				      <input type="text" class="form-control" name="id_sesi" value="${requestScope.idSesi}" readonly>
				    </div>
				  </div>
				  <div class="row form-group d-nones">
				  	<label for="" class="col-sm-2 col-form-label text-md-right">Jabatan</label>
				    <div class="col-sm-8">
				      <select name="id_bidang_teknis" class="form-control" onchange="getSelectJabatan(this);" >
				      	<option value="">Pilih</option>
				      	<c:forEach items="${requestScope.listBidangTeknis}" var="k" varStatus="loopStatus">
						<option value="${k.id}" data-idjabatan="${k.idKelompokJabatan}">${k.nama}</option>	
					  	</c:forEach>
				      </select>
				    </div>
				  </div>
				  <div class="row form-group ">
				  	<label for="" class="col-sm-2 col-form-label text-md-right">Jenjang</label>
				    <div class="col-sm-8">
				      <select name="id_jabatan" class="form-control form-control-lg select select2">
				      	<%-- <c:forEach items="${requestScope.listMasterJabatan}" var="k" varStatus="loopStatus">
						<option value="${k.id}">${k.nama}</option>	
					  	</c:forEach> --%>
				      </select>
				    </div>
				  </div>
				  <small class="card-title text-primary">TIPS: Apabila Kolom <strong>Jabatan</strong> atau <strong>Jenjang</strong> anda tidak muncul, coba lakukan refresh(CTRL+F5) atau logout, kemudian coba login kembali.</small>
				  
				</div>
			  	<div class="card-footer ">
				<button type="submit" class="btn btn-success float-right">Selanjutnya</button>
				</div>
			</div>
			</form>
         </c:otherwise>
      </c:choose>
		
    </div>
	
	</div>
	</div>
	<script src="${pageContext.request.contextPath}/js/general-index-script.js"></script>
	<%@include file="inc/footer.jsp"%>
</body>

</html>