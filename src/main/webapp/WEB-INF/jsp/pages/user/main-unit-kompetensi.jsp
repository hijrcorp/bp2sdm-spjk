
<%@include file="inc/redirect.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
	<script>
	function init(){}
	var user_id = "<%= request.getAttribute("userId") %>";
	var sesi_id = "${requestScope.idSesi}";
	var responden_id = "${requestScope.idResponden}";
	var statusResponden = "${requestScope.statusResponden}";
	</script>
	<style>
	.swal2-content{text-align:left!important}
	</style>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>

    <div class="container ">
      <div class="row justify-content-center pt-5">
        <div class="col-md-10">
         	<div class="card mt-3 mb-3">
				<div class="card-header "><h6 class="card-title p-0 m-0">SESI: ${requestScope.statusUnitKompResponden} </h6></div>
			 	
			  	<div id="msg-survey" class="card-body text-justify">
			  		<h6 class="card-title p-0 m-0">Petunjuk Pengisian :</h6>
				  	${requestScope.introduceSoal.value}
			  	</div>
			</div>
      	<c:choose>
         <c:when test = "${requestScope.statusUnitKompResponden == null}">
         	<c:redirect url="/page/index"/>
         </c:when>
         <c:when test = "${requestScope.statusResponden == 'PROSES'}">
         	<c:redirect url="/page/simulasi-ujian"/>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'TEKNIS_INTI'}">
         	<div class="card mt-3 mb-3">
				<div class="card-header "><h6 class="card-title p-0 m-0">Kompetensi INTI Anda : </h6></div>
			 	
			  	<div id="msg-survey" class="card-body">
			  	<form id="form-inti">
				<ul class="list-group">
				<c:forEach items="${requestScope.listIntiJkt}" var="k" varStatus="loopStatus">
				  <c:choose>
				  <c:when test = "${k.jumlahPertanyaan > 0}">
				  	<li class="list-group-item">
         		  </c:when>
         		  <c:otherwise>
         		  	<li class="list-group-item bg-danger ${requestScope.hideDanger.value}">
					<c:set var="passUnit" value="disabled"/>
				  </c:otherwise>
				  </c:choose>
			  		<div class="form-check">
					  <input class="form-check-input" type="checkbox" value="${k.idUnitKompetensiTeknis}" name="teknis_inti" checked onchange="myghty(this)">
					  <label class="form-check-label" for="">${k.kodeUnitKompetensiTeknis} ${k.judulUnitKompetensiTeknis} </label>
					</div>
				  </li>
			  	</c:forEach>
				</ul>
				</form>
			  	</div>
			  	<div class="card-footer ">
				<button id="btn-mulai" class="btn btn-success float-right" ${passUnit}>Lanjut</button>
				</div>
			</div>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'TEKNIS_PILIHAN'}">
	        <div class="card mt-3">
				<div class="card-header ">
				<h6 class="card-title p-0 m-0">Kompetensi Pilihan : </h6>
				</div>
			 	
			  	<div id="msg-survey" class="card-body"  style="max-height: calc(100vh - 239px);overflow: auto;">
			  	<form id="form-pilihan">
			  	<h6 class="card-title">Silahkan Pilih Kompetensi minat anda</h6>
				<ul class="list-group">
				<c:forEach items="${requestScope.listPilihanJkt}" var="k" varStatus="loopStatus">
				  <c:choose>
				  <c:when test = "${k.jumlahPertanyaan > 0}">
				  	<li class="list-group-item">
         		  </c:when>
         		  <c:otherwise>
         		  	<li class="list-group-item bg-danger disabled ${requestScope.hideDanger.value}">
					<c:set var="passUnit" value="disabled"/>
				  </c:otherwise>
				  </c:choose>
			  		<div class="form-check">
					  <input class="form-check-input" type="checkbox" value="${k.idUnitKompetensiTeknis}" name="teknis_pilihan">
					  <label class="form-check-label" for="">${k.kodeUnitKompetensiTeknis} ${k.judulUnitKompetensiTeknis}</label>
					</div>
				  </li>
			  	</c:forEach>
				</ul>
				</form>
			  	</div>
			  	<div class="card-footer ">
				<button id="btn-mulai" class="btn btn-success float-right" >Lanjut</button>
				</div>
			</div>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'MANAJERIAL'}">
         	
	        <div class="card mt-3">
				<div class="card-header ">
				<h6 class="card-title p-0 m-0">Kompetensi Manajerial : </h6>
				</div>
			 	
			  	<div id="msg-survey" class="card-body"  style="max-height: calc(100vh - 239px);overflow: auto;">
			  	<form id="form-pilihan">
			  	<h6 class="card-title">Silahkan Pilih Kompetensi:</h6>
				<ul class="list-group">
				<c:forEach items="${requestScope.listManajerialJkt}" var="k" varStatus="loopStatus">
				  <c:choose>
				  <c:when test = "${k.jumlahPertanyaan > 0}">
				  	<li class="list-group-item">
         		  </c:when>
         		  <c:otherwise>
         		  	<li class="list-group-item bg-danger">
					<c:set var="passUnit" value="disabled"/>
				  </c:otherwise>
				  </c:choose>
			  		<div class="form-check">
					  <input class="form-check-input" type="checkbox" value="${k.idUnitKomp}" name="manajerial" checked onchange="myghty(this)">
					  <label class="form-check-label" for="">${k.kodeUnitKomp} ${k.judulUnitKomp}</label>
					</div>
				  </li>
			  	</c:forEach>
				</ul>
				</form>
			  	</div>
			  	<div class="card-footer ">
				<button id="btn-mulai" class="btn btn-success float-right" ${passUnit}>Lanjut</button>
				</div>
			</div>
         </c:when>
         <c:when test = "${requestScope.statusUnitKompResponden == 'SOSIOKULTURAL'}">
         	
	        <div class="card mt-3">
				<div class="card-header ">
				<h6 class="card-title p-0 m-0">Kompetensi Sosiokultural : </h6>
				</div>
			 	
			  	<div id="msg-survey" class="card-body"  style="max-height: calc(100vh - 239px);overflow: auto;">
			  	<form id="form-pilihan">
			  	<h6 class="card-title">Silahkan Pilih Kompetensi:</h6>
				<ul class="list-group">
				<c:forEach items="${requestScope.listSosiokulturalJkt}" var="k" varStatus="loopStatus">
				  <li class="list-group-item">
			  		<div class="form-check">
					  <input class="form-check-input" type="checkbox" value="${k.id}" name="sosiokultural" checked onchange="myghty(this)">
					  <label class="form-check-label" for="">${k.kode} ${k.judul}</label>
					</div>
				  </li>
			  	</c:forEach>
				</ul>
				</form>
			  	</div>
			  	<div class="card-footer ">
				<button id="btn-mulai" class="btn btn-success float-right" ${passUnit}>Lanjut</button>
				</div>
			</div>
         </c:when>
         <%-- <c:otherwise>
		 </c:otherwise> --%>
		</c:choose>
	        
         <%-- </c:otherwise>
      </c:choose> --%>
		
    </div>
	
	</div>
	</div>
	
	<script src="${pageContext.request.contextPath}/js/general-main-unit-kompetensi.js"></script>
	<%@include file="inc/footer.jsp"%>
</body>

</html>