<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>

<spring:eval expression="@environment.getProperty('app.mode')" var="mode" />

<a href="${frontendUrl}">Click ${req}</a>
<hr/>
<a href="${frontendUrl}">Click ${url}</a>
<hr/>
<c:choose>
    <c:when test = "${mode == 1}">
		<% out.print(request.getServerName().equals("simrenbang.bp2sdm.menlhk.go.id")); %>
    </c:when>
    <c:otherwise>
    	<% response.sendRedirect(request.getContextPath()+ "/page"); %>
    </c:otherwise>
  </c:choose>
<%-- <%
response.sendRedirect(request.getContextPath()+ "/page");
%> --%>