var path_unsur = ctx + '/unsur';
var path_sub_unsur = ctx + '/sub-unsur';
var path_maturitas_tingkat = ctx + '/maturitas-tingkat';
var path_maturitas_parameter = ctx + '/maturitas-parameter';
var path_unit_auditi = ctx + '/unit-auditi';
var urlRefrence = ctx + "/rest/reference/";
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

var jenis = '';
var list_unsur = [];
var list_sub_unsur = [];
var list_unit_auditi = [];
var list_unit_auditi_raw = [];
var list_maturitas_penilaian = [];
var list_eselon1 = [];
var list_unit_auditi_by_id_eselon1 = [];

function init(){
	
	jenis = $.urlParam('jenis');
	jenis = jenis.split(' ')[0];
	$('[id=menu]').html($.urlParam('menu'));
	$('#menu').html($.urlParam('menu').split(' ')[2]);
	ajaxGET(path_unsur + '/list?filter_jenis='+jenis+'&filter_tahun='+moment().format('YYYY'),function(response) {
		var list_sub_unsur = [];
		$.each(response.data, function(key, value){ $.each(value.list_sub_unsur, function(key, sub_unsur){ list_sub_unsur.push(sub_unsur); }); });
		list_unsur.push({ id: '00', nama: 'Semua Unsur', list_sub_unsur: list_sub_unsur });
		$.each(response.data, function(key, value){ list_unsur.push(value); });
//		list_unsur.concat(response.data);
		console.log('list_unsur: ', list_unsur);
		var nav_tab = '';
		var nav_tab_content = '';
		$.each(list_unsur, function(key, value){
			nav_tab += '<a id="nav-'+value.id+'-tab" data-toggle="tab" href="#nav-'+value.id+'" role="tab" aria-controls="nav-'+value.id+'" aria-selected="true"  class="nav-item nav-link '+(key==0?'active':'')+'" onclick="getSelectSubUnsur(\''+value.id+'\')">'+value.nama+'</a>';
			nav_tab_content += '<div id="nav-'+value.id+'" aria-labelledby="nav-'+value.id+'-tab" role="tabpanel" class="tab-pane fade '+(key==0?'show active':'')+'">';
			nav_tab_content += $('#template-content').html().replace('add()', 'add(\''+jenis.toUpperCase().trim()+'\', \''+value.id+'\')').replace('card-data-index', 'card-data-'+value.id);
			nav_tab_content += '</div>';
		});
		$('#nav-tab').html(nav_tab);
		$('#nav-tab-content').html(nav_tab_content);
		
		
		if(list_unsur.length > 0) { getSelectSubUnsur(list_unsur[0].id); }
		
		ajaxGET(path_unit_auditi + '/list', function(response){
			list_unit_auditi_raw = response.data;
			console.log('list_unit_auditi_raw: ', list_unit_auditi_raw);
			display();
		});
		
		getListTahun(null, moment().format('YYYY'));
		$('[name=filter_tahun]').change(function(){
//			$('[name=filter_tahun]').val($(this).val());
//			getUnitAuditiSummaryList($(this).val());
			display();
		});
		getUnitAuditiSummaryList(moment().format('YYYY'));
		
		$('[name=id_unsur]').empty();
		$('[name=id_unsur]').append(new Option('Pilih Unsur', ''));
		$.each(response.data, function(key, value) {
			$('[name=id_unsur]').append(new Option((key+1)+'. '+value.nama, value.id));
		});
		$('[name=id_unsur]').change(function(){
			getSelectSubUnsur($(this).val());
		});
		getSelectSubUnsur('00');
	});
	
	ajaxGET(path_sub_unsur + '/list',function(response) {
		console.log('the real sub_unsur : ', response.data);
		list_sub_unsur = response.data;
	},'onGetSelectSubUnsurError');
	

    $.getJSON(urlRefrence+'eselon1/?display=ALL', function(response) {
        if (response.status == 'OK') {
        	list_eselon1 = response.data;
            $.each(response.data, function(key, value) {
                $('[name=filter_id_eselon1]').append(new Option(value.namaEselon1, value.id));
            });
            $('[name=filter_id_eselon1]').select2();
            $('[name=filter_id_eselon1]').change(function(){ display(); });
            display();
        } else { alert("Connection error"); }
	},'onGetSelectEselon1Error');

	getSelectTingkatMaturitas();
    
    $('#btn-download').click(function(e){
    	params = $( "#search-form" ).serialize();
    	params += 'filter_tahun='+$('[name=filter_tahun]').val();
    	params += '&filter_id_eselon1='+$('[name=filter_id_eselon1]').val();
    	params += '&filter_jenis='+jenis.toUpperCase();
        location = path_unsur + '/download/monitoring-bukti?'+params;
    });
	
	$('#search-form').submit(function(e){
		display();
		e.preventDefault();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
}

function getEselon1List(){
    $.getJSON(urlRefrence+'eselon1/'+"?display=ALL&token="+token, function(response) {
        if (response.status == 'OK') {
            $.each(response.data, function(key, value) {
                $('[name=id_eselon1]').append(new Option(value.namaEselon1, value.id));
            });
            $('[name=id_eselon1]').select2({ dropdownParent: $('#modalDetail') });
        } else { alert("Connection error"); }
    });
}

function add(jenis, id_unsur){
	selected_id = '';
	selected_action = 'add';
	clearForm();
	if(jenis == null || id_unsur == null) setTimeout(function() { $('#modal-form').modal('hide') }, 500);
	if(jenis!=null) $('[name=jenis]').val(jenis);
	if(id_unsur!=null) $('[name=id_unsur]').val(id_unsur);
	if(id_unsur=='00') $('[name=id_unsur]').val('');
}

function getSelectSubUnsur(id_unsur){
	if(id_unsur == null) { console.error('[getSelectSubUnsur] id_unsur tidak boleh kosong'); return false; }
	
	$('[name=id_sub_unsur]').empty();
	$('[name=id_sub_unsur]').append(new Option('Pilih Sub Unsur', ''));
	
//	console.log('id_unsur: ', id_unsur);
	var i = 1;
	$.each(list_sub_unsur, function(key, value) {
		if(id_unsur == value.id_unsur) {
			$('[name=id_sub_unsur]').append(new Option((i++)+'. '+value.nama, value.id));
		}
	});
	
//	ajaxGET(path_sub_unsur + '/list?filter_id_unsur='+id_unsur,'onGetSelectSubUnsur','onGetSelectSubUnsurError');
}

function getSelectTingkatMaturitas(){
	$('[name=id_maturitas_tingkat]').empty();
	$('[name=id_maturitas_tingkat]').append(new Option('Pilih Tingkat', ''));
	ajaxGET(path_maturitas_tingkat + '/list',function(response){
		$.each(response.data, function(key, value) {
			$('[name=id_maturitas_tingkat]').append(new Option((key+1)+'. '+value.judul, value.id));
		});
	},'onGetSelectError');
}

function display(index = 'all', page = 1){
		
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){ tbody.text(''); }
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');

	console.log('[display] filter_tahun: ', $('[name=filter_tahun]').val());
	console.log('[display] filter_id_eselon1: ', $('[name=filter_id_eselon1]').val());
	
	if($('[name=filter_tahun]').val() == '' || $('[name=filter_id_eselon1]').val() == '') {
		$.each(list_unsur,function(key,unsur){
			$('#card-data-'+unsur.id).html('<p class="text-center pt-3 pb-3">Silahkan pilih Tahun dan Eselon1 terlebih dahulu.</p>');
		});
		return false;
	}
	
	params = $( "#search-form" ).serialize();
	params += 'filter_tahun='+$('[name=filter_tahun]').val();
	params += '&filter_id_eselon1='+$('[name=filter_id_eselon1]').val();
	params += '&filter_jenis='+jenis.toUpperCase();
	list_unit_auditi_by_id_eselon1 = [];
	$.each(list_unit_auditi_raw,function(key,unit_auditi){
		if(unit_auditi.id_eselon1 == $('[name=filter_id_eselon1]').val()) {
			list_unit_auditi_by_id_eselon1.push(unit_auditi);
		}
	});

	
	$.each(list_unsur,function(key,unsur){
		$('#card-data-'+unsur.id).html('');
	});
	
	console.log(path_unsur + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
	ajaxGET(path_unsur + '/list?'+params,'onGetListSuccess','onGetListError');
		
	current_page = page;

}

function onGetListSuccess(response){
	
	if(response.data.length == 0) return;
	
	var list_unsur = []; 
	var list_sub_unsur = [];
	$.each(response.data, function(key, value){ $.each(value.list_sub_unsur, function(key, sub_unsur){ list_sub_unsur.push(sub_unsur); }); });
	list_unsur.push({ id: '00', nama: 'Semua Unsur', list_sub_unsur: list_sub_unsur });
	$.each(response.data, function(key, value){ list_unsur.push(value); });
	
	console.log('response.data: ', list_unsur);
	
	list_maturitas_penilaian = [];
	
	$.each(list_unsur,function(key,unsur){
	
		var index = unsur.id;
		
		console.log('[display] response sub-unsur-'+index+': ', unsur);
		
		$('[id=loading-row]').remove();
		$('[id=no-data-row]').remove();
		
		var card = $('#card-data-'+index);
		var html = '';
		
		$.each(unsur.list_sub_unsur,function(key,sub_unsur){
			html += '<div class="card-body mb-0 pb-0"><h3 class="card-text"><span class="custom-title">'+(key+1)+'. '+sub_unsur.nama+' ('+sub_unsur.kode+')</span></h3></div>';
			html += '<div class="card-body">';
			html += '	<table id="tbl-data-'+unsur.id+'-'+sub_unsur.id+'" class="table table-bordered table-striped table-hover" style="table-layout: fixed;">';
			html += '		<thead>';
			html += '			<tr>';
			html += '				<th rowspan="2" width="50" class="text-center">No</th>';
			html += '				<th rowspan="2" width="100" class="text-center">Ref</th>';
			html += '				<th rowspan="2" width="300" class="text-center">Parameter</th>';
			$.each(list_unit_auditi_by_id_eselon1,function(key,unit_auditi){
				html += '			<th rowspan="2" width="65" class="text-center"><a href="#" data-toggle="tooltip" data-placement="top" title="'+unit_auditi.nama+'">'+unit_auditi.kode+'</a></th>';
			});
			html += '			</tr>';
			html += '		</thead>';
			html += '		<tbody>';
			$.each(sub_unsur.list_maturitas_tingkat,function(key,maturitas_tingkat){
				html += '		<tr>';
				html += '			<td colspan="'+(list_unit_auditi_by_id_eselon1.length + 3)+'"><h4 class="custom-title">'+maturitas_tingkat.kode+'. LEVEL ('+maturitas_tingkat.kode.split('.')[2]+'): '+maturitas_tingkat.judul+'</h4></td>';
				html += '		</tr>';
				
				$('[id=loading-row]').remove();
				$('[id=no-data-row]').remove();
				
				var row = "";
				$.each(maturitas_tingkat.list_maturitas_parameter,function(key,value){
					$.each(value.list_maturitas_penilaian,function(key,value){
						var isAlreadyExist = false;
						$.each(list_maturitas_penilaian,function(key,value2){
							if(value.id == value2.id) {
								isAlreadyExist = true;
								return;
							}
						});
						if(isAlreadyExist == false) {
							list_maturitas_penilaian.push(value);
						}
					})
				});
				$.each(maturitas_tingkat.list_maturitas_parameter,function(key,value){
					if(!contains.call(appendIds, value.id)){
						row += renderRow(value, (key+1));
					}
				});
				html += (row == "" ? '<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>' : row);
			});
			html += '		</tbody>';
			html += '	</table>';
			html += '</div>';
		});
		
		card.html(html);
		
		$('[data-toggle="tooltip"]').tooltip();
	
	});
	
}

function appendRow(value){
	$('[id=loading-row]').remove();
	$('[id=no-data-row]').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data-"+value.id_unsur+'-'+value.id_sub_unsur).find('tbody');
	
	console.log('tambah ke sini: ', "#tbl-data-"+value.id_unsur+'-'+value.id_sub_unsur);
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var num = $('#row-'+value.id_unsur+'-'+value.id_sub_unsur+'-'+value.id_maturitas_tingkat+'-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data-"+value.id_unsur+'-'+value.id_sub_unsur).find('tbody');
	
	$('#row-'+value.id_unsur+'-'+value.id_sub_unsur+'-'+value.id_maturitas_tingkat+'-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}
//$.each(maturitas_tingkat.list_maturitas,function(key,maturitas){
//html += '	<tr>';
//html += '		<td class="text-center">'+(key+1)+'</td>';
//html += '		<td class="text-center">'+maturitas.kode_pernyataan+'</td>';
//html += '		<td>'+maturitas.judul_pernyataan+'</td>';
//html += '		<td class="text-center" nowrap>';
//html += '			<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+maturitas.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
//html += '			<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+maturitas.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
//html += '		</td>';
//html += '	</tr>';

function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id_unsur+'-'+value.id_sub_unsur+'-'+value.id_maturitas_tingkat+'-'+value.id+'">';
	}
	
	row += '<td class="text-center">'+(num)+'</td>';
	row += '<td class="text-center">'+value.kode_pernyataan+'</td>';
	row += '<td class="">'+value.judul_pernyataan+'</td>';
	$.each(list_unit_auditi_by_id_eselon1,function(key,unit_auditi){
		var hasil = '';
		$.each(list_maturitas_penilaian,function(key,maturitas_penilaian){
			if(maturitas_penilaian.id_unit_auditi == unit_auditi.id && maturitas_penilaian.id_maturitas_parameter == value.id){
				hasil = maturitas_penilaian.hasil;
			}
		});
		row += '<td class="text-center" style="vertical-align: middle"><big><big><big>'+(hasil=='Y' ? getStatus(hasil, 'CHECKLIST') : '')+'</big></big></big></td>';
	});
//	row += '<td class="text-center"><textarea class="form-control" rows="3"></textarea></td>';
//	row += '<td class="text-center"><textarea class="form-control" rows="3"></textarea></td>';
//	row += '<td><select class="form-control"></select></td>';
//    if($("#tbl-data").find('thead').children().first().find('th').last().html() == 'Aksi'){
//		row += '<td class="text-center" nowrap>';
//		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
//		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
//		row += '</td>';
//    }
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('[name=id_unsur]').val('');
	$('[name=id_sub_unsur]').val('');
	$('[name=id_maturitas_tingkat]').val('');
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		ajaxGET(path_maturitas_parameter + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		ajaxPOST(path_maturitas_parameter + '/'+id+'/'+selected_action,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		//appendRow(response.data);
		display(); // logic-nya banyak jadi sementara gini dulu
	}else if(selected_action == 'edit'){
		//updateRow(response.data);
		display(); // logic-nya banyak jadi sementara gini dulu
	}else if(selected_action == 'delete'){
		//removeRow(response.data);
		display(); // logic-nya banyak jadi sementara gini dulu
	}
	
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function fillFormValue(value){
	//console.log(value);
	clearForm();
	
	getSelectSubUnsur(value.id_unsur);

	$('[name=jenis]').val(value.jenis);
	$('[name=id_unsur]').val(value.id_unsur);//.select2().trigger('change');
	$('[name=id_sub_unsur]').val(value.id_sub_unsur);//.select2().trigger('change');
	$('[name=id_maturitas_tingkat]').val(value.id_maturitas_tingkat);//.select2().trigger('change');
	$('[name=kode_pernyataan]').val(value.kode_pernyataan);
	$('[name=judul_pernyataan]').val(value.judul_pernyataan);
	
}

function save(){
	startLoading('btn-save');
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(path_maturitas_parameter + '/save',obj,'onModalActionSuccess','onModalActionError');
}

