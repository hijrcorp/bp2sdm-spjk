var path = ctx + '/restql';
var path_inventori = ctx + '/inventori';

var chartBar = null;

window.isMobile = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

var public_filter = 'kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')';
var selected_id_kelompok = ($.urlParam('id_kelompok')!=null?$.urlParam('id_kelompok'):"");
var selected_id_tingkatan = ($.urlParam('id_tingkatan')!=null?$.urlParam('id_tingkatan'):"");
var selected_id_jenjang = ($.urlParam('id_jenjang')!=null?$.urlParam('id_jenjang'):"");
var selected_id_eselon1 = ($.urlParam('id_eselon1')!=null?$.urlParam('id_eselon1'):"");
var selected_id_unit_auditi = ($.urlParam('id_unit_auditi')!=null?$.urlParam('id_unit_auditi'):"");
function init(){
	if($.urlParam('kode-sesi')==null){
		showListSesi();
	}

	display();
	
//	ajaxGET(path + '/eselon1'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
//		$.each(response.data, function(key,value){ $('[name=filter_id_eselon1]').append(new Option(value.nama, value.id)); });
//	});
//
//	ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
//		$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
//	});

	ajaxGET(path + '/tingkat_jabatan',function(response){
		$.each(response.data, function(key,value){ $('[id=filter_tingkat_jabatan], [id=filter_chart_tingkat_jabatan]').append(new Option(value.nama, value.id)); });
	});
	
	ajaxGET(path + '/jenjang_jabatan',function(response){
		$.each(response.data, function(key,value){ $('[id=filter_jenjang_jabatan], [id=filter_chart_jenjang_jabatan]').append(new Option(value.nama, value.id)); });
	});
//	$('#filter_tingkat_jabatan, #filter_jenjang_jabatan').on('change', function (e) {
//		displayTeknis();
//		displayManajerial();
//		displaySosiokultural();
//	});
	
//	$('[name=filter_id_eselon1]').change(function(){
//		ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter=id_eselon1.eq(\""+this.value+"\")",function(response){
//			$('[name=filter_id_unit_auditi]').empty();
//			$('[name=filter_id_unit_auditi]').append('<option value="">Satker</option>');
//			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
//		});
//	});
}

function printPDF(){
	$('aside').attr("style", "display:none!important");
	$('.page-container').attr('style', 'padding-left:0px');
	$('header').attr("style", "display:none!important");
	$('.main-content').attr('style', 'padding:0px');
}

function display(){
	displayTeknis();
	displayManajerial();
	displaySosiokultural();
}

function getBarOption(data1) {
	return {
		maintainAspectRatio: false,
   		barThickness: 1, 
		tooltips: { 
			enabled: true,
			callbacks: {
                label: function(tooltipItem, data) {
                	console.log(tooltipItem);
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += Math.ceil(parseInt(tooltipItem.yLabel * total_gender_usia) / 100);
                    //
                    //ctx.fillText(Math.ceil(parseInt((data*objnew.total))/100), bar._model.x+35, bar._model.y);
					
                    //
                    //label += tooltipItem.xLabel+"%";
                    return label;
                }
            }
		},
		hover :{ animationDuration: 0 },
		legend:{ display: false, position: 'bottom' },
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero:true,
					fontFamily: "'verdana', sans-serif",
					fontSize:10,
//                    userCallback: function(value) {
//                    	console.log(value+"="+value.length);
//                        if(value.length>10){
//                        	value.split(" ");
//                        	value+=value[0]+"<br/>"+value[1];
//                        }
//                    	return value;
//                    },

		              //autoSkip: false,
		              padding: 12
				},
				scaleLabel:{ display:false },
				gridLines: { }
			}],
			yAxes: [{
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:12,
                    beginAtZero:true,
                    min: 0,
                    max: 100,
                    userCallback: function(value) {
                        return value+'%';
                    },
				},
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				console.log(ctx);
				ctx.textAlign = "right";
				ctx.font = "12px verdana";
				ctx.fillStyle = "#000";
				//ctx.shadowOffsetY = -5;
				ctx.textBaseline = "bottom";
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						//data = parseFloat((dataset.data[index] /* / (data1[index] + data2[index])*/).toFixed(2)*1) + '%';
						data = parseFloat((dataset.data[index])) + '%';
						ctx.fillText(data, bar._model.x+20, bar._model.y-5);
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}

function randomScalingFactor() {
	return Math.round(Math.random() * 10);
};

function getBarChart(data){
	var rulecolor =[];
	var rulehovercolor =[];
	$.each(data.nilai, function(key){
		if(this < 60){
			rulecolor.push("#dc3545");
			rulehovercolor.push("#dc3545c7");
		}else{
			rulecolor.push("#4A9B82");
			rulehovercolor.push("#70c2a9");
		}
	});
	resetCanvas();
	var ctx = document.getElementById("bar");
	//ctx.height = 250;
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
	    labels: data.label,
	    datasets: [{
    		label: 'Skor',
	        data: data.nilai,
	        backgroundColor: rulecolor,//"#4A9B82", 
	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
	    }]
		},
		options: {
			maintainAspectRatio: false,
			responsive: true,
			tooltips: { enabled: false },
			hover :{ animationDuration: 0 },
			legend:{ display: false, position: 'bottom' },
			legendCallback: function(chart) {
				var labels = ["Tercapai", "Tidak Tercapai"];
				var background = ["#4A9B82", "#dc3545"];
			    var text = []; 
			    text.push('<ul class="' + chart.id + '-legend nav">'); 
			    for (var i = 0; i < labels.length; i++) { 
			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
			        if (labels[i]) { 
			            text.push(labels[i]); 
			        } 
			        text.push('</li>'); 
			    } 
			    text.push('</ul>'); 
			    return text.join(''); 
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontFamily: "'verdana', sans-serif",
						fontSize:16,
	                    min: 0,
	                    max: 100
					},
					scaleLabel:{ display:false },
					gridLines: { }, 
					stacked: true
				}],
				yAxes: [{
					gridLines: {
						display:false,
						color: "#fff",
						zeroLineColor: "#fff",
						zeroLineWidth: 0
					},
					ticks: {
						fontFamily: "'verdana', sans-serif",
						fontSize:16
					},
					stacked: true
				}]
			},
			animation: {
				onComplete: function () {
					var chartInstance = this.chart;
					var ctx = chartInstance.ctx;
					ctx.textAlign = "right";
					ctx.font = "14px verdana";
					ctx.fillStyle = "#fff";
					
					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						Chart.helpers.each(meta.data.forEach(function (bar, index) {
							newdata = (dataset.data[index]);
							//console.log(data);
							if(newdata >= 0) {
								ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
							} else {
								ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
							}
						}),this)
					}),this);
				}
			},
			pointLabelFontFamily : "Quadon Extra Bold",
			scaleFontFamily : "Quadon Extra Bold",
		}
	});
	$("#your-legend-container").html(myChart.generateLegend());
}


var resetCanvas = function (data) {
	  
	  if(data.element_container=="container-bar-inti"){
		  $('#'+data.element+'').remove(); // this is my <canvas> element //width="325" height:500px;"
		  
		  $('#'+data.element_container+'').append('<canvas class="w-100s" style="position: relative;"  id="'+data.element+'"><canvas>');
		  //
		  canvas = document.querySelector('#'+data.element+''); // why use jQuery?
		  ctx = canvas.getContext('2d');
//		  ctx.canvas.width = $('#container-bar').width(); // resize to parent width
//		  ctx.canvas.height = $('#container-bar').height(); // resize to parent height

		  var x = canvas.width/2;
		  var y = canvas.height/2;
		  ctx.font = '10pt Verdana';
		  ctx.textAlign = 'center';
		  ctx.fillText('This text is centered on the canvas', x, y);
	  }
	  if(data.element_container=="container-bar-pilihan"){
		  $('#'+data.element+'').remove(); // this is my <canvas> element //width="325" height="325"height:600px;" 
		  
		  $('#'+data.element_container+'').append('<canvas class="w-100s" style="position: relative;" id="'+data.element+'"><canvas>');
		  //
		  canvas = document.querySelector('#'+data.element+''); // why use jQuery?
		  ctx = canvas.getContext('2d');
//		  ctx.canvas.width = $('#container-bar').width(); // resize to parent width
//		  ctx.canvas.height = $('#container-bar').height(); // resize to parent height

		  var x = canvas.width/2;
		  var y = canvas.height/2;
		  ctx.font = '10pt Verdana';
		  ctx.textAlign = 'center';
		  ctx.fillText('This text is centered on the canvas', x, y);
	  }
};

function getRadarChart(data) {

	var randomScalingFactor = function() {
		return Math.round(Math.random() * 10);
	};

	var color = Chart.helpers.color;
	var config = {
		type: 'radar',
		data: {
			labels: data.label,
			datasets: [{
				label: 'Nilai Pemetaan',
				backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
				borderColor: window.chartColors.green,
				pointBackgroundColor: window.chartColors.green,
				data: data.nilai
			}, {
				label: 'Nilai Standard',
				backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
				borderColor: window.chartColors.blue,
				pointBackgroundColor: window.chartColors.blue,
				data: data.nilai_standar
			}]
		},
		options: {
			legend: {
				position: 'bottom',
			},
			scale: {
				ticks: {
					beginAtZero: true
				}
			}
		}
	};

	var ctx = document.getElementById("radar");
//	ctx.height = isMobile() ? 200 : 200;
//
//	var colorNames = Object.keys(window.chartColors);

	if(myChart == null) {
		var myChart = new Chart(ctx, config);
	}else { 
		myChart.data.datasets[0].data = data.nilai;
		myChart.data.datasets[1].data = data.nilai_standar;
		myChart.update(); 
    }
}

function addPercentage(canvas, ctx, data){
    var cx = canvas.width / 4;
    var cy = canvas.height / 4;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '12px verdana';
    ctx.fillStyle = 'black';
    ctx.fillText(parseInt((data[0]/(data[0]+data[1]))*100)+"%", cx, cy);
}

var arr_total_gender_usia=[];
var total_gender_usia=0;

function displayProgress(page=1, limit=10, params='') {
  limit=100000;
  //
	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
		params=',id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')';
		params+=',id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')';
	}
	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
		params=',id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')';
	}
	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
		params=',id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')';
	}
	//
ajaxGET(path + '/kelompok_jabatan?page='+page+'&limit='+limit,function(response){
	var responsedatakelompokjabatan=response.data;
  ajaxGET(path + '/progress_peserta_ujian_detil?page='+page+'&limit='+limit+'&join=kelompok_jabatan,unit_auditi&filter='+public_filter+params,function(response){
  	console.log('list_progress_peserta_ujian_detil', response.data);
  	//
    var unique_kelompok=[];
    var unique_kelompok_with_string=[];
	$.each(response.data,function(key){
    	var valueunique = this.id_kelompok_jabatan;
		if(unique_kelompok.indexOf(valueunique) == -1) unique_kelompok.push(valueunique);
		//if(unique_kelompok_teknis_with_string.indexOf(valueunique) == -1) unique_kelompok_teknis_with_string.push(valueunique);
		//else unique_kelompok_teknis_with_string.push("");
	});
  	var obj=[];
  	$.each(unique_kelompok, function(keyunqiue, valueunique){
  	  	var count_jumlah=0;
  	  	var count_total=0;
	  	$.each(response.data, function(key){
	  		if(valueunique == this.id_kelompok_jabatan){
	  			count_jumlah+=this.jumlah_peserta_berjalan;
	  			count_total+=this.total_peserta;
	  		}
	  	});
	  	$.each(responsedatakelompokjabatan, function(key){
	  		if(this.id==valueunique){
				obj.push({
					"id_kelompok_jabatan" : valueunique,
					"jumlah_peserta_berjalan" : count_jumlah,
					"total_peserta" : count_total,
					"kode_sesi" : $.urlParam('kode-sesi'),
					"kelompok_jabatan" : {"id" : valueunique, "kode" : this.kode}
				});
	  		}
	  	})
  	});

  	console.log(obj);
  	//
  	public_list_data_progress_peserta_ujian = obj;
  	$.each(obj, function(key){
  		if(selected_id_kelompok!=""){
	  		if(selected_id_kelompok==this.id_kelompok_jabatan){
	  			arr_total_gender_usia.push(this.jumlah_peserta_berjalan);
	  			total_gender_usia+=this.jumlah_peserta_berjalan;
	  		}
  		}else{
  			arr_total_gender_usia.push(this.jumlah_peserta_berjalan);
			total_gender_usia+=this.jumlah_peserta_berjalan;
  		}
  	});
  	generateChartProgressPesertaUjian();
  },'onGetListError');
},'onGetListError');
}

function displaySosiokultural(){
	var params="";
	var tbody = $("#tbl-sosiokultural").find('tbody');
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
//    if(selected_id_kelompok!=""){
//    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
//    	
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
////    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
//    }else{
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
////    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params='?filter=id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
//    }
//    if(params==""){
//    	params+='?filter=jenis_kompetensi.eq(\''+'SOSIOKULTURAL'+'\'),'+public_filter;
//    }else{
//    	params+=',jenis_kompetensi.eq(\''+'SOSIOKULTURAL'+'\'),'+public_filter;
//    }
//	if(selected_id_jenjang!=""){
//    	params+=',id_jenjang_master_jabatan.eq('+selected_id_jenjang+')';
//    }
//	if(selected_id_tingkatan!=""){
//    	params+=',id_tingkat_master_jabatan.eq('+selected_id_tingkatan+')';
//    }
//	params+='&order=id_unit_kompetensi';
//    ajaxGET(path + '/detil_perjabatan_kompetensi'+params+"&limit=10000000",'onGetListDetilSosiSuccess','onGetListDetilError');
    params+="filter_jenis_kompetensi=SOSIOKULTURAL&";
    if(selected_id_kelompok!="") params+='filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
    
	if(selected_id_unit_auditi!=""){
		params+='&filter_id_unit_auditi='+selected_id_unit_auditi;
	}
	if(selected_id_eselon1!=""){
		params+='&filter_id_eselon1='+selected_id_eselon1;
	}
	if(selected_id_tingkatan!="") params+='&filter_id_tingkat_jabatan='+selected_id_tingkatan+')';
	if(selected_id_jenjang!="") params+='&filter_id_jenjang_jabatan='+selected_id_jenjang+')';
	
    //params+=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
    
	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000",'onGetListDetilSosiSuccess','onGetListDetilError');
}

function displayManajerial(){
    var params="";
    var tbody = $("#tbl-manajerial").find('tbody');
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
//    if(selected_id_kelompok!=""){
//    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
//    	
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
////    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
//    }else{
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
////    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
////    	}
////    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
////    		params='?filter=id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
////    	}
//    }
//    if(params==""){
//    	params+='?filter=jenis_kompetensi.eq(\''+'MANAJERIAL'+'\'),'+public_filter;
//    }else{
//    	params+=',jenis_kompetensi.eq(\''+'MANAJERIAL'+'\'),'+public_filter;
//    }
//    if(selected_id_jenjang!="") params+=',id_jenjang_master_jabatan.eq('+selected_id_jenjang+')';
//	if(selected_id_tingkatan!="") params+=',id_tingkat_master_jabatan.eq('+selected_id_tingkatan+')';
//	
//	params+='&order=id_unit_kompetensi';
//    ajaxGET(path + '/detil_perjabatan_kompetensi'+params+"&limit=10000000",'onGetListDetilManaSuccess','onGetListDetilError');
    params+="filter_jenis_kompetensi=MANAJERIAL&";
    if(selected_id_kelompok!="") params+='filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
    
	if(selected_id_unit_auditi!=""){
		params+='&filter_id_unit_auditi='+selected_id_unit_auditi;
	}
	if(selected_id_eselon1!=""){
		params+='&filter_id_eselon1='+selected_id_eselon1;
	}
	if(selected_id_tingkatan!="") params+='&filter_id_tingkat_jabatan='+selected_id_tingkatan+')';
	if(selected_id_jenjang!="") params+='&filter_id_jenjang_jabatan='+selected_id_jenjang+')';
	
    //params+=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
    
	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000",'onGetListDetilManaSuccess','onGetListDetilError');
}

function displayTeknis(){
	var params="";
    var tbody = $("#tbl-teknis").find('tbody');
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
//    if(selected_id_kelompok!=""){
//    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
//    	
//    	if(selected_id_unit_auditi!="" && selected_id_eselon1!=""){
//    		params+=',id_unit_kerja_user_responden.eq('+selected_id_unit_auditi+')';
//    		params+=',id_eselon1_user_responden.eq('+selected_id_eselon1+')';
//    	}
//    	if(selected_id_unit_auditi!="" && selected_id_eselon1==""){
//    		params+=',id_unit_kerja_user_responden.eq('+selected_id_unit_auditi+')';
//    	}
//    	if(selected_id_unit_auditi=="" && selected_id_eselon1!=""){
//    		params+=',id_eselon1_user_responden.eq('+selected_id_eselon1+')';
//    	}
//    }else{
//    	if(selected_id_unit_auditi!="" && selected_id_eselon1!=""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+selected_id_unit_auditi+')';
//    		params+=',id_eselon1_user_responden.eq('+selected_id_eselon1+')';
//    	}
//    	if(selected_id_unit_auditi!="" && selected_id_eselon1==""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+selected_id_unit_auditi+')';
//    	}
//    	if(selected_id_unit_auditi=="" && selected_id_eselon1!=""){
//    		params='?filter=id_eselon1_user_responden.eq('+selected_id_eselon1+')';
//    	}
//    }
//    if(params==""){
//    	params+='?filter=jenis_kompetensi.in(\''+'TEKNIS_INTI'+'\',\''+'TEKNIS_PILIHAN'+'\'),'+public_filter;
//    }else{
//    	params+=',jenis_kompetensi.in(\''+'TEKNIS_INTI'+'\',\''+'TEKNIS_PILIHAN'+'\'),'+public_filter;
//    }
//    if(selected_id_jenjang!="") params+=',id_jenjang_master_jabatan.eq('+selected_id_jenjang+')';
//	if(selected_id_tingkatan!="") params+=',id_tingkat_master_jabatan.eq('+selected_id_tingkatan+')';
//    
//	params+='&order=id_unit_kompetensi';
//    ajaxGET(path + '/detil_perjabatan_kompetensi'+params+"&limit=10000000",'onGetListTeknisDetilSuccess','onGetListDetilError');
    params+="filter_jenis_kompetensi=TEKNIS&";
    if(selected_id_kelompok!="") params+='filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
    
	if(selected_id_unit_auditi!=""){
		params+='&filter_id_unit_auditi='+selected_id_unit_auditi;
	}
	if(selected_id_eselon1!=""){
		params+='&filter_id_eselon1='+selected_id_eselon1;
	}
	if(selected_id_tingkatan!="") params+='&filter_id_tingkat_jabatan='+selected_id_tingkatan+')';
	if(selected_id_jenjang!="") params+='&filter_id_jenjang_jabatan='+selected_id_jenjang+')';
	
    //params+=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000",'onGetListTeknisDetilSuccess','onGetListDetilError');
}

function onGetListDetilManaSuccess(response){
    $.LoadingOverlay("hide");
    console.log('response: ', response.data);
    var tbody="";  
    var row = "";
    var num = 1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[];
    $.each(response.data,function(key,value){
    	value.nilai=Math.floor(value.nilai);
        row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(num)+'</td>';
	        row += '<td class=""> '+value.nama_kelompok +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_standar +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
        row += '</tr>';
        num++;
        arrnama.push(value.nama_kelompok);
        arrstandar.push(value.nilai_standar);
        arrnilai.push(value.nilai);
    });
    myobj={
    		label : arrnama,
    		nilai_standar : arrstandar,
    		nilai : arrnilai
    };
    
    tbody = $("#tbl-manajerial").find('tbody');
	getRadarChart(myobj);
	if(row=="") tbody.html('<tr><td colspan="20"><div class="list-group-item text-center">Tidak Ada Data.</div></td></tr>');
	else tbody.html(row);
	
    console.log(myobj);
}

function onGetListDetilSosiSuccess(response){
    $.LoadingOverlay("hide");
    console.log('response: ', response.data);
    var tbody="";  
    var row = "";
    var num = 1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[];
    $.each(response.data,function(key,value){
    	value.nilai=Math.floor(value.nilai);
        row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(num)+'</td>';
	        row += '<td class=""> '+value.nama_kelompok +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_standar +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
        row += '</tr>';
        num++;
        arrnama.push(value.nama_kelompok);
        arrstandar.push(value.nilai_standar);
        arrnilai.push(value.nilai);
    });
    myobj={
    		label : arrnama,
    		nilai_standar : arrstandar,
    		nilai : arrnilai
    };

	tbody = $("#tbl-sosiokultural").find('tbody');
	if(row=="") tbody.html('<tr><td colspan="20"><div class="list-group-item text-center">Tidak Ada Data.</div></td></tr>');
	else  tbody.html(row);
	
    console.log(myobj);
}

function renderRowDetail(row){
	
}
	
function onGetListTeknisDetilSuccess(response){
    $.LoadingOverlay("hide");
    console.log('response: ', response.data);
    var tbody = $("#tbl-teknis").find('tbody');
    var row = "";
    var numi= 1, nump=1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[],arrjumlah=[];
    var count_inti=0, count_pilihan=0;
    var sum_inti=0, sum_pilihan=0;
    var idx_inti=0, idx_pilihan=0;

    $.each(response.data,function(key,value){
        if(value.jenis_kompetensi=="TEKNIS_INTI") count_inti++;
    });

    $.each(response.data,function(key,value){
        if(value.jenis_kompetensi=="TEKNIS_INTI"){
        	if(idx_inti==0){
        	    row += '<tr>';
        	    	row += '<td rowspan="'+(count_inti+1)+'" class="text-center">Kompetensi Inti</td>';
        	    	row += '<td class="text-center">No.</td>';
        	    	row += '<td class="text-center">Kode</td>';
        	    	row += '<td class="text-center">Nama Kompetensi</td>';
        	    	row += '<td class="text-center">Jumlah Responden</td>';
        	    	row += '<td class="text-center">Skor</td>';
        	    row += '</tr>';
        	}
            row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(numi)+'</td>';
	        row += '<td align="" class="text-center"> '+value.kode_unit_kompetensi +' </td>';
	        row += '<td class=""> '+value.nama_unit_kompetensi +' </td>';
	        row += '<td align="" class="text-center"> '+value.jumlah_unit_komp +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai+' </td>';
	        idx_inti++;
	        row += '</tr>';
		    numi++;
		    sum_inti+=value.nilai;

		    arrnama.push(value.kode_unit_kompetensi);
		    arrnilai.push(value.nilai);
		    arrjumlah.push(value.jumlah_unit_komp);
        };
    });
    row += '<tr>';
		row += '<td class="text-center"></td>';
		row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<th class="text-center">Rata-Rata</th>';
    	row += '<th class="text-center">'+(sum_inti/count_inti).toFixed(2)+'</th>';
    row += '</tr>';
    row += '<tr>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    row += '</tr>';
    //set nilai rata2 inti

    var obj_inti={
    		element : 'bar-inti',
    		element_container : 'container-bar-inti',
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };
    console.log(obj_inti);

    arrnama=[],arrstandar=[],arrnilai=[],arrjumlah=[];
    //
    var unique_kelompok_teknis=[];
    var unique_kelompok_teknis_with_string=[];
	$.each(response.data,function(key){
        if(this.jenis_kompetensi=="TEKNIS_PILIHAN"){
        	var valueunique = this.nama_kelompok;
			if(unique_kelompok_teknis.indexOf(valueunique) == -1) unique_kelompok_teknis.push(valueunique);
			if(unique_kelompok_teknis_with_string.indexOf(valueunique) == -1) unique_kelompok_teknis_with_string.push(valueunique);
			else unique_kelompok_teknis_with_string.push("");
        }
	});
//    //
	row += '<tr>';
	   	row += '<td class="text-center">Pilihan</td>';
	   	row += '<td class="text-center">No.</td>';
	   	row += '<td class="text-center">Kode</td>';
	   	row += '<td class="text-center">Nama Kompetensi</td>';
    	row += '<td class="text-center">Jumlah Responden</td>';
	   	row += '<td class="text-center">Skor</td>';
   	row += '</tr>';
    $.each(unique_kelompok_teknis,function(keyunique, valueuniqe){
    	$.each(response.data,function(key,value){
            if(value.jenis_kompetensi=="TEKNIS_PILIHAN"){
            	//start here
            	if(valueuniqe==value.nama_kelompok){
            		if(unique_kelompok_teknis_with_string[idx_pilihan]!=""){
//	    	        if(nump==1 && idx_pilihan>0){
	    	        	row += '<tr>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<th class="text-center">Rata-Rata</th>';
	            	    	row += '<th class="text-center">'+(sum_pilihan/count_pilihan).toFixed(2)+'</th>';
	            	    row += '</tr>';
	            	    row += '<tr>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                row += '</tr>';
		    	        sum_pilihan=0;
		    		    count_pilihan=0;
	    	        }
	                row += '<tr class="data-row" id="row-'+value.id+'">';
	    	        //
	                row += '<td class="text-center">'+unique_kelompok_teknis_with_string[idx_pilihan]+'</td>';
	    	        //
	    	        row += '<td class="text-center">'+(nump)+'</td>';
	    	        row += '<td align="" class="text-center"> '+value.kode_unit_kompetensi +' </td>';
	    	        row += '<td class=""> '+value.nama_unit_kompetensi +' </td>';
	    	        row += '<td align="" class="text-center"> '+value.jumlah_unit_komp +' </td>';
	    	        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
	    	        
	    	        row += '</tr>';
	    	        nump++;
	    	        idx_pilihan++;
	    		    sum_pilihan+=value.nilai;
	    		    count_pilihan++;

	    		    arrnama.push(value.kode_unit_kompetensi);
	    		    arrnilai.push(value.nilai);
	    		    arrjumlah.push(value.jumlah_unit_komp);
            	}else{
	    	        nump=1;
            	}
    		    //end here
            }
        });
    });

	row += '<tr>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<th class="text-center">Rata-Rata</th>';
    	row += '<th class="text-center">'+(sum_pilihan/count_pilihan).toFixed(2)+'</th>';
    row += '</tr>';
    tbody.html(row);
//    myobj={
//    		label : arrnama,
//    		nilai : arrnilai,
//    		jumlah : arrjumlah
//    };
//    console.log(myobj);
//    getBarChart(myobj);

    
    var obj_pilihan={
    		element : 'bar-pilihan',
    		element_container : 'container-bar-pilihan',
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };
    console.log(obj_pilihan);
    var newobj = {obj_inti, obj_pilihan};
    console.log(newobj);
    getHorizontalBarChart(newobj);
}

function getHorizontalBarChart(data){
	// scale second chart based on ratio of data to the first
    var fiddleFactor = 1.1; // determined by guesswork
    var ratio = data.obj_pilihan.label.length * fiddleFactor / data.obj_inti.label.length;
    var container1Height = parseInt(document.getElementById(data.obj_inti.element_container).style.height);
    // scale height of second chart
    document.getElementById(data.obj_pilihan.element_container).style.height = container1Height * ratio + 'px';
    
    if(data.obj_inti.element_container=="container-bar-inti"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_inti.nilai, function(key){
    		if(this < 60){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});
    	resetCanvas(data.obj_inti);

    	var data_inti = data.obj_inti;
    	if(data_inti.jumlah.length < 3 ){
    		$('#container-bar-inti').attr('style', 'height:300px;');
        }else if(data_inti.jumlah.length < 5 ){
        	$('#container-bar-inti').attr('style', 'height:400px;');
    	}else{
  		  	$('#container-bar-inti').attr('style', 'height:'+(50*(data_inti.jumlah.length))+'px;');
    	}
		  
    	var ctx = document.getElementById(data.obj_inti.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: {
    	    labels: data.obj_inti.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_inti.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:16,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            fontColor: 'black',
				            fontStyle: 'normal',
				            maxTicksLimit: 5,
				            paddingLeft: 20,
				            //mirror: true,
				          },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
									ctx.fillStyle = "#fff";
    								ctx.fillText(data.obj_inti.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
    							} else {
    								if(newdata == 0) {
    									ctx.fillStyle = "#dc3545";
    									ctx.fillText(data.obj_inti.jumlah[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
    								}else{
    									ctx.fillStyle = "#fff";
    									ctx.fillText(data.obj_inti.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend1").html(myChart.generateLegend());
    }
    
    if(data.obj_pilihan.element_container=="container-bar-pilihan"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_pilihan.nilai, function(key){
    		if(this < 60){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});
    	resetCanvas(data.obj_pilihan);
    	var data_pilihan = data.obj_pilihan;
    	if(data_pilihan.jumlah.length < 3 ){
  		  $('#container-bar-pilihan').attr('style', 'height:200px;');
      	}else if(data_pilihan.jumlah.length < 5 ){
		  $('#container-bar-pilihan').attr('style', 'height:400px;');
    	}else{
    		$('#container-bar-pilihan').attr('style', 'height:'+(50*(data_pilihan.jumlah.length))+'px;');
    	}
    	
    	var ctx = document.getElementById(data.obj_pilihan.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: {
    	    labels: data.obj_pilihan.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_pilihan.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:16,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            fontColor: 'black',
				            fontStyle: 'normal',
				            maxTicksLimit: 5,
				            paddingLeft: 20,
				            //mirror: true,
				          },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
    								ctx.fillStyle = "#fff";
    								ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
    							} else {
    								if(newdata == 0) {
    									ctx.fillStyle = "#dc3545";
    									ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
    								}else{
    									ctx.fillStyle = "#fff";
    									ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend2").html(myChart.generateLegend());
    }
}

function displayDemografi(){
	var params="";
    if(selected_id_kelompok!=""){
    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
    	
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    }else{
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
    		params='?filter=id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    }
	if(params==""){
		params+='?filter='+public_filter
	}else{
		params+=','+public_filter
	}
    ajaxGET(path + '/detil_perjabatan'+params+"&limit=10000000",'onGetDisplayDemografiSuccess','onGetListDetilError');
}

function onGetDisplayDemografiSuccess(response){
    console.log(response);

    var arrtotaljumlah=0;
    
    var arrnama=[];
    var arrjumlah=[];
    
    var arrnama_keterampilan=[];
    var arrjumlah_keterampilan=[];

    var jenjang_unique=[];
    var jenjang_unique_keterampilan=[];
    $.each(response.data, function(key){
    	if(this.id_tingkat_master_jabatan==2){
    		var valueunique=this.nama_jenjang_master_jabatan;
			if(jenjang_unique.indexOf(valueunique) == -1) jenjang_unique.push(valueunique);
    	}
    	if(this.id_tingkat_master_jabatan==1){
    		var valueunique=this.nama_jenjang_master_jabatan;
			if(jenjang_unique_keterampilan.indexOf(valueunique) == -1) jenjang_unique_keterampilan.push(valueunique);
    	}
    });
    $.each(jenjang_unique,function(keyunique, valueuniqe){
	    arrnama.push(valueuniqe);
	    var count=0;
		    $.each(response.data,function(key,value){
		    	if(value.id_tingkat_master_jabatan==2){
		        	if(valueuniqe==value.nama_jenjang_master_jabatan){
		        		count++;
		        	}
		    	}
		    });
		    arrjumlah.push(count);
		    arrtotaljumlah+=count;
    });
    $.each(jenjang_unique_keterampilan,function(keyunique, valueuniqe){
    	arrnama_keterampilan.push(valueuniqe);
	    var count=0;
		    $.each(response.data,function(key,value){
		    	if(value.id_tingkat_master_jabatan==1){
		        	if(valueuniqe==value.nama_jenjang_master_jabatan){
		        		count++;
		        	}
		    	}
		    });
		    arrjumlah_keterampilan.push(count);
		    arrtotaljumlah+=count;
    });
  //render chart keterampilan
    renderChartKeterampilan(arrnama_keterampilan, arrjumlah_keterampilan, arrtotaljumlah);

	//render chart keahlihan
    renderChartKeahlihan(arrnama, arrjumlah, arrtotaljumlah);
	
	//ini data chart usia
	var arrlabelkat=["20-29", "30-39", "40-49", "50-59", "60+"];
	var arrkat=[
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0}
	];
	
	var countAll=0;
	var countL=0, countP=0;
    $.each(response.data,function(key,value){
    	if(value.umur_user_responden <= 29){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[0].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[0].P+=1;
    		}
    	}
    		
    	if(value.umur_user_responden > 29 && value.umur_user_responden <= 39){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[1].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[1].P+=1;
    		}
    	}
    	
    	if(value.umur_user_responden > 39 && value.umur_user_responden <= 49){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[2].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[2].P+=1;
    		}
    	}

    	if(value.umur_user_responden > 49 && value.umur_user_responden <= 59){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[3].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[3].P+=1;
    		}
    	}

    	if(value.umur_user_responden > 59){
    		if(value.jenis_kelamin_user_responden=="L") {
    			countL++;
    			arrkat[4].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			countP++;
    			arrkat[4].P+=1;
    		}
    	}
    	countAll++;
    });

	//render chart usia
	renderChartUsia({"label" : arrlabelkat, "jumlah" : arrkat, "total" : countAll});
}

function renderChartKeterampilan(arrnama_keterampilan, arrjumlah_keterampilan, total){
	console.log(arrjumlah_keterampilan);
	$('#bar1').remove();
	$('#ContainerBar1').append('<canvas class="mw-100" width="425" height="425" id="bar1"><canvas>');
	var canvas = document.querySelector('#bar1'); // why use jQuery?
	var ctx = canvas.getContext('2d');
	var x = canvas.width;
	var y = canvas.height;
	ctx.font = '10pt Verdana';
	ctx.textAlign = 'center';
	ctx.fillText('This text is centered on the canvas', x, y);
	//end here
	
	var data =[];
	$.each(arrjumlah_keterampilan, function(key){
		data.push(((this/total)*100).toFixed(2));
	});
	
	var ctx = document.getElementById("bar1");
	//ctx.height = 400;
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
	    labels: arrnama_keterampilan,//["Pemula", "Terampil", "Mahir", "Penyelia"],
	    datasets: [{
    		label: 'Jumlah',
	        data: data,//arrjumlah_keterampilan,
	        backgroundColor: "#3494BA", // biru
	        hoverBackgroundColor: "#5da0ba" // biru lebih muda
	    }]
		},
		options: getBarOption(arrjumlah_keterampilan),
	});
}
function renderChartKeahlihan(arrnama, arrjumlah, total){
	console.log(arrjumlah);
	//
	$('#bar2').remove();
	$('#ContainerBar2').append('<canvas class="mw-100" width="425" height="425" id="bar2"><canvas>');
	var canvas = document.querySelector('#bar2'); // why use jQuery?
	var ctx = canvas.getContext('2d');
	var x = canvas.width;
	var y = canvas.height;
	ctx.font = '10pt Verdana';
	ctx.textAlign = 'center';
	ctx.fillText('This text is centered on the canvas', x, y);
	//end here
	var data =[];
	$.each(arrjumlah, function(key){
		data.push(((this/total)*100).toFixed(2));
	});
    var ctx = document.getElementById("bar2");
	//ctx.height = 400;
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
	    labels: arrnama,//["Muda", "Madya", "Utama"],
	    datasets: [{
    		label: 'Jumlah',
	        data: data,//arrjumlah,//data1,
	        backgroundColor: "#3494BA", // biru
	        hoverBackgroundColor: "#5da0ba" // biru lebih muda
	    }]
		},
		options: getBarOption(arrjumlah),
	});
}

function renderPercentage(value, numeric) {
	if(numeric == 0) return 0.5;
	return (value < 3 ? 3 : value);
}

function renderChartUsia(objnew){
	console.log(objnew);
	/*
	const chartBar = response.data.chartBar.percentage
	const chartBarNumeric = response.data.chartBar.numeric

	var color = Chart.helpers.color;
    var context1 = document.getElementById("chart-bar").getContext("2d");
    
    let dataChart = [
    	[
    		(chartBarNumeric.rekomendasi_semua).formatMoney(0, ',', '.'), 
    		(chartBarNumeric.rekomendasi_belum_selesai).formatMoney(0, ',', '.'), 
    		(chartBarNumeric.rekomendasi_sudah_selesai).formatMoney(0, ',', '.')
    	],
    	[
    		'Rp.' + (chartBarNumeric.kn_rp_1).formatMoney(0, '.', ','), 
    		'Rp.' + (chartBarNumeric.kn_rp_2).formatMoney(0, '.', ','), 
    		'Rp.' + (chartBarNumeric.kn_rp_3).formatMoney(0, '.', ',')
    	],
    	[
    		'$' + (chartBarNumeric.kn_dollar_1).formatMoney(0, '.', ','), 
    		'$' + (chartBarNumeric.kn_dollar_2).formatMoney(0, '.', ','), 
    		'$' + (chartBarNumeric.kn_dollar_3).formatMoney(0, '.', ',')
    	],
    ]
    */
	/*********** begin chart bar ***********/ 
	/*var yearnow=new Date().getFullYear();
    window.myBar = new Chart(context1, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Progress Tindak Lanjut Hasil Audit '+(yearnow-5)+" s.d "+yearnow
            },
            scales: {
                yAxes: [{
                ticks: {
                       min: 0,
                       max: 100,
                       callback: function(value){return value+ "%"}
                    },  
    				scaleLabel: {
                       display: true,
                       labelString: "Percentage"
                    }
                }]
            },
            tooltips: {
                callbacks: {
                  label: function(tooltipItem, data) {
                  	var dataset = data.datasets[tooltipItem.datasetIndex];
                    return ' ' + dataset.label + ': ' + dataChart[tooltipItem.index][tooltipItem.datasetIndex];
                  }
                }
           }
        }
    });
    */
	/*********** end chart bar ***********/
	//
	$('#barHorizontal').remove();
	$('#ContainerBarHorizontal').append('<canvas class="mw-100" width="425" height="425" id="barHorizontal"><canvas>');
	var canvas = document.querySelector('#barHorizontal'); // why use jQuery?
	var ctx = canvas.getContext('2d');
	//var x = canvas.width/2;
	//var y = canvas.height/2;
	//ctx.font = '16pt Verdana';
	//ctx.textAlign = 'center';
	//ctx.fillText('This text is centered on the canvas', x, y);
	//end here
	
	// Bar Horizontal
	var datalabel = objnew.label;

	var data1 =[];
	var data2 =[];
	$.each(objnew.jumlah, function(key){
		data1.push(((this.L/objnew.total)*100).toFixed(2));
		data2.push(((this.P/objnew.total)*100).toFixed(2));
		console.log("data2: "+((this.P)+"/"+objnew.total));///objnew.total)*100
	});
	console.log(data2);
	var color = Chart.helpers.color;
	
	var data1_custom = [];
	$.each(data1, function(key, value){
		data1_custom.push(value*-1);
	});
	//
	//renderPercentage(chartBar.rekomendasi_semua, chartBarNumeric.rekomendasi_semua)
	//
	var ctx = document.getElementById("barHorizontal");
	//ctx.height = 300;
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
	    labels: datalabel,
	    datasets: [{
			label: 'Laki-Laki',
		    data: data1_custom,
			backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
			borderColor: window.chartColors.blue,
	    },{
			label: 'Perempuan',
		    data: data2,
			backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
			borderColor: window.chartColors.red,
	    }]
		},
		options: 
		{
			maintainAspectRatio: true,
			tooltips: { 
				enabled: true,
				//bodySpacing : 8,
				//titleSpacing : 15, titleMarginBottom : 8, yPadding : 12
				callbacks: {
	                label: function(tooltipItem, data) {
	                	console.log(tooltipItem);
	                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

	                    if (label) {
	                        label += ': ';
	                    }
	                    //label += Math.round(tooltipItem.yLabel * 100) / 100;
	                    if(tooltipItem.xLabel < 0){
	                    	label += Math.round(parseInt((tooltipItem.xLabel*objnew.total))/100).toString().replaceAll("-","");
//	                    	label += tooltipItem.xLabel.toString().replaceAll("-","")+"%";
	                    	//label += tooltipItem.xLabel.toString().replaceAll("-","")+"%";
	                    }else{
	                    	label += Math.round(parseInt((tooltipItem.xLabel*objnew.total))/100);
	                    	//label += tooltipItem.xLabel+"%";
	                    }
	                    /*
						if(data >= 0) {
							ctx.fillText(Math.ceil(parseInt((data*objnew.total))/100), bar._model.x+35, bar._model.y);
						}else{
							data=data.toString().replaceAll("-","")
							ctx.fillText(Math.ceil(parseInt((data*objnew.total))/100), bar._model.x-10, bar._model.y);
						}
	                    */
	                    return label;
	                }
	            }
			},
			hover :{ animationDuration: 0 },
			legend:{ display: true, position: 'bottom' },
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontFamily: "'verdana', sans-serif",
						fontSize:11,
	                    min: -100,
	                    max: 100,
	                    userCallback: function(value) {
	                        if(value < 0){
	                        	value = value.toString().replaceAll("-","")
	                        }
	                        if(value=="25"){
	                        	value+=' %';
	                        }
	                        return value;
	                    },
	                    stepSize: 15,
	                    padding: 17
					},
					scaleLabel:{ display:false },
					gridLines: { }, 
					stacked: true
				}],
				yAxes: [{
					gridLines: {
						display:false,
						color: "#fff",
						zeroLineColor: "#fff",
						zeroLineWidth: 0
					},
					ticks: {
						fontFamily: "'verdana', sans-serif",
						fontSize:13
					},
					stacked: true,
				}]
			},
			animation: {
				onComplete: function () {
					var chartInstance = this.chart;
					var ctx = chartInstance.ctx;
					ctx.textAlign = "right";
					ctx.font = "14px verdana";
					ctx.fillStyle = "#516b73";

					ctx.textBaseline = "bottom";
					
					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						Chart.helpers.each(meta.data.forEach(function (bar, index) {
							console.log(dataset.data[index]);
							//data = parseFloat((dataset.data[index]).toFixed(2)*100);
							data = dataset.data[index];
							//data = parseFloat((dataset.data[index] / (data1[index] + data2[index])).toFixed(2)*100);
							//console.log(data);
//							if(data >= 0) {
//								ctx.fillText(data + '%', bar._model.x-10, bar._model.y);
//							} else {
//								ctx.fillText(data + '%', bar._model.x+50, bar._model.y);
//							}
							//
							//tooltipItem.xLabel.toString().replaceAll("-","")+"%";
							//
							if(data >= 0) {
								ctx.fillText(data+"%", bar._model.x+50, bar._model.y);
							}else{
								data=data.toString().replaceAll("-","")
								ctx.fillText(data+"%", bar._model.x-5, bar._model.y);
							}
							
						}),this)
					}),this);
				}
			},
			pointLabelFontFamily : "Quadon Extra Bold",
			scaleFontFamily : "Quadon Extra Bold",
		}	
	});
	
}


function generateChart(data) {
  try {
    var ctx = document.getElementById("monthlyChart");
    if (ctx) {
      ctx.height = 100;
      chartBar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          legend: { position: 'top', labels: { fontFamily: 'Poppins' } },
          scales: { xAxes: [{ ticks: { fontFamily: "Poppins" } }], yAxes: [{ ticks: { beginAtZero: true, fontFamily: "Poppins" } }] }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }

};