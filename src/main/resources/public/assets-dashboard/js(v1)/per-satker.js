var path = ctx + '/report';
var path_inventori = ctx + '/inventori';

var chartBar = null;

window.isMobile = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

function init(){
	
	// stacked group
	var data1 = [21, 30, 30, 34];

	var ctx = document.getElementById("bar1");
	ctx.height = 350;
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
	    labels: ["Pemula", "Terampil", "Mahir", "Penyelia"],
	    datasets: [{
    		label: 'Kompetensi Teknis',
	        data: data1,
	        backgroundColor: "#3494BA", // biru
	        hoverBackgroundColor: "#5da0ba" // biru lebih muda
	    }]
		},
		options: getBarOption(data1),
	});

	var ctx = document.getElementById("bar2");
	ctx.height = 350;
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
	    labels: ["Muda", "Madya", "Utama"],
	    datasets: [{
    		label: 'Kompetensi Teknis',
	        data: data1,
	        backgroundColor: "#3494BA", // biru
	        hoverBackgroundColor: "#5da0ba" // biru lebih muda
	    }]
		},
		options: getBarOption(data1),
	});
		

	// stacked group
	var data1 = [21, 30, 30, 34, 18];
	var data2 = [19, 20, 33, 36, 20];

	var ctx = document.getElementById("bar");
	ctx.height = 300;
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
	    labels: ["PEH", "PEDAL", "POLHUT", "PK", "PLH"],
	    datasets: [{
    		label: 'Kompetensi Teknis',
        data: data1,
        backgroundColor: "#4A9B82",
        hoverBackgroundColor: "#70c2a9"
	    }]
		},
		options: getBarOptionStacked(data1, data2),
	});
	
	
	
	
	// Bar Horizontal
	var data1 = [19, 20, 33, 36, 20];
	var data2 = [21, 30, 30, 34, 18];
	var color = Chart.helpers.color;
	
	var data1_custom = [];
	$.each(data1, function(key, value){
		data1_custom.push(value*-1);
	});
	
	var ctx = document.getElementById("barHorizontal");
	ctx.height = 300;
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
	    labels: ["PEH", "PEDAL", "POLHUT", "PK", "PLH"],
	    datasets: [{
			label: 'Laki-Laki',
		    data: data1_custom,
			backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
			borderColor: window.chartColors.blue,
	    },{
			label: 'Perempuan',
		    data: data2,
			backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
			borderColor: window.chartColors.red,
	    }]
		},
		options: getBarOptionStacked(data1, data2),
	});
	
	getRadarChart();
		

    
    
    
    getListTahun('Tahun: ', new Date().getFullYear());
    $('#nama_tahun').html(new Date().getFullYear());
    //ajaxGET(path_inventori + '/chart/'+new Date().getFullYear(),'onGetTahunSuccess','onGetTahunError');
    $('[name=filter_tahun]').change(function(){
        $('#nama_tahun').html($(this).val());
        //ajaxGET(path_inventori + '/chart/'+$(this).val(),'onGetTahunSuccess','onGetTahunError');
    });
    
    // urutkan nomor di #tbl-laporan
    $.each($('#tbl-laporan').find('tbody').find('tr'), function(key){ $(this).find('td:first-child').text(key+1);});
    //ajaxGET(path + '/rekap/laporan','onGetRekapLaporanSuccess','onGetRekapLaporanError');
    
    // tampilin taksa
    display();
}

function getBarOption(data1) {
	return {
		maintainAspectRatio: false,
   		barThickness: 1, 
		tooltips: { enabled: true },
		hover :{ animationDuration: 0 },
		legend:{ display: false, position: 'bottom' },
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero:true,
					fontFamily: "'verdana', sans-serif",
					fontSize:12
				},
				scaleLabel:{ display:false },
				gridLines: { }
			}],
			yAxes: [{
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:10,
                    beginAtZero:true,
                    min: 0,
                    max: 100
				},
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				ctx.textAlign = "right";
				ctx.font = "16px verdana";
				ctx.fillStyle = "#000";
				
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						data = parseFloat((dataset.data[index] /* / (data1[index] + data2[index])*/).toFixed(2)*1) + '%';
						ctx.fillText(data, bar._model.x+20, bar._model.y-15);
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}

function getStackedGroup() {
}


function randomScalingFactor() {
	return Math.round(Math.random() * 10);
};

function getRadarChart() {

	var color = Chart.helpers.color;
	var config = {
		type: 'radar',
		data: {
			labels: ['Integrity', 'Ability to Change', 'Planning Organizing', 'Comm Skills', 'Relation Building', 'Analitic Thinking', 'Leadership', 'Teamwork', 'Culture Knowledge'],
			datasets: [{
				label: 'Nilai Pemetaan',
				backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
				borderColor: window.chartColors.green,
				pointBackgroundColor: window.chartColors.green,
				data: [ randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor() ]
			}, {
				label: 'Nilai Standard',
				backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
				borderColor: window.chartColors.blue,
				pointBackgroundColor: window.chartColors.blue,
				data: [ randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor() ]
			}]
		},
		options: {
			legend: {
				position: 'bottom',
			},
			scale: {
				ticks: {
					beginAtZero: true
				}
			}
		}
	};

	var ctx = document.getElementById("radar");
	ctx.height = isMobile() ? 200 : 200;
	var myChart = new Chart(ctx, config);

	var colorNames = Object.keys(window.chartColors);
		
}

function addPercentage(canvas, ctx, data){
    var cx = canvas.width / 4;
    var cy = canvas.height / 4;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '12px verdana';
    ctx.fillStyle = 'black';
    ctx.fillText(parseInt((data[0]/(data[0]+data[1]))*100)+"%", cx, cy);
}














function onGetRekapLaporanSuccess(response){
    console.log('rekapitulasi-laporan: ', response.data);
    $.each(response.data, function(key, value) {
        //$('[name=id_disposal]').append(new Option(value.nama, value.id));
        if(value.id == 1) $('#jumlahLaporanCatatanKandang').text(value.jumlah);
        if(value.id == 2) $('#jumlahLaporanCatatanKesehatan').text(value.jumlah);
        if(value.id == 3) $('#jumlahLaporanPenerimaanSatwa').text(value.jumlah);
        if(value.id == 4) $('#jumlahLaporanPengeluaranSatwa').text(value.jumlah);
    });
}


function display(){
    
    var tbody = $("#tbl-data").find('tbody');
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
    
    //ajaxGET(path + '/rekap/taksa','onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
    console.log('rekapitulasi-satwa: ', response.data);
    var tbody = $("#tbl-data").find('tbody');
    
    var row = "";
    var num = 1;
    var hidup = 0;
    var sakit = 0;
    var mati = 0;
    var dalam = 0;
    var titip = 0;
    var keluar = 0;
    $.each(response.data,function(key,value){
        row += '<tr class="data-row" id="row-'+value.id+'">';
        row += '<td class="text-center">'+(num)+'</td>';
        row += '<td class=""> '+value.nama +' </td>';
        row += '<td align="right" class=""> '+value.hidup +' </td>';
        row += '<td align="right" class=""> '+value.sakit +' </td>';
        row += '<td align="right" class=""> '+value.mati +' </td>';
        //row += '<td align="right" class=""> '+value.titip +' </td>'
        row += '<td align="right" class=""> '+value.titip +' </td>';
        row += '<td align="right" class=""> '+value.keluar +' </td>';
        row += '</tr>';
        
        hidup += value.hidup;
        sakit += value.sakit;
        mati += value.mati;
        dalam += value.titip;
        titip += value.titip;
        keluar += value.keluar;
        
        num++;
    });
    
    tbody.html(row);
    
    var tfoot = $("#tbl-data").find('tfoot');
    
    row = '<tr class="data-row" id="row-total">';
    row += '<td align="center" colspan="2"><strong>JUMLAH TOTAL</strong></td>';
    row += '<td align="right" class=""> '+hidup +' </td>';
    row += '<td align="right" class=""> '+sakit +' </td>';
    row += '<td align="right" class=""> '+mati +' </td>';
    row += '<td align="right" class=""> '+titip +' </td>';
    row += '<td align="right" class=""> '+keluar +' </td>';
//    row += '<td align="right" class=""> '+dalam +' </td>';
    row += '</tr>';
    
    tfoot.html(row);
    
}

function onGetTahunSuccess(response){
    console.log('chart-tahunan: ', response.data);
    var MONTHS = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var color = Chart.helpers.color;
    var data = {
        labels: MONTHS,
        datasets: [
          {
            label: "Satwa Masuk",
            data: response.data.penerimaan,
            backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
            borderColor: window.chartColors.yellow,
            borderWidth: 1
          },
          {
            label: "Satwa Keluar",
            data: response.data.pengeluaran,
            backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
            borderColor: window.chartColors.green,
            borderWidth: 1
          }
        ]
      }
    if(chartBar == null) generateChart(data);
    else { chartBar.data = data; window.chartBar.update(); }
}

function generateChart(data) {
  try {
    var ctx = document.getElementById("monthlyChart");
    if (ctx) {
      ctx.height = 100;
      chartBar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          legend: { position: 'top', labels: { fontFamily: 'Poppins' } },
          scales: { xAxes: [{ ticks: { fontFamily: "Poppins" } }], yAxes: [{ ticks: { beginAtZero: true, fontFamily: "Poppins" } }] }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }

};