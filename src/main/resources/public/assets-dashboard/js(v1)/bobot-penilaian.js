var path_unsur = ctx + '/unsur';
var path_maturitas_penilaian_persiapan = ctx + '/maturitas-penilaian-persiapan';
var path_maturitas_persiapan = ctx + '/maturitas-persiapan';
var params = '';

var jenis = '';
var list_unit_auditi = [];

var list_maturitas_persiapan = [];
var list_maturitas_penilaian_persiapan = [];

function init(){

	$.getJSON(path_maturitas_persiapan+'/list', function(response) {
		list_maturitas_persiapan = response.data;
	});

	jenis = $.urlParam('jenis');
	jenis = jenis.split(' ')[0];
	$('[id=menu]').html($.urlParam('menu'));
	$('#menu').html($.urlParam('menu').split(' ')[2]);
	
	getListTahun(null, moment().format('YYYY'));
	$('[name=filter_tahun]').change(function(){
		getUnitAuditiSummaryList($(this).val());
		display();
	});
	getUnitAuditiSummaryList(moment().format('YYYY'));
    
    $('#btn-download').click(function(e){
    	params = $( "#search-form" ).serialize();
    	params += 'filter_tahun='+$('[name=filter_tahun]').val();
    	params += '&filter_id_unit_auditi='+$('[name=filter_id_unit_auditi]').val();
    	params += '&filter_jenis='+jenis.toUpperCase();
        location = path_unsur + '/download/bobot-penilaian?'+params;
    });
	
	$('#search-form').submit(function(e){
		display();
		e.preventDefault();
	});
	
}

function display(index = 'all', page = 1){
		
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){ tbody.text(''); }
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');

	console.log('[display] filter_tahun: ', $('[name=filter_tahun]').val());
	console.log('[display] filter_id_unit_auditi: ', $('[name=filter_id_unit_auditi]').val());
	
	if($('[name=filter_tahun]').val() == '' || $('[name=filter_id_unit_auditi]').val() == '') {
//		$.each(list_unsur,function(key,unsur){
//			$('#tbl').find('tbody').html('<tr><td colspan="10"><p class="text-center pt-3 pb-3">Silahkan pilih <b>Tahun</b> dan <b>Unit Auditi</b> terlebih dahulu.</p></td></tr>');
//		});
		console.log('=> ada yg kosong jadi tidak tampil');
		return false;
	}
	
	params = $( "#search-form" ).serialize();
	params += '&filter_jenis='+jenis.toUpperCase();
	params += '&filter_tahun='+$('[name=filter_tahun]').val();
	params += '&filter_id_unit_auditi='+$('[name=filter_id_unit_auditi]').val();
	

	console.log(path_maturitas_penilaian_persiapan + '/list?'+params);
	ajaxGET(path_maturitas_penilaian_persiapan + '/list?'+params,function(response){
		list_maturitas_penilaian_persiapan = response.data;
		console.log('list_maturitas_penilaian_persiapan: ', list_maturitas_penilaian_persiapan);

		console.log(path_unsur + '/list?'+params);
		ajaxGET(path_unsur + '/list?'+params,'onGetListSuccess','onGetListError');
		
	},'onGetListError');
		
	current_page = page;

}

function onGetListSuccess(response){
	
	if(response.data.length == 0) return;
	
	var list_unsur = response.data;
	console.log('response.data: ', list_unsur);

    var tbody = $('#tbl').find($('tbody'));
	
	var sumA = 0;
	$.each(list_maturitas_persiapan, function(key, value){
    	var tingkat = 0;
    	$.each(list_maturitas_penilaian_persiapan, function(key2, value2){
    		if(value.id == value2.id_maturitas_persiapan){
    			tingkat = value2.tingkat;
    			return false;
    		}
    	});
    	sumA += getValue(value.bobot, 0);
//		sumA += (getValue(value.bobot, 0) * getValue(tingkat, 0));
	});
    
    tbody.text('');
    tbody.append('<tr><td colspan="20" align="center">Loading data....</td></tr>');
    
    var row = '';
    
    row += '<tr>';
    	row += '<td class="font-weight-bold" style="width: 50px">A</td>';
		row += '<td colspan="2" class="font-weight-bold">Persiapan Penyelenggaraan SPIP</td>';
		row += '<td class="text-center font-weight-bold">'+sumA+'</td>';
		row += '<td></td><td></td><td></td>';
	row += '</tr>';
    
    $.each(list_maturitas_persiapan, function(key, value) {
    	var tingkat = 0;
    	$.each(list_maturitas_penilaian_persiapan, function(key2, value2){
    		if(value.id == value2.id_maturitas_persiapan){
    			tingkat = value2.tingkat;
    			return false;
    		}
    	});
        row += '<tr>';
	        row += '<td class="text-center"></td>';
	        row += '<td class="text-right font-weight-bold" style="width: 70px">'+(key+1)+'</td>';
	        row += '<td>'+value.nama+'</td>';
	        row += '<td class="text-center"></td>';
	        row += '<td class="text-center">'+getValue(value.bobot, 0)+'</td>';
    		row += '<td class="text-center">'+getValue(tingkat, 0)+'</td>';
    		row += '<td class="text-center">'+(getValue(value.bobot, 0) * getValue(tingkat, 0))+'</td>';
        row += '</tr>';
        i++;
    });

    row += '<tr>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-right font-weight-bold" style="width: 70px"></td>';
    	row += '<td>Jumlah Persiapan Penyelenggaraan SPIP</td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
	row += '</tr>';
	

	var sumB = 0;
    $.each(list_unsur, function(key1, value1) {
    	$.each(value1.list_sub_unsur, function(key2, value2) { 
    		sumB += getValue(value2.bobot, 0);
//    		sumB += (getValue(value2.bobot, 0) * getValue(value2.tingkat, 0));
		});
    });

	row += '<tr>';
		row += '<td class="font-weight-bold" style="width: 50px">B</td>';
		row += '<td colspan="2" class="font-weight-bold">Pelaksanaan Sub Unsur SPIP</td>';
		row += '<td class="text-center font-weight-bold">'+sumB+'</td>';
		row += '<td class="text-center"></td>';
		row += '<td class="text-center"></td>';
		row += '<td class="text-center"></td>';
	row += '</tr>';
    
    $.each(list_unsur, function(key1, value1) {
    	var sum = 0; $.each(value1.list_sub_unsur, function(key2, value2) { sum += (getValue(value2.bobot, 0) * getValue(value2.tingkat, 0)); });
        row += '<tr>';
	        row += '<td class="text-right">'+(key1+1)+'</td>';
	        row += '<td colspan="2" class="font-weight-bold">'+value1.nama+'</td>';
	        row += '<td class="text-center">'+sum+'</td>';
	        row += '<td class="text-center"></td>';
	        row += '<td class="text-center"></td>';
	        row += '<td class="text-center"></td>';
        row += '</tr>';
        $.each(value1.list_sub_unsur, function(key2, value2) {
        	row += '<tr>';
        		row += '<td class="text-center"></td>';
        		row += '<td class="text-right">'+(key2+1)+'</td>';
        		row += '<td>'+value2.nama+'</td>';
        		row += '<td class="text-center"></td>';
        		row += '<td class="text-center">'+getValue(value2.bobot, 0)+'</td>';
        		row += '<td class="text-center">'+getValue(value2.tingkat, 0)+'</td>';
        		row += '<td class="text-center">'+(getValue(value2.bobot, 0) * getValue(value2.tingkat, 0))+'</td>';
    		row += '</tr>';
        });
        i++;
    });
    
    tbody.html(row);
	
}
