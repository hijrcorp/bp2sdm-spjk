var path_unsur = ctx + '/unsur';
var path_sub_unsur = ctx + '/sub-unsur';
var path_maturitas_tingkat = ctx + '/maturitas-tingkat';
var path_maturitas_parameter = ctx + '/maturitas-parameter';
var path_maturitas_penilaian = ctx + '/maturitas-penilaian';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

var jenis = '';
var list_unsur = [];
var list_sub_unsur = [];
var list_maturitas_penilaian = [];

function init(){
	
	jenis = $.urlParam('jenis');
	jenis = jenis.split(' ')[0];
	$('[id=menu]').html($.urlParam('menu'));
	$('#menu').html($.urlParam('menu').split(' ')[2]);
	ajaxGET(path_unsur + '/list?filter_jenis='+jenis,function(response) {
		var list_sub_unsur = [];
		$.each(response.data, function(key, value){ $.each(value.list_sub_unsur, function(key, sub_unsur){ list_sub_unsur.push(sub_unsur); }); });
		list_unsur.push({ id: '00', nama: 'Semua Unsur', list_sub_unsur: list_sub_unsur });
		$.each(response.data, function(key, value){ list_unsur.push(value); });
		console.log('list_unsur: ', list_unsur);
		var nav_tab = '';
		var nav_tab_content = '';
		$.each(list_unsur, function(key, value){
			nav_tab += '<a id="nav-'+value.id+'-tab" data-toggle="tab" href="#nav-'+value.id+'" role="tab" aria-controls="nav-'+value.id+'" aria-selected="true"  class="nav-item nav-link '+(key==0?'active':'')+'" onclick="getSelectSubUnsur(\''+value.id+'\')">'+value.nama+'</a>';
			nav_tab_content += '<div id="nav-'+value.id+'" aria-labelledby="nav-'+value.id+'-tab" role="tabpanel" class="tab-pane fade '+(key==0?'show active':'')+'">';
			nav_tab_content += $('#template-content').html().replace('add()', 'add(\''+jenis.toUpperCase().trim()+'\', \''+value.id+'\')').replace('card-data-index', 'card-data-'+value.id);
			nav_tab_content += '</div>';
		});
		$('#nav-tab').html(nav_tab);
		$('#nav-tab-content').html(nav_tab_content);
		display(); // supaya perintah pilihan tahun dan unit auditi tampil
		if(list_unsur.length > 0) getSelectSubUnsur(list_unsur[0].id);
		
		getListTahun(null, moment().format('YYYY'));
		$('[name=filter_tahun]').change(function(){
			$('[name=filter_tahun]').val($(this).val());
			getUnitAuditiSummaryList($(this).val());
			display();
		});
		getUnitAuditiSummaryList(moment().format('YYYY'));
		
	}, function(response) { console.log('error'); });
	
	getSelectTingkatMaturitas();
    
    $('#btn-download').click(function(e){
        var param = $("#search-form" ).serialize();
        location = path_unsur + '/download?pakai_foto=true&'+param;
    });
	
	$('[name=filter_taksa]').change(function(e){
		$('#search-form').submit();
	});
	
	$('#search-form').submit(function(e){
		display();
		e.preventDefault();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
}

function add(jenis, id_unsur){
	selected_id = '';
	selected_action = 'add';
	clearForm();
	if(jenis == null || id_unsur == null) setTimeout(function() { $('#modal-form').modal('hide') }, 500);
	if(jenis!=null) $('[name=jenis]').val(jenis);
	if(id_unsur!=null) $('[name=id_unsur]').val(id_unsur);
}

function getSelectSubUnsur(id_unsur){
	if(id_unsur == null) { console.error('[getSelectSubUnsur] id_unsur tidak boleh kosong'); return false; }
	$('[name=filter_id_sub_unsur]').empty();
	$('[name=filter_id_sub_unsur]').append(new Option('Semua Sub Unsur', ''));
	
	$('[name=id_sub_unsur]').empty();
	$('[name=id_sub_unsur]').append(new Option('Pilih Sub Unsur', ''));
	
	ajaxGET(path_sub_unsur + '/list?filter_id_unsur='+id_unsur,'onGetSelectSubUnsur','onGetSelectError');
}
function onGetSelectSubUnsur(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_sub_unsur]').append(new Option(value.nama, value.id));
		$('[name=id_sub_unsur]').append(new Option(value.nama, value.id));
	});
}

function getSelectTingkatMaturitas(){
	$('[name=filter_id_maturitas_tingkat]').empty();
	$('[name=filter_id_maturitas_tingkat]').append(new Option('Semua Tingkat', ''));
	
	$('[name=id_maturitas_tingkat]').empty();
	$('[name=id_maturitas_tingkat]').append(new Option('Pilih Tingkat', ''));
	
	ajaxGET(path_maturitas_tingkat + '/list','onGetSelectTingkatMaturitas','onGetSelectError');
}
function onGetSelectTingkatMaturitas(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_maturitas_tingkat]').append(new Option(value.judul, value.id));
		$('[name=id_maturitas_tingkat]').append(new Option(value.judul, value.id));
	});
}

function display(page = 1){
	
	if($('[name=filter_tahun]').val() == '' || $('[name=filter_id_unit_auditi]').val() == '') {
		console.error('tidak bisa display karena tahun: ' + $('[name=filter_tahun]').val() + ' dan id_unit_auditi: ' + $('[name=filter_id_unit_auditi]').val());
		$.each(list_unsur,function(key,unsur){
			$('#card-data-'+unsur.id).html('<p class="text-center pt-3 pb-3">Silahkan pilih Tahun dan Unit Auditi terlebih dahulu.</p>');
		});
		return false;
	}
		
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){ tbody.text(''); }
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	params += '&filter_jenis='+jenis.toUpperCase();
	params += '&filter_tahun='+$('[name=filter_tahun]').val();
	params += '&filter_id_unit_auditi='+$('[name=filter_id_unit_auditi]').val();
	console.log(path_unsur + '/list?'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
	ajaxGET(path_unsur + '/list?'+params,'onGetListSuccess','onGetListError');
		
	current_page = page;
}

function onGetListSuccess(response){
	
	if(response.data.length == 0) return;
	
	var list_unsur = []; 
	var list_sub_unsur = [];
	$.each(response.data, function(key, value){ $.each(value.list_sub_unsur, function(key, sub_unsur){ list_sub_unsur.push(sub_unsur); }); });
	list_unsur.push({ id: '00', nama: 'Semua Unsur', list_sub_unsur: list_sub_unsur });
	$.each(response.data, function(key, value){ list_unsur.push(value); });
	
	console.log('response.data: ', list_unsur);
	
	list_maturitas_penilaian = [];
	
	$.each(list_unsur,function(key,unsur){
	
		var index = unsur.id;
		
		console.log('[display] response sub-unsur-'+index+': ', unsur);
		
		$('[id=loading-row]').remove();
		$('[id=no-data-row]').remove();
		
		var card = $('#card-data-'+index);
		var html = '';
		
		$.each(unsur.list_sub_unsur,function(key,sub_unsur){
			html += '<div class="card-body mb-0 pb-0"><h3 class="card-text"><span class="custom-title">'+(key+1)+'. '+sub_unsur.nama+' ('+sub_unsur.kode+')</span></h3></div>';
			html += '<div class="card-body">';
			html += '	<table id="tbl-data-'+unsur.id+'-'+sub_unsur.id+'" class="table table-bordered table-striped table-hover">';
			html += '		<thead>';
			html += '			<tr>';
			html += '				<th rowspan="2" width="50" class="text-center">No</th>';
			html += '				<th rowspan="2" width="90" class="text-center">Ref</th>';
			html += '				<th rowspan="2" width="160" class="text-center">Parameter</th>';
			html += '				<th colspan="3" width="400" class="text-center">Hasil Pengujian Bukti</th>';
			html += '				<th rowspan="2" width="70" class="text-center">Action</th>';
			html += '			</tr>';
			html += '			<tr>';
			html += '				<th class="text-center" width="300">Dokumen</th>';
			html += '				<th class="text-center" width="300">Substansi</th>';
			html += '				<th class="text-center" width="100" nowrap>Hasil (Y/T)</th>';
			html += '			</tr>';
			html += '		</thead>';
			html += '		<tbody>';
			$.each(sub_unsur.list_maturitas_tingkat,function(key,maturitas_tingkat){
				html += '		<tr>';
				html += '			<td colspan="7"><h4 class="custom-title">'+maturitas_tingkat.kode+'. LEVEL ('+maturitas_tingkat.kode.split('.')[2]+'): '+maturitas_tingkat.judul+'</h4></td>';
				html += '		</tr>';
				
				$('[id=loading-row]').remove();
				$('[id=no-data-row]').remove();
				
				var row = "";
				$.each(maturitas_tingkat.list_maturitas_parameter,function(key,value){
					if(!contains.call(appendIds, value.id)){
						row += renderRow(value, (key+1));
					}
				});
				html += (row == "" ? '<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>' : row);
			});
			html += '		</tbody>';
			html += '	</table>';
			html += '</div>';
		
		});
		
		card.html(html);
	
	});
	
}

function appendRow(value){
	$('[id=loading-row]').remove();
	$('[id=no-data-row]').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data-"+value.id_unsur+'-'+value.id_sub_unsur).find('tbody');
	
	console.log('tambah ke sini: ', "#tbl-data-"+value.id_unsur+'-'+value.id_sub_unsur);
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var num = $('#row-'+value.id_unsur+'-'+value.id_sub_unsur+'-'+value.id_maturitas_tingkat+'-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data-"+value.id_unsur+'-'+value.id_sub_unsur).find('tbody');
	
	$('#row-'+value.id_unsur+'-'+value.id_sub_unsur+'-'+value.id_maturitas_tingkat+'-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}

function renderRow(value, num, method='add'){
	var row = "";
	var rowId = value.id_unsur+'-'+value.id_sub_unsur+'-'+value.id_maturitas_tingkat+'-'+value.id;
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+rowId+'">';
	}

	var id, dokumen = '', substansi = '', hasil = '';
	try { if(value.maturitas_penilaian != null) list_maturitas_penilaian.push(value.maturitas_penilaian); } catch {}
	try { id = value.maturitas_penilaian.id } catch {}
	try { dokumen = value.maturitas_penilaian.dokumen } catch {}
	try { substansi = value.maturitas_penilaian.substansi } catch {}
	try { hasil = value.maturitas_penilaian.hasil } catch {}
	
	row += '<td class="text-center">'+(num)+'</td>';
	row += '<td class="text-center">'+value.kode_pernyataan+'</td>';
	row += '<td class="">'+value.judul_pernyataan+'</td>';
	
	// View
	row += '<td class="text-centet" id="aksi-view" data-id-maturitas-penilaian="'+rowId+'" data-content="dokumen">'+dokumen+'</td>';
	row += '<td class="text-centet" id="aksi-view" data-id-maturitas-penilaian="'+rowId+'" data-content="substansi">'+substansi+'</td>';
	row += '<td class="text-center" id="aksi-view" data-id-maturitas-penilaian="'+rowId+'" data-content="hasil">'+(hasil==''||hasil==null?'':hasil=='Y'?'Ya':'Tidak')+'</td>';
	row += '<td class="text-center" id="aksi-view" data-id-maturitas-penilaian="'+rowId+'" nowrap>';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneUpdate(\''+value.id+'\',\''+id+'\',\''+rowId+'\')" title="Edit"><i class="fas fa-fw fa-pencil-alt"></i> Edit</a> ';
	row += '</td>';
	// Input
	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian="'+rowId+'" style="display: none;"><textarea data-id-maturitas-penilaian="'+rowId+'" class="form-control" name="dokumen"   rows="3">'+dokumen+'</textarea></td>';
	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian="'+rowId+'" style="display: none;"><textarea data-id-maturitas-penilaian="'+rowId+'" class="form-control" name="substansi" rows="3">'+substansi+'</textarea></td>';
	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian="'+rowId+'" style="display: none;"><select   data-id-maturitas-penilaian="'+rowId+'" class="form-control" name="hasil" style="width: 80px; display: inherit;"><option value="Y" '+(hasil=='Y'?'selected':hasil==''?'selected':'')+'>Ya</option><option value="T" '+(hasil=='T'?'selected':'')+'>Tidak</option></select></td>';
	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian="'+rowId+'" nowrap style="display: none;">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneSave(\''+value.id+'\',\''+id+'\',\''+rowId+'\')" title="Save"><i class="fas fa-fw fa-save"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneCancel(\''+value.id+'\',\''+rowId+'\')" title="Cancel"><i class="fas fa-fw fa-remove"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function oneUpdate(id, id_maturitas_penilaian, rowId) {
	if(id_maturitas_penilaian == 'undefined') id_maturitas_penilaian = null;
    console.log('id_maturitas_parameter', id);
    console.log('id_maturitas_penilaian', id_maturitas_penilaian);
    console.log('rowId', rowId);
    selected_id = id;
    var dokumen = '';
    var substansi = '';
    var hasil = '';
    $.each(list_maturitas_penilaian, function(key, value){
        if(value.id_maturitas_parameter == id) {
        	dokumen = value.dokumen;
        	substansi = value.substansi;
        	hasil = value.hasil;
        }
    });
    console.log('[oneUpdate] dokumen: ', dokumen);
    console.log('[oneUpdate] substansi: ', substansi);
    console.log('[oneUpdate] hasil: ', hasil);
    $('[name=dokumen][data-id-maturitas-penilaian='+rowId+']').val(dokumen=='null'?'':dokumen);
    $('[name=substansi][data-id-maturitas-penilaian='+rowId+']').val(substansi=='null'?'':substansi);
    $('[name=hasil][data-id-maturitas-penilaian='+rowId+']').val(hasil==''||hasil==null?'Y':hasil);
    $('[id=aksi-view][data-id-maturitas-penilaian='+rowId+']').hide();
    $('[id=aksi-input][data-id-maturitas-penilaian='+rowId+']').show();
    $('#tbl').parent().scrollLeft($('#tbl').width());
}

function oneSave(id, id_maturitas_penilaian, rowId) {
	// validasi level 
	
	if(id_maturitas_penilaian == 'undefined') id_maturitas_penilaian = null;
    var obj = new FormData();
    var dokumen = $('[name=dokumen][data-id-maturitas-penilaian='+rowId+']').val();
    var substansi = $('[name=substansi][data-id-maturitas-penilaian='+rowId+']').val();
    var hasil = $('[name=hasil][data-id-maturitas-penilaian='+rowId+']').val();
    console.log('id_maturitas_penilaian: ', id_maturitas_penilaian);
    console.log('id_maturitas_parameter: ', id);
    console.log('tahun: ', $('[name=filter_tahun]').val());
    console.log('id_unit_auditi: ', $('[name=filter_id_unit_auditi]').val());
    console.log('dokumen: ', dokumen);
    console.log('substansi: ', substansi);
    console.log('hasil: ', hasil);
    if(id_maturitas_penilaian != null) obj.append('id', id_maturitas_penilaian);
    obj.append('id_maturitas_parameter', id);
    obj.append('tahun', $('[name=filter_tahun]').val());
    obj.append('id_unit_auditi', $('[name=filter_id_unit_auditi]').val());
    obj.append('dokumen', dokumen);
    obj.append('substansi', substansi);
    obj.append('hasil', hasil);
    obj.append('jenis', jenis.toUpperCase());
    ajaxPOST(path_maturitas_penilaian + '/save',obj,function(response){
        console.log('[id=aksi-view][data-id-maturitas-penilaian='+rowId+'][data-content=dokumen]: ', response.data.dokumen);
        console.log('[id=aksi-view][data-id-maturitas-penilaian='+rowId+'][data-content=uraian]: ', response.data.substansi);
        console.log('[id=aksi-view][data-id-maturitas-penilaian='+rowId+'][data-content=sesuai]: ', response.data.hasil);
        display();
        $.notify({ message: 'Data berhasil disimpan!' },{type: 'success'});
        setTimeout(function() { $.notifyClose(); }, 2000);

//        $.each(list_maturitas_penilaian, function(key, value){ if(value.id == id) { value.dokumen = response.data.dokumen; value.substansi = response.data.substansi; value.hasil = response.data.hasil; } });
//
//        $('[id=aksi-view][data-id-maturitas-penilaian='+rowId+'][data-content=tanggal_realisasi]').html(getDateForView(response.data.tanggal_realisasi));
//        $('[id=aksi-view][data-id-maturitas-penilaian='+rowId+'][data-content=uraian]').html(getValue(response.data.uraian, '-'));
//        $('[id=aksi-view][data-id-maturitas-penilaian='+rowId+'][data-content=nama_file_bukti]').html(getDownload(response.data.id, response.data.nama_file_bukti, '-', 'folder'));
//        $('[data-id-maturitas-penilaian='+rowId+'][data-content=triwulan]').html(getValue(response.data.triwulan, '-'));
//        $('[data-id-maturitas-penilaian='+rowId+'][data-content=sesuai]').html(getStatusSesuai(response.data.sesuai, '-'));
//
//        $('[id=aksi-view][data-id-maturitas-penilaian='+rowId+']').show();
//        $('[id=aksi-input][data-id-maturitas-penilaian='+rowId+']').hide();
    },function(response){
    	$.notify({ title: 'Data gagal disimpan!', message: '<br/>'+response.responseJSON.message },{type: 'danger'});
    });
}

function oneCancel(id, rowId) {
    $('[id=aksi-view][data-id-maturitas-penilaian='+rowId+']').show();
    $('[id=aksi-input][data-id-maturitas-penilaian='+rowId+']').hide();
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('[name=id_sub_unsur]').val('').select().trigger('change');
	$('[name=id_maturitas_tingkat]').val('').select().trigger('change');
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		ajaxGET(path_maturitas_parameter + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		ajaxPOST(path_maturitas_parameter + '/'+id+'/'+selected_action,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
//	if(selected_action == 'add'){
//		//appendRow(response.data);
//		display(); // logic-nya banyak jadi sementara gini dulu
//	}else if(selected_action == 'edit'){
//		//updateRow(response.data);
//		display(); // logic-nya banyak jadi sementara gini dulu
//	}else if(selected_action == 'delete'){
//		//removeRow(response.data);
//		display(); // logic-nya banyak jadi sementara gini dulu
//	}
	
	display();
	
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function fillFormValue(value){
	//console.log(value);
	clearForm();

	$('[name=jenis]').val(value.jenis);
	$('[name=id_unsur]').val(value.id_unsur);
	$('[name=id_sub_unsur]').val(value.id_sub_unsur).select2().trigger('change');
	$('[name=id_maturitas_tingkat]').val(value.id_maturitas_tingkat).select2().trigger('change');
	$('[name=kode_pernyataan]').val(value.kode_pernyataan);
	$('[name=judul_pernyataan]').val(value.judul_pernyataan);
	
}

function save(){
	startLoading('btn-save');
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(path_maturitas_parameter + '/save',obj,'onModalActionSuccess','onModalActionError');
}

