var path_maturitas_penilaian_persiapan = ctx + '/maturitas-penilaian-persiapan';
var path_maturitas_persiapan = ctx + '/maturitas-persiapan';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

var list_maturitas_persiapan = [];
var list_maturitas_penilaian_persiapan = [];

function init(){
	
	$('[id=menu]').html($.urlParam('menu'));
	
	getListTahun(null, moment().format('YYYY'));
	$('[name=filter_tahun]').change(function(){
		$('[name=filter_tahun]').val($(this).val());
		getUnitAuditiSummaryList($(this).val());
		display();
	});

	$.getJSON(path_maturitas_persiapan+'/list', function(response) {
		list_maturitas_persiapan = response.data;
		getUnitAuditiSummaryList(moment().format('YYYY'));
	});
	
}

function display(page = 1){
	
	if($('[name=filter_tahun]').val() == '' || $('[name=filter_id_unit_auditi]').val() == '') {
		console.error('tidak bisa display karena tahun: ' + $('[name=filter_tahun]').val() + ' dan id_unit_auditi: ' + $('[name=filter_id_unit_auditi]').val());
		$("#tbl").find('tbody').html('<tr><td colspan="10"><p class="text-center pt-3 pb-3">Silahkan pilih <b>Tahun</b> dan <b>Unit Auditi</b> terlebih dahulu.</p></td></tr>');
		return false;
	}
		
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	params = $( "#search-form" ).serialize();
	params += '&filter_tahun='+$('[name=filter_tahun]').val();
	params += '&filter_id_unit_auditi='+$('[name=filter_id_unit_auditi]').val();
	console.log(path_maturitas_penilaian_persiapan + '/list?'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
	ajaxGET(path_maturitas_penilaian_persiapan + '/list?'+params,function(response){

		console.log('response.data: ', response.data);
		list_maturitas_penilaian_persiapan = response.data;
		
	    var row = '';
	    var tbody = $('#tbl').find($('tbody'));
	    tbody.text('');
	    $.each(list_maturitas_persiapan, function(key, value) {
	    	
	    	var rowId = value.id, id_maturitas_penilaian_persiapan = null, dokumen = '', substansi = '', hasil = '';
	    	$.each(list_maturitas_penilaian_persiapan, function(key2, value2){
	    		if(value.id == value2.id_maturitas_persiapan){
	    			id_maturitas_penilaian_persiapan = value2.id;
	    			dokumen = value2.dokumen;
	    			substansi = value2.substansi;
	    			hasil = value2.hasil;
	    			return false;
	    		}
	    	});
	    	
	        row += '<tr>';
	        	
	            row += '<td scope="row" class="text-center">'+(key+1)+'</td>';
	            row += '<td>'+value.nama+'</td>';
	        	
	        	// View
	        	row += '<td class="text-centet" id="aksi-view" data-id-maturitas-penilaian-persiapan="'+rowId+'" data-content="dokumen">'+getValue(dokumen, '')+'</td>';
	        	row += '<td class="text-centet" id="aksi-view" data-id-maturitas-penilaian-persiapan="'+rowId+'" data-content="substansi">'+getValue(substansi, '')+'</td>';
	        	row += '<td class="text-center" id="aksi-view" data-id-maturitas-penilaian-persiapan="'+rowId+'" data-content="hasil">'+(hasil==''||hasil==null?'':hasil=='Y'?'Ya':'Tidak')+'</td>';
	        	row += '<td class="text-center" id="aksi-view" data-id-maturitas-penilaian-persiapan="'+rowId+'" nowrap>';
	        	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneUpdate(\''+value.id+'\',\''+id_maturitas_penilaian_persiapan+'\',\''+rowId+'\')" title="Edit"><i class="fas fa-fw fa-pencil-alt"></i> Edit</a> ';
	        	row += '</td>';
	        	
	        	// Input
	        	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian-persiapan="'+rowId+'" style="display: none;"><textarea data-id-maturitas-penilaian-persiapan="'+rowId+'" class="form-control" name="dokumen"   rows="3">'+dokumen+'</textarea></td>';
	        	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian-persiapan="'+rowId+'" style="display: none;"><textarea data-id-maturitas-penilaian-persiapan="'+rowId+'" class="form-control" name="substansi" rows="3">'+substansi+'</textarea></td>';
	        	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian-persiapan="'+rowId+'" style="display: none;"><select   data-id-maturitas-penilaian-persiapan="'+rowId+'" class="form-control" name="hasil" style="width: 80px; display: inherit;"><option value="Y" '+(hasil=='Y'?'selected':hasil==''?'selected':'')+'>Ya</option><option value="T" '+(hasil=='T'?'selected':'')+'>Tidak</option></select></td>';
	        	row += '<td class="text-center" id="aksi-input" data-id-maturitas-penilaian-persiapan="'+rowId+'" nowrap style="display: none;">';
	        	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneSave(\''+value.id+'\',\''+id_maturitas_penilaian_persiapan+'\',\''+rowId+'\')" title="Save"><i class="fas fa-fw fa-save"></i></a> ';
	        	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneCancel(\''+value.id+'\',\''+rowId+'\')" title="Cancel"><i class="fas fa-fw fa-remove"></i></a>';
	        	row += '</td>';
	        	
	        row += '</tr>';
	    });
	    tbody.html(row!=''?row:'<tr><td colspan="10"><p class="text-center pt-3 pb-3">Data tidak tersedia.</p></td></tr>');
		
	},'onGetListError');
		
	current_page = page;
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
	tbody.append(row);
}

function oneUpdate(id, id_maturitas_penilaian_persiapan, rowId) {
	if(id_maturitas_penilaian_persiapan == 'undefined') id_maturitas_penilaian_persiapan = null;
    console.log('id_maturitas_persiapan', id);
    console.log('id_maturitas_penilaian_persiapan', id_maturitas_penilaian_persiapan);
    console.log('rowId', rowId);
    selected_id = id;
    var dokumen = '';
    var substansi = '';
    var hasil = '';
    $.each(list_maturitas_penilaian_persiapan, function(key, value){ 
    	if(value.id_maturitas_persiapan == id) {
    		dokumen = value.dokumen;
    		substansi = value.substansi;
    		hasil = value.hasil;
    		return false; 
    	}
    });
    console.log('[oneUpdate] dokumen: ', dokumen);
    console.log('[oneUpdate] substansi: ', substansi);
    console.log('[oneUpdate] hasil: ', hasil);
    $('[name=dokumen][data-id-maturitas-penilaian-persiapan='+rowId+']').val(dokumen=='null'?'':dokumen);
    $('[name=substansi][data-id-maturitas-penilaian-persiapan='+rowId+']').val(substansi=='null'?'':substansi);
    $('[name=hasil][data-id-maturitas-penilaian-persiapan='+rowId+']').val(hasil==''||hasil==null?'Y':hasil);
    $('[id=aksi-view][data-id-maturitas-penilaian-persiapan='+rowId+']').hide();
    $('[id=aksi-input][data-id-maturitas-penilaian-persiapan='+rowId+']').show();
    $('#tbl').parent().scrollLeft($('#tbl').width());
}

function oneSave(id, id_maturitas_penilaian_persiapan, rowId) {
    var obj = new FormData();
    var dokumen = $('[name=dokumen][data-id-maturitas-penilaian-persiapan='+rowId+']').val();
    var substansi = $('[name=substansi][data-id-maturitas-penilaian-persiapan='+rowId+']').val();
    var hasil = $('[name=hasil][data-id-maturitas-penilaian-persiapan='+rowId+']').val();
    console.log('id: ', id_maturitas_penilaian_persiapan);
    console.log('tahun: ', $('[name=filter_tahun]').val());
    console.log('id_unit_auditi: ', $('[name=filter_id_unit_auditi]').val());
    console.log('id_maturitas_persiapan: ', id);
    console.log('dokumen: ', dokumen);
    console.log('substansi: ', substansi);
    console.log('hasil: ', hasil);
    if(id_maturitas_penilaian_persiapan == null || id_maturitas_penilaian_persiapan == 'null') {} else obj.append('id', id_maturitas_penilaian_persiapan);
    obj.append('tahun', $('[name=filter_tahun]').val());
    obj.append('id_unit_auditi', $('[name=filter_id_unit_auditi]').val());
    obj.append('id_maturitas_persiapan', id);
    obj.append('dokumen', dokumen);
    obj.append('substansi', substansi);
    obj.append('hasil', hasil);
    ajaxPOST(path_maturitas_penilaian_persiapan + '/save',obj,function(response){
        console.log('[id=aksi-view][data-id-maturitas-penilaian-persiapan='+rowId+'][data-content=dokumen]: ', response.data.dokumen);
        console.log('[id=aksi-view][data-id-maturitas-penilaian-persiapan='+rowId+'][data-content=uraian]: ', response.data.substansi);
        console.log('[id=aksi-view][data-id-maturitas-penilaian-persiapan='+rowId+'][data-content=hasil]: ', response.data.hasil);
        display();
        $.notify({ message: 'Data berhasil disimpan!' },{type: 'success'});
        setTimeout(function() { $.notifyClose(); }, 2000);
    },function(response){
    	$.notify({ title: 'Data gagal disimpan!', message: '<br/>'+response.responseJSON.message },{type: 'danger'});
    });
}

function oneCancel(id, rowId) {
    $('[id=aksi-view][data-id-maturitas-penilaian-persiapan='+rowId+']').show();
    $('[id=aksi-input][data-id-maturitas-penilaian-persiapan='+rowId+']').hide();
}
