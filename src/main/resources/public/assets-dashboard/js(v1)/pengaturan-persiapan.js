var path_maturitas_persiapan = ctx + '/maturitas-persiapan';
var list_maturitas_persiapan = [];

function init(){
	
	$('[id=menu]').html($.urlParam('menu').replace('.', ''));

	$.getJSON(path_maturitas_persiapan+'/list', function(response) {
		list_maturitas_persiapan = response.data;
	    var row = '';
	    var tbody = $('#tbl').find($('tbody'));
	    tbody.text('');
	    $.each(list_maturitas_persiapan, function(key, value) {
	    	
	        row += '<tr>';
	        	
	            row += '<td scope="row" class="text-center">'+(key+1)+'</td>';
	            row += '<td>'+value.nama+'</td>';
	        	
	        	// View
	        	row += '<td class="text-center" id="aksi-view" data-id-maturitas-persiapan="'+value.id+'" data-content="bobot">'+getValue(value.bobot, 0)+'</td>';
	        	row += '<td class="text-center" id="aksi-view" data-id-maturitas-persiapan="'+value.id+'" nowrap>';
	        	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneUpdate(\''+value.id+'\')" title="Edit"><i class="fas fa-fw fa-pencil-alt"></i> Edit</a> ';
	        	row += '</td>';
	        	
	        	// Input
	        	row += '<td class="text-center" id="aksi-input" data-id-maturitas-persiapan="'+value.id+'" style="display: none;"><input type="number" step=".1" data-id-maturitas-persiapan="'+value.id+'" class="form-control text-center" name="bobot" value="'+value.bobot+'"></input></td>';
	        	row += '<td class="text-center" id="aksi-input" data-id-maturitas-persiapan="'+value.id+'" nowrap style="display: none;">';
	        	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneSave(\''+value.id+'\')" title="Save"><i class="fas fa-fw fa-save"></i></a> ';
	        	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="oneCancel(\''+value.id+'\')" title="Cancel"><i class="fas fa-fw fa-remove"></i></a>';
	        	row += '</td>';
	        	
	        row += '</tr>';
	    });
	    tbody.html(row!=''?row:'<tr><td colspan="10"><p class="text-center pt-3 pb-3">Data tidak tersedia.</p></td></tr>');
	    
	}, function(response){
		$('#loading-row').remove();
		$('#no-data-row').remove();
		var tbody = $("#tbl-data").find('tbody');
		var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="init()">Error! Coba lagi..</button>';
		row += '</div></td></tr>';
		tbody.append(row);
	});
	
}

function oneUpdate(id) {
    console.log('id', id);
    var bobot = 0; $.each(list_maturitas_persiapan, function(key, value){ if(value.id == id) { bobot = value.bobot; return false; } });
    console.log('[oneUpdate] bobot: ', bobot);
    $('[name=bobot][data-id-maturitas-persiapan='+id+']').val(bobot);
    $('[id=aksi-view][data-id-maturitas-persiapan='+id+']').hide();
    $('[id=aksi-input][data-id-maturitas-persiapan='+id+']').show();
    $('#tbl').parent().scrollLeft($('#tbl').width());
}

function oneSave(id) {
    var obj = new FormData();
    var bobot = $('[name=bobot][data-id-maturitas-persiapan='+id+']').val();
    console.log('id: ', id);
    console.log('bobot: ', bobot);
    obj.append('id', id);
    obj.append('bobot', bobot);
    ajaxPOST(path_maturitas_persiapan + '/save',obj,function(response){
        console.log('[id=aksi-view][data-id-maturitas-persiapan='+id+'][data-content=bobot]: ', response.data.bobot);
        init();
        $.notify({ message: 'Data berhasil disimpan!' },{type: 'success'});
        setTimeout(function() { $.notifyClose(); }, 2000);
    },function(response){
    	$.notify({ title: 'Data gagal disimpan!', message: '<br/>'+response.responseJSON.message },{type: 'danger'});
    });
}

function oneCancel(id) {
    $('[id=aksi-view][data-id-maturitas-persiapan='+id+']').show();
    $('[id=aksi-input][data-id-maturitas-persiapan='+id+']').hide();
}
