var selected_menu = '';

var roleArray = roleList.replace('[', '').replace(']', '').replace(/ROLE_PPS_/g, '').split(', ');

var public_list_data_progress_peserta_ujian = [];

$(document).ready(function(){
    selected_menu = ($.urlParam('menu') == null ? '' : $.urlParam('menu'));
    
    if(Cookies.set(localeCookieName) == null) Cookies.set(localeCookieName, 'id_ID');

    $.ajaxSetup({
        headers : {
            'Authorization' : 'Bearer '+ getCookieValue(tokenCookieName)
        }
    });
    
    $('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
        $(this).val(picker.endDate.format('DD-MM-YYYY'));
        picker.autoUpdateInput = true;
        $(this).change();
        
    });
    $('.single-date-picker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    
    
    $('.select2').select2();
    $('.select2').select2({
            width: '100%'
    });
    
    $('.sidebar .nav-item' ).each(function() {
        var a = this.firstElementChild;
        if(selected_menu != ''){
            if(a.innerText.trim()==selected_menu.trim()){
                this.classList.add("active");
            }
        }else{
            if((window.location.href.indexOf(a.getAttribute('href'))> 0) == true){
                this.classList.add("active");
            }
        }
        
        
    });
    
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    
    
    
    moment.locale('id', {
        relativeTime : {
            future: "in %s",
            past:   "%s yg lalu",
            s  : '1 detik',
            ss : '%d detik',
            m:  "1 menit",
            mm: "%d menit",
            h:  "1 jam",
            hh: "%d jam",
            d:  "1 hari",
            dd: "%d hari",
            M:  "1 bulan",
            MM: "%d bulan",
            y:  "1 tahun",
            yy: "%d tahun"
        }
    });
    
    if(Cookies.get(localeCookieName) == null || Cookies.get(localeCookieName) == 'en_US'){
        $('#lang-dropdown').html('<span class="flag-icon flag-icon-us"> </span> English');
        $('#lang-option').html('<a class="dropdown-item" href="#" onclick="changeLang(\'id_ID\')"><span class="flag-icon flag-icon-id"> </span>  Indonesia</a>');
        moment.locale('en');
    }else{
        $('#lang-dropdown').html('<span class="flag-icon flag-icon-id"> </span> Indonesia');
        $('#lang-option').html('<a class="dropdown-item" href="#" onclick="changeLang(\'en_US\')"><span class="flag-icon flag-icon-us"> </span>  English</a>');
        moment.locale('id');
    }
    
    $('.single-date-picker').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        locale: {
                format: 'DD-MM-YYYY',
                cancelLabel: 'Clear'
        }
    });
    
    //init();
});

/* Chart SPJK */

window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

window.isMobile = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

function generateChartHasilKeseluruhanWithRest() {
    ajaxGET(path + '/progress_peserta_ujian?page=1&join=kelompok_jabatan',function(response){
    	//console.log('public_list_data_progress_peserta_ujian: ', response.data);
    	public_list_data_progress_peserta_ujian = response.data;
    	generateChartProgressPesertaUjian();
    },'onGetListError');   	
}


function generateChartProgressPesertaUjian() {
	$.each(public_list_data_progress_peserta_ujian,function(key,value){
	
		$('#progress-chart-'+key).remove();
		$('.progress-chart-'+key).append('<canvas id="progress-chart-'+key+'"><canvas>');
		var canvas = document.querySelector('#progress-chart-'+key); // why use jQuery?
		var ctx = canvas.getContext('2d');
		var x = canvas.width/2;
		var y = canvas.height/2;
		ctx.font = '10pt Verdana';
		ctx.textAlign = 'center';
		ctx.fillText('This text is centered on the canvas', x, y);
		//end here
		
		$('#julah-peserta-berjalan-'+key).text(value.jumlah_peserta_berjalan);
		$('#total-peserta-'+key).text(value.total_peserta);
		
		// Percent Chart Diatas
		$('#progress-nama-kode-kelompok-berjalan-'+key).html(value.kelompok_jabatan.kode);
		$('#progress-id-kelompok-'+key).data('id',value.kelompok_jabatan.id);
		$('#progress-id-kelompok-'+key).attr('data-id',value.kelompok_jabatan.id);
		if($('#progress-id-kelompok-'+key).data('id')==$.urlParam('id_kelompok')){
			$('#progress-id-kelompok-'+key).addClass("bg-info text-white");
		}
		var canvas = document.getElementById('progress-chart-'+key);
		if(canvas == null) return;
		var ctx = canvas.getContext("2d");
		if (ctx) {
			var data = [value.jumlah_peserta_berjalan, value.total_peserta-value.jumlah_peserta_berjalan];
			ctx.height = 100;
			var myChart = new Chart(ctx, {
				type: 'doughnut',
				data: {
					datasets: [
						{
							label: "Data Chart SPJK",
							data: data,
							backgroundColor: [ '#4A9B82', '#cfe3dd' ],
							hoverBackgroundColor: [ '#7ac2ac', '#dfebe7' ],
							borderWidth: [	0, 0 ],
							hoverBorderColor: [ 'transparent', 'transparent' ]
						}
					],
					labels: [ 'Telah Login', 'Belum Login' ]
				},
				options: {
				    aspectRatio: 1,
					maintainAspectRatio: false,
					responsive: true,
					cutoutPercentage: 55,
					animation: {
						duration: 0,
						animateScale: false,
						animateRotate: false,
						onComplete : function(e){
							var width = this.chart.width, height = this.chart.height;
							var cx = width / 2;
							var cy = height / 2;
							this.chart.ctx.textAlign = 'center';
							this.chart.ctx.textBaseline = 'middle';
							this.chart.ctx.font = '12px verdana';
							this.chart.ctx.fillStyle = 'black';
							this.chart.ctx.fillText(((data[0]/(data[0]+data[1]))*100).toFixed(2)+"%", cx, cy);
							this.chart.ctx.animation = false;
						}
					},
					hover: { animationDuration: 0 },
					responsiveAnimationDuration: 0,
					legend: {
						display: false
					},
				}
			});	
		} 
	});
}

function generateDynamicChartProgressPesertaUjian() {
	$.each(public_list_data_progress_peserta_ujian,function(key,value){
	
		$('#progress-chart-'+key).remove();
		$('.progress-chart-'+key).append('<canvas id="progress-chart-'+key+'"><canvas>');
		var canvas = document.querySelector('#progress-chart-'+key); // why use jQuery?
		var ctx = canvas.getContext('2d');
		var x = canvas.width/2;
		var y = canvas.height/2;
		ctx.font = '10pt Verdana';
		ctx.textAlign = 'center';
		ctx.fillText('This text is centered on the canvas', x, y);
		//end here
		
		$('#julah-peserta-berjalan-'+key).text(value.jumlah_peserta_berjalan);
		$('#total-peserta-'+key).text(value.total_peserta);
		
		// Percent Chart Diatas
		$('#progress-nama-kode-kelompok-berjalan-'+key).html(value.kode);
		$('#progress-id-kelompok-'+key).data('id',value.id);
		$('#progress-id-kelompok-'+key).attr('data-id',value.id);
		if($('#progress-id-kelompok-'+key).data('id')==$.urlParam('id_kelompok')){
			$('#progress-id-kelompok-'+key).addClass("bg-info text-white");
		}
		var canvas = document.getElementById('progress-chart-'+key);
		if(canvas == null) return;
		var ctx = canvas.getContext("2d");
		if (ctx) {
			var data = [value.jumlah_peserta_berjalan, value.total_peserta-value.jumlah_peserta_berjalan];
			ctx.height = 100;
			var myChart = new Chart(ctx, {
				type: 'doughnut',
				data: {
					datasets: [
						{
							label: "Data Chart SPJK",
							data: data,
							backgroundColor: [ '#4A9B82', '#cfe3dd' ],
							hoverBackgroundColor: [ '#7ac2ac', '#dfebe7' ],
							borderWidth: [	0, 0 ],
							hoverBorderColor: [ 'transparent', 'transparent' ]
						}
					],
					labels: [ 'Telah Login', 'Belum Login' ]
				},
				options: {
				    aspectRatio: 1,
					maintainAspectRatio: false,
					responsive: true,
					cutoutPercentage: 55,
					animation: {
						duration: 0,
						animateScale: false,
						animateRotate: false,
						onComplete : function(e){
							var width = this.chart.width, height = this.chart.height;
							var cx = width / 2;
							var cy = height / 2;
							this.chart.ctx.textAlign = 'center';
							this.chart.ctx.textBaseline = 'middle';
							this.chart.ctx.font = '10px verdana';
							this.chart.ctx.fillStyle = 'black';
							this.chart.ctx.fillText((( Number.isNaN(data[0]/(data[0]+data[1]))?0:data[0]/(data[0]+data[1]) )*100).toFixed(2)+"%", cx, cy);
							this.chart.ctx.animation = false;
						}
					},
					hover: { animationDuration: 0 },
					responsiveAnimationDuration: 0,
					legend: {
						display: false
					},
				}
			});	
		} 
	});
}

function sgenerateChartProgressPesertaUjian() {

	$.each(public_list_data_progress_peserta_ujian,function(key,value){
		
		$('#julah-peserta-berjalan-'+key).text(value.jumlah_peserta_berjalan);
		$('#total-peserta-'+key).text(value.total_peserta);
		
		// Percent Chart Diatas
		$('#progress-nama-kode-kelompok-berjalan-'+key).html(value.kelompok_jabatan.kode);
		$('#progress-id-kelompok-'+key).data('id',value.kelompok_jabatan.id);
		var canvas = document.getElementById('progress-chart-'+key);
		if(canvas == null) return;
		var ctx = canvas.getContext("2d");
		if (ctx) {
			var data = [value.jumlah_peserta_berjalan, value.total_peserta-value.jumlah_peserta_berjalan];
			//ctx.height = 100;
			var myChart = new Chart(ctx, {
				type: 'doughnut',
				data: {
					datasets: [
						{
							label: "Data Chart SPJK",
							data: data,
							backgroundColor: [ '#4A9B82', '#cfe3dd' ],
							hoverBackgroundColor: [ '#7ac2ac', '#dfebe7' ],
							borderWidth: [	0, 0 ],
							hoverBorderColor: [ 'transparent', 'transparent' ]
						}
					],
					labels: [ 'Telah Mengikuti', 'Belum Mengikuti' ]
				},
				options: {
					tooltips: { enabled: true },
					hover :{ animationDuration:0 },
					legend:{ display: false, position: 'bottom' },

		            responsive:false,
//		            scales: {
//		                yAxes: [{
//		                    ticks: {
//		                        beginAtZero:true
//		                    }
//		                }]
//		            },
				    aspectRatio: 1,
					maintainAspectRatio: false,
					responsive: true,
					cutoutPercentage: 55,
//					animation: {
//						duration: 0,
//						animateScale: false,
//						animateRotate: false,
//						onComplete : function(e){
//							var width = this.chart.width, height = this.chart.height;
//							var cx = width / 2;
//							var cy = height / 2;
//							this.chart.ctx.textAlign = 'center';
//							this.chart.ctx.textBaseline = 'middle';
//							this.chart.ctx.font = '12px verdana';
//							this.chart.ctx.fillStyle = 'black';
//							this.chart.ctx.fillText(((data[0]/(data[0]+data[1]))*100).toFixed(2)+"%", cx, cy);
//							this.chart.ctx.animation = false;
//						}
//					},
//					hover: { animationDuration: 0 },
//					responsiveAnimationDuration: 0,
//					legend: {
//						display: false
//					},
				}
			});	
		} 
	});

}

function getBarOption(data, custom_option={ data_type: 'percentage', legend_enabled:true }) {
	var xAxesTicks = {
		beginAtZero:true,
		fontFamily: "'verdana', sans-serif",
		fontSize:16
	}
	if(custom_option.data_type == 'percentage') xAxesTicks.max = 100;
	if(custom_option.data_type == 'nopercentage') xAxesTicks.max = 100;
	else {
		xAxesTicks.stepSize = 1;
		xAxesTicks.max = 5; //xAxesTicks.max = Math.max(...data) + 10;
	}
	
	return {
		//maintainAspectRatio: false,
		tooltips: { enabled: false },
		hover :{ animationDuration:0 },
		legend:{ display: custom_option.legend_enabled, position: 'bottom' },
		scales: {
			xAxes: [{
				ticks: xAxesTicks,
				scaleLabel:{ display:false },
				gridLines: { }, 
				stacked: true
			}],
			yAxes: [{
				gridLines: {
					display:false,
					color: "#fff",
					zeroLineColor: "#fff",
					zeroLineWidth: 0
				},
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:16
				},
				stacked: true
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				ctx.textAlign = "right";
				ctx.font = "14px verdana";
				ctx.fillStyle = "#fff";
				
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						if(custom_option.data_type == 'percentage') {
							var data_display = parseFloat((dataset.data[index] / 100).toFixed(2)*100);
							if(data_display > 0) ctx.fillText(data_display + '%', bar._model.x-10, bar._model.y);
						}else if(custom_option.data_type == 'nopercentage') {
							var data_display = parseFloat((dataset.data[index] / 100).toFixed(2)*100);
							if(data_display > 0) ctx.fillText(data_display + '', bar._model.x-10, bar._model.y);
						}else {
							if(dataset.data[index] > 0) ctx.fillText(dataset.data[index], bar._model.x-10, bar._model.y);
						}
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}

function getBarOptionTes(data, custom_option={ data_type: 'percentage', legend_enabled:true }) {
	var xAxesTicks = {
		beginAtZero:true,
		fontFamily: "'verdana', sans-serif",
		fontSize:16
	}
	if(custom_option.data_type == 'percentage') xAxesTicks.max = 100;
	if(custom_option.data_type == 'nopercentage') xAxesTicks.max = 100;
	else {
		xAxesTicks.stepSize = 1;
		xAxesTicks.max = 5; //xAxesTicks.max = Math.max(...data) + 10;
	}
	
	return {
		//maintainAspectRatio: false,
		tooltips: { enabled: false },
		hover :{ animationDuration:0 },
		legend:{ display: custom_option.legend_enabled, position: 'bottom' },
		scales: {
			xAxes: [{
				ticks: xAxesTicks,
				scaleLabel:{ display:false },
				gridLines: { }, 
				stacked: true
			}],
			yAxes: [{
				gridLines: {
					display:false,
					color: "#fff",
					zeroLineColor: "#fff",
					zeroLineWidth: 0
				},
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:16
				},
				stacked: true
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				ctx.textAlign = "right";
				ctx.font = "14px verdana";
				ctx.fillStyle = "#fff";
				
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						if(custom_option.data_type == 'percentage') {
							var data_display = parseFloat((dataset.data[index] / 100).toFixed(2)*100);
							if(data_display > 0) ctx.fillText(data_display + '%', bar._model.x-10, bar._model.y);
						}else if(custom_option.data_type == 'nopercentage') {
							var data_display = parseFloat((dataset.data[index] / 100).toFixed(2)*100);
							if(data_display > 0) ctx.fillText(custom_option.data_label[index]+":"+data_display + '', bar._model.x-10, bar._model.y);
						}else {
							if(dataset.data[index] > 0) ctx.fillText(dataset.data[index], bar._model.x-10, bar._model.y);
						}
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}


function getBarOptionStacked(data1, data2) {
	return {
		maintainAspectRatio: false,
		tooltips: { enabled: false },
		hover :{ animationDuration: 0 },
		legend:{ display: true, position: 'bottom' },
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero:true,
					fontFamily: "'verdana', sans-serif",
					fontSize:16
				},
				scaleLabel:{ display:false },
				gridLines: { }, 
				stacked: true
			}],
			yAxes: [{
				gridLines: {
					display:false,
					color: "#fff",
					zeroLineColor: "#fff",
					zeroLineWidth: 0
				},
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:16
				},
				stacked: true
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				ctx.textAlign = "right";
				ctx.font = "14px verdana";
				ctx.fillStyle = "#fff";
				
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						data = parseFloat((dataset.data[index] / (data1[index] + data2[index])).toFixed(2)*100);
						//console.log(data);
						if(data >= 0) {
							ctx.fillText(data + '%', bar._model.x-10, bar._model.y);
						} else {
							ctx.fillText(data + '%', bar._model.x+50, bar._model.y);
						}
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}

/*function onGetListDetilError(response){
	console.log(response);
	if(response.responseJSON!=undefined){
		Swal.fire({
		  icon: 'warning',
		  title: 'Mohon maaf',
		  html: '<p class="text-left">'+response.responseJSON.message+'</p>'+
			  '<div class="text-left"><strong></strong></div>',
		})
	}
}*/