var path = ctx + '/kandang';
var path_taksa = ctx + '/taksa';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	getSelectTaksa();
	display();
    
    $("[name=file_foto]").change(function (e){
       var fileName = e.target.files[0].name;
       $("[name=nama_file_foto]").html(fileName);
     });
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
    
    $('#btn-download').click(function(e){
        var param = $("#search-form" ).serialize();
        location = path + '/download?pakai_foto=true&'+param;
    });
	
	$('[name=filter_taksa]').change(function(e){
		$('#search-form').submit();
	});
	
	$('#search-form').submit(function(e){
		display();
		e.preventDefault();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
}

function getSelectTaksa(){
	$('[name=filter_taksa]').empty();
	$('[name=filter_taksa]').append(new Option('Semua Taksa', ''));
	
	$('[name=id_taksa]').empty();
	$('[name=id_taksa]').append(new Option('Pilih Taksa', ''));
	
	ajaxGET(path_taksa + '/list','onGetSelectTaksa','onGetSelectError');
}
function onGetSelectTaksa(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_taksa]').append(new Option(value.nama, value.id));
		$('[name=id_taksa]').append(new Option(value.nama, value.id));
	});
}

function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');

		
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(path + '/list?page='+page+'&'+params,'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
//	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}

function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	var badge='success';
	if(value.status_kondisi == 'PERLU_PERBAIKAN'){
		badge='warning';
	}else if(value.status_kondisi == 'TIDAK_DAPAT_DIGUNAKAN'){
		badge='danger';
	}
	
	row += '<td>'+(num)+'</td>';
	row += '<td class="">';
	row += '<strong>Nama/Kode:</strong> '+value.nama +' <br>';
	row += '<strong>Jenis:</strong> '+value.jenis +' <br>';
	row += '<strong>Taksa:</strong> '+value.taksa.nama +' <br>';
	row += '</td>';
	row += '<td class="">';
	row += '<strong>Kapasitas:</strong> '+value.kapasitas +' <br>';
	row += '<strong>Status:</strong> <span class="badge badge-pill badge-'+badge+'">'+value.status_kondisi +'</span> <br>';
	row += '<strong>Keterangan:</strong> '+value.keterangan_kondisi +' <br>';
	row += '</td>';
    row += '<td class="">';
    row += '<strong>Jumlah Terisi</strong> '+value.stok_hitung+' <br>';
    row += '<strong>Kapasitas:</strong> '+value.kapasitas+' <br>';
//    row += '<strong>Jumlah Terisi</strong> '+value.stok_hitung+'/'+value.kapasitas+' <br>';
    row += '</td>';
    row += '<td class="">';
    if(value.nama_file_foto != null && value.nama_file_foto != '') {
        row += '<a href="' + ctx + '/files/' + value.nama_file_foto_ext+'?filename=' + value.nama_file_foto + '&'+new Date().getTime()+'" data-toggle="lightbox" data-max-width="600">';
        row += '<img class="rounded float-left px-1" src="' + ctx + '/files/' + value.id+'?preview=1&filename=' + value.nama_file_foto + '&'+new Date().getTime()+'" width="150px" />';
        row += '</a>';
    }else{
        row += '<img class="rounded float-left px-1" src="' + ctx + '/files/' + value.id+'?preview=1&filename=noimage.png" width="150px" />';
    }
    row += '</td>';
//    row += '<td class="">';
//    row += '<strong>Stok (Keeper):</strong> '+value.stok_hitung +' <br>';
//    row += '<strong>Stok (Kandang):</strong> '+value.stok_entri +' <br>';
//    row += '<strong>Sisa Tersedia:</strong> '+(value.kapasitas-value.stok_hitung) +' <br>';
//    row += '</td>';
	/*
	row += '<td class="">';
	row += '<strong>Dibuat:</strong> '+moment(value.time_added).fromNow()+'<br>';
	row += '<strong>Diupdate:</strong> '+moment(value.time_modified).fromNow()+'<br>';
	row += '</td>';
	*/
    if($("#tbl-data").find('thead').children().first().find('th').last().html() == 'Aksi'){
		row += '<td class="">';
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
		row += '</td>';
    }
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
    $('[name=nama_file_foto]').html('Upload File');
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		ajaxPOST(path + '/'+id+'/'+selected_action,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function fillFormValue(value){
	//console.log(value);
	clearForm();
	
	$('[name=nama]').val(value.nama);
	$('[name=id_taksa]').val(value.id_taksa);
	$('[name=kapasitas]').val(value.kapasitas);
	$('[name=keterangan_kondisi]').val(value.keterangan_kondisi);

    $('[name=nama_file_foto]').html(value.nama_file_foto);
	$("input[name=jenis][value=" + value.jenis + "]").prop('checked', true);
	$("input[name=status_kondisi][value=" + value.status_kondisi + "]").prop('checked', true);
}

function save(){
	startLoading('btn-save');
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(path + '/save',obj,'onModalActionSuccess','onModalActionError');
}

