var path = ctx + '/restql';

var public_list_kelompok_jabatan = [], public_list_kelompok_manajerial = [];
var public_list_data_teknis_chart = [], public_list_data_sosiokultural_chart = [], public_list_data_manajerial_chart = [];
var public_list_rata_rata_kompetensi_individu = [];
//var public_list_data_progress_peserta_ujian = []; tidak diperlukan karena bersifat public, adanya di common.js

var public_filter = 'kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')';
var public_qry_join = '&join=kelompok_jabatan,tingkat_jabatan,jenjang_jabatan';

var teknisChart = null, sosiokulturalChart = null, manajerialChart = null;

function init(){
		if($.urlParam('kode-sesi')==null){
			showListSesi();
		}
		//generateChartHasilKeseluruhanWithRest(); dicomment karena udah ada di display
		
		ajaxGET(path + '/kelompok_jabatan',function(response){
				public_list_kelompok_jabatan = response.data;
				ajaxGET(path + '/kelompok_manajerial',function(response){
						public_list_kelompok_manajerial = response.data;
	    			display();
				});
		});
		
		ajaxGET(path + '/tingkat_jabatan',function(response){
				$.each(response.data, function(key,value){ $('[id=filter_tingkat_jabatan], [id=filter_chart_tingkat_jabatan]').append(new Option(value.nama, value.id)); });
		});
		
		ajaxGET(path + '/jenjang_jabatan',function(response){
				$.each(response.data, function(key,value){ $('[id=filter_jenjang_jabatan], [id=filter_chart_jenjang_jabatan]').append(new Option(value.nama, value.id)); });
		});
		
		ajaxGET(path + '/eselon1'+"?filter=kode_simpeg.is(null)",function(response){
				$.each(response.data, function(key,value){ $('[name=filter_id_eselon1]').append(new Option(value.nama, value.id)); });
		});
		
		ajaxGET(path + '/kelompok_jabatan',function(response){
				$.each(response.data, function(key,value){ $('[name=filter_id_kelompok_jabatan]').append(new Option(value.nama, value.id)); });
		});
		
		ajaxGET(path + '/jenjang_jabatan',function(response){
				$.each(response.data, function(key,value){ $('[name=filter_id_jenjang_jabatan]').append(new Option(value.nama, value.id)); });
		});
		
		ajaxGET(path + '/account',function(response){
				$.each(response.data, function(key,value){ $('[name=filter_id_account]').append(new Option(value.username+' ('+value.first_name+' '+value.last_name+')', value.username)); });
		});
		
		$('[id=filter_tingkat_jabatan], [id=filter_jenjang_jabatan]').change(function(){
				var chart = $(this).data('chart');
				if(chart == null) return false;
				if(chart == 'teknisChart') generateChartTeknis();
				if(chart == 'sosiokulturalChart') generateChartSosiokultural();
				if(chart == 'manajerialChart') generateChartManajerial();
				
		});
	
	
    
}














function generateChartTeknis() {
		var list_kelompok_jabatan_data_chart = JSON.parse(JSON.stringify(public_list_kelompok_jabatan));
		console.log(list_kelompok_jabatan_data_chart);
		var temp_kelompok_jabatan_data_chart = [];
		var tingkat = $('[id=filter_tingkat_jabatan][data-chart=teknisChart]').val();
		var jenjang = $('[id=filter_jenjang_jabatan][data-chart=teknisChart]').val();
		$.each(list_kelompok_jabatan_data_chart,function(key,value){ 
			value.list_nilai = [], value.nilai_rata_rata = 0 
		});
		$.each(public_list_data_teknis_chart,function(key,value){ 
			temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id] = { 
					id: value.kelompok_jabatan.id, 
					kode: value.kelompok_jabatan.kode, 
					list_nilai: [] 
			}; 
		});
		$.each(public_list_data_teknis_chart,function(key,value){
			var valid = false;
			if(tingkat == '' && jenjang == '') valid = true; 
			else if(tingkat == value.id_tingkat_jabatan && jenjang == value.id_jenjang_jabatan) valid = true;
			else if(tingkat == value.id_tingkat_jabatan && jenjang == '') valid = true;
			else if(tingkat == '' && jenjang == value.id_jenjang_jabatan) valid = true;
			if(valid) temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id].list_nilai.push((value.nilai!=undefined?value.nilai:0));
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
			if(value == null) return;
			if(value.list_nilai.length==0) value.nilai_rata_rata = 0;
			else value.nilai_rata_rata = value.list_nilai.reduce(function(a,b){return a+b;},0)/value.list_nilai.length;
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
			if(value == null) return;
			list_kelompok_jabatan_data_chart[value.kode] = value;
			$.each(list_kelompok_jabatan_data_chart,function(key2,value2){
				if(value.kode == value2.kode) {
					value2.list_nilai = value.list_nilai;
					value2.nilai_rata_rata = value.nilai_rata_rata; 
				}
			});
		});
		
		if(teknisChart == null) {
			$('#teknisChart').remove();
			$('#ContainerTeknisChart').append('<canvas id="teknisChart"><canvas>');
			var canvas = document.querySelector('#teknisChart'); // why use jQuery?
			var ctx = canvas.getContext('2d');
			var x = canvas.width/2;
			var y = canvas.height/2;
			//ctx.font = '16pt Verdana';
			ctx.textAlign = 'center';
			ctx.fillText('This text is centered on the canvas', x, y);
			//end here
			
			var ctx = document.getElementById("teknisChart");
			ctx.height = 100;
			teknisChart = new Chart(ctx, {
				type: 'horizontalBar',
				data: {
			    	labels: list_kelompok_jabatan_data_chart.map(function(value) { return value.kode; }),
			    	datasets: [{
			    		label: 'Kompetensi Teknis',
				        data: list_kelompok_jabatan_data_chart.map(function (value) { return value.nilai_rata_rata; }),
				        backgroundColor: "#4A9B82", hoverBackgroundColor: "#70c2a9"
			    	}]
				},
				options: getBarOption(list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; }), { data_type: 'nopercentage', legend_enabled: false }),
			});
		}else{
			teknisChart.data.datasets[0].data = list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; });
			teknisChart.update();
		}
}





function generateChartSosiokultural() {
	
		var list_kelompok_jabatan_data_chart = JSON.parse(JSON.stringify(public_list_kelompok_jabatan));
		var temp_kelompok_jabatan_data_chart = [];
		var tingkat = $('[id=filter_tingkat_jabatan][data-chart=sosiokulturalChart]').val();
		var jenjang = $('[id=filter_jenjang_jabatan][data-chart=sosiokulturalChart]').val();
		$.each(list_kelompok_jabatan_data_chart,function(key,value){ value.list_nilai = [], value.nilai_rata_rata = 0 });
		$.each(public_list_data_sosiokultural_chart,function(key,value){ temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id] = { id: value.kelompok_jabatan.id, kode: value.kelompok_jabatan.kode, list_nilai: [] }; });
		$.each(public_list_data_sosiokultural_chart,function(key,value){
				var valid = false;
				if(tingkat == '' && jenjang == '') valid = true; 
				else if(tingkat == value.id_tingkat_jabatan && jenjang == value.id_jenjang_jabatan) valid = true;
				else if(tingkat == value.id_tingkat_jabatan && jenjang == '') valid = true;
				else if(tingkat == '' && jenjang == value.id_jenjang_jabatan) valid = true;
				if(valid) temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id].list_nilai.push(value.nilai);
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
				if(value == null) return;
				if(value.list_nilai.length==0) value.nilai_rata_rata = 0;
				else value.nilai_rata_rata = value.list_nilai.reduce(function(a,b){return a+b;},0)/value.list_nilai.length;
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
			if(value == null) return;
			list_kelompok_jabatan_data_chart[value.kode] = value;
			$.each(list_kelompok_jabatan_data_chart,function(key2,value2){
				if(value.kode == value2.kode) {
					value2.list_nilai = value.list_nilai;
					value2.nilai_rata_rata = parseInt(value.nilai_rata_rata); 
				}
			});
		});
		
		if(sosiokulturalChart == null) {
			var ctx = document.getElementById("sosiokulturalChart");
			ctx.height = 200;
			sosiokulturalChart = new Chart(ctx, {
				type: 'horizontalBar',
				data: {
			    	labels: list_kelompok_jabatan_data_chart.map(function(value) { return value.kode; }),
			    	datasets: [{
			    		label: 'Kompetensi Sosiokultural',
				        data: list_kelompok_jabatan_data_chart.map(function (value) { return value.nilai_rata_rata; }),
				        backgroundColor: "#4A9B82", hoverBackgroundColor: "#70c2a9"
			    	}]
				},
				options: getBarOption(list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; }), { data_type: 'regular' }),
			});
		}
    else {
    		sosiokulturalChart.data.datasets[0].data = list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; });
    		sosiokulturalChart.update();
  	}
			
}


function generateChartManajerial(kode='') {
	if($('#div-btn-kompetensi-manajerial').html() == '') {
		var html_button = '';
		$.each(public_list_kelompok_jabatan,function(key,value){
				html_button += '<button class="btn btn-small btn-primary mx-1" id="btn-kompetensi-manajerial" onclick="generateChartManajerial(\''+value.kode+'\')" data-kode="'+value.kode+'">'+value.kode+'</button>';
		});
		$('#div-btn-kompetensi-manajerial').html(html_button);
	}
	
	if($('[id=btn-kompetensi-manajerial][class*=btn-danger]').data('kode') == kode) {
		kode = '';
		$('[id=btn-kompetensi-manajerial][class*=btn-danger]').removeClass('btn-danger').addClass('btn-primary');
		$('[id=btn-kompetensi-manajerial][class*=btn-danger]').data('condition', '');
	}
	else if(kode!='') {
		$.each($('[id=btn-kompetensi-manajerial]'),function(key,value){
			if(kode == $(this).data('kode')) {
				$(this).addClass('btn-danger').removeClass('btn-primary');
				$(this).data('condition', 'active');
			}
			else {
				$(this).removeClass('btn-danger').addClass('btn-primary');
				$(this).data('condition', '');
			}
		});
	}

	var list_data_manajerial_chart = JSON.parse(JSON.stringify(public_list_kelompok_manajerial));
	$.each(list_data_manajerial_chart,function(key,value){
		value.list_nilai_standard = [];
		value.list_nilai_pemetaan = [];	
	});
//	var list_data_manajerial_chart=[];
//	var list_data_manajerial_unique=[];
//	$.each(public_list_data_manajerial_chart,function(key,value){
//		var id = value.id_kelompok_manajerial;
//		if(list_data_manajerial_unique.indexOf(id) == -1) {
//			list_data_manajerial_unique.push(id);
//		}
//	});
	//
//	$.each(JSON.parse(JSON.stringify(public_list_kelompok_manajerial)), function(key, value){
//		$.each(list_data_manajerial_unique,function(key){
//			if(this==value.id){
//				//value.list_nilai_standard = [];
//				//value.list_nilai_pemetaan = [];	
//				list_data_manajerial_chart.push({
//					"id" : value.id,
//					"nama" : value.nama ,
//					"list_nilai_standard" : [],
//					"list_nilai_pemetaan" : [],
//				});
//			}
//		});
//	});
	//
//	console.log(list_data_manajerial_unique);
	console.log(list_data_manajerial_chart);
	var tingkat = $('[id=filter_tingkat_jabatan][data-chart=manajerialChart]').val();
	var jenjang = $('[id=filter_jenjang_jabatan][data-chart=manajerialChart]').val();
	//var kode = $('[id=btn-kompetensi-manajerial][data-condition=active]').val();
	
	$.each(public_list_data_manajerial_chart,function(key,value){
		var valid = false;
		if(tingkat == '' && jenjang == '') valid = true; 
		else if(tingkat == value.id_tingkat_jabatan && jenjang == value.id_jenjang_jabatan) valid = true;
		else if(tingkat == value.id_tingkat_jabatan && jenjang == '') valid = true;
		else if(tingkat == '' && jenjang == value.id_jenjang_jabatan) valid = true;
		if(valid && kode == '') valid = true;
		else if(valid && kode == value.kelompok_jabatan.kode) valid = true;
		else valid = false;
		if(valid) {
			console.log(list_data_manajerial_chart);
			$.each(list_data_manajerial_chart,function(key2,value2){
				if(value.kelompok_manajerial.id == value2.id) {
					value2.list_nilai_standard.push(value.nilai_standard);
					value2.list_nilai_pemetaan.push(value.nilai_pemetaan);
					value2.nilai_rata_rata_standard = value2.list_nilai_standard.reduce(function(a, b){return a + b}, 0) / value2.list_nilai_standard.length
					value2.nilai_rata_rata_pemetaan = value2.list_nilai_pemetaan.reduce(function(a, b){return a + b}, 0) / value2.list_nilai_pemetaan.length
				}
			});
		}
	});
	var co=0;
	$.each(list_data_manajerial_chart,function(key,value){
		if(value!=undefined){
			co++;
		}
	});
	var list_data_manajerial_chart_new=[]
	$.each(list_data_manajerial_chart,function(key,value){
		try{
			if(value!=undefined){
				if(value.list_nilai_standard.length > 0 && value.list_nilai_pemetaan.length > 0) {
					list_data_manajerial_chart_new.push(this);
				}
			}
		}catch(e){
			console.log(e);
		}
	});
	console.log('list_data_manajerial_chart: ', list_data_manajerial_chart_new);

	if(manajerialChart == null) {
		var color = Chart.helpers.color;
		ctx.height = 400;
		manajerialChart = new Chart(document.getElementById("manajerialChart"), {
			type: 'radar',
			data: {
				labels: list_data_manajerial_chart_new.map(function (value) { return value.nama; }),//['Integrity', 'Ability to Change', 'Planning Organizing', 'Comm Skills', 'Relation Building', 'Analitic Thinking', 'Leadership', 'Teamwork', 'Culture Knowledge', 'Social Interaction'],
				datasets: [ {
					label: 'Nilai Pemetaan',
					backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
					borderColor: window.chartColors.green,
					pointBackgroundColor: window.chartColors.green,
					data: list_data_manajerial_chart_new.map(function (value) { return parseInt(value.nilai_rata_rata_pemetaan); }),
				},{
					label: 'Nilai Standard',
					backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
					borderColor: window.chartColors.blue,
					pointBackgroundColor: window.chartColors.blue,
					data: list_data_manajerial_chart_new.map(function (value) { return parseInt(value.nilai_rata_rata_standard); }),
				}]
			},
			options: {
				responsive: true,
				legend: { position: 'bottom', },
				scale: { ticks: { beginAtZero: true, max: 5, min: 0, stepSize: 1 } }
			}
		});
	}
	else {
			manajerialChart.data.labels = list_data_manajerial_chart_new.map(function(value) { return value.nama; });
			manajerialChart.data.datasets[1].data = list_data_manajerial_chart_new.map(function(value) { return parseInt(value.nilai_rata_rata_standard); });
			manajerialChart.data.datasets[0].data = list_data_manajerial_chart_new.map(function(value) { return parseInt(value.nilai_rata_rata_pemetaan); });
			manajerialChart.update();
	}	
}

function clearIndividu() {
	//$('#filter-kompetensi-individu')[0].reset();
	//$('#modal-form-msg').hide();
	//$('.modal table').find('tbody').html('');
	$('[name=filter_id_eselon1]').val('').trigger('change');
	$('[name=filter_id_kelompok_jabatan]').val('').trigger('change');
	$('[name=filter_id_jenjang_jabatan]').val('').trigger('change');
	$('[name=filter_id_account]').val('').trigger('change');
}

function displayIndividu(page=1, limit=10, params='') {
	if($('[name=filter_id_eselon1]').val() != '') params += ',id_eselon1.eq(\''+$('[name=filter_id_eselon1]').val()+'\')';
	if($('[name=filter_id_kelompok_jabatan]').val() != '') params += ',id_kelompok_jabatan.eq(\''+$('[name=filter_id_kelompok_jabatan]').val()+'\')';
	if($('[name=filter_id_jenjang_jabatan]').val() != '') params += ',id_jenjang_jabatan.eq(\''+$('[name=filter_id_jenjang_jabatan]').val()+'\')';
	if($('[name=filter_id_account]').val() != '') params += ',nip.eq(\''+$('[name=filter_id_account]').val()+'\')';
	var tbody = $("#tbl-kompetensi-individu").find('tbody'), row = "";
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(path + '/rata_rata_kompetensi_individu?page='+page+'&limit='+limit+'&join=user_responden,kelompok_jabatan,jenjang_jabatan,eselon1,unit_auditi&filter='+public_filter+params,function(response){
  	console.log('list_rata_rata_kompetensi_individu', response.data);
  	public_list_rata_rata_kompetensi_individu = response.data;
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
  	//generateChartIndividu();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.user_responden.nama+'</td>';
      row += '<td class="text-center">'+value.user_responden.nip+'</td>';
      row += '<td class="text-center">'+value.golongan+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</td>';
      row += '<td>'+value.jenjang_jabatan.nama+'</td>';
      row += '<td>'+value.eselon1.nama+'</td>';
      row += '<td>'+value.unit_auditi.nama+'</td>';
      row += '<td class="text-center">'+value.nilai_kompetensi_manajerial+'</td>';
      row += '<td class="text-center">'+value.nilai_kompetensi_sosiokultural+'</td>';
      row += '<td class="text-center">'+value.nilai_kompetensi_teknis+'%</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayIndividu('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}

function displayProgress(page=1, limit=10, params='') {
	var tbody = $("#tbl-progress-peserta-ujian").find('tbody'), row = "";
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(path + '/progress_peserta_ujian?page='+page+'&limit='+limit+'&join=kelompok_jabatan&filter='+public_filter+params,function(response){
  	console.log('list_progress_peserta_ujian', response.data);
  	public_list_data_progress_peserta_ujian = response.data;
  	generateChartProgressPesertaUjian();
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</td>';
      row += '<td>'+value.jumlah_peserta_berjalan+'</td>';
      row += '<td>'+value.total_peserta+'</td>';
      row += '<td class="text-center">'+value.kode_sesi+'</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayProgress('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}

function displayRataRataKompetensiTeknis(page=1, limit=10, params='') {
	var url = path + '/rata_rata_kompetensi_teknis?'+public_qry_join+'&filter='+public_filter+params;
	if(public_list_data_teknis_chart.length==0) {
	  ajaxGET(url+'&limit=1000000',function(response){
	  	console.log('list_rata_rata_kompetensi_teknis', response.data);
	  	public_list_data_teknis_chart = response.data;
	  	generateChartTeknis();
	  });
	}
}

function displayRataRataKompetensiSosiokultural(page=1, limit=10, params='') {
	var tbody = $("#tbl-sosiokultural").find('tbody'), row = "";
	var url = path + '/rata_rata_kompetensi_sosiokultural?'+public_qry_join+'&filter='+public_filter+params;
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
	if(public_list_data_sosiokultural_chart.length==0) {
	  ajaxGET(url+'&limit=1000000',function(response){
	  	console.log('list_rata_rata_kompetensi_sosiokultural', response.data);
	  	public_list_data_sosiokultural_chart = response.data;
	  	generateChartSosiokultural();
		});
	}
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(url+'&page='+page+'&limit='+limit,function(response){
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</td>';
      row += '<td>'+value.tingkat_jabatan.nama+'</td>';
      row += '<td>'+value.jenjang_jabatan.nama+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+' '+value.tingkat_jabatan.nama+' '+value.jenjang_jabatan.nama+'</td>';
      row += '<td class="text-center">'+value.nilai+'</td>';
      row += '<td class="text-center">'+value.kode_sesi+'</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayRataRataKompetensiSosiokultural('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}

function displayRataRataKompetensiManajerial(page=1, limit=10, params='') {
	var tbody = $("#tbl-manajerial").find('tbody'), row = "";
	var url = path + '/rata_rata_kompetensi_manajerial?'+public_qry_join+',kelompok_manajerial&filter='+public_filter+params;
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
	if(public_list_data_manajerial_chart.length==0) {
	  ajaxGET(url+'&limit=1000000',function(response){
	  	console.log('list_rata_rata_kompetensi_manajerial', response.data);
	  	public_list_data_manajerial_chart = response.data;
	  	generateChartManajerial();
		});
	}
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(url+'&page='+page+'&limit='+limit,function(response){
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</td>';
      row += '<td>'+value.tingkat_jabatan.nama+'</td>';
      row += '<td>'+value.jenjang_jabatan.nama+'</td>';
      row += '<td>'+value.kelompok_manajerial.nama+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+' '+value.tingkat_jabatan.nama+' '+value.jenjang_jabatan.nama+'</td>';
      row += '<td class="text-center">'+value.nilai_standard+'</td>';
      row += '<td class="text-center">'+value.nilai_pemetaan+'</td>';
      row += '<td class="text-center">'+value.kode_sesi+'</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayRataRataKompetensiManajerial('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}


function display(){
	
	// individuChart
	displayIndividu();

	// progressChart
    displayProgress();
    
	// teknisChart
    displayRataRataKompetensiTeknis();
    
	// sosiokulturalChart
    displayRataRataKompetensiSosiokultural();
    
	// manajerialChart
    displayRataRataKompetensiManajerial();
    
}

function onGetListDetilSuccess(response){
    console.log('rekapitulasi-satwa: ', response.data);
    var tbody = $("#tbl-data").find('tbody');
    
    var row = "";
    var num = 1;
    var hidup = 0;
    var sakit = 0;
    var mati = 0;
    var dalam = 0;
    var titip = 0;
    var keluar = 0;
    $.each(response.data,function(key,value){
        row += '<tr class="data-row" id="row-'+value.id+'">';
        row += '<td class="text-center">'+(num)+'</td>';
        row += '<td class=""> '+value.nama +' </td>';
        row += '<td align="right" class=""> '+value.hidup +' </td>';
        row += '<td align="right" class=""> '+value.sakit +' </td>';
        row += '<td align="right" class=""> '+value.mati +' </td>';
        //row += '<td align="right" class=""> '+value.titip +' </td>'
        row += '<td align="right" class=""> '+value.titip +' </td>';
        row += '<td align="right" class=""> '+value.keluar +' </td>';
        row += '</tr>';
        
        hidup += value.hidup;
        sakit += value.sakit;
        mati += value.mati;
        dalam += value.titip;
        titip += value.titip;
        keluar += value.keluar;
        
        num++;
    });
    
    tbody.html(row);
    
    var tfoot = $("#tbl-data").find('tfoot');
    
    row = '<tr class="data-row" id="row-total">';
    row += '<td align="center" colspan="2"><strong>JUMLAH TOTAL</strong></td>';
    row += '<td align="right" class=""> '+hidup +' </td>';
    row += '<td align="right" class=""> '+sakit +' </td>';
    row += '<td align="right" class=""> '+mati +' </td>';
    row += '<td align="right" class=""> '+titip +' </td>';
    row += '<td align="right" class=""> '+keluar +' </td>';
//    row += '<td align="right" class=""> '+dalam +' </td>';
    row += '</tr>';
    
    tfoot.html(row);
    
}

function onGetTahunSuccess(response){
    console.log('chart-tahunan: ', response.data);
    var MONTHS = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var color = Chart.helpers.color;
    var data = {
        labels: MONTHS,
        datasets: [
          {
            label: "Satwa Masuk",
            data: response.data.penerimaan,
            backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
            borderColor: window.chartColors.yellow,
            borderWidth: 1
          },
          {
            label: "Satwa Keluar",
            data: response.data.pengeluaran,
            backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
            borderColor: window.chartColors.green,
            borderWidth: 1
          }
        ]
      }
    if(chartBar == null) generateChart(data);
    else { chartBar.data = data; window.chartBar.update(); }
}

function generateChart(data) {
  try {
    var ctx = document.getElementById("monthlyChart");
    if (ctx) {
      ctx.height = 100;
      chartBar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          legend: { position: 'top', labels: { fontFamily: 'Poppins' } },
          scales: { xAxes: [{ ticks: { fontFamily: "Poppins" } }], yAxes: [{ ticks: { beginAtZero: true, fontFamily: "Poppins" } }] }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }

};