var path = ctx + '/restql';

var public_list_kelompok_jabatan = [], public_list_kelompok_manajerial = [];
var public_list_data_teknis_chart = [], public_list_data_sosiokultural_chart = [], public_list_data_manajerial_chart = [];
var public_list_rata_rata_kompetensi_individu = [];
// var public_list_data_progress_peserta_ujian = []; tidak diperlukan karena
// bersifat public, adanya di common.js

var public_filter = 'kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')';
var public_qry_join = '&join=kelompok_jabatan,tingkat_jabatan,jenjang_jabatan';

var teknisChart = null, sosiokulturalChart = null, manajerialChart = null;
var mode_print=0;

function init(){

	if($.urlParam('kode-sesi')==null){
		showListSesi();
	}
	
	ajaxGET(path + '/multi/entity?entity=tingkat_jabatan;jenjang_jabatan',function(response){
			$.each(response.data.tingkat_jabatan, function(key,value){ $('[id=filter_tingkat_jabatan], [id=filter_chart_tingkat_jabatan]').append(new Option(value.nama, value.id)); });
			
			$.each(response.data.jenjang_jabatan, function(key,value){ $('[name=filter_id_jenjang_jabatan], [id=filter_jenjang_jabatan], [id=filter_chart_jenjang_jabatan]').append(new Option(value.nama, value.id)); });
	});
	
	ajaxGET(path + '/multi/entity?entity=kelompok_jabatan;kelompok_manajerial',function(response){
			public_list_kelompok_jabatan = response.data.kelompok_jabatan;
			public_list_kelompok_manajerial = response.data.kelompok_manajerial;
			display();
			$.each(response.data.kelompok_jabatan, function(key,value){ $('[name=filter_id_kelompok_jabatan]').append(new Option(value.kode, value.id)); });
	});
	
	ajaxGET(path + '/mapping_organisasi'+"?limit=1000000",function(response){
		$.each(response.data, function(key,value){ 
			if(response.data.length==1) $('[name=filter_id_organisasi]').html(new Option(value.nama, value.id));
			else $('[name=filter_id_organisasi]').append(new Option(value.nama, value.id)); 
		});
		if(response.data.length==1) $('[name=filter_id_organisasi]').trigger('change');
	});
	
	ajaxGET(path + '/multi/entity?entity=eselon1;unit_auditi;propinsi&limit=unit_auditi->2000&filter=eselon1->kode_simpeg.not(null)',function(response){
		$.each(response.data.eselon1, function(key,value){ $('[name=filter_id_eselon1]').append(new Option(value.nama, value.id)); });
		if(response.data.length==1){
			$('[name=filter_id_eselon1]').val(response.data.eselon1[0].id);
		}
		
		$.each(response.data.unit_auditi, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
		$.each(response.data.propinsi, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.nama, value.id)); });
	});

	/*ajaxGET(path + '/eselon1'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
		$.each(response.data, function(key,value){ $('[name=filter_id_eselon1]').append(new Option(value.nama, value.id)); });
		if(response.data.length==1){
			$('[name=filter_id_eselon1]').val(response.data[0].id);
		}
	});

	ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
	});

	ajaxGET(path + '/propinsi'+"?limit=1000000&",function(response){
			$.each(response.data, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.nama, value.id)); });
	});*/
	
	$('[id=filter_tingkat_jabatan], [id=filter_jenjang_jabatan]').change(function(){
			var chart = $(this).data('chart');
			if(chart == null) return false;
			if(chart == 'teknisChart') generateChartTeknis();
			if(chart == 'sosiokulturalChart') generateChartSosiokultural();
			if(chart == 'manajerialChart') generateChartManajerial();
	});
	
	$('[name=filter_id_eselon1]').change(function(){
		var params="kode_simpeg.not(null),";
		if(this.value==100 || $("[name=filter_id_eselon1] option:selected").text()=="Pemerintahan Daerah"){
			params="";
		}
		if($('[name=filter_id_eselon1]').val()!="") params+="id_eselon1.eq(\""+$('[name=filter_id_eselon1]').val()+"\")";
		ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter="+params,function(response){
			$('[name=filter_id_unit_auditi]').empty();
			$('[name=filter_id_unit_auditi]').append('<option value="">Satker</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
		});
	});
	
	$('[name=filter_id_propinsi]').change(function(){
		var params="kode_simpeg.not(null),";
		params="";
		if($('[name=filter_id_propinsi]').val()!="") params+="id_propinsi.eq(\""+$('[name=filter_id_propinsi]').val()+"\")";
		ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter="+params,function(response){
			$('[name=filter_id_unit_auditi]').empty();
			$('[name=filter_id_unit_auditi]').append('<option value="">Satker</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
		});
	});
	
	
	$('[name=filter_id_organisasi]').change(function(){
		var params = (this.value!=""?"&filter=id_header.eq(\""+this.value+"\")":"");
		params+="&join=propinsi";
		ajaxGET(path + '/mapping_organisasi_detil'+"?limit=1000000"+params,function(response){
			$('[name=filter_id_propinsi]').empty();
			$('[name=filter_id_propinsi]').append('<option value="">Propinsi</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.propinsi.nama, value.propinsi.id)); });
		});
	});
	
	$('#btn-print').click(function () {
		if(mode_print==1){
			window.print();
		}else{
			printPDF();
			mode_print=1;
			//window.open(window.location.href+"&print=1", '_blank');
		}
	});

	$('#btn-download').click(function () {
		generateExcel();
	});

	$('#btn-search').click(function () {
		Swal.fire({
			title: 'Konfirmasi Pencarian Data',
			showDenyButton: true,
			showCancelButton: true,
			confirmButtonText: `Terapkan Filter hanya pada tabel`,
			denyButtonText: `Terapkan Filter pada grafik & tabel`,
			cancelButtonText: `Batalkan Penerapan Filter Data`,
			allowOutsideClick : false
		}).then((result) => {
		  if(result.isConfirmed) {
			displayIndividu();
		  }else if (result.isDenied) {
			displayIndividu();
			displayProgress()
		  }
		})
	});

//	if($.urlParam('print')==1){
//		printPDF();
//		$('#list-kelompok').remove();
//		$('.main-content .row')[0].remove();
//	}
}

function setKelompokJabatan(e){
	$('#list-kelompok').on('click', '.overview-item', function() {
		 $('.overview-item.bg-info').removeClass('bg-info');
		 $(this).addClass('bg-info');
	});
	$('[name=filter_id_kelompok_jabatan]').val(e.dataset.id);
	//$(e).addClass("bg-info");
	//selected_id_kelompok=e.dataset.id;
	//display();
	//getSelectBidangTeknis(selected_id_kelompok);
}

function printPDF(){
	$('aside').attr("style", "display:none!important");
	$('.page-container').attr('style', 'padding-left:0px');
	$('header').attr("style", "display:none!important");
	$('.main-content').attr('style', 'padding:0px');
	$.each($('.card'), function(key){
		if(key>0) $(this).attr("style", "display:none!important");
	});
	$('.more-button-row').remove();
	$('tfoot').remove();
	$('#list-kelompok').remove();
	$('.main-content .row')[0].remove();
	$('#filter-kompetensi-individu').html("");
}

function generateChartTeknis() {
		var list_kelompok_jabatan_data_chart = JSON.parse(JSON.stringify(public_list_kelompok_jabatan));
		console.log(list_kelompok_jabatan_data_chart);
		var temp_kelompok_jabatan_data_chart = [];
		var tingkat = $('[id=filter_tingkat_jabatan][data-chart=teknisChart]').val();
		var jenjang = $('[id=filter_jenjang_jabatan][data-chart=teknisChart]').val();
		$.each(list_kelompok_jabatan_data_chart,function(key,value){ value.list_nilai = [], value.nilai_rata_rata = 0 });
		$.each(public_list_data_teknis_chart,function(key,value){ temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id] = { id: value.kelompok_jabatan.id, kode: value.kelompok_jabatan.kode, list_nilai: [] }; });
		$.each(public_list_data_teknis_chart,function(key,value){
			var valid = false;
			if(tingkat == '' && jenjang == '') valid = true; 
			else if(tingkat == value.id_tingkat_jabatan && jenjang == value.id_jenjang_jabatan) valid = true;
			else if(tingkat == value.id_tingkat_jabatan && jenjang == '') valid = true;
			else if(tingkat == '' && jenjang == value.id_jenjang_jabatan) valid = true;
			if(valid) temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id].list_nilai.push((value.nilai!=undefined?value.nilai:0));
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
			if(value == null) return;
			if(value.list_nilai.length==0) value.nilai_rata_rata = 0;
			else value.nilai_rata_rata = value.list_nilai.reduce(function(a,b){return a+b;},0)/value.list_nilai.length;
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
				if(value == null) return;
				list_kelompok_jabatan_data_chart[value.kode] = value;
				$.each(list_kelompok_jabatan_data_chart,function(key2,value2){
					if(value.kode == value2.kode) {
						value2.list_nilai = value.list_nilai;
						value2.nilai_rata_rata = value.nilai_rata_rata; 
					}
				});
		});
		
		if(teknisChart == null) {
			$('#teknisChart').remove();
			$('#ContainerTeknisChart').append('<canvas id="teknisChart"><canvas>');
			var canvas = document.querySelector('#teknisChart'); // why use
																	// jQuery?
			var ctx = canvas.getContext('2d');
			var x = canvas.width/2;
			var y = canvas.height/2;
			// ctx.font = '16pt Verdana';
			ctx.textAlign = 'center';
			ctx.fillText('This text is centered on the canvas', x, y);
			// end here
			
			var ctx = document.getElementById("teknisChart");
			ctx.height = 200;
			teknisChart = new Chart(ctx, {
				type: 'horizontalBar',
				data: {
			    	labels: list_kelompok_jabatan_data_chart.map(function(value) { return value.kode; }),
			    	datasets: [{
			    		label: 'Kompetensi Teknis',
				        data: list_kelompok_jabatan_data_chart.map(function (value) { return value.nilai_rata_rata; }),
				        backgroundColor: "#4A9B82", hoverBackgroundColor: "#70c2a9"
			    	}]
				},
				options: getBarOptionTes(list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; }),{ data_type: 'nopercentage', data_label: list_kelompok_jabatan_data_chart.map(function(value) { return value.list_nilai.length; }) }),
			});
		}
    else {
    		teknisChart.data.datasets[0].data = list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; });
    		teknisChart.update();
  	}
			
}


function generateChartSosiokultural() {
	
		var list_kelompok_jabatan_data_chart = JSON.parse(JSON.stringify(public_list_kelompok_jabatan));
		var temp_kelompok_jabatan_data_chart = [];
		var tingkat = $('[id=filter_tingkat_jabatan][data-chart=sosiokulturalChart]').val();
		var jenjang = $('[id=filter_jenjang_jabatan][data-chart=sosiokulturalChart]').val();
		$.each(list_kelompok_jabatan_data_chart,function(key,value){ value.list_nilai = [], value.nilai_rata_rata = 0 });
		$.each(public_list_data_sosiokultural_chart,function(key,value){ temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id] = { id: value.kelompok_jabatan.id, kode: value.kelompok_jabatan.kode, list_nilai: [] }; });
		$.each(public_list_data_sosiokultural_chart,function(key,value){
				var valid = false;
				if(tingkat == '' && jenjang == '') valid = true; 
				else if(tingkat == value.id_tingkat_jabatan && jenjang == value.id_jenjang_jabatan) valid = true;
				else if(tingkat == value.id_tingkat_jabatan && jenjang == '') valid = true;
				else if(tingkat == '' && jenjang == value.id_jenjang_jabatan) valid = true;
				if(valid) temp_kelompok_jabatan_data_chart[value.kelompok_jabatan.id].list_nilai.push(value.nilai);
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
				if(value == null) return;
				if(value.list_nilai.length==0) value.nilai_rata_rata = 0;
				else value.nilai_rata_rata = value.list_nilai.reduce(function(a,b){return a+b;},0)/value.list_nilai.length;
		});
		$.each(temp_kelompok_jabatan_data_chart,function(key,value){
				if(value == null) return;
				list_kelompok_jabatan_data_chart[value.kode] = value;
				$.each(list_kelompok_jabatan_data_chart,function(key2,value2){
					if(value.kode == value2.kode) {
						value2.list_nilai = value.list_nilai;
						value2.nilai_rata_rata = value.nilai_rata_rata; 
					}
				});
		});
		
		if(sosiokulturalChart == null) {
				var ctx = document.getElementById("sosiokulturalChart");
				ctx.height = 200;
				sosiokulturalChart = new Chart(ctx, {
					type: 'horizontalBar',
					data: {
				    	labels: list_kelompok_jabatan_data_chart.map(function(value) { return value.kode; }),
				    	datasets: [{
				    		label: 'Kompetensi Sosiokultural',
					        data: list_kelompok_jabatan_data_chart.map(function (value) { return value.nilai_rata_rata; }),
					        backgroundColor: "#4A9B82", hoverBackgroundColor: "#70c2a9"
				    	}]
					},
					options: getBarOption(list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; }), { data_type: 'regular' }),
				});
		}
    else {
    		sosiokulturalChart.data.datasets[0].data = list_kelompok_jabatan_data_chart.map(function(value) { return value.nilai_rata_rata; });
    		sosiokulturalChart.update();
  	}
			
}




function generateChartManajerial(kode='') {

		if($('#div-btn-kompetensi-manajerial').html() == '') {
				var html_button = '';
				$.each(public_list_kelompok_jabatan,function(key,value){
						html_button += '<button class="btn btn-small btn-primary mx-1" id="btn-kompetensi-manajerial" onclick="generateChartManajerial(\''+value.kode+'\')" data-kode="'+value.kode+'">'+value.kode+'</button>';
				});
				$('#div-btn-kompetensi-manajerial').html(html_button);
		}
		if($('[id=btn-kompetensi-manajerial][class*=btn-danger]').data('kode') == kode) {
			kode = '';
			$('[id=btn-kompetensi-manajerial][class*=btn-danger]').removeClass('btn-danger').addClass('btn-primary');
			$('[id=btn-kompetensi-manajerial][class*=btn-danger]').data('condition', '');
		}
		else if(kode!='') {
			$.each($('[id=btn-kompetensi-manajerial]'),function(key,value){
				if(kode == $(this).data('kode')) {
					$(this).addClass('btn-danger').removeClass('btn-primary');
					$(this).data('condition', 'active');
				}
				else {
					$(this).removeClass('btn-danger').addClass('btn-primary');
					$(this).data('condition', '');
				}
			});
		}

		var list_data_manajerial_chart = JSON.parse(JSON.stringify(public_list_kelompok_manajerial));
		$.each(list_data_manajerial_chart,function(key,value){
			value.list_nilai_standard = [];
			value.list_nilai_pemetaan = [];	
		});
		var tingkat = $('[id=filter_tingkat_jabatan][data-chart=manajerialChart]').val();
		var jenjang = $('[id=filter_jenjang_jabatan][data-chart=manajerialChart]').val();
		// var kode =
		// $('[id=btn-kompetensi-manajerial][data-condition=active]').val();
		$.each(public_list_data_manajerial_chart,function(key,value){
				var valid = false;
				if(tingkat == '' && jenjang == '') valid = true; 
				else if(tingkat == value.id_tingkat_jabatan && jenjang == value.id_jenjang_jabatan) valid = true;
				else if(tingkat == value.id_tingkat_jabatan && jenjang == '') valid = true;
				else if(tingkat == '' && jenjang == value.id_jenjang_jabatan) valid = true;
				if(valid && kode == '') valid = true;
				else if(valid && kode == value.kelompok_jabatan.kode) valid = true;
				else valid = false;
				if(valid) {
						$.each(list_data_manajerial_chart,function(key2,value2){
								if(value.kelompok_manajerial.id == value2.id) {
										value2.list_nilai_standard.push(value.nilai_standard);
										value2.list_nilai_pemetaan.push(value.nilai_pemetaan);
										value2.nilai_rata_rata_standard = value2.list_nilai_standard.reduce(function(a, b){return a + b}, 0) / value2.list_nilai_standard.length
										value2.nilai_rata_rata_pemetaan = value2.list_nilai_pemetaan.reduce(function(a, b){return a + b}, 0) / value2.list_nilai_pemetaan.length
								}
						});
				}
		});
		// console.log('list_data_manajerial_chart: ',
		// list_data_manajerial_chart);
	
		if(manajerialChart == null) {
			$('#manajerialChart').remove();
			$('#ContainerManajerialChart').append('<canvas id="manajerialChart"><canvas>');
			var canvas = document.querySelector('#manajerialChart'); // why
																		// use
																		// jQuery?
			var ctx = canvas.getContext('2d');
			var x = canvas.width/2;
			var y = canvas.height/2;
			// ctx.font = '16pt Verdana';
			ctx.textAlign = 'center';
			ctx.fillText('This text is centered on the canvas', x, y);
			// end here
			
				var color = Chart.helpers.color;
				ctx.height = 400;
				manajerialChart = new Chart(document.getElementById("manajerialChart"), {
					type: 'radar',
					data: {
						labels: ['Integrity', 'Ability to Change', 'Planning Organizing', 'Comm Skills', 'Relation Building', 'Analitic Thinking', 'Leadership', 'Teamwork', 'Culture Knowledge', 'Social Interaction'],
						datasets: [{
							label: 'Nilai Pemetaan',
							backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
							borderColor: window.chartColors.green,
							pointBackgroundColor: window.chartColors.green,
							data: list_data_manajerial_chart.map(function (value) { return (value.nilai_rata_rata_standard != null && value.nilai_rata_rata_standard.toFixed(0) < value.nilai_rata_rata_standard ? value.nilai_rata_rata_standard.toFixed(2) : value.nilai_rata_rata_standard); }),
						}, {
							label: 'Nilai Standard',
							backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
							borderColor: window.chartColors.blue,
							pointBackgroundColor: window.chartColors.blue,
							data: list_data_manajerial_chart.map(function (value) { return (value.nilai_rata_rata_pemetaan != null && value.nilai_rata_rata_pemetaan.toFixed(0) < value.nilai_rata_rata_pemetaan ? value.nilai_rata_rata_pemetaan.toFixed(2) : value.nilai_rata_rata_pemetaan); }),
						}]
					},
					options: {
						legend: { position: 'bottom', },
						scale: { ticks: { beginAtZero: true, max: 5, min: 0, stepSize: 1 } }
					}
				});
		}
    else {
    		manajerialChart.data.datasets[0].data = list_data_manajerial_chart.map(function(value) { return value.nilai_rata_rata_standard; });
    		manajerialChart.data.datasets[1].data = list_data_manajerial_chart.map(function(value) { return value.nilai_rata_rata_pemetaan; });
    		manajerialChart.update();
  	}
		
}

function clearIndividu() {
	// $('#filter-kompetensi-individu')[0].reset();
	// $('#modal-form-msg').hide();
	// $('.modal table').find('tbody').html('');
	$('[name=filter_id_eselon1]').val('').trigger('change');
	$('[name=filter_id_kelompok_jabatan]').val('').trigger('change');
	$('[name=filter_id_jenjang_jabatan]').val('').trigger('change');
	$('[name=filter_nip]').val('').trigger('change');
	$('[name=filter_id_organisasi]').val('').trigger('change');
	$('[name=filter_id_propinsi]').val('').trigger('change');
	 $('.overview-item.bg-info').removeClass('bg-info');
}

function displayIndividu(page=1, limit=100, params='') {
//	if($('[name=filter_id_eselon1]').val() != '') params += ',id_eselon1.eq(\''+$('[name=filter_id_eselon1]').val()+'\')';
//	if($('[name=filter_id_unit_auditi]').val() != '') params += ',id_unit_auditi.eq(\''+$('[name=filter_id_unit_auditi]').val()+'\')';
//	if($('[name=filter_id_kelompok_jabatan]').val() != '') params += ',id_kelompok_jabatan.eq(\''+$('[name=filter_id_kelompok_jabatan]').val()+'\')';
//	if($('[name=filter_id_jenjang_jabatan]').val() != '') params += ',id_jenjang_jabatan.eq(\''+$('[name=filter_id_jenjang_jabatan]').val()+'\')';
//	if($('[name=filter_id_account]').val() != '') params += ',nip.eq(\''+$('[name=filter_id_account]').val()+'\')';
//	if($('[name=filter_status]').val() == 'SELESAI'){
//		//params += ',nilai_kompetensi_manajerial.more(\''+0+'\'),nilai_kompetensi_sosiokultural.more(\''+0+'\'),nilai_kompetensi_teknis.more(\''+0+'\')';
//		params += ',status_responden.eq(\'SELESAI\')';
//	}else if($('[name=filter_status]').val() == 'PROSES'){
//		params += ',status_responden.eq(\'PROSES\')';
//	}else if($('[name=filter_status]').val() == 'SELESAI_PROSES'){
//		params += ',status_responden.not(null)';
//	}
	var tbody = $("#tbl-kompetensi-individu").find('tbody'), row = "";
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var params = $('#filter-kompetensi-individu').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	//public_filter = 'kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')';public_filter
	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
	ajaxGET(ctx + '/dashboard/laporan-individu?page='+page+'&limit='+limit+"&"+params,function(response){
  	console.log('list_rata_rata_kompetensi_individu', response.data);
  	public_list_rata_rata_kompetensi_individu = response.data;
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
		
		var nilai_komp_teknis=0,count_nilai_komp_teknis=0;
		var nilai_komp_sosiokultural=0,count_nilai_komp_sosiokultural=0;
		var nilai_komp_manajerial=0,count_nilai_komp_manajerial=0;
	    $.each(response.data,function(key,value){
	    	value.nilai_alt_kompetensi_manajerial=Math.floor(value.nilai_alt_kompetensi_manajerial);
	    	value.nilai_alt_kompetensi_sosiokultural=Math.floor(value.nilai_alt_kompetensi_sosiokultural);
	    	value.nilai_alt_kompetensi_teknis=(value.nilai_alt_kompetensi_teknis==null?0:parseFloat(value.nilai_alt_kompetensi_teknis));
	      row += '<tr class="data-row" id="row-'+value.id+'">';
	      row += '<td width="50" class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
	      if(value.mobileAccount!=null){
		
	    	  row += '<td width="150"><a href="per-individu?mode=nilai-kompetensi&nip='+value.nip+'&kode-sesi='+value.kode_sesi+'">'+value.nama+'<a/><br/><span class="badge '+(value.status_responden=='SELESAI'?'badge-success':'badge-secondary')+'">'+value.status_responden+'</span><br/><a target="_blank" href="https://wa.me/62'+(value.mobileAccount).substring(1, (value.mobileAccount).length)+'">'+value.mobileAccount+'</a></td>';
	      }else{
	    	  row += '<td width="150"><a href="per-individu?mode=nilai-kompetensi&nip='+value.nip+'&kode-sesi='+value.kode_sesi+'">'+value.nama+'<a/><br/><span class="badge '+(value.status_responden=='SELESAI'?'badge-success':'badge-secondary')+'">'+value.status_responden+'</span><br/><a target="_blank" href="">'+value.mobileAccount+'</a></td>';
	      }
	      row += '<td width="100" class="text-center">'+value.nip+'</td>';
	      row += '<td width="100" class="text-center">'+value.golongan+'</td>';
	      row += '<td width="100">'+value.namaKelompokJabatan+'</td>';
	      row += '<td width="100">'+value.namaJenjangJabatan+'</td>';
	      row += '<td width="100">'+value.kodeEselon1+'</td>';
	      row += '<td width="100">'+value.namaUnitAuditi+'</td>';
	      row += '<td width="100">'+value.namaPropinsi+'</td>';
	      row += '<td width="100" class="text-center">'+value.nilai_alt_kompetensi_manajerial+'</td>';
	      row += '<td width="100" class="text-center">'+value.nilai_alt_kompetensi_sosiokultural+'</td>';
	      row += '<td width="100" class="text-center">'+value.nilai_alt_kompetensi_teknis+'</td>';
	      row += '</tr>';
	      nilai_komp_teknis+=value.nilai_alt_kompetensi_teknis;
	      count_nilai_komp_teknis++;
	      nilai_komp_sosiokultural+=value.nilai_alt_kompetensi_sosiokultural;
	      count_nilai_komp_sosiokultural++;
	      nilai_komp_manajerial+=value.nilai_alt_kompetensi_manajerial;
	      count_nilai_komp_manajerial++;
	    });
	    $('#nilai-komp-teknis').text((nilai_komp_teknis/count_nilai_komp_teknis).toFixed(2));
	    $('#nilai-komp-sosiokultural').text(Math.floor(nilai_komp_sosiokultural/count_nilai_komp_sosiokultural));
	    $('#nilai-komp-sosiokultural').attr('title',(nilai_komp_sosiokultural/count_nilai_komp_sosiokultural));
	    $('#nilai-komp-manajerial').text(Math.floor(nilai_komp_manajerial/count_nilai_komp_manajerial));
	    $('#nilai-komp-manajerial').attr('title', (nilai_komp_manajerial/count_nilai_komp_manajerial));
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayIndividu('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
		
		initTableHeadFixed();  
		
},'onGetListError');
}
function initTableHeadFixed(){
	$('#hidar-table').html($('#tbl-kompetensi-individu thead').html())
	$('#tbl-kompetensi-individu thead').attr('style', 'visibility: collapse;')
	$('#tbl-hidar-table').attr('style','width:'+$('#tbl-kompetensi-individu').width()+'px');
	
	
	//get width
	var arrWidth=[];
	$.each($('#tbl-kompetensi-individu').find('thead tr').children(), function(key){
		console.log(this.clientWidth)
		arrWidth.push(this.clientWidth);
	});
	$.each($('#tbl-hidar-table').find('thead tr').children(), function(key){
		console.log(this.width=arrWidth[key]);
	});
	//end width
		
	/*$(".header-fixed").html("");
	var tableOffset = $("#tbl-kompetensi-individu").offset().top;
	var $header = $("#tbl-kompetensi-individu").find('thead').clone();
	var $fixedHeader = $(".header-fixed").append($header);

	$(window).bind("scroll", function() {
	    var offset = $(this).scrollTop();

	    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
	        $fixedHeader.show();
	    }
	    else if (offset < tableOffset) {
	        $fixedHeader.hide();
	    }
	});*/
}

var chartDisplayProgess;
function displayProgress() {
	/*old param
	var select_id_eselon1=$('[name=filter_id_eselon1]').val();
  	var select_id_unit_auditi=$('[name=filter_id_unit_auditi]').val();
  
	  var clause=[];
	  if($('[name=filter_id_unit_auditi]').val()!=""){
		  clause.push('id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')');
	  }
	  if($('[name=filter_id_eselon1]').val()!=""){
		 //clause.push('id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')');
		  clause.push('id_eselon1_responden.eq('+$('[name=filter_id_eselon1]').val()+')');
	  }
	  if($('[name=filter_id_jenjang_jabatan]').val()!=""){
		  //clause.push('id_jenjang_master_jabatan.eq('+$('[name=filter_id_jenjang_jabatan]').val()+')');
	  }
	  if($('[name=filter_status]').val()!=""){
		  //clause.push('id_eselon1_responden.eq('+$('[name=filter_status]').val()+')');
	  }

	var newclause=[];
	var newparam="";
	if($('[name=filter_id_propinsi]').val()!=""){
		newclause.push('id_propinsi_unit_auditi.eq('+$('[name=filter_id_propinsi]').val()+')');
	}
	if($('[name=filter_id_organisasi]').val()!=""){
		var val1=[];
		$("[name=filter_id_propinsi] option").each(function(){
		   if(this.value!="") val1.push(this.value)
		});
		newclause.push('id_propinsi_unit_auditi.in('+val1.toString()+')');
	}
	if($('[name=filter_id_jenjang_jabatan]').val()!=""){
		newclause.push('id_jenjang_jabatan_jabatan.eq('+$('[name=filter_id_jenjang_jabatan]').val()+')');
	}
	
	if(newclause.length > 0){
		newparam="&filter_funct="+newclause.toString();
	}
	
  	if(satkerId!="") clause.push('id_unit_auditi.eq('+satkerId+')');
  
	params=clause.toString();
	//end param */
	var params=$('#filter-kompetensi-individu').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
  	if(params!="") params+="&";
	params+="filter_kode_sesi="+$.urlParam('kode-sesi');
	
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(ctx + '/dashboard/laporan-progress-responden?'+params,function(response){
  		$.LoadingOverlay("hide");
		console.log(response);
		$.each(response.data,function(key,value){
			$('#progress-chart-'+key).remove();
			$('.progress-chart-'+key).append('<canvas id="progress-chart-'+key+'"><canvas>');
			var canvas = document.querySelector('#progress-chart-'+key); // why use jQuery?
			var ctx = canvas.getContext('2d');
			var x = canvas.width/2;
			var y = canvas.height/2;
			ctx.font = '10pt Verdana';
			ctx.textAlign = 'center';
			ctx.fillText('This text is centered on the canvas', x, y);
			//end here
			
			$('#julah-peserta-berjalan-'+key).text(value.totalProgress);
			$('#total-peserta-'+key).text(value.totalExisting);
			
			// Percent Chart Diatas
			$('#progress-nama-kode-kelompok-berjalan-'+key).html(value.kodeKelompokJabatan);
			$('#progress-id-kelompok-'+key).data('id',value.idKelompokJabatan);
			$('#progress-id-kelompok-'+key).attr('data-id',value.idKelompokJabatan);
			if($('#progress-id-kelompok-'+key).data('id')==$.urlParam('id_kelompok')){
				$('#progress-id-kelompok-'+key).addClass("bg-info text-white");
			}
			var canvas = document.getElementById('progress-chart-'+key);
			if(canvas == null) return;
			var ctx = canvas.getContext("2d");
			if (ctx) {
				var data = [value.totalProgress, value.totalExisting-value.totalProgress];
				ctx.height = 100;
				chartDisplayProgess = new Chart(ctx, {
					type: 'doughnut',
					data: {
						datasets: [
							{
								label: "Data Chart SPJK",
								data: data,
								backgroundColor: [ '#4A9B82', '#cfe3dd' ],
								hoverBackgroundColor: [ '#7ac2ac', '#dfebe7' ],
								borderWidth: [	0, 0 ],
								hoverBorderColor: [ 'transparent', 'transparent' ]
							}
						],
						labels: [ 'Telah Login', 'Belum Login' ]
					},
					options: {
					    aspectRatio: 1,
						tooltips: { 
						    titleFontSize: 7.5,
						    bodyFontSize: 7.5,
							enabled: true,
						},
						maintainAspectRatio: false,
						responsive: true,
						cutoutPercentage: 65,
						percentageInnerCutout: 40,
						animation: {
							duration: 0,
							animateScale: false,
							animateRotate: false,
							onComplete : function(e){
								var width = this.chart.width, height = this.chart.height;
								var cx = width / 2;
								var cy = height / 2;
								this.chart.ctx.textAlign = 'center';
								this.chart.ctx.textBaseline = 'middle';
								this.chart.ctx.font = '10px verdana';
								this.chart.ctx.fillStyle = 'black';
								this.chart.ctx.fillText((( Number.isNaN(data[0]/(data[0]+data[1]))?0:data[0]/(data[0]+data[1]) )*100).toFixed(2)+"%", cx, cy);
								this.chart.ctx.animation = false;
							}
						},
						hover: { animationDuration: 0 },
						responsiveAnimationDuration: 0,
						legend: {
							display: false
						},
					}
				});	
			} 
		});
  	},'onGetListError');
}

/*function displayProgress(page=1, limit=10, params='') {
  var select_id_eselon1=$('[name=filter_id_eselon1]').val();
  var select_id_unit_auditi=$('[name=filter_id_unit_auditi]').val();
  
  var clause=[];
  if($('[name=filter_id_unit_auditi]').val()!=""){
	  clause.push('id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')');
  }
  if($('[name=filter_id_eselon1]').val()!=""){
	 //clause.push('id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')');
	  clause.push('id_eselon1_responden.eq('+$('[name=filter_id_eselon1]').val()+')');
  }
  if($('[name=filter_id_jenjang_jabatan]').val()!=""){
	  //clause.push('id_jenjang_master_jabatan.eq('+$('[name=filter_id_jenjang_jabatan]').val()+')');
  }
  if($('[name=filter_status]').val()!=""){
	  //clause.push('id_eselon1_responden.eq('+$('[name=filter_status]').val()+')');
  }

	var newclause=[];
	var newparam="";
	if($('[name=filter_id_propinsi]').val()!=""){
		newclause.push('id_propinsi_unit_auditi.eq('+$('[name=filter_id_propinsi]').val()+')');
	}
	if($('[name=filter_id_organisasi]').val()!=""){
		var val1=[];
		$("[name=filter_id_propinsi] option").each(function(){
		   if(this.value!="") val1.push(this.value)
		});
		newclause.push('id_propinsi_unit_auditi.in('+val1.toString()+')');
	}
	if($('[name=filter_id_jenjang_jabatan]').val()!=""){
		newclause.push('id_jenjang_jabatan_jabatan.eq('+$('[name=filter_id_jenjang_jabatan]').val()+')');
	}
	
	if(newclause.length > 0){
		newparam="&filter_funct="+newclause.toString();
	}
	
  if(satkerId!="") clause.push('id_unit_auditi.eq('+satkerId+')');
  
	params=clause.toString();
	ajaxGET(path + '/kelompok_jabatan?page='+page+'&limit='+limit,function(response){
		var responsedatakelompokjabatan=response.data;
		ajaxGET(path + '/progress_peserta_ujian_detil?page='+page+'&limit='+100000+'&join=kelompok_jabatan,unit_auditi,jabatan&filter='+public_filter+","+params+newparam,function(response){
		  	$.each(responsedatakelompokjabatan, function(key, value){
		  		value["jumlah_peserta_berjalan"]=0;
		  		value["total_peserta"]=0;
		  		value["kode_sesi"]=$.urlParam('kode-sesi');
		
		  		$.each(response.data, function(key){
		  	  		if(this.id_kelompok_jabatan==value.id){
		  	  			value["jumlah_peserta_berjalan"]+=this.jumlah_peserta_berjalan;
		  	  			value["total_peserta"]+=this.total_peserta;
		  	  		}
		  	  	})
		  	})
		  	public_list_data_progress_peserta_ujian = responsedatakelompokjabatan;
		  	generateDynamicChartProgressPesertaUjian();
		},'onGetListError');
	},'onGetListError');
}*/

/*function displayProgress(page=1, limit=10, params='') {
	var tbody = $("#tbl-progress-peserta-ujian").find('tbody'), row = "";
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(path + '/progress_peserta_ujian?page='+page+'&limit='+limit+'&join=kelompok_jabatan&filter='+public_filter+params,function(response){
  	console.log('list_progress_peserta_ujian', response.data);
  	public_list_data_progress_peserta_ujian = response.data;
  	generateChartProgressPesertaUjian();
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</td>';
      row += '<td>'+value.jumlah_peserta_berjalan+'</td>';
      row += '<td>'+value.total_peserta+'</td>';
      row += '<td class="text-center">'+value.kode_sesi+'</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayProgress('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}*/

function displayRataRataKompetensiTeknis(page=1, limit=10, params='') {
	var tbody = $("#tbl-teknis").find('tbody'), row = "";
	var url = path + '/rata_rata_kompetensi_teknis?'+public_qry_join+'&filter='+public_filter+params;
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
	if(public_list_data_teknis_chart.length==0) {
	  ajaxGET(url+'&limit=1000000',function(response){
	  	console.log('list_rata_rata_kompetensi_teknis', response.data);
	  	public_list_data_teknis_chart = response.data;
	  	generateChartTeknis();
		});
	}
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(url+'&page='+page+'&limit='+limit,function(response){
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</br>'+value.kode_sesi+'</td>';
      row += '<td>'+value.tingkat_jabatan.nama+'</td>';
      row += '<td>'+value.jenjang_jabatan.nama+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+' '+value.jenjang_jabatan.nama+' '+value.tingkat_jabatan.nama+'</td>';
      row += '<td class="text-center">'+value.nilai+'</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayRataRataKompetensiTeknis('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}

function displayRataRataKompetensiSosiokultural(page=1, limit=10, params='') {
	var tbody = $("#tbl-sosiokultural").find('tbody'), row = "";
	var url = path + '/rata_rata_kompetensi_sosiokultural?'+public_qry_join+'&filter='+public_filter+params;
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
	if(public_list_data_sosiokultural_chart.length==0) {
	  ajaxGET(url+'&limit=1000000',function(response){
	  	console.log('list_rata_rata_kompetensi_sosiokultural', response.data);
	  	public_list_data_sosiokultural_chart = response.data;
	  	generateChartSosiokultural();
		});
	}
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(url+'&page='+page+'&limit='+limit,function(response){
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</br>'+value.kode_sesi+'</td>';
      row += '<td>'+value.tingkat_jabatan.nama+'</td>';
      row += '<td>'+value.jenjang_jabatan.nama+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+' '+value.tingkat_jabatan.nama+' '+value.jenjang_jabatan.nama+'</td>';
      row += '<td class="text-center">'+value.nilai+'</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayRataRataKompetensiSosiokultural('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}

function displayRataRataKompetensiManajerial(page=1, limit=10, params='') {
	var tbody = $("#tbl-manajerial").find('tbody'), row = "";
	var url = path + '/rata_rata_kompetensi_manajerial?'+public_qry_join+',kelompok_manajerial&filter='+public_filter+params;
	if(page==1) tbody.text('');
	tbody.find('#more-button-row').remove();
	tbody.find('#retry-button-row').remove();
	if(public_list_data_manajerial_chart.length==0) {
	  ajaxGET(url+'&limit=1000000',function(response){
	  	console.log('list_rata_rata_kompetensi_manajerial', response.data);
	  	public_list_data_manajerial_chart = response.data;
	  	generateChartManajerial();
		});
	}
  tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
  ajaxGET(url+'&page='+page+'&limit='+limit,function(response){
		tbody.find('#loading-row').remove();
		tbody.find('#no-data-row').remove();
    $.each(response.data,function(key,value){
      row += '<tr class="data-row" id="row-'+value.id+'">';
      row += '<td class="text-center">'+(((page-1)*limit)+key+1)+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+'</br>'+value.kode_sesi+'</td>';
      row += '<td>'+value.tingkat_jabatan.nama+'</td>';
      row += '<td>'+value.jenjang_jabatan.nama+'</td>';
      row += '<td>'+value.kelompok_manajerial.nama+'</td>';
      row += '<td>'+value.kelompok_jabatan.kode+' '+value.tingkat_jabatan.nama+' '+value.jenjang_jabatan.nama+'</td>';
      row += '<td class="text-center">'+value.nilai_standard+'</td>';
      row += '<td class="text-center">'+value.nilai_pemetaan+'</td>';
      row += '</tr>';
    });
		if(response.next_more){
			row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
			row += '	<button class="btn btn-outline-secondary" onclick="displayRataRataKompetensiManajerial('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
			row += '</div></td></tr>';
		}
		row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
  },'onGetListError');
}


function display(){
	
	// individuChart
	displayIndividu();
    
		// progressChart
    displayProgress();
    
		// teknisChart
    displayRataRataKompetensiTeknis();
    
		// sosiokulturalChart
    displayRataRataKompetensiSosiokultural();
    
		// manajerialChart
    displayRataRataKompetensiManajerial();
    
}

function onGetListDetilSuccess(response){
    console.log('rekapitulasi-satwa: ', response.data);
    var tbody = $("#tbl-data").find('tbody');
    
    var row = "";
    var num = 1;
    var hidup = 0;
    var sakit = 0;
    var mati = 0;
    var dalam = 0;
    var titip = 0;
    var keluar = 0;
    $.each(response.data,function(key,value){
        row += '<tr class="data-row" id="row-'+value.id+'">';
        row += '<td class="text-center">'+(num)+'</td>';
        row += '<td class=""> '+value.nama +' </td>';
        row += '<td align="right" class=""> '+value.hidup +' </td>';
        row += '<td align="right" class=""> '+value.sakit +' </td>';
        row += '<td align="right" class=""> '+value.mati +' </td>';
        // row += '<td align="right" class=""> '+value.titip +' </td>'
        row += '<td align="right" class=""> '+value.titip +' </td>';
        row += '<td align="right" class=""> '+value.keluar +' </td>';
        row += '</tr>';
        
        hidup += value.hidup;
        sakit += value.sakit;
        mati += value.mati;
        dalam += value.titip;
        titip += value.titip;
        keluar += value.keluar;
        
        num++;
    });
    
    tbody.html(row);
    
    var tfoot = $("#tbl-data").find('tfoot');
    
    row = '<tr class="data-row" id="row-total">';
    row += '<td align="center" colspan="2"><strong>JUMLAH TOTAL</strong></td>';
    row += '<td align="right" class=""> '+hidup +' </td>';
    row += '<td align="right" class=""> '+sakit +' </td>';
    row += '<td align="right" class=""> '+mati +' </td>';
    row += '<td align="right" class=""> '+titip +' </td>';
    row += '<td align="right" class=""> '+keluar +' </td>';
// row += '<td align="right" class=""> '+dalam +' </td>';
    row += '</tr>';
    
    tfoot.html(row);
    
}

function onGetTahunSuccess(response){
    console.log('chart-tahunan: ', response.data);
    var MONTHS = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var color = Chart.helpers.color;
    var data = {
        labels: MONTHS,
        datasets: [
          {
            label: "Satwa Masuk",
            data: response.data.penerimaan,
            backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
            borderColor: window.chartColors.yellow,
            borderWidth: 1
          },
          {
            label: "Satwa Keluar",
            data: response.data.pengeluaran,
            backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
            borderColor: window.chartColors.green,
            borderWidth: 1
          }
        ]
      }
    if(chartBar == null) generateChart(data);
    else { chartBar.data = data; window.chartBar.update(); }
}

function generateChart(data) {
  try {
    var ctx = document.getElementById("monthlyChart");
    if (ctx) {
      ctx.height = 100;
      chartBar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          legend: { position: 'top', labels: { fontFamily: 'Poppins' } },
          scales: { xAxes: [{ ticks: { fontFamily: "Poppins" } }], yAxes: [{ ticks: { beginAtZero: true, fontFamily: "Poppins" } }] }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
};

function generateExcel(){
//	if($('input:checkbox:checked').length==0){
//		return alert("Kode Unit Belum dipilih, minimal pilih 1.");
//	}
	//window.location.href=ctx+"/pertanyaan/preview-export";
	//var obj = $('#entry-form-tab').serialize();
	Swal.fire({
		title: 'Anda akan mendownload Laporan Responden?',
		showDenyButton: true,
		showCancelButton: true,
		confirmButtonText: `Download Semua Sesi`,
		denyButtonText: `Download Hanya Sesi Yang Sekarang Sedang dipilih`,
		allowOutsideClick : false
	}).then((result) => {
	  if(result.isConfirmed) {
		doGenerateExcel(1);
	  }else if (result.isDenied) {
	  	doGenerateExcel();
	  }
	})
}

function doGenerateExcel(mode){
	var obj = $('#filter-kompetensi-individu').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	//obj+='&id_bidang_teknis='+$('[name=filter_id_bidang_teknis]').val(); 
	if(mode==undefined && $.urlParam('kode-sesi')!=null){
		if(obj=="") obj="kode_sesi="+$.urlParam('kode-sesi');
	}
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    ajaxGETDATA(ctx + '/dashboard/laporan-export?'+obj,'onGenerateExcelSuccess','onSaveError');
}

function onGenerateExcelSuccess(response){
	$.LoadingOverlay("hide");
	var a = document.createElement('a');
    var url = window.URL.createObjectURL(response);
    a.href = url;
    a.download = 'myfile.xlsx';
    document.body.append(a);
    a.click();
    a.remove();
    window.URL.revokeObjectURL(url);

    Swal.fire('Berhasil!', '', 'success');
}

function onGetListError(response){
	alert(response.statusText);
	console.log(response);
}