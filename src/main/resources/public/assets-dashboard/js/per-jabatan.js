var path = ctx + '/restql';
var path_inventori = ctx + '/inventori';

var chartBar = null;

window.isMobile = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

var public_filter = 'kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')';
var selected_id_kelompok = ($.urlParam('id_kelompok')!=null?$.urlParam('id_kelompok'):"");
var selected_mode = ($.urlParam('mode')!=null?$.urlParam('mode'):"");
function init(){
	if($.urlParam('kode-sesi')==null){
		showListSesi();
	}
	
	if($.urlParam('print')==1){
		printPDF();
	}
	
	ajaxGET(path + '/eselon1'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
		$.each(response.data, function(key,value){ $('[name=filter_id_eselon1]').append(new Option(value.nama, value.id)); });
	
		if(response.data.length==1){
			$('[name=filter_id_eselon1]').val(response.data[0].id);
		}
	});

	ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
		$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
	});
	
	if($('[name=filter_id_organisasi]').val()!=undefined){
		ajaxGET(path + '/mapping_organisasi'+"?limit=1000000",function(response){
			$.each(response.data, function(key,value){ 
				if(response.data.length==1) $('[name=filter_id_organisasi]').html(new Option(value.nama, value.id));
				else $('[name=filter_id_organisasi]').append(new Option(value.nama, value.id)); 
			});
			if(response.data.length==1) $('[name=filter_id_organisasi]').trigger('change');
			//if(response.data.length==1) $('[name=filter_id_organisasi]')[0].remove();
		});
	}else{
		ajaxGET(path + '/propinsi'+"?limit=1000000&",function(response){
			$.each(response.data, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.nama, value.id)); });
		});
	}
	
	/*
	var params = (this.value!=""?"&filter=id_header.eq(\""+this.value+"\")":"");
	params+="&join=propinsi";
	ajaxGET(path + '/mapping_organisasi_detil'+"?limit=1000000"+params,function(response){
		$('[name=filter_id_propinsi]').empty();
		$('[name=filter_id_propinsi]').append('<option value="">Propinsi</option>');
		$.each(response.data, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.propinsi.nama, value.id)); });
	});
	*/

	display();
	
	$('[name=filter_id_eselon1]').change(function(){
		var params = (this.value!=""?"&filter=id_eselon1.eq(\""+this.value+"\")":"");
		ajaxGET(path + '/unit_auditi'+"?limit=1000000"+params,function(response){
			$('[name=filter_id_unit_auditi]').empty();
			$('[name=filter_id_unit_auditi]').append('<option value="">Satker</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
		});
	});
	
	$('[name=filter_id_organisasi]').change(function(){
		var params = (this.value!=""?"&filter=id_header.eq(\""+this.value+"\")":"");
		params+="&join=propinsi";
		ajaxGET(path + '/mapping_organisasi_detil'+"?limit=1000000"+params,function(response){
			$('[name=filter_id_propinsi]').empty();
			$('[name=filter_id_propinsi]').append('<option value="">Propinsi</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.propinsi.nama, value.propinsi.id)); });
		});
	});
	
	//new
	$('[name=filter_id_propinsi]').change(function(){
		var params="kode_simpeg.not(null),";
		params="";
		if($('[name=filter_id_propinsi]').val()!="") params+="id_propinsi.eq(\""+$('[name=filter_id_propinsi]').val()+"\")";
		ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter="+params,function(response){
			$('[name=filter_id_unit_auditi]').empty();
			$('[name=filter_id_unit_auditi]').append('<option value="">Satker</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
		});
	});
	//end new
	
	$('#filter_tingkat_jabatan, #filter_jenjang_jabatan, #filter_bidang_teknis').on('change', function (e) {
		displayTeknis();
		displayManajerial();
		displaySosiokultural();
	});
	
	$('#btn-reset').on('click', function (e) {
		$('#form-search').trigger('reset');
		$('[name=filter_id_unit_auditi]').val('').trigger('change');
		$('[name=filter_id_eselon1]').val('').trigger('change');
		
		$('.overview-item.bg-info').removeClass('bg-info');
		selected_id_kelompok="";
		display();
	});
}

function setKelompokJabatan(e){
	$('#list-kelompok').on('click', '.overview-item', function() {
		 $('.overview-item.bg-info').removeClass('bg-info');
		 $(this).addClass('bg-info');
	});
	//$(e).addClass("bg-info");
	selected_id_kelompok=e.dataset.id;
	//display();
	getSelectBidangTeknis(selected_id_kelompok);
}
function getSelectBidangTeknis(id){
	$('[name=filter_id_bidang_teknis]').empty();
	$('[name=filter_id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	//
	ajaxGET(path + '/bidang_teknis?filter=id_kelompok_jabatan.eq("'+id+'")','onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		//if(this.id!=9) 
		$('[name=filter_id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	//$('[name=filter_id_bidang_teknis]').append(new Option("ERROR", 7));
	if($('[name=filter_id_bidang_teknis]').find('option').length == 2){
		$('[name=filter_id_bidang_teknis]').find('option')[0].remove();
	}
	display();
}


function printPDF(){
	$('aside').attr("style", "display:none!important");
	$('.page-container').attr('style', 'padding-left:0px');
	$('header').attr("style", "display:none!important");
	$('.main-content').attr('style', 'padding:0px');
}

function display(){
	displayProgress();
	displayDemografiExisting();
}

function getBarOption(total, obj={"max" : 0, "param" : null}) {
	return {
		maintainAspectRatio: false,
   		barThickness: 1, 
		tooltips: { 
			enabled: true,
			callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    //label += Math.ceil(parseInt(tooltipItem.yLabel * total) / 100);
                   //ctx.fillText(Math.ceil(parseInt((data*objnew.total))/100), bar._model.x+35, bar._model.y);
					label += obj.param[tooltipItem.index]+"";
                    return label;
                }
            }
		},
		hover :{ animationDuration: 0 },
		legend:{ display: false, position: 'bottom' },
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero:true,
					fontFamily: "'verdana', sans-serif",
					fontSize:10,
//                    userCallback: function(value) {
//                    	console.log(value+"="+value.length);
//                        if(value.length>10){
//                        	value.split(" ");
//                        	value+=value[0]+"<br/>"+value[1];
//                        }
//                    	return value;
//                    },

		              //autoSkip: false,
		              padding: 12,
				        autoSkip: false,
				        maxRotation: 90,
				        minRotation: 90,

				},
				scaleLabel:{ display:false },
				gridLines: { }
			}],
			yAxes: [{
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:10,
                    beginAtZero:true,
                    min: 0,
                    max: obj.max,
                    userCallback: function(value) {
                        return value+'%';
                    },
				},
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				console.log(ctx);
				ctx.textAlign = "right";
				ctx.font = "10px verdana";
				ctx.fillStyle = "#000";
				//ctx.shadowOffsetY = -5;
				ctx.textBaseline = "bottom";
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						//data = parseFloat((dataset.data[index] /* / (data1[index] + data2[index])*/).toFixed(2)*1) + '%';
						
						data = parseFloat((dataset.data[index])) + '%';
						if(dataset.data[index]==100){
							ctx.fillText(data, bar._model.x+20, bar._model.y+20);
						}else{
							ctx.fillText(data, bar._model.x+20, bar._model.y-5);
						}
						
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}

function randomScalingFactor() {
	return Math.round(Math.random() * 10);
};


function getHorizontalBarChart(data){
	// scale second chart based on ratio of data to the first
    var fiddleFactor = 1.1; // determined by guesswork
    var ratio = data.obj_pilihan.label.length * fiddleFactor / data.obj_inti.label.length;
    var container1Height = parseInt(document.getElementById(data.obj_inti.element_container).style.height);
    // scale height of second chart
    document.getElementById(data.obj_pilihan.element_container).style.height = container1Height * ratio + 'px';
    
    if(data.obj_inti.element_container=="container-bar-inti"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_inti.nilai, function(key){
    		if(this < 60){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});
    	resetCanvas(data.obj_inti);

    	var data_inti = data.obj_inti;
    	if(data_inti.jumlah.length < 3 ){
    		$('#container-bar-inti').attr('style', 'height:300px;');
        }else if(data_inti.jumlah.length < 5 ){
        	$('#container-bar-inti').attr('style', 'height:400px;');
    	}else{
  		  	$('#container-bar-inti').attr('style', 'height:'+(50*(data_inti.jumlah.length))+'px;');
    	}
		  
    	var ctx = document.getElementById(data.obj_inti.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: {
    	    labels: data.obj_inti.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_inti.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:16,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            fontColor: 'black',
				            fontStyle: 'normal',
				            maxTicksLimit: 5,
				            paddingLeft: 20,
				            //mirror: true,
				          },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
									ctx.fillStyle = "#fff";
    								ctx.fillText(data.obj_inti.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
    							} else {
    								if(newdata == 0) {
    									ctx.fillStyle = "#dc3545";
    									ctx.fillText(data.obj_inti.jumlah[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
    								}else{
    									ctx.fillStyle = "#fff";
    									ctx.fillText(data.obj_inti.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend1").html(myChart.generateLegend());
    }
    
    if(data.obj_pilihan.element_container=="container-bar-pilihan"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_pilihan.nilai, function(key){
    		if(this < 60){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});
    	resetCanvas(data.obj_pilihan);
    	var data_pilihan = data.obj_pilihan;
    	if(data_pilihan.jumlah.length < 3 ){
  		  $('#container-bar-pilihan').attr('style', 'height:200px;');
      	}else if(data_pilihan.jumlah.length < 5 ){
		  $('#container-bar-pilihan').attr('style', 'height:400px;');
    	}else{
    		$('#container-bar-pilihan').attr('style', 'height:'+(50*(data_pilihan.jumlah.length))+'px;');
    	}
    	
    	var ctx = document.getElementById(data.obj_pilihan.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: {
    	    labels: data.obj_pilihan.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_pilihan.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:16,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            fontColor: 'black',
				            fontStyle: 'normal',
				            maxTicksLimit: 5,
				            paddingLeft: 20,
				            //mirror: true,
				          },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
    								ctx.fillStyle = "#fff";
    								ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
    							} else {
    								if(newdata == 0) {
    									ctx.fillStyle = "#dc3545";
    									ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
    								}else{
    									ctx.fillStyle = "#fff";
    									ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend2").html(myChart.generateLegend());
    }
}

function getBarChart(data){
	var rulecolor =[];
	var rulehovercolor =[];
	$.each(data.nilai, function(key){
		if(this < 60){
			rulecolor.push("#dc3545");
			rulehovercolor.push("#dc3545c7");
		}else{
			rulecolor.push("#4A9B82");
			rulehovercolor.push("#70c2a9");
		}
	});
	resetCanvas();
	var ctx = document.getElementById("bar");
	//ctx.height = 250;
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
	    labels: data.label,
	    datasets: [{
    		label: 'Skor',
	        data: data.nilai,
	        backgroundColor: rulecolor,//"#4A9B82", 
	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
	    }]
		},
		options: {
			maintainAspectRatio: false,
			responsive: true,
			tooltips: { enabled: false },
			hover :{ animationDuration: 0 },
			legend:{ display: false, position: 'bottom' },
			legendCallback: function(chart) {
				var labels = ["Tercapai", "Tidak Tercapai"];
				var background = ["#4A9B82", "#dc3545"];
			    var text = []; 
			    text.push('<ul class="' + chart.id + '-legend nav">'); 
			    for (var i = 0; i < labels.length; i++) { 
			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
			        if (labels[i]) { 
			            text.push(labels[i]); 
			        } 
			        text.push('</li>'); 
			    } 
			    text.push('</ul>'); 
			    return text.join(''); 
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontFamily: "'verdana', sans-serif",
						fontSize:16,
	                    min: 0,
	                    max: 100
					},
					scaleLabel:{ display:false },
					gridLines: { }, 
					stacked: true
				}],
				yAxes: [{
					gridLines: {
						display:false,
						color: "#fff",
						zeroLineColor: "#fff",
						zeroLineWidth: 0
					},
					ticks: {
						fontFamily: "'verdana', sans-serif",
						fontSize:16
					},
					stacked: true
				}]
			},
			animation: {
				onComplete: function () {
					var chartInstance = this.chart;
					var ctx = chartInstance.ctx;
					ctx.textAlign = "right";
					ctx.font = "14px verdana";
					ctx.fillStyle = "#fff";
					
					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						Chart.helpers.each(meta.data.forEach(function (bar, index) {
							newdata = (dataset.data[index]);
							//console.log(data);
							if(newdata > 0) {
								ctx.fillStyle = "#fff";
								ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
							} else {
								if(newdata == 0) {
									ctx.fillStyle = "#dc3545";
									ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
								}else{
									ctx.fillStyle = "#fff";
									ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
								}
							}
							
						}),this)
					}),this);
				}
			},
			pointLabelFontFamily : "Quadon Extra Bold",
			scaleFontFamily : "Quadon Extra Bold",
		}
	});
	$("#your-legend-container").html(myChart.generateLegend());
}


var resetCanvas = function (data) {
	  
	  if(data.element_container=="container-bar-inti"){
		  $('#'+data.element+'').remove(); // this is my <canvas> element //width="325" height:500px;"
		  
		  $('#'+data.element_container+'').append('<canvas class="w-100s" style="position: relative;"  id="'+data.element+'"><canvas>');
		  //
		  canvas = document.querySelector('#'+data.element+''); // why use jQuery?
		  ctx = canvas.getContext('2d');
//		  ctx.canvas.width = $('#container-bar').width(); // resize to parent width
//		  ctx.canvas.height = $('#container-bar').height(); // resize to parent height

		  var x = canvas.width/2;
		  var y = canvas.height/2;
		  ctx.font = '10pt Verdana';
		  ctx.textAlign = 'center';
		  ctx.fillText('This text is centered on the canvas', x, y);
	  }
	  if(data.element_container=="container-bar-pilihan"){
		  $('#'+data.element+'').remove(); // this is my <canvas> element //width="325" height="325"height:600px;" 
		  
		  $('#'+data.element_container+'').append('<canvas class="w-100s" style="position: relative;" id="'+data.element+'"><canvas>');
		  //
		  canvas = document.querySelector('#'+data.element+''); // why use jQuery?
		  ctx = canvas.getContext('2d');
//		  ctx.canvas.width = $('#container-bar').width(); // resize to parent width
//		  ctx.canvas.height = $('#container-bar').height(); // resize to parent height

		  var x = canvas.width/2;
		  var y = canvas.height/2;
		  ctx.font = '10pt Verdana';
		  ctx.textAlign = 'center';
		  ctx.fillText('This text is centered on the canvas', x, y);
	  }
};

function getRadarChart(data) {

	var randomScalingFactor = function() {
		return Math.round(Math.random() * 10);
	};

	var color = Chart.helpers.color;
	var config = {
		type: 'radar',
		data: {
			labels: data.label,
			datasets: [{
				label: 'Nilai Pemetaan',
				backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
				borderColor: window.chartColors.green,
				pointBackgroundColor: window.chartColors.green,
				data: data.nilai
			}, {
				label: 'Nilai Standard',
				backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
				borderColor: window.chartColors.blue,
				pointBackgroundColor: window.chartColors.blue,
				data: data.nilai_standar
			}]
		},
		options: {
			legend: {
				position: 'bottom',
			},
			scale: {
				ticks: {
					beginAtZero: true
				}
			}
		}
	};

	var ctx = document.getElementById("radar");
//	ctx.height = isMobile() ? 200 : 200;
//
//	var colorNames = Object.keys(window.chartColors);

	if(myChart == null) {
		var myChart = new Chart(ctx, config);
	}else { 
		myChart.data.datasets[0].data = data.nilai;
		myChart.data.datasets[1].data = data.nilai_standar;
		myChart.update(); 
    }
}

function addPercentage(canvas, ctx, data){
    var cx = canvas.width / 4;
    var cy = canvas.height / 4;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '12px verdana';
    ctx.fillStyle = 'black';
    ctx.fillText(parseInt((data[0]/(data[0]+data[1]))*100)+"%", cx, cy);
}

var arr_total_gender_usia=[];
var total_gender_usia=0;
var chartDisplayProgess;
function displayProgress(page=1, limit=0, params='') {
	var params=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
  	if(params!="") params+="&";
	params+="filter_kode_sesi="+$.urlParam('kode-sesi');
	if(selected_id_kelompok!="") params+='&filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
	
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(ctx + '/dashboard/laporan-progress-responden?'+params,function(response){
  		$.LoadingOverlay("hide");
		console.log(response);
		$.each(response.data,function(key,value){
			$('#progress-chart-'+key).remove();
			$('.progress-chart-'+key).append('<canvas id="progress-chart-'+key+'"><canvas>');
			var canvas = document.querySelector('#progress-chart-'+key); // why use jQuery?
			var ctx = canvas.getContext('2d');
			var x = canvas.width/2;
			var y = canvas.height/2;
			ctx.font = '10pt Verdana';
			ctx.textAlign = 'center';
			ctx.fillText('This text is centered on the canvas', x, y);
			//end here
			
			$('#julah-peserta-berjalan-'+key).text(value.totalProgress);
			$('#total-peserta-'+key).text(value.totalExisting);
			
			// Percent Chart Diatas
			$('#progress-nama-kode-kelompok-berjalan-'+key).html(value.kodeKelompokJabatan);
			$('#progress-id-kelompok-'+key).data('id',value.idKelompokJabatan);
			$('#progress-id-kelompok-'+key).attr('data-id',value.idKelompokJabatan);
			if($('#progress-id-kelompok-'+key).data('id')==$.urlParam('id_kelompok')){
				$('#progress-id-kelompok-'+key).addClass("bg-info text-white");
			}
			var canvas = document.getElementById('progress-chart-'+key);
			if(canvas == null) return;
			var ctx = canvas.getContext("2d");
			if (ctx) {
				var data = [value.totalProgress, value.totalExisting-value.totalProgress];
				ctx.height = 100;
				chartDisplayProgess = new Chart(ctx, {
					type: 'doughnut',
					data: {
						datasets: [
							{
								label: "Data Chart SPJK",
								data: data,
								backgroundColor: [ '#4A9B82', '#cfe3dd' ],
								hoverBackgroundColor: [ '#7ac2ac', '#dfebe7' ],
								borderWidth: [	0, 0 ],
								hoverBorderColor: [ 'transparent', 'transparent' ]
							}
						],
						labels: [ 'Telah Login', 'Belum Login' ]
					},
					options: {
					    aspectRatio: 1,
						tooltips: { 
						    titleFontSize: 7.5,
						    bodyFontSize: 7.5,
							enabled: true,
						},
						maintainAspectRatio: false,
						responsive: true,
						cutoutPercentage: 65,
						percentageInnerCutout: 40,
						animation: {
							duration: 0,
							animateScale: false,
							animateRotate: false,
							onComplete : function(e){
								var width = this.chart.width, height = this.chart.height;
								var cx = width / 2;
								var cy = height / 2;
								this.chart.ctx.textAlign = 'center';
								this.chart.ctx.textBaseline = 'middle';
								this.chart.ctx.font = '10px verdana';
								this.chart.ctx.fillStyle = 'black';
								this.chart.ctx.fillText((( Number.isNaN(data[0]/(data[0]+data[1]))?0:data[0]/(data[0]+data[1]) )*100).toFixed(2)+"%", cx, cy);
								this.chart.ctx.animation = false;
							}
						},
						hover: { animationDuration: 0 },
						responsiveAnimationDuration: 0,
						legend: {
							display: false
						},
					}
				});	
			} 
		});
  	},'onGetListError');
}

function displaySosiokultural(){
	var params="";
	var tbody = $("#tbl-sosiokultural").find('tbody');
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
//    if(selected_id_kelompok!=""){
//    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
//    	
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
//    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    }else{
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params='?filter=id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    }
//    if(params==""){
//    	params+='?filter=jenis_kompetensi.eq(\''+'SOSIOKULTURAL'+'\'),'+public_filter;
//    	if($('#filter_jenjang_jabatan').val()!=""){
//        	params+=',id_jenjang_master_jabatan.eq('+$('#filter_jenjang_jabatan').val()+')';
//        }
//    	if($('#filter_tingkat_jabatan').val()!=""){
//        	params+=',id_tingkat_master_jabatan.eq('+$('#filter_tingkat_jabatan').val()+')';
//        }
//    	params+='&order=id_unit_kompetensi';
//    }else{
//    	params+=',jenis_kompetensi.eq(\''+'SOSIOKULTURAL'+'\'),'+public_filter;
//    	if($('#filter_jenjang_jabatan').val()!=""){
//        	params+=',id_jenjang_master_jabatan.eq('+$('#filter_jenjang_jabatan').val()+')';
//        }
//    	if($('#filter_tingkat_jabatan').val()!=""){
//        	params+=',id_tingkat_master_jabatan.eq('+$('#filter_tingkat_jabatan').val()+')';
//        }
//    	params+='&order=id_unit_kompetensi';
//    }
    params+="filter_jenis_kompetensi=SOSIOKULTURAL&";
    if(selected_id_kelompok!="") params+='filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
    params+=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000",'onGetListDetilSosiSuccess','onGetListDetilError');
    //ajaxGET(path + '/detil_perjabatan_kompetensi'+params+"&limit=10000000",'onGetListDetilSosiSuccess','onGetListDetilError');
}

function displayManajerial(){
    var params="";
    var tbody = $("#tbl-manajerial").find('tbody');
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
//    if(selected_id_kelompok!=""){
//    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
//    	
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
//    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    }else{
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params='?filter=id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    }
//    if(params==""){
//    	params+='?filter=jenis_kompetensi.eq(\''+'MANAJERIAL'+'\'),'+public_filter;
//    	if($('#filter_jenjang_jabatan').val()!=""){
//        	params+=',id_jenjang_master_jabatan.eq('+$('#filter_jenjang_jabatan').val()+')';
//        }
//    	if($('#filter_tingkat_jabatan').val()!=""){
//        	params+=',id_tingkat_master_jabatan.eq('+$('#filter_tingkat_jabatan').val()+')';
//        }
//    	params+='&order=id_unit_kompetensi';
//    }else{
//    	params+=',jenis_kompetensi.eq(\''+'MANAJERIAL'+'\'),'+public_filter;
//    	if($('#filter_jenjang_jabatan').val()!=""){
//        	params+=',id_jenjang_master_jabatan.eq('+$('#filter_jenjang_jabatan').val()+')';
//        }
//    	if($('#filter_tingkat_jabatan').val()!=""){
//        	params+=',id_tingkat_master_jabatan.eq('+$('#filter_tingkat_jabatan').val()+')';
//        }
//    	params+='&order=id_unit_kompetensi';
//    }
    params+="filter_jenis_kompetensi=MANAJERIAL&";
    params+=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000",'onGetListDetilManaSuccess','onGetListDetilError');
    //ajaxGET(path + '/detil_perjabatan_kompetensi'+params+"&limit=10000000",'onGetListDetilManaSuccess','onGetListDetilError');
}

function displayTeknis(){
	var params="";
    var tbody = $("#tbl-teknis").find('tbody');
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
//    if(selected_id_kelompok!=""){
//    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
//    	
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
//    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    }else{
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
//    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
//    	}
//    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
//    		params='?filter=id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
//    	}
//    }
//    if(params==""){
//    	params+='?filter=jenis_kompetensi.in(\''+'TEKNIS_INTI'+'\',\''+'TEKNIS_PILIHAN'+'\'),'+public_filter;
//    	if($('#filter_jenjang_jabatan').val()!=""){
//        	params+=',id_jenjang_master_jabatan.eq('+$('#filter_jenjang_jabatan').val()+')';
//        }
//    	if($('#filter_tingkat_jabatan').val()!=""){
//        	params+=',id_tingkat_master_jabatan.eq('+$('#filter_tingkat_jabatan').val()+')';
//        }
//    	params+='&order=nama_kelompok,id_unit_kompetensi';
//    }else{
//    	params+=',jenis_kompetensi.in(\''+'TEKNIS_INTI'+'\',\''+'TEKNIS_PILIHAN'+'\'),'+public_filter;
//    	if($('#filter_jenjang_jabatan').val()!=""){
//        	params+=',id_jenjang_master_jabatan.eq('+$('#filter_jenjang_jabatan').val()+')';
//        }
//    	if($('#filter_tingkat_jabatan').val()!=""){
//        	params+=',id_tingkat_master_jabatan.eq('+$('#filter_tingkat_jabatan').val()+')';
//        }
//    	params+='&order=nama_kelompok,id_unit_kompetensi';
//    }
    params+="filter_jenis_kompetensi=TEKNIS&";
    if(selected_id_kelompok!="") params+='filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
    params+=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');

	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000",'onGetListTeknisDetilSuccess','onGetListDetilError');
}

function onGetListDetilManaSuccess(response){
	$.LoadingOverlay("hide");
    console.log('response: ', response.data);
    var tbody="";  
    var row = "";
    var num = 1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[];
    $.each(response.data,function(key,value){
    	value.nilai_alt=Math.floor(value.nilai_alt);
        row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(num)+'</td>';
	        row += '<td class=""> '+value.nama_kelompok +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_standar +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_alt +' </td>';
        row += '</tr>';
        num++;
        arrnama.push(value.nama_kelompok);
        arrstandar.push(value.nilai_standar);
        arrnilai.push(value.nilai_alt);
    });
    myobj={
    		label : arrnama,
    		nilai_standar : arrstandar,
    		nilai : arrnilai
    };
    
    tbody = $("#tbl-manajerial").find('tbody');
	getRadarChart(myobj);
	if(row=="") tbody.html('<tr><td colspan="20"><div class="list-group-item text-center">Tidak Ada Data.</div></td></tr>');
	else  tbody.html(row);
	
    console.log(myobj);
}

function onGetListDetilSosiSuccess(response){
	$.LoadingOverlay("hide");
    console.log('response: ', response.data);
    var tbody="";  
    var row = "";
    var num = 1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[];
    $.each(response.data,function(key,value){
    	value.nilai_alt=Math.floor(value.nilai_alt);
        row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(num)+'</td>';
	        row += '<td class=""> '+value.nama_kelompok +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_standar +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_alt +' </td>';
        row += '</tr>';
        num++;
        arrnama.push(value.nama_kelompok);
        arrstandar.push(value.nilai_standar);
        arrnilai.push(value.nilai_alt);
    });
    myobj={
    		label : arrnama,
    		nilai_standar : arrstandar,
    		nilai : arrnilai
    };

	tbody = $("#tbl-sosiokultural").find('tbody');
	if(row=="") tbody.html('<tr><td colspan="20"><div class="list-group-item text-center">Tidak Ada Data.</div></td></tr>');
	else  tbody.html(row);
	
    console.log(myobj);
}

function renderRowDetail(row){
	
}

function onGetListTeknisDetilSuccess(response){
	$.LoadingOverlay("hide");
    console.log('response: ', response.data);
    var tbody = $("#tbl-teknis-inti").find('tbody');
    var tbody2 = $("#tbl-teknis-pilihan").find('tbody');
    var row = "";
    var numi= 1, nump=1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[],arrjumlah=[];
    var count_inti=0, count_pilihan=0;
    var sum_inti=0, sum_pilihan=0;
    var idx_inti=0, idx_pilihan=0;

    $.each(response.data,function(key,value){
        if(value.jenis_kompetensi=="TEKNIS_INTI") count_inti++;
    });

    $.each(response.data,function(key,value){
        if(value.jenis_kompetensi=="TEKNIS_INTI"){
        	if(idx_inti==0){
        	    row += '<tr class="bg-secondary text-white">';
        	    	row += '<th rowspan="'+(count_inti+1)+'" class="text-center">Kompetensi Inti</th>';
        	    	row += '<th class="text-center">No.</tdth>';
        	    	row += '<th class="text-center">Kode</th>';
        	    	row += '<th class="text-center">Nama Kompetensi</th>';
        	    	row += '<th class="text-center">Jumlah Responden</th>';
        	    	row += '<th class="text-center">Skor</th>';
        	    row += '</tr>';
        	}
            row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(numi)+'</td>';
	        row += '<td align="" class="text-left"> '+value.kode_unit_kompetensi +' </td>';
	        row += '<td class=""> '+value.nama_unit_kompetensi +' </td>';
	        row += '<td align="" class="text-center"> '+value.jumlah_unit_komp +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_alt+' </td>';
	        idx_inti++;
	        row += '</tr>';
		    numi++;
		    sum_inti+=value.nilai_alt;

		    arrnama.push(value.kode_unit_kompetensi);
		    arrnilai.push(value.nilai_alt);
		    arrjumlah.push(value.jumlah_unit_komp);
        };
    });
    row += '<tr class="bg-secondary text-white">';
		row += '<td class="text-center"></td>';
		row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<th class="text-center">Rata-Rata</th>';
    	row += '<th class="text-center">'+(sum_inti/count_inti).toFixed(2)+'</th>';
    row += '</tr>';
    tbody.html(row);
    var obj_inti={
    		element : 'bar-inti',
    		element_container : 'container-bar-inti',
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };
    console.log(obj_inti);

    arrnama=[],arrstandar=[],arrnilai=[],arrjumlah=[];
    row="";
    tbody = tbody2;
    var unique_kelompok_teknis=[];
    var unique_kelompok_teknis_with_string=[];
	$.each(response.data,function(key){
        if(this.jenis_kompetensi=="TEKNIS_PILIHAN"){
        	var valueunique = this.nama_kelompok;
			if(unique_kelompok_teknis.indexOf(valueunique) == -1) unique_kelompok_teknis.push(valueunique);
			if(unique_kelompok_teknis_with_string.indexOf(valueunique) == -1) unique_kelompok_teknis_with_string.push(valueunique);
			else unique_kelompok_teknis_with_string.push("");
        }
	});
    
	row += '<tr class="bg-secondary text-white">';
	   	row += '<th class="text-center">Komp.Pilihan</th>';
    	row += '<th class="text-center">No.</tdth>';
    	row += '<th class="text-center">Kode</th>';
    	row += '<th class="text-center">Nama Kompetensi</th>';
    	row += '<th class="text-center">Jumlah Responden</th>';
    	row += '<th class="text-center">Skor</th>';
   	row += '</tr>';
    $.each(unique_kelompok_teknis,function(keyunique, valueuniqe){
    	$.each(response.data,function(key,value){
            if(value.jenis_kompetensi=="TEKNIS_PILIHAN"){
            	//start here
            	if(valueuniqe==value.nama_kelompok){
	    	        if(nump==1 && idx_pilihan>0){
	    	        	row += '<tr>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<th class="text-center">Rata-Rata</th>';
	            	    	row += '<th class="text-center">'+(sum_pilihan/count_pilihan).toFixed(2)+'</th>';
	            	    row += '</tr>';
	            	    row += '<tr>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                row += '</tr>';
		    	        sum_pilihan=0;
		    		    count_pilihan=0;
	    	        }
	                row += '<tr class="data-row" id="row-'+value.id+'">';
	    	        //unique_kelompok_teknis_with_string[idx_pilihan]+"|"+value.nama_kelompok+":"+(value.nama_kelompok==unique_kelompok_teknis_with_string[idx_pilihan])
	                row += '<th class="text-left small">'+unique_kelompok_teknis_with_string[idx_pilihan]+'</th>';
	    	        //
	    	        row += '<td class="text-center">'+(nump)+'</td>';
	    	        row += '<td align="" class="text-center"> '+value.kode_unit_kompetensi +' </td>';
	    	        row += '<td class="small"> '+value.nama_unit_kompetensi +' </td>';
	    	        row += '<td align="" class="text-center"> '+value.jumlah_unit_komp +' </td>';
	    	        row += '<td align="" class="text-center"> '+value.nilai_alt +' </td>';
	    	        
	    	        row += '</tr>';
	    	        nump++;
	    	        idx_pilihan++;
	    		    sum_pilihan+=value.nilai_alt;
	    		    count_pilihan++;

	    		    arrnama.push(value.kode_unit_kompetensi);
	    		    arrnilai.push(value.nilai_alt);
	    		    arrjumlah.push(value.jumlah_unit_komp);
            	}else{
	    	        nump=1;
            	}
    		    //end here
            }
        });
    });

	row += '<tr class="bg-secondary text-white">';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<th class="text-center">Rata-Rata</th>';
    	row += '<th class="text-center">'+(sum_pilihan/count_pilihan).toFixed(2)+'</th>';
    row += '</tr>';
    tbody.html(row);
    
    var obj_pilihan={
    		element : 'bar-pilihan',
    		element_container : 'container-bar-pilihan',
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };
    console.log(obj_pilihan);
    var newobj = {obj_inti, obj_pilihan};
    console.log(newobj);
    getHorizontalBarChart(newobj);
    /*
    myobj={
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };
    console.log(myobj);
    getBarChart(myobj); */
}

function displayDemografi(){
	var params="";
    if(selected_id_kelompok!=""){
    	params='?filter=id_kelompok_master_jabatan.eq('+selected_id_kelompok+')';
    	
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
    		params+=',id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    }else{
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    		params+=',id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
    		params='?filter=id_unit_kerja_user_responden.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
    		params='?filter=id_eselon1_user_responden.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    }
	if(params==""){
		params+='?filter='+public_filter
	}else{
		params+=','+public_filter
	}
	
	var newclause=[];
	var newparam="";
	if($('[name=filter_id_propinsi]').val()!=""){
		newclause.push('id_propinsi_unit_auditi.eq('+$('[name=filter_id_propinsi]').val()+')');
	}
	if($('[name=filter_id_organisasi]').val()!=""){
		var val1=[];
		$("[name=filter_id_propinsi] option").each(function(){
		   if(this.value!="") val1.push(this.value)
		});
		newclause.push('id_propinsi_unit_auditi.in('+val1.toString()+')');
	}
	if(newclause.length > 0){
		newparam="&filter_funct="+newclause.toString();
	}
	params+="&join=unit_auditi";//kelompok_jabatan,tingkat_jabatan,jenjang_jabatan,
    ajaxGET(path + '/detil_perjabatan'+params+newparam+"&limit=10000000",'onGetDisplayDemografiSuccess','onGetListDetilError');
}

function onGetDisplayDemografiSuccess(response){
	$.LoadingOverlay("hide");
	//start master
    var master_jenjang=[];
    var jenjang_unique=[];
    $.each(response.data, function(key){
		var idunique=this.id_jenjang_master_jabatan;
		var valueunique=this.nama_jenjang_master_jabatan;
		if(jenjang_unique.indexOf(valueunique) == -1) {
			jenjang_unique.push(valueunique);
			master_jenjang.push({"id" : idunique, "nama" : valueunique, "jumlah" : 0, "jumlahPercentage" : 0, "total" : 0});
		}
   	});
	
    var sumtotaljumlah=0;
    $.each(master_jenjang,function(master_key, master_val){
	    var count=0;
	    $.each(response.data,function(key,value){
        	if(master_val.id==value.id_jenjang_master_jabatan){
        		count++;
        	}
	    });
	    sumtotaljumlah+=count;
		master_val.jumlah=count;
    });
	$.each(master_jenjang,function(master_key, master_val){
		master_val.jumlahPercentage=((master_val.jumlah/sumtotaljumlah)*100).toFixed(2);
		master_val.total=sumtotaljumlah;
	});
	
	//do sortir data
	master_jenjang.sort(function(a, b){
	    return a.id - b.id;
	});
	
	renderChartJenjangJab(master_jenjang, 'barJenjangJab', 'ContainerBarJenjangJab');
	
	//new
	var objUsia=[
		{"L" : 0, "P" : 0, "label" : "20-29", "start" : 20, "end" : 29, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "30-39", "start" : 30, "end" : 39, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "40-49", "start" : 40, "end" : 49, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "50-59", "start" : 50, "end" : 59, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "60+", "start" : 60, "end" : 89, "total" : 0}
	];
	
	
    $.each(objUsia,function(key, temp_value){
		var countAll=0;
		var countL=0, countP=0;
	    $.each(response.data,function(key,value){
			if(value.umur_user_responden <= temp_value.end && value.umur_user_responden >= temp_value.start){
				if(value.jenis_kelamin_user_responden=="L") {
					countL++;
					temp_value.L=countL;
	    		}
				if(value.jenis_kelamin_user_responden=="P") {
					countP++;
					temp_value.P=countP;
	    		}
			}
	    	countAll++;
	    });
		temp_value.total=countAll;
	});
	console.log(objUsia);
	//"label" : arrlabelkat, 
	//"jumlah" : arrkat, 
	//"total" : countAll, 
	var objED = {
					"data" : objUsia, 
					"canvas" : "barHorizontal", 
					"containerCanvas" : "ContainerBarHorizontal"
				};
				
	renderChartUsia(objED);
	//ini data chart usia
	/*var arrlabelkat=["20-29", "30-39", "40-49", "50-59", "60+"];
	var arrkat=[
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0}
	];
	
	var countAll=0;
	var countL=0, countP=0;
    $.each(response.data,function(key,value){
    	if(value.umur_user_responden <= 29){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[0].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[0].P+=1;
    		}
    	}
    		
    	if(value.umur_user_responden > 29 && value.umur_user_responden <= 39){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[1].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[1].P+=1;
    		}
    	}
    	
    	if(value.umur_user_responden > 39 && value.umur_user_responden <= 49){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[2].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[2].P+=1;
    		}
    	}

    	if(value.umur_user_responden > 49 && value.umur_user_responden <= 59){
    		if(value.jenis_kelamin_user_responden=="L") {
    			arrkat[3].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			arrkat[3].P+=1;
    		}
    	}

    	if(value.umur_user_responden > 59){
    		if(value.jenis_kelamin_user_responden=="L") {
    			countL++;
    			arrkat[4].L+=1;
    		}
    		if(value.jenis_kelamin_user_responden=="P") {
    			countP++;
    			arrkat[4].P+=1;
    		}
    	}
    	countAll++;
    });

	renderChartUsia({"label" : arrlabelkat, "jumlah" : arrkat, "total" : countAll, "canvas" : "barHorizontal", "containerCanvas" : "ContainerBarHorizontal"});*/
}


//function renderChartJenjangJab(arrnama, arrjumlah, total, eCanvas, eContainer){
function renderChartJenjangJab(arr, eCanvas, eContainer){
	$('#'+eCanvas).remove();
	$('#'+eContainer).append('<canvas class="mw-100" width="425" height="425" id="'+eCanvas+'"><canvas>');
	var canvas = document.querySelector('#'+eCanvas);
	var ctx = canvas.getContext('2d');
	var x = canvas.width;
	var y = canvas.height;
	ctx.font = '10pt Verdana';
	ctx.textAlign = 'center';
	ctx.fillText('This text is centered on the canvas', x, y);

	var arrnama =[];
	var arrjumlah =[];
	var arrjumlahNoPer =[];
	var total = 0;
	$.each(arr, function(key){
		arrnama.push(this.nama);
		arrjumlah.push(parseFloat(this.jumlahPercentage).toFixed(2));
		arrjumlahNoPer.push(this.jumlah);
		total=this.total;
	});
	var maxdata=checkNumber(Math.max(...arrjumlah));
    var ctx = document.getElementById(eCanvas);
	//ctx.height = 400;
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
	    labels: arrnama,//["Muda", "Madya", "Utama"],
	    datasets: [{
    		label: 'Jumlah',
	        data: arrjumlah,//arrjumlah,//data1,
	        backgroundColor: "#3494BA", // biru
	        hoverBackgroundColor: "#5da0ba" // biru lebih muda
	    }]
		},
		options: getBarOption(total, {"max" : parseInt(maxdata)+10, "param" : arrjumlahNoPer}),
	});
}

function renderPercentage(value, numeric) {
	if(numeric == 0) return 0.5;
	return (value < 3 ? 3 : value);
}

var chartUsia;
function renderChartUsia(objnew){
	console.log(objnew);
	//
	$('#'+objnew.canvas).remove();
	$('#'+objnew.containerCanvas).append('<canvas class="mw-100" width="425" height="425" id="'+objnew.canvas+'"><canvas>');
	var canvas = document.querySelector('#'+objnew.canvas); 
	var ctx = canvas.getContext('2d');
	var color = Chart.helpers.color;
	
	// Bar Horizontal
	

	var dataNotPer1 =[];
	var dataNotPer2 =[];
	
	var data1 =[];
	var data2 =[];
	var datalabel = [];
	var total = 0;
	$.each(objnew.data, function(key){
		data1.push(Number.isNaN((this.L/this.total))?0:((this.L/this.total)*100).toFixed(2));
		data2.push(Number.isNaN((this.P/this.total))?0:((this.P/this.total)*100).toFixed(2));
		
		if(this.L*-1==0) {
			dataNotPer1.push("-0");
		}else{
			dataNotPer1.push(this.L*-1);
		}
		dataNotPer2.push(this.P);
		
		datalabel.push(this.label);
		total=this.total;
	});
	console.log(data2);
	
	var data1_custom = [];
	$.each(data1, function(key, value){
		data1_custom.push((value*-1)+(-0.50));
	});
	var data2_custom = [];
	$.each(data2, function(key, value){
		data2_custom.push((value*1)+0.50);
	});
	console.log(dataNotPer1);//console.log(data1_custom);
	console.log(data2_custom);
	
	
	//to get max numb
	var newdata = [];
	$.each(data2, function(key, value){
		newdata.push(value*1);
	});
	$.each(data1_custom, function(key, value){
		newdata.push(value*-1);
	});
	var maxNum = Math.max(...newdata);
	maxNum = checkNumber(maxNum);
	//end here
	
	var ctx = document.getElementById(objnew.canvas);
	//ctx.height = 300;
	chartUsia = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
		    labels: datalabel,
		    datasets: [{
				label: 'Laki-Laki',
			    data: data1_custom,
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
		    },{
				label: 'Perempuan',
			    data: data2_custom,
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
		    }]
		},
		options: 
		{
			maintainAspectRatio: false,
			tooltips: { 
				enabled: true,
				//bodySpacing : 8,
				//titleSpacing : 15, titleMarginBottom : 8, yPadding : 12
				callbacks: {
	                label: function(tooltipItem, data) {
	                	//console.log(tooltipItem);
	                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

	                    if (label) {
	                        label += ': ';
	                    }
	                    //label += Math.round(tooltipItem.yLabel * 100) / 100;
						console.log(tooltipItem);
						console.log(dataNotPer1);
						console.log(dataNotPer2);
	                    var converlabelx = tooltipItem.xLabel;
						if(converlabelx==0){
							label += 0;
						}else{
							if(converlabelx.toString().substr(0,1)=="-"){
								label += dataNotPer1[tooltipItem.index].toString().replaceAll("-","");		
							}else{
								label += dataNotPer2[tooltipItem.index]
							}
						}
	                    return label;
	                }
	            }
			},
			hover :{ animationDuration: 0 },
			legend:{ display: true, position: 'bottom' },
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontFamily: "'verdana', sans-serif",
						fontSize:11,
	                    min: -maxNum-(25),
	                    max: maxNum+(25),
				        autoSkip: false,
				        maxRotation: 90,
				        minRotation: 90,
	                    userCallback: function(value) {
							//untuk hapus tanda minus pada label
	                        if(value < 0){
	                        	value = value.toString().replaceAll("-","")
	                        }
	                        return value;
	                    },
	                    stepSize: 5,
	                    padding: 17
					},
					scaleLabel:{ display:false },
					gridLines: { }, 
					stacked: true,
           
		            /*barPercentage: 0.5,
		            barThickness: 6,
		            maxBarThickness: 8,*/
		            minBarLength: 0,
				}],
				yAxes: [{
					gridLines: {
						/*display:true,
						color: "#fff",
						zeroLineColor: "#fff",
						zeroLineWidth: 0*/
					},
					ticks: {
						fontFamily: "'verdana', sans-serif",
						fontSize:13
					},
					stacked: true,
				}]
			},
			animation: {
				onComplete: function () {
					var chartInstance = this.chart;
					var ctx = chartInstance.ctx;
					ctx.textAlign = "right";
					ctx.font = "12px verdana";
					ctx.fillStyle = "#516b73";
					//console.log("ctx", ctx);
					ctx.textBaseline = "bottom";
					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						Chart.helpers.each(meta.data.forEach(function (bar, index) {
							//console.log(dataset.data[index]);
							//data = parseFloat((dataset.data[index]).toFixed(2)*100);
							
							console.log("data "+dataset.data);
							data = dataset.data[index];
							strdata = dataset.data[index];
							
							if(data >= 0.00){
								data = data-0.50;
								strdata = strdata;
							}else{
								data = data-(-0.50);
								strdata = strdata;
							}
							data = data.toFixed(2);
							//strdata = strdata.toFixed(2);
							if(data >= 0.00){
								if(data==100){
									ctx.fillText(strdata+"%", bar._model.x, bar._model.y+5);
								}else if(data>=75){
									ctx.fillText(strdata+"%", bar._model.x+50, bar._model.y+5);
								}else{
									if(data==0){
									}else{
										ctx.fillText(data+"%", bar._model.x+50, bar._model.y+5);
									}
									
								}
							}else{
								if(data==-100){
									strdata=data.toString().replaceAll("-","")
									ctx.fillText(strdata+"%", bar._model.x+40, bar._model.y+5);
								}else if(data<=-75){
									strdata=data.toString().replaceAll("-","")
									ctx.fillText(strdata+"%", bar._model.x+50, bar._model.y+5);
								}else{
									if(data ==-0){
										//strdata=data.toString().replaceAll("-","")
										//ctx.fillText(strdata+"%", bar._model.x-5, bar._model.y+5);
									}else{
										strdata=data.toString().replaceAll("-","")
										ctx.fillText(strdata+"%", bar._model.x-5, bar._model.y+5);
									}
								}
								
							}
						}),this)
					}),this);
				}
			},
			pointLabelFontFamily : "Quadon Extra Bold",
			scaleFontFamily : "Quadon Extra Bold",
		}	
	});
}


function generateChart(data) {
  try {
    var ctx = document.getElementById("monthlyChart");
    if (ctx) {
      ctx.height = 100;
      chartBar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          legend: { position: 'top', labels: { fontFamily: 'Poppins' } },
          scales: { xAxes: [{ ticks: { fontFamily: "Poppins" } }], yAxes: [{ ticks: { beginAtZero: true, fontFamily: "Poppins" } }] }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }

};


function goToJabatanRekomendasi(){
	if(selected_id_kelompok=="") alert("Kelompok Jabatan Belum dipilih.");
	else{
		var params="";
		if(selected_mode!="") params+="mode="+selected_mode+"&";
		if(selected_id_kelompok!=""){
			params+="id_kelompok="+selected_id_kelompok;
		}
		if($('#filter_tingkat_jabatan').val()!="" && $('#filter_tingkat_jabatan').val()!=undefined){
			params+="&id_tingkatan="+$('#filter_tingkat_jabatan').val();
		}
		if($('#filter_jenjang_jabatan').val()!="" && $('#filter_jenjang_jabatan').val()!=undefined){
			params+="&id_jenjang="+$('#filter_jenjang_jabatan').val();
		}
		if($('[name=filter_id_eselon1]').val()!=""){
			params+="&id_eselon1="+$('[name=filter_id_eselon1]').val();
		}
		if($('[name=filter_id_organisasi]').val()!='' && $('[name=filter_id_organisasi]').val()!=undefined){
			params+="&id_organisasi="+$('[name=filter_id_organisasi]').val();
		}
		if($('[name=filter_id_propinsi]').val()!=""){
			params+="&id_propinsi="+$('[name=filter_id_propinsi]').val();
		}
		if($('[name=filter_id_unit_auditi]').val()!=""){
			params+="&id_unit_auditi="+$('[name=filter_id_unit_auditi]').val();
		}
		if($.urlParam('kode-sesi')!=null){
			params+="&kode-sesi="+$.urlParam('kode-sesi');
		}
		console.log(params);
		//window.location.href="per-jabatan-rekomendasi?"+params;
		window.open("per-jabatan-rekomendasi?"+params, '_blank');
	}
}


function displayDemografiExisting(page=1, limit=0, params='') {
	var params=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
  	if(params!="") params+="&";
	params+="filter_kode_sesi="+$.urlParam('kode-sesi');
	if(selected_id_kelompok!="") params+='&filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
	
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(ctx + '/dashboard/laporan-demografi?'+params,function(response){
  		$.LoadingOverlay("hide");
		
		var response_data_existing = response.data.dataExisting;
		var response_data_pemetaan = response.data.dataPemetaan;
		
		//render existing
		var master_jenjang=[];
		$.each(response_data_existing.dataJenjang, function(){
			master_jenjang.push(this);
		});
		
		master_jenjang.sort(function(a, b){ return a.id - b.id; });
		renderChartJenjangJab(master_jenjang, 'barJenjangJabEx', 'ContainerBarJenjangJabEx');
		
		var objED = {
				"data" : response_data_existing.dataUsia, 
				"canvas" : "barHorizontalEx", 
				"containerCanvas" : "ContainerBarHorizontalEx"
		};
		renderChartUsia(objED);
		//end here
		
		//render pemetaan
		var master_jenjang=[];
		$.each(response_data_pemetaan.dataJenjang, function(){
			master_jenjang.push(this);
		});
		
		master_jenjang.sort(function(a, b){ return a.id - b.id; });
		renderChartJenjangJab(master_jenjang, 'barJenjangJab', 'ContainerBarJenjangJab');
		
		var objED = {
				"data" : response_data_pemetaan.dataUsia, 
				"canvas" : "barHorizontal", 
				"containerCanvas" : "ContainerBarHorizontal"
		};
		renderChartUsia(objED);
		//end here
		
  	},'onGetListError');
}

function OLDdisplayDemografiExisting(){
	var params="";
    if(selected_id_kelompok!=""){
    	params='?filter=id_kelompok_jabatan.eq('+selected_id_kelompok+')';
    	
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
    		params+=',id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    		params+=',id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
    		params+=',id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
    		params+=',id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    }else{
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()!=""){
    		params='?filter=id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    		params+=',id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()!="" && $('[name=filter_id_eselon1]').val()==""){
    		params='?filter=id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')';
    	}
    	if($('[name=filter_id_unit_auditi]').val()=="" && $('[name=filter_id_eselon1]').val()!=""){
    		params='?filter=id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')';
    	}
    }

	if(params==""){
		params+='?filter='+public_filter
	}else{
		params+=','+public_filter
	}
	params+="&join=kelompok_jabatan,tingkat_jabatan,jenjang_jabatan,unit_auditi";
	var select = "select_funct=timestampdiff(YEAR, tgl_lahir_rata_rata_individu_existing, curdate()) umur_rata_rata_individu_existing";
    //
	var newclause=[];
	var newparam="";
	if($('[name=filter_id_propinsi]').val()!=""){
		newclause.push('id_propinsi_unit_auditi.eq('+$('[name=filter_id_propinsi]').val()+')');
	}
	if($('[name=filter_id_organisasi]').val()!=""){
		var val1=[];
		$("[name=filter_id_propinsi] option").each(function(){
		   if(this.value!="") val1.push(this.value)
		});
		newclause.push('id_propinsi_unit_auditi.in('+val1.toString()+')');
	}
	if(newclause.length > 0){
		newparam="&filter_funct="+newclause.toString();
	}
	//
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(path + '/rata_rata_individu_existing'+params+newparam+"&"+select+"&"+"&limit=10000000",'onGetDisplayDemografiExistingSuccess','onGetListDetilError');
}

var totalRespondenExist=0;
function onGetDisplayDemografiExistingSuccess(response){
    console.log(response);
	//$.LoadingOverlay("hide");
	totalRespondenExist=response.count;
	displayDemografi();

	//start master
    var master_jenjang=[];
    var jenjang_unique=[];
    $.each(response.data, function(key){
		var idunique=this.jenjang_jabatan.id;
		var valueunique=this.jenjang_jabatan.nama;
		if(jenjang_unique.indexOf(valueunique) == -1) {
			jenjang_unique.push(valueunique);
			master_jenjang.push({"id" : idunique, "nama" : valueunique, "jumlah" : 0, "jumlahPercentage" : 0, "total" : 0});
		}
   	});
	
    var sumtotaljumlah=0;
    $.each(master_jenjang,function(master_key, master_val){
	    var count=0;
	    var countAll=0;
	    $.each(response.data,function(key,value){
        	if(master_val.id==value.jenjang_jabatan.id){
        		count++;
				master_val.jumlah=count;
        	}
    		countAll++;
	    });
		master_val.jumlahPercentage=((master_val.jumlah/countAll)*100).toFixed(2);
		master_val.total=countAll;
    });

	//do sortir data
	master_jenjang.sort(function(a, b){
	    return a.id - b.id;
	});
	
	console.log(master_jenjang);
  	
	//renderChartJenjangJab(arrnama, arrjumlah, arrtotaljumlah, 'barJenjangJabEx', 'ContainerBarJenjangJabEx');
	renderChartJenjangJab(master_jenjang, 'barJenjangJabEx', 'ContainerBarJenjangJabEx');
	
	//ini data chart usia
	//new
	var objUsia=[
		{"L" : 0, "P" : 0, "label" : "20-29", "start" : 20, "end" : 29, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "30-39", "start" : 30, "end" : 39, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "40-49", "start" : 40, "end" : 49, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "50-59", "start" : 50, "end" : 59, "total" : 0},
		{"L" : 0, "P" : 0, "label" : "60+", "start" : 60, "end" : 89, "total" : 0}
	];
	
	
    $.each(objUsia,function(key, temp_value){
		var countAll=0;
		var countL=0, countP=0;
	    $.each(response.data,function(key,value){
			if(value.umur <= temp_value.end && value.umur >= temp_value.start){
				if(value.jenis_kelamin=="LAKI-LAKI") {
					countL++;
					temp_value.L=countL;
	    		}
				if(value.jenis_kelamin=="PEREMPUAN") {
					countP++;
					temp_value.P=countP;
	    		}
			}
	    	countAll++;
	    });
		temp_value.total=countAll;
	});
	console.log(objUsia);
	//"label" : arrlabelkat, 
	//"jumlah" : arrkat, 
	//"total" : countAll, 
	var objED = {
					"data" : objUsia, 
					"canvas" : "barHorizontalEx", 
					"containerCanvas" : "ContainerBarHorizontalEx"
				};
				
	renderChartUsia(objED);
	
	/*
	//old
	var arrlabelkat=["20-29", "30-39", "40-49", "50-59", "60+"];
	var arrkat=[
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0},
		{"L" : 0, "P" : 0}
	];
	
	var countAll=0;
	var countL=0, countP=0;
    $.each(response.data,function(key,value){
    	if(value.umur <= 29){
    		if(value.jenis_kelamin=="LAKI-LAKI") {
    			arrkat[0].L+=1;
    		}
    		if(value.jenis_kelamin=="PEREMPUAN") {
    			arrkat[0].P+=1;
    		}
    	}
    		
    	if(value.umur > 29 && value.umur <= 39){
    		if(value.jenis_kelamin=="LAKI-LAKI") {
    			arrkat[1].L+=1;
    		}
    		if(value.jenis_kelamin=="PEREMPUAN") {
    			arrkat[1].P+=1;
    		}
    	}
    	
    	if(value.umur > 39 && value.umur <= 49){
    		if(value.jenis_kelamin=="LAKI-LAKI") {
    			arrkat[2].L+=1;
    		}
    		if(value.jenis_kelamin=="PEREMPUAN") {
    			arrkat[2].P+=1;
    		}
    	}

    	if(value.umur > 49 && value.umur <= 59){
    		if(value.jenis_kelamin=="LAKI-LAKI") {
    			arrkat[3].L+=1;
    		}
    		if(value.jenis_kelamin=="PEREMPUAN") {
    			arrkat[3].P+=1;
    		}
    	}

    	if(value.umur > 59){
    		if(value.jenis_kelamin=="LAKI-LAKI") {
    			countL++;
    			arrkat[4].L+=1;
    		}
    		if(value.jenis_kelamin=="PEREMPUAN") {
    			countP++;
    			arrkat[4].P+=1;
    		}
    	}
    	countAll++;
    });

	var objED = {
					"label" : arrlabelkat, 
					"jumlah" : arrkat, 
					"total" : countAll, 
					"canvas" : "barHorizontalEx", 
					"containerCanvas" : "ContainerBarHorizontalEx"
				};
				
	renderChartUsia(objED);*/
}

function checkNumber(num, force=false){
	if(num >= 75) num=num-25;
	if(force) num=num*-1;
	if(num <=9){
		return 10;
	}else if(num <=19){
		return 20;
	}else if(num <=29){
		return 30;
	}else if(num <=39){
		return 40;
	}else if(num <=49){
		return 50;
	}else if(num <=59){
		return 60;
	}else if(num <=69){
		return 70;
	}else if(num <=79){
		return 80;
	}else if(num <=89){
		return 90;
	}else{
		return num;
	}
}

function onGetListError(response){
	$.LoadingOverlay("hide");
	Swal.fire({
	  icon: 'error',
	  html: '<p class="text-left">Waktu load terlalu lama atau mohon coba beberapa saat lagi, dan pastikan internet anda dalam keadaan stabil.</p>'+
		  '<div class="text-left"><strong>Terima Kasih</strong></div>',
	})
}
