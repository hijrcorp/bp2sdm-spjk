var path = ctx + '/restql';
var selected_responden="";
var selected_nip=($.urlParam('nip')==null?"":$.urlParam('nip'));

var chartBar = null;

window.isMobile = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

function init(){
	if($.urlParam('kode-sesi')==null){
		showListSesi();
	}
	
	displayRiwayat();
	
	if(selected_nip!="" && $.urlParam('kode-sesi')!=null){
		//display profile
		ajaxGET(path+'/rata_rata_kompetensi_individu?join=kelompok_jabatan,jenjang_jabatan,unit_auditi,user_responden&filter=nip.eq('+selected_nip+'),kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')',function(response){
			if(response.data.length > 0){
				var value=response.data[0];
				$('#nama_profile').text(value.nama);
				$('#satker_profle').text(value.unit_auditi.nama);
				$('#jabatan_profile').text(value.kelompok_jabatan.nama+" "+value.jenjang_jabatan.nama);
				//nilai
				$('#progress_manajerial').text(Math.floor(value.nilai_kompetensi_manajerial));
				$('#progress_manajerial').attr('title', value.nilai_kompetensi_manajerial);
				$('#progress_sosiokultural').text(Math.floor(value.nilai_kompetensi_sosiokultural));
				$('#progress_sosiokultural').attr('title', value.nilai_kompetensi_sosiokultural);
				$('#progress_teknis').text(value.nilai_kompetensi_teknis);
				//end nilai
				ajaxGET(path+'/bidang_teknis?filter=id.eq('+value.user_responden.id_bidang_teknis+')',function(response){
					var value=response.data[0];
					$('#bidang_teknis').text(value.nama);
				},'onGetListError');
				selected_responden=value.id_user_responden
				//
				displayManajerial();
				displaySosiokultural();
				displayTeknis();
			}else{
				alert('Data Tidak ditemukan');
				if(window.location.search.split('&').length==3) window.location.href=window.location.search.split('&')[0]+"&"+window.location.search.split('&')[2];
				
			}
		},'onGetListError');
		//end display
	}
	$('#btn-search').click(function(){
		if($('[name=nip]').val()!="" && $.urlParam('kode-sesi')!=null){
			ajaxGET(path+'/rata_rata_kompetensi_individu?join=kelompok_jabatan,jenjang_jabatan,unit_auditi,user_responden&filter=nip.eq('+$('[name=nip]').val()+'),kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')',function(response){
				if(response.data.length > 0){
					var value=response.data[0];
					window.location.search.split('&')
					//old
					//window.location.href=window.location.search.split('&').splice(0, 2).toString().replaceAll(",","&")+"&nip="+value.nip;
					//new
					window.location.href=window.location.search.split('&')[0]+"&nip="+value.nip+"&"+window.location.search.split('&')[1];
					
					console.log(window.location.search.split('&').splice(0, 2).toString().replaceAll(",","&")+"&nip="+value.nip);
				}else{
					alert('Data Tidak ditemukan');
					if(window.location.search.split('&').length==3) window.location.href=window.location.search.split('&')[0]+"&"+window.location.search.split('&')[2];
					
				}
			},'onGetListError');
		}else{
			alert("NIP/Kode Sesi Tidak Boleh Kosong.")
		}
	});
}

function getBarOption(data1) {
	return {
	    //responsive: true,
		maintainAspectRatio: false,
   		barThickness: 1, 
		tooltips: { enabled: true },
		hover :{ animationDuration: 0 },
		legend:{ display: false, position: 'bottom' },
        lineAt: 60,
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero:true,
					fontFamily: "'verdana', sans-serif",
					fontSize:13,
					padding:27,
					display:false,
//                    maxRotation: 90,
//                    minRotation: 90
				},
				scaleLabel:{ display:false },
				gridLines: { }
			}],
			yAxes: [{
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:10,
                    beginAtZero:true,
                    min: 0,
                    max: 100
				},
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				console.log(ctx);
				ctx.textAlign = "right";
				ctx.font = "12px verdana";
				ctx.fillStyle = "#000";
				//ctx.shadowOffsetY = -5;
				ctx.textBaseline = "bottom";
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						//data = parseFloat((dataset.data[index] /* / (data1[index] + data2[index])*/).toFixed(2)*1) + '%';
						data = parseFloat((dataset.data[index])) + '';
						ctx.fillText(data, bar._model.x+8, bar._model.y-5);
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}

function getBarChart(data){
	console.log(data);
	//data.label.push(["My", "long", "long", "long", "label"]);
	//modif label
	var bgc=[
        'rgba(255, 99, 132, 0.8)',
        'rgba(54, 162, 235, 0.8)',
        'rgba(255, 206, 86, 0.8)',
        'rgba(75, 192, 192, 0.8)',
        'rgba(153, 102, 255, 0.8)',
        'rgba(255, 159, 64, 0.8)',
        '#a1fb74',
        '#fb74ea'
    ]
	var newlabel=[];
	var htm="";
	$.each(data.label, function(key){
		console.log(this);
		//console.log(this.split(" "));
		var split=this.split(" ");
		if(split.length>0){
			newlabel.push(split);
		}else{
			newlabel.push(this);
		}
		htm+='<p><i class="fas fa-square-full" style="color:'+bgc[key]+'"></i> '+this+'</p>';
	});
	$('#container-legend').append(htm);
	console.log(newlabel);
	var ctx = document.getElementById("bar");

	//init line
	Chart.pluginService.register({
	    afterDraw: function(chart) {
	        if (typeof chart.config.options.lineAt != 'undefined') {
	        	var lineAt = chart.config.options.lineAt;
	            var ctxPlugin = chart.chart.ctx;
	            var xAxe = chart.scales[chart.config.options.scales.xAxes[0].id];
	            var yAxe = chart.scales[chart.config.options.scales.yAxes[0].id];
	           	
	            // I'm not good at maths
	            // So I couldn't find a way to make it work ...
	            // ... without having the `min` property set to 0
	            if(yAxe.min != 0) return;
	            
	            ctxPlugin.strokeStyle = "red";
	        	ctxPlugin.beginPath();
	            lineAt = (lineAt - yAxe.min) * (100 / yAxe.max);
	            lineAt = (100 - lineAt) / 100 * (yAxe.height) + yAxe.top;
	            ctxPlugin.moveTo(xAxe.left, lineAt);
	            ctxPlugin.lineTo(xAxe.right, lineAt);
	            ctxPlugin.stroke();
	        }
	    }
	});
	//
	//ctx.height = 350;

//    backgroundColor: [
//        'rgba(255, 99, 132, 0.2)',
//        'rgba(54, 162, 235, 0.2)',
//        'rgba(255, 206, 86, 0.2)',
//        'rgba(75, 192, 192, 0.2)',
//        'rgba(153, 102, 255, 0.2)',
//        'rgba(255, 159, 64, 0.2)'
//    ],
	//for(var i=0; i < data.length)
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: data.label,//newlabel,
		    datasets: [{
		    		label: 'Kompetensi Teknis',
			        data: data.nilai,
			        //backgroundColor: "#3494BA", // biru
				    backgroundColor: bgc,
			        hoverBackgroundColor: [
				        'rgba(255, 99, 132, 0.5)',
				        'rgba(54, 162, 235, 0.5)',
				        'rgba(255, 206, 86, 0.5)',
				        'rgba(75, 192, 192, 0.5)',
				        'rgba(153, 102, 255, 0.5)',
				        'rgba(255, 159, 64, 0.5)'
				    ],
			        	//"#5da0ba" // biru lebih muda
		    }]
			/*
			labels: ["Teknis"],//data.label,
		    datasets: [
		    	{
		    		label: 'Kompetensi Teknis',
			        data: [Math.random() * 100],//data.nilai,
			        backgroundColor: "rgba(255, 99, 132, 0.2)", // biru
			        //hoverBackgroundColor: "#5da0ba" // biru lebih muda
		    	},
		    	{
		    		label: 'Kompetensi Teknis',
			        data: [Math.random() * 100],//data.nilai,
			        backgroundColor: "rgba(54, 162, 235, 0.2)", // biru
			        //hoverBackgroundColor: "#5da0ba" // biru lebih muda
		    	}
		    ]
			*/
		},
		options: getBarOption(data.nilai),
	});
}

var resetCanvas = function (data) {
	  
	  if(data.element_container=="container-bar-inti"){
		  $('#'+data.element+'').remove(); // this is my <canvas> element //width="325" height:500px;"
		  
		  $('#'+data.element_container+'').append('<canvas class="w-100s" style="position: relative;"  id="'+data.element+'"><canvas>');
		  //
		  canvas = document.querySelector('#'+data.element+''); // why use jQuery?
		  ctx = canvas.getContext('2d');
//		  ctx.canvas.width = $('#container-bar').width(); // resize to parent width
//		  ctx.canvas.height = $('#container-bar').height(); // resize to parent height

		  var x = canvas.width/2;
		  var y = canvas.height/2;
		  ctx.font = '10pt Verdana';
		  ctx.textAlign = 'center';
		  ctx.fillText('This text is centered on the canvas', x, y);
	  }
	  if(data.element_container=="container-bar-pilihan"){
		  $('#'+data.element+'').remove(); // this is my <canvas> element //width="325" height="325"height:600px;" 
		  
		  $('#'+data.element_container+'').append('<canvas class="w-100s" style="position: relative;" id="'+data.element+'"><canvas>');
		  //
		  canvas = document.querySelector('#'+data.element+''); // why use jQuery?
		  ctx = canvas.getContext('2d');
//		  ctx.canvas.width = $('#container-bar').width(); // resize to parent width
//		  ctx.canvas.height = $('#container-bar').height(); // resize to parent height

		  var x = canvas.width/2;
		  var y = canvas.height/2;
		  ctx.font = '10pt Verdana';
		  ctx.textAlign = 'center';
		  ctx.fillText('This text is centered on the canvas', x, y);
	  }
};

function getHorizontalBarChart(data){
	// scale second chart based on ratio of data to the first
    var fiddleFactor = 1.1; // determined by guesswork
    var ratio = data.obj_pilihan.label.length * fiddleFactor / data.obj_inti.label.length;
    var container1Height = parseInt(document.getElementById(data.obj_inti.element_container).style.height);
    // scale height of second chart
    document.getElementById(data.obj_pilihan.element_container).style.height = container1Height * ratio + 'px';
    
    if(data.obj_inti.element_container=="container-bar-inti"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_inti.nilai, function(key){
    		if(this < 60){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});
    	resetCanvas(data.obj_inti);

    	var data_inti = data.obj_inti;
    	if(data_inti.jumlah.length < 3 ){
    		$('#container-bar-inti').attr('style', 'height:300px;');
        }else if(data_inti.jumlah.length < 5 ){
        	$('#container-bar-inti').attr('style', 'height:400px;');
    	}else{
  		  	$('#container-bar-inti').attr('style', 'height:700px;');
    	}
		  
    	var ctx = document.getElementById(data.obj_inti.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: {
    	    labels: data.obj_inti.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_inti.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:16,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            fontColor: 'black',
				            fontStyle: 'normal',
				            maxTicksLimit: 5,
				            paddingLeft: 20,
				            //mirror: true,
				          },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata >= 0) {
    								ctx.fillText(newdata, bar._model.x-10, bar._model.y);
    							} else {
    								ctx.fillText(newdata, bar._model.x+50, bar._model.y);
    							}
    						}),this)
    					}),this);
    				}
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend1").html(myChart.generateLegend());
    }
    
    if(data.obj_pilihan.element_container=="container-bar-pilihan"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_pilihan.nilai, function(key){
    		if(this < 60){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});
    	resetCanvas(data.obj_pilihan);
    	var data_pilihan = data.obj_pilihan;
    	if(data_pilihan.jumlah.length < 3 ){
  		  $('#container-bar-pilihan').attr('style', 'height:200px;');
      	}else if(data_pilihan.jumlah.length < 5 ){
		  $('#container-bar-pilihan').attr('style', 'height:400px;');
    	}else{
  		  $('#container-bar-pilihan').attr('style', 'height:700px;');
    	}
    	
    	var ctx = document.getElementById(data.obj_pilihan.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: {
    	    labels: data.obj_pilihan.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_pilihan.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:16,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            fontColor: 'black',
				            fontStyle: 'normal',
				            maxTicksLimit: 5,
				            paddingLeft: 20,
				            //mirror: true,
				          },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
    								ctx.fillStyle = "#fff";
    								ctx.fillText(newdata, bar._model.x-10, bar._model.y);
    							} else {
    								if(newdata == 0) {
    									ctx.fillStyle = "#dc3545";
    									ctx.fillText(newdata, bar._model.x+45, bar._model.y);
    								}else{
    									ctx.fillStyle = "#fff";
    									ctx.fillText(newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend2").html(myChart.generateLegend());
    }
}

function getRadarChart(data) {

	var randomScalingFactor = function() {
		return Math.round(Math.random() * 10);
	};

	var color = Chart.helpers.color;
	var config = {
		type: 'radar',
		data: {
			labels: data.label,
			datasets: [{
				label: 'Nilai Pemetaan',
				backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
				borderColor: window.chartColors.green,
				pointBackgroundColor: window.chartColors.green,
				data: data.nilai
			}, {
				label: 'Nilai Standard',
				backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
				borderColor: window.chartColors.blue,
				pointBackgroundColor: window.chartColors.blue,
				data: data.nilai_standar
			}]
		},
		options: {
			legend: {
				position: 'bottom',
			},
			scale: {
				ticks: {
					beginAtZero: true
				}
			}
		}
	};

	var ctx = document.getElementById("radar");
	ctx.height = isMobile() ? 200 : 200;
	var myChart = new Chart(ctx, config);

	var colorNames = Object.keys(window.chartColors);
}

function addPercentage(canvas, ctx, data){
    var cx = canvas.width / 4;
    var cy = canvas.height / 4;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '12px verdana';
    ctx.fillStyle = 'black';
    ctx.fillText(parseInt((data[0]/(data[0]+data[1]))*100)+"%", cx, cy);
}

function displayRiwayat(){
	//join=kelompok_jabatan,jenjang_jabatan,unit_auditi,user_responden&
	//?filter=nip.eq('+selected_nip+'),kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')
	ajaxGET(path+'/account_riwayat_jabatan',function(response){
			//var value=response.data[0];
			//$('#nama_profile').text(value.nama);
			//$('#satker_profle').text(value.unit_auditi.nama);
			//$('#jabatan_profile').text(value.kelompok_jabatan.nama+" "+value.jenjang_jabatan.nama);
			var tbody = $("#tbl-riw-jabatan").find('tbody');
			var row = "";
		    var now=0;
			$.each(response.data,function(key,value){
		        row += '<tr class="data-row" id="row-'+value.id+'">';
			        row += '<td class="text-center">'+(num)+'</td>';
			        row += '<td class=""> '+value.nama_kelompok +' </td>';
			        row += '<td align="" class="text-center"> '+value.nilai_standar +' </td>';
			        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
		        row += '</tr>';
		        num++;
		    });
			tbody.html(row);
		},'onGetListError');
}

function displayProfile(){
    
    var tbody = $("#tbl-teknis").find('tbody');
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
    
    ajaxGET(path + '/detil_individu'+'?order=jenis_kompetensi&filter=jenis_kompetensi.in(\''+'TEKNIS_INTI'+'\',\''+'TEKNIS_PILIHAN'+'\'),id_user_responden.eq(\''+selected_responden+'\')','onGetListTeknisDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
    console.log('response: ', response.data);
    var tbody="";
    try {
    	if(response.data[0].jenis_kompetensi=="MANAJERIAL"){
    		tbody = $("#tbl-manajerial").find('tbody');
    	}else if(response.data[0].jenis_kompetensi=="SOSIOKULTURAL"){
    		tbody = $("#tbl-sosiokultural").find('tbody');
    	}
	} catch (e) {
		alert("Data Tidak Ditemukan.");
	}
    
    var row = "";
    var num = 1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[];
    $.each(response.data,function(key,value){
        row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(num)+'</td>';
	        row += '<td class=""> '+value.nama_kelompok +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_standar +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
        row += '</tr>';
        num++;
        arrnama.push(value.nama_kelompok);
        arrstandar.push(value.nilai_standar);
        arrnilai.push(value.nilai);
    });
    myobj={
    		label : arrnama,
    		nilai_standar : arrstandar,
    		nilai : arrnilai
    };
    tbody.html(row);
    console.log(myobj);
    if(response.data[0].jenis_kompetensi=="MANAJERIAL"){
    	getRadarChart(myobj);
    }
}

function displaySosiokultural(){
	var tbody = $("#tbl-sosiokultural").find('tbody');
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
    
    ajaxGET(path + '/detil_individu'+'?filter=jenis_kompetensi.eq(\''+'SOSIOKULTURAL'+'\'),id_user_responden.eq(\''+selected_responden+'\')','onGetListDetilSuccess','onGetListDetilError');
}

function displayManajerial(){
    
    var tbody = $("#tbl-manajerial").find('tbody');
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
    
    ajaxGET(path + '/detil_individu'+'?filter=jenis_kompetensi.eq(\''+'MANAJERIAL'+'\'),id_user_responden.eq(\''+selected_responden+'\')','onGetListDetilSuccess','onGetListDetilError');
}

function displayTeknis(){
    
    var tbody = $("#tbl-teknis").find('tbody');
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
    
    ajaxGET(path + '/detil_individu'+'?order=nama_kelompok&filter=jenis_kompetensi.in(\''+'TEKNIS_INTI'+'\',\''+'TEKNIS_PILIHAN'+'\'),id_user_responden.eq(\''+selected_responden+'\')','onGetListTeknisDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
    console.log('response: ', response.data);
    var tbody="";  
    var row = "";
    var num = 1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[];
    $.each(response.data,function(key,value){
    	value.nilai=Math.floor(value.nilai);
        row += '<tr class="data-row" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(num)+'</td>';
	        row += '<td class=""> '+value.nama_kelompok +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai_standar +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
        row += '</tr>';
        num++;
        arrnama.push(value.nama_kelompok);
        arrstandar.push(value.nilai_standar);
        arrnilai.push(value.nilai);
    });
    myobj={
    		label : arrnama,
    		nilai_standar : arrstandar,
    		nilai : arrnilai
    };
    try {
    	if(response.data[0].jenis_kompetensi=="MANAJERIAL"){
    		tbody = $("#tbl-manajerial").find('tbody');
        	getRadarChart(myobj);
    	}else if(response.data[0].jenis_kompetensi=="SOSIOKULTURAL"){
    		tbody = $("#tbl-sosiokultural").find('tbody');
    	}
        tbody.html(row);
	} catch (e) {
		//alert("Data Tidak Ditemukan.");
		$("#tbl-manajerial").find('tbody').html('<tr><td colspan="20"><div class="list-group-item text-center">Tidak Ada Data.</div></td></tr>');
		$("#tbl-sosiokultural").find('tbody').html('<tr><td colspan="20"><div class="list-group-item text-center">Tidak Ada Data.</div></td></tr>');
	}
    console.log(myobj);
}

function onGetListTeknisDetilSuccess(response){
    console.log('response: ', response.data);
    var tbody = $("#tbl-teknis-inti").find('tbody');
    var tbody2 = $("#tbl-teknis-pilihan").find('tbody');
    var row = "";
    var numi= 1, nump=1;
    var myobj={};
    var arrnama=[],arrstandar=[],arrnilai=[],arrjumlah=[];
    var count_inti=0, count_pilihan=0;
    var sum_inti=0, sum_pilihan=0;
    var idx_inti=0, idx_pilihan=0;

    $.each(response.data,function(key,value){
        if(value.jenis_kompetensi=="TEKNIS_INTI") count_inti++;
    });

    $.each(response.data,function(key,value){
        if(value.jenis_kompetensi=="TEKNIS_INTI"){
        	if(idx_inti==0){
        	    row += '<tr class="bg-secondary text-white">';
        	    	row += '<td rowspan="'+(count_inti+1)+'" class="text-left">Inti</td>';
        	    	row += '<td class="text-center">No.</td>';
        	    	row += '<td class="text-center">Kode</td>';
        	    	row += '<td class="text-center">Nama Kompetensi</td>';
        	    	row += '<td class="text-center">Skor</td>';
        	    row += '</tr>';
        	}
            row += '<tr class="data-row '+(value.nilai < 60?'bg-danger text-white':'')+'" id="row-'+value.id+'">';
	        row += '<td class="text-center">'+(numi)+'</td>';
	        row += '<td align="" class="text-center"> '+value.kode_unit_kompetensi +' </td>';
	        row += '<td class=""> '+value.nama_unit_kompetensi +' </td>';
	        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
	        idx_inti++;
	        row += '</tr>';
		    numi++;
		    sum_inti+=value.nilai;

		    arrnama.push(value.kode_unit_kompetensi);
		    arrnilai.push(value.nilai);
		    arrjumlah.push(value.jumlah_unit_komp);
        };
    });
    row += '<tr class="bg-secondary text-white">';
		row += '<td class="text-center"></td>';
		row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<th class="text-center">Rata-Rata</th>';
    	row += '<th class="text-center">'+(sum_inti/count_inti).toFixed(2)+'</th>';
    row += '</tr>';
    //set nilai rata2 inti
    //arrnama.push("Inti");
    //arrnilai.push((sum_inti/count_inti).toFixed(2));
    var obj_inti={
    		element : 'bar-inti',
    		element_container : 'container-bar-inti',
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };
    console.log(obj_inti);

    tbody.html(row);
    arrnama=[],arrstandar=[],arrnilai=[],arrjumlah=[];
    row="";
    tbody = tbody2;
    ////////////////////////
    //
    var unique_kelompok_teknis=[];
    var unique_kelompok_teknis_with_string=[];
	$.each(response.data,function(key){
        if(this.jenis_kompetensi=="TEKNIS_PILIHAN"){
        	var valueunique = this.nama_kelompok;
			if(unique_kelompok_teknis.indexOf(valueunique) == -1) unique_kelompok_teknis.push(valueunique);
			if(unique_kelompok_teknis_with_string.indexOf(valueunique) == -1) unique_kelompok_teknis_with_string.push(valueunique);
			else unique_kelompok_teknis_with_string.push("");
        }
	});
    //
	row += '<tr class="bg-secondary text-white">';
	   	row += '<td class="text-left">Pilihan</td>';
	   	row += '<td class="text-center">No.</td>';
	   	row += '<td class="text-center">Kode</td>';
	   	row += '<td class="text-center">Nama Kompetensi</td>';
	   	row += '<td class="text-center">Skor</td>';
   	row += '</tr>';
    $.each(unique_kelompok_teknis,function(keyunique, valueuniqe){
	    //arrnama.push(valueuniqe);
    	$.each(response.data,function(key,value){
            if(value.jenis_kompetensi=="TEKNIS_PILIHAN"){
            	//start here
            	if(valueuniqe==value.nama_kelompok){
	    	        if(nump==1 && idx_pilihan>0){
		    		    //set nilai rata2 pilihan
		    		    //arrnilai.push((sum_pilihan/count_pilihan).toFixed(2));
	    	        	row += '<tr class="bg-secondary text-white">';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<td class="text-center"></td>';
	            	    	row += '<th class="text-center">Rata-Rata</th>';
	            	    	row += '<th class="text-center">'+(sum_pilihan/count_pilihan).toFixed(2)+'</th>';
	            	    row += '</tr>';
	            	    row += '<tr>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                	row += '<td class="text-center"></td>';
		                row += '</tr>';
		    	        sum_pilihan=0;
		    		    count_pilihan=0;
	    	        }
	                row += '<tr class="data-row '+(value.nilai < 60?'bg-danger text-white':'')+'" id="row-'+value.id+'">';
	    	        //
	                row += '<th class="text-left">'+unique_kelompok_teknis_with_string[idx_pilihan]+'</th>';
	    	        //
	    	        row += '<td class="text-center">'+(nump)+'</td>';
	    	        row += '<td align="" class="text-center"> '+value.kode_unit_kompetensi +' </td>';
	    	        row += '<td class=""> '+value.nama_unit_kompetensi +' </td>';
	    	        row += '<td align="" class="text-center"> '+value.nilai +' </td>';
	    	        
	    	        row += '</tr>';
	    	        nump++;
	    	        idx_pilihan++;
	    		    sum_pilihan+=value.nilai;
	    		    count_pilihan++;


	    		    arrnama.push(value.kode_unit_kompetensi);
	    		    arrnilai.push(value.nilai);
	    		    arrjumlah.push(value.jumlah_unit_komp);
            	}else{
	    	        nump=1;
            	}
    		    //end here
            }
        });
    });

	row += '<tr class="bg-secondary text-white">';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<td class="text-center"></td>';
    	row += '<th class="text-center">Rata-Rata</th>';
    	row += '<th class="text-center">'+(sum_pilihan/count_pilihan).toFixed(2)+'</th>';
    row += '</tr>';
    tbody.html(row);
    
    var obj_pilihan={
    		element : 'bar-pilihan',
    		element_container : 'container-bar-pilihan',
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };
    console.log(obj_pilihan);
    var newobj = {obj_inti, obj_pilihan};
    console.log(newobj);
    getHorizontalBarChart(newobj);
}

function generateChart(data) {
  try {
    var ctx = document.getElementById("monthlyChart");
    if (ctx) {
      ctx.height = 100;
      chartBar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          legend: { position: 'top', labels: { fontFamily: 'Poppins' } },
          scales: { xAxes: [{ ticks: { fontFamily: "Poppins" } }], yAxes: [{ ticks: { beginAtZero: true, fontFamily: "Poppins" } }] }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
};