var path = ctx + '/restql';
var path_inventori = ctx + '/inventori';

var chartBar = null;
var myChartKompTeknis = null;
var myChartFungsiUtama = null;
var myChartStandarKomp = null;
var myChartPenguasaanKomp = null;
// Register plugin to always show tooltip
// ref: https://github.com/chartjs/Chart.js/issues/4045
Chart.plugins.register({
	beforeRender: function(chart) {
		if (chart.config.options.showAllTooltips) {
			// create an array of tooltips
			// we can't use the chart tooltip because there is only one tooltip per chart
			chart.pluginTooltips = [];
			chart.config.data.datasets.forEach(function(dataset, i) {
				chart.getDatasetMeta(i).data.forEach(function(sector, j) {
					chart.pluginTooltips.push(
						new Chart.Tooltip(
							{
								_chart: chart.chart,
								_chartInstance: chart,
								_data: chart.data,
								_options: chart.options.tooltips,
								_active: [sector]
							},
							chart
						)
					);
				});
			});

			// turn off normal tooltips
			chart.options.tooltips.enabled = false;
		}
	},
	afterDraw: function(chart, easing) {
		if (chart.config.options.showAllTooltips) {
			// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
			if (!chart.allTooltipsOnce) {
				if (easing !== 1) return;
				chart.allTooltipsOnce = true;
			}

			// turn on tooltips
			chart.options.tooltips.enabled = true;
			Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
				tooltip.initialize();
				tooltip._options.bodyFontFamily = "'Lato', sans-serif"
				tooltip._options.displayColors = false;
				tooltip._options.bodyFontSize = tooltip._chart.height * 0.06;
				tooltip._options.yPadding = 0;
				tooltip._options.xPadding = 0;
				// tooltip._options.position = 'average';
				// tooltip._options.caretSize = tooltip._options.bodyFontSize * 0.5;
				//tooltip._options.cornerRadius = tooltip._options.bodyFontSize * 0.5;
				tooltip.update();
				// we don't actually need this since we are not animating tooltips
				tooltip.pivot();
				tooltip.transition(easing).draw();
			});
			chart.options.tooltips.enabled = false;
		}
	}
});


window.isMobile = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

var public_filter = 'kode_sesi.eq(\''+$.urlParam('kode-sesi')+'\')';
var selected_id_kelompok = ($.urlParam('id_kelompok')!=null?$.urlParam('id_kelompok'):"");
var selected_mode = ($.urlParam('mode')!=null?$.urlParam('mode'):"");
function init(){
	if($.urlParam('kode-sesi')==null){
		showListSesi();
	}
	if($.urlParam('print')==1){
		printPDF();
	}
	ajaxGET(path + '/eselon1'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
		$.each(response.data, function(key,value){ $('[name=filter_id_eselon1]').append(new Option(value.nama, value.id)); });
	});

	ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter=kode_simpeg.not(null)",function(response){
		$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
	});

	ajaxGET(path + '/tingkat_jabatan',function(response){
		$.each(response.data, function(key,value){ $('[id=filter_tingkat_jabatan], [id=filter_chart_tingkat_jabatan]').append(new Option(value.nama, value.id)); });
	});
	
	ajaxGET(path + '/jenjang_jabatan',function(response){
		$.each(response.data, function(key,value){ $('[id=filter_jenjang_jabatan], [id=filter_chart_jenjang_jabatan]').append(new Option(value.nama, value.id)); });
	});
	display(1);
	$('#filter_tingkat_jabatan, #filter_jenjang_jabatan, #filter_bidang_teknis').on('change', function (e) {
		displayTeknis();
		displayManajerial();
		displaySosiokultural();
	});
	
	if($('[name=filter_id_organisasi]').val()!=undefined){
		ajaxGET(path + '/mapping_organisasi'+"?limit=1000000",function(response){
			$.each(response.data, function(key,value){ 
				if(response.data.length==1) $('[name=filter_id_organisasi]').html(new Option(value.nama, value.id));
				else $('[name=filter_id_organisasi]').append(new Option(value.nama, value.id)); 
			});
			if(response.data.length==1) $('[name=filter_id_organisasi]').trigger('change');
			//if(response.data.length==1) $('[name=filter_id_organisasi]')[0].remove();
		});
	}else{
		ajaxGET(path + '/propinsi'+"?limit=1000000&",function(response){
			$.each(response.data, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.nama, value.id)); });
		});
	}
	$('[name=filter_id_organisasi]').change(function(){
		var params = (this.value!=""?"&filter=id_header.eq(\""+this.value+"\")":"");
		params+="&join=propinsi";
		ajaxGET(path + '/mapping_organisasi_detil'+"?limit=1000000"+params,function(response){
			$('[name=filter_id_propinsi]').empty();
			$('[name=filter_id_propinsi]').append('<option value="">Propinsi</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_propinsi]').append(new Option(value.propinsi.nama, value.propinsi.id)); });
		});
	});
	
	//new
	$('[name=filter_id_propinsi]').change(function(){
		var params="kode_simpeg.not(null),";
		params="";
		if($('[name=filter_id_propinsi]').val()!="") params+="id_propinsi.eq(\""+$('[name=filter_id_propinsi]').val()+"\")";
		ajaxGET(path + '/unit_auditi'+"?limit=1000000&filter="+params,function(response){
			$('[name=filter_id_unit_auditi]').empty();
			$('[name=filter_id_unit_auditi]').append('<option value="">Satker</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
		});
	});
	
	$('[name=filter_id_eselon1]').change(function(){
		var params="";
		if(this.value!="") params="filter=id_eselon1.eq(\""+this.value+"\")";
		ajaxGET(path + '/unit_auditi'+"?limit=1000000&"+params,function(response){
			$('[name=filter_id_unit_auditi]').empty();
			$('[name=filter_id_unit_auditi]').append('<option value="">Satker</option>');
			$.each(response.data, function(key,value){ $('[name=filter_id_unit_auditi]').append(new Option(value.nama, value.id)); });
		});
	});
	
	$('#btn-reset').on('click', function (e) {
		$('#form-search').trigger('reset');
		$('[name=filter_id_unit_auditi]').val('').trigger('change');
		$('[name=filter_id_eselon1]').val('').trigger('change');
		
		$('.overview-item.bg-info').removeClass('bg-info');
		$('table').find('tbody').html("");
		
		//if(myChartStandarKomp!=null){
			//chartBar.update();
			myChartKompTeknis.clear();
			myChartFungsiUtama.clear();
			myChartStandarKomp.clear();
			myChartPenguasaanKomp.clear();
		//}
		$('[name=filter_id_bidang_teknis]').html("<option>Pilih Bidang</option>");
		selected_id_kelompok="";
		display(1);
	});
}

function setKelompokJabatan(e){
	$('#list-kelompok').on('click', '.overview-item', function() {
		 $('.overview-item.bg-info').removeClass('bg-info');
		 $(this).addClass('bg-info');
	});
	//$(e).addClass("bg-info");
	selected_id_kelompok=e.dataset.id;
	
	getSelectBidangTeknis(selected_id_kelompok);
	//display();
}

function getSelectBidangTeknis(id){
	$('[name=filter_id_bidang_teknis]').empty();
	$('[name=filter_id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	//
	ajaxGET(path + '/bidang_teknis?filter=id_kelompok_jabatan.eq("'+id+'")','onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		//if(this.id!=9) 
		$('[name=filter_id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	//$('[name=filter_id_bidang_teknis]').append(new Option("ERROR", 7));
	if($('[name=filter_id_bidang_teknis]').find('option').length == 2){
		$('[name=filter_id_bidang_teknis]').find('option')[0].remove();
		$('[name=filter_id_bidang_teknis]').hide();
	}else{
		
		$('[name=filter_id_bidang_teknis]').show();
	}
	display();
}

function printPDF(){
	$('aside').attr("style", "display:none!important");
	$('.page-container').attr('style', 'padding-left:0px');
	$('header').attr("style", "display:none!important");
	$('.main-content').attr('style', 'padding:0px');
}

function display(mode){
	//displayDemografi();
	displayProgress();
	if(mode==undefined || selected_id_kelompok!=""){
		//display teknis and fungsi utama
		displayTeknis();
		displayManajerial();
		displaySosiokultural();
	}
}

function hexToRgb(hex) {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}

function getBarOption(data1) {
	return {
		maintainAspectRatio: false,
   		barThickness: 1, 
		tooltips: { 
			enabled: true,
			callbacks: {
                label: function(tooltipItem, data) {
                	console.log(tooltipItem);
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += Math.ceil(parseInt(tooltipItem.yLabel * total_gender_usia) / 100);
                    //
                    //ctx.fillText(Math.ceil(parseInt((data*objnew.total))/100), bar._model.x+35, bar._model.y);
					
                    //
                    //label += tooltipItem.xLabel+"%";
                    return label;
                }
            }
		},
		hover :{ animationDuration: 0 },
		legend:{ display: false, position: 'bottom' },
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero:true,
					fontFamily: "'verdana', sans-serif",
					fontSize:10,
//                    userCallback: function(value) {
//                    	console.log(value+"="+value.length);
//                        if(value.length>10){
//                        	value.split(" ");
//                        	value+=value[0]+"<br/>"+value[1];
//                        }
//                    	return value;
//                    },

		              //autoSkip: false,
		              padding: 12
				},
				scaleLabel:{ display:false },
				gridLines: { }
			}],
			yAxes: [{
				ticks: {
					fontFamily: "'verdana', sans-serif",
					fontSize:12,
                    beginAtZero:true,
                    min: 0,
                    max: 100,
                    userCallback: function(value) {
                        return value+'%';
                    },
				},
			}]
		},
		animation: {
			onComplete: function () {
				var chartInstance = this.chart;
				var ctx = chartInstance.ctx;
				console.log(ctx);
				ctx.textAlign = "right";
				ctx.font = "12px verdana";
				ctx.fillStyle = "#000";
				//ctx.shadowOffsetY = -5;
				ctx.textBaseline = "bottom";
				Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					Chart.helpers.each(meta.data.forEach(function (bar, index) {
						//data = parseFloat((dataset.data[index] /* / (data1[index] + data2[index])*/).toFixed(2)*1) + '%';
						data = parseFloat((dataset.data[index])) + '%';
						ctx.fillText(data, bar._model.x+20, bar._model.y-5);
					}),this)
				}),this);
			}
		},
		pointLabelFontFamily : "Quadon Extra Bold",
		scaleFontFamily : "Quadon Extra Bold",
	};
}

function randomScalingFactor() {
	return Math.round(Math.random() * 10);
};


function getHorizontalBarChart(data){
	console.log("dagta:", data);
	// scale second chart based on ratio of data to the first
    var fiddleFactor = 1.1; // determined by guesswork
    var ratio = data.obj_pilihan.label.length * fiddleFactor / data.obj_inti.label.length;
    var container1Height = parseInt(document.getElementById(data.obj_inti.element_container).style.width);
    
	// scale height of second chart
    //document.getElementById(data.obj_pilihan.element_container).style.width = container1Height * ratio + 'px';
    
    if(data.obj_inti.element_container=="container-bar-inti"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_inti.nilai, function(key){
    		if(this <= 50){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});

    	resetCanvas(data.obj_inti);

    	var data_inti = data.obj_inti;
    	if(data_inti.jumlah.length < 3 ){
    		$('#container-bar-inti').attr('style', 'width:300px;');
        }else if(data_inti.jumlah.length < 5 ){
        	$('#container-bar-inti').attr('style', 'width:400px;');
    	}else{
  		  	$('#container-bar-inti').attr('style', 'width:'+(50*(data_inti.jumlah.length))+'px;');
    	}
		
		var other=[];
		$.each(data.obj_inti.label, function(k, label){
			var month = label.split(";")[0];
	     	var year = label.split(";")[1];
	      	var index = label.split(";")[2];
	      	if(index === "0"){
	        	//return year;
				other.push(year);
	      	}else{
				//other.push("");
	        	//return "";
	      	}	
		})	 
	
    	var ctx = document.getElementById(data.obj_inti.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'bar',
    		data: {
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_inti.nilai,
      			xAxisID:'xAxis1',
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},

    			scales: {
    				xAxes: [
					        {
					          id:'xAxis1',
					          type:"category",
					          ticks:{
					            /*callback:function(label){
					              var month = label.split(";")[0];
					              var year = label.split(";")[1];
					              return month;
					            },*/
	    						beginAtZero:true,
	    						fontFamily: "'verdana', sans-serif",
	    						fontSize:12,
	    	                    min: 0,
	    	                    max: 100,
						        autoSkip: false,
						        maxRotation: 90,
						        minRotation: 90,
					          },
    	    				  labels: data.obj_inti.label,
					        },
					        {
					          id:'xAxis2',
					          type:"category",
					          gridLines: {
					            drawOnChartArea: false, // only want the grid lines for one axis to show up
								//drawTicks: false,
					          },
					          ticks:{
					            padding:0,
					            maxRotation:0,
					            autoSkip: false,
					            /*callback:function(label){
					              var month = label.split(";")[0];
					              var year = label.split(";")[1];
					              var index = label.split(";")[2];
					              if(index === "0"){
					                return year;
					              }else{
					                return "";
					              }
					            }*/
					          },
							  labels: other
					        }
						/*{
	    					ticks: {
	    						beginAtZero:true,
	    						fontFamily: "'verdana', sans-serif",
	    						fontSize:12,
	    	                    min: 0,
	    	                    max: 100,
						        autoSkip: false,
						        maxRotation: 90,
						        minRotation: 90,
	    					},
	    					scaleLabel:{ display:true },
	    					gridLines: { }, 
	    					stacked: true
	    				}*/
					],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            //fontStyle: 'bold',
				            //maxTicksLimit: 5,
    						fontSize:11,
    	                    min: 0,
    	                    max: 100
				            //mirror: true,
				          },
    					//scaleLabel:{ display:true },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
						//not need if hasbeen modfied from scale x or y
    					/*ctx.textAlign = "right";
    					ctx.font = "12px verdana";
    					ctx.fillStyle = "#fff";*/
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
									//ctx.fillStyle = "#000";//fff
									//+ '|'+data.obj_inti.jumlah[index]
    								if(newdata==100){
									ctx.fillText(newdata, bar._model.x-10, bar._model.y+10);
									}else{
										ctx.fillText(newdata, bar._model.x-10, bar._model.y-4.6);
    								}
								} else {
    								if(newdata == 0) {
    									//ctx.fillStyle = "#dc3545";
										//+ '|'+data.obj_inti.jumlah[index]
    									ctx.fillText(newdata, bar._model.x+45, bar._model.y);
    								}else{
    									//ctx.fillStyle = "#000";//fff
										//+ '|'+data.obj_inti.jumlah[index]
    									ctx.fillText(newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    			},
				
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend1").html(myChart.generateLegend());
    }
    
    /*
if(data.obj_pilihan.element_container=="container-bar-pilihan"){
    	var rulecolor =[];
    	var rulehovercolor =[];
    	$.each(data.obj_pilihan.nilai, function(key){
    		if(this < 60){
    			rulecolor.push("#dc3545");
    			rulehovercolor.push("#dc3545c7");
    		}else{
    			rulecolor.push("#4A9B82");
    			rulehovercolor.push("#70c2a9");
    		}
    	});
    	resetCanvas(data.obj_pilihan);
    	var data_pilihan = data.obj_pilihan;
    	if(data_pilihan.jumlah.length < 3 ){
  		  $('#container-bar-pilihan').attr('style', 'height:200px;');
      	}else if(data_pilihan.jumlah.length < 5 ){
		  $('#container-bar-pilihan').attr('style', 'height:400px;');
    	}else{
    		$('#container-bar-pilihan').attr('style', 'height:'+(50*(data_pilihan.jumlah.length))+'px;');
    	}
    	
    	var ctx = document.getElementById(data.obj_pilihan.element);
    	//ctx.height = 250;
    	var myChart = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: {
    	    labels: data.obj_pilihan.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.obj_pilihan.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:16,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
//    					gridLines: {
//    						display:false,
//    						color: "#fff",
//    						zeroLineColor: "#fff",
//    						zeroLineWidth: 0
//    					},
//    					ticks: {
//    						fontFamily: "'verdana', sans-serif",
//    						fontSize:16
//    					},
//    					stacked: true
				          tabIndex: 0,
//				          maxBarThickness: 100,
//				          categoryPercentage: 1.0,
//				          barPercentage: 1.0,
//				          barThickness: 20,
				          barPercentage: 0.9,
				          barThickness: 100,
				          maxBarThickness: 40,
				          minBarLength: 8,
				          gridLines: {
				            display: false,
				            drawBorder: false,
				          },
				          ticks: {
				            fontColor: 'black',
				            fontStyle: 'normal',
				            maxTicksLimit: 5,
				            paddingLeft: 20,
				            //mirror: true,
				          },
    				}]
    			},
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
    								ctx.fillStyle = "#fff";
    								ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
    							} else {
    								if(newdata == 0) {
    									ctx.fillStyle = "#dc3545";
    									ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
    								}else{
    									ctx.fillStyle = "#fff";
    									ctx.fillText(data.obj_pilihan.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	$("#container-legend2").html(myChart.generateLegend());
    }
	*/
}

function getBarChart(data){
	var rulecolor =[];
	var rulehovercolor =[];
	$.each(data.nilai, function(key){
		if(this < 60){
			rulecolor.push("#dc3545");
			rulehovercolor.push("#dc3545c7");
		}else{
			rulecolor.push("#4A9B82");
			rulehovercolor.push("#70c2a9");
		}
	});
	resetCanvas();
	var ctx = document.getElementById("bar");
	//ctx.height = 250;
	var myChart = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
	    labels: data.label,
	    datasets: [{
    		label: 'Skor',
	        data: data.nilai,
	        backgroundColor: rulecolor,//"#4A9B82", 
	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
	    }]
		},
		options: {
			maintainAspectRatio: false,
			responsive: true,
			tooltips: { enabled: false },
			hover :{ animationDuration: 0 },
			legend:{ display: false, position: 'bottom' },
			legendCallback: function(chart) {
				var labels = ["Tercapai", "Tidak Tercapai"];
				var background = ["#4A9B82", "#dc3545"];
			    var text = []; 
			    text.push('<ul class="' + chart.id + '-legend nav">'); 
			    for (var i = 0; i < labels.length; i++) { 
			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
			        if (labels[i]) { 
			            text.push(labels[i]); 
			        } 
			        text.push('</li>'); 
			    } 
			    text.push('</ul>'); 
			    return text.join(''); 
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontFamily: "'verdana', sans-serif",
						fontSize:16,
	                    min: 0,
	                    max: 100
					},
					scaleLabel:{ display:false },
					gridLines: { }, 
					stacked: true
				}],
				yAxes: [{
					gridLines: {
						display:false,
						color: "#fff",
						zeroLineColor: "#fff",
						zeroLineWidth: 0
					},
					ticks: {
						fontFamily: "'verdana', sans-serif",
						fontSize:16
					},
					stacked: true
				}]
			},
			animation: {
				onComplete: function () {
					var chartInstance = this.chart;
					var ctx = chartInstance.ctx;
					ctx.textAlign = "right";
					ctx.font = "14px verdana";
					ctx.fillStyle = "#fff";
					
					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						Chart.helpers.each(meta.data.forEach(function (bar, index) {
							newdata = (dataset.data[index]);
							//console.log(data);
							if(newdata > 0) {
								ctx.fillStyle = "#fff";
								ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
							} else {
								if(newdata == 0) {
									ctx.fillStyle = "#dc3545";
									ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
								}else{
									ctx.fillStyle = "#fff";
									ctx.fillText(data.jumlah[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
								}
							}
							
						}),this)
					}),this);
				}
			},
			pointLabelFontFamily : "Quadon Extra Bold",
			scaleFontFamily : "Quadon Extra Bold",
		}
	});
	$("#your-legend-container").html(myChart.generateLegend());
}


var resetCanvas = function (data) {
	  
	  if(data.element_container=="container-bar-inti"){
		  $('#'+data.element+'').remove(); // this is my <canvas> element //width="325" height:500px;"
		  
		  $('#'+data.element_container+'').append('<canvas height="400" width="400" id="'+data.element+'"><canvas>');
			//'<canvas class="w-100s" style="position: relative;overflow-x: scroll"  id="'+data.element+'"><canvas>');
		  // 
		  canvas = document.querySelector('#'+data.element+''); // why use jQuery?
		  ctx = canvas.getContext('2d');
//		  ctx.canvas.width = $('#container-bar').width(); // resize to parent width
//		  ctx.canvas.height = $('#container-bar').height(); // resize to parent height

		  var x = canvas.width/2;
		  var y = canvas.height/2;
		  ctx.font = '10pt Verdana';
		  ctx.textAlign = 'center';
		  ctx.fillText('This text is centered on the canvas', x, y);
		  
	  }
};

function getRadarChart(data, ctx) {

	var randomScalingFactor = function() {
		return Math.round(Math.random() * 10);
	};

	var color = Chart.helpers.color;
	var config = {
		type: 'radar',
		data: {
			labels: data.label,
			datasets: [{
				label: 'Nilai Pemetaan',
				backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
				borderColor: window.chartColors.green,
				pointBackgroundColor: window.chartColors.green,
	            borderWidth: 1,
				data: data.nilai
			}, {
				label: 'Nilai Standard',
				backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
				borderColor: window.chartColors.blue,
				pointBackgroundColor: window.chartColors.blue,
	            borderWidth: 1,
				data: data.nilai_standar
			}]
		},
		options: {
			maintainAspectRatio: true,
			responsive: true,
			legend: {
				position: 'bottom',
			},
			scale: {
				ticks: {
					beginAtZero: true
				},
			        pointLabels: {
			            
           //fontSize: 0  
			        }
			}
		}
	};
	
	ctx.height = isMobile() ? 230 : 230;
	//var myChart = new Chart(ctx, config);

	//var colorNames = Object.keys(window.chartColors);
//	ctx.height = isMobile() ? 200 : 200;
//
//	var colorNames = Object.keys(window.chartColors);

	if(myChart == null) {
		var myChart = new Chart(ctx, config);
	}else { 
		myChart.data.datasets[0].data = data.nilai;
		myChart.data.datasets[1].data = data.nilai_standar;
		myChart.update(); 
    }
}

var chartSosiokultural;
var color = Chart.helpers.color;
function getChartSosiokultural(data, ctx){
	var ctx = document.getElementById('chart-sosiokultural');
	if(chartSosiokultural==null){
	chartSosiokultural = new Chart(ctx, {
	//var config = {
	    type: 'bar',
	    data: {
			labels: data.label,//["PEMULA","PELAKSANA","PELAKSANA LANJUTAN","TERAMPIL","MAHIR","PENYELIA","PERTAMA","MUDA","MADYA", "UTAMA"],
	        datasets: [{
	            label: "Nilai Standar",
	            type: "line",
	            backgroundColor: color(window.chartColors.blue).alpha(0.3).rgbString(),
	            borderColor: window.chartColors.blue,
				pointBackgroundColor: window.chartColors.blue,
	            borderWidth: 1,
	            fill: false,
	            yAxisID: "axis-time",
	            data: data.nilai_standar,//[3, 2, 4, 3, 3, 2, 1, 2, 4, 1]
	          },{
	            label: "Nilai Pemetaan",
	            type: "bar",
	            backgroundColor: color(window.chartColors.green).alpha(0.3).rgbString(),
	            borderColor: window.chartColors.green,
				pointBackgroundColor: window.chartColors.green,
	            borderWidth: 1,
	            fill: true,
	            yAxisID: "axis-bar",
	            data: data.nilai,//[3.5, 2.4, 4.2, 3.6, 3.4, 2.1, 1.9]
	          }]
	    },
	    options: {
	        tooltips: {
	            displayColors: true,
	          },
	          scales: {
	            xAxes: [{
	              stacked: true,
					ticks: {
						beginAtZero:true,
						fontFamily: "'verdana', sans-serif",
						fontSize:11,
				        autoSkip: false,
				        maxRotation: 90,
				        minRotation: 90,
	                    //padding: 17
					},
	            }],
	            yAxes: [
					{
		              stacked: true,
		              display: false,
		              id: 'axis-bar',
						ticks: {
							beginAtZero:true,
		                    min: 0,
		                    max: 5
						},
		            },
					{
		              stacked: true,
		              id: 'axis-time',
						ticks: {
							beginAtZero:true,
							fontFamily: "'verdana', sans-serif",
							fontSize:11,
					        autoSkip: false,
		                    min: 0,
		                    max: 5
						},
		            }
				]
	          },
	      		responsive: true,
	      		//maintainAspectRatio: false,
	      		//legend: { display: false },
	    }
	});
	}else {
		chartSosiokultural.data.labels = data.label;
		chartSosiokultural.data.datasets[0].data = data.nilai_standar;
		chartSosiokultural.data.datasets[1].data = data.nilai;
		chartSosiokultural.update(); 
    }
}

function addPercentage(canvas, ctx, data){
    var cx = canvas.width / 4;
    var cy = canvas.height / 4;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '12px verdana';
    ctx.fillStyle = 'black';
    ctx.fillText(parseInt((data[0]/(data[0]+data[1]))*100)+"%", cx, cy);
}

var arr_total_gender_usia=[];
var total_gender_usia=0;

function displayProgress(page=1, limit=10, params='') {
  limit=100000;
  //
  var select_id_eselon1=$('[name=filter_id_eselon1]').val();
  var select_id_unit_auditi=$('[name=filter_id_unit_auditi]').val();
  
  var clause=[];
  if($('[name=filter_id_unit_auditi]').val()!=""){
	  clause.push('id_unit_auditi.eq('+$('[name=filter_id_unit_auditi]').val()+')');
  }
  if($('[name=filter_id_eselon1]').val()!=""){
	  clause.push('id_eselon1.eq('+$('[name=filter_id_eselon1]').val()+')');
  }

	var newclause=[];
	var newparam="";
	if($('[name=filter_id_propinsi]').val()!=""){
		newclause.push('id_propinsi_unit_auditi.eq('+$('[name=filter_id_propinsi]').val()+')');
	}
	if($('[name=filter_id_organisasi]').val()!="" && $('[name=filter_id_organisasi]').val()!=undefined){
		var val1=[];
		$("[name=filter_id_propinsi] option").each(function(){
		   if(this.value!="") val1.push(this.value)
		});
		newclause.push('id_propinsi_unit_auditi.in('+val1.toString()+')');
	}
	if(newclause.length > 0){
		newparam="&filter_funct="+newclause.toString();
	}
	
  if(satkerId!="") clause.push('id_unit_auditi.eq('+satkerId+')');
  params=clause.toString();
	//
ajaxGET(path + '/kelompok_jabatan?page='+page+'&limit='+limit,function(response){
	var responsedatakelompokjabatan=response.data;
  ajaxGET(path + '/progress_peserta_ujian_detil?page='+page+'&limit='+limit+'&join=kelompok_jabatan,unit_auditi&filter='+public_filter+","+params+newparam,function(response){
  	console.log('list_progress_peserta_ujian_detil', response.data);
  	/*
    var unique_kelompok=[];
    var unique_kelompok_with_string=[];
	$.each(response.data,function(key){
    	var valueunique = this.id_kelompok_jabatan;
		if(unique_kelompok.indexOf(valueunique) == -1) unique_kelompok.push(valueunique);
		//if(unique_kelompok_teknis_with_string.indexOf(valueunique) == -1) unique_kelompok_teknis_with_string.push(valueunique);
		//else unique_kelompok_teknis_with_string.push("");
	});
	*/
  	
  	var obj=[];
//  	$.each(unique_kelompok, function(keyunqiue, valueunique){
//  	  	var count_jumlah=0;
//  	  	var count_total=0;
//	  	$.each(response.data, function(key){
//	  		if(valueunique == this.id_kelompok_jabatan){
//	  			if(select_id_unit_auditi!=""){
//		  			if(this.id_unit_auditi==select_id_unit_auditi){
//		  				count_jumlah+=this.jumlah_peserta_berjalan;
//			  			count_total+=this.total_peserta;
//		  			}
//	  			}else{
//		  			count_jumlah+=this.jumlah_peserta_berjalan;
//	  				count_total+=this.total_peserta;
//	  			}
//	  		}
//	  	});
	  	$.each(responsedatakelompokjabatan, function(key){
//	  		console.log(this);
//	  		if(this.id==valueunique){
//				obj.push({
//					"id_kelompok_jabatan" : valueunique,
//					"jumlah_peserta_berjalan" : count_jumlah,
//					"total_peserta" : count_total,
//					"kode_sesi" : $.urlParam('kode-sesi'),
//					"kelompok_jabatan" : {"id" : valueunique, "kode" : this.kode}
//				});
//	  		}
//	  		var count_jumlah=0;
//	  	  	var count_total=0;
//		  	$.each(response.data, function(key){
//		  		if(valueunique == this.id_kelompok_jabatan){
//		  			if(select_id_unit_auditi!=""){
//			  			if(this.id_unit_auditi==select_id_unit_auditi){
//			  				count_jumlah+=this.jumlah_peserta_berjalan;
//				  			count_total+=this.total_peserta;
//			  			}
//		  			}else{
//			  			count_jumlah+=this.jumlah_peserta_berjalan;
//		  				count_total+=this.total_peserta;
//		  			}
//		  		}
//		  	});
	  	})
//  	});

  	$.each(responsedatakelompokjabatan, function(key, value){
  		value["jumlah_peserta_berjalan"]=0;
  		value["total_peserta"]=0;
  		value["kode_sesi"]=$.urlParam('kode-sesi');

  		$.each(response.data, function(key){
  	  		if(this.id_kelompok_jabatan==value.id){
  	  			value["jumlah_peserta_berjalan"]+=this.jumlah_peserta_berjalan;
  	  			value["total_peserta"]+=this.total_peserta;
  	  		}
  	  	})
  	})

  	console.log(responsedatakelompokjabatan);
  	//
  	public_list_data_progress_peserta_ujian = responsedatakelompokjabatan;
  	$.each(responsedatakelompokjabatan, function(key){
  		if(selected_id_kelompok!=""){
	  		if(selected_id_kelompok==this.id){
	  			arr_total_gender_usia.push(this.jumlah_peserta_berjalan);
	  			total_gender_usia+=this.jumlah_peserta_berjalan;
	  		}
  		}else{
  			arr_total_gender_usia.push(this.jumlah_peserta_berjalan);
			total_gender_usia+=this.jumlah_peserta_berjalan;
  		}
  	});
  	generateDynamicChartProgressPesertaUjian();
  },'onGetListError');
},'onGetListError');
}


function displaySosiokultural(){
	var params="";
	var tbody = $("#tbl-komp-sosio").find('tbody');
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');

    //params+="filter_jenis_kompetensi=SOSIOKULTURAL&";
    //if(selected_id_kelompok!="") params+='filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
    //params+=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	//params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
    //ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000",'onGetListDetilSosiSuccess','onGetListDetilError');
    ////ajaxGET(path + '/detil_perjabatan_kompetensi'+params+"&limit=10000000",'onGetListDetilSosiSuccess','onGetListDetilError');
	
	params="?";
	ajaxGET(path + '/jenjang_jabatan'+params,function(responseMasterJenjang){
		params=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
		params+="&filter_jenis_kompetensi=SOSIOKULTURAL";
		params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
  		if(selected_id_kelompok!="") params+='&filter_id_kelompok_jabatan='+selected_id_kelompok;
		params+="&filter_is_non_teknis=";
	    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000", function(response){
		//	
			var myCanvasBody={
		    		label : [],
		    		nilai_standar : [],
		    		nilai : []
		    }

			var doFilter=false;
			if($('#filter_jenjang_jabatan').val()!="") doFilter=true;
			$.LoadingOverlay("hide");
			var row="";
			if(doFilter){
				$.each(responseMasterJenjang.data,function(keyjenjang,value){
					if($('#filter_jenjang_jabatan').val()==value.id){
				        row += '<tr class="data-row" id="row-'+value.id+'">';
				        row += '<th class="bg-secondary text-white"> '+value.nama +' </th>';
						
				        row += '<td id="tdsosinilai-'+keyjenjang+'" align="" class="text-center"></td>';
				        row += '<td id="tdsosistandar-'+keyjenjang+'" align="" class="text-center"></td>';
					    
				        row += '</tr>';
						//myCanvasBody.label.push(value.nama);
					}
				});
			}else{
				$.each(responseMasterJenjang.data,function(keyjenjang,value){
					
			        row += '<tr class="data-row" id="row-'+value.id+'">';
			        row += '<th class="bg-secondary text-white"> '+value.nama +' </th>';
					
			        row += '<td id="tdsosinilai-'+keyjenjang+'" align="" class="text-center"></td>';
			        row += '<td id="tdsosistandar-'+keyjenjang+'" align="" class="text-center"></td>';
				    
			        row += '</tr>';

					//myCanvasBody.label.push(value.nama);
				});
			}
		    tbody.html(row);

			//set value
			$.each(responseMasterJenjang.data,function(keyjenjang,valueJenjang){
				$.each(response.data,function(key,value){
					if(this.id_jenjang_jabatan==valueJenjang.id){
						$("#tdsosinilai-"+keyjenjang).text(this.nilai_alt!=null?this.nilai_alt.toFixed(1):0);
						$("#tdsosinilai-"+keyjenjang).attr('title', "Jumlah Responden:"+this.jumlah_unit_komp);
						if(this.nilai_alt < this.nilai_standar) $("#tdsosinilai-"+keyjenjang).addClass('bg-danger text-white');
						$("#tdsosistandar-"+keyjenjang).text(this.nilai_standar!=null?this.nilai_standar:0);
						
						myCanvasBody.label.push(valueJenjang.nama);
						myCanvasBody.nilai_standar.push(this.nilai_standar!=null?this.nilai_standar:0);
						myCanvasBody.nilai.push(this.nilai_alt!=null?this.nilai_alt.toFixed(1):0);
					}
		    	});
		    });
		
			console.log(myCanvasBody);
			/*
			$.each(myCanvasBody, function(key, value){
				var myobj={
			    		label : [],
			    		nilai_standar : [],
			    		nilai : []
			    }
				$.each(this.kel, function(i){
					var split_name_kel = (this.unit+"").split(" ");
					myobj.label.push(split_name_kel);
					myobj.nilai_standar.push(this.nilai_standar);
					myobj.nilai.push(this.nilai_alt.toFixed(1));
				});
				
				//getChartSosiokultural(myobj, document.getElementById("radar-"+key));
				//getRadarChart(myobj, document.getElementById("radar-"+key));
				console.log(myobj);
			});
			*/
			getChartSosiokultural(myCanvasBody, document.getElementById("chart-sosiokultural"));
			
		},'onGetListDetilError');
	},'onGetListDetilError');
}

function displayManajerial(){
    var params="";
    var tbody = $("#tbl-komp-manaj").find('tbody');
	var bodyCanvasManajerial = $('#container-manajerial');
    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');

	params="?";
	ajaxGET(path + '/unit_kompetensi_manajerial'+params+"join=kelompok_manajerial",function(responseMasterUnit){
		params="?";
		ajaxGET(path + '/jenjang_jabatan'+params,function(responseMasterJenjang){
			params=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
			params+="&filter_jenis_kompetensi=MANAJERIAL";
			params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
	  		if(selected_id_kelompok!="") params+='&filter_id_kelompok_jabatan='+selected_id_kelompok;
			params+="&filter_is_non_teknis=";
		    ajaxGET(ctx2 + '/dashboard/detil-perjabatan-kompetensi?'+params+"&limit=10000000", function(response){
			//
				$.LoadingOverlay("hide");
				//render kolom head jenjang
				var row=""
				$('#tr-head-manaj').html($('#tr-head-manaj').find('th')[0]);
				$.each(responseMasterJenjang.data,function(key){
					row += '<th class="align-middle text-center" style="writing-mode: tb-rl;transform: rotateZ(180deg);height:min-content">'+this.nama+'</th>';
			    });
				$('#tr-head-manaj').append(row);
				
				//render body
				var row="";
				$.each(responseMasterUnit.data,function(key,value){
			        row += '<tr class="data-row" id="row-'+value.id+'">';
			        row += '<th id="tdjudul-'+key+'" class="bg-secondary text-white"> '+value.judul +' </th>';
					
					$.each(responseMasterJenjang.data,function(keyjenjang,value){
			        	row += '<td id="tdkol-'+key+"-"+keyjenjang+'" align="" class="text-center"></td>';
				    });
			     
			        row += '</tr>';
			    });
			    tbody.html(row);
		
				//set value
    			var arrnama=[],arrstandar=[],arrnilai=[];
				var myCanvasBody=[];
				var myCanvasValue=[];
				$.each(responseMasterUnit.data,function(keyunit,valueUnit){
					$.each(responseMasterJenjang.data,function(keyjenjang,valueJenjang){
						$.each(response.data,function(key,value){
							if(this.id_jenjang_jabatan==valueJenjang.id && this.id_unit_kompetensi==valueUnit.id){
								console.log(this);
								$("#tdkol-"+keyunit+"-"+keyjenjang).text((this.nilai_alt!=null?this.nilai_alt:0)+" | "+this.nilai_standar);
								$("#tdkol-"+keyunit+"-"+keyjenjang).attr('title', "Nilai Standar:"+this.nilai_standar+"\n"+"Jumlah Responden:"+this.jumlah_unit_komp);
								if(this.nilai_alt < this.nilai_standar) $("#tdkol-"+keyunit+"-"+keyjenjang).addClass('bg-danger text-white');
								
								//create contqainer canvas
								if(keyunit==0) myCanvasBody.push({"id" : valueJenjang.id, "nama" : valueJenjang.nama, "kel" : []});
								
								myCanvasValue.push({"id_jenjang" : this.id_jenjang_jabatan, "unit" : valueUnit.judul, "nilai_alt" : this.nilai_alt, "nilai_standard" : this.nilai_standar});
								
							}
				    	});
				    });
			    });
				
				bodyCanvasManajerial.html("");
				var doFilter=false;
				if($('#filter_jenjang_jabatan').val()!="") doFilter=true;
				$.each(myCanvasBody,function(keyjenjang,value){
					console.log(this.id==$('#filter_jenjang_jabatan').val() && doFilter);
					if(this.id==$('#filter_jenjang_jabatan').val() && doFilter){
						myCanvasBody[filter_jenjang_jabatan]=this;
					}
				});
				console.log(Array.isArray(myCanvasBody));
				$.each(myCanvasBody,function(keyjenjang,value){
					var row = '<div class="col mb-4">'+
					'				    <div class="card h-100">'+
					'				      <div class="card-body">'+
					'				        <h5 class="card-title">'+value.nama+'</h5>'+
					'				        <div style="margin-top: 0px"> <!-- zoom: 1.1764705882352942;  -->'+
					'							<canvas id="radar-'+keyjenjang+'"></canvas>'+
					'						</div>'+
					'				      </div>'+
					'				    </div>'+
					'				  </div>';
						
					bodyCanvasManajerial.append(row);
					
					if(doFilter){
						if(value.id==$('#filter_jenjang_jabatan').val()){
							$.each(myCanvasValue,function(keyMyCanvasValue){
								if(value.id==this.id_jenjang) value.kel.push({"id" : this.id_jenjang, "unit" : this.unit, "nilai_alt" : this.nilai_alt, "nilai_standar" : this.nilai_standard});
							});
						}
					}else{
						$.each(myCanvasValue,function(keyMyCanvasValue){
							if(value.id==this.id_jenjang) value.kel.push({"id" : this.id_jenjang, "unit" : this.unit, "nilai_alt" : this.nilai_alt, "nilai_standar" : this.nilai_standard});
						});
					}
			    });
				
				console.log(myCanvasBody);
				
				$.each(myCanvasBody, function(key, value){
					var myobj={
				    		label : [],
				    		nilai_standar : [],
				    		nilai : []
				    }
					$.each(this.kel, function(i){
						var split_name_kel = (this.unit+"").split(" ");
						myobj.label.push(split_name_kel);
						myobj.nilai_standar.push(this.nilai_standar);
						myobj.nilai.push(this.nilai_alt.toFixed(1));
					});
					getRadarChart(myobj, document.getElementById("radar-"+key));
					console.log(myobj);
				});
			//
			},'onGetListDetilError');
		},'onGetListDetilError');
	},'onGetListDetilError');
}

function displayTeknis(){
	var params=$('#form-search').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
    
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    
	params+="&filter_jenis_kompetensi=TEKNIS&";
  	if(selected_id_kelompok!="") params+='filter_id_kelompok_jabatan='+selected_id_kelompok+"&";
	params+="&filter_kode_sesi="+$.urlParam('kode-sesi');
	ajaxGET(ctx2 + '/dashboard/laporan-nilai-analisis?'+params,function(response){
	$.LoadingOverlay("hide");
	
	var html = [];
	$.each(response.data.kompTeknis, function(key, value) {
		var name = this.judulFungsiUtama;
		if(html.indexOf(name) == -1) html.push(name);
		else html.push("");
	});
	
    var i = 1;
	var countrow=0;
	var arrMainrow=[];
	var arrcountrow=[];
	//
	var count_teknis=0;
	var sum_teknis=0;
	var sum_jumlah_teknis=0;
	//
	var myData = [];
	//
	var totalKomp=0,totalKompYgTdkDipilih=0,totalKompYgDipilih=0;
	var totalKompTdkKuasai=0, totalKompKuasai=0;
	$.each(response.data.kompTeknis,function(key,value){
		if(html[key]){
			arrcountrow=[];
			countrow=0;
			arrMainrow.push(arrcountrow);
		}else{
			arrcountrow.push(countrow);
			i = i-1;
			countrow++;
		}
		
		//data for bar grafik komp teknis
        myData.push([value.kodeFungsiUtama, "  "+value.kodeUnitKompetensiTeknis, value.judulFungsiUtama, value.nilaiUnitKompetensiTeknis, value.jumlahUnitKompetensiTeknis]);
		
		//data for pie grafik standar
		if(value.nilaiUnitKompetensiTeknis==null) {
			totalKompYgTdkDipilih+=1
		}else{
			totalKompYgDipilih+=1
		}
		totalKomp+=1;
		
		//data for pie grafik penguasaan standar
		if(value.nilaiUnitKompetensiTeknis!=null){
			if(value.nilaiUnitKompetensiTeknis<60) {
				totalKompTdkKuasai+=1;
			}
			if(value.nilaiUnitKompetensiTeknis>=60) {
				totalKompKuasai+=1;
			}
		}
		
		count_teknis++;
	 	sum_teknis+=this.nilaiUnitKompetensiTeknis;
	 	sum_jumlah_teknis+=this.jumlahUnitKompetensiTeknis;
		i++;
    });
	
	var row="";
	var arrnama=[],arrnilai=[],arrjumlah=[];
	$.each(response.data.fungsiUtama,function(key,value){
		arrnama.push(value.kode);
	    arrnilai.push(value.nilai);
	    //arrjumlah.push(value.jumlahUnitKompetensiTeknis);
    });

	for(var idx=0; idx < (response.data.fungsiUtama.length/4); idx++){
		console.log("idx", idx);
		row+="<td id='td"+idx+"'>";
			row+='<table id="tbl-'+idx+'" class="table table-sm">'+
		'         <thead class="thead-light">'+
		'            <tr>'+
		'               <th scope="col">Kode</th>'+
		'               <th scope="col">Fungsi Utama</th>'+
		'            </tr>'+
		'         </thead>'+
		'<tbody></tbody>';
		
		row+='</table>';
		row+="</td>";
	}
	var container_tbl="<tr>"+row+"</tr>";
	$("#tbl-fungsi-utama").find('tbody').html(container_tbl);
	
	var idx_row=0;
	$.each(response.data.fungsiUtama,function(key,value){
		var row_body="<tr><td>"+value.kode+"</td><td data-toggle='tooltip' data-placement='top' title='"+value.judul+"'><span  class='d-inline-blocks text-truncates w-75s'>"+(value.judul.toString().split(" ").length>1?value.judul.toString().split(" ")[0]+" "+value.judul.toString().split(" ")[1]:value.judul)+"</span></td></tr>";
		
		console.log("check", parseInt(idx_row/(4)));
		$('#tbl-'+parseInt(idx_row/(4))).find('tbody').append(row_body);
		idx_row++;
    });

    var objFungsiUtama={
    		element : 'bar-fungsi-utama',
    		element_container : 'container-bar-fungsi-utama',
    		label : arrnama,
    		nilai : arrnilai,
    		jumlah : arrjumlah
    };

	renderChartKompTeknis(myData);
	renderChartFungsiUtama(objFungsiUtama);
	
	var dataSK = {
		totalResponden : 0, //belum diset
		totalKomp : totalKomp,
		totalKompYgDipilih : totalKompYgDipilih,
		totalKompYgTdkDipilih : totalKompYgTdkDipilih
	}
	console.log(dataSK);
	renderChartStandarKomp(dataSK);
	var dataPK = {
		totalResponden : 0,//belum diset
		totalKomp : totalKomp,
		totalKompYgDipilih : totalKompYgDipilih,
		totalKompKuasai : totalKompKuasai,
		totalKompTdkKuasai : totalKompTdkKuasai
	}
	console.log(dataPK);
	renderChartPenguasaanKomp(dataPK);
	
	},'onGetListError');
}

function renderChartStandarKomp(data){
	var chartdata = {
        labels: ['Belum dilaksanakan', 'Telah dilaksanakan'],
        datasets: [
          {
            label: 'My First dataset',
            data: [((data.totalKompYgTdkDipilih/data.totalKomp)*100), ((data.totalKompYgDipilih/data.totalKomp)*100)],
            backgroundColor: [
              '#FF6384',
              '#36A2EB'
            ]
          }
        ]
     };
	
	//if var null that's mean chart is first render and need render new canvas
	//but if not null then chart only need update the data only
	if(myChartStandarKomp!=null) {
		myChartStandarKomp.data = chartdata;
		myChartStandarKomp.update();
	}else{
		myChartStandarKomp = new Chart(document.getElementById('pie-chart'),{
			  type: 'pie',
			  data: chartdata,
			  options : {
					showAllTooltips: true, // call plugin we created
	    			maintainAspectRatio: true,
	    			responsive: true,
					//showTooltips: true,
	    			hover :{ animationDuration: 0 },
	    			legend:{ display: true, position: 'right' },
	    			legendCallback: function(chart) {
						console.log(chart);
	    				var labels = ["Tercapai", "Tidak Tercapai"];
	    				var background = ["#4A9B82", "#dc3545"];
	    			    var text = []; 
	    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
	    			    for (var i = 0; i < labels.length; i++) { 
	    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
	    			        if (labels[i]) { 
	    			            text.push(labels[i]); 
	    			        } 
	    			        text.push('</li>'); 
	    			    } 
	    			    text.push('</ul>'); 
	    			    return text.join(''); 
	    			},
		            tooltips: {
		                enabled: true,
						intersect: false,
		                mode: 'single',
		                callbacks: {
		                    label: function(tooltipItems, data) { 
		                        return data.datasets[0].data[tooltipItems.index].toFixed(2) + ' %';
		                    }
	
		                }
		            },
	    			animation: {
	    				onComplete: function () {
	    					var chartInstance = this.chart;
	    					var ctx = chartInstance.ctx;
	    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
	    						var meta = chartInstance.controller.getDatasetMeta(i);
	    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
	    							newdata = (dataset.data[index]);
	    							console.log(ctx);
	    							console.log("phan");
	    						}),this)
	    					}),this);
	    				}
					}
			},
		});
	}
}

function renderChartPenguasaanKomp(data){
	var chartdata = {
        labels: ['Belum Menguasai', 'Telah Menguasai'],
        datasets: [
          {
            label: 'My First dataset',
            data: [((data.totalKompTdkKuasai/data.totalKompYgDipilih)*100), ((data.totalKompKuasai/data.totalKompYgDipilih)*100)],
            backgroundColor: [
              '#ffc107',
              '#4A9B82'
            ]
          }
        ]
     };
	//if var null that's mean chart is first render and need render new canvas
	//but if not null then chart only need update the data only
	if(myChartPenguasaanKomp!=null) {
		myChartPenguasaanKomp.data = chartdata;
		myChartPenguasaanKomp.update();
	}else{
		myChartPenguasaanKomp = new Chart(document.getElementById('pie-chart-penguasaan'),{
			  type: 'pie',
			  data: chartdata,
			  options : {
					showAllTooltips: true, // call plugin we created
	    			maintainAspectRatio: true,
	    			responsive: true,
					//showTooltips: true,
	    			hover :{ animationDuration: 0 },
	    			legend:{ display: true, position: 'right' },
	    			legendCallback: function(chart) {
						console.log(chart);
	    				var labels = ["Tercapai", "Tidak Tercapai"];
	    				var background = ["#4A9B82", "#dc3545"];
	    			    var text = []; 
	    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
	    			    for (var i = 0; i < labels.length; i++) { 
	    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
	    			        if (labels[i]) { 
	    			            text.push(labels[i]); 
	    			        } 
	    			        text.push('</li>'); 
	    			    } 
	    			    text.push('</ul>'); 
	    			    return text.join(''); 
	    			},
		            tooltips: {
		                enabled: true,
						intersect: false,
		                mode: 'single',
		                callbacks: {
		                    label: function(tooltipItems, data) { 
		                        return data.datasets[0].data[tooltipItems.index].toFixed(2) + ' %';
		                    }
	
		                }
		            },
	    			animation: {
	    				onComplete: function () {
	    					var chartInstance = this.chart;
	    					var ctx = chartInstance.ctx;
	    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
	    						var meta = chartInstance.controller.getDatasetMeta(i);
	    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
	    							newdata = (dataset.data[index]);
	    							console.log(ctx);
	    							console.log("phan");
	    						}),this)
	    					}),this);
	    				}
					}
			},
		});
	}
}

function renderChartFungsiUtama(data){
	console.log("data fungsi utama:", data);
	// scale second chart based on ratio of data to the first
    var fiddleFactor = 1.1; // determined by guesswork
    var ratio = data.label.length * fiddleFactor / data.label.length;
    var container1Height = parseInt(document.getElementById(data.element_container).style.width);
    
	// scale height of second chart
    //document.getElementById(data.obj_pilihan.element_container).style.width = container1Height * ratio + 'px';
    var rulecolor =[];
	var rulehovercolor =[];
	$.each(data.nilai, function(key){
		if(this < 60){
			rulecolor.push("#dc3545");
			rulehovercolor.push("#dc3545c7");
		}else{
			rulecolor.push("#4A9B82");
			rulehovercolor.push("#70c2a9");
		}
	});
	
	//reset canvas
	 $('#'+data.element).remove();
	 $('#'+data.element_container).append('<canvas height="400" width="400" id="'+data.element+'"><canvas>');
	
	 canvas = document.querySelector('#'+data.element);
	 ctx = canvas.getContext('2d');

	 var x = canvas.width/2;
	 var y = canvas.height/2;
	 ctx.font = '10pt Verdana';
	 ctx.textAlign = 'center';
	 ctx.fillText('This text is centered on the canvas', x, y);	  
	//end reset
	
    	var data_utama = data;
    	if(data_utama.nilai.length < 10){
		  $('#container-bar-fungsi-utama').attr('style', 'width:400px;');
    	}else if(data_utama.nilai.length < 15 ){
    		$('#container-bar-fungsi-utama').attr('style', 'width:100%');
		}else{
    		$('#container-bar-fungsi-utama').attr('style', 'width:'+(50*(data_utama.nilai.length))+'px;');
    	}
    	
    	var ctx = document.getElementById(data.element);
    	//ctx.height = 250;
    	myChartFungsiUtama = new Chart(ctx, {
    		type: 'bar',
    		data: {
    	    labels: data.label,
    	    datasets: [{
    	    	label: 'Skor',
    	        data: data.nilai,
    	        backgroundColor: rulecolor,//"#4A9B82", 
    	        hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
//    	        barPercentage: 0.5,
//    	        barThickness: 6,
//    	        maxBarThickness: 10,
//    	        minBarLength: 10,
    	    }]
    		},
    		options: {
//    			elements: {
//		          rectangle: {
//		            borderSkipped: 'left',
//		          },
//		        },
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
					console.log(chart);
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
    			scales: {
    				xAxes: [{
    					ticks: {
    						beginAtZero:true,
    						fontFamily: "'verdana', sans-serif",
    						fontSize:12,
    	                    min: 0,
    	                    max: 100
    					},
    					scaleLabel:{ display:false },
    					gridLines: { }, 
    					stacked: true
    				}],
    				yAxes: [{
					
			          tabIndex: 0,
			          barPercentage: 0.9,
			          barThickness: 100,
			          maxBarThickness: 40,
			          minBarLength: 0.5,
			          gridLines: {
			            display: false,
			            drawBorder: false,
			          },
			          ticks: {
						fontSize:11,
	                    min: 0,
	                    max: 100
			          },
    				}]
    			},
    			animation: {
	
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
						//not need if hasbeen modfied from scale x or y
    					/*ctx.textAlign = "right";
    					ctx.font = "12px verdana";
    					ctx.fillStyle = "#fff";*/
    					
						ctx.textAlign = "center";
						
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
									//
									//+ '|'+data.obj_inti.jumlah[index]
    								if(newdata==100){
										//ctx.fillStyle = "#fff";
										ctx.fillText(newdata, bar._model.x, bar._model.y+15);
									}else{
										//ctx.fillStyle = "#000";
										ctx.fillText(newdata, bar._model.x, bar._model.y-4.6);
    								}
								} else {
    								if(newdata == 0) {
    									//ctx.fillStyle = "#dc3545";
										//+ '|'+data.obj_inti.jumlah[index]
    									ctx.fillText(newdata, bar._model.x, bar._model.y);
    								}else{
    									//ctx.fillStyle = "#000";//fff
										//+ '|'+data.obj_inti.jumlah[index]
    									ctx.fillText(newdata, bar._model.x, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}
    				/*onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
    					ctx.textAlign = "right";
    					ctx.font = "14px verdana";
    					ctx.fillStyle = "#fff";
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							
    							newdata = (dataset.data[index]);
    							//console.log(data);
    							if(newdata > 0) {
    								ctx.fillStyle = "#fff";
    								ctx.fillText(data.nilai[index]+ '|'+newdata, bar._model.x-10, bar._model.y);
    							} else {
    								if(newdata == 0) {
    									ctx.fillStyle = "#dc3545";
    									ctx.fillText(data.nilai[index]+ '|'+newdata, bar._model.x+45, bar._model.y);
    								}else{
    									ctx.fillStyle = "#fff";
    									ctx.fillText(data.nilai[index]+ '|'+newdata, bar._model.x+50, bar._model.y);
    								}
    							}
    						}),this)
    					}),this);
    				}*/
    			},
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
    		}
    	});
    	//$("#container-legend-bar-fungsi-utama").html(myChart.generateLegend());
}

function renderChartKompTeknis(myData){
	//the chart to big, use this, to improve some issue render chart not complete
	Chart.helpers.canvas.clipArea = function() {};
	Chart.helpers.canvas.unclipArea = function() {};
	
	var labels = calculateGroupLabels(myData);
	
	var rulecolor =[];
	var rulehovercolor =[];
	$.each(myData.map((entry)=>entry[3]), function(key){
		if(this < 60){
			rulecolor.push("#dc3545");
			rulehovercolor.push("#dc3545c7");
		}else{
			rulecolor.push("#4A9B82");
			rulehovercolor.push("#70c2a9");
		}
	});
	//reset canvas
	 $('#bar-teknis').remove();
	 $('#container-bar-teknis').append('<canvas height="400" width="400" id="bar-teknis"><canvas>');
	
	 canvas = document.querySelector('#bar-teknis');
	 ctx = canvas.getContext('2d');

	 var x = canvas.width/2;
	 var y = canvas.height/2;
	 ctx.font = '10pt Verdana';
	 ctx.textAlign = 'center';
	 ctx.fillText('This text is centered on the canvas', x, y);	  
	//end reset
	
	var obj_jumlah = myData.map((entry)=>entry[4]);
	if(obj_jumlah.length < 3 ){
		$('#container-bar-teknis').attr('style', 'width:300px;');
    }else if(obj_jumlah.length < 5 ){
    	$('#container-bar-teknis').attr('style', 'width:400px;');
	}else if(obj_jumlah.length < 20 ){
    	$('#container-bar-teknis').attr('style', 'width:100%');
	}else{
	  	$('#container-bar-teknis').attr('style', 'width:'+(50*(obj_jumlah.length))+'px;');
	}
	
	var ctx = $("#bar-teknis");
	myChartKompTeknis = new Chart(ctx, {
	  type: 'bar',
	  data: {
	    datasets: [{
    		label: 'Skor',
	      	data: myData.map((entry)=>entry[3]),
			xAxisID:'modelAxis',
        	backgroundColor: rulecolor,//"#4A9B82", 
        	hoverBackgroundColor: rulehovercolor,//"#70c2a9" 
	    }]
	  },
	  options:{
    			maintainAspectRatio: false,
    			responsive: true,
    			tooltips: { enabled: false },
    			hover :{ animationDuration: 0 },
    			legend:{ display: false, position: 'bottom' },
    			legendCallback: function(chart) {
    				var labels = ["Tercapai", "Tidak Tercapai"];
    				var background = ["#4A9B82", "#dc3545"];
    			    var text = []; 
    			    text.push('<ul class="' + chart.id + '-legend nav">'); 
    			    for (var i = 0; i < labels.length; i++) { 
    			        text.push('<li class="nav-item mr-3"><span style="background-color:' + background[i] + '"></span>'); 
    			        if (labels[i]) { 
    			            text.push(labels[i]); 
    			        } 
    			        text.push('</li>'); 
    			    } 
    			    text.push('</ul>'); 
    			    return text.join(''); 
    			},
			    scales:{
			      xAxes:[
			        {
			          id:'modelAxis',
			          type:"category",
			          ticks:{
				            //maxRotation:0,
				            //autoSkip: false,
				            callback:function(label, x2, x3, x4){
				              console.log("modelAxis", label, x2, x3, x4)
				              return label;//+"("+(myData.map((entry=>entry[4]))[x2]!=null?myData.map((entry=>entry[4]))[x2]:"0")+")";
				            },
							beginAtZero:true,
							fontFamily: "'verdana', sans-serif",
							fontSize:12,
		                    min: 0,
		                    max: 100,
					        autoSkip: false,
					        maxRotation: 90,
					        minRotation: 90,
                   		},
			         	labels:myData.map((entry=>entry[1]))
			        },
			        {
			          id:'groupAxis',
			          type:"category",
			          gridLines: {
			            drawOnChartArea: false,
						//display: false ,
  						color: "#000",
						//ineHeight:3,
			          },
			          ticks:{
						fontSize:13,
						//fontColor: '#fff',
						//lineHeight:3,
			            maxRotation:0,
			            autoSkip: false,
			            callback:function(label){
			            	return label;
			            }
			          },
			          labels:labels
			          
			        }],
			      yAxes:[{
			        /*ticks:{
			          beginAtZero:true
			        },*/
			          tabIndex: 0,
			          barPercentage: 0.9,
			          barThickness: 100,
			          maxBarThickness: 40,
			          minBarLength: 0.5,
			          gridLines: {
			            display: false,
			            drawBorder: false,
			          },
			          ticks: {
						fontSize:11,
	                    min: 0,
	                    max: 100
			          },
			      }]
			    },
    			animation: {
    				onComplete: function () {
    					var chartInstance = this.chart;
    					var ctx = chartInstance.ctx;
						//not need if hasbeen modfied from scale x or y
    					ctx.textAlign = "center";
    					/*ctx.font = "12px verdana";*/
    					ctx.fillStyle = "#fff";
	//ctx.textAlign = 'left';
          ctx.textBaseline = 'bottom';
    					
    					Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
    						var meta = chartInstance.controller.getDatasetMeta(i);
    						Chart.helpers.each(meta.data.forEach(function (bar, index) {
    							newdata = (dataset.data[index]);
								obj_jumlah = myData.map((entry)=>entry[4])[index];
    							if(newdata > 0) {
									//
									//+ '|'+data.obj_inti.jumlah[index]
    								if(newdata==100){
										//ctx.fillStyle = "#fff";
										ctx.fillText(obj_jumlah, bar._model.x, bar._model.y+235);
									}else{
										//ctx.fillStyle = "#000";
										//ctx.fillText(newdata, bar._model.x+4, bar._model.y-25);
										ctx.fillText(obj_jumlah, bar._model.x, bar._model.y+50);
    								}
								} else {
									console.log(newdata);
									/*
    								if(newdata == 0) {
    									//ctx.fillStyle = "#dc3545";
										//+ '|'+data.obj_inti.jumlah[index]
    									ctx.fillText(newdata, bar._model.x+45, bar._model.y);
    								}else{
    									//ctx.fillStyle = "#000";//fff
										//+ '|'+data.obj_inti.jumlah[index]
    									ctx.fillText(newdata, bar._model.x+50, bar._model.y);
    								}*/
				
									ctx.fillText(obj_jumlah, bar._model.x-5, bar._model.y-4.6);
    							}
    						}),this)
    					}),this);
		
						 // draw total count
					      this.data.datasets[0].data.forEach(function(data, index) {
					        var total = data + this.data.datasets[0].data[index];
					        var obj_jumlah = myData.map((entry)=>entry[4])[index];
							var new_data = data;//(dataset.data[index]);

							var meta = chartInstance.controller.getDatasetMeta(0);
					        var posX = meta.data[index]._model.x;
					        var posY = meta.data[index]._model.y;
							ctx.textAlign = "center";
					        ctx.fillStyle = "white";
					        ctx.fillText(new_data, posX , posY+14);
					      }, this);
    				}
    			},
				
    			pointLabelFontFamily : "Quadon Extra Bold",
    			scaleFontFamily : "Quadon Extra Bold",
	  }
	});
}

function calculateGroupLabels(data){
  const scaleFactor=100;
  var labels = _(data)
  .groupBy((elem)=>elem[0])
  .map((entriesOnSameGroup, key)=>{
      var newSize = entriesOnSameGroup.length*scaleFactor;
      var newArray = new Array(newSize);
      newArray[0]="";
      newArray[newArray.length-1]="";
      newArray[parseInt((newArray.length-1)/2)]=key;
      return newArray;
  }).flatten().value()

  return labels;
}


function renderPercentage(value, numeric) {
	if(numeric == 0) return 0.5;
	return (value < 3 ? 3 : value);
}

function generateChart(data) {
  try {
    var ctx = document.getElementById("monthlyChart");
    if (ctx) {
      ctx.height = 100;
      chartBar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          legend: { position: 'top', labels: { fontFamily: 'Poppins' } },
          scales: { xAxes: [{ ticks: { fontFamily: "Poppins" } }], yAxes: [{ ticks: { beginAtZero: true, fontFamily: "Poppins" } }] }
        }
      });
    }
  } catch (error) {
    console.log(error);
  }

};

function goToJabatanRekomendasi(){
	if(selected_id_kelompok=="") alert("Kelompok Jabatan Belum dipilih.");
	else{
		var params="";
		if(selected_mode!="") params+="mode="+selected_mode+"&";
		if(selected_id_kelompok!=""){
			params+="id_kelompok="+selected_id_kelompok;
		}
		if($('#filter_tingkat_jabatan').val()!=""){
			params+="&id_tingkatan="+$('#filter_tingkat_jabatan').val();
		}
		if($('#filter_jenjang_jabatan').val()!=""){
			params+="&id_jenjang="+$('#filter_jenjang_jabatan').val();
		}
		if($('[name=filter_id_eselon1]').val()!=""){
			params+="&id_eselon1="+$('[name=filter_id_eselon1]').val();
		}
		if($('[name=filter_id_organisasi]').val()!='' && $('[name=filter_id_organisasi]').val()!=undefined){
			params+="&id_organisasi="+$('[name=filter_id_organisasi]').val();
		}
		if($('[name=filter_id_propinsi]').val()!=""){
			params+="&id_propinsi="+$('[name=filter_id_propinsi]').val();
		}
		if($('[name=filter_id_unit_auditi]').val()!=""){
			params+="&id_unit_auditi="+$('[name=filter_id_unit_auditi]').val();
		}
		if($.urlParam('kode-sesi')!=null){
			params+="&kode-sesi="+$.urlParam('kode-sesi');
		}
		console.log(params);
		//window.location.href="per-jabatan-rekomendasi?"+params;
		window.open("per-jabatan-rekomendasi?"+params, '_blank');
	}
}