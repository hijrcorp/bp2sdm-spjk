var url = ctx + "/simulasi-survey/";
var url_responden = ctx + "/responden/";
var url_responden_jawaban = ctx + "/responden-jawaban/";
var urlExport = ctx +"/rest/export/";
var param = "";
var selected_id='';
var selected_id_responden_jawaban;
var mode='';
var judul_sesi='';
var kodean="";
var jkom="";
var toPass=false;
var timeou=0;
var temp_timeou=0;
var id_pertanyaan=0;
var timenow; 
var duration_actual=0;
var public_jawaban = {};

let socket;
let reconnectInterval = 30000; // Time in milliseconds between reconnection attempts
let isReconnecting = false;

let public_responden = {};

function connectWebSocket(){
	socket = new WebSocket(window.location.hostname === 'localhost' ? 'ws://localhost:7077'+ctx+'/exam-updates' : 'wss://simrenbang.bp2sdm.menlhk.go.id'+ctx+'/exam-updates');
	socket.onopen = function() {
	    console.log('WebSocket connection established');
	    // Kirim data awal ke server (misalnya, waktu mulai ujian)
	    //socket.send(JSON.stringify({ action: 'start', time: new Date().toISOString() }));
		
		if(isReconnecting){
			counter(sessionStorage.getItem("temp_timeou"));
	    	$("#table-card-body").find('.card-body, ul').removeClass('blur-text');
			$("#table-card-body").find('input, button').prop('disabled',false);
			showNotification('Connection restored', true);
	        setTimeout(hideNotification, 3000); // Hide after 3 seconds
		}else{
			isReconnecting = false;
		}
	};
	
	socket.onmessage = function(event) {
	    //console.log('Message from server:', event.data);
	    // Lakukan pembaruan UI atau logika lain sesuai kebutuhan
	};
	
	socket.onclose = function(event) {
		clearTimeout(timeou);
	    $("#table-card-body").find('.card-body, ul').addClass('blur-text');
		$("#table-card-body").find('input, button').prop('disabled',true)
		//console.error('WebSocket connection closed:', event);
	    showNotification('Connection lost. Reconnecting... in 30 seconds', false);
		if (!isReconnecting) {
            isReconnecting = true;
            reconnectWebSocket();
        }
	};
	
	socket.onerror = function(error) {
	    console.error('WebSocket error:', error);
		isReconnecting = true;
        reconnectWebSocket();
	};
}

function reconnectWebSocket() {
    console.log('Attempting to reconnect...');
    setTimeout(() => {
        connectWebSocket();
    }, reconnectInterval);
}

// Show notification function
function showNotification(message, isConnected) {
    const notification = document.getElementById('connectionNotification');
    const messageElement = document.getElementById('connectionMessage');
    messageElement.textContent = message;
    if (isConnected) {
        notification.classList.add('connected');
    } else {
        notification.classList.remove('connected');
    }
    notification.style.display = 'block';
}

// Hide notification function
function hideNotification() {
    const notification = document.getElementById('connectionNotification');
    notification.style.display = 'none';
}

function init() {
	connectWebSocket();

	selected_id = idResponden;
	selected_page = ($.urlParam('page') == null ? '' : $.urlParam('page'));
	kegiatan_pengawasan_id = ($.urlParam('kegiatan_pengawasan_id') == null ? '' : $.urlParam('kegiatan_pengawasan_id'));
	
	cek_status_responden();
	
	$('.navbar-toggler ').addClass('d-none');
	
	$(window).resize(function() {
	  //resize just happened, pixels changed
		cekConsole();
	});
}
function cekConsole(){
	var devtools = function() {};
	devtools.toString = function() {
	  if (!this.opened) {
	    //alert("Opened");
	    //window.location.href=ctx+"/login-logout?banned";
	  }
	  this.opened = true;
	}

	console.log('%c', devtools);
	// devtools.opened will become true if/when the console is opened
}
function trysave(){
//	var tbody = $("#table-card-body");
//	tbody.html("will refresh");
//	return "refresh";
}

function cek_status_responden(){
	ajaxGET(url +"check-responden/"+ selected_id,'onCheckSuccess','onCheckError');
}

function onCheckSuccess(response){
	public_responden = response.data;
	if(public_responden.status=="SELESAI") {
		window.location.href="index";
	}else{
		renderDisplay(1);//render soal-soal
	}
}

function setLoad(elem){
	elem.html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	$('#modal-form').modal('hide');
	var tbody = $("#table-card-body");
	setLoad(tbody);
	params+="header_responden="+selected_id;//+"&durasi="+durasi;
	setTimeout(() => ajaxGET(url + '?page='+page+'&limit='+1+'&'+params,'onGetListSuccess','onGetListError'), timeout);
}

function onGetListSuccess(response){
	var value = response.data.data[0];
	
	//reset timer here 
	//why harus reset????
	if((value.durasi-value.durasi_berjalan) < 1 ){
		clearTimeout(timeou);
	}
	
	//<!--start set body-->
	var tbody = $("#table-card-body"), tbodyside = $("#row-card-side"), tfooter = $("#footer-pagination");
	var row = "", rowside = "";
	
	var num = response.next_page_number-1;
	row += render_row_card(response, num);
	rowside += render_rowside_card(response, num);

	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	rowside == "" ? tbodyside.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbodyside.html(rowside);
	
	var myvar = '';
	if(response.next_page_number > 2 || response.next_page_number > response.count)
	myvar += '<button onclick="renderDisplay('+(response.next_page_number-2)+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>';
	
	if(response.next_more==false && (response.next_page_number-response.count)==2){
		myvar += '<button onclick="saveSaran()" type="button" class="btn m-0 p-0"><i class="fas fa-check-circle text-success"></i></button>';
	}else{
		myvar += '<button onclick="renderDisplay('+response.next_page_number+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>';
	}
	tfooter.html(myvar);

	var count_pernyataan=$('#table-card-body').find('.data-row').length;
	var count_pernyataan_answer=$('#table-card-body').find('input:checked').length;
	$('#pernyataan-titles').html('Terjawab <strong class="text-success">'+count_pernyataan_answer+'</strong> dari <strong class="text-danger">'+count_pernyataan+'</strong> Pernyataan  ');	
	if($.urlParam('confirm')=="true") $('.title-pembatasa').remove();
	$('#pernyataan-title').html("<strong>Kompetensi:</strong> "+jkom);
	$('#info-pertanyaan').html(response.message);	
	//<!--end setbody-->
	
	//set timmer here 
	//why set in here?????
	if(parseInt(sessionStorage.getItem("temp_timeou")) > 0){
		timeou=counter(sessionStorage.getItem("temp_timeou"));
		timenow=setInterval(() => {
			//console.log("sending something"+timeou+"/"+sessionStorage.getItem("temp_timeou"));
		    //ajaxGET(url + 'update-durasi-soal?id='+id_pertanyaan+'&durasi='+sessionStorage.getItem("temp_timeou"),'onUpdateSucc','onSaveError');
			
		    const duration = sessionStorage.getItem("temp_timeou");
		    const id = id_pertanyaan;
			const responden = public_responden.nip+"::"+public_responden.nama+"::"+public_responden.status+"::"+public_responden.status_unit_kompetensi+"::"+public_responden.id_account;
		    if (duration && socket.readyState === WebSocket.OPEN) {
		        const message = JSON.stringify({ id: id, duration: duration, duration_actual: duration_actual, responden: responden });
		       	socket.send(message);
		        //console.log('Data sent:', message);
		    }
		}, 5000);
	}
}

function render_row_card(response, num){
	var valuedata = response.data.data[0];
	var alpha = ["a","b","c","d","e","f","g"];
	data=response.data.data;
	console.log(data);
	var row = "";
	var count_pernyataan=0;
	if($.urlParam('confirm')=="true") {
		row += '<div class="card-header bg-warning text-white border mb-3 ">'+
	'	    	<div class="row">'+
'			    <div class="col col-md-12" id="pernyataan-title">Mohon <strong>Konfirmasi</strong> kembali jawaban anda sebelum lanjut</div>';
	row += '	</div>'+
		'		</div>';
	}

	if($.urlParam('confirm')=="finish"){
		$('#container-btn-end').removeClass("d-none");
		$('#container-btn-end').find('button').attr('onclick', 'renderDisplay('+(parseInt($.urlParam('page'))+1)+')');
	}

	/*row += '<div class="row ">'+
	'    <div class="col-md-12 text-center">'+
	'       <h5>'+"Durasi Waktu:"+ " <span id='spam-timer'>00:00</span>" +'</h5>'+
	'		<hr class="mb-2 mt-2">'+
	'	</div>'+
	'</div>';*/
	
	/*$.each(data.main,function(key,value_main){*/

			row += '<div class="card-header border mb-3 title-pembatas">'+
			'<div class="row">'+
			'<div class="col-7 col-md-8" id="pernyataan-title"><strong class="">'+"value_main.nama_aspek"+'</strong></div>';
			row +='<div id="head-pagination" class="col-5 col-md-4 text-right">';
				row += '<label>'+"Durasi Waktu:"+ " <span id='spam-timer'>00:00</span>" +'</label>';
			row += '</div>';
			row += '</div>'+
			'</div>'; 
			
		if(data.length != 0){
		var no=0;
		if($.urlParam('confirm')=="true"){
			$('#container-btn-end').removeClass("d-none");
			$('#container-btn-end').find('button').attr('onclick', 'renderDisplay(\''+1+'\',\'page_is='+"final"+'&\')');
		}
		$.each(data, function(key, value){
			jkom=value.jenis_kompetensi;
				row += '<div class="data-row card mb-3" id="row-'+value.id+'">';
				if(this.jenis_kompetensi=="MANAJERIAL" || this.jenis_kompetensi=="SOSIOKULTURAL"){
					row += //'<div class="card">'+
						'  <div class="card-body">'+
						'    <strong class="d-none">'+(key+1)+'.</strong>'+"Pilihlah salah satu pernyataan yang paling sesuai dengan diri anda!:"+' <button onclick="showRemarks(\''+value.keterangan+'\')" type="button" class="btn m-0 p-0"><i class="fas fa-question-circle text-info"></i></button>'+"<strong class='d-none'>("+value.aspek_id_sesi_pernyataan+")</strong>"+
						'  </div>'+
						'  <ul class="list-group list-group-flush">';
				}else{
					var newisi = $.trim(value.isiAndFilename).replace(/\n/g, '<br/>');
					row += //'<div class="card">'+
					'  <div class="card-body">'+
					'    <strong class="d-none">'+(key+1)+'.</strong>'+newisi+' <button onclick="showRemarks(\''+value.keterangan+'\')" type="button" class="btn m-0 p-0"><i class="fas fa-question-circle text-info"></i></button>'+"<strong class='d-none'>("+value.aspek_id_sesi_pernyataan+")</strong>"+
					'  </div>'+
					'  <ul class="list-group list-group-flush">';
				}
				
				if(value.userPertanyaanJawaban.length > 0){
					$.each(value.userPertanyaanJawaban, function(key, val_jawaban){
						row +='<li class="list-group-item list-group-item-action">';
						var cheked_radio="";
						var bgcolor="";
						if(val_jawaban.is_checked==1) {
							if($.urlParam('confirm')=="true"){
								//bgcolor="bg-primary"
							} 
							cheked_radio+=" checked ";
						}
						if(value.kunci_jawaban==this.order) {
							//bgcolor="bg-primary";
						}
						row += '<div class="form-check '+bgcolor+'">';
						if($.urlParam('confirm')=="true"){
							cheked_radio+=" disabled ";
						}
						cheked_radio+=' onclick="save(this)"';
						if((value.durasi-value.durasi_berjalan) > 1 ){
							row +='<input data-pertanyaan='+val_jawaban.id_header+' data-pernyataan='+value.id+' data-jawaban='+val_jawaban.id+' '+cheked_radio+' class="form-check-input" type="radio" name="'+value.id+'" id="radio-'+value.id+"-"+val_jawaban.id+'" value="'+val_jawaban.id+'">';
						}
						//when u see me
						var newisijawaban = $.trim(value.isiAndFilename).replace(/\n/g, '<br/>');
						/*if(value.jenis_kompetensi=="MANAJERIAL" || value.jenis_kompetensi=="SOSIOKULTURAL"){
//							row +='<label class="form-check-label" for="radio-'+value.id+"-"+val_jawaban.id+'">'+
//							'		    <strong>'+this.bobot+'-'+alpha[key]+'</strong>. '+this.isi+
//							'	   </label>'+
//							'</div>';
							row +='<label class="form-check-label" for="radio-'+value.id+"-"+val_jawaban.id+'">'+
							'		    <strong>'+alpha[key]+'</strong>. '+this.isiAndFilename+
							'	   </label>'+
							'</div>';
						}else{
						}*/
						row +='<label class="form-check-label" for="radio-'+value.id+"-"+val_jawaban.id+'">'+
						'		    <strong>'+alpha[key]+'</strong>. '+this.isiAndFilename+
						'	   </label>'+
						'</div>';
						row +='</li>';
						sessionStorage.setItem("status", value.jenis_kompetensi);
					});
				}else{
					row +='<li class="list-group-item text-danger">Jawaban belum ada, mohon laporkan pada admin.</li>';
				}
				
				'  </ul>';
				
				row += '</div>';
				
				count_pernyataan++;
				no++;
				//
				id_pertanyaan=this.id;
				kodean=this.kode;
				jkom=this.jenis_kompetensi;
				temp_timeou=this.durasi;
				
				sessionStorage.setItem("temp_timeou", this.durasi-parseInt(this.durasi_berjalan==null?0:this.durasi_berjalan));
				duration_actual = this.durasi;
		});
	}else{
		row += '<div class="mb-3 text-danger">Pertanyaan belum ada, mohon laporkan pada admin.</div>';
	}
		num++;
		row += '<div class="card-header border mb-3 title-pembatasa">'+
		'	    	<div class="row">'+
		'				<div id="footer-pagination" class="col-12 col-md-12 text-right">';
							var response_next_page_number=response.next_page_number;
							if(response.next_page_number > 2 || response.next_page_number > response.count){
								response_next_page_number='\''+(response.next_page_number-2)+'\',\''+'mark=1&'+'\'';
								//row += '<button onclick="renderDisplay('+response_next_page_number+')" type="button" class="btn m-0 p-0 float-left"><i class="fas fa-chevron-circle-left"></i> Prev</button>';
							}
							if(response.count==0){
								row += '<button onclick="saveSaran();" type="button" class="btn m-0 p-0 float-right"><i class="fas fa-chevron-circle-right"></i> Finish</button>';
							}else{
								response_next_page_number='\''+(response.next_page_number)+'\',\''+'mark=1&'+'\'';
								row += '<button onclick="submitPertanyaan('+response.next_page_number+')" type="button" class="btn m-0 p-0 float-right"><i class="fas fa-chevron-circle-right"></i> Next</button>';
							}
		row += '		</div>';
		row += '	</div>'+
		'		</div>';
	//});
	return row;
}

function render_rowside_card(response, num){
	var rowside="";
	$.each(response.data.dataref, function(key){
		var active="";active_jawab="";
		if((response.next_page_number-1)==this.no){
			active="bg-warning";
		}
		if(this.jawaban=="1"){
			active_jawab="bg-primary text-white";
		}
		rowside+='<div class="border '+active+' '+active_jawab+' col-md-2 col-2 p-1 m-1 text-center">'+(this.no)+'</div>';
	});
	return rowside;
}

function onGetListError(response) {
	var tbody = $("#table-card-body");
	tbody.html('<div class="loading-row list-group-item text-center">Jika pesan ini muncul, refresh browser anda, atau klik <a href="javascript:void(0)" onclick="window.location.reload()">disini</a> atau, coba restart browser anda.</div>');
}


function submitPertanyaan(next_now){
	if($('.form-check-input').length==0){
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		ajaxGET(url + 'update-status-soal?id='+id_pertanyaan+'&mark='+next_now,'onSubmitNowSucc','onSaveError');
	}else{
		swal.fire({
		 // title: response.responseJSON.code,
		  html: '<div class="text-center">'+"Anda yakin ingin lanjut ke soal berikutnya, anda tidak dapat kembali lagi jika melanjutkan?"+'</div>',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'YA',
		  cancelButtonText: 'TIDAK',
		  reverseButtons: true
		}).then((result) => {
		  if(result.value) {
			  clearTimeout(timeou);
			  clearTimeout(timenow);
			  $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
			  ajaxGET(url + 'update-status-soal?id='+id_pertanyaan+'&mark='+next_now,'onSubmitNowSucc','onSaveError');
		  } 
		});
	}
}

function onSubmitNowSucc(response){
	$.LoadingOverlay("hide");
	renderDisplay(1);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function counter(cnt){
 	cnt-= 1;
    //aktifkan lagi
    sessionStorage.setItem("temp_timeou", cnt);
    var minutes = parseInt(cnt / 60, 10)
    var seconds = parseInt(cnt % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    jQuery("#spam-timer").html("<strong>"+minutes+":"+seconds+"</strong>");
   
    if(cnt>0){
    	timeou=setTimeout(counter,1000,cnt);
    }else{
		$('#spam-timer').html("<strong>Habis</strong>");
		$('.form-check-input').remove();
		var next_now=parseInt($('.bg-warning').text())+1;
		
		//ajaxGET(url + 'update-durasi-soal?id='+id_pertanyaan+'&durasi='+sessionStorage.getItem("temp_timeou")+'&mark='+next_now,'onUpdateSucc','onSaveError');
		const duration = sessionStorage.getItem("temp_timeou");
	    const id = id_pertanyaan;
		const mark = next_now;
	    if (duration && socket.readyState === WebSocket.OPEN) {
	        const message = JSON.stringify({ id: id, duration: duration, duration_actual: duration_actual, flag: mark});
	        socket.send(message);
	        //console.log('Data sent:', message);
	    }
		clearInterval(timenow);
		//var row = '<button onclick="renderDisplay(1)" type="button" class="btn m-0 p-0 float-right"><i class="fas fa-chevron-circle-right"></i> Next</button>';
		//$('#footer-pagination').html(row);
    }
}



function onUpdateSucc(response){
	/*var value = response.data;
	console.log(value);
	console.log(value.durasi_berjalan > value.durasi || value.durasi_berjalan == value.durasi);
	if(value.durasi_berjalan > value.durasi || value.durasi_berjalan == value.durasi){
		clearTimeout(counter);
	}*/
}

function save(elem){
	var obj = new FormData();
	if(selected_id != '') obj.append('header_responden', selected_id);
	obj.append('id_pertanyaan', $(elem).data('pernyataan'));
	obj.append('id_jawaban',  $(elem).data('jawaban'));
	mode='progress_to_answer';
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    ajaxPOST(url + 'jawab',obj,'onSaveSuccess','onSaveError');
	//public_jawaban['header_responden'] = selected_id;
	//public_jawaban['id_pertanyaan'] = $(elem).data('pernyataan');
	//public_jawaban['id_jawaban'] = $(elem).data('jawaban');
}
function onSaveSuccess(response){
	$.LoadingOverlay("hide");
	toPass=true;
	//refresh_label
	var count_pernyataan=$('#table-card-body').find('.data-row').length;
	var count_pernyataan_answer=$('#table-card-body').find('input:checked').length;
	$('#pernyataan-titles').html('Terjawab <strong class="text-success">'+count_pernyataan_answer+'</strong> dari <strong class="text-danger">'+count_pernyataan+'</strong> Pernyataan  ');	
}

function saveSaran(){
	var obj = new FormData();
	if(selected_id != '') obj.append('id', selected_id);
	Swal.fire({
	  title: 'Mohon Tunggu',
	  allowEscapeKey: false,
	  allowOutsideClick: false,
	  showCancelButton: false,
	  showConfirmButton: false,
	  html: 'Jawaban disimpan.. <b></b> .',
	  timer: 30000,
	  timerProgressBar: true,
	  willOpen: () => {
	    Swal.showLoading()
	    timerInterval = setInterval(() => {
	      const content = Swal.getContent()
	      if (content) {
	        const b = content.querySelector('b')
	        if (b) {
	          b.textContent = Swal.getTimerLeft()
	        }
	      }
	    }, 100)
	    ajaxPOST(url + 'save',obj,'onSaveSaranSuccess','onSaveError');
	  },
	  willClose: () => {
	    clearInterval(timerInterval)
	  }
	}).then((result) => {
	  // Read more about handling dismissals below 
	  if(result.dismiss === Swal.DismissReason.timer) {
		cek_status_responden();
		Swal.fire({
		  icon: 'error',
		  //title: 'Oops...',
		  html: '<p class="text-left">Waktu load terlalu lama, mohon coba beberapa saat lagi, dan pastikan internet anda dalam keadaan stabil.</p>'+
			  '<div class="text-left"><strong>Terima Kasih</strong></div>',
		})
	  }
	})
}

function onSaveSaranSuccess(response){
	//clearTimeout(counter);
	toPass=true;
	//document.cookie = "cnt="+ cnt;
	console.log(response);

	window.location.reload();
	//refresh_label
	var count_pernyataan=$('#table-card-body').find('.data-row').length;
	var count_pernyataan_answer=$('#table-card-body').find('input:checked').length;
	$('#pernyataan-titles').html('Terjawab <strong class="text-success">'+count_pernyataan_answer+'</strong> dari <strong class="text-danger">'+count_pernyataan+'</strong> Pernyataan  ');	
}

function onSaveError(response){
	$.LoadingOverlay("hide");
//	Swal.fire({
//	  icon: 'error',
//	  //title: 'Oops...',
//	  html: '<p class="text-left">Waktu load terlalu lama, mohon coba beberapa saat lagi, dan pastikan internet anda dalam keadaan stabil.</p>'+
//		  '<div class="text-left"><strong>Terima Kasih</strong></div>',
//	})
	console.log(response);
	if(response.responseJSON==undefined){
		console.log("try logout");
		window.location.href=ctx+"/login-logout?connection";
	}else{
		if(response.responseJSON.code==401){
			window.location.href=ctx+"/login-logout?duplicate";
		}else if(response.responseJSON.code==406){
			swal.fire({
			  //title: response.responseJSON.code,
			  html: '<div class="text-center">'+response.responseJSON.message+'</div>',
			  icon: 'warning',
			  showCancelButton: false,
			  confirmButtonText: 'OK',
			  cancelButtonText: 'Review',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {
					
			  } 
			});
		}else{
			window.location.href=ctx+"/login-logout?connection";
		}
	}
}

function showRemarks(html=""){
	$('#exampleModalCenterTitle').html("Keterangan")
	$('#exampleModalCenter').modal('show');
	html="<p class='card-text'>"+html+"</p>"
	$('#modal-content').html(html);
	html='<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
	$('#modal-action').html(html);
}
function showConfirm(html=""){
	$('#exampleModalCenterTitle').html("Konfirmasi")
	//$('#exampleModalCenter').modal('show');
	html="<p class='card-text'>"+html+"</p>"
	$('#modal-content').html(html);
	html='<button type="button" class="btn btn-secondary" data-dismiss="modal">Review</button>';
	html+='<button type="button" class="btn btn-success" data-dismiss="modal">Lanjut</button>';
	$('#modal-action').html(html);
	$('#exampleModalCenter .close').addClass('d-none');
	//dismissible:false
	$('#exampleModalCenter').modal({backdrop: 'static', keyboard: false})
}
