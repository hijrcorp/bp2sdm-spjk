var url = ctx + "/simulasi-survey/";
var url_responden = ctx + "/responden/";
var url_responden_jawaban = ctx + "/responden-jawaban/";
var urlExport = ctx +"/rest/export/";
var param = "";
var selected_id='';
var selected_id_responden_jawaban;
var mode='';
var judul_sesi='';
var timean="";
var kodean="";
var toPass=false;
var timeou;

function init() {
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
	selected_page = ($.urlParam('page') == null ? '' : $.urlParam('page'));
	kegiatan_pengawasan_id = ($.urlParam('kegiatan_pengawasan_id') == null ? '' : $.urlParam('kegiatan_pengawasan_id'));
	
	cek_status_responden();
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#form-surat-tugas').submit(function(e){
		save();
		e.preventDefault();
	});
	$('.navbar-toggler ').addClass('d-none');
	
	/*$('#modal-form').modal({
	  keyboard: false
	})*/
	$('#modal-form').on('hidden.bs.modal', function (e) {
		//cek_status_responden();
	})
}
function cek_status_responden(){
	ajaxGET(url +"check-responden/"+ selected_id,'onCheckSuccess','onCheckError');
}

function onCheckSuccess(response){
	var tempPage=getCookie("page");
	if(tempPage=="")tempPage=1;

	if($.urlParam('confirm')=="true") tempPage=1;
		//document.cookie = "page="+ 1;	
	console.log(response);
	var value = response.data;
	//judul_sesi=value.sesi.keterangan_sesi;
	if(value.status=="SELESAI") {
		document.cookie = "cnt="+ 0;
		document.cookie = "page="+ tempPage;
		//if(value.periode_awal_expired < 0 && value.periode_akhir_expired < 0 || value.status_responden=="SELESAI")
		window.location.href="index";
	}
	else {
		if(mode!='progress_to_answer')
		renderDisplay(tempPage);
	}
}

function save(elem){
//	var obj = new FormData(document.querySelector('#form-surat-tugas'));
	var obj = new FormData();
	if(selected_id != '') obj.append('header_responden', selected_id);
	obj.append('id_pertanyaan', $(elem).data('pernyataan'));
	obj.append('id_jawaban',  $(elem).data('jawaban'));
//  console.log(obj);
	mode='progress_to_answer';
    ajaxPOST(url + 'jawab',obj,'onSaveSuccess','onSaveError');
}

function saveSaran(){
	var obj = new FormData();
	if(selected_id != '') obj.append('id', selected_id);
	obj.append('saran', $('[name=isi]').val());
    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	//clearTimeout(counter);
	toPass=true;
	//document.cookie = "cnt="+ cnt;
	console.log(response);
//	$("#progressBar").hide();
//	$("#modalPengguna").modal('hide');
//	refresh();
//	reset();
//	window.location.href="survey?id="+response.data.id_responden;
	cek_status_responden();
	
	//refresh_label
	var count_pernyataan=$('#table-card-body').find('.data-row').length;
	var count_pernyataan_answer=$('#table-card-body').find('input:checked').length;
	$('#pernyataan-titles').html('Terjawab <strong class="text-success">'+count_pernyataan_answer+'</strong> dari <strong class="text-danger">'+count_pernyataan+'</strong> Pernyataan  ');	
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(page, '', 0);
}

//batas
function setLoad(elem){
	elem.html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	if(page > 1 && getCookie("cnt")>0 && $('.data-row').find('input').is(':checked')==false){
		Swal.fire({
			  type: 'warning',
			  //title: 'Oops...',
			  text: 'Maaf anda tidak dapat lanjut, sebelum anda mengisi semua jawaban anda.',
			});
		return;
	}else{
		$('#modal-form').modal('hide');
	}

	if(toPass || $('.data-row').find('input').is(':checked')==true) {
		clearTimeout(timeou);
		document.cookie = "cnt="+ 0;
		document.cookie = "page="+ page;
		
	}
	var tbody = $("#table-card-body");
	setLoad(tbody);
	params+="header_responden="+selected_id+"&filter_status=1";
	limit=1;
	if($.urlParam('confirm')=="true") limit="";
	if($.urlParam('confirm')=="finish") page=selected_page;
	
	setTimeout(() => ajaxGET(url + '?page='+page+'&limit='+limit+'&'+params,'onGetListSuccess','onGetListError'), timeout);
}

function onGetListSuccess(response){
	var tbody = $("#table-card-body");
	var tfooter = $("#head-pagination");
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = response.next_page_number-1;
	
	row += render_row_card(response, num);
	
	if(response.next_more==false && $.urlParam('confirm')==null){
		const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-success',
		    cancelButton: 'btn btn-secondary'
		  },
		  buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
		  title: 'Konfirmasi',
		  html: "Mohon <strong>review</strong> kembali jawaban anda, sebelum lanjut.",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Lanjut',
		  cancelButtonText: 'Review',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
			window.location.href=ctx+'/page/survey?id='+selected_id+'&confirm=finish';
		  } else if (result.dismiss === Swal.DismissReason.cancel) {
			window.location.href=ctx+'/page/survey?id='+selected_id+'&confirm=true';
		  }
		});
		
		row='Mohon tunggu...';
		$('#modal-action').find('.btn-secondary').attr('onclick','window.location.href="'+ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=true&page='+(response.next_page_number-1)+'"');
		$('#modal-action').find('.btn-success').attr('onclick','window.location.href="'+ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=finish&page='+(response.next_page_number)+'"');
		clearTimeout(timeou);
		document.cookie = "cnt="+ 0;
	}
	else if($.urlParam('confirm')=="true" && response.next_more==false && response.next_page_number==0 || $.urlParam('confirm')=="finish")
	{
		$('#pernyataan-title').html("<strong>Saran  </strong>");
		row = '<div class="card-header border mb-3 ">	    	<div class="row">			    <div class="col col-md-12" id="pernyataan-title"><strong>Saran  </strong></div>	</div>		</div>';
		row+='<textarea name="isi" class="form-control mb-3" placeholder="Beritahu kami saran dan masukan anda terhadap survey yang dilakukan, agar survey ini dapat menjadi lebih baik. (Opsional)" rows="6"></textarea>';
		row+='<br / > * Harap menekan tombol selesai di bawah ini untuk menyelesaikan survey.';
		$('#info-answers').addClass('d-none');
		$('#container-btn-end').find('button').attr('onclick', 'saveSaran()');
		$('#container-btn-end').find('button').text('Selesai');
	}
	
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);

	myvar = '';
	if(response.next_page_number > 2 || response.next_page_number > response.count)
	myvar += '<button onclick="renderDisplay('+(response.next_page_number-2)+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>';
	
	if(response.next_more==false && (response.next_page_number-response.count)==2){
		myvar += '<button onclick="saveSaran()" type="button" class="btn m-0 p-0"><i class="fas fa-check-circle text-success"></i></button>';
	}else{
		myvar += '<button onclick="renderDisplay('+response.next_page_number+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>';
	}
	tfooter.html(myvar);

	var count_pernyataan=$('#table-card-body').find('.data-row').length;
	var count_pernyataan_answer=$('#table-card-body').find('input:checked').length;
	$('#pernyataan-titles').html('Terjawab <strong class="text-success">'+count_pernyataan_answer+'</strong> dari <strong class="text-danger">'+count_pernyataan+'</strong> Pernyataan  ');	
	if($.urlParam('confirm')=="true") $('.title-pembatasa').remove();
	$('#pernyataan-title').html("<strong>Kompetensi:</strong> "+jkom);
	
	//set countdown timer
	if($.urlParam('confirm')==null && response.next_more==true) {
		timeou=counter(timean, response.next_page_number-1);
	}
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function counter(cnt, nextPage){
    if(getCookie("cnt") > 0){
        cnt = getCookie("cnt");
        nextPage = getCookie("page");
    }
    cnt -= 1;
    document.cookie = "cnt="+ cnt;
    document.cookie = "page="+ nextPage;
    var minutes = parseInt(getCookie("cnt") / 60, 10)
    var seconds = parseInt(getCookie("cnt") % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    jQuery("#spam-timer").text(minutes+":"+seconds);
    console.log("?");
    
    if(cnt>0){
    	timeou=setTimeout(counter,1000,cnt, nextPage);
    }
    if(getCookie("cnt")==0){

        document.cookie = "page="+ (parseInt(nextPage)+1);
        toPass=true;
		$('#kode_pertanyaan').text(kodean);
		$('#modal-form').modal('show');
		
		$('#btn-mulai').attr('onclick', 'renderDisplay('+(parseInt(nextPage)+1)+')');
	

		//clearTimeout(timeou);
    }
}
function render_row_card(response, num, method='add'){
	var alpha = ["a","b","c","d"];
	data=response.data;
	console.log(data);
	var row = "";
	var count_pernyataan=0;
	if($.urlParam('confirm')=="true") {
		row += '<div class="card-header bg-warning text-white border mb-3 ">'+
	'	    	<div class="row">'+
'			    <div class="col col-md-12" id="pernyataan-title">Mohon <strong>Konfirmasi</strong> kembali jawaban anda sebelum lanjut</div>';
	row += '	</div>'+
		'		</div>';
	}

	if($.urlParam('confirm')=="finish"){
		$('#container-btn-end').removeClass("d-none");
		$('#container-btn-end').find('button').attr('onclick', 'renderDisplay('+(parseInt($.urlParam('page'))+1)+')');
	}

	row += '<div class="row ">'+
	'    <div class="col-md-12 text-center">'+
	'       <h5>'+"Durasi Waktu:"+ " <span id='spam-timer'>00:00</span>" +'</h5>'+
	'		<hr class="mb-2 mt-2">'+
	'	</div>'+
	'</div>';
	
	/*$.each(data.main,function(key,value_main){*/

			row += '<div class="card-header border mb-3 title-pembatas">'+

			'	    	<div class="row">'+
			'			    <div class="col-10 col-md-11" id="pernyataan-title"><strong class="">'+"value_main.nama_aspek"+'</strong></div>';
			
			row +='<div id="head-pagination" class="col-2 col-md-1 text-right">'

			if(response.next_page_number > 2 || response.next_page_number > response.count){
				row += '<button onclick="renderDisplay(\''+(response.next_page_number-2)+'\',\''+'page_is=back&'+'\')" type="button" class="btn m-0 p-0 d-none"><i class="fas fa-chevron-circle-left"></i></button>';
			}
			if($.urlParam('confirm')!="true") {
				var response_next_page_number=response.next_page_number;
				if(response.next_more==false)response_next_page_number='\''+(response.next_page_number)+'\',\''+'page_is=confirm&'+'\'';
				row += '<button onclick="renderDisplay('+response_next_page_number+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>';
			}
			row += '	</div>';
			row += '	</div>'+
			'			</div>'; 
			
		if(data.length != 0){
		var no=0;
		if($.urlParam('confirm')=="true"){
			$('#container-btn-end').removeClass("d-none");
			$('#container-btn-end').find('button').attr('onclick', 'renderDisplay(\''+1+'\',\'page_is='+"final"+'&\')');
		}
		$.each(data, function(key, value){
			jkom=value.jenis_kompetensi;
			//row +='<h5 class="card-title">'+value.jenis_kompetensi+'</h5>';
				if(method != 'replace') {
					row += '<div class="data-row card mb-3" id="row-'+value.id+'">';
				}
				row += //'<div class="card">'+
				'  <div class="card-header">'+
				//+((key+1))+'.'//value.order
				'    <strong>'+(key+1)+'</strong>.'+value.isi+' <button onclick="showRemarks(\''+value.keterangan+'\')" type="button" class="btn m-0 p-0"><i class="fas fa-question-circle text-info"></i></button>'+"<strong class='d-none'>("+value.aspek_id_sesi_pernyataan+")</strong>"+
				'  </div>'+
				'  <ul class="list-group list-group-flush">';
				
				if(value.userPertanyaanJawaban.length > 0){
					$.each(value.userPertanyaanJawaban, function(key, val_jawaban){
						row +='<li class="list-group-item list-group-item-action">';
						var cheked_radio="";
						var bgcolor="";
						if(val_jawaban.is_checked==1) {
							if($.urlParam('confirm')=="true"){
								bgcolor="bg-primary"
							} 
							cheked_radio+=" checked ";
						}
						row += '<div class="form-check '+bgcolor+'">';
						if($.urlParam('confirm')=="true"){
							cheked_radio+=" disabled ";
						}
						cheked_radio+=' onclick="save(this)"';
					
						row +='<input data-pertanyaan='+val_jawaban.id_header+' data-pernyataan='+value.id+' data-jawaban='+val_jawaban.id+' '+cheked_radio+' class="form-check-input" type="radio" name="'+value.id+'" id="radio-'+value.id+"-"+val_jawaban.id+'" value="'+val_jawaban.id+'">';
						
						row +='<label class="form-check-label" for="radio-'+value.id+"-"+val_jawaban.id+'">'+
						'		    <strong>'+alpha[this.order-1]+'</strong>. '+this.isi+
						'	   </label>'+
						'</div>';
						row +='</li>';
					});
				}else{
					row +='<li class="list-group-item text-danger">Jawaban belum ada, mohon laporkan pada admin.</li>';
				}
				
				'  </ul>';
				
				if(method != 'replace') row += '</div>';
				count_pernyataan++;
				no++;
				//
				timean=this.durasi;
				kodean=this.kode;
		})
	}else{
		row += '<div class="mb-3 text-danger">Pertanyaan belum ada, mohon laporkan pada admin.</div>';
	}
		num++;
		row += '<div class="card-header border mb-3 title-pembatasa">'+
		'	    	<div class="row">'+
//		'			    <div class="col-8 col-md-6" id="pernyataan-title">Pernyataan : <strong>'+value_main.nama_aspek+'</strong></div>';
		'				<div id="head-pagination" class="col-12 col-md-12 text-right">';
							if(response.next_page_number > 2 || response.next_page_number > response.count){
								row += '<button onclick="renderDisplay(\''+(response.next_page_number-2)+'\',\''+'page_is=back&'+'\')" type="button" class="btn m-0 p-0 float-left d-none"><i class="fas fa-chevron-circle-left"></i> Prev</button>';
							}
							if($.urlParam('confirm')!="true") {
								var response_next_page_number=response.next_page_number;
								if(response.next_more==false)response_next_page_number='\''+(response.next_page_number)+'\',\''+'page_is=confirm&'+'\'';
								row += '<button onclick="renderDisplay('+response_next_page_number+')" type="button" class="btn m-0 p-0 float-right"><i class="fas fa-chevron-circle-right"></i> Next</button>';
							}
		row += '		</div>';
		row += '	</div>'+
		'		</div>';
	//});
	return row;
}

function showRemarks(html=""){
	$('#exampleModalCenterTitle').html("Keterangan")
	$('#exampleModalCenter').modal('show');
	html="<p class='card-text'>"+html+"</p>"
	$('#modal-content').html(html);
	html='<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
	$('#modal-action').html(html);
}
function showConfirm(html=""){
	$('#exampleModalCenterTitle').html("Konfirmasi")
	//$('#exampleModalCenter').modal('show');
	html="<p class='card-text'>"+html+"</p>"
	$('#modal-content').html(html);
	html='<button type="button" class="btn btn-secondary" data-dismiss="modal">Review</button>';
	html+='<button type="button" class="btn btn-success" data-dismiss="modal">Lanjut</button>';
	$('#modal-action').html(html);
	$('#exampleModalCenter .close').addClass('d-none');
	//dismissible:false
	$('#exampleModalCenter').modal({backdrop: 'static', keyboard: false})
}
