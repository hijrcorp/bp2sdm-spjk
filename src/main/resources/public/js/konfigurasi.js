var path = ctx + '/restql';
function initTinyMce(){
    tinymce.init({
      height : "450px",
      selector: '#value',
//      plugins: 'code', //'image code'
      toolbar: 'undo redo | code',//'undo redo | link image | code'
      /* enable title field in the Image dialog*/
      image_title: true,
      /* enable automatic uploads of images represented by blob or data URIs*/
      automatic_uploads: true,
      /*
        URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: 'postAcceptor.php',
        here we add custom filepicker only to Image dialog
      */
      file_picker_types: 'image',
      /* and here's our custom image picker*/
      file_picker_callback: function (cb, value, meta) {
    	  
        var input = document.createElement('input');
        input.setAttribute('name', 'lampiran[]');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        /*
          Note: In modern browsers input[type="file"] is functional without
          even adding it to the DOM, but that might not be the case in some older
          or quirky browsers like IE, so you might want to add it to the DOM
          just in case, and visually hide it. And do not forget do remove it
          once you do not need it anymore.
        */

        input.onchange = function () {
          var file = this.files[0];

          var reader = new FileReader();
          reader.onload = function () {
            /*
              Note: Now we need to register the blob in TinyMCEs image blob
              registry. In the next release this part hopefully won't be
              necessary, as we are looking to handle it internally.
            */
            var id = 'blobid' + (new Date()).getTime();
            var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            /* call the callback and populate the Title field with the file name */
            cb(blobInfo.blobUri(), { title: file.name});
          };
          reader.readAsDataURL(file);
          
          input.setAttribute('data-name', file.name);
        };

        input.click();
        $('#form-file').append(input);
      }
    });
}
function init(){
	initTinyMce();
	display();

	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-reset').click(function(e){
		clearForm();
		$("#search-form").trigger('reset');
	});
	
	$("#entry-form").submit(function() {
		save();
		return false;
	});
	

	$('[name=filter_id_pegawai]').on('change', function(){
		$('[name=filter_id_akun_bank]').empty();
		$('[name=filter_id_akun_bank]').append(new Option('Semua Akun Bank', ''));

		var params='filter=';
		params+='id_pegawai.eq \''+$('[name=filter_id_pegawai]').val()+'\';';
		ajaxGET(path + '/akun_bank'+'?'+params,'onGetSelectFilterAkunBank','onGetSelectError');
	});
	$('#search-form').submit(function(e){
		display(1, 'filter=');
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('.single-date-picker').on('change', function(){
         var dateFirst = new Date($('[name=tanggal_mulai]').val());//"11/25/2017"
         var dateSecond = new Date($('[name=tanggal_selesai]').val());//"11/28/2017"
         // time difference
         var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());
         // days difference
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
         $('#jumlah_cuti').text(diffDays+" Hari");
         $('[name=jumlah_cuti]').val(diffDays+" Hari"); 
	});

	$('[name=type]').on('change', function(){
		if(this.value=="SINGLE"){
			tinymce.remove('#value');
		}else{
			initTinyMce();
		}
	});
}

function onGetSelectFilterAkunBank(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_akun_bank]').append(new Option(value.nama_bank+'-'+value.nomor_rekening, value.id));
	});
}


function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/config/save';
	if($('[name=id]').val() != "") url = path+'/config/save?append=false';
	var new_s="";
	if(tinymce.get("value")!=null){
		var htmlObject = $.parseHTML(tinymce.get("value").getContent());
		$.each(htmlObject, function(key){
			$.each($(this).find('img'), function(key_2){
				console.log(this);
				this.src=this.title;
			})
			if(this.outerHTML != undefined)
			new_s+=this.outerHTML;
		});
	}else{
		new_s+=$('[name=value]').val();
	}

	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray()),
			dataMain : {
				"column" : [
					$('[name=code]').val(), new_s
				]
			}
		};
	obj.data.value=new_s;
	
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form').modal('hide');
        		display();
        },
        error : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        		$('#modal-form-msg').show();
        },
    });
    
}
function getSelectJenisCuti(){
	$('[name=id_jenis]').empty();
	$('[name=id_jenis]').append(new Option('Pilih Jenis Cuti', ''));
	//
	ajaxGET(path + '/cuti_pengaturan','onGetSelectJenisCuti','onGetSelectError');
}
function onGetSelectJenisCuti(response){
	$.each(response.data, function(key, value) {
		$('[name=id_jenis]').append(new Option(value.nama, value.id));
	});
}

function display(page=1, params=''){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');
	}
	
	var clause=[];
	//if($('[name=filter_nama]').val() != ''){
		params+='id_pegawai.slike \''+$('[name=filter_id_pegawai]').val()+'\',';
	//}
	//if($('[name=filter_jenis]').val() != ''){
		params+='id_akun_bank.slike \''+$('[name=filter_id_akun_bank]').val()+'\';';
	//}
	if($('[name=filter_id_pegawai]').val() == '' && $('[name=filter_id_akun_bank]').val() == ''){
		params='';
	}
	// "%'+$('[name=filter]').val()+'%"
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//console.log(path + '/list?page='+page+'&'+params);
	var qry="join=pegawai,akun_bank";
	qry="";
	ajaxGET(path + '/config?page='+page+'&'+params+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += '<tr class="data-row" id="row-'+value.id+'">';
			row += '<td>'+(num)+'</td>';
			row += '<td> '+'<i class="fas fa-fw '+(value.status==1?"fa-check-circle text-success":"fa-times-circle text-danger")+'"></i> ' +value.code + ' </td>';
			row += '<td> '+value.value +' </td>';
			row += '<td class="text-center">';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
			row += '</td>';
		row += '</tr>';
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		//var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		var query=select+"&fetch=penggajian_detil.select(id,id_penggajian,jenis,komponen,jumlah,keterangan)";//akun_bank.limit(1)
		query=""
		ajaxGET(path + '/config/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		var cascade="";
		//if(selected_action=="delete")  cascade="&cascade=penggajian_detil";
		var query="";
		ajaxPOST(path + '/config/'+id+'/'+selected_action+'?'+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'update-true' || selected_action == 'update-false'){
		if(selected_action=="update-true")
			updateStatus(0);
		else if(selected_action=="update-false")
			updateStatus(1);
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('.modal table').find('tbody').html('');
//	$('[name=filter_id_pegawai]').val('').trigger('change');
//	$('[name=filter_id_akun_bank]').val('').trigger('change');
	if($('#type2').is(':checked')){
		initTinyMce();
		tinymce.activeEditor.setContent("");
		$('#value').html("");
	}else {
		tinymce.remove('#value');
		$('#value').val("");
	}
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=code]').val(value.code);
	if(value.type=="MULTIPLE"){
		$('#' + "type2").prop('checked',true);
		$('#' + "type2").trigger('change');
		initTinyMce();
		tinymce.activeEditor.setContent(value.value);
		$('#value').html(value.value);
	}else {
		$('#' + "type1").prop('checked',true);
		tinymce.remove('#value');
		$('#value').val(value.value);
	}
	$('input:radio[name=value]').val([value.type]);
	
	$('[name=nama]').val(value.nama);

	if(value.status=="1"){
		$('#' + "status1").prop('checked',true);
	}else {
		$('#' + "status2").prop('checked',true);
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display();
	
	// Update data ke existing tabel
	/*if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}*/
	
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


