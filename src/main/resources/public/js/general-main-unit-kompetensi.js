var url = ctx + "/simulasi-survey/";

var param = "";
var selected_id='';

function init() {
	$('#btn-mulai').click(function(){
  		var obj = new FormData(document.forms['form-inti']); // with the file input
  		var obj2 = jQuery(document.forms['form-pilihan']).serializeArray();
  		for (var i=0; i<obj2.length; i++)
  			obj.append(obj2[i].name, obj2[i].value);
  		obj.append("id_responden", responden_id);
  		obj.append("jumlah_kompetensi_pilihan", $("[name=teknis_pilihan]").length);
  		if($(".form-check-input:checked").length==0){
  			return alert("Kompetensi belum dipilih");
  		}
  		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
  	    ajaxPOST(url + 'mulai',obj,'onSaveSuccess','onSaveError');
	});
}

function updateStatusResponden(id){
	window.location.href=ctx+"/page/simulasi-ujian";
	//ajaxGET(url + 'update-status-responden?id='+id,'onSaveSuccess','onSaveError');
}

function myghty(e){
	e.checked=true;
}

function onSaveSuccess(response){
	window.location.href=ctx+"/page/simulasi-ujian";
}

function onSaveError(response){
	console.log(response);
	if(response.responseJSON==undefined){
		console.log("try logout");
		window.location.href=ctx+"/login-logout?connection";
	}else{
		if(response.responseJSON.code==401){
			window.location.href=ctx+"/login-logout?duplicate";
		}else{
			$.LoadingOverlay("hide");
			swal.fire({
			  //title: response.responseJSON.code,
			  html: '<div class="text-center">'+response.responseJSON.message+'</div>',
			  icon: 'warning',
			  showCancelButton: false,
			  confirmButtonText: 'Lanjut',
			  cancelButtonText: 'Review',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {
					
			  } 
			});
		}
	}
}

//function mulai(mode=0){
//	var obj = new FormData(document.forms['form-inti']); // with the file input
//	var obj2 = jQuery(document.forms['form-pilihan']).serializeArray();
//	for (var i=0; i<obj2.length; i++)
//		obj.append(obj2[i].name, obj2[i].value);
//	obj.append('id_sesi', sesi_id);
//	if(responden_id!="") obj.append('id_responden', responden_id);
//	obj.append("tambah_mode", mode);
//	var func;
//	if(mode==0) func="onCheckMulai";
//	else mode=func="onSaveSuccess";
//    ajaxPOST(url + 'mulai',obj,func,'onSaveError');
//}
//
//function onCheckMulai(resp){
//	const swalWithBootstrapButtons = Swal.mixin({
//		  customClass: {
//		    confirmButton: 'btn btn-success',
//		    cancelButton: 'btn btn-secondary'
//		  },
//		  buttonsStyling: false
//		})
//
//		swalWithBootstrapButtons.fire({
//		  title: 'Tata Cara Ujian',
//		  html: resp.message,
//		  footer: resp.data.estimasi,
//		  type: 'info',
//		  showCancelButton: true,
//		  allowOutsideClick: false,
//		  confirmButtonText: 'Mulai Ujian',
//		  cancelButtonText: 'Batal',
//		  reverseButtons: true
//		}).then((result) => {
//		  if (result.value) {
//			  mulai(1);
//		  } else if (result.dismiss === Swal.DismissReason.cancel) {
//		  }
//		});
//}


function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}

function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

