var url = ctx + "/simulasi-survey/";
var pathsql = ctx + '/restql';

var param = "";
var selected_id='';

function init() {
	$('#form-entri').submit(function(e){
		var obj = new FormData(document.querySelector('#form-entri'));
  		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
		e.preventDefault();
	});

	$('[name=id_unit_kerja]').on('change', function() {
	    var selectedOption = $(this).find(':selected');
	    var id_propinsi = selectedOption.data('id_propinsi');
	    var nama_propinsi = selectedOption.data('nama_propinsi');
	    $("[name=id_propinsi]").append(new Option(nama_propinsi, id_propinsi));
	});
	getSelectGolongan();
}

function getSelectJabatan(ele){
	var id=$('[name=id_bidang_teknis] :selected').data('idjabatan');
	$('[name=id_jabatan]').empty();
	$('[name=id_jabatan]').append(new Option('Pilih Jenjang', ''));

	var qry="join=kelompok_jabatan,jenjang_jabatan,tingkat_jabatan";
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"'),id_jenis_jabatan.is(NULL)&limit=100000";
//	if(id==undefined) params="";
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(pathsql + '/jabatan?'+params+'&'+qry,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.LoadingOverlay("hide");
	$.each(response.data, function(key, value) {
		$('[name=id_jabatan]').append(new Option(value.tingkat_jabatan.nama+" "+value.jenjang_jabatan.nama, value.id));
	});
}

function getSelectGolongan(){
	$('[name=golongan]').empty();
	$('[name=golongan]').append(new Option('Pilih Gol', ''));

	var qry="join=kelompok_jabatan,jenjang_jabatan,tingkat_jabatan";
	var page=1,params="filter=id_kelompok_jabatan.eq('"+1+"')";
	params="", qry="";
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(pathsql + '/golongan?'+params+'&'+qry,'onGetSelectGolongan','onGetSelectError');
}
function onGetSelectGolongan(response){
	$.LoadingOverlay("hide");
	$.each(response.data, function(key, value) {
		$('[name=golongan]').append(new Option(value.nama, value.nama));
	});
}

function onSaveSuccess(response){
	window.location.href=ctx+"/page/main-unit-kompetensi";
}

function onSaveError(response){
	console.log(response);
	
	if(response.responseJSON==undefined){
		console.log("try logout");
		window.location.href=ctx+"/login-logout?connection";
	}else{
		if(response.responseJSON.code==401){
			window.location.href=ctx+"/login-logout?duplicate";
		}else{

			$.LoadingOverlay("hide");
			swal.fire({
			  title: "Mohon Maaf..",
			  html: '<div class="text-center">'+response.responseJSON.message+'</div>',
			  icon: 'warning',
			  showCancelButton: false,
			  confirmButtonText: 'OK',
			  cancelButtonText: 'Review',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {
					
			  } 
			});
		}
	}
}

//function mulai(mode=0){
//	var obj = new FormData(document.forms['form-inti']); // with the file input
//	var obj2 = jQuery(document.forms['form-pilihan']).serializeArray();
//	for (var i=0; i<obj2.length; i++)
//		obj.append(obj2[i].name, obj2[i].value);
//	obj.append('id_sesi', sesi_id);
//	if(responden_id!="") obj.append('id_responden', responden_id);
//	obj.append("tambah_mode", mode);
//	var func;
//	if(mode==0) func="onCheckMulai";
//	else mode=func="onSaveSuccess";
//    ajaxPOST(url + 'mulai',obj,func,'onSaveError');
//}
//
//function onCheckMulai(resp){
//	const swalWithBootstrapButtons = Swal.mixin({
//		  customClass: {
//		    confirmButton: 'btn btn-success',
//		    cancelButton: 'btn btn-secondary'
//		  },
//		  buttonsStyling: false
//		})
//
//		swalWithBootstrapButtons.fire({
//		  title: 'Tata Cara Ujian',
//		  html: resp.message,
//		  footer: resp.data.estimasi,
//		  type: 'info',
//		  showCancelButton: true,
//		  allowOutsideClick: false,
//		  confirmButtonText: 'Mulai Ujian',
//		  cancelButtonText: 'Batal',
//		  reverseButtons: true
//		}).then((result) => {
//		  if (result.value) {
//			  mulai(1);
//		  } else if (result.dismiss === Swal.DismissReason.cancel) {
//		  }
//		});
//}


function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}

function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function getSatker(){
	var id=$('[name=id_eselon1]').val();
	$('[name=id_unit_kerja]').empty();
	$('[name=id_unit_kerja]').append(new Option('Pilih Satker', ''));

	var qry="join=eselon1,propinsi";
	var page=1,params="filter=id_eselon1.eq('"+id+"')&limit=100000";
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(pathsql + '/unit_auditi?'+params+'&'+qry,'onGetSelectSatker','onGetSelectError');
}

function onGetSelectSatker(response){
	$.LoadingOverlay("hide");
	$.each(response.data, function(key, value) {
		var option = new Option(value.nama, value.id);
	    $(option).attr('data-id_propinsi', value.propinsi.id); 
	    $(option).attr('data-nama_propinsi', value.propinsi.nama); 
		$('[name=id_unit_kerja]').append(option);
	});
}
