var path = ctx + '/restql';

var param = "";
var selected_id='';
var selected_action='';

var appendIds = [];

const Toast = Swal.mixin({
toast: true,
  position: 'top-right',
  iconColor: 'white',
  customClass: {
    popup: 'colored-toast'
  },
  showConfirmButton: false,
  timer: 1500,
  timerProgressBar: true
})
	
function init() {
	getSelectKelompokJabatan();
	
	$('#btn-add,#btn-adds').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
		$('[name=id_kelompok_jabatan]').val($('#nav-tab .active').data('id'));
		getSelectJabatan($('#nav-tab .active').data('id'));
		getSelectUnitKompetensiTeknis($('#nav-tab .active').data('id'));
		getSelectKelompokTeknis($('#nav-tab .active').data('id'));

		$('[name=id_bidang_teknis]').val($('[name=filter_id_bidang_teknis]').val());
		$('#id_bidang_teknis').val($('[name=filter_id_bidang_teknis] option:selected').text());
		$('[name=id_unit_kompetensi_teknis]').val("").trigger('change');
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('[name=id_jenjang_jabatan]').on('change', function(){
		console.log("sebelum");
		console.log(this.value);
		var newValue = this.value.split("/");
		console.log("sesudah");
		console.log(newValue);
		$('[name=id_tingkat_jenjang]').val(newValue[1]);
	});
	
	$('[name=jenis_kompetensi]').on('click', function(){
		if(this.value=="INTI"){
			$('[name=id_kelompok_teknis]').attr('disabled',true);
			$('[name=id_kelompok_teknis]').val('').trigger('change');
		}else if(this.value=="PILIHAN"){
			$('[name=id_kelompok_teknis]').attr('disabled',false);
		}
	});
	
	$('[name=id_kelompok_jabatan]').on('change', function(){
		getSelectJabatan(this.value);
	});

	
	$('[name=filter_id_bidang_teknis]').on('change', function(){
		display(1,'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+this.value+'")');//,id_master_jabatan.eq("'+$('[name=filter_id_master_jabatan]').val()+'")
		$('#btn-add').attr('disabled',false);
	});
	
	$('[name=filter_id_master_jabatan]').on('change', function(){
		display(1,'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+$('[name=filter_id_bidang_teknis]').val()+'"),id_master_jabatan.eq("'+this.value+'")');
		$('#btn-add').attr('disabled',false);
	});
	
	$('#tbl-data').on('click', '.data-row', function(event) {
		$(this).addClass('table-primary').siblings().removeClass('table-primary');
		 
	});
	$('#tbl-data').on('click', '.data-head th', function(event) {
		if($(this).hasClass("table-primary")){
			$(this).removeClass('table-primary');
		}else{
			$(this).addClass('table-primary');
		}
	});
	
}

function initTableHeadFixed(){
	$(".header-fixed").html("");
	var tableOffset = $("#tbl-data").offset().top;
	var $header = $("#head").clone();
	var $fixedHeader = $(".header-fixed").append($header);

	$(window).bind("scroll", function() {
	    var offset = $(this).scrollTop();

	    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
	        $fixedHeader.show();
	    }
	    else if (offset < tableOffset) {
	        $fixedHeader.hide();
	    }
	});
}

function getSelectBidangTeknis(id){
	$('[name=id_bidang_teknis]').empty();
	$('[name=id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	$('[name=filter_id_bidang_teknis]').empty();
	//
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="";
	ajaxGET(path + '/bidang_teknis?'+params,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_bidang_teknis]').append(new Option(value.nama, value.id));
		$('[name=filter_id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	display();
}

function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	//$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		var params='onclick="getSelectBidangTeknis('+value.id+')"';
		$('#nav-tab').append('<a data-id="'+value.id+'" '+params+' class="nav-item nav-link '+(key==0?"active":"")+'" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">'+value.kode+'</a>');
		
	});
	getSelectBidangTeknis($('#nav-tab .active').data('id'));
	display();
}

function getSelectJabatan(id_kelompok_jabatan){
	$('[name=id_master_jabatan]').empty();
	$('[name=filter_id_master_jabatan]').empty();
	$('[name=id_master_jabatan]').append(new Option('Pilih Jabatan', ''));
	var page=1,params="filter=id_kelompok.eq('"+id_kelompok_jabatan+"')",qry="";
	ajaxGET(path + '/master_jabatan?page='+page+'&'+params+'&'+qry,'onGetSelectJabatan','onGetSelectError');
}
function onGetSelectJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_master_jabatan]').append(new Option(value.nama, value.id));
		$('[name=filter_id_master_jabatan]').append(new Option(value.nama, value.id));
	});
	$('[name=id_master_jabatan]').val($('#table-data tbody,tr.data-row.table-primary').data('id')).trigger('change');
}

function getSelectKelompokTeknis(id_kelompok_jabatan){
	$('[name=id_kelompok_teknis]').empty();
	$('[name=id_kelompok_teknis]').append(new Option('Pilih Kelompok Teknis', ''));
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id_kelompok_jabatan+"'),id_bidang_teknis.eq('"+$('[name=filter_id_bidang_teknis]').val()+"')",qry="";
	ajaxGET(path + '/kelompok_teknis?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokTeknis','onGetSelectError');
}
function onGetSelectKelompokTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_teknis]').append(new Option(value.nama, value.id));
	});
	if($('#table-data thead,th.table-primary').data('id')!="INTI" && $('#table-data thead,th.table-primary').data('id')!=undefined){
		$('[name=id_kelompok_teknis]').val($('#table-data thead,th.table-primary').data('id')).trigger('change');
		$('#jenis_kompetensi2').click();
	}else if($('#table-data thead,th.table-primary').data('id')!=undefined) {
		$('#jenis_kompetensi1').click();
	}
		
}

function getSelectUnitKompetensiTeknis(id_kelompok_jabatan){
	$('#id_unit_kompetensi_teknis').empty();
//	$('[name=id_unit_kompetensi_teknis]').append(new Option('Pilih Unit Kompetensi Teknis', ''));
	var page=1,
	params="filter=id_kelompok_jabatan.eq('"+id_kelompok_jabatan+"')",
	qry="limit=100000000";
	ajaxGET(path + '/unit_kompetensi_teknis?page='+page+'	&'+params+'&'+qry,'onGetSelectUnitKompetensiTeknis','onGetSelectError');
}
function onGetSelectUnitKompetensiTeknis(response){
	$.each(response.data, function(key, value) {
		$('#id_unit_kompetensi_teknis').append(new Option(value.kode, value.id));
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		//param = $('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		//console.log(param);
		display(1, 'filter=');
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function save(){
	var obj = new FormData(document.querySelector('#entry-form'));
    ajaxPOST(ctx + '/jabatan-kompetensi-teknis/save',obj,'onModalActionSuccess','onSaveError');
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();

	$('[name=id_master_jabatan]').val('').trigger('change');
	$('[name=id_kelompok_teknis]').val('').trigger('change');
}


function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#entry-form').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

//batas
function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	//var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	//tbody.text('');
	//tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=kelompok_jabatan,bidang_teknis,metode_pengembangan_jenis,unit_kompetensi_teknis&limit=10000000";
	if(params=="") qry+="&filter=id_kelompok_jabatan.eq("+$('#nav-tab .active').data('id')+"),id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	else qry+="&filter="+params;
	
	var qry  = "entity=metode_pengembangan_kompetensi_non;jabatan"
	qry+="&limit=metode_pengembangan_kompetensi_non->900000;";
	qry+="&filter=jabatan->id_kelompok_jabatan.eq("+$('#nav-tab .active').data('id')+");metode_pengembangan_kompetensi_non->id_kelompok_jabatan.eq("+$('#nav-tab .active').data('id')+"),id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	qry+="&join=metode_pengembangan_kompetensi_non->kelompok_jabatan,bidang_teknis,metode_pengembangan_jenis;";
	ajaxGET(path + '/multi/entity?page='+page+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	var responseDataMetodePengembangan = response.data.metode_pengembangan_kompetensi_non;
	var responseDataJabatan = response.data.jabatan;
	var tbody = $("#tbl-data").find('tbody');
	var qry="join=kelompok_jabatan,bidang_teknis,metode_pengembangan_jenis,master_jabatan&limit=10000000";
	var page=1;
	var params="";
	if($('[name=filter_id_bidang_teknis]').val()!=""){
		params+='filter=id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+$('[name=filter_id_bidang_teknis]').val()+'")';
		params+=',other.eq("NON_TEKNIS")';
		params+='&order=id_master_jabatan,id_metode_pengembangan_jenis'
	$.ajax({
        url : path + '/metode_pengembangan_kelompok_nilai?page='+page+'&'+params+'&'+qry,
        type : "GET",
        traditional : true,
        contentType : "application/json",
        success : function (response) {
        	var responseDataKelompokNilai = response.data;
        	//render head
        	var thead = $("#head");
			var rowHead = '';
            var rowHead = '<tr class="sticky-header">'+
                '<th width="5%" rowspan="4">No</th>'+
                '<th rowspan="4">FUNGSI DASAR/UNIT KOMPETENSI</th>'+
                '<th rowspan="4" class="sticky-cell">KODE UNIT</th>';

				var htmlhead1=[];
				var htmlhead2=[];
				var htmlArr=[];
	        	$.each(responseDataKelompokNilai,function(key,value){
	        		var name = value.master_jabatan.nama_jenjang;
					if(htmlhead1.indexOf(name) == -1) {
						htmlhead1.push(name);
						htmlArr.push(value.master_jabatan.id_jenjang);
					}
	        		var name2 = value.metode_pengembangan_jenis.type;
					if(htmlhead2.indexOf(name2) == -1) {
						htmlhead2.push(name2);
					}
				})
				
				var count_jabatan=[];
	        	$.each(htmlhead1,function(key,value_jenjang){
					var count=0;
					$.each(responseDataKelompokNilai,function(key,value){
						if(value_jenjang==value.master_jabatan.nama_jenjang){count++;}
					});
					count_jabatan.push(
						{"id" : htmlArr[key], "jabatan" : value_jenjang, "count" : count, "type" : [], "obj" : []}	
					);
				});
				console.log(count_jabatan);
				
	        	$.each(count_jabatan,function(key,value_jabatan){
					rowHead +='<th class="text-center data-head" rowspan="3" data-id="'+key+'">'+"Nilai Standar <"+'</th>';
					rowHead +='<th class="text-center data-head" colspan="'+value_jabatan.count+'" data-id="'+key+'">'+value_jabatan.jabatan+'</th>';
					$.each(htmlhead2,function(key,value_type){
						var count_type=0;
						$.each(responseDataKelompokNilai,function(key,value){
							//value.jabatan == value.master_jabatan.nama_jenjang
							if(value_type==value.metode_pengembangan_jenis.type && 
							value_jabatan.jabatan == value.master_jabatan.nama_jenjang)
							{
								value_jabatan.obj.push(this);
								count_type++;
							}
						});
						if(count_type>0) value_jabatan.type.push({"name" : value_type, "count" : count_type});
					});
				});
				console.log(count_jabatan);
				
            rowHead +='</tr>';
            rowHead +='<tr class="sticky-header">';
                //rowHead +='<th class="text-center" colspan="4">Klasikal(<=30)</th>';
                //rowHead +='<th class="text-center" colspan="4">Non-Klasikal(31-60)</th>';
	        	$.each(count_jabatan,function(key){
					$.each(this.type,function(key,value){
						rowHead +='<th class="text-center data-head"  colspan="'+value.count+'" data-id="'+key+'">'+value.name+'</th>';
					});
				});
            rowHead +='</tr>';

            /*rowHead +='<tr class="sticky-header">';
		        	$.each(responseDataKelompokNilai,function(key,value){
						rowHead +='<th class="text-center data-head"  data-id="'+value.id+'">'+value.operator+""+value.nilai+'</th>';
					});
            rowHead +='</tr>';
			
            rowHead +='<tr class="sticky-header">';
               		$.each(responseDataKelompokNilai,function(key,value){
						var string_name=value.metode_pengembangan_jenis.nama;
						if(string_name.toString().length > 13) {
							string_name = string_name.splice(13, 0, "<br/>");
						}
						rowHead +='<th class="text-right data-head" style="writing-mode: tb-rl;height:min-content" data-id="'+value.id+'">'+string_name+'</th>';
					});
            rowHead +='</tr>';*/
			
			//
            rowHead +='<tr class=" sticky-header">';
               		$.each(responseDataKelompokNilai,function(key,value){
						var string_name=value.metode_pengembangan_jenis.nama;
						if(string_name.toString().length > 13) {
							string_name = string_name.splice(13, 0, "<br/>");
						}
						//var string_container='<div>'+value.operator+""+value.nilai+'</div><hr class="border">
						var string_container='<div style="writing-mode: tb-rl;height:100px;">'+string_name+'</div>';
						var bgdy;
						if(value.metode_pengembangan_jenis.type=="KLASIKAL"){
							bgdy="background-color:#ffc107!important;";
						}else{
							bgdy="background-color:#08c!important";
						}
						rowHead +='<th class="text-right data-head" style="'+bgdy+'" data-id="'+value.id+'">'+string_container+'</th>';
					});
            rowHead +='</tr>';
			//
			
        	thead.html(rowHead);
			//initTableHeadFixed();
        	//end head
			var data = {"head" : responseDataKelompokNilai, "body" : responseDataMetodePengembangan, "count" : count_jabatan, "filter" : responseDataJabatan};
			renderBody(data);
        },
        error : function (response) {
        	alert("error");
        	$.LoadingOverlay("hide");
        },
    });
	}
}

String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function renderBody(data){
	var tbody = $("#tbl-data").find('tbody');
	tbody.text('');
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=kelompok_jabatan,fungsi_utama&limit=10000000";
	//if(params=="") 
	//qry+="&filter=id_kelompok_jabatan.eq("+$('#nav-tab .active').data('id')+")";
	//,id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	//else qry+="&filter="+params;//+",id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	//qry="";
	//ajaxGET(path + '/unit_kompetensi_manajerial?'+qry,'onRenderBody','onGetListDetilError', data);
//	var qry="&filter_id_kelompok="+$('#nav-tab .active').data('id');//join=kelompok_jabatan
//	qry="&"+sub_params;
	qry="&entity=unit_kompetensi_manajerial;unit_kompetensi_sosiokultural;jabatan_manajerial";
	qry+="&limit=jabatan_manajerial->900000;";
	
	var param =[];//= "'26','27','28','29','30','31','32','33'";
	
	$.each(data.filter, function(key){
		param.push(this.id);
	});
	console.log(param.toString());
	//param= "'26','27','28','29','30','31','32','33'";
	qry+="&filter=jabatan_manajerial->id_master_jabatan.in("+param+"),nilai_minimum.not(null)";
	qry+="&join=jabatan_manajerial->master_jabatan;";
	ajaxGET(path + '/multi/entity?page='+1+qry,'onRenderBody','onGetListDetilError', data);
}

function addValue(data, obj){
	$.each(data, function(){
		this[obj.name] = obj.value;
	});
	return data;
}

function onRenderBody(response, data){
	console.log(response);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var data_manajerial = response.data.jabatan_manajerial;
	var response = {
		data : addValue(response.data.unit_kompetensi_manajerial, {"name" : "type", "value" : "MANAJERIAL"}).concat(addValue(response.data.unit_kompetensi_sosiokultural, {"name" : "type", "value" : "SOSIOKULTURAL"}))
	}
	var tbody = $("#tbl-data").find('tbody');
	var row = "";
	var num = $('.data-row').length+1;
	num = 1;
	var newkey=0;
	console.log(data);
	$.each(response.data,function(key,value){
		row += '<tr data-id="'+value.id+'" data-type="'+value.type+'" class="data-row" id="row-'+value.id+'">';
			row += '<td>'+(num)+'</td>';
			row += '<td>'+value.judul+'</td>';
			row += '<td class="sticky-cell" style="min-width:100px">'+value.kode+'</td>';
			
			/*
			$.each(data.head,function(key_head){
				//if(key_head==key)console.log("newkey1:"+newkey+","+key_head+","+key);
				//if(key_head==key)console.log("newkey2:"+newkey+","+key_head+","+key);
				//if(key_head==key)console.log("newkey3:"+newkey+","+key_head+","+key);
				//if(key_head==newkey)console.log("newkey4:"+newkey+","+key_head+","+key);
				
				//console.log("newkey5:"+newkey+","+key_head+","+key);
				//if(0==key_head || 8==key_head  || 18==key_head || 27==key_head) 
				//row += '<td class="bg-secondary">'+"CRYING"+'</td>';
				var dataAttr="data-id-master-jabatan='"+this.id_master_jabatan+"' data-id-kelompok-jabatan='"+this.id_kelompok_jabatan+"' data-id-bidang-teknis='"+this.id_bidang_teknis+"' ";
				 	dataAttr+="data-id-unit-kompetensi-teknis='"+value.id+"' data-id-metode-pengembangan-jenis='"+this.id_metode_pengembangan_jenis+"' ";
					dataAttr+='data-id-with-jabatan-with-unit-komptek-with-type-'+this.id_metode_pengembangan_jenis+"-"+this.id_master_jabatan+"-"+value.id+"-"+value.type+'='+this.id_metode_pengembangan_jenis+"-"+this.id_master_jabatan+"-"+value.id+"-"+value.type;
					//$("#"+key+"-"+key_head).attr('data-id-with-jabatan-'+this.id+"-"+this.id_master_jabatan, this.id+"-"+this.id_master_jabatan);
				row += '<td id="'+key+'-'+key_head+'" '+dataAttr+' data-type=\''+value.type+'\' onclick="saveColumn(this)"></td>';
				//newkey++;
			});*/
			
			$.each(data.count,function(key_count,value_jabatan){
				//id_unit_komp, nama_jenjang_jabatan, type;
				row +='<th id="'+value.id+'-'+value_jabatan.id+'-'+value.type+'-standar'+'" class="text-center bg-dark text-white" rowspan="" data-id="'+key+'">'+""+'</th>';
				
				$.each(this.obj,function(key_obj,value_obj){
					var dataAttr="data-id-master-jabatan='"+this.id_master_jabatan+"' data-id-kelompok-jabatan='"+this.id_kelompok_jabatan+"' data-id-bidang-teknis='"+this.id_bidang_teknis+"' ";
					//dataAttr+="data-nilai='"+value.nilai_minimum+"' ";
				 	dataAttr+="data-id-unit-kompetensi-teknis='"+value.id+"' data-id-metode-pengembangan-jenis='"+this.id_metode_pengembangan_jenis+"' ";
					dataAttr+='data-id-with-jabatan-with-unit-komptek-with-type-'+this.id_metode_pengembangan_jenis+"-"+this.id_master_jabatan+"-"+value.id+"-"+value.type+'='+this.id_metode_pengembangan_jenis+"-"+this.id_master_jabatan+"-"+value.id+"-"+value.type;
					
				//for(var i=0; i < this.count; i++){
					row += '<td id="'+value.id+'-'+value_jabatan.id+'-'+value.type+'-'+this.id_metode_pengembangan_jenis+'" '+dataAttr+' data-type=\''+value.type+'\' ></td>'; //onclick="saveColumn(this)"
				//}
				});
			});
			
		row += '</tr>';
		num++;
	});
	
	tbody.html(row);
	
	
	$.each(response.data,function(key,value){
		//var data = {"head" : responseDataKelompokNilai, "body" : responseDataMetodePengembangan};
		$.each(data.head,function(key_head, val_head){
			$.each(data.body,function(){
				//lets set data in here
				//unit, jenis_metode, jabatan
				if(value.type==this.tipe_kompetensi &&value.id==this.id_unit_kompetensi_teknis && val_head.id_metode_pengembangan_jenis==this.id_metode_pengembangan_jenis
				 && val_head.id_master_jabatan==this.id_master_jabatan){
					$("#"+this.id_unit_kompetensi_teknis+"-"+val_head.master_jabatan.id_jenjang+"-"+value.type+"-"+val_head.id_metode_pengembangan_jenis).html("<a href='javascript:void(0)'>V</a>");
					$("#"+this.id_unit_kompetensi_teknis+"-"+val_head.master_jabatan.id_jenjang+"-"+value.type+"-"+val_head.id_metode_pengembangan_jenis).attr('data-id', this.id);
				}
			});
		});
		
		$.each(data.count,function(key_count,value_jabatan){
			$.each(this.obj, function(k, val_head){
				
				$.each(data_manajerial, function(){
					
					if(this.jenis==value.type && value.id==this.id_kelompok_manajerial && value_jabatan.jabatan==this.master_jabatan.nama_jenjang){// && this.master_jabatan.nama_jenjang==val_head.master_jabatan.nama_jenjang
						//id_unit_komp, nama_jenjang_jabatan, type;
						$("#"+this.id_kelompok_manajerial+"-"+value_jabatan.id+"-"+value.type+"-standar").html(this.nilai_minimum);//"<a href='javascript:void(0)'>V:"+this.nilai_minimum+"</a>");
						
						console.log(this.id_kelompok_manajerial+"-"+value_jabatan.id+"-"+val_head.id_metode_pengembangan_jenis);
						$("#"+this.id_kelompok_manajerial+"-"+value_jabatan.id+"-"+value.type+"-"+val_head.id_metode_pengembangan_jenis).attr('data-nilai', this.nilai_minimum);
						$("#"+this.id_kelompok_manajerial+"-"+value_jabatan.id+"-"+value.type+"-"+val_head.id_metode_pengembangan_jenis).attr('onclick', 'saveColumn(this)');
						//id="'+value.id+'-'+value_jabatan.id+'-'+value.type+'-'+this.id_metode_pengembangan_jenis+'"
					}
				});
			});
		});
	});
	
	if(data.head.length==0) tbody.html("");
	
}

function saveColumn(e){
	//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	$(e).LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	var url = path+'/metode_pengembangan_kompetensi_non/save?append=false&join=kelompok_jabatan,bidang_teknis,metode_pengembangan_jenis,unit_kompetensi_teknis';
	var obj_data = {
		"id" : e.dataset.id,
		"id_kelompok_jabatan" : e.dataset.idKelompokJabatan,
		"id_bidang_teknis" : e.dataset.idBidangTeknis,
		"id_unit_kompetensi_teknis" : e.dataset.idUnitKompetensiTeknis,
		"id_metode_pengembangan_jenis" : e.dataset.idMetodePengembanganJenis,
		"id_master_jabatan" : e.dataset.idMasterJabatan,
		"nilai" : e.dataset.nilai,
		"status" : 1,
		"tipe_kompetensi" : e.dataset.type
	}
	window.obj = {
			data : obj_data,
			/*dataMain : {
				"column" : [
					$('[name=nama]').val(),
					$('[name=type]').val()
				]
			}*/
	};
	
	if(obj_data.id == 0 || obj_data.id == undefined) {
		$.ajax({
	        url : url,
	        type : "POST",
	        traditional : true,
	        contentType : "application/json",
	        dataType : "json",
	        data : JSON.stringify(obj),
	        success : function (response) {
					var value=response.data;
	        		$(e).LoadingOverlay("hide");
	        		//$('#modal-form').modal('hide');
	        		//display();
					//data-id-with-jabatan'+this.id+"-"+this.id_master_jabatan
					//$('[data-id-with-jabatan-with-unit-komptek-'+4+'-'+26+'-'+2+']')
					///console.log(value.id_metode_pengembangan_jenis+'-'+value.id_master_jabatan+'-'+value.id_unit_kompetensi_teknis);
					///console.log($('[data-id-with-jabatan-with-unit-komptek-'+value.id_metode_pengembangan_jenis+'-'+value.id_master_jabatan+'-'+value.id_unit_kompetensi_teknis+']'));
					
					$('[data-id-with-jabatan-with-unit-komptek-with-type-'+value.id_metode_pengembangan_jenis+'-'+value.id_master_jabatan+'-'+value.id_unit_kompetensi_teknis+'-'+value.tipe_kompetensi+']').html('<a href="javascript:void(0)">V</a>');
	        		$('[data-id-with-jabatan-with-unit-komptek-with-type-'+value.id_metode_pengembangan_jenis+'-'+value.id_master_jabatan+'-'+value.id_unit_kompetensi_teknis+'-'+value.tipe_kompetensi+']').attr('data-id', value.id);
					
					//onModalActionSuccess(response);
					//showAlertMessage(response.message, 1500);
					Toast.fire({
					  icon: 'success',
					  title: 'Data berhasil disimpan'
					})
	        },
	        error : function (response) {
	        		$(e).LoadingOverlay("hide");
					showAlertMessage(response.responseJSON.message, 1500);
	        },
	    });
	}else{
		doAction(obj_data.id, 'delete', 1);
	}
}

function almghty(e){
	console.log(e.dataset.idKompetensiTeknis);
	console.log(e.dataset.id);
	//window.location.href="form-pertanyaan?id="+e.dataset.id;
	window.open("form-pertanyaan?id="+e.dataset.idKompetensiTeknis+"&jenis=teknis&bidang_teknis="+$('[name=filter_id_bidang_teknis]').val(), '_blank');
	console.log(e);
}
function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		query="";
		ajaxGET(path + '/metode_pengembangan_kompetensi_non/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=kelompok_jabatan,bidang_teknis,metode_pengembangan_jenis,unit_kompetensi_teknis';
		ajaxPOST(path + '/metode_pengembangan_kompetensi_non/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	var value=response.data;
	$('[data-id-with-jabatan-with-unit-komptek-with-type-'+value.id_metode_pengembangan_jenis+'-'+value.id_master_jabatan+'-'+value.id_unit_kompetensi_teknis+'-'+value.tipe_kompetensi+']').html('');
	// Kalo mau refresh semua data yang tampil di table
	////display(1,'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+$('[name=filter_id_bidang_teknis]').val()+'"),id_master_jabatan.eq("'+response.data.id_master_jabatan+'")');
	////$('[name=filter_id_master_jabatan]').val(response.data.id_master_jabatan);
	// Update data ke existing tabel
	/*if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}*/
	var e = $('[data-id-with-jabatan-with-unit-komptek-with-type-'+value.id_metode_pengembangan_jenis+'-'+value.id_master_jabatan+'-'+value.id_unit_kompetensi_teknis+'-'+value.tipe_kompetensi+']');
	e.LoadingOverlay("hide");
	e.removeAttr('data-id');
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	//showAlertMessage(response.message, 1500);
	Toast.fire({
	  icon: 'success',
	  title: 'Data berhasil dihapus'
	})
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=id_kelompok_jabatan]').val(value.id_kelompok_jabatan);
	$('[name=id_master_jabatan]').val(value.id_master_jabatan).trigger('change');
	$('[name=id_bidang_teknis]').val(value.id_bidang_teknis);
	$('[name=id_kelompok_teknis]').val(value.id_kelompok_teknis).trigger('change');
	$('[name=id_unit_kompetensi_teknis]').val(value.id_unit_kompetensi_teknis);
	$('[name=jenis_kompetensi]').val(value.jenis_kompetensi);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

var html = [];
function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';//value.kelompok_jabatan.kode
	row += '<td class="">'+value.master_jabatan.nama+'</td>';
	var name = value.master_jabatan.nama;
	if(html.indexOf(name) == -1) html.push(name);
	else html.push("");
	row += '<td class="">'+value.bidang_teknis.nama+'</td>';
	row += '<td class="">'+value.kelompok_teknis.nama+'</td>';
	row += '<td class="">'+value.unit_kompetensi_teknis.kode+'</td>';
	row += '<td class="">'+value.jenis_kompetensi+'</td>';


	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}