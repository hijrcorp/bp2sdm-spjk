var pathsql = ctx + '/restql';
var path = ctx + '/authorities';
var path_account = ctx + '/account';
var path_simpeg = ctx + '/simpeg';
var path_position = ctx + '/position';
var path_group = ctx + '/account/group';
var selected_id = '';
var selected_account_id = '';
var current_page = 1;
var selected_action = '';
var selected_mode = ($.urlParam('mode')==null?"":$.urlParam('mode'));
var params = '';
var appendIds = [];

function init(){
	getSelectGroup();
	getSelectUnitAuditi();
	getSelectEselon1();
	getSelectPropinsi();
	display();

	if(selected_mode=="multiform") {
		selected_id = '';
		selected_account_id = '';
		selected_action = 'add';
		clearForm();
		$('#modal-form').modal({backdrop: 'static', keyboard: false});
	}
	getSelectKelompokJabatan();
//	else{
//		selected_id = '';
//		selected_account_id = '';
//		selected_action = 'add';
//		clearForm();
//	}
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_account_id = '';
		selected_action = 'add';
		clearForm();

		$('#modal-form').modal({backdrop: 'static', keyboard: false});
	});

	
	$('[name=filter_group]').change(function(e){
		$('#search-form').submit();
	});
	
	$('[name=filter_unit_auditi]').change(function(e){
		$('#search-form').submit();
	});
	
	$('[name=group_id]').change(function(e){
		if(this.value=="2002400" || this.value=="2002300" || this.value=="2002200"){
			if(this.value=="2002400"){
				$('#form-eselon1').show();
				$('#form-eselon1').removeClass('d-none');
				
				$('#form-jabatan').addClass('d-none');
				$('[name=id_kelompok_jabatan]').val('').trigger('change');
				$('[name=id_jabatan]').val('').trigger('change');
				$('[name=unit_auditi_id]').val('').trigger('change');
				
				
				$('#form-unit-auditi').addClass('d-none');
				$('#form-unit-auditi').hide();
			}else{
				$('#form-unit-auditi').show();
				$('#form-unit-auditi').removeClass('d-none');
				if(this.value=="2002200"){
					//$('#form-jabatan').show();
					$('#form-jabatan').removeClass('d-none');
				}else{
					$('#form-jabatan').addClass('d-none');
					$('[name=id_kelompok_jabatan]').val('').trigger('change');
					$('[name=id_jabatan]').val('').trigger('change');
					$('[name=eselon1_id]').val('').trigger('change');
				}
				
				$('#form-eselon1').addClass('d-none');
				$('#form-eselon1').hide();
			}
		}else{
			$('#form-unit-auditi').addClass('d-none');
			$('#form-unit-auditi').hide();
			$('#form-eselon1').addClass('d-none');
			$('#form-eselon1').hide();
			$('[name=unit_auditi_id]').val('').trigger('change');
			$('[name=eselon1_id]').val('').trigger('change');
			$('#form-jabatan').addClass('d-none');
			$('[name=unit_auditi_id]').val('').trigger('change');
			$('[name=id_kelompok_jabatan]').val('').trigger('change');
			$('[name=id_jabatan]').val('').trigger('change');
		}
	});

	$('[name=id_kelompok_jabatan]').change(function(e){
		getSelectJabatan(this.value);
	});
	
	$('#search-form').submit(function(e){
		display();
		e.preventDefault();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});

	
	$('#btn-lookup').click(function(e){
		getPegawai();
		
	});
	$('#modal-form').on('hidden.bs.modal', function (e) {
		if(selected_mode=="multiform") {
			window.location.href="account"
		}

		$('#form-unit-auditi').addClass('d-none');
		$('[name=unit_auditi_id]').val('').trigger('change');
		
		$('#form-eselon1').addClass('d-none');
		$('[name=eselon1_id]').val('').trigger('change');
	})

	$(".button-minus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepDown();
	});
	$(".button-plus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepUp();
	});
}

function importExcel(upload){
	$('#modal-form-table-msg').hide();
	if(upload!=undefined){
		var obj = new FormData();
		obj.append('file_excel', $('[name=file_excel]')[0].files[0]);
		if(upload==2) obj.append('mode', 'save');
		
		if($('[name=file_excel]')[0].files[0].size > 8871848){
			alert("file excel terlalu besar, minimal 8mb");
			return;
		}
		
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	    ajaxPOST(ctx + '/account/preview-import',obj,'onImportExcelSuccess','onImportExcelError');
	}else{
		$('#modal-form-table').modal('show');
	}
}

function onImportExcelError(response){
	console.log(response);
	$.LoadingOverlay("hide");
	$('#modal-form-table-msg').html(response.responseJSON.message.replace("[","").replace("]",""));
	$('#modal-form-table-msg').show();
	var table = $('#tbl-responden-modal-table').find('tbody');
	table.html("");
}

function onImportExcelSuccess(response){
	console.log(response);
	$.LoadingOverlay("hide");
	if(response.data=="save"){
		showAlertMessage(response.message, 1500);
		$('[name=file_excel]').val('').trigger('change');
		var table = $('#tbl-responden-modal-table').find('tbody');
		table.html("");
	}else{
		var table = $('#tbl-responden-modal-table').find('tbody');
		var row="";
		$.each(response.data, function(key){
			row+="<tr>";
				row+="<td>"+(key+1)+"</td>";
				row+="<td>"+this.account.first_name+"</td>";
				row+="<td>"+this.account.username+"</td>";
				row+="<td>"+this.unitAuditi.nama_unit_auditi+"</td>";
			row+="</tr>";
		});
		table.html(row);
		
		$('#btn-form-table').removeAttr('disabled');
	}
}

function inputInitToPaste(){
//	$('[name=username]').bind('paste', null, function (e) {
//	    $this = $(this);
//
//	    setTimeout(function () {
//	        var columns = $this.val().split(/\s+/);
//
//	        var i;
//	        var input = $this;
//	        for (i = 0; i < columns.length; i++) {
//	            input.val(columns[i]);
//	            input = input.next();
//	        }
//	    }, 0);
//	});
	$('[name=username]').on('paste', function(e){
		console.log(this);
	    var $this = $(this);
	    $.each(e.originalEvent.clipboardData.items, function(i, v){
	        if (v.type === 'text/plain'){
	            v.getAsString(function(text){
	                var x = $this.closest('td').index(),
	                    y = $this.closest('tr').index(),
	                    obj = {};
	                text = text.trim('\r\n');
	                $.each(text.split('\r\n'), function(i2, v2){
	                    $.each(v2.split('\t'), function(i3, v3){
	                        var row = y+i2, col = x+i3;
	                        obj['cell-'+row+'-'+col] = v3;
							console.log(this);
							console.log("v2 "+v2);
							console.log($this.closest('table').find('tr:eq('+row+') td:eq('+col+') input').val());
							console.log($this.closest('table').find('tr:eq('+row+') td:eq('+col+') select'));
							console.log($this.closest('table').find('tr:eq('+row+') td:eq('+col+')'));
							console.log(i3);
	                       	if(i3==0) {
								$this.closest('table').find('tr:eq('+row+') td:eq('+col+') input').val(v3.replaceAll(/\s/g, ""));
							}else if(i3==3) {
								var myelem = $this.closest('table').find('tr:eq('+row+') td:eq('+col+') select option');
								myelem.filter(function() {
								  //may want to use $.trim in here
								 return $(this).text().split(" ")[1] == v3.toUpperCase()
								}).prop('selected', true);
							}else{
								$this.closest('table').find('tr:eq('+row+') td:eq('+col+') input').val(v3);
							}
	                    });
	                });
	                //$('div').text(JSON.stringify(obj));
	            });
	        }
	    });
	    return false;
	   
	});
}

function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(pathsql + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	var tabs="";
	var forms="";
	var template="";
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		var params='onclick="display(1,\''+'filter_id_kelompok='+""+ value.id +""+'\');"';
		params='onclick="renderForm(\''+'filter_id_kelompok'+'\');"';
//		$('#myTab').append('<a data-id="'+value.id+'" '+params+' class="nav-link '+(key==0?"active":"")+'" id="nav-'+value.kode+'-tab" data-toggle="tab" href="#nav-'+value.kode+'" role="tab" aria-controls="nav-home" aria-selected="true">'+value.kode+'</a>');
		tabs+='<li class="nav-item" role="presentation">';
			tabs+='<a data-id="'+value.id+'" class="nav-link '+(key==0?"active":"")+'" '+params+' data-toggle="tab" href="#nav-home" role="tab">'+value.kode+'</a>';
		tabs+='</li>';
//		forms+='<div class="tab-pane fade '+(key==0?"active show":"")+'" id="nav-'+value.kode+'" role="tabpanel" aria-labelledby="home-tab">'+template+'</div>';
	});
	$('#myTab').html(tabs);
//	$('#myTab').html(tabs);
//	$('#myTabForm').html(forms);
}

function getSelectJabatan(id){
	$('[name=id_jabatan]').empty();
	$('[name=id_jabatan]').append(new Option('Pilih Bidang Teknis', ''));

	var qry="join=kelompok_jabatan,jenjang_jabatan,tingkat_jabatan";
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')";
	if(id==undefined) params="";
	ajaxGET(pathsql + '/jabatan?'+params+'&'+qry,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_jabatan]').append(new Option(value.tingkat_jabatan.nama+" "+value.jenjang_jabatan.nama, value.id));
	});
}

function renderForm(){
	var template = '<div class="form-row d-none">'+
	'    <div class="form-group col-md-6">'+
	'        <label>User Group</label>'+
	'        <input name="group_id" type="text" class="form-control" value="2002200" />'+
	'    </div>'+
	'</div>'+
	'<div class="form-row pt-3">'+
	'    <div class="form-group col-md-4">'+
	'        <label class="">Jumlah Responden : </label>'+
	'        <div class="input-group mb-3">'+
	'            <input min="1" type="number" class="form-control" minlength="2" value="2" name="jumlah_responden" placeholder="Jumlah Responden" />'+
	'            <div class="input-group-append">'+
	'                <button data-obj="" id="btn-create-form" onclick="createFormResponden()" class="btn btn-outline-success" type="button">Create</button>'+
	'            </div>'+
	'        </div>'+
	'    </div>'+
	'</div>'+
	'<div class="form-row">'+
	'    <div class="form-group col-md-12"><strong class="control-label">Form Responden : </strong></div>'+
	'</div>'+
	'<div class="table-responsive">'+
	'    <table id="tbl-responden" class="table table-sm table-bordered table-hover" width="100%" cellspacing="0">'+
	'        <thead class="bg-light">'+
	'            <tr>'+
	'                <th width="5%">No</th>'+
	'                <th width="20%">NIP</th>'+
	'                <th width="20%">Nama</th>'+
	'                <th width="20%">Satker</th>'+
	'                <th width="20%">Jabatan/Jenjang</th>'+
	'            </tr>'+
	'        </thead>'+
	'        <tbody></tbody>'+
	'    </table>'+
	'</div>';
	$('#myTabForm').html(template);
}

function createFormResponden(){
	var temp="";
	for(var i=1; i <= $('[name=jumlah_responden]').val(); i++){
		temp += '<tr class="data-row-modal" id="rowresponden-'+i+'">'+
		'    <td>'+i+'</td>'+
		'    <td class="d-none"><input name="id" type="text"/></td>'+
		'    <td class=""><input onchange="getPegawai(this)" name="username" type="text" class="form-control"/></td>'+
		'    <td class=""><input name="nama" type="text" class="form-control"/></td>'+
		'    <td class=""><select name="id_unit_auditi" class="form-control form-control-sm"></select></td>'+
		'    <td class=""><select name="id_jabatan" class="form-control form-control-sm"></select></td>'+
		'</tr>';
	}
	$('#tbl-responden tbody').html(temp);
	
	/*if($('#btn-create-form').data('obj').length>0){
		$.each($('#btn-create-form').data('obj'), function(key){
			$('[name=bobot_jawaban]')[key].value=this.bobot;
			$('[name=id_jawaban]')[key].value=this.id;
			$('[name=isi_jawaban]')[key].value=this.isi;
		});
	}*/

	getSelectJabatan($('#myTab .active').data('id'));
	getSelectUnitAuditi();
	inputInitToPaste();
}

var temp_val;
var temp_elem;
function getPegawai(e){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	console.log(e.parentElement.parentElement);
	var nip = $(e).val();
	temp_val=e.parentElement.parentElement;
	temp_elem=e;
	ajaxGET(path_simpeg + '/pegawai?filter_nip='+nip,'onGetPegawaiSuccess','onGetError');
}

function onGetPegawaiSuccess(response){
	console.log(response);
	if(response.data!=null){
		temp_val.children[3].children[0].value=response.data.nama;
//		temp_val.children[4].innerText=response.data.nama;
//		temp_val.children[5].innerText=response.data.nama_unit_auditi;
//		temp_val.children[6].innerText=response.data.kode_jabatan;
		temp_val.setAttribute("class", "");

		temp_val.children[4].children[0].value=response.data.id_unit_auditi;
	}else{
//		temp_val.children[3].children[0].value="";
//		temp_val.children[4].innerText="";
//		temp_val.children[5].innerText="";
//		temp_val.children[6].innerText="";
		temp_val.setAttribute("class", "bg-danger text-white");

	}
	$.LoadingOverlay("hide");
}

function getListPosition(){
	ajaxGET(path_reference + '/list/account/position','onGetPositionSuccess','onGetError');
}
function onGetPositionSuccess(response){
	$.each(response.data, function(key, value) {
		$('[name=position_id]').append(new Option(value.name, value.id));
	});
	state_list_position = true;
	set_if_true();
}

function add(mode=""){
	selected_mode=mode;
	selected_id = '';
	selected_account_id = '';
	selected_action = 'add';
	clearForm();
	$('#modal-form').modal('show');
	if(mode=="PNS") $("#form-pns").show();
	else $("#form-pns").hide();
	if(mode=="NON_PNS") $("#form-non-pns").show();
	else $("#form-non-pns").hide();
}


function getSelectPropinsi(){
	
	$('[name=propinsi_id]').empty();
	$('[name=propinsi_id]').append(new Option('Select Propinsi', ''));
	var qry="";
	var params="limit=1000";
	var page=1;
	ajaxGET(ctx + '/ref/propinsi/list?page='+page+'&'+params+'&'+qry,'onGetSelectPropinsi','onGetSelectError');
}
function onGetSelectPropinsi(response){
	console.log(response);
	$.each(response.data, function(key, value) {
		$('[name=propinsi_id]').append(new Option(value.nama_propinsi, value.id_propinsi));
		
		if(response.count==1) {
			$('[name=propinsi_id]').val(value.id_propinsi);
		}
	});
}

function getSelectUnitAuditi(){
	$('[name=filter_unit_auditi]').empty();
	$('[name=filter_unit_auditi]').append(new Option('All Satker', ''));
	
	$('[name=id_unit_auditi]').empty();
	$('[name=id_unit_auditi]').append(new Option('Select Satker', ''));
	var qry="";
	var params="limit=1000";
	var page=1;
	ajaxGET(ctx + '/unit-auditi/list?page='+page+'&'+params+'&'+qry,'onGetSelectUnitAuditi','onGetSelectError');
}
function onGetSelectUnitAuditi(response){
	console.log(response);
	$.each(response.data, function(key, value) {
		$('[name=unit_auditi_id]').append(new Option(value.nama_unit_auditi, value.id_unit_auditi));
		$('[name=id_unit_auditi]').append(new Option(value.nama_unit_auditi, value.id_unit_auditi));
		$('[name=filter_unit_auditi]').append(new Option(value.nama_unit_auditi, value.id_unit_auditi));
		
		if(response.count==1) {
			$('[name=unit_auditi_id]').val(value.id_unit_auditi);
			$('[name=id_unit_auditi]').val(value.id_unit_auditi);
		}
	});
}

function getSelectEselon1(){
	
	$('[name=eselon1_id]').empty();
	$('[name=eselon1_id]').append(new Option('Select Eselon1', ''));
	
	$('[name=id_eselon1]').empty();
	$('[name=id_eselon1]').append(new Option('Select Eselon1', ''));
	var qry="";
	var params="limit=1000";
	var page=1;
	ajaxGET(ctx + '/eselon1/list?page='+page+'&'+params+'&'+qry,'onGetSelectEselon1','onGetSelectError');
}
function onGetSelectEselon1(response){
	console.log(response);
	$.each(response.data, function(key, value) {
		$('[name=eselon1_id]').append(new Option(value.nama, value.id));
		$('[name=id_eselon1]').append(new Option(value.nama, value.id));
	});
}

function getSelectGroup(){
	$('[name=filter_group]').empty();
	$('[name=filter_group]').append(new Option('All Groups', ''));
	
	$('[name=group_id]').empty();
	$('[name=group_id]').append(new Option('Select Group', ''));
	
	ajaxGET(path_group + '/list?filter_application='+$('[name=filter_application]').val(),'onGetSelectGroup','onGetSelectError');
}
function onGetSelectGroup(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_group]').append(new Option(value.name, value.id));
		$('[name=group_id]').append(new Option(value.name, value.id));
	});
}

function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');

		
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(path + '/list?page='+page+'&'+params,'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
//	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function appendRow(value, action){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num, action);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}

function renderRow(value, num, method=''){
//	console.log(value);
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	
	row += '<td>'+(num)+'</td>';
	if(method=="add"){
		row += '<td class="">'+value.account.username+'</td>';
		row += '<td class="">'+value.account.first_name+' '+value.account.last_name+'</td>';
		row += '<td class="">'+value.group.name+'</td>';
		row += '<td class="">'+value.nama_jabatan+'</td>';
		if(value.account.enabled==true){
			row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
		}else{
			row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
		}
		row += '<td>'+value.unitAuditi.nama_unit_auditi+'</td>';
		row += '<td class="d-none">'+moment(value.account.time_added).fromNow();+'</td>';
		/*row += '<td class="">'+value.username+'</td>';
		row += '<td class="">'+value.first_name+'</td>';
		row += '<td class="">'+value.last_name+'</td>';
		row += '<td class="">'+value.group.name+'</td>';
		if(value.account.enabled==true){
			row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
		}else{
			row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
		}
		row += '<td class="">'+moment(value.account.time_added).fromNow();+'</td>';*/
	}else{
		row += '<td class="">'+value.account.username+'</td>';
		row += '<td class="">'+value.account.first_name+' '+value.account.last_name+'</td>';
		
		if(value.account.enabled==true){
			row += '<td class="">'+value.group.name+'<br/><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
		}else{
			row += '<td class="">'+value.group.name+'<br/><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
		}
		row += '<td class="">'+value.nama_jabatan+'</td>';
		row += '<td>'+value.unitAuditi.nama_unit_auditi+'</td>';
		row += '<td class="d-none">'+moment(value.account.time_added).fromNow();+'</td>';
	}
	row += '<td class="">';
	if(value.group.name!="Responden"){
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	}
	
	if(roleList.split(",").includes("ROLE_SPJK_SATKER") && value.group.name=="Satker"){
		
	}else{
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	}
	row += '<a href="javascript:void(0)" class="btn btn-sm text-primary" type="button" onclick="doAction(\''+value.id+'\',\'reset\')"><i class=""></i>Reset</a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	//else if(action=="reset") ajaxPOST(path_account + '/'+id+'/'+selected_action+'?force=true',{},'onModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+selected_action+'?force=true',{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data.account;
//	console.log(value);
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(response.data);
		selected_account_id = value.account.id;
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'reset'){
		selected_account_id = value.account.id;
		Swal.fire({
		  title: 'Apakah anda yakin?',
		  text: "Anda akan mereset password akun ini..",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan, reset!'
		}).then((result) => {
		  if (result.isConfirmed) {
			doAction(selected_id, selected_action, 1);
		  }
		})
	}
	
}

function onModalActionSuccess(response){
//	console.log(response);
//	console.log(selected_action);
	// Kalo mau refresh semua data yang tampil di table
	// 

	if(selected_mode=="multiform"){
		window
	}else{
		// Update data ke existing tabel
		if(selected_action == 'add'){
			appendRow(response.data, 'add');
		}else if(selected_action == 'edit' || selected_action == 'reset'){
			updateRow(response.data);
		}else if(selected_action == 'delete'){
			removeRow(response.data);
		}
		
		
	}
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');

//    Swal.fire(
//      'Reset!',
//      'Password akun berhasil direset.',
//      'success'
//    )
	//selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function fillFormValue(data){
	var value=data.account
	var value_unit_auditi=data.unit_auditi
	clearForm();
	
	$('[name=username]').val(value.account.username);
	$('[name=first_name]').val(value.account.first_name);
	$('[name=last_name]').val(value.account.last_name);
	$('[name=email]').val(value.account.email);
	$('[name=mobile]').val(value.account.mobile);
	$('[name=unit_auditi_id]').val(value_unit_auditi).trigger('change');
	$('[name=group_id]').val(value.group.id).trigger('change');
	if(value.account.enabled == false) $('[name=enabled]').prop('checked', true);
}

function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_mode=="multiform"){
	    ajaxPOST(path_account + '/savemulti',obj,'onModalActionSuccess','onModalActionError');
	}else{
		if(selected_account_id != '') obj.append('id', selected_account_id);
	    ajaxPOST(path_account + '/save',obj,'onModalActionSuccess','onModalActionError');
	}
}



