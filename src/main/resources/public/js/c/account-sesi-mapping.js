var pathsql = ctx + '/restql';
var path = ctx + '/authorities';
var path_account = ctx + '/account';
var path_sesi = ctx + '/sesi';
var path_simpeg = ctx + '/simpeg';
var path_position = ctx + '/position';
var path_group = ctx + '/account/group';
var selected_id = '';
var selected_account_id = '';
var current_page = 1;
var selected_action = '';
var selected_sesi = ($.urlParam('id_sesi')==null?"":$.urlParam('id_sesi'));
var selected_mode = ($.urlParam('mode')==null?"":$.urlParam('mode'));
var params = '';
var appendIds = [];

function init(){
	displaySesi();
	getSelectUnitAuditi();
	getSelectKelompokJabatan();

	if(selected_mode=="multiform") {
		selected_id = '';
		selected_account_id = '';
		selected_action = 'add';
		clearForm();
		$('#modal-form').modal({backdrop: 'static', keyboard: false});
	}
	
	if(selected_sesi!=""){
		display();
	}
	
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_account_id = '';
		selected_action = 'add';
		clearForm();

		$('#modal-form').modal({backdrop: 'static', keyboard: false});
	});

	
	$('[name=filter_group]').change(function(e){
		$('#search-form').submit();
	});
	
	$('.filter-by-kriteria').on('click', function(){
		console.log(this.dataset.column);
		///function changeFilter(col, label){
			/*if (col == '') {
				label = 'Kriteria Pencarian';
				$('#txtsearch').val('');
			}
			$('#column').data('id', col);
			$('#column').text(label);
			;*/
		//}
		if(this.dataset.column!=undefined){
			$('[name=filter_keyword]').data('column', this.dataset.column);
			$('[name=filter_keyword]').focus();
			$('#btn-label').text(this.text);
		}else{
			$('[name=filter_keyword]').val("");
			$('[name=filter_keyword]').focus();
		}
	});
	
	$('#filter_action').change(function(e){
		if(this.value=="deleteall" && $('[name=cek_id_account]:checked').length >0){
			var obj = new FormData();
			$.each($('[name=cek_id_account]:checked'), function(){
				obj.append('cek_id_account', this.value);
			});
			ajaxPOST(path_account + '/sesi-mapping/delete',obj,'onSaveModalActionSuccess','onModalActionError');
		}else{
			alert("account belum dipilih");
			$('#filter_action').val("0");
		}
	});
	
	$('[name=mode_copy]').change(function(e){
		if(this.value==1){
			$('[name=cek_all_account_modal]').prop('disabled', false)
			$('[name=cek_id_account_modal]').prop('disabled', false)
		}else if(this.value==2){
			$('[name=cek_id_account_modal]').prop('disabled', true)
			$('[name=cek_all_account_modal]').prop('disabled', true)
		}
	});
	
	$('[name=group_id]').change(function(e){
		if(this.value=="2002300" || this.value=="2002200"){
			$('#form-unit-auditi').show();
			$('#form-unit-auditi').removeClass('d-none');
			if(this.value=="2002200"){
				//$('#form-jabatan').show();
				$('#form-jabatan').removeClass('d-none');
			}else{
				$('#form-jabatan').addClass('d-none');
				$('[name=id_kelompok_jabatan]').val('').trigger('change');
				$('[name=id_jabatan]').val('').trigger('change');
			}
		}else{
			$('#form-unit-auditi').addClass('d-none');
			$('#form-unit-auditi').hide();
			$('[name=unit_auditi_id]').val('').trigger('change');
			$('#form-jabatan').addClass('d-none');
			$('[name=unit_auditi_id]').val('').trigger('change');
			$('[name=id_kelompok_jabatan]').val('').trigger('change');
			$('[name=id_jabatan]').val('').trigger('change');
		}
	});

	$('[name=id_kelompok_jabatan]').change(function(e){
		getSelectJabatan(this.value);
	});
	
	$('#search-form').submit(function(e){
		//display(1,"id_kelompok_jabatan.eq('"+$('#myTab .active').data('id')+"')");
		var params="id_kelompok_jabatan.eq('"+$('#myTab .active').data('id')+"')";
		if($('#myTab .active').data('id')=="NON_TEKNIS") params="";
		display(1,params);
		e.preventDefault();
	});
	
	$('#entry-form-modal').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});

	$('#btn-lookup').click(function(e){
		getPegawai();
		
	});
	
	$('#modal-form').on('hidden.bs.modal', function (e) {
		if(selected_mode=="multiform") {
			window.location.href="account"
		}

		$('#form-unit-auditi').addClass('d-none');
		$('[name=unit_auditi_id]').val('').trigger('change');
	})
	

	// Listen for click on toggle checkbox
	$('[name=cek_all_account]').click(function(event) { 
		var arr=[];
	    if(this.checked) {
	        // Iterate each checkbox
	        $(':checkbox').each(function() {
	            this.checked = true;   
	        });
	    } else {
	        $(':checkbox').each(function() {
	            this.checked = false;                       
	        });
	    }
	    $("input:checkbox[name=cek_id_account]:checked").each(function(){
            arr.push(this.value);
	    });
        $('[name=id]').val(arr);
	});
	$('[name=cek_all_account_modal]').click(function(event) { 
		var arr=[];
	    if(this.checked) {
	        // Iterate each checkbox
	        $(':checkbox').each(function() {
	            this.checked = true;   
	        });
	    } else {
	        $(':checkbox').each(function() {
	            this.checked = false;                       
	        });
	    }
	    $("input:checkbox[name=cek_id_account_modal]:checked").each(function(){
            arr.push(this.value);
	    });
        //$('[name=id]').val(arr);
	});
	
}

function filterModalUser(){
	var params="id_kelompok_jabatan.eq('"+$('#myTabModal1 .active').data('id')+"')"
	if($('#myTabModal1 .active').data('id')=="NON_TEKNIS") {
		if($('[name=filter_username]').val()!='') params="username_account.eq('"+$('[name=filter_username]').val()+"')";
	}else{
		if($('[name=filter_username]').val()!='') params+=",username_account.eq('"+$('[name=filter_username]').val()+"')";
	}
	displayGeneralUser(1,params);
}

function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(pathsql + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}

function setMode(value){
	$('#myTab .active').data('id', value);
	$('#myTabModal1 .active').data('id', value);
	$("[name=filter_username]").val('');
}

function onGetSelectKelompokJabatan(response){
	var tabs="";
	params='onclick="setMode(\''+'NON_TEKNIS'+'\');display(1)"';
	var defaulttab='<li class="nav-item" role="presentation">';
	defaulttab+='<a data-id="NON_TEKNIS" class="nav-link '+("active show")+'" '+params+' data-toggle="tab" href="#nav-home" role="tab">'+"ALL"+'</a>';
	defaulttab+='</li>';
	tabs+=defaulttab;
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		if(key==0) $('[name=id_kelompok_jabatan]').val(this.id);
		var params='onclick="display(1,\''+'id_kelompok_jabatan.eq'+"("+ value.id +")"+'\');"';
		tabs+='<li class="nav-item" role="presentation">';
			tabs+='<a data-id="'+value.id+'" class="nav-link '+(key==0?"actives":"")+'" '+params+' data-toggle="tab" href="#nav-home" role="tab">'+value.kode+' <span class="d-none badge badge-primary">4</span></a>';
		tabs+='</li>';
	});
	$('#myTab').html(tabs);
	
	var tabs=defaulttab;
	$.each(response.data, function(key, value) {
		var params='onclick="display(1,\''+'id_kelompok_jabatan.eq'+"("+ value.id +")"+'\');"';
		tabs+='<li class="nav-item" role="presentation">';
			tabs+='<a data-id="'+value.id+'" class="nav-link '+(key==0?"actives":"")+'" '+params+' data-toggle="tab" href="#nav-home" role="tab">'+value.kode+'</a>';
		tabs+='</li>';
	});
	$('#myTabModal').html(tabs);
	
	
	params='onclick="setMode(\''+'NON_TEKNIS'+'\');displayGeneralUser(1)"';
	var tabs='<li class="nav-item" role="presentation">';
	tabs+='<a data-id="NON_TEKNIS" class="nav-link '+("active show")+'" '+params+' data-toggle="tab" href="#nav-home" role="tab">'+"ALL"+'</a>';
	tabs+='</li>';
	$.each(response.data, function(key, value) {
		var params='onclick="displayGeneralUser(1,\''+'id_kelompok_jabatan.eq'+"("+ value.id +")"+'\');"';
		tabs+='<li class="nav-item" role="presentation">';
			tabs+='<a data-id="'+value.id+'" class="nav-link '+(key==0?"actives":"")+'" '+params+' data-toggle="tab" href="#nav-home-modal" role="tab">'+value.kode+'</a>';
		tabs+='</li>';
	});
	$('#myTabModal1').html(tabs);
}

function getSelectJabatan(id,e){
	//$('[name=id_jabatan]')
	$(e.children).empty();
	$(e.children).append(new Option('Pilih Bidang Teknis', ''));

	var qry="join=kelompok_jabatan,jenjang_jabatan,tingkat_jabatan";
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')";
	if(id==undefined) params="";
	ajaxGET(pathsql + '/jabatan?'+params+'&'+qry,'onGetSelectBidangTeknis','onGetSelectError', e);
}
function onGetSelectBidangTeknis(response, e){
	$.each(response.data, function(key, value) {
		$(e.children).append(new Option(value.kelompok_jabatan.kode+" "+value.tingkat_jabatan.nama+" "+value.jenjang_jabatan.nama, value.id));
	});
	
	if(e!=undefined){
		console.log(e.children);
		console.log(e.parentElement.parentElement.dataset.jabatan_id);
		$(e.children).val(e.parentElement.parentElement.dataset.jabatan_id);
	}
}

function getSelectUnitAuditi(e){
	console.log(e);
	$('[name=unit_auditi_id]').empty();
	$('[name=unit_auditi_id]').append(new Option('Select Satker', ''));
	
	//$('[name=id_unit_auditi]')
	if(e!=undefined){
		$(e.children).empty();
		$(e.children).append(new Option('Select Satker', ''));
	}
	var qry="";
	var params="limit=1000";
	var page=1;
	ajaxGET(ctx + '/ref/unit-auditi/list?page='+page+'&'+params+'&'+qry,'onGetSelectUnitAuditi','onGetSelectError', e);
}
function onGetSelectUnitAuditi(response, e){
	console.log(response);
	$.each(response.data, function(key, value) {
		$('[name=unit_auditi_id]').append(new Option(value.nama_unit_auditi, value.id_unit_auditi));
		//$('[name=id_unit_auditi]').
		
		if(e!=undefined) $(e.children).append(new Option(value.nama_unit_auditi, value.id_unit_auditi));
	});
	if(e!=undefined){
		console.log(e.children);
		console.log(e.parentElement.parentElement.dataset.unit_auditi_id);
		$(e.children).val(e.parentElement.parentElement.dataset.unit_auditi_id);
	}
}


function renderForm(){
	var template = '<div class="form-row pt-3">'+
	'    <div class="form-group col-md-12"><strong class="control-label">Daftar Responden : </strong></div>'+
	'</div>'+
	'<div class="table-responsive">'+
	'    <table id="tbl-responden" class="table table-sm table-bordered table-hover" width="100%" cellspacing="0">'+
	'        <thead>'+
	'            <tr>'+
	'                <th width="5%">No</th>'+
	'                <th width="20%">NIP</th>'+
	'                <th width="20%">Nama</th>'+
	'                <th width="20%">Satker</th>'+
	'                <th width="20%">Jabatan</th>'+
	'            </tr>'+
	'        </thead>'+
	'        <tbody></tbody>'+
	'    </table>'+
	'</div>';
	$('#myTabForm').html(template);
}

function createFormResponden(){
	var temp="";
	for(var i=1; i <= $('[name=jumlah_responden]').val(); i++){
		temp += '<tr class="data-row-modal" id="rowresponden-'+i+'">'+
		'    <td>'+i+'</td>'+
		'    <td class="d-none"><input name="id" type="text"/></td>'+
		'    <td class=""><input onchange="getPegawai(this)" name="username" type="text" class="form-control"/></td>'+
		'    <td class=""><input name="nama" type="text" class="form-control"/></td>'+
		'    <td class=""><select name="id_unit_auditi" class="form-control form-control-sm"></select></td>'+
		'    <td class=""><select name="id_jabatan" class="form-control form-control-sm"></select></td>'+
		'</tr>';
	}
	$('#tbl-responden tbody').html(temp);
	

	getSelectJabatan($('#myTab .active').data('id'));
	getSelectUnitAuditi();
}

var temp_val;
var temp_elem;
function getPegawai(e){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	console.log(e.parentElement.parentElement);
	var nip = $(e).val();
	temp_val=e.parentElement.parentElement;
	temp_elem=e;
	ajaxGET(path_simpeg + '/pegawai?filter_nip='+nip,'onGetPegawaiSuccess','onGetError');
}

function onGetPegawaiSuccess(response){
	console.log(response);
	if(response.data!=null){
		temp_val.children[3].children[0].value=response.data.nama;
//		temp_val.children[4].innerText=response.data.nama;
//		temp_val.children[5].innerText=response.data.nama_unit_auditi;
//		temp_val.children[6].innerText=response.data.kode_jabatan;
		temp_val.setAttribute("class", "");

		temp_val.children[4].children[0].value=response.data.id_unit_auditi;
	}else{
//		temp_val.children[3].children[0].value="";
//		temp_val.children[4].innerText="";
//		temp_val.children[5].innerText="";
//		temp_val.children[6].innerText="";
		temp_val.setAttribute("class", "bg-danger text-white");

	}
	$.LoadingOverlay("hide");
}

function getListPosition(){
	ajaxGET(path_reference + '/list/account/position','onGetPositionSuccess','onGetError');
}
function onGetPositionSuccess(response){
	$.each(response.data, function(key, value) {
		$('[name=position_id]').append(new Option(value.name, value.id));
	});
	state_list_position = true;
	set_if_true();
}

function add(mode=""){
	selected_mode=mode;
	selected_id = '';
	selected_account_id = '';
	selected_action = 'add';
	clearForm();
	if(mode=="CREATED"){
		$('#modal-form-create-user').modal('show');
	}else{
		$('#modal-form').modal('show');
		if(mode=="PNS") $("#form-pns").show();
		else $("#form-pns").hide();
		if(mode=="NON_PNS") $("#form-non-pns").show();
		else $("#form-non-pns").hide();
	}
}


function displaySesi(){
	var tbody=$('#container-sesi');
	//$('[name=filter_group]').empty();
	//$('[name=filter_group]').append(new Option('All Groups', ''));
	tbody.append('<a href="#" class="list-group-item list-group-item-action text-center" id="loading-row"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</a>');
	ajaxGET(path_sesi + '/list?','onDisplaySesi','onGetSelectError');
}
function onDisplaySesi(response){
	var row="";
	var tbody=$('#container-sesi');
	
	$('[name=from_sesi_id]').append(new Option("Master User", 000));
	$.each(response.data, function(key, value) {
		row +='<a data-id="'+value.id+'" href="?id_sesi='+value.id+'" class="list-group-item list-group-item-action '+(value.id==selected_sesi?"active":"")+'">'+value.kode+" "+(value.active==1?"<i class='fas fa-check-circle text-success'></i>":"")+'</a>';
		if(selected_sesi!=value.id) {
			$('[name=from_sesi_id]').append(new Option(value.kode, value.id));
		}
		$('[name=sesi_id]').append(new Option(value.kode, value.id));
	});
	$('[name=sesi_id]').val(selected_sesi);
	tbody.html(row);
	
	$('#container-sesi').find('.active').trigger('click');
}

function display(page = 1, params=""){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	$('#total-responden').html("-");
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	if(params!="") {
		var param=","+params;
		params="filter=id_sesi.eq('"+selected_sesi+"')";
		params+=param;
	}else{
		params+="filter=id_sesi.eq('"+selected_sesi+"')";
		//params+=",id_kelompok_jabatan.eq('"+1+"')";
	}	
	if($('[name=filter_keyword]').val()!='') {
		var kolom=$('[name=filter_keyword]').data('column');
		var operator = "eq";
		if(kolom!="username_account") operator="like";
		params+=","+$('[name=filter_keyword]').data('column')+"."+operator+"('"+$('[name=filter_keyword]').val()+"')";
	}
	setTimeout(() => {
		//view_account_user_responden
		ajaxGET(pathsql + '/view_mapping_jabatan_unit_auditi?page='+page+'&'+params+'&limit=100&join=account,unit_auditi','onGetListSuccess','onGetListError');
	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
//	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row-account').length+1;
	$.each(response.data,function(key,value){
		//if(!contains.call(appendIds, value.id)){

			//if($('#myTab .active').data('id')==value.jabatan.id_kelompok){
			row += renderRow(value, num);
			num++;
			//}
		//}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	if(typeof removeHtmlData === "function") removeHtmlData();
	$('#total-responden').html(response.data.length);
}

function renderRow(value, num, method=''){
//	console.log(value);
	var row = "";
	if(method != 'replace') {
		row += '<tr data-unit_auditi_id="'+value.unit_auditi.id+'" data-unit_auditi_nama="'+value.unit_auditi.nama+'"  data-jabatan_nama="'+value.nama_master_jabatan+'" data-jabatan_id="'+value.id_jabatan+'" class="data-row-account" id="row-'+value.id+'">';
	}
	row += '<td class="_not_satker"><input type="checkbox" name="cek_id_account" value="'+value.id+'"></td>'
	row += '<td>'+(num)+'</td>';
	var status_responden = value.status!=undefined?value.status:"NO_PROGRESS";
	row += '<td class="">'+value.account.first_name+" "+value.account.last_name+"<br/>"+value.account.username+'<br/><span class="badge '+(status_responden=="SELESAI"?"badge-success":status_responden=="NO_PROGRESS"?"badge-warning":"badge-primary")+'">'+status_responden+'<span></td>';
	row += '<td class="d-none">'+value.account.first_name+" "+value.account.last_name+'</td>';
	//row += '<td class="">Satker:'+value.unit_auditi.nama+'<br/>Jabatan:'+value.nama_master_jabatan+'</td>';
	row += '<td class=""><div ondblclick="updateSatker(this)">Satker:'+value.unit_auditi.nama+'</div><div ondblclick="updateJabatan(this)">Jabatan:'+value.nama_master_jabatan+'</div></td>';
	row += '<td class="_not_satker">';
//	if(value.account.enabled==true){
//		row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
//	}else{
//		row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
//	}
//	row += '<td class="d-none">'+moment(value.account.time_added).fromNow();+'</td>';
	
	/*if(method=="add"){
		row += '<td class="">'+value.account.username+'</td>';
		row += '<td class="">'+value.account.first_name+" "+value.account.last_name+'</td>';
		if(value.account.enabled==true){
			row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
		}else{
			row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
		}
		row += '<td class="d-none">'+moment(value.account.time_added).fromNow();+'</td>';
	}else{
		row += '<td class="">'+value.account.username+'</td>';
		row += '<td class="">'+value.account.first_name+" "+value.account.last_name+'</td>';
		if(value.account.enabled==true){
			row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
		}else{
			row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
		}
		row += '<td class="d-none">'+moment(value.account.time_added).fromNow();+'</td>';
	}
	row += '<td class="">';
	if(value.group.name!="Responden"){
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	}*/
	
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="editThisForm(\''+value.id+'\',this)"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	return row;
}

function editThisForm(id, elem){
	console.log(id);
	//console.log(elem.parentElement.parentElement.cells[4].children);
	
	var newelem = elem.parentElement.parentElement.cells;
	console.log(elem!="save");
	if(elem.dataset.mode=="save"){
		var url = pathsql+'/mapping_jabatan_unit_auditi/save?append=false&mode=1';
		window.obj = {
			data : {
				id : id,
				id_unit_auditi : newelem[4].children[0].children[0].value,
				id_jabatan : newelem[4].children[1].children[0].value
			},
		};
		console.log(obj);
		//
		var newkolom="<td class='text-center' colspan='6'><img width='20' src='"+ctx+"/images/loading-spinner.gif'/> Mohon Tunggu..</td>";
		console.log(elem.parentElement.parentElement);
		elem.parentElement.parentElement.innerHTML=newkolom
		//
		$.ajax({
	        url : url,
	        type : "POST",
	        traditional : true,
	        contentType : "application/json",
	        dataType : "json",
	        data : JSON.stringify(obj),
	        success : function (response) {
	        		console.log(response);
					//newelem[4].children[0].innerHTML='Satker:'+elem.parentElement.parentElement.dataset.unit_auditi_nama;
					//newelem[4].children[1].innerHTML='Jabatan:'+elem.parentElement.parentElement.dataset.jabatan_nama;
					
					//newelem[5].children[0].outerHTML='<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="editThisForm(\''+id+'\',this)"><i class="fas fa-fw fa-pencil-alt"></i></a> '
					//newelem[5].children[1].outerHTML='<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>'
					display(1,"id_kelompok_jabatan.eq('"+$('#myTab .active').data('id')+"')");
	        },
	        error : function (response) {
	        		alert("gagal, mohon maaf, silahkan coba lagi..");
	        },
	    });
		//end save
	}else if(elem.dataset.mode=="cancel"){
		newelem[4].children[0].innerHTML='Satker:'+elem.parentElement.parentElement.dataset.unit_auditi_nama;
		newelem[4].children[1].innerHTML='Jabatan:'+elem.parentElement.parentElement.dataset.jabatan_nama;
		
		newelem[5].children[0].outerHTML='<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="editThisForm(\''+id+'\',this)"><i class="fas fa-fw fa-pencil-alt"></i></a> '
		newelem[5].children[1].outerHTML='<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>'
	}else if(elem!="save"){
		newelem[4].children[0].innerHTML='<select name="id_unit_auditi" class="form-control form-control-sm"></select>';
		newelem[4].children[1].innerHTML='<select name="id_jabatan" class="form-control form-control-sm"></select>';
		
		//update form
		newelem[5].children[0].outerHTML='<a data-mode="save" href="javascript:void(0)" class="btn btn-sm" type="button" onclick="editThisForm(\''+id+'\',this)"><i class="fas fa-fw fa-check"></i></a> '
		newelem[5].children[1].outerHTML='<a data-mode="cancel" href="javascript:void(0)" class="btn btn-sm" type="button" onclick="editThisForm(\''+id+'\',this)"><i class="fas fa-fw fa-times-circle"></i></a> '
		//render data
		//getSelectJabatan($('#myTab .active').data('id'));
		getSelectUnitAuditi(newelem[4].children[0]);
		getSelectJabatan(undefined,newelem[4].children[1]);
	}else{
		newelem[4].children[0].innerHTML='<select name="id_unit_auditi" class="form-control form-control-sm"></select>';
		newelem[4].children[1].innerHTML='<select name="id_jabatan" class="form-control form-control-sm"></select>';
		
		//update form
		newelem[5].children[0].outerHTML='<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="editThisForm(\''+id+'\',this)"><i class="fas fa-fw fa-pencil"></i></a> '
	}
}

function onSaveThisFormSuccess(response, container){
	console.log(response);
}

function appendRow(value, action){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num, action);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(pathsql + '/mapping_jabatan_unit_auditi/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	//else if(action=="reset") ajaxPOST(path_account + '/'+id+'/'+selected_action+'?force=true',{},'onModalActionSuccess','onActionError');
	else ajaxPOST(pathsql + '/mapping_jabatan_unit_auditi/'+id+'/'+selected_action+'?force=true',{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
//	console.log(value);
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(response.data);
		selected_account_id = value.account.id;
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'reset'){
		selected_account_id = value.account.id;
		Swal.fire({
		  title: 'Apakah anda yakin?',
		  text: "Anda akan mereset password akun ini..",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Lanjutkan, reset!'
		}).then((result) => {
		  if (result.isConfirmed) {
			doAction(selected_id, selected_action, 1);
		  }
		})
	}
	
}

function onModalActionSuccess(response){
//	console.log(response);
//	console.log(selected_action);
	// Kalo mau refresh semua data yang tampil di table
	// 

	if(selected_mode=="multiform"){
		window
	}else{
		// Update data ke existing tabel
		if(selected_action == 'add'){
			appendRow(response.data, 'add');
		}else if(selected_action == 'edit' || selected_action == 'reset'){
			updateRow(response.data);
		}else if(selected_action == 'delete'){
			removeRow(response.data);
		}
		
		
	}
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');

//    Swal.fire(
//      'Reset!',
//      'Password akun berhasil direset.',
//      'success'
//    )
	//selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function fillFormValue(data){
	var value=data.account
	var value_unit_auditi=data.unit_auditi
	clearForm();
	
	$('[name=username]').val(value.account.username);
	$('[name=first_name]').val(value.account.first_name);
	$('[name=last_name]').val(value.account.last_name);
	$('[name=email]').val(value.account.email);
	$('[name=mobile]').val(value.account.mobile);
	$('[name=unit_auditi_id]').val(value_unit_auditi).trigger('change');
	$('[name=group_id]').val(value.group.id).trigger('change');
	if(value.account.enabled == false) $('[name=enabled]').prop('checked', true);
}

function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form-modal'));
	
	if($('[name=mode_copy]').val()==1){
		//$('[name=cek_all_account_modal]').prop('disabled', false)
		//$('[name=cek_id_account_modal]').prop('disabled', false)
	}else if($('[name=mode_copy]').val()==2){
		//$('[name=cek_id_account_modal]').prop('disabled', true)
		//$('[name=cek_all_account_modal]').prop('disabled', true)
		obj.append('kelompok_jabatan_id', $('#myTabModal1 .active').data('id'));
	}
	ajaxPOST(path_account + '/sesi-mapping/save',obj,'onSaveModalActionSuccess','onModalActionError');
}

function onSaveModalActionSuccess(response){
	console.log(response);
	window.location.reload();
}

function displayGeneralUser(page = 1, params=""){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	
	$('#more-button-row-modal1').remove();
	$('#retry-button-row-modal1').remove();
	
	var tbody = $("#tbl-data-modal1").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form-modal-created" ).serialize() || page ==1){
	if(page ==1){
		tbody.text('');
	}
	
	tbody.append('<tr id="loading-row-modal-1"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	if(params!='') {
		var param=params;
		params="filter=id_sesi.eq('"+$('[name=from_sesi_id]').val()+"')";
		params+=","+param;
	}else{
		params+="filter=id_sesi.eq('"+$('[name=from_sesi_id]').val()+"')";
		//params+=",id_kelompok_jabatan.eq('"+$('#myTabModal1 .active').data('id')+"')";
	}
	setTimeout(() => {
		ajaxGET(pathsql + '/view_mapping_jabatan_unit_auditi?page='+page+'&'+params+'&limit=100&join=account,unit_auditi','onGetListGeneralUserSuccess','onGetListError');
	}, 1000);
	
	current_page = page;
}

function onGetListGeneralUserSuccess(response){
	console.log(response);
	$.LoadingOverlay("hide");
	$('#loading-row-modal-1').remove();
	$('#no-data-row-modal-1').remove();
	
	var tbody = $("#tbl-data-modal1").find('tbody');
	
	var row = "";
	var num = $('.data-row-modal1').length+1;
	
	var id_kel_jab=0;
	$.each(response.data,function(key,value){
		row += '<tr class="data-row-modal1" id="row-'+value.id+'">';
		row += '<td><input type="checkbox" name="cek_id_account_modal" value="'+value.id+'"></td>'
		row += '<td>'+(num)+'</td>';
		row += '<td class="">'+value.account.username+'</td>';
		row += '<td class="">'+value.account.first_name+" "+value.account.last_name+'</td>';
		
		//row += '<td class=""><select name="id_unit_auditi" class="form-control form-control-sm"></select> <select name="id_jabatan" class="form-control form-control-sm"></select><div ondblclick="updateSatker(this)">Satker:'+value.unit_auditi.nama+'</div><div ondblclick="updateJabatan(this)">Jabatan:'+value.nama_master_jabatan+'</div></td>';
		row += '<td class=""><div ondblclick="updateSatker(this)">Satker:'+value.unit_auditi.nama+'</div><div ondblclick="updateJabatan(this)">Jabatan:'+value.nama_master_jabatan+'</div></td>';
		
		//row += '<td class=""><select name="id_unit_auditi" class="form-control form-control-sm"></select></td>'+
		//'    <td class=""><select name="id_jabatan" class="form-control form-control-sm"></select></td>';
		
		/*row += '<tr class="data-row-modal1" id="row-'+value.id+'">';
		row += '<td><input type="checkbox" name="cek_id_account_modal" value="'+value.id+'"></td>'
		row += '<td>'+(num)+'</td>';
			row += '<td class="">'+value.account.username+'</td>';
			row += '<td class="">'+value.account.first_name+' '+value.account.last_name+'</td>';
			row += '<td class="">'+value.group.name+'</td>';
			if(value.account.enabled==true){
				row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
			}else{
				row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
			}
			row += '<td class="d-none">'+moment(value.account.time_added).fromNow();+'</td>';;*/
		row += '</tr>'
		num++;
		id_kel_jab=value.id_kelompok_jabatan;
	});
	if(response.next_more){
		row += '<tr id="more-button-row-modal1" class="more-button-row-row-modal1"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="displayGeneralUser('+response.next_page_number+',\''+'id_kelompok_jabatan.eq'+"("+ id_kel_jab +")"+'\');">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
	$('[name=cek_id_account_modal]').click(function(event) { 
		$("[name=cek_all_account_modal]").each(function() {
            this.checked = false;                       
        });
	});
	
	//getSelectJabatan($('#myTab .active').data('id'));
	//getSelectUnitAuditi();
}

function updateJabatan(e){
	console.log(e);
}

function updateSatker(e){
	console.log(e);
}

function onGetListErrors(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}