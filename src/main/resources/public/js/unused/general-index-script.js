var url = ctx + "/sesi/";
var url_responden = ctx + "/responden/";

var param = "";
var selected_id='';
function init() {
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));

	if(selected_id != "") cek_status_responden();
	else checkResponden();
}

function cek_status_responden(){
	ajaxGET(url_responden + selected_id,'onCheckSuccess','onCheckError');
}

function onCheckSuccess(response){
	console.log(response);
	var value = response.data;
	$('.container').removeClass("d-none");
	if(value.status_responden=="SELESAI") {
		$('#msg-survey').html("<h4>Terima Kasih Telah mengikuti Survey</h4>");
	}else{
		if(value.periode_awal_expired < 0 && value.periode_akhir_expired < 0 ){
			console.log('expired');
			$('#msg-survey').html("<h4>Anda sudah melewati batas akhir periode survey.</h4>");
			/*
			var obj = new FormData();
			obj.append('id', selected_id);
			obj.append('saran', "Added by: System");
		    ajaxPOST(url_responden + 'save',obj,'onSaveEndSuccess','onSaveEndError');
		    */
		}else{

			window.location.href=ctx+"/page/survey?id="+response.data.id_responden;
		}
	}
}

function onSaveEndSuccess(resp){
	$('#msg-survey').html("<h5>Mohon maaf periode sesi anda telah habis. <br/><small>Anda tidak dapat mengikuti survey kembali.</small></h5>");
	$('#msg-survey').addClass("alert-danger");
}

function onSaveEndError(resp){
	$('#msg-survey').html("<h5>Cek Sesi GAGAL. <br/><small>404</small></h5>");
	$('#msg-survey').addClass("alert-danger");
}


function checkResponden(){
	$.getJSON(url_responden+'list', function(response_responden) {
		console.log("responden", response_responden);
		var check=0;
		var status="";
		var id_responden=0;
		var surat_tugas=0;
		if (response_responden.code == 200) {
			//
			$.getJSON(url+'user/responden/list', function(response_user_responden) {
				console.log("user_responden(sesi)", response_user_responden);
				if (response_user_responden.code == 200) {
					//check user diresponden
					$.each(response_responden.data,function(key){
						console.log(this.user_id_responden==user_id);
						if(this.user_id_responden==user_id){
							//alert(1)
							check=1;
							id_responden=this.id_responden;
							status=this.status_responden
						}
					});
					//check sesi user
					$.each(response_user_responden.data,function(key){
						if(this.user_id_sesi_user_responden==user_id){
							surat_tugas=this.sesi_id_sesi_user_responden;
						}
					});
					var obj = new FormData();

					if(check == 1) {
						obj.append('id', id_responden);
					}
					obj.append('user_id', user_id);
					obj.append('surat_tugas_id', surat_tugas);
					console.log(surat_tugas);
					if(status=="SELESAI") {
						$('.container').removeClass("d-none");
						$('#msg-survey').html("<h4>Terima Kasih Telah mengikuti Survey</h4>");
					}else{
						ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
					}
				}
			});
		}
	});
}

function save(){
	var obj = new FormData(document.querySelector('#form-surat-tugas'));
	if($(".list-group .list-group-item.active").data('responden') != undefined) obj.append('id', $(".list-group .list-group-item.active").data('responden'));
	
	if($(".list-group .list-group-item.active").length > 0)
	obj.append('surat_tugas_id', $(".list-group .list-group-item.active").data('id'));
    console.log(obj);
    ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
//	$("#progressBar").hide();
//	$("#modalPengguna").modal('hide');
//	refresh();
//	reset();
	window.location.href=ctx+"/page/survey?id="+response.data.id_responden;
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	console.log(response);
	addAlert('msg', "alert-danger", "kesalahan dari server");
	if(response.responseJSON.code=="409"){
		alert(response.responseJSON.message)
		//$('.container').removeClass("d-none");
	}else{
		$('.container').removeClass("d-none");
		$('.container').find('.card-body').addClass("alert-danger");
		//$('.container').find('.card-body').html("<h4>Tidak tersedia survey untuk anda</h4><a href='"+ctx+"/admin/'>kembali ke menu utama.</a>");
		$('.container').find('.card-body').html("<h4>Tidak tersedia survey untuk anda</h4>");
	}
}



function getSuratTugasList(){
	var body=$("#wraper-surat-tugas");
	var myvar="";
	//setLoad('wraper-surat-tugas');
	$.getJSON(url+'list', function(response_surat_tugas) {
		console.log("sesi", response_surat_tugas);
		if (response_surat_tugas.code == 200) {
			$.getJSON(url+'user/responden/list', function(response_user_responden) {
				console.log("user_responden(sesi)", response_user_responden);
				if (response_user_responden.code == 200) {
					$.getJSON(url_responden+'list', function(response_responden) {
						console.log("responden", response_responden);
						if (response_responden.code == 200) {
							$.each(response_surat_tugas.data, function(key, value_surat_tugas) {
								$.each(response_user_responden.data, function(key, value_user_responden) {
									var id_responden="";
									var status_responden="";
									var user_id="";
									var kegiatan_id="";
									$.each(response_responden.data, function(key, value_responden) {
										$('[name=user_id]').val(value_user_responden.user_id);
										$('[name=jenis_audit_id]').val(value_surat_tugas.id_kegiatan_pengawasan);
										user_id=value_user_responden.user_id;
										kegiatan_id=value_surat_tugas.id_kegiatan_pengawasan;
										//if(status.equals("SELESAI")){
											//out.print("<li onclick='setVal(this);' data-id="+su.getIdSuratTugas()+" class='list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"'>"+su.getNomorSuratTugas()+"<span class='badge badge-success badge-pill'>"+status+"</span></li>");
										//}else{
										//myvar += '<li onclick=\'setVal(this);\' data-id='+value.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"\'>'+value.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>"+status+"</span></li>';
										//}
										
										if(value_surat_tugas.id_sesi==value_responden.sesi_id_responden && value_user_responden.user_id==value_responden.user_id_responden) {
											id_responden= value_responden.id_responden;
											status_responden=value_responden.status_responden;
										}
										//
										//
										
									});
									if(status_responden=="SELESAI"){
										alert(status_responden);
//										if(value_surat_tugas.id_sesi==value_user_responden.sesi_id_sesi_user_responden)
//											myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_sesi+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center" disabled\'>'+value_surat_tugas.kode_sesi+'<span class="badge badge-success badge-pill">'+status_responden+'</span></li>';	
									}else{
										alert(status_responden);
										alert(id_responden);
										alert(user_id);
										alert(kegiatan_id);
//										obj.append('id', $(".list-group .list-group-item.active").data('responden'));
//									
//										if($(".list-group .list-group-item.active").length > 0)
//										obj.append('surat_tugas_id', $(".list-group .list-group-item.active").data('id'));
//									    
//										console.log(obj);
//									    ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
//										
//										if(value_surat_tugas.id_sesi==value_user_responden.sesi_id_sesi_user_responden)
//											myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_sesi+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center\'>'+value_surat_tugas.kode_sesi+'<span class=\'badge badge-warning badge-pill\'>'+status_responden+'</span></li>';
									}
									
									
								});
							});
							body.append(myvar);
						}
					});
					
				}
			});
		}
	});
}

//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

