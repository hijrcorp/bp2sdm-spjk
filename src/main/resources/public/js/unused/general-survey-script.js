var url = ctx + "/simulasi-survey/";
var url_responden = ctx + "/responden/";
var url_responden_jawaban = ctx + "/responden-jawaban/";
var urlExport = ctx +"/rest/export/";
var param = "";
var selected_id='';
var selected_id_responden_jawaban;
var mode='';
var judul_sesi='';

function init() {
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
	selected_page = ($.urlParam('page') == null ? '' : $.urlParam('page'));
	kegiatan_pengawasan_id = ($.urlParam('kegiatan_pengawasan_id') == null ? '' : $.urlParam('kegiatan_pengawasan_id'));
	
	cek_status_responden();
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#form-surat-tugas').submit(function(e){
		save();
		e.preventDefault();
	});
	$('.navbar-toggler ').addClass('d-none');
}
function cek_status_responden(){
	ajaxGET(url_responden + selected_id,'onCheckSuccess','onCheckError');
}

function onCheckSuccess(response){
//	alert(1);
	console.log(response);
	var value = response.data;
	judul_sesi=value.sesi.keterangan_sesi;
	//if(value.status_responden=="SELESAI") {
		if(value.periode_awal_expired < 0 && value.periode_akhir_expired < 0 || value.status_responden=="SELESAI")
		window.location.href="index?id="+value.id_responden;
	//}
	else {
		if(mode!='progress_to_answer')
		renderDisplay(1);
	}
}

function save(elem){
//	selected_id_responden_jawaban=$(elem).data('idRespondenJawaban');
//	console.log(elem);
//	console.log(selected_id_responden_jawaban);
//	var obj = new FormData(document.querySelector('#form-surat-tugas'));
	var obj = new FormData();
////	if(selected_id != '') obj.append('id', selected_id);
	if(selected_id != '') obj.append('header_responden', selected_id);
//	if(selected_id_responden_jawaban != undefined) obj.append('id', selected_id_responden_jawaban);
	obj.append('pertanyaan', $(elem).data('pernyataan'));
	obj.append('jawaban',  $(elem).data('jawaban'));
//    console.log(obj);
	mode='progress_to_answer';
    ajaxPOST(url_responden_jawaban + 'save',obj,'onSaveSuccess','onSaveError');
}

function saveSaran(){
	var obj = new FormData();
	if(selected_id != '') obj.append('id', selected_id);
	obj.append('saran', $('[name=isi]').val());
	mode='saran';
    ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	console.log(response);
//	$("#progressBar").hide();
//	$("#modalPengguna").modal('hide');
//	refresh();
//	reset();
//	window.location.href="survey?id="+response.data.id_responden;
	cek_status_responden();
	
	//refresh_label
	var count_pernyataan=$('#table-card-body').find('.data-row').length;
	var count_pernyataan_answer=$('#table-card-body').find('input:checked').length;
	$('#pernyataan-titles').html('Terjawab <strong class="text-success">'+count_pernyataan_answer+'</strong> dari <strong class="text-danger">'+count_pernyataan+'</strong> Pernyataan  ');	
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(page, '', 0);
}

//batas
function setLoad(elem){
	elem.html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var msg_exist="";
	$.each($('.data-row'), function(key){
		console.log($(this).find('input').is(':checked'));
		if($(this).find('input').is(':checked')==false){
			msg_exist="1"
		}
	});
	if(params=='back') {
		msg_exist="";
		params='';
	}
	if(msg_exist!="") {
		Swal.fire({
			  type: 'warning',
			  title: 'Oops...',
			  text: 'Maaf anda tidak dapat lanjut, sebelum anda mengisi semua jawaban anda.',
			});
		return;
	}
	var tbody = $("#table-card-body");
	setLoad(tbody);
//	params+="header_responden="+selected_id+"&kegiatan_pengawasan_id="+kegiatan_pengawasan_id+"&filter_status=1";
	params+="header_responden="+selected_id+"&filter_status=1";
	limit=1;
	if($.urlParam('confirm')=="true") limit="";
	if($.urlParam('confirm')=="finish") page=selected_page;
	
	setTimeout(() => ajaxGET(url + 'list-pernyataan?page='+page+'&limit='+limit+'&'+params,'onGetListSuccess','onGetListError'), timeout);
}

function onGetListSuccess(response){
	console.log(response);
	
	var tbody = $("#table-card-body");
//	var tfooter = $("#table-card-footer");
	var tfooter = $("#head-pagination");
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	
	row += render_row_card(response, num);
	
	if(response.next_more==false && (response.next_page_number-response.count)==2){
		//showConfirm('Mohon <strong>review</strong> kembali jawaban anda, sebelum lanjut.');
		//
		const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-success',
		    cancelButton: 'btn btn-secondary'
		  },
		  buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
		  title: 'Konfirmasi',
		  html: "Mohon <strong>review</strong> kembali jawaban anda, sebelum lanjut.",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Lanjut',
		  cancelButtonText: 'Review',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
			window.location.href=ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=finish&page='+(response.next_page_number);
//			window.location.href="/survey-itjen/page/survey?id=1571123338151699&kegiatan_pengawasan_id=&confirm=finish&page=7"
		  } else if (result.dismiss === Swal.DismissReason.cancel) {
			window.location.href=ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=true&page='+(response.next_page_number-1);
//			window.location.href="/survey-itjen/page/survey?id=1571123338151699&kegiatan_pengawasan_id=&confirm=true&page=6"
		  }
		});
		//
		//window.location.href=ctx+"/page/survey?id="+selected_id+"&kegiatan_pengawasan_id="+kegiatan_pengawasan_id+"&confirm=true&page="+(response.next_page_number-1);
		row='Mohon tunggu...';
		$('#modal-action').find('.btn-secondary').attr('onclick','window.location.href="'+ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=true&page='+(response.next_page_number-1)+'"');
		$('#modal-action').find('.btn-success').attr('onclick','window.location.href="'+ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=finish&page='+(response.next_page_number)+'"');
	}
	if(response.next_more==false && (response.next_page_number-response.count)==3){
		$('#pernyataan-title').html("<strong>Saran  </strong>");

		row = '<div class="card-header border mb-3 ">	    	<div class="row">			    <div class="col col-md-12" id="pernyataan-title"><strong>Saran  </strong></div>	</div>		</div>';
			

		row+='<textarea name="isi" class="form-control mb-3" placeholder="Beritahu kami saran dan masukan anda terhadap survey yang dilakukan, agar survey ini dapat menjadi lebih baik. (Opsional)" rows="6"></textarea>';
		row+='<br / > * Harap menekan tombol selesai di bawah ini untuk menyelesaikan survey.';
		$('#info-answers').addClass('d-none');
		$('#container-btn-end').find('button').attr('onclick', 'saveSaran()');
		$('#container-btn-end').find('button').text('Selesai');
	}
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);

	myvar = '';
	if(response.next_page_number > 2 || response.next_page_number > response.count)
	myvar += '<button onclick="renderDisplay('+(response.next_page_number-2)+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>';
	
	if(response.next_more==false && (response.next_page_number-response.count)==2){
		myvar += '<button onclick="saveSaran()" type="button" class="btn m-0 p-0"><i class="fas fa-check-circle text-success"></i></button>';
	}else{
		myvar += '<button onclick="renderDisplay('+response.next_page_number+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>';
	}
	console.log(myvar);
	tfooter.html(myvar);

	var count_pernyataan=$('#table-card-body').find('.data-row').length;
	var count_pernyataan_answer=$('#table-card-body').find('input:checked').length;
	$('#pernyataan-titles').html('Terjawab <strong class="text-success">'+count_pernyataan_answer+'</strong> dari <strong class="text-danger">'+count_pernyataan+'</strong> Pernyataan  ');	
	if($.urlParam('confirm')=="true") $('.title-pembatasa').remove();
}

function render_row_card(response, num, method='add'){
	data=response.data;
	console.log(data);
	var row = "";
	var count_pernyataan=0;
	if($.urlParam('confirm')=="true") {
		row += '<div class="card-header bg-warning text-white border mb-3 ">'+
	'	    	<div class="row">'+
'			    <div class="col col-md-12" id="pernyataan-title">Mohon <strong>Konfirmasi</strong> kembali jawaban anda sebelum lanjut</div>';
	row += '	</div>'+
		'		</div>';
	}

	if($.urlParam('confirm')=="finish"){
		$('#container-btn-end').removeClass("d-none");
		$('#container-btn-end').find('button').attr('onclick', 'renderDisplay('+(parseInt($.urlParam('page'))+1)+')');
		
	}

	row += '<div class="row ">'+
	'    <div class="col-md-12 text-center">'+
	'       <h5>'+judul_sesi+'</h5>'+
	'		<hr class="mb-2 mt-2">'+
	'	</div>'+
	'</div>';
	$.each(data.main,function(key,value_main){

			row += '<div class="card-header border mb-3 title-pembatas">'+

			'	    	<div class="row">'+
			'			    <div class="col-10 col-md-11" id="pernyataan-title"><strong>'+value_main.nama_aspek+'</strong></div>';
			
			row +='<div id="head-pagination" class="col-2 col-md-1 text-right">'

			if(response.next_page_number > 2 || response.next_page_number > response.count){
				row += '<button onclick="renderDisplay(\''+(response.next_page_number-2)+'\',\''+'back'+'\')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>';
			}
			if($.urlParam('confirm')!="true") {
				row += '<button onclick="renderDisplay('+response.next_page_number+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>';
			}
			row += '	</div>';
			row += '	</div>'+
			'			</div>';
		//row +='<h5 class="card-title">'+value_main.nama_aspek+'</h5>';
		if(data.penetapan_pernyataan.length != 0){
		var list_responden_jawaban = data.list_responden_jawaban;

		$.each(data.penetapan_pernyataan, function(key, value){
			console.log(value);
			if($.urlParam('confirm')=="true" && value.aspek_id_sesi_pernyataan==value_main.id_aspek){
				$('#container-btn-end').removeClass("d-none");
				$('#container-btn-end').find('button').attr('onclick', 'renderDisplay('+(parseInt($.urlParam('page'))+1)+')');
				
				if(method != 'replace') {
					row += '<div class="data-row disabled card mb-3" id="row-'+value.id_sesi_pernyataan_penetapan+'">';
				}
				
				row += //'<div class="card">'+
				'  <div class="card-header">'+
				'    '+value.isi_sesi_pernyataan+' <button onclick="showRemarks(\''+value.keterangan_sesi_pernyataan+'\')" type="button" class="btn m-0 p-0"><i class="fas fa-question-circle text-info"></i></button>'+"<strong class='d-none'>("+value.aspek_id_sesi_pernyataan+")</strong>"+
				'  </div>'+
				
				'  <ul class="list-group list-group-flush">';
				if(value.listSesiPernyataanJawaban != null){
					$.each(value.listSesiPernyataanJawaban, function(key, val_jawaban){
						row +='<li class="list-group-item list-group-item-action">';
							row += '<div class="form-check">';
							
							var cheked_radio="";
							if(list_responden_jawaban != null){
								$.each(list_responden_jawaban, function(key, dum_val){
									if(dum_val.jawaban_id_responden_jawaban == val_jawaban.id_sesi_pernyataan_jawaban) {
										cheked_radio+=" checked";
									}
								});
							}
							
							cheked_radio+=' onclick="save(this)"';
						
							row +='<input data-pernyataan='+value.id_sesi_pernyataan+' data-jawaban='+val_jawaban.id_sesi_pernyataan_jawaban+' '+cheked_radio+' class="form-check-input" type="radio" name="'+value.id_sesi_pernyataan+'" id="radio-'+value.id_sesi_pernyataan+"-"+val_jawaban.id_sesi_pernyataan_jawaban+'" value="'+val_jawaban.id_sesi_pernyataan_jawaban+'">';
							
							row +='<label class="form-check-label" for="radio-'+value.id_sesi_pernyataan+"-"+val_jawaban.id_sesi_pernyataan_jawaban+'">'+
							'		    '+this.isi_sesi_pernyataan_jawaban+
							'		  </label>'+
							'		</div>';
						row +='</li>';
					});
				}else{
					row +='<li class="list-group-item text-danger">Jawaban belum ada, mohon laporkan pada admin.</li>';
				}
				
				'  </ul>';
				
				if(method != 'replace') row += '</div>';
				count_pernyataan++;
			}
			//form normal
			if($.urlParam('confirm')!="true"){
				if(method != 'replace') {
					row += '<div class="data-row card mb-3" id="row-'+value.id_sesi_pernyataan+'">';
				}
				
				row += //'<div class="card">'+
				'  <div class="card-header">'+
				'    '+value.isi_sesi_pernyataan+' <button onclick="showRemarks(\''+value.keterangan_sesi_pernyataan+'\')" type="button" class="btn m-0 p-0"><i class="fas fa-question-circle text-info"></i></button>'+"<strong class='d-none'>("+value.aspek_id_sesi_pernyataan+")</strong>"+
				'  </div>'+
				'  <ul class="list-group list-group-flush">';
				
				if(value.listSesiPernyataanJawaban != null){
					
					$.each(value.listSesiPernyataanJawaban, function(key, val_jawaban){
						//if(val_jawaban.header_pernyataan_id_pernyataan_jawaban==value.id_pernyataan){
							row +='<li class="list-group-item list-group-item-action">';
					
							row += '<div class="form-check">';
							var cheked_radio="";
							console.log(list_responden_jawaban);
							if(list_responden_jawaban != null){
								$.each(list_responden_jawaban, function(key, dum_val){
									console.log(dum_val);
									if(dum_val.jawaban_id_responden_jawaban == val_jawaban.id_sesi_pernyataan_jawaban) {
										cheked_radio+=" checked ";
									}
								});
							}
							cheked_radio+=' onclick="save(this)"';
						
							row +='<input data-pernyataan='+value.id_sesi_pernyataan+' data-jawaban='+val_jawaban.id_sesi_pernyataan_jawaban+' '+cheked_radio+' class="form-check-input" type="radio" name="'+value.id_sesi_pernyataan+'" id="radio-'+value.id_sesi_pernyataan+"-"+val_jawaban.id_sesi_pernyataan_jawaban+'" value="'+val_jawaban.id_sesi_pernyataan_jawaban+'">';
							
							row +='<label class="form-check-label" for="radio-'+value.id_sesi_pernyataan+"-"+val_jawaban.id_sesi_pernyataan_jawaban+'">'+
							'		    '+this.isi_sesi_pernyataan_jawaban+
							'	   </label>'+
							'</div>';
								
							row +='</li>';
						//}
					});
				}else{
					row +='<li class="list-group-item text-danger">Jawaban belum ada, mohon laporkan pada admin.</li>';
				}
				
				'  </ul>';
				
				if(method != 'replace') row += '</div>';
				count_pernyataan++;
			}
		})
	}else{
		row += '<div class="mb-3 text-danger">Pernyataan belum ada, mohon laporkan pada admin.</div>';
	}
		num++;
		row += '<div class="card-header border mb-3 title-pembatasa">'+
		'	    	<div class="row">'+
//		'			    <div class="col-8 col-md-6" id="pernyataan-title">Pernyataan : <strong>'+value_main.nama_aspek+'</strong></div>';
		'				<div id="head-pagination" class="col-12 col-md-12 text-right">';
							if(response.next_page_number > 2 || response.next_page_number > response.count){
								row += '<button onclick="renderDisplay(\''+(response.next_page_number-2)+'\',\''+'back'+'\')" type="button" class="btn m-0 p-0 float-left"><i class="fas fa-chevron-circle-left"></i> Prev</button>';
							}
							if($.urlParam('confirm')!="true") {
								row += '<button onclick="renderDisplay('+response.next_page_number+')" type="button" class="btn m-0 p-0 float-right"><i class="fas fa-chevron-circle-right"></i> Next</button>';
							}
		row += '		</div>';
		row += '	</div>'+
		'		</div>';
	});
	return row;
}

function onGetListRespondenSuccess(response){
	console.log(response);
//	var tbody = $("#table-card-body");
//	var tfooter = $("#head-pagination");
//	$('#refresh').removeClass('disabled');
//	var row = "";
//	var num = $('.data-row').length+1;
//	row += render_row_card(response.data, num);
//	if(response.next_more==false && (response.next_page_number-response.count)==2){
//		window.location.href=ctx+"/page/survey?id="+selected_id+"&confirm=true&page="+(response.next_page_number-1);
//		$('#pernyataan-title').html("<strong>Saran : </strong>");
//		row='<textarea name="isi" class="form-control" placeholder="Beritahu kami saran dan masukan anda terhadap survey yang dilakukan, agar survey ini dapat menjadi lebih baik." rows="5"></textarea>';
//	}
//	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
//
//	myvar = '';
//	if(response.next_page_number >2 || response.next_page_number > response.count)
//	myvar += '<button onclick="renderDisplay('+(response.next_page_number-2)+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>';
//	
//	if(response.next_more==false && (response.next_page_number-response.count)==2){
//		myvar += '<button onclick="saveSaran()" type="button" class="btn m-0 p-0"><i class="fas fa-check-circle text-success"></i></button>';
//	}else{
//		myvar += '<button onclick="renderDisplay('+response.next_page_number+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>';
//	}
//	tfooter.html(myvar);
}

function showRemarks(html=""){
	$('#exampleModalCenterTitle').html("Keterangan")
	$('#exampleModalCenter').modal('show');
	html="<p class='card-text'>"+html+"</p>"
	$('#modal-content').html(html);
	html='<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
	$('#modal-action').html(html);
}
function showConfirm(html=""){
	$('#exampleModalCenterTitle').html("Konfirmasi")
	//$('#exampleModalCenter').modal('show');
	html="<p class='card-text'>"+html+"</p>"
	$('#modal-content').html(html);
	html='<button type="button" class="btn btn-secondary" data-dismiss="modal">Review</button>';
	html+='<button type="button" class="btn btn-success" data-dismiss="modal">Lanjut</button>';
	$('#modal-action').html(html);
	$('#exampleModalCenter .close').addClass('d-none');
	//dismissible:false
	$('#exampleModalCenter').modal({backdrop: 'static', keyboard: false})
}
