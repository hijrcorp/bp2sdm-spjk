var url = ctx + "/pernyataan/";
var url_jenis_pertanyaan = ctx + "/kategori-pertanyaan/";
var urlExport = ctx +"/rest/export/";
var param = "";
var selected_id='';
var mode_form;

function init() {
//	display(1);
	renderDisplay(1, '', 0);
//	getListTahun();
//	getJenisPertanyaanList();
//	
	$("#btnAdd").click(function(e){
		add();
	});

	$("#btnEdit").click(function(e){
		var id=$('#table-card').find('.collapse .in .list-group-item.active').data('pernyataan');
		edit(id);
	});
	
	$("#btnDel").click(function(e){
		var id=$('#table-card').find('.collapse .in .list-group-item.active').data('pernyataan');
		confirmRemove(id);
	});
	
	$(document).on('click', '.list-group .collapse a.list-group-item', function() {
      $(".list-group .collapse a.list-group-item").removeClass("active");
      $(this).toggleClass("active");
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
}

function add(mode, elem){
	console.log($(elem).parent());
	console.log($(elem).parent()[0].id);
	$("#modalPengguna").data('object',$(elem).parent()[0].id);
//	var row='<a style="padding-left: 60px;"  href="javascript:void(0)" class="list-group-item"><span  style="padding-right:10px" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>11111111</a>';
//	$(elem).parent().prepend(row);
	reset();
	if(mode=="aspek"){
		$("#modal-form-aspek").find('#progressBar').hide();
		$("#modal-form-aspek").modal('show');
	}else{
		$('#progressBar').hide();
		$("#modalPengguna").modal('show');
		$("[name=jenis_pertanyaan]").html(new Option($(elem).data('labelaspek'), $(elem).data('aspek')));
		$("[name=jenis_audit]").html(new Option($(elem).data('labelkegiatan'), $(elem).data('kegiatan')));
		
	}
	mode_form=mode;
	
}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    if(mode_form=='aspek'){
    	url = ctx + "/aspek/";
    	obj = new FormData(document.querySelector('#form-aspek'));
    }
    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	$("#progressBar").hide();
	if(mode_form=='aspek'){
//		$("#modal-form-aspek").find('#progressBar').hide();
//		if(mode=="aspek"){
			$("#modal-form-aspek").modal('hide');
    }else{
    	$("#modalPengguna").modal('hide');
    }
	url=ctx + "/pernyataan/";
	refresh(response);
	reset();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(obj){
	var page = $('#btnRefresh').data("id");
//	display(page);
	if(obj==undefined || selected_id!=''){
		renderDisplay(page, '', 0);
	}else{
		render_update_row(obj);
	}
}

function confirmRemove(id){

	if(id == 0 || id == undefined){
		alert('Pilih data pernyataan yang akan hapus');
		return;
	}
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}
function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	refresh();
	$("#progressBar1").hide();
	$("#modal-confirm").modal('hide');
}
//batas
function edit(id){
	if(id == 0 || id == undefined){
		alert('Pilih data pernyataan yang akan diubah');
		return;
	}
	
	reset();
	$("#progressBar").show();
//	$("#modalPengguna").modal('show');
	
//	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	var value=response.data;
	
	reset();

	selected_id = value.id_pernyataan;
	$('#progressBar').hide();
	$("#modalPengguna").modal('show');
	var elem = $('#table-card').find('.collapse .in .list-group-item.active');
	$("[name=jenis_pertanyaan]").html(new Option(elem.data('labelaspek'), elem.data('aspek')));
	$("[name=jenis_audit]").html(new Option(elem.data('labelkegiatan'), elem.data('kegiatan')));
	$('[name=isi_pertanyaan]').val(value.isi_pernyataan);
	$('[name=keterangan_pernyataan]').val(value.keterangan_pernyataan);
	$.each(value.listPernyataanJawaban, function(key){
		$('[name=isi_jawaban]')[key].value=this.isi_pernyataan_jawaban;
	});
	console.log(value);
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modalconfirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#table-card");
	setLoad('table-card');
//	if(params == '') params='jenis='+jenis;
//	console.log(params);
	
	setTimeout(() => ajaxGET(path_manual + 'list?page='+page+'&'+params,'onGetListSuccess','onGetListError'), timeout)
}



function onGetListSuccess(response){
	console.log(response);
	var tbody = $("#table-card");
	tbody.html('');
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += render_rows(value, num);
		num++;
	});
	//row+= '<a style="padding-left: 10px;"  href="javascript:void(0)" onclick="add(\''+'aspek'+'\',0)" class="list-group-item list-group-item-success"><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>++++ Tambah Baru ++++</a>';
	
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
//	new_obj.data=response;
//	createPagination('soal_pengetahuan','renderDisplay', new_obj);
}

function render_rows(value, num, is_edit=false){
	var row = "";
//	if(!is_edit) row += '<div class="card" id="row-'+value.id_pernyataan+'">';
	console.log(value)
	row +='<a data-toggle="collapse" href="#collapseExample'+num+'" class="list-group-item pernyataan"><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>'+(num)+': '+value.nama_aspek+'</a>';

	row+= '<div class="collapse" id="collapseExample'+num+'">';
	$.each(value.listKegiatan,function(key, val_jenis){
		console.log(this);
		
		row+='<a style="padding-left: 30px;" data-toggle="collapse" href="#collapseExample'+key+num+'2" class="list-group-item pernyataan"><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>'+(key+1)+" : "+this.nama_kegiatan+'</a>';
		row+= '<div class="collapse" id="collapseExample'+key+num+'2">';
		
//		$.each(this.list_pernyataan,function(idx, val_pernyataan){
//			if(value.id_aspek==val_pernyataan.aspek_id_pernyataan && val_jenis.id_kegiatan==val_pernyataan.jenis_audit_id_pernyataan){
//				row+= '<a data-toggle="collapse" href="#collapseExample'+idx+key+'3" data-pernyataan="'+val_pernyataan.id_pernyataan+'" data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+val_jenis.id_kegiatan+'" data-labelkegiatan="'+val_jenis.nama_kegiatan+'" style="padding-left: 60px;"   class="list-group-item pernyataan"><span  style="padding-right:10px" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'+val_pernyataan.isi_pernyataan+'</a>';
//				row+= '<div class="collapse" id="collapseExample'+idx+key+'3">';
//
//				$.each(this.listPernyataanJawaban,function(key_pernyataan_jawaban, val_pernyataan_jawaban){
//					console.log(val_pernyataan_jawaban);
//					if(val_pernyataan.id_pernyataan == val_pernyataan_jawaban.header_pernyataan_id_pernyataan_jawaban){
//						row+= '<div data-pernyataan="'+val_pernyataan.id_pernyataan+'" data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+val_jenis.id_kegiatan+'" data-labelkegiatan="'+val_jenis.nama_kegiatan+'" style="padding-left: 90px;"   class="list-group-item "><span  style="padding-right:10px" class="" aria-hidden="true">-</span>'+val_pernyataan_jawaban.isi_pernyataan_jawaban+'</div>';
//					}
//				});
//				row+='</div>';
//			}
//		});
		$.each(this.list_pernyataan,function(idx, val_pernyataan){
			if(value.id_aspek==val_pernyataan.aspek_id_pernyataan && val_jenis.id_kegiatan==val_pernyataan.jenis_audit_id_pernyataan){
				row+= '<a data-toggle="collapse" href="#collapseExample'+idx+key+'3" data-pernyataan="'+val_pernyataan.id_pernyataan+'" data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+val_jenis.id_kegiatan+'" data-labelkegiatan="'+val_jenis.nama_kegiatan+'" style="padding-left: 60px;"   class="list-group-item pernyataan"><span  style="padding-right:10px" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'+val_pernyataan.isi_pernyataan+'</a>';
				row+= '<div class="collapse" id="collapseExample'+idx+key+'3">';

				$.each(this.listPernyataanJawaban,function(key_pernyataan_jawaban, val_pernyataan_jawaban){
					console.log(val_pernyataan_jawaban);
					//if(val_pernyataan.id_pernyataan == val_pernyataan_jawaban.header_pernyataan_id_pernyataan_jawaban){
						row+= '<div data-pernyataan="'+val_pernyataan.id_pernyataan+'" data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+val_jenis.id_kegiatan+'" data-labelkegiatan="'+val_jenis.nama_kegiatan+'" style="padding-left: 90px;"   class="list-group-item "><span  style="padding-right:10px" class="" aria-hidden="true">-</span>'+val_pernyataan_jawaban.isi_pernyataan_jawaban+'</div>';
					//}
				});
				row+='</div>';
			}
		});
		
		row+= '<a data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+val_jenis.id_kegiatan+'" data-labelkegiatan="'+val_jenis.nama_kegiatan+'" style="padding-left: 60px;"  href="javascript:void(0)" onclick="add(\''+'pernyataan'+'\',this)" class="list-group-item list-group-item-success"><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>++++ Tambah Pernyataan Baru ++++</a>';
		row+='</div>';
	});
	//row+= '<a data-id="'+value.id_aspek+'" style="padding-left: 30px;"  href="javascript:void(0)" class="list-group-item list-group-item-success"><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>++++ Tambah Baru ++++</a>';
	row+='</div>';
	
	
		

//	if(!is_edit) row += '</div>';
	
	return row;
	
}
function render_update_row(response){
	console.log(response);
	var elem = $("#modalPengguna").data('object');
	var tbody = $("#"+elem);
	var value= response.data;
	var row = '<a data-pernyataan="'+value.id_pernyataan+'" data-aspek="'+value.aspek_id_pernyataan+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+value.jenis_audit_id_pernyataan+'" data-labelkegiatan="'+value.nama_jenis_audit+'" style="padding-left: 60px;"  href="javascript:void(0)" class="list-group-item"><span  style="padding-right:10px" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'+value.isi_pernyataan+'</a>';
//	row+= '<a data-pernyataan="'+val_pernyataan.id_pernyataan+'" data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+val_jenis.id+'" data-labelkegiatan="'+val_jenis.namaJenisAudit+'" style="padding-left: 60px;"  href="javascript:void(0)" class="list-group-item"><span  style="padding-right:10px" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'+val_pernyataan.isi_pernyataan+'</a>';'
	tbody.prepend(row);
}


