var path = ctx + '/restql';

function init(){

	getSelectPegawai();
	getSelectJenisCuti();//get from pengaturan cuti
	
	display();

	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-reset').click(function(e){
		clearForm();
		$("#search-form").trigger('reset');
	});
	
	$("#entry-form").submit(function() {
		save();
		return false;
	});
	

	$('[name=filter_id_pegawai]').on('change', function(){
		$('[name=filter_id_akun_bank]').empty();
		$('[name=filter_id_akun_bank]').append(new Option('Semua Akun Bank', ''));

		var params='filter=';
		params+='id_pegawai.eq \''+$('[name=filter_id_pegawai]').val()+'\';';
		ajaxGET(path + '/akun_bank'+'?'+params,'onGetSelectFilterAkunBank','onGetSelectError');
	});
	$('#search-form').submit(function(e){
		display(1, 'filter=');
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('.single-date-picker').on('change', function(){
         var dateFirst = new Date($('[name=tanggal_mulai]').val());//"11/25/2017"
         var dateSecond = new Date($('[name=tanggal_selesai]').val());//"11/28/2017"
         // time difference
         var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());
         // days difference
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
         $('#jumlah_cuti').text(diffDays+" Hari");
         $('[name=jumlah_cuti]').val(diffDays+" Hari"); 
	});
}

async function unduh(id){
	var n = new Date().getTime();
	var select="";
	var query=select+"&join=pegawai,akun_bank&fetch=penggajian_detil.select(id,id_penggajian,jenis,komponen,jumlah,keterangan)";//akun_bank.limit(1)
	const resp = await $.ajax(path + '/penggajian/'+id+'?'+query);
	console.log(resp.data);
	var value = resp.data;
	var params = "?namaPegawai="+value.pegawai.nama+"&periodePenggajian="+value.tanggal+"&bankTransfer="+value.akun_bank.nama_bank+"&nomorRekening="+value.akun_bank.nomor_rekening;
	
	params+="&";
	location.href=ctx+"/export/slip-gaji/"+id+params;
}

function onGetSelectFilterAkunBank(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_akun_bank]').append(new Option(value.nama_bank+'-'+value.nomor_rekening, value.id));
	});
}

function add(type=''){
	console.log(type);
	if(type=="gaji"){
		var row="";
		row+='<tr id="row-gaji-'+($('#tbl-gaji').find('tbody tr').length+1)+'">';
			row+='<th scope="row">'+($('#tbl-gaji').find('tbody tr').length+1)+'</th>';
			row+='<td contenteditable="true"></td>';
			row+='<td contenteditable="true"></td>';
			row+='<td contenteditable="true"></td>';
			row+='<td>'+'<i onclick="$(\''+'#row-gaji-'+($('#tbl-gaji').find('tbody tr').length+1)+'\').remove()" class="fas fa-times-circle text-danger"></i>'+'</td>';
		row+='</tr>';
		$('#tbl-gaji').find('tbody').append(row);
	}
	if(type=="gaji-potongan"){
		var row="";
		row+='<tr id="row-gaji-potongan-'+($('#tbl-gaji-potongan').find('tbody tr').length+1)+'">';
			row+='<th scope="row">'+($('#tbl-gaji-potongan').find('tbody tr').length+1)+'</th>';
			row+='<td contenteditable="true"></td>';
			row+='<td contenteditable="true"></td>';
			row+='<td contenteditable="true"></td>';
			row+='<td>'+'<i onclick="$(\''+'#row-gaji-potongan-'+($('#tbl-gaji-potongan').find('tbody tr').length+1)+'\').remove()" class="fas fa-times-circle text-danger"></i>'+'</td>';
		row+='</tr>';
		$('#tbl-gaji-potongan').find('tbody').append(row);
	}
	if(type=="beban-perusahaan"){
		var row="";
		row+='<tr id="row-beban-perusahaan-'+($('#tbl-beban-perusahaan').find('tbody tr').length+1)+'">';
			row+='<th scope="row">'+($('#tbl-beban-perusahaan').find('tbody tr').length+1)+'</th>';
			row+='<td contenteditable="true"></td>';
			row+='<td contenteditable="true"></td>';
			row+='<td contenteditable="true"></td>';
			row+='<td>'+'<i onclick="$(\''+'#row-beban-perusahaan-'+($('#tbl-beban-perusahaan').find('tbody tr').length+1)+'\').remove()" class="fas fa-times-circle text-danger"></i>'+'</td>';
		row+='</tr>';
		$('#tbl-beban-perusahaan').find('tbody').append(row);
	}
}


function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/cuti_pegawai/save';
	if($('[name=id]').val() != "") url = path+'/penggajian/save?append=false';
	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray())
		};
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form').modal('hide');
        		display();
        },
        error : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        },
    });
    
}
function getSelectJenisCuti(){
	$('[name=id_jenis]').empty();
	$('[name=id_jenis]').append(new Option('Pilih Jenis Cuti', ''));
	//
	ajaxGET(path + '/cuti_pengaturan','onGetSelectJenisCuti','onGetSelectError');
}
function onGetSelectJenisCuti(response){
	$.each(response.data, function(key, value) {
		$('[name=id_jenis]').append(new Option(value.nama, value.id));
	});
}
function getSelectPegawai(){
	$('[name=id_pegawai]').empty();
	$('[name=id_pegawai]').append(new Option('Pilih Pegawai', ''));
	$('[name=filter_id_pegawai]').empty();
	$('[name=filter_id_pegawai]').append(new Option('Semua Pegawai', ''));
	//
	ajaxGET(path + '/pegawai','onGetSelectPegawai','onGetSelectError');
}
function onGetSelectPegawai(response){
	$.each(response.data, function(key, value) {
		var row = '<option data-upah="'+value.upah_pokok+'" value="'+value.id+'">'+value.nama+'</option>';
		//$('[name=id_pegawai]').append(new Option(value.nama, value.id));
		$('[name=id_pegawai]').append(row);
		$('[name=filter_id_pegawai]').append(row);
	});
}
function checkPegawai(id){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
	select="";
	var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening),beban_perusahaan.select(id,id_pegawai,komponen,biaya)";//akun_bank.limit(1)
	ajaxGET(path + '/pegawai/'+id+'?'+query,'onCheckPegawai','onActionError');
}
function onCheckPegawai(response){
	$.LoadingOverlay("hide");
	console.log(response);
	var row="";
	$.each(response.data.beban_perusahaan, function(key){
		row+='<tr id="row-perusahaan-'+(key+1)+'">';
			row+='<th scope="row">'+(key+1)+'</th>';
			row+='<td contenteditable="true">'+this.komponen+'</td>';
			row+='<td contenteditable="true">'+this.biaya+'</td>';
			row+='<td contenteditable="true"></td>';
			//row+='<td class="text-center">'+'<i onclick="$(\''+'#row-perusahaan-'+(key+1)+'\').remove()" class="fas fa-times-circle text-danger"></i>'+'</td>';
			row+='<td>'+''+'</td>';
		row+='</tr>';
	});
	$('#tbl-beban-perusahaan').find('tbody').html(row);
	
	var upah_pokok = $('[name=id_pegawai] option:selected').attr('data-upah');
	var row="";
	row+='<tr id="row-gaji-'+($('#tbl-gaji').find('tbody tr').length+1)+'">';
		row+='<th scope="row">'+1+'</th>';//($('#tbl-gaji').find('tbody tr').length+1)
		row+='<td contenteditable="true">Upah Pokok</td>';
		row+='<td contenteditable="true">'+(upah_pokok)+'</td>';
		row+='<td contenteditable="true"></td>';
		row+='<td>'+''+'</td>';
	row+='</tr>';
	$('#tbl-gaji').find('tbody').html(row);
}
function getSelectAkunBank(){
	$('[name=id_akun_bank]').empty();
	$('[name=id_akun_bank]').append(new Option('Pilih Akun Bank', ''));

	var params='filter=';
	params+='id_pegawai.eq \''+$('[name=id_pegawai]').val()+'\';';
	ajaxGET(path + '/akun_bank'+'?'+params,'onGetSelectAkunBank','onGetSelectError');
}
function onGetSelectAkunBank(response){
	$.each(response.data, function(key, value) {
		$('[name=id_akun_bank]').append(new Option(value.nama_bank+'-'+value.nomor_rekening, value.id));
	});
}

function display(page=1, params=''){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');
	}
	
	var clause=[];
	//if($('[name=filter_nama]').val() != ''){
		params+='id_pegawai.slike \''+$('[name=filter_id_pegawai]').val()+'\',';
	//}
	//if($('[name=filter_jenis]').val() != ''){
		params+='id_akun_bank.slike \''+$('[name=filter_id_akun_bank]').val()+'\';';
	//}
	if($('[name=filter_id_pegawai]').val() == '' && $('[name=filter_id_akun_bank]').val() == ''){
		params='';
	}
	// "%'+$('[name=filter]').val()+'%"
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//console.log(path + '/list?page='+page+'&'+params);
	var qry="join=pegawai,akun_bank";
	qry="";
	ajaxGET(path + '/cuti_pegawai?page='+page+'&'+params+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += '<tr class="data-row" id="row-'+value.id+'">';
			row += '<td>'+(num)+'</td>';
			row += '<td> '+value.pegawai.nama +' </td>';
			row += '<td> '+value.cuti_pengaturan.nama_bank+"-"+value.akun_bank.nomor_rekening +' </td>';
			row += '<td> '+value.tanggal +' </td>';
			row += '<td> '+getDateForList(value.timestamp_added) +' </td>';
			row += '<td class="text-center">';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="unduh(\''+value.id+'\',\'print\')"><i class="fas fa-fw fa-print text-info"></i></a>';
			row += '</td>';
		row += '</tr>';
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		//var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		var query=select+"&fetch=penggajian_detil.select(id,id_penggajian,jenis,komponen,jumlah,keterangan)";//akun_bank.limit(1)
		ajaxGET(path + '/penggajian/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=penggajian_detil";
		var query="";
		ajaxPOST(path + '/penggajian/'+id+'/'+selected_action+'?'+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'update-true' || selected_action == 'update-false'){
		if(selected_action=="update-true")
			updateStatus(0);
		else if(selected_action=="update-false")
			updateStatus(1);
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('.modal table').find('tbody').html('');
	$('[name=filter_id_pegawai]').val('').trigger('change');
	$('[name=filter_id_akun_bank]').val('').trigger('change');
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=id_pegawai]').val(value.id_pegawai).trigger('change');
	setTimeout(function(){
		$('[name=id_akun_bank]').val(value.id_akun_bank);
		$('.modal table').find('tbody').html('');
		

		console.log(value);
		$.each(value.penggajian_detil,function(key){
			if(this.jenis=="GAJI_UPAH"){
				var row="";
				row+='<tr id="row-gaji-'+($('#tbl-gaji').find('tbody tr').length+1)+'">';
					row+='<th scope="row">'+($('#tbl-gaji').find('tbody tr').length+1)+'</th>';
					row+='<td contenteditable="true">'+this.komponen+'</td>';
					row+='<td contenteditable="true">'+this.jumlah+'</td>';
					row+='<td contenteditable="true">'+this.keterangan+'</td>';
					row+='<td>'+'<i onclick="$(\''+'#row-gaji-'+($('#tbl-gaji').find('tbody tr').length+1)+'\').remove()" class="fas fa-times-circle text-danger"></i>'+'</td>';
				row+='</tr>';
				$('#tbl-gaji').find('tbody').append(row);
			}
			if(this.jenis=="GAJI_POTONGAN"){
				var row="";
				row+='<tr id="row-gaji-potongan-'+($('#tbl-gaji-potongan').find('tbody tr').length+1)+'">';
					row+='<th scope="row">'+($('#tbl-gaji-potongan').find('tbody tr').length+1)+'</th>';
					row+='<td contenteditable="true">'+this.komponen+'</td>';
					row+='<td contenteditable="true">'+this.jumlah+'</td>';
					row+='<td contenteditable="true">'+this.keterangan+'</td>';
					row+='<td>'+'<i onclick="$(\''+'#row-gaji-potongan-'+($('#tbl-gaji-potongan').find('tbody tr').length+1)+'\').remove()" class="fas fa-times-circle text-danger"></i>'+'</td>';
				row+='</tr>';
				$('#tbl-gaji-potongan').find('tbody').append(row);
			}
			if(this.jenis=="BEBAN_PERUSAHAAN"){
				var row="";
				row+='<tr id="row-beban-perusahaan-'+($('#tbl-beban-perusahaan').find('tbody tr').length+1)+'">';
					row+='<th scope="row">'+($('#tbl-beban-perusahaan').find('tbody tr').length+1)+'</th>';
					row+='<td contenteditable="true">'+this.komponen+'</td>';
					row+='<td contenteditable="true">'+this.jumlah+'</td>';
					row+='<td contenteditable="true">'+this.keterangan+'</td>';
					row+='<td>'+'<i onclick="$(\''+'#row-beban-perusahaan-'+($('#tbl-beban-perusahaan').find('tbody tr').length+1)+'\').remove()" class="fas fa-times-circle text-danger"></i>'+'</td>';
				row+='</tr>';
				$('#tbl-beban-perusahaan').find('tbody').append(row);
			}
		});
		},1000);
	$('[name=tanggal]').val(value.tanggal);
	
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display();
	
	// Update data ke existing tabel
	/*if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}*/
	
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


