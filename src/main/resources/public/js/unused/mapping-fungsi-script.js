var path = ctx + '/restql';
var path_manual = ctx + "/mapping-fungsi/";

var param = "";
var selected_id='';
var selected_action='';
var selected_mode='';

var appendIds = [];
function init() {
//	display();
	renderDisplay();
	getSelectJabatan();
	//getSelectJenjangJabatan();
	//getSelectTingkatJenjang();
	
	$("#btnAdd").click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#form-fungsi-kunci').submit(function(e){
		//
		var url = path+'/mapping_fungsi_kunci_detil/save?append=false';//&join=kelompok_jabatan
		
		window.obj = {
			data : objectifyForm($('#form-fungsi-kunci').serializeArray())
		};
		
		if($('[name=id]').val() == "") delete obj.data.id;
		console.log(obj);
		
		$.ajax({
	        url : url,
	        type : "POST",
	        traditional : true,
	        contentType : "application/json",
	        dataType : "json",
	        data : JSON.stringify(obj),
	        success : function (response) {
	        		//$.LoadingOverlay("hide");
	        		//$('#modal-form').modal('hide');
	        		//display();
	        	onModalActionSuccess(response);
	        },
	        error : function (response) {
	        		//$.LoadingOverlay("hide");
	        		$('#modal-form-msg').text(response.responseJSON.message);
	        		$('#modal-form-msg').removeClass('d-none');
	        },
	    });
		//
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
	
	$('#btnRemove').click(function(e){//btn-modal-confirm-yes
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('[name=id_jenjang_jabatan]').on('change', function(){
		console.log("sebelum");
		console.log(this.value);
		var newValue = this.value.split("/");
		console.log("sesudah");
		console.log(newValue);
		$('[name=id_tingkat_jenjang]').val(newValue[1]);
	});
}

function getSelectJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	$('[name=id_kelompok_jabatan]').append(new Option('Pilih Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'&'+params+'&'+qry,'onGetSelectJabatan','onGetSelectError');
}
function onGetSelectJabatan	(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.nama, value.id));
	});
}

function getSelectJenjangJabatan(){
	$('[name=id_jenjang_jabatan]').empty();
	$('[name=id_jenjang_jabatan]').append(new Option('Pilih Jenjang Jabatan', ''));
	var page=1,params="",qry="join=tingkat_jenjang";
	ajaxGET(path + '/jenjang_jabatan?page='+page+'&'+params+'&'+qry,'onGetSelectJenjangJabatan','onGetSelectError');
}
function onGetSelectJenjangJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_jenjang_jabatan]').append(new Option(value.tingkat_jenjang.nama+" "+value.nama, value.id+"/"+value.tingkat_jenjang.id));
	});
}

function getSelectTingkatJenjang(){
	$('[name=id_tingkat_jenjang]').empty();
	$('[name=id_tingkat_jenjang]').append(new Option('Pilih Tingkat Jenjang', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/tingkat_jenjang?page='+page+'&'+params+'&'+qry,'onGetSelectTingkatJenjang','onGetSelectError');
}
function onGetSelectTingkatJenjang(response){
	$.each(response.data, function(key, value) {
		$('[name=id_tingkat_jenjang]').append(new Option(value.nama, value.id));
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		//param = $('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		//console.log(param);
		display(1, 'filter=');
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(mode='', parent=''){
	reset();
	selected_id = '';
	selected_action = 'add';
	clearForm();
	$("#progressBar").hide();
	if(mode=="fungsi_kunci"){
		$("#modal-form-fungsi-kunci").modal('show');
		$('[name=id_mapping_fungsi]').val(parent);

		selected_mode="fungsi-kunci";
	}else{

		$("#modal-form").modal('show');
	}
}

function save(){
	//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/mapping_fungsi/save?append=false&join=kelompok_jabatan';
	
	window.obj = {
			data : objectifyForm($('#frmInput').serializeArray())
		};
	
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		//$.LoadingOverlay("hide");
        		//$('#modal-form').modal('hide');
        		//display();
        	onModalActionSuccess(response);
        },
        error : function (response) {
        		//$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        },
    });
}

function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function clearForm(){
	$('#frmInput')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();
}


function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

function doRemove(id){
	$("#progressBar1").show();
	if(selected_action == 'delete' ){
		doAction(selected_id, selected_action, 1);
	}
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modal-form").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=nama]').val(value.nama_aspek);
	$('[name=sumber]').val(value.sumber_aspek);

//	$('#error_msg').hide();
//	$('#modal-form').modal('show');
//	reset();
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	if($('#txtsearch').val() != '' && $('#column').data('id') != '' || page ==1){
		tbody.text('');
	}
	
	if($('#column').data('id') != ''){
		params+= $('#column').data('id')+'.slike \''+$('#txtsearch').val()+'\';';
	}
	
	if($('[name=filter_nama]').val() == '' && $('[name=filter_nomor]').val() == '' 
		&& $('[name=filter_id_jabatan]').val() == '' && $('[name=filter_alamat_rumah]').val() == '' 
			&& $('[name=filter_id_propinsi]').val() == ''){
		params='';
	}
	// "%'+$('[name=filter]').val()+'%"
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=kelompok_jabatan&limit=3";
	ajaxGET(path + '/mapping_fungsi?page='+page+'&'+params+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}
function renderDisplay(page=1, params = '', timeout=0){
	var tbody = $("#tbl-data>tbody");//.find('tbody');
	//setLoad('table-card');
//	if(params == '') params='jenis='+jenis;
//	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	if($('#txtsearch').val() != '' && $('#column').data('id') != '' || page ==1){
		tbody.text('');
	}
	setTimeout(() => ajaxGET(path_manual + 'list?page='+page+'&'+params+"&limit=3",'onGetListDetilSuccess','onGetListError'), timeout)
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data>tbody");//.find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += renderRow(value, num);
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="renderDisplay('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		query="";
		ajaxGET(path + '/mapping_fungsi/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='';
		query="";
		ajaxPOST(path + '/mapping_fungsi/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	//display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=tujuan_utama]').val(value.tujuan_utama);
	$('[name=id_kelompok_jabatan]').val(value.id_kelompok_jabatan);
}

function appendRow(value){
	console.log("append "+value.id);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = "";
	if(selected_mode=="fungsi-kunci"){
		num = $('.data-row-fungsi-kunci'+value.id_mapping_fungsi).length+1;
		tbody = $("#tbl-sub-data"+value.id_mapping_fungsi).find('tbody');
		row = renderRowFungsiKunci(value, num);
	}else{
		row = renderRow(value, num);
	}
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function renderRowFungsiKunci(value, num){
	var row = "";
	row += '<tr class="data-row-fungsi-kunci">';
	row += '<td>'+(num+1)+'</td>';
	row += '<td>'+value.fungsi_kunci+'</td>';
	row += '<td>';
		row += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-sub'+value.id+'">'+0+'</a>';
	row += '</td>';
	row += '</tr>';

	row += '<tr>';
		row += '<td colspan="4">';
			row += '<div id="collapse-sub'+value.id+'" class="panel-collapse collapse ins" role="tabpanel">'+
				'    <div class="panel panel-default">'+
				'      <div class="panel-body">'+
				'<div class="table-responsive">'+
				'    <table id="tbl-sub-data" class="table">'+
				'        <thead>'+
				'            <tr>'+
				'                <th width="20">No</th>'+
				'                <th width="150">Fungsi Utama</th>'+
				'                <th width="50">Fungsi Dasar</th>'+
				'                <th width="50">Action</th>'+
				'            </tr>'+
				'        </thead>'+
				''+
				'<tbody>';
				
				row +='</tbody>'+
				'    </table>'+
				'</div>'+
				'      </div>'+
				'    </div>'+
				'</div>';
		row += '</td>';
	row += '</tr>';

	return row;
}

function renderRow(value, num, method='add', mode=''){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';
	row += '<td class="">'+value.tujuan_utama+'</td>';
	row += '<td class="">'+value.kelompokJabatan.nama+'</td>';
	row += '<td>';
		row += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+value.id+'">'+(value.listMappingFungsiKunciDetil.length)+'</a>';
	row += '</td>';
//	row += '<td class=""></td>';
//	row += '<td class=""></td>';

	row += '<td><button onclick="doAction(\''+value.id+'\',\'edit\')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
	row += '<button onclick="doAction(\''+value.id+'\',\'delete\')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	row += '<tr>';
		row += '<td colspan="5">';
			row += '<div id="collapse'+value.id+'" class="panel-collapse collapse ins" role="tabpanel">'+
				'    <div class="panel panel-default">'+
				'      <div class="panel-body">'+
				'<div class="table-responsive">'+
				'    <table id="tbl-sub-data'+value.id+'" class="table">'+
				'        <thead>'+
				'            <tr>'+
				'                <th width="20">No</th>'+
				'                <th width="150">Fungsi Kunci</th>'+
				'                <th width="50">Fungsi Utama</th>'+
				'                <th width="50">Action</th>'+
				'            </tr>'+
				'        </thead>'+
				''+
				'<tbody>';
				$.each(value.listMappingFungsiKunciDetil, function(key){
					row += '<tr class="data-row-fungsi-kunci'+this.id+'">';
					row += '<td>'+(key+1)+'</td>';
					row += '<td>'+this.fungsi_kunci+'</td>';
					row += '<td>';
						row += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-sub'+this.id+'">'+(this.listMappingFungsiUtamaDetil.length)+'</a>';
					row += '</td>';
					row += '</tr>';

					row += '<tr>';
						row += '<td colspan="4">';
							row += '<div id="collapse-sub'+this.id+'" class="panel-collapse collapse ins" role="tabpanel">'+
								'    <div class="panel panel-default">'+
								'      <div class="panel-body">'+
								'<div class="table-responsive">'+
								'    <table id="tbl-sub-data" class="table">'+
								'        <thead>'+
								'            <tr>'+
								'                <th width="20">No</th>'+
								'                <th width="150">Fungsi Utama</th>'+
								'                <th width="50">Fungsi Dasar</th>'+
								'                <th width="50">Action</th>'+
								'            </tr>'+
								'        </thead>'+
								''+
								'<tbody>';
								$.each(this.listMappingFungsiUtamaDetil, function(key){
									row += '<tr>';
									row += '<td>'+(key+1)+'</td>';
									row += '<td>'+this.fungsi_utama+'</td>';
									row += '<td>';
										row += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-sub-sub'+this.id+'">'+(this.listMappingFungsiDasarDetil.length)+'</a>';
									row += '</td>';
									row += '</tr>';

									row += '<tr>';
										row += '<td colspan="4">';
											row += '<div id="collapse-sub-sub'+this.id+'" class="panel-collapse collapse ins" role="tabpanel">'+
												'    <div class="panel panel-default">'+
												'      <div class="panel-body">'+
												'<div class="table-responsive">'+
												'    <table id="tbl-sub-sub-data" class="table">'+
												'        <thead>'+
												'            <tr>'+
												'                <th width="20">No</th>'+
												'                <th width="150">Fungsi Dasar</th>'+
												'                <th width="50">Kode Unit Kompetensi</th>'+
												'                <th width="50">Action</th>'+
												'            </tr>'+
												'        </thead>'+
												''+
												'<tbody>';
												$.each(this.listMappingFungsiDasarDetil, function(key){
													row += '<tr>';
													row += '<td>'+(key+1)+'</td>';
													row += '<td>'+this.unitKompetensi.nama+'</td>';
													row += '<td>'+this.unitKompetensi.kode+'</td>';
													row += '</tr>';
												});
												row +='</tbody>'+
												'    </table>'+
												'</div>'+
												'      </div>'+
												'    </div>'+
												'</div>';
										row += '</td>';
									row += '</tr>';
								});
								row +='</tbody>'+
								'    </table>'+
								'</div>'+
								'      </div>'+
								'    </div>'+
								'</div>';
						row += '</td>';
					row += '</tr>';
				});
				row +='</tbody>'+
				'    </table>'+
				'</div>';

				row += '<div class="row">'+
				'    <div class="col-md-12">'+
				'        <a class="btn btn-danger btn-sm" data-toggle="collapse" data-parent="#accordion" href="#collapse'+value.id+'" aria-expanded="true">Tutup</a>'+
				'        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" onclick="add(\'fungsi_kunci\',\''+value.id+'\');">Tambah</button>'+
				'    </div>'+
				'</div>'+
					

				'      </div>'+
				'    </div>'+
				'</div>';
		row += '</td>';
	row += '</tr>';
	return row;
}