var url = ctx + "/sesi/";
var url_kegiatan = ctx + "/kegiatan/";
var url_unit_kerja = ctx + "/unit_kerja/";
var urlExport = ctx +"/rest/export/";
var param = "";
var selected_id='';
var sumber_sesi="INTERNAL";
function init() {
	String.prototype.replaceAll = function (find, replace) {
	    var result = this;
	    do {
	        var split = result.split(find);
	        result = split.join(replace);
	    } while (split.length > 1);
	    return result;
	};
	//
	selected_id = $.urlParam('id');
    if(selected_id == null || selected_id == '') {
        alert('ID Sesi tidak ditemukan.');
        window.history.back();
    }else{
    	$.getJSON(url+selected_id, function(response) {
            if (response.code == 200) {
            	var value=response.data;
                console.log('sesi: ', response.data);
                $('[name=kode]').val(value.kode_sesi);
                $('[name=tahun]').val(value.tahun_sesi);
                $('[name=periode_awal]').val(value.periode_awal_sesi_serialize);
                $('[name=periode_akhir]').val(value.periode_akhir_sesi_serialize);
//                $('#html_keterangan').html('<textarea class="form-control" name="keterangan">'+value.keterangan_sesi+'</textarea>')
               
                var newline = String.fromCharCode(13, 10);
                $('[name=keterangan]').val(value.keterangan_sesi.replaceAll('\\n', newline));
                if(value.unit_kerja != null)
                $('[name=unit_kerja]').val(value.unit_kerja.nama_unit_kerja);
                if(value.kegiatan != null)
                $('[name=kegiatan]').val(value.kegiatan.nama_kegiatan);
                if(value.unit_auditi != null)
                $('[name=unit_auditi]').val(value.unit_auditi.nama_unit_auditi);
                if(value.eselon1 != null)
                $('[name=eselon1]').val(value.eselon1.nama_eselon1);
//                $('[name=tanggal_surat_tugas]').val(value.tanggal_surat_tugas_serialize);
//                getListSuratTugasTim();
                if(value.sumber_sesi != "INTERNAL") sumber_sesi = value.sumber_sesi;
            }else{
            	alert('ID Sesi tidak ditemukan.');
                window.history.back();
            }
        });
    }
    
//    console.log('paramId: ', paramId);
    
	$('#btnAddTL').click(function(){
	    if($('[name=id_surat_tugas_tim]').val() == null || $('[name=id_surat_tugas_tim]').val() == '') {
	        alert('TIM harus dipilih dulu jika ingin menambahkan responden.');
	    } else {
    		$('.satker-row').hide();
    		$('.unit-kerja-row').hide();
    		$("#frm-input").trigger("reset");
    		$('#fUnitAuditi').trigger("chosen:updated");
    		$('#modal-input').modal('show');
    		$("#passwordForm").show();
	    }
	});
	$("#btnRefresh").click(function(e){
		refresh();
	});

	$("#form-view :input").not("select").attr("disabled", true);
	$("#form-view :input").attr("disabled", true);
	//
	renderDisplay(1, '', 0);
	
//	getListTahun();
//	getKegiatanList();
//	getUnitKerjaList();
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
	
	$('#btnExport').click(function(){
		window.location = urlExport+"TUSI"+param;
	});
	
	$(document).on('click', 'table tbody tr', function() {
       $("table tbody tr").removeClass("info");
       $(this).toggleClass("info");
	});
	$(function() {
	    $("body").delegate(".datepicker", "focusin", function(){
	        $(this).datepicker();
	    });
	});
}

function download(){
    var param = '?';
//    if($('[name=id_surat_tugas_tim]').val() != '') param += '&filter_tim_id='+$('[name=id_surat_tugas_tim]').val();
    if(selected_id != '' && selected_id != null) param += '&filter_id_header_sesi='+selected_id;//$('[name=id_surat_tugas]').val();
    
    console.log(url + '/export'+param);
    location.href=url + '/export'+param;
}

function setData(elem){
	var id = $(elem).data('parent');
	console.log(id);
	$('#btnConfirmImport').removeClass('disabled');
	$('#btnConfirmImport').data('id', elem);
	//findSuratTugas(elem);
}

function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=tahun]").append(new Option(i, i));
	}
}

function getKegiatanList(){
	$.getJSON(url_kegiatan+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=id_kegiatan]").append(new Option(value.nama_kegiatan, value.id_kegiatan));
			});
		}
	});
}

function getUnitKerjaList(){
	$.getJSON(url_unit_kerja+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=id_unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
			});
		}
	});
}
function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		display(1);
		
		param = "&"+$('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		console.log(param);
		renderDisplay(1, param, 0);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id_sesi', selected_id);
    console.log(obj);
    ajaxPOST(url + 'generate-responden',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	$("#progressBar").hide();
	$("#modal-form").modal('hide');

	refresh();
	reset();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function reset(){
	cleanMessage('msg');
//	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(page, '', 0);
}

function confirmRemove(id){
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}
function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	refresh();
	$("#progressBar1").hide();
	$("#modal-confirm").modal('hide');
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modal-form").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=kode]').val(value.kode_sesi);
	$('[name=tahun]').val(value.tahun_sesi);
	$('[name=kode]').val(value.kode_sesi);
	$('[name=periode_awal]').val(value.periode_awal_sesi_serialize);
	$('[name=periode_akhir]').val(value.periode_akhir_sesi_serialize);
	$('[name=keterangan]').val(value.keterangan_sesi);
	$('[name=id_unit_kerja]').val(value.unit_kerja_id_sesi);
	$('[name=id_kegiatan]').val(value.kegiatan_id_sesi);
	$('[name=jumlah_responden]').val(value.jumlah_responden_sesi);
	
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(confirm == 0) ajaxGET(url+"user/responden/" + id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url+"user/responden/" + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
	$('#btnRemove').attr('onclick',"remove($(this).data('id'), 1);");
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
	$("#modal-confirm").modal('hide');
}
//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#tbl").find('tbody');

	var new_obj={
			'id' : 0,
			'element' : tbody,
			'path' : url+"user/responden/",
			'fn' : {
				'remove' : {
					'name' : 'onRemoveSuccess',
					'type' : 'button'
				}
			},
			'value' : {
					'id' : 'id_sesi_user_responden', 
					'value1' : 'user_id_sesi_user_responden', 
					'value2' : 'email', 
					'value3' : 'tim_id_sesi_user_responden', 
					'value4' : 'status_sesi_user_responden'
			},
			'data' : {},
			'mode' : {
				'filter' : false
			}
		};
	params += 'filter_id_sesi='+selected_id;
//	if(params == '') params='jenis='+jenis;	
//	if(new_obj.mode.filter==true) params = $( "#frmFilter" ).serialize();
//	else params ="uraian="+$('#txtsearch').val();
	console.log(params);
//	setLoad('tbl');
	setTimeout(() => ajaxGET_new(new_obj.path + 'list?page='+page+'&'+params,'onGetListSuccess','onGetListError',new_obj), timeout)
}



function onGetListSuccess(response, new_obj){
	console.log(response);
	var tbody = new_obj.element
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += render_row(response.data[key], key, false, new_obj);
		num++;
	});
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	new_obj.data=response;
	createPagination('soal_pengetahuan','renderDisplay', new_obj);
}

function render_row(value, num, is_edit=false, new_obj){
//	new_obj.id=value[new_obj.value0];
	arr = JSON.stringify(new_obj);
	var row = "";
	if(!is_edit) row += '<tr class="data-row" id="row-'+value[new_obj.value.id]+'">';
	row += '<td>'+(num+1)+'</td>';
	
	var arr1 = [];
	var arr2 = [];

	for (var prop in new_obj.value) {
	   arr2.push(new_obj.value[prop]);
	}
	console.log(arr2);
	$.each(arr2,function(key){
		console.log(this.search("-"));
		if(this.search("-") != -1){
			var newThis = this.split("-");
			row += '<td>'+value[newThis[0]]+"-"+value[newThis[1]]+'</td>';
		}else{
			if(key>0) row += '<td>'+value[this]+'</td>';
		}
		
	});

	
	row += '<td>';
		row += '<a href="javascript:void(0)" class="btn btn-default btn-sm" type="button" onclick="remove(\''+value[new_obj.value.id]+'\',0,arr)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> ';
	row += '</td>';
	
	if(!is_edit) row += '</tr>';
	return row;
}
/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(page){
	var tbody = $('#tbl').find($('tbody'));
	$('#btnRefresh').data("id",page);
	
	tbody.text('');
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	
	var param="";
	if($('#column').data('id') != "" && $('#txtsearch').val() != ""){
		param = "?"+$('#column').data('id')+"="+$('#txtsearch').val();
	}
	if(page > 1){
		param = "?page="+page+param;
	}
	$.ajax({
        url : url+"list"+param,
        type : "GET",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        success : function (response) {
        	console.log(response);
        	if (response.status == 200) {
        		var row = '';
        		tbody.text('');
        		$.each(response.data, function(key, value) {
        			row += '<tr>';
	        			row += '<td scope="row">'+(getRow(response)+key)+'</td>';
	        			row += '<td>'+value.namaKelompokUPT+'</td>';
	        			row += '<td>'+value.uraianTugasFungsi+'</td>';
	        			
	        			row += '<td><button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
	    				
	    				row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
	    				row += '</td>';
        			row += '</tr>';
        			
        		});
        		if(row == ''){
        			tbody.text('');
        			tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
        		}else{
        			tbody.html(row);
        		}
        		createPagination1('tblpages','display', response);
			}
        },
        error : function (response) {
        	alert("Connection error");
        },
    });
}





//function onGetListSuccess(response, new_obj){
//	console.log(response);
//	var tbody = new_obj.element
//	$('#refresh').removeClass('disabled');
//	var row = "";
//	var num = $('.data-row').length+1;
//	$.each(response.data,function(key,value){
//		row += render_row(value, num);
//		num++;
//	});
//	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
//	new_obj.data=response;
//	//createPagination('soal_pengetahuan','renderDisplay', new_obj);
//}

//function render_row(value, num, is_edit=false){
////	new_obj.id=value[new_obj.value0];
//	var row = "";
//	if(!is_edit) row += '<tr  class="data-row" id="row-'+value.id_surat_tugas+'">';
////	row += '<td>'+(num)+'</td>';
//	row += '<td>'+value.nomor_surat_tugas+'</td>';
//	row += '<td>'+value.tanggal_surat_tugas_serialize+'</td>';
//	row += '<td>'+value.unit_kerja_pelaksana.nama_unit_kerja+'</td>';
//	row += '<td>'+value.nama_pelakasana+'</td>';
//	row += '<td>'+value.kegiatan_pengawasan.nama_kegiatan+'</td>';
//	row += '<th>Eselon I</th>';
//	row += '<th>Propinsi</th>';
//	
//	if(!is_edit) row += '</tr>';
//
//	var html = [];
//	var new_arr=[];
//	$.each(value.list_surat_tugas_detil_tim, function(key_value) {
//		console.log(this);
//		var name = this.surat_tugas_tim.id_group;
//		if(html.indexOf(name) == -1) html.push(name);
//		else html.push("");
//		
//		//if(this.id_grup==null || this.id_grup=="") new_arr.push(this.pegawai.nama_pegawai)
//	});
////	'<td></td>'+
////	'<td colspan="6">'+
////	'<div id="collapseOne" class="collapse in">';
////	$.each(value.list_surat_tugas_detil_tim,function(key,new_value){
////		row += '<p><a href="#">'+new_value.id_grup+'|'+new_value.pegawai.nama_pegawai+'</a></p>';
////		'</tr>';
////	});
////	row += '</div></td>'+
////	'</tr>';
//
//	var i = 1;
//	console.log(value);
//	$.each(value.list_surat_tugas_detil_tim,function(key,new_value){
//		if(html[key]!=null && html[key]!=""){
//			row += '<tr >';
//			row += '<td></td>';
//			row += '<td><a href="javascript:void(0)" data-id='+new_value.surat_tugas_tim.id+' data-parent='+value.id_surat_tugas+' onclick="setData(this)">'+html[key]+'</a></td>';
//			
//			row += '<td colspan="3">';
//			$.each(value.list_surat_tugas_detil_tim,function(key2,new_value2){
//				if(new_value.surat_tugas_tim.id_group==new_value2.surat_tugas_tim.id_group){
//					row += '<p>'+new_value2.pegawai.nama_pegawai+':'+new_value2.peran.nama_peran+'</p>';
////					row += '<p>'+new_arr[0]+'</p>';
////					$.each(value.list_surat_tugas_detil_tim,function(key2,new_value2){
////						if(new_value2.peran.nama_peran=="Inspektur Jenderal" || new_value2.peran.nama_peran=="Penanggung Jawab"){
////							row += '<p>'+new_value2.pegawai.nama_pegawai+':'+new_value2.peran.nama_peran+'</p>';
//////							row += '<p>'+new_arr[0]+'</p>';
////						}
////					});
//				}else{
//					if(new_value.surat_tugas_tim.id_group==null || new_value2.surat_tugas_tim.id_group==null){
//						row += '<p class="text-success">'+new_value2.pegawai.nama_pegawai+':'+new_value2.peran.nama_peran+'</p>';
//					}
//				}
//			});
//			row += '</td>';
//
//			row += '<td>'+new_value.surat_tugas_tim.nama_eselon1+'</td>';
//			row += '<td>'+new_value.surat_tugas_tim.nama_propinsi+'</td>';
//			i++;
//			row += '</tr>';
//		}
//	});
//	
//	return row;
//}
