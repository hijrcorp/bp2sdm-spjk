var url = ctx + "/pernyataan/";
var url_aspek = ctx + "/aspek/";
var url_penetapan = ctx + "/pernyataan-penetapan/";
var urlExport = ctx +"/rest/export/";
var url_ref = ctx +"/ref/";

var param = "";
var selected_id='';
var mode_form;

function init() {
//	display(1);
//	renderDisplay(1, '', 0);
	getListTahun();
	getSumberList();
//	
	$("#btnAdd").click(function(e){
		add();
	});

	$("#btnEdit").click(function(e){
		var id=$('#table-card').find('.collapse .in .list-group-item.active').data('pernyataan');
		edit(id);
	});
	
	$("#btnDel").click(function(e){
		var id=$('#table-card').find('.collapse .in .list-group-item.active').data('pernyataan');
		confirmRemove(id);
	});
	
	$(document).on('click', '.list-group .list-group-item', function() {
      $(".list-group .list-group-item").removeClass("active");
      $(this).toggleClass("active");
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$(".button-minus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepDown();
	});
	$(".button-plus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepUp();
	});
	
}

function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=tahun]").append(new Option(i));
	}
}

function getSumberList(){
	$.getJSON(url_ref+'sumber/list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
//				$("[name=sumber]").append(new Option(value.nama_sumber, value.nama_sumber));
				$("[name=filter_sumber]").append(new Option(value.nama_sumber, value.nama_sumber));
			});
		}
	});
}
function add(mode, elem){

//	reset();
//	if(mode=="aspek"){
//		$("#modal-form-aspek").find('#progressBar').hide();
//		$("#modal-form-aspek").modal('show');
//	}else{
//		elem = $('#table-card').find('.collapse .active');
//		if(elem.length == 0){
//			alert('Data aspek dan kegiatan wajib dipilih.');
//			return;
//		}
//		$('#progressBar').hide();
//		$("#modal-form-pernyataan").modal('show');
//		$("[name=jenis_pertanyaan]").html(new Option(elem.data('labelaspek'), elem.data('aspek')));
//		$("[name=jenis_audit]").html(new Option(elem.data('labelkegiatan'), elem.data('kegiatan')));
//		var params="aspek_pernyataan="+$("[name=jenis_pertanyaan]").val()+"&kegiatan_pernyataan="+$("[name=jenis_audit]").val();
//		renderPernyataanDisplay(1, params);
//	}
//	mode_form=mode;
//	console.log($(elem).parent());
//	console.log($(elem).parent()[0].id);
//	$("#modal-form-pernyataan").data('object',$(elem).parent()[0].id);
//	var row='<a style="padding-left: 60px;"  href="javascript:void(0)" class="list-group-item"><span  style="padding-right:10px" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>11111111</a>';
//	$(elem).parent().prepend(row);
	reset();
	//
	elem = $('#table-card').find('.active');
	if(elem.length == 0){
		alert('Data aspek dan kegiatan wajib dipilih.');
		return;
	}
	//
	$('#progressBar').hide();
	$("#modal-form-pernyataan").modal('show');
	$("[name=jenis_pertanyaan]").html(new Option($(elem).data('labelaspek'), $(elem).data('aspek')));
//	$("[name=jenis_audit]").html(new Option($(elem).data('labelkegiatan'), $(elem).data('kegiatan')));
	
	mode_form=mode;
	
}
//function save(){
//	var obj = new FormData(document.querySelector('#frmInput'));
//	if(selected_id != '') obj.append('id', selected_id);
//    console.log(obj);
////    if(mode_form=='aspek'){
////    	url = ctx + "/aspek/";
////    	obj = new FormData(document.querySelector('#form-aspek'));
////    }
////    obj.append('list_pernyataan', $('#table-modal').find('input:checked').val());
//    ajaxPOST(url_penetapan + 'save',obj,'onSaveSuccess','onSaveError');
//}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
}
function active(elem){
	var obj = new FormData();
	obj.append('id', $(elem).data('id'));
	var status=0;
	if($(elem).prop('checked')==true) status=1;
	obj.append('status', status);
    ajaxPOST(url_penetapan + 'active',obj,'onSaveSuccess','onSaveError');
}
function onSaveSuccess(response){
	$("#progressBar").hide();
//	$("#modal-form-pernyataan").modal('hide');
	$("#modal-form-pernyataan").modal('hide');
//	refresh();
	reset();
	
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	console.log(response);
	if(response.responseJSON.code==400){
		addAlert('msg', "alert-danger", response.responseJSON.message);
	}else{
		addAlert('msg', "alert-danger", "kesalahan dari server");
	}
}

function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
	$('#table-card').find('.active').click();
	$('#form-pilihan-jawaban').html("");
	$('[name=jumlah_jawaban]').attr("min", 1);
}

function refresh(obj){
	var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(page, '', 0);
}

function confirmRemove(id){

	if(id == 0 || id == undefined){
		alert('Pilih data pernyataan yang akan hapus');
		return;
	}
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}
function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	//refresh();
	reset();
	$("#progressBar1").hide();
	$("#modal-confirm").modal('hide');
}
//batas
function edit(id){
	if(id == 0 || id == undefined){
		alert('Pilih data pernyataan yang akan diubah');
		return;
	}
	
	reset();
	$("#progressBar").show();
//	$("#modalPengguna").modal('show');
	
//	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	var value=response.data;
	console.log(value);
	reset();

	selected_id = value.id_pernyataan;
	$('#progressBar').hide();
	$("#modal-form-pernyataan").modal('show');
	var elem = $('#table-card').find('.list-group-item.active');
	$("[name=jenis_pertanyaan]").html(new Option(value.nama_aspek, value.aspek_id_pernyataan));
	$('[name=kode_pertanyaan]').val(value.kode_pernyataan);
	$('[name=isi_pertanyaan]').val(value.isi_pernyataan);
	$('[name=keterangan_pernyataan]').val(value.keterangan_pernyataan);
	$('[name=jumlah_jawaban]').val(value.listPernyataanJawaban.length);
	$('[name=jumlah_jawaban]').attr("min", value.listPernyataanJawaban.length);
	
	$.each(value.listPernyataanJawaban, function(key){
		$('#form-pilihan-jawaban').append('<div class="input-group" style="margin-bottom:10px">'+
		'						  <input class="form-control hidden" name="order_jawaban" type="text" value="'+this.order_pernyataan_jawaban+'" readonly>'+
		'						  <span class="input-group-addon " id="basic-addon'+this.order_pernyataan_jawaban+'">'+this.order_pernyataan_jawaban+'</span>'+
		'						<input class="form-control hidden" name="id_jawaban" type="text" value="'+this.id_pernyataan_jawaban+'" readonly>'+
		'						  <span class="input-group-addon hidden" id="basic-addon'+this.order_pernyataan_jawaban+'">-</span>'+
		'						  <input name="isi_jawaban" type="text" class="form-control" placeholder="Pilihan jawaban ke '+(this.order_pernyataan_jawaban)+'" autocomplete="off" value="'+this.isi_pernyataan_jawaban+'">'+
		'						</div>');
	});
	$('#btn-create-form').data('obj', value.listPernyataanJawaban);
//	$('#form-jumlah-jawaban').addClass("hidden");
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url_penetapan +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url_penetapan + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modalconfirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas

function setLoad(elem, typ = ''){
	if(typ == 'tbl') {
		elem.html('<tr><td colspan="10" align="center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</td></tr>');	
	}else{
		elem.html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');	
	}
	
}

function renderDisplayAspek(page, params='', timeout=0){
	var tbody = $("#table-card");
	setLoad(tbody);
	$("#table").find('tbody').html('');
	params="&filter_sumber="+$('[name=filter_sumber]').val();
	setTimeout(() => ajaxGET(url_aspek + 'list?page='+page+'&'+params,'onRenderAspekListSuccess','onGetListError'), timeout)
}

function onRenderAspekListSuccess(response){
	console.log(response);
	var tbody = $("#table-card");
	tbody.html('');
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row +='<a data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" onclick="renderPernyataanDisplay(1, '+value.id_aspek+');" href="javascript:void(0);" class="list-group-item">'+(num)+': '+value.nama_aspek+'</a>';
		num++;
	});
	
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#table-card");
	setLoad(tbody);
	$("#table").find('tbody').html('');
//	if(params!="") 
		params="&filter_sumber="+$('[name=filter_sumber]').val();
	setTimeout(() => ajaxGET(url_penetapan + 'list-aspek-kegiatan?page='+page+'&'+params,'onGetListSuccess','onGetListError'), timeout)
}


function onGetListSuccess(response){
	console.log(response);
	var tbody = $("#table-card");
	tbody.html('');
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
//	$.each(response.data,function(key,value){
		row += render_rows(response, num);
//		num++;
//	});
	//row+= '<a style="padding-left: 10px;"  href="javascript:void(0)" onclick="add(\''+'aspek'+'\',0)" class="list-group-item list-group-item-success"><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>++++ Tambah Baru ++++</a>';
	
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
//	new_obj.data=response;
//	createPagination('soal_pengetahuan','renderDisplay', new_obj);
}
var obj_temp;
function render_rows(response, num, is_edit=false){
	var row = "";

	console.log(response)
	arr_temp=[];
	$.each(response.data.main,function(key,value){
		row +='<a data-toggle="collapse" href="#collapseExample'+num+'" class="list-group-item"><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>'+(num)+': '+value.nama_aspek+'</a>';

		row+= '<div class="collapse" id="collapseExample'+num+'">';
		
//		$.each(value.listKegiatan,function(key, val_jenis){
//			row+='<a data-aspek="'+value.id_aspek+'" data-labelaspek="'+value.nama_aspek+'" data-kegiatan="'+val_jenis.id_kegiatan+'" data-labelkegiatan="'+val_jenis.nama_kegiatan+'" onclick="render_penetapan_pernyataan(this)" style="padding-left: 30px;" data-toggle="collapse" href="javascript:void(0)" class="list-group-item "><span  style="padding-right:10px" class="glyphicon glyphicon-plus" aria-hidden="true"></span>'+(key+1)+" : "+this.nama_kegiatan+'</a>';
//		});
		
		row+='</div>';
		
		num++;
	});
	
	obj_temp = response.data.penetapan_pernyataan;
	return row;
}

function render_penetapan_pernyataan(elem){
	console.log(elem);
	console.log(obj_temp);
	var tbody = $("#table").find('tbody');
	tbody.html('');
	
	var row="";
	var id_aspek=$(elem).data('aspek');
	var id_jenis=$(elem).data('kegiatan');
	var num = $("#table").find('tbody tr').length;
	num=num-num;
	
	$.each(obj_temp,function(idx, val_pernyataan){
		console.log(val_pernyataan);
		if(id_aspek==val_pernyataan.aspek_id_pernyataan && 
				id_jenis==val_pernyataan.jenis_audit_id_pernyataan){
//			console.log(arr_jawaban);
			row += '<tr>';

			row += '<td class="text-center"><div class="checkbox">'+
			'				  <label>'+
			'				    <input data-id='+val_pernyataan.id_pernyataan_penetapan+' '+(val_pernyataan.status_pernyataan_penetapan==1?"checked":"")+' onclick="active(this)" type="checkbox" id="blankCheckbox'+(num+1)+'" value="1" name=blankCheckbox'+(num+1)+'>'+
			'				  </label>'+
			'				</div></td>';
			row += '<td>'+(num+1)+'</td>';
			row +=	'<td>'+val_pernyataan.isi_pernyataan+'</td>';
			
			var arr_jawaban=[];
			if(val_pernyataan.listPernyataanJawaban != null)
			$.each(val_pernyataan.listPernyataanJawaban, function(idx2, val_jawaban){
				if(val_pernyataan.id_pernyataan==val_jawaban.header_pernyataan_id_pernyataan_jawaban){
					arr_jawaban.push(val_jawaban.isi_pernyataan_jawaban);	
				}	
			});

			row +='<td>'+arr_jawaban.length+'</td>';
			row +='<td>'+val_pernyataan.tahun_pernyataan_penetapan+'</td>';

			row +='<td><button onclick="edit(' + val_pernyataan.id_pernyataan_penetapan + ',)" class="btn btn-default btn-sm hidden"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
				row += '<button onclick="confirmRemove(' + val_pernyataan.id_pernyataan_penetapan + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
			row += '</td>';
					
			row +='</tr>';
			num++;
			
		}
	});
	console.log(row);
	tbody.html(row);
}
function render_update_row(response){
	var elem = $("#modalPengguna").data('object');
	var tbody = $("#"+elem);
	var value= response.data;
	var row = '<a style="padding-left: 60px;"  href="javascript:void(0)" class="list-group-item"><span  style="padding-right:10px" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'+value.isi_pernyataan+'</a>';
	tbody.prepend(row);
}


function renderPernyataanDisplay(page, params = '', timeout=0){
	var tbody = $("#table").find('tbody');
	setLoad(tbody, 'tbl');
	console.log(params);
	if(params!="") params="filter_aspek="+params;
	setTimeout(() => ajaxGET(url + 'list?page='+page+'&'+params+"&limits=20",'onGetListPernyataanSuccess','onGetListError'), timeout)
}



function onGetListPernyataanSuccess(response){
	console.log(response);
	var tbody = $("#table").find('tbody');
	tbody.html('');
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += '<tr>';
			row += '<td>'+(key+1)+'</td>';
			row += '<td>'+(value.kode_pernyataan!=null?value.kode_pernyataan:"-")+'</td>';
			row += '<td>'+value.isi_pernyataan+'</td>';
//			row += '<td><div class="checkbox">'+
//			'  <label>'+
//			'    <input name="pernyataan_id" type="checkbox" value="'+value.id_pernyataan+'">'+
//			'  </label>'+
//			'</div></td>';
			var arr_jawaban=[];
			if(value.listPernyataanJawaban != null)
			$.each(value.listPernyataanJawaban, function(idx2, val_jawaban){
				if(value.id_pernyataan==val_jawaban.header_pernyataan_id_pernyataan_jawaban){
					arr_jawaban.push(val_jawaban.isi_pernyataan_jawaban);	
				}	
			});

			row +='<td class="text-center">'+arr_jawaban.length+'</td>';

			row +='<td><button onclick="edit(' + value.id_pernyataan + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
			row += '<button onclick="confirmRemove(' + value.id_pernyataan + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
			row += '</td>';
					
		row += '</tr>';
		num++;
	});	
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
//	new_obj.data=response;
	//createPagination1('tblpages','renderPernyataanDisplay', response);
}

function createFormJawaban(){
	var temp="";
	for(var i=1; i <= $('[name=jumlah_jawaban]').val(); i++){
		temp += '<div class="input-group" style="margin-bottom:10px">'+
		'						  <input class="form-control hidden" name="order_jawaban" type="text" value="'+i+'">'+
		'						  <span class="input-group-addon" id="basic-addon1">'+i+'</span>'+
		'						  <input class="form-control hidden" name="id_jawaban" type="text" value="">'+
		'						  <span class="input-group-addon hidden" id="basic-addon1">-</span>'+
		'						  <input name="isi_jawaban" type="text" class="form-control" placeholder="Pilihan jawaban ke '+(i)+'" autocomplete="off" value="">'+
		'						</div>';
	}
	$('#form-pilihan-jawaban').html(temp);
	
	if($('#btn-create-form').data('obj').length>0){
		$.each($('#btn-create-form').data('obj'), function(key){
			$('[name=order_jawaban]')[key].value=this.order_pernyataan_jawaban;
			$('[name=id_jawaban]')[key].value=this.id_pernyataan_jawaban;
			$('[name=isi_jawaban]')[key].value=this.isi_pernyataan_jawaban;
		});
	}
	
}
