var url = ctx + "/sesi/";
var url_user_responden = ctx + "/sesi";
var url_responden = ctx + "/responden/";
var url_unit_kerja = ctx + "/unit_kerja/";
var url_kegiatan = ctx + "/kegiatan/";
var url_aspek = ctx + "/aspek/";
var url_ref = ctx +"/ref/";

var selected_status='';
var selected_satker='';
var selected_id='';

var param = "";
var selected_id='';
function init() {
	selected_status = ($.urlParam('status') == null ? 'SELESAI' : $.urlParam('status'));
	selected_satker = ($.urlParam('unit_auditi') == null ? '' : $.urlParam('unit_auditi'));
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));

	if(selected_satker != "") $('#label_satker').text(selected_satker)
	else $('#label_satker').html("<strong>Semua Satker</strong>");
	if(selected_id != "") {
		renderDisplay(1, '');
	}
	$('[name=filter_status_responden]').val(selected_status);
	
	getKegiatanList();
	getAspekList();

	getUnitKerjaList();
	getListTahun();
	getSumberList();
	

	$("#btnSearch").click(function(e){
		if($('#txtsearch').val()=="") {
			alert("Parameter pencarian tidak boleh kosong!");
			return;
		}
//		renderDisplay(1, selected_id); 
		window.location.href="hasil-survey?id="+$('#txtsearch').val()+"&status="+selected_status;
	});
}
function download(){
//	alert("DWN");
	if(selected_id!="")
	window.location.href=ctx+"/hasil-survey/export?filter_kode="+selected_id+"&filter_status_responden="+selected_status;
	else alert("SOMETHING WRONG");
}

function addLhp(){
	//resetRekomendasi();
	$("#progressBarSearchLhp").hide();
	//$("#temuanRekomendasi").val($("#test").text());
	$("#modalSearchLhp").modal('show');
}
function getKegiatanList(){
	$.getJSON(url_kegiatan+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=filter_kegiatan]").append(new Option(value.nama_kegiatan, value.id_kegiatan));
			});
		}
	});
}

function getAspekList(){
	$.getJSON(url_aspek+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=filter_aspek]").append(new Option(value.nama_aspek, value.id_aspek));
			});
		}
	});
}
function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=filter_tahun]").append(new Option(i, i));
	}
}

function getSumberList(){
	$.getJSON(url_ref+'sumber/list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
//				$("[name=sumber]").append(new Option(value.nama_sumber, value.nama_sumber));
				$("[name=filter_sumber]").append(new Option(value.nama_sumber, value.nama_sumber));
			});
		}
	});
}

function getUnitKerjaList(){
	$.getJSON(url_unit_kerja+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=filter_unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
			});
		}
	});
}

//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#table-card-body");
	if(selected_id !=""){
		params+="filter_kode="+selected_id;
	}else{
		params+=$('#form-filter').serialize();
	}
	params+="&filter_status=1&filter_status_responden="+selected_status;
	console.log(params);
	limit=100000;
	$.getJSON(ctx + "/hasil-survey/" + 'list?page='+page+'&limit='+limit+'&'+params, function(response) {
		if(response.code == 200){
			console.log("pernyataan_penetapan", response);
			//
			var wraper_menu_tab=$('#tab-menu');
			var wraper_content_tab=$('#tab-content');
			var li_menu="";
			var li_menu2="";
			var count_total_pernyataan=0;
			var count_all_total_pernyataan=0;
			var count_responden=0;
			var ctx;
			var chart_data_labels=[];
			var count_jawaban=0;
			var count_jawaban_new=0;
			$.each(response.data.main,function(key,value_main){
				console.log(value_main);
				
				li_menu2 += '<div class="tab-pane '+(key==0?"active":"")+'" id="tab-'+key+'">';

				count_total_pernyataan=0;
				
				$.each(response.data.penetapan_pernyataan,function(key,value_penetapan_pernyataan){
					console.log(value_penetapan_pernyataan);
					count_jawaban_new=0;
					if(value_main.id_aspek==value_penetapan_pernyataan.aspek_id_sesi_pernyataan){
						count_responden=0;
						$.each(value_penetapan_pernyataan.listSesiPernyataanJawaban,function(key,value_pernyataan_jawaban){
							count_jawaban=0;
							if(value_pernyataan_jawaban.header_sesi_pernyataan_id_pernyataan_jawaban==value_penetapan_pernyataan.id_sesi_pernyataan){
								
								$.each(response.data.list_responden_jawaban,function(key,value_responden_jawaban){
									if(value_responden_jawaban.jawaban_id_responden_jawaban==value_pernyataan_jawaban.id_sesi_pernyataan_jawaban){
										//if(value_responden_jawaban.jawaban_id_responden_jawaban==value_pernyataan_jawaban.id_sesi_pernyataan_jawaban){
											count_responden++;
										//}
									}
								});
								
								chart_data_labels.push(value_pernyataan_jawaban.isi_sesi_pernyataan_jawaban);
							}
						});
						
						var kode = '';
						if(value_penetapan_pernyataan.kode_sesi_pernyataan != null && value_penetapan_pernyataan.kode_sesi_pernyataan.length >0){
							kode = value_penetapan_pernyataan.kode_sesi_pernyataan + ': ';
						}
						
						li_menu2 += '<div class="row">'+
						'						<div class="col-md-12">'+
						'						<div class="panel panel-success">'+
						'						<div class="panel-body bg-success"><h5>'+kode + value_penetapan_pernyataan.isi_sesi_pernyataan+'</h5><small style="color: #777;">'+count_responden+' responden</small></div>'+
						
						'<div class="row">'+
						'							<div class="col-md-6 col-md-offset-3">'+
						'								<canvas id="myChart'+key+'">1</canvas>'+
						'							</div>'+
						'</div>'+
						'						</div>'+
						'						</div>'+
						'</div>';
						count_total_pernyataan++;
						count_all_total_pernyataan++;
					}
				});
				li_menu+='<li class='+(key==0?"active":"")+'><a href="#tab-'+key+'" data-toggle="tab">'+value_main.nama_aspek+' (<span id="jumlah-pernyataan">'+count_total_pernyataan+'</span>)</a></li>';

				li_menu2 +='</div>';
			});
			
			//
			wraper_menu_tab.html(li_menu);
			wraper_content_tab.html(li_menu2);
			
			//render form chart
			$.each(response.data.main,function(key,value_main){
				$.each(response.data.penetapan_pernyataan,function(key,value_penetapan_pernyataan){
					console.log(value_penetapan_pernyataan);
					var ars_jumlah_data=[];
					var ars_label_data=[];
					var count_jawaban=0;
					
					if(value_main.id_aspek==value_penetapan_pernyataan.aspek_id_sesi_pernyataan){
						ctx  = document.getElementById("myChart"+key+"").getContext('2d');
						
						$.each(value_penetapan_pernyataan.listSesiPernyataanJawaban,function(key,value_pernyataan_jawaban){
							count_jawaban=0;
							if(value_pernyataan_jawaban.header_sesi_pernyataan_id_pernyataan_jawaban==value_penetapan_pernyataan.id_sesi_pernyataan){
								$.each(response.data.list_responden_jawaban,function(key,value_responden_jawaban){
									if(value_responden_jawaban.jawaban_id_responden_jawaban==value_pernyataan_jawaban.id_sesi_pernyataan_jawaban){
										if(value_responden_jawaban.jawaban_id_responden_jawaban==value_pernyataan_jawaban.id_sesi_pernyataan_jawaban){
										   count_jawaban++;
										}
									}
								});
								
								chart_data_labels.push(value_pernyataan_jawaban.isi_sesi_pernyataan_jawaban);
								
								ars_jumlah_data.push(count_jawaban);
								ars_label_data.push(value_pernyataan_jawaban.isi_sesi_pernyataan_jawaban);
							}
						});
						//render dari sini,karena key
						var chart = new Chart(ctx, {
//										    // The type of chart we want to create
						    type: 'horizontalBar',

						    // The data for our dataset
						    data: {
						        labels: ars_label_data,
						        datasets: [
						        	{
							            label: 'Jawaban',
							            backgroundColor: 'rgba(75, 203, 115, 1)',
							            borderColor: 'rgb(255, 99, 132)',
							            data: ars_jumlah_data
							        }
						        ]
						    },

						    // Configuration options go here
						    options: {
						        legend: {
						            display: false,
						            labels: {
						                fontColor: 'rgb(255, 99, 132)'
						            }
						        }
						    }
						});
					}
					
				});	
				
			});
		}else{
			$("#tab-menu").html("<div class='text-center'>Data yang anda cari tidak ditemukan.</div>");
		}
	});
	//hide if succ
	$("#modalSearchLhp").modal('hide');
	if(selected_id!="")$('#txtsearch').val(selected_id);
	//
}

function onGetListSuccess(response){
	console.log(response);
	var wraper_menu_tab=$('#tab-menu');
	var wraper_content_tab=$('#tab-content');
	//render aspek kegiatan
	var li_menu="";
	var li_menu2="";
	var count_total_pernyataan=0;
	$.each(response.data.main,function(key,value_main){
		console.log(value_main);
		
		li_menu2 += '<div class="tab-pane '+(key==0?"active":"")+'" id="tab-'+key+'">';

		count_total_pernyataan=0;
		$.each(response.data.penetapan_pernyataan,function(key,value_penetapan_pernyataan){
			console.log(value_penetapan_pernyataan);
			if(value_main.id_aspek==value_penetapan_pernyataan.aspek_id_pernyataan){
				li_menu2 += '<div class="row">'+
				'						<div class="col-md-12">'+
				'						<div class="panel panel-success">'+
				'							<div class="panel-body bg-success"><p>'+value_penetapan_pernyataan.isi_pernyataan+'('+value_penetapan_pernyataan.aspek_id_pernyataan+')'+'</p></div>'+
				'							<ul class="list-group">';
				$.each(value_penetapan_pernyataan.listPernyataanJawaban,function(key,value_pernyataan_jawaban){
					if(value_pernyataan_jawaban.header_pernyataan_id_pernyataan_jawaban==value_penetapan_pernyataan.id_pernyataan){
						var count_jawaban=0;
						$.each(response.data.list_responden_jawaban,function(key,value_responden_jawaban){
							if(value_responden_jawaban.jawaban_id_responden_jawaban==value_pernyataan_jawaban.id_pernyataan_jawaban){
								count_jawaban++;
							}
						});
						li_menu2 += '<li class="list-group-item">'+value_pernyataan_jawaban.isi_pernyataan_jawaban+'<span class="badge">'+count_jawaban+'</span></li>';
					}
				});
				li_menu2 += ' 				</ul>'+
				'						</div>'+
				'						</div>'+
				'					</div>';
				count_total_pernyataan++;
			}
		});
		li_menu+='<li class='+(key==0?"active":"")+'><a href="#tab-'+key+'" data-toggle="tab">'+value_main.nama_aspek+' (<span id="jumlah-pernyataan">'+count_total_pernyataan+'</span>)</a></li>';

		li_menu2 +='</div>';
	});
	wraper_menu_tab.html(li_menu);
	wraper_content_tab.html(li_menu2);
}

function renderSesi(page, params = '', timeout=0){
	var tbody = $("#tblModal").find('tbody');
//	setLoad(tbody);
//	if($('#txtsearch').val() == ""){
		params+=$('#form-filter').serialize();
//	}else{
//		params+="filter_kode="+$('#txtsearch').val();
//	}
	params+="&filter_status=1";
	$.getJSON(url+'list'+ '?page='+page+'&limit='+100000+'&'+params, function(response) {
		console.log("sesi", response);
		//var row="";
		if (response.code == 200) {
			var row = "";
			$.each(response.data, function(key, value){
				row += '<tr onclick="location.href=\'hasil-survey?id=' + value.kode_sesi + '\'" class="data-row" id="row-'+value.kode_sesi+'">';
				row += '<td>'+(key+1)+'</td>';
				row += '<td>'+value.kode_sesi+'</td>';
				row += '<td>'+value.tahun_sesi+'</td>';
				row += '<td>'+value.periode_awal_sesi_serialize+' <strong>s/d</strong> '+value.periode_akhir_sesi_serialize+'</td>';


				row += '<td>'+(value.kegiatan==null?"-":value.kegiatan.nama_kegiatan)+'</td>';
				row += '<td>'+(value.unit_kerja==null?"-":value.unit_kerja.nama_unit_kerja)+'</td>';
				row += '<td>'+(value.unit_auditi==null?"-":value.unit_auditi.nama_unit_auditi)+'</td>';
				row += '<td class="text-center">'+value.jumlah_responden+'</td>';
				
				row += '</tr>';
			})
			tbody.html(row);
		}
	});
}

function resetFilterModal(){

	var tbody = $('#tblModal').find($('tbody')).text('');
	$('#form-filter').trigger('reset');
}