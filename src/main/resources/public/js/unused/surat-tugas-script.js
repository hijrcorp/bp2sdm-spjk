var url = ctx + "/surat-tugas/";
var url_jenis_pertanyaan = ctx + "/kategori-pertanyaan/";
var urlExport = ctx +"/rest/export/";
var param = "";
var selected_id='';
function init() {
//	display(1);
	renderDisplay(1, '', 0);
	getListTahun();
	getJenisPertanyaanList();
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
	
	$('#btnExport').click(function(){
		window.location = urlExport+"TUSI"+param;
	});
}
function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("#periodeAwal").append(new Option(i));
		$("#periodeAkhir").append(new Option(i));
	}
}
function getJenisPengawasanList(){
	$.getJSON(url+'jenisPengawasan/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("[name=jenis_pengawasan]").append(new Option(value.nama_jenis_pengawasan, value.id_jenis_pengawasan));
			});
			jenisPengawasan = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}

function getUnitKerjaList(){
	$.getJSON(url+'unitKerja/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("[name=unit_kerja]").append(new Option(value.namaUnitKerja, value.id));
			});
			unitKerja = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}
function getEselon1List(){
	$.getJSON(url+'eselon1/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("[name=unit_eselon1]").append(new Option(value.namaEselon1, value.id));
			});
			eselon1 = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}

function getJenisPertanyaanList(){
	$.getJSON(url_jenis_pertanyaan+'list', function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=jenis_pertanyaan]").append(new Option(value.nama_kategori_pertanyaan, value.id_kategori_pertanyaan));
			});
		}else{
			alert("Connection error");
		}
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		display(1);
		
		param = "&"+$('#column').data('id')+"="+$('#txtsearch').val()+'&jenis='+jenis;
		console.log(param);
		renderDisplay(1, param, 0);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modalPengguna").modal('show');
}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	$("#progressBar").hide();
	$("#modalPengguna").modal('hide');
	refresh();
	reset();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(page, '', 0);
}

function confirmRemove(id){
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modalconfirm").modal('show');
}
function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	refresh();
	$("#progressBar1").hide();
	$("#modalconfirm").modal('hide');
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modalPengguna").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=nomor_surat_tugas]').val(value.nomor_surat_tugas);
	$('[name=jenis_pengawasan]').val(value.jenis_pengawasan_id_surat_tugas);
	$('[name=unit_kerja]').val(value.unit_kerja_id_surat_tugas);
	$('[name=unit_eselon1]').val(value.unit_eselon1_id_surat_tugas);
//	if($('[name=bobot_jawaban]:checked').val() == value.isi_pertanyaan){
//		$('[name=bobot_jawaban]').val(value.isi_pertanyaan);
//	}
//	if(value.isi_jawaban && value.bobot_jawaban==1)
//	$('[name=bobot_jawaban]').each(function(key){
//		console.log(value.isi_jawaban)
//		if(value.isi_jawaban==this.value){
//			console.log(this.value)
//		}
//	});
//	$('#error_msg').hide();
//	$('#modal-form').modal('show');
//	reset();
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modalPengguna").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modalconfirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#tbl").find('tbody');

	var new_obj={
			'id' : 0,
			'element' : tbody,
			'path' : url,
			'fn' : {
				'edit' : {
					'name' : 'onEditSuccess',
					'type' : 'button'
				},
				'remove' : {
					'name' : 'onRemoveSuccess',
					'type' : 'button'
				}
			},
			'value' : {
					'id' : 'id_surat_tugas', 
					'value1' : 'nomor_surat_tugas', 
					'value2' : 'nama_jenis_pengawasan', 
					'value3' : 'nama_unit_kerja', 
					'value4' : 'nama_eselon1'
			},
			'data' : {},
			'mode' : {
				'filter' : false
			}
		};
	if(params == '') params='jenis='+jenis;
//	if(new_obj.mode.filter==true) params = $( "#frmFilter" ).serialize();
//	else params ="uraian="+$('#txtsearch').val();
	console.log(params);
//	setLoad('tbl');
	setTimeout(() => ajaxGET_new(new_obj.path + 'list?page='+page+'&'+params,'onGetListSuccess','onGetListError',new_obj), timeout)
}



function onGetListSuccess(response, new_obj){
	console.log(response);
	var tbody = new_obj.element
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += render_row(response.data[key], key, false, new_obj);
		num++;
	});
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	new_obj.data=response;
	createPagination('soal_pengetahuan','renderDisplay', new_obj);
}

function render_row(value, num, is_edit=false, new_obj){
//	new_obj.id=value[new_obj.value0];
	arr = JSON.stringify(new_obj);
	var row = "";
	if(!is_edit) row += '<tr class="data-row" id="row-'+value[new_obj.value.id]+'">';
	row += '<td>'+(num+1)+'</td>';
	
	var arr1 = [];
	var arr2 = [];

	for (var prop in new_obj.value) {
	   arr2.push(new_obj.value[prop]);
	}
	console.log(arr2);
	$.each(arr2,function(key){
		console.log(this.search("-"));
		if(this.search("-") != -1){
			var newThis = this.split("-");
			row += '<td>'+value[newThis[0]]+"-"+value[newThis[1]]+'</td>';
		}else{
			if(key>0) row += '<td>'+value[this]+'</td>';
		}
		
	});

	
	row += '<td>';
	console.log(new_obj.fn.edit.type=='href');
		if(new_obj.fn.edit.type=='href')
			row += '<a href="pertanyaan?mode=form&id='+value[new_obj.value.id]+'" class="btn btn-sm btn-square btn-outline-info" type="buttond"><i class="fa fa-pencil"></i></a> ';
		else 
			row += "<a href='javascript:void(0)' class='btn btn-default btn-sm' type='button' onclick='edit(\""+value[new_obj.value.id]+"\",arr)'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a> ";
		
		row += '<a href="javascript:void(0)" class="btn btn-default btn-sm" type="button" onclick="remove(\''+value[new_obj.value.id]+'\',0,arr)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
		
		//		row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="buttond" onclick="remove(\''+value.id_kelompok_jabatan+'\')"><i class="fa fa-trash-o"></i></a>';
	row += '</td>';
	
	if(!is_edit) row += '</tr>';
	return row;
}
/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(page){
	var tbody = $('#tbl').find($('tbody'));
	$('#btnRefresh').data("id",page);
	
	tbody.text('');
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	
	var param="";
	if($('#column').data('id') != "" && $('#txtsearch').val() != ""){
		param = "?"+$('#column').data('id')+"="+$('#txtsearch').val();
	}
	if(page > 1){
		param = "?page="+page+param;
	}
	$.ajax({
        url : url+"list"+param,
        type : "GET",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        success : function (response) {
        	console.log(response);
        	if (response.status == 200) {
        		var row = '';
        		tbody.text('');
        		$.each(response.data, function(key, value) {
        			row += '<tr>';
	        			row += '<td scope="row">'+(getRow(response)+key)+'</td>';
	        			row += '<td>'+value.namaKelompokUPT+'</td>';
	        			row += '<td>'+value.uraianTugasFungsi+'</td>';
	        			
	        			row += '<td><button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
	    				
	    				row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
	    				row += '</td>';
        			row += '</tr>';
        			
        		});
        		if(row == ''){
        			tbody.text('');
        			tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
        		}else{
        			tbody.html(row);
        		}
        		createPagination1('tblpages','display', response);
			}
        },
        error : function (response) {
        	alert("Connection error");
        },
    });
}
