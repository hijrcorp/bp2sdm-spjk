var url = ctx + "/sesi/";
var url_kegiatan = ctx + "/kegiatan/";
var url_unit_kerja = ctx + "/unit_kerja/";
var url_unit_auditi = ctx + "/unit_auditi/";
var url_eselon1 = ctx + "/eselon1/";
var urlExport = ctx +"/rest/export/";
var url_ref = ctx +"/ref/";

var param = "";
var selected_id='';
function init() {
	
	renderDisplay(1, '', 0);
	
	getListTahun();
	getKegiatanList();
	getUnitKerjaList();
	getUnitAuditiList();
	getEselon1List();
	getSumberList();
	

	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
//	$('#form-filter').submit(function(e){
//		//save();
//		renderDisplay(1, $(this).serialize(), 0);
//		e.preventDefault();
//	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
	
	$('#btnExport').click(function(){
		window.location = urlExport+"TUSI"+param;
	});
	
	$(document).on('click', 'table tbody tr', function() {
       $("table tbody tr").removeClass("info");
       $(this).toggleClass("info");
	});
	$(function() {
	    $("body").delegate(".datepicker", "focusin", function(){
	        $(this).datepicker();
	    });
	});
}

function setData(elem){
	var id = $(elem).data('parent');
	console.log(id);
	$('#btnConfirmImport').removeClass('disabled');
	$('#btnConfirmImport').data('id', elem);
	//findSuratTugas(elem);
}

function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=tahun]").append(new Option(i, i));
		$("[name=filter_tahun]").append(new Option(i, i));
	}
}

function getKegiatanList(){
	$.getJSON(url_kegiatan+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=id_kegiatan]").append(new Option(value.nama_kegiatan, value.id_kegiatan));
				$("[name=filter_kegiatan]").append(new Option(value.nama_kegiatan, value.id_kegiatan));
			});
		}
	});
}

function getUnitKerjaList(){
	$.getJSON(url_unit_kerja+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=id_unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
				$("[name=filter_unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
			});
		}
	});
}

function getUnitAuditiList(){
	$.getJSON(url_unit_auditi+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=id_unit_auditi]").append(new Option(value.nama_unit_auditi, value.id_unit_auditi));
				$("[name=filter_unit_auditi]").append(new Option(value.nama_unit_auditi, value.id_unit_auditi));
			});
		}
	});
}

function getEselon1List(){
	$.getJSON(url_eselon1+'list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=id_eselon1]").append(new Option(value.nama_eselon1, value.id_eselon1));
				$("[name=filter_eselon1]").append(new Option(value.nama_eselon1, value.id_eselon1));
			});
		}
	});
}

function getSumberList(){
	$.getJSON(url_ref+'sumber/list'+"?page="+1, function(response) {
		console.log(response);
		if (response.code == 200) {
			$.each(response.data, function(key, value) {
				$("[name=sumber]").append(new Option(value.nama_sumber, value.nama_sumber));
				$("[name=filter_sumber]").append(new Option(value.nama_sumber, value.nama_sumber));
			});
		}
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		//display(1);
		
		param = "&"+$('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		console.log(param);
		renderDisplay(1, param, 0);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	$("#progressBar").hide();
	$("#modal-form").modal('hide');

	$('#modalImportST').modal('hide');
	refresh();
	reset();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	console.log(response);
	addAlert('msg', "alert-danger", response.responseJSON.message);
}

function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
	$('[name=id_unit_auditi]').val('0').trigger("change");
}

function refresh(){
	//var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(1, '', 0);
}

function confirmRemove(id){
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}
function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	refresh();
	$("#progressBar1").hide();
	$("#modal-confirm").modal('hide');
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modal-form").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=kode]').val(value.kode_sesi);
	$('[name=tahun]').val(value.tahun_sesi);
	$('[name=kode]').val(value.kode_sesi);
	$('[name=periode_awal]').val(value.periode_awal_sesi_serialize);
	$('[name=periode_akhir]').val(value.periode_akhir_sesi_serialize);
	$('[name=keterangan]').val(value.keterangan_sesi);
	$('[name=id_unit_kerja]').val(value.unit_kerja_id_sesi);
	$('[name=id_unit_auditi]').val(value.unit_auditi_id_sesi).trigger("change");
	$('[name=id_eselon1]').val(value.eselon1_id_sesi);
	$('[name=id_kegiatan]').val(value.kegiatan_id_sesi);
	$('[name=jumlah_responden]').val(value.jumlah_responden_sesi);
	$('[name=sumber]').val(value.sumber_sesi);
	
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#tbl").find('tbody');

	var new_obj={
			'id' : 0,
			'element' : tbody,
			'path' : url,
			'fn' : {
				'edit' : {
					'name' : 'onEditSuccess',
					'type' : 'button'
				},
				'remove' : {
					'name' : 'onRemoveSuccess',
					'type' : 'button'
				},
				'responden' : {
					'name' : 'onRespondenSuccess',
					'type' : 'button'
				}
			},
			'value' : {
					'id' : 'id_sesi', 
					'value1' : 'kode_sesi', 
					'value2' : 'tahun_sesi', 
					'value3' : 'periode_awal_sesi_serialize' +'-'+'periode_akhir_sesi_serialize', 
					'value4' : 'keterangan_sesi', 
					'value5' : 'jumlah_responden', 
					'value6' : 'sumber_sesi'
			},
			'data' : {},
			'mode' : {
				'filter' : false
			}
		};
//	if(params == '') params='jenis='+jenis;
//	if(new_obj.mode.filter==true) params = $( "#frmFilter" ).serialize();
//	else params ="uraian="+$('#txtsearch').val();
	console.log(params);
//	setLoad('tbl');
	if(params!="") params=$('#form-filter').serialize();
	setTimeout(() => ajaxGET_new(new_obj.path + 'list?page='+page+'&'+params,'onGetListSuccess','onGetListError',new_obj), timeout)
}



function onGetListSuccess(response, new_obj){
	createPagination2('tblpages','renderDisplay', response);
	console.log(response);
	var tbody = new_obj.element
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
//	$no = (($page - 1) * $limit) + 1;
	var no = ((($('#tblpages').find('.active').text())-1)*response.data.length)+1;
	$.each(response.data,function(key,value){
//		row += render_row(response.data[key], key, false, new_obj);
		row += render_row1(value, no, false);
		num++;
		no++;
	});
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	new_obj.data=response;
//	createPagination('soal_pengetahuan','renderDisplay', new_obj);
}

//function render_row(value, num, is_edit=false, new_obj){
//	console.log(value);
////	new_obj.id=value[new_obj.value0];
//	arr = JSON.stringify(new_obj);
//	var row = "";
//	if(!is_edit) row += '<tr class="data-row" id="row-'+value[new_obj.value.id]+'">';
//	row += '<td>'+(num+1)+'</td>';
//	
//	var arr1 = [];
//	var arr2 = [];
//
//	for (var prop in new_obj.value) {
//	   arr2.push(new_obj.value[prop]);
//	}
//	console.log(arr2);
//	$.each(arr2,function(key){
//		console.log(this.search("-"));
//		if(this.search("-") != -1){
//			var newThis = this.split("-");
//			row += '<td>'+value[newThis[0]]+"-"+value[newThis[1]]+'</td>';
//		}else{
//			if(key>0) row += '<td>'+value[this]+'</td>';
//		}
//		
//	});
//
//	
//	row += '<td>';
//	console.log(new_obj.fn.edit.type=='href');
//		if(new_obj.fn.edit.type=='href')
//			row += '<a href="pertanyaan?mode=form&id='+value[new_obj.value.id]+'" class="btn btn-sm btn-square btn-outline-info" type="buttond"><i class="fa fa-pencil"></i></a> ';
//		else 
//			row += "<a href='javascript:void(0)' class='btn btn-default btn-sm' type='button' onclick='edit(\""+value[new_obj.value.id]+"\",arr)'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a> ";
//		
//		row += '<a href="javascript:void(0)" class="btn btn-default btn-sm" type="button" onclick="remove(\''+value[new_obj.value.id]+'\',0,arr)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> ';
//		row += '<a href="generate-user-survey?id='+value[new_obj.value.id]+'" class="btn btn-default btn-sm" type="button" ><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>';
//		
//		//		row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="buttond" onclick="remove(\''+value.id_kelompok_jabatan+'\')"><i class="fa fa-trash-o"></i></a>';
//	row += '</td>';
//	
//	if(!is_edit) row += '</tr>';
//	return row;
//}


function render_row1(value, num, is_edit=false){
	
	var row = "";
	if(!is_edit) row += '<tr class="data-row small" id="row-'+value.id_surat_tugas+'">';
	row += '<td>'+(num)+'</td>';
	row += '<td>'+value.kode_sesi+'</td>';
	
	row += '<td>'+value.tahun_sesi+'</td>';
	
	row += '<td class="">'+value.periode_awal_sesi_serialize+' <strong>s.d</strong> '+value.periode_akhir_sesi_serialize+' ('+value.jumlah_hari_periode+' Hari)</td>';

//	console.log(value.keterangan_sesi.toString());
//	console.log(JSON.parse(value.keterangan_sesi));
	row += '<td class="text-small ">'+value.keterangan_sesi+'</td>';

	row += '<td>'+(value.kegiatan==null?"-":value.kegiatan.nama_kegiatan)+'</td>';
	row += '<td>'+(value.unit_kerja==null?"-":value.unit_kerja.nama_unit_kerja)+'</td>';
	row += '<td>'+(value.unit_auditi==null?"-":value.unit_auditi.nama_unit_auditi)+'</td>';
//	row += '<td>'+value.unit_auditi.nama_unit_auditi+'</td>';
	row += '<td class="text-center"><a href="generate-user-survey?id='+value['id_sesi']+'" >'+value.jumlah_responden+"/"+value.jumlah_responden_selesai+'<a/></td>';

	row += '<td>'+value.sumber_sesi+'</td>';
	
	row += '<td class="text-center">';
	if(value.sumber_sesi.startsWith('INTERNAL')){
		row += "<a href='javascript:void(0)' class='btn btn-default btn-sm' type='button' onclick='edit(\""+value['id_sesi']+"\")'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a> ";
		row += '<a href="javascript:void(0)" class="btn btn-default btn-sm" type="button" onclick="remove(\''+value['id_sesi']+'\',0)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> ';
	}
		row += '<a href="generate-user-survey?id='+value['id_sesi']+'" class="btn btn-default btn-sm" type="button"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a> ';
	row += '</td>';
	
	if(!is_edit) row += '</tr>';
	
	return row;
}
