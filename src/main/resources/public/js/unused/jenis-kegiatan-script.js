var url = ctx + "/kegiatan/";
var urlExport = ctx +"/rest/export/";
var param = "";
var selected_id='';
function init() {
//	display(1);
	renderDisplay(1, '', 0);
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
	
	$('#btnExport').click(function(){
		window.location = urlExport+"TUSI"+param;
	});
}
function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("#periodeAwal").append(new Option(i));
		$("#periodeAkhir").append(new Option(i));
	}
}
function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		param = "&"+$('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		console.log(param);
		renderDisplay(1, param, 0);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	$("#progressBar").hide();
	$("#modal-form").modal('hide');
	refresh();
	reset();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(page, '', 0);
}

function confirmRemove(id){
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}
function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	refresh();
	$("#progressBar1").hide();
	$("#modal-confirm").modal('hide');
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modal-form").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=kode]').val(value.kode_kegiatan);
	$('[name=nama]').val(value.nama_kegiatan);

//	$('#error_msg').hide();
//	$('#modal-form').modal('show');
//	reset();
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#tbl").find('tbody');

	var new_obj={
			'id' : 0,
			'element' : tbody,
			'path' : url,
			'fn' : {
				'edit' : {
					'name' : 'onEditSuccess',
					'type' : 'button'
				},
				'remove' : {
					'name' : 'onRemoveSuccess',
					'type' : 'button'
				}
			},
			'value' : {
					'id' : 'id_kegiatan', 
					'value1' : 'kode_kegiatan', 
					'value2' : 'nama_kegiatan'
			},
			'data' : {},
			'mode' : {
				'filter' : false
			}
		};
//	if(params == '') params='jenis='+jenis;
//	if(new_obj.mode.filter==true) params = $( "#frmFilter" ).serialize();
//	else params ="uraian="+$('#txtsearch').val();
	console.log(params);
//	setLoad('tbl');
	setTimeout(() => ajaxGET_new(new_obj.path + 'list?page='+page+'&'+params,'onGetListSuccess','onGetListError',new_obj), timeout)
}



function onGetListSuccess(response, new_obj){
	console.log(response);
	var tbody = new_obj.element
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += render_row(response.data[key], key, false, new_obj);
		num++;
	});
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	new_obj.data=response;
	createPagination('soal_pengetahuan','renderDisplay', new_obj);
}

function render_row(value, num, is_edit=false, new_obj){
//	new_obj.id=value[new_obj.value0];
	arr = JSON.stringify(new_obj);
	var row = "";
	if(!is_edit) row += '<tr class="data-row" id="row-'+value[new_obj.value.id]+'">';
	row += '<td>'+(num+1)+'</td>';
	
	var arr1 = [];
	var arr2 = [];

	for (var prop in new_obj.value) {
	   arr2.push(new_obj.value[prop]);
	}
	console.log(arr2);
	$.each(arr2,function(key){
		console.log(this.search("-"));
		if(this.search("-") != -1){
			var newThis = this.split("-");
			row += '<td>'+value[newThis[0]]+"-"+value[newThis[1]]+'</td>';
		}else{
			if(key>0) row += '<td>'+value[this]+'</td>';
		}
		
	});

	
	row += '<td>';
	console.log(new_obj.fn.edit.type=='href');
		if(new_obj.fn.edit.type=='href')
			row += '<a href="pertanyaan?mode=form&id='+value[new_obj.value.id]+'" class="btn btn-sm btn-square btn-outline-info" type="buttond"><i class="fa fa-pencil"></i></a> ';
		else 
			row += "<a href='javascript:void(0)' class='btn btn-default btn-sm' type='button' onclick='edit(\""+value[new_obj.value.id]+"\",arr)'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a> ";
		
		row += '<a href="javascript:void(0)" class="btn btn-default btn-sm" type="button" onclick="remove(\''+value[new_obj.value.id]+'\',0,arr)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
		
		//		row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="buttond" onclick="remove(\''+value.id_kelompok_jabatan+'\')"><i class="fa fa-trash-o"></i></a>';
	row += '</td>';
	
	if(!is_edit) row += '</tr>';
	return row;
}

