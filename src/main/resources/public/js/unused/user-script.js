var url = ctx + "/user/";
var selected_id=0;
function init() {
	
	display(1);
	getPositionList();
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		display(1);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function getPositionList(){
	$.getJSON(url+'position/'+"?token="+token, function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("#position").append(new Option(value.name_position, value.position_id));
			});
		}
	});
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#passwordForm").show();
	$("#modalPengguna").modal('show');
}

function validate(){
	var msg = '';
	
	if($('#password').val() != $('#repassword').val()){
		msg += 'Ketikan ulang kata kunci tidak cocok <br />';
	}
	
	// if save existing record (EDIT/UPDATE)
	if ($('#id').val() == '0'){
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == '' || $('#password').val() == '' || $('#repassword').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}else{
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}	
	return msg;
}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(url + 'save',obj,'succSav','onSaveError');
}
function succSav(response){
	refresh();
	$('#modalPengguna').modal('hide');
	$('#frmInput')[0].reset();
}
function onSaveError(response){
	console.log(response.responseJSON.message);
	addAlert('msg', "alert-danger", response.responseJSON.message);
}

//function save(){
//	var msg = validate();
//	
//	if (msg == '') {
//		cleanMessage('msg');
//		$("#progressBar").show();
//		
//		var obj = {
//				"user" : {
//					"id" : $('#id').val(),
//					"firstName" : $('#first_name').val(),
//					"lastName" : $('#last_name').val(),
//					"currentPassword" : $('#password').val(),
//					"username" : $('#username').val(),
//					"position" : {
//						"id": $('#position').val()
//					},
//					"description" : $('#description').val()
//				}
//		}
//
//		console.log(JSON.stringify(obj));
//		$.ajax({
//	        url : url+"save?token="+token,
//	        type : "POST",
//	        traditional : true,
//	        contentType : "application/json",
//	        dataType : "json",
//	        data : JSON.stringify(obj),
//	        success : function (response) {
//	        	$("#progressBar").hide();
//	        	if (response.status == 'OK') {
//	        		
//	        		$("#modalPengguna").modal('hide');
//	        		refresh();
//	        		reset();
//				}else{
//					var msg = 'Gagal Simpan. ';
//					//if(response.message.indexOf('DUPLICATE') >= 0) {
//					//	var errmsg = response.message.split(':');
//					//	addAlert('msg', "alert-danger", msg + 'Duplikasi data input: `' + errmsg[1] + '`');
//					//}else{
//						addAlert('msg', "alert-danger", msg + 'Error tidak diketahui');
//					//}
//					
//				}
//	        },
//	        error : function (response) {
//	        	$("#progressBar").hide();
//	        	alert("Connection error");
//	        },
//	    });
//	}else{
//		addAlert('msg', "alert-danger", msg);
//	}
//	
//}

function reset(){
	cleanMessage('msg');
	
	$('#id').val('0');
	$('#fullname').val('');
	$('#username').val('');
	$('#first_name').val('');
	$('#last_name').val('');
	$('#password').val('');
	$('#repassword').val('');
	$('#password1').val('');
	$('#repassword1').val('');
	$('#position').val('0');
	$('#description').val('');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

function confirmRemove(id){
	// set data id to confirm dialog
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	console.log(response);
	refresh();
	$("#modal-confirm").modal('hide');
}

function edit(id){
	reset();
	$("#progressBar").show();
	$("#passwordForm").hide();
	$("#modalPengguna").modal('show');
	
	ajaxGET(url+id,'onSuccessById','onFailedById','onError');
	selected_id = id;
}
function onSuccessById(response) {
	var o = response.data;
	console.log(response);
	$('[name=first_name]').val(o.firstName);
	$('[name=last_name]').val(o.lastName);
	$('[name=username]').val(o.username);
	$('[name=position]').val(o.positionId);
	$('[name=email]').val(o.email);

	$("#progressBar").hide();
}

function editPassword(id){
	$("#progressBar2").hide();
	$("#btnUpdate").data("id",id);
	$("#modalPassword").modal('show');
}

function doUpdate(id){
	var obj = new FormData(document.querySelector('#frmInputPass'));
	$("#progressBar2").show();
	ajaxPOST(url+id+'/updatePass',obj,'succUpdate','onUpdateError');
}
//function doUpdate(){
//	var obj = $('#frm-password').serialize();
//	ajaxPOST(ctx + '/user/'+paramId+'/updatePass',obj,'succUpdate','failSav','errSav');
//}
function succUpdate(response){
	$('#modalPassword').modal('hide');
	$('#frmInputPass')[0].reset();
}
function onUpdateError(response){
	console.log(response.responseJSON.message);
	addAlert('msg1', "alert-danger", response.responseJSON.message);
}

//function display(page){
//
//	var filter = [];
//	
//	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
//    	var clause = {
//					"column" : $('#column').data('id'),
//					"operator" : "like",
//					"value" : $('#txtsearch').val()
//				};
//    	
//    	filter.push(clause);
//	}
//	
//	var obj = {
//			"pagination" : {
//				"rowcount" : 20,
//				"activepage" : page,
//				"filter": filter
//			}
//	}
//	
//	var tbody = $('#tbl').find($('tbody'));
//	$('#btnRefresh').data("id",page);
//	
//	tbody.text('');
//	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
//	
//	$.ajax({
//        url : url+"?token="+token,
//        type : "POST",
//        traditional : true,
//        contentType : "application/json",
//        dataType : "json",
//        data : JSON.stringify(obj),
//        success : function (response) {
//        	if (response.status == 'OK') {
//        		var row = '';
//        		tbody.text('');
//        		var i = 1;
//        		
//        		$.each(response.data, function(key, value) {
//        			
//        			row += '<tr>';
//        			row += '<td scope="row">'+(getRow(response)+key)+'</td>';
//        			row += '<td>'+value.name+'</td>';
//        			row += '<td>'+value.username+'</td>';
//        			row += '<td>'+value.position.name+'</td>';
//        			row += '<td>'+value.description+'</td>';
//        			
//        			row += '<td><button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
//    				
//    				row += '<button onclick="editPassword(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></button> ';
//    				
//    				row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
//    				row += '</td>';
//        			
//        			row += '</tr>';
//        			
//        			i++;
//        		});
//        		if(row == ''){
//        			tbody.text('');
//        			tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
//        		}else{
//        			tbody.html(row);
//        		}
//        		createPagination('tblpages','display', response.pagination);
//			}else{
//				alert("Connection error");
//			}
//        },
//        error : function (response) {
//        	alert("Connection error");
//        },
//    });
//}

//batas
function display(page = 1, param=""){
	var params = "";
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		param = "&"+$('#column').data('id')+"="+$('#txtsearch').val()
	}
	
	if($('[name=filter_tanpa_responden]:checked').val()!=undefined){
		param += "&filter_tanpa_responden=1";
	}
	params = "?page="+page+param;
	
	var tbody = $('#tbl').find($('tbody'));
	$('#btnRefresh').data("id",page);
	ajaxGET(url + 'list'+params+"&limit=",'succDisp','failDisp');
}
function succDisp(response){
	var tbody = $("#tbl").find('tbody');
	tbody.text("");
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	var row = "";
	console.log(response.data);
	$.each(response.data,function(key,value){
		row += "<tr scope='row'>";
			row += "<th class=text-left>"+(key+1)+"</th>";
			row += "<td>"+(this.name != "null null" ? this.name : "(Tidak Ada)")+"</td>";
			row += "<td>"+this.username+"</td>";
			row += "<td>"+this.positionName+"</td>";
			row += "<td>"+(this.unitKerjaName != null && this.unitKerjaName != "" ? this.unitKerjaName : "(Tidak Ditentukan)")+"</td>";
			row += "<td>"+(this.email != null && this.email != "" ? this.email : "(Tidak Ada)")+"</td>";
			//row += "<td>"+renderBtnAct(this.id,this.dokumen)+"</td>";
		
			row += '<td>';
			row += '<button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
			row += '<button onclick="editPassword(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></button> ';
			row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>';
			row += '</td>';
			
		row += "</tr>";
	});
	row == "" ? tbody.html('<tr><td colspan="10" align="center">Data Tidak Ada.</td></tr>') : tbody.html(row);
	createPagination1('tblpages','display', response);
}

function failDisp(response){
	console.log(response);
}
function renderBtnAct(id,dokumen){
	return '<button onclick="edit('+id+')" class="btn btn-yellow btn-xs shadowmod waves-effect waves-light" style="margin: 1px;"><i class="fa fa-pencil"></i></button>'+
		   '<button onclick="editPassword('+id+')" class="btn btn-info btn-xs shadowmod waves-effect waves-light" style="margin: 1px;"><i class="fa fa-lock"></i></button>'+
		   '<button onclick="remove('+id+')" class="btn btn-danger btn-xs shadowmod waves-effect waves-light" style="margin: 1px;"><i class="fa fa-trash"></i></button>';
}

