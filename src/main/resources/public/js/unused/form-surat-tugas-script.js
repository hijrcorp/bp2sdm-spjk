var url = ctx + "/rest/lhp/";
var url_surat_tugas = ctx + "/surat-tugas/";
var url_surat_tugas_tim = ctx + "/surat-tugas-tim/";
var urlPemeriksa = ctx + "/rest/pemeriksa/";
var urlPegawai = ctx + "/rest/pegawai/";
var urlUpload = ctx + "/rest/uploadFile/";

var unitKerja = false;
var pegawai = false;
var peran = false;
var selected_id = 0;
var selected_sub_id = 0;
var header_id = 0;
var eselon1 = false;
var jenisPengawasan = false;

function finishLoading(){
	if (unitKerja == true && pegawai == true && peran == true && eselon1 == true && jenisPengawasan == true){
		
//		var paramid = getUrlParameter("id");
		if (selected_id > 0) {
			getUrl(selected_id);
		}else{
			$('.combobox').combobox();
			$('#modalProgress').modal('hide');
		}
		
	}
}

function init() {
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
	//display(1);
	$('#modalProgress').modal('show');
	
	getUnitKerjaList();
	getJenisPengawasanList();
	getPegawaiList();
	getPeranList();
	getEselon1List();
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnBatal").click(function(e){
		var pegawai = $("#selPegawaiI").text();
		var peran = $("#selPeranI").text();
		if ($("#id").val == 0) {
			$("#pegawai").val(pegawai);
			$("#pegawai").val(peran);
		}
	});
	
	$("#btnReset").click(function(e){
		reset();
	});

	$("#btnResetPemeriksa").click(function(e){
		resetPemeriksa();
	});
	
	$("#btnRefresh").click(function(e){
		//refresh();
		//alert($("#selPegawai").text());
	});
	
	$('#btnSimpan').click(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btnSavePemeriksa').click(function(e){
		savePemeriksaD();
	});
	
}

function getUrl(id) {
	
    //$("#id").val(data);
    
    	console.log("ini mau update");
    	console.log("ini mau update");
    	reset();
    	//$('#modalProgress').modal('show');
    	$.getJSON(url_surat_tugas+id+"?token="+token, function(response, status) {
    		console.log(response);
    		//$("#progressBar").hide();
        	if (response.code == 200) {
        		var o = response.data.surat_tugas;
    	    	$('[name=unit_kerja]').val(o.unit_kerja_id_surat_tugas);
    	    	$('[name=nomor_surat_tugas]').val(o.nomor_surat_tugas);
    	    	$('[name=unit_eselon1]').val(o.unit_eselon1_id_surat_tugas);
    	    	$('[name=jenis_pengawasan]').val(o.jenis_pengawasan_id_surat_tugas);
    	    	
    	    	display(response.data.surat_tugas_tim);
    	    	$('#modalProgress').modal('hide');
        	}else{
    			alert("Connection error");
    		}
//        	$('.combobox').combobox();
        	$('#modalProgress').modal('hide');
    	});    	
    	//show data form pemeriksa
    	//display(data);
}

function getJenisPengawasanList(){
	$.getJSON(url+'jenisPengawasan/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("[name=jenis_pengawasan]").append(new Option(value.nama_jenis_pengawasan, value.id_jenis_pengawasan));
			});
			jenisPengawasan = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}

function getUnitKerjaList(){
	$.getJSON(url+'unitKerja/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("[name=unit_kerja]").append(new Option(value.namaUnitKerja, value.id));
			});
			unitKerja = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}
function getEselon1List(){
	$.getJSON(url+'eselon1/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("[name=unit_eselon1]").append(new Option(value.namaEselon1, value.id));
			});
			eselon1 = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}
function getPegawaiList(){
	$.getJSON(url+'pegawai/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("#pegawai").append(new Option(value.namaPegawai, value.id));

			});
			pegawai = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}


function getPeranList(){
	$.getJSON(url+'peran/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("#peran").append(new Option(value.namaPeran, value.id));
			});
			peran = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}

function add(){
	resetPemeriksa();
	$("#progressBar").hide();
	$("#modalPengguna").modal('show');
}

function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    ajaxPOST(url_surat_tugas + 'save',obj,'onSaveSuccess','onSaveError');
}
function onSaveSuccess(response){
	$("#progressBar").hide();
	$("#modalPengguna").modal('hide');
	refresh();
	reset();
	location.href='form-surat-tugas?id='+selected_id;
//	if(selected_id > 0){
//		console.log(response);
//		display(response.data);
//	}else{
//		location.href='surat-tugas';
//	}
}
function failSav(response){
	addAlert('msg1', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg1', "alert-danger", "kesalahan dari server");
}

function reset(){
	cleanMessage('msg');
	selected_sub_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmPemeriksa').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
//	display(page);
	renderDisplay(page, '', 0);
}

function savePemeriksaD(){
	var obj = new FormData(document.querySelector('#frmPemeriksa'));
	if(selected_sub_id != '') obj.append('id', selected_sub_id);
	obj.append('header_surat_tugas', selected_id);
	obj.append('jabatan_tim', $('[name=jabatan] :selected').text());
	obj.append('nama_tim', $('[name=nama] :selected').text());
    console.log(obj);
    ajaxPOST(url_surat_tugas_tim + 'save',obj,'onSaveSuccess','onSaveError');
	//$("#modalPengguna").modal('hide');
	//addAlert('msg', "alert-danger", msg);
}

function reset(){
//	var tbody = $('#tbl').find($('tbody'));
//	tbody.text('');
//	cleanMessage('msg1');
//
//	$('#id').val('0');
//	$('#nomorLhp').val('');
//	$('#tanggalLhp').val('');
//	$('#tanggalTerimaLhp').val('');
//	$('#pkpt').val('');
//	$('#unitAuditi').val('0');
//	$('#auditiKepala').val('');
//	$('#auditiNip').val('');
//	$('#jenisAudit').val('0');
//	$('#unitKerja').val('0');
//	$('#nomorSurat').val('');
//	$('#tanggalSurat').val('');
//	$('#lamaAudit').val('');
//	$('#tanggalAwal').val('');
//	$('#tanggalAkhir').val('');
//	$('#periodeAwal').val('0');
//	$('#periodeAkhir').val('0');
//	$('#fileBerkas').val('0');
//	$('#fileName').val('');
//	$('#eselon1').val('0');
//	$('#propinsi').val('0');
	
}

function resetPemeriksa(){
	cleanMessage('msg');

	$('#rowNum').val('0');
	$('#idPemeriksa').val('0');
	$('#lhp').val('0');
	$('#peran').val('0');
	$('#pegawai').val('').trigger("change");
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

function confirmRemove(id){
	// set data id to confirm dialog
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modalconfirm").modal('show');
}

function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url_surat_tugas_tim+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	console.log(response);
	location.href='form-surat-tugas?id='+selected_id;
//	refresh();
	$("#progressBar1").hide();
	$("#modalconfirm").modal('hide');
}

//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modalPengguna").modal('show');
	selected_sub_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url_surat_tugas_tim + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=nama]').val(value.nama_surat_tugas_tim);
	$('[name=jabatan]').val(value.jabatan_surat_tugas_tim);
	$("#progressBar").hide();
	$("#modalPengguna").modal('show');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(data){
	console.log(data);
	console.log(typeof(data));
	var tbody = $('#tbl').find($('tbody'));
	$('#btnRefresh').data("id",1);
	
	tbody.text('');
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	
	var row="";
	i = 1;
	$.each(data, function(key, value) {
//
		row += '<tr>';
		row += '<td>'+i+'</td>';
		row += '<td>'+value.nama_tim+' </td>';
		row += '<td>'+value.jabatan_tim+'</td>';
		
		row += '<td><button onclick="edit(\'' + value.id_surat_tugas_tim + '\')" class="btn btn-default btn-sm" type="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
		
		row += '<button onclick="confirmRemove(' + value.id_surat_tugas_tim + ')" class="btn btn-default btn-sm" type="button"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
		row += '</td>';
		
		row += '</tr>';
		
		i++;
	});
	if(row == ''){
		tbody.text('');
		//tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
	}else{
		tbody.html(row);
	}
			
}
