var path = ctx + '/restql';

var param = "";
var selected_id='';

var appendIds = [];
function init() {
	display();
	
	$("#btnAdd").click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
	
	$('#btnRemove').click(function(e){//btn-modal-confirm-yes
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		//param = $('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		//console.log(param);
		display(1, 'filter=');
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function save(){
	//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/unit_kompetensi/save?append=false';
	
	window.obj = {
			data : objectifyForm($('#frmInput').serializeArray())
		};
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		//$.LoadingOverlay("hide");
        		//$('#modal-form').modal('hide');
        		//display();
        	onModalActionSuccess(response);
        },
        error : function (response) {
        		//$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        },
    });
}

/*function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    ajaxPOST(url + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
	$("#progressBar").hide();
	$("#modal-form").modal('hide');
	refresh();
	reset();
}*/

function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function clearForm(){
	$('#frmInput')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();
}


function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#frmInput').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

function doRemove(id){
	$("#progressBar1").show();
	if(selected_action == 'delete' ){
		doAction(selected_id, selected_action, 1);
	}
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modal-form").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=nama]').val(value.nama_aspek);
	$('[name=sumber]').val(value.sumber_aspek);

//	$('#error_msg').hide();
//	$('#modal-form').modal('show');
//	reset();
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	var tbody = $("#tbl").find('tbody');

	var new_obj={
			'id' : 0,
			'element' : tbody,
			'path' : url,
			'fn' : {
				'edit' : {
					'name' : 'onEditSuccess',
					'type' : 'button'
				},
				'remove' : {
					'name' : 'onRemoveSuccess',
					'type' : 'button'
				}
			},
			'value' : {
					'id' : 'id_aspek', 
					'value1' : 'nama_aspek', 
					'value2' : 'sumber_aspek'
			},
			'data' : {},
			'mode' : {
				'filter' : false
			}
		};
//	if(params == '') params='jenis='+jenis;
//	if(new_obj.mode.filter==true) params = $( "#frmFilter" ).serialize();
//	else params ="uraian="+$('#txtsearch').val();
	console.log(params);
//	setLoad('tbl');
	setTimeout(() => ajaxGET_new(new_obj.path + 'list?page='+page+'&'+params,'onGetListSuccess','onGetListError',new_obj), timeout)
}



function onGetListSuccess(response, new_obj){
	console.log(response);
	var tbody = new_obj.element
	$('#refresh').removeClass('disabled');
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += render_row(response.data[key], key, false, new_obj);
		num++;
	});
	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	new_obj.data=response;
	createPagination('soal_pengetahuan','renderDisplay', new_obj);
}

function render_row(value, num, is_edit=false, new_obj){
//	new_obj.id=value[new_obj.value0];
	arr = JSON.stringify(new_obj);
	var row = "";
	if(!is_edit) row += '<tr class="data-row" id="row-'+value[new_obj.value.id]+'">';
	row += '<td>'+(num+1)+'</td>';
	
	var arr1 = [];
	var arr2 = [];

	for (var prop in new_obj.value) {
	   arr2.push(new_obj.value[prop]);
	}
	console.log(arr2);
	$.each(arr2,function(key){
		console.log(this.search("-"));
		if(this.search("-") != -1){
			var newThis = this.split("-");
			row += '<td>'+value[newThis[0]]+"-"+value[newThis[1]]+'</td>';
		}else{
			if(key>0) row += '<td>'+value[this]+'</td>';
		}
		
	});

	
	row += '<td>';
	console.log(new_obj.fn.edit.type=='href');
		if(new_obj.fn.edit.type=='href')
			row += '<a href="pertanyaan?mode=form&id='+value[new_obj.value.id]+'" class="btn btn-sm btn-square btn-outline-info" type="buttond"><i class="fa fa-pencil"></i></a> ';
		else 
			row += "<a href='javascript:void(0)' class='btn btn-default btn-sm' type='button' onclick='edit(\""+value[new_obj.value.id]+"\",arr)'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a> ";
		
		row += '<a href="javascript:void(0)" class="btn btn-default btn-sm" type="button" onclick="remove(\''+value[new_obj.value.id]+'\',0,arr)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
		
		//		row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-danger" type="buttond" onclick="remove(\''+value.id_kelompok_jabatan+'\')"><i class="fa fa-trash-o"></i></a>';
	row += '</td>';
	
	if(!is_edit) row += '</tr>';
	return row;
}
/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	if($('#txtsearch').val() != '' && $('#column').data('id') != '' || page ==1){
		tbody.text('');
	}
	
	if($('#column').data('id') != ''){
		params+= $('#column').data('id')+'.slike \''+$('#txtsearch').val()+'\';';
	}
	
	if($('[name=filter_nama]').val() == '' && $('[name=filter_nomor]').val() == '' 
		&& $('[name=filter_id_jabatan]').val() == '' && $('[name=filter_alamat_rumah]').val() == '' 
			&& $('[name=filter_id_propinsi]').val() == ''){
		params='';
	}
	// "%'+$('[name=filter]').val()+'%"
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=jabatan";
	qry="limit=3";
	ajaxGET(path + '/unit_kompetensi?page='+page+'&'+params+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row+=renderRow(value, num);
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		query="";
		ajaxGET(path + '/unit_kompetensi/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=jabatan';
		query="";
		ajaxPOST(path + '/unit_kompetensi/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	//display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=kode]').val(value.kode);
	$('[name=nama]').val(value.nama);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';
	row += '<td class="">'+value.kode+'</td>';
	row += '<td class="">'+value.nama+'</td>';

	row += '<td><button onclick="doAction(\''+value.id+'\',\'edit\')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
	row += '<button onclick="doAction(\''+value.id+'\',\'delete\')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}