var path = ctx + '/restql';

var param = "";
var selected_id='';
var selected_action='';
var selected_mode=$.urlParam('mode')==null?"":$.urlParam('mode');

var appendIds = [];


const Toast = Swal.mixin({
toast: true,
  position: 'top-right',
  iconColor: 'white',
  customClass: {
    popup: 'colored-toast'
  },
  showConfirmButton: false,
  timer: 1500,
  timerProgressBar: true
})

function init() {
	getSelectKelompokJabatan();
	//getSelectJenisMetode();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
		$('[name=id_kelompok_jabatan]').val($('#nav-tab .active').data('id'));
		$('[name=id_bidang_teknis]').val($('[name=filter_id_bidang_teknis]').val());
		if($('[name=filter_id_master_jabatan]').val()!=""){
			$('[name=id_master_jabatan]').val($('[name=filter_id_master_jabatan]').val());
			$("#modal-form").modal('show');
		}else{
			alert("Jenjang Jabatan belum dipilih.");
		}
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	$('[name=filter_id_master_jabatan]').on('change', function(){
		//display(1,'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+this.value+'")');
		display();
		//$('#btn-add').attr('disabled',false);
	});
	
}

function setValBidang(value){
	$('[name=filter_id_bidang_teknis]').val(value);
	$('[name=id_bidang_teknis]').val(value);
	//display();
}

function getSelectJenisMetode(data){
	$('[name=id_metode_pengembangan_jenis]').empty();
	$('[name=id_metode_pengembangan_jenis]').append(new Option('-- Pilih --', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/metode_pengembangan_jenis?page='+page+'&'+params+'&'+qry,'onGetSelectJenisMetode','onGetSelectError', data);
}

function onGetSelectJenisMetode(response,data){

	$.each(response.data, function(key1, value) {
		$('[name=id_metode_pengembangan_jenis]').append(new Option(value.type+":"+value.nama, value.id));
	});
	
	$.each($('[name=id_metode_pengembangan_jenis] option'), function(key, element){
	   console.log(this) 
		$.each(data, function(key2, value) {
			if(element.value==value.id_metode_pengembangan_jenis) {
				$(element).attr('disabled', true);
			}
		});
	});
}
function getSelectMasterJabatan(id){
	$('[name=filter_id_master_jabatan]').empty();
	$('[name=filter_id_master_jabatan]').append(new Option('Pilih Jenjang Jabatan', ''));
	$('[name=id_master_jabatan]').empty();
	$('[name=id_master_jabatan]').append(new Option('-- Pilih --', ''));
	var page=1,params="filter=id_kelompok.eq('"+id+"')",qry="";
	ajaxGET(path + '/master_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectMasterJabatan','onGetSelectError');
}
function onGetSelectMasterJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_master_jabatan]').append(new Option(value.nama, value.id));
		$('[name=id_master_jabatan]').append(new Option(value.nama, value.id));
	});
}

function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	//$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		var params='onclick="getSelectBidangTeknis('+value.id+');getSelectMasterJabatan('+value.id+')"';
		$('#nav-tab').append('<a data-id="'+value.id+'" '+params+' class="nav-item nav-link '+(key==0?"active":"")+'" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">'+value.kode+'</a>');
		
	});
	getSelectBidangTeknis($('#nav-tab .active').data('id'));
	
	getSelectMasterJabatan($('#nav-tab .active').data('id'));
}

function getSelectBidangTeknis(id){
	$('[name=id_bidang_teknis]').empty();
	$('[name=id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	$('[name=filter_id_bidang_teknis]').empty();
	//
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="";
	ajaxGET(path + '/bidang_teknis?'+params,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_bidang_teknis]').append(new Option(value.nama, value.id));
		$('[name=filter_id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	display();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/metode_pengembangan_kelompok_nilai/save?append=false&join=kelompok_jabatan,bidang_teknis,metode_pengembangan_jenis';
	
	var disabled =  $('#entry-form').find(':input:disabled').removeAttr('disabled');
	

	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray()),
			dataMain : {
				"column" : [
					$('[name=id_metode_pengembangan_jenis]').val(), $('[name=operator]').val(),$('[name=nilai]').val(), 
					$('[name=id_bidang_teknis]').val(), $('[name=id_kelompok_jabatan]').val(), $('[name=id_master_jabatan]').val()
				]
			}
		};
	if($('[name=id]').val() == "") delete obj.data.id;
	if(selected_mode=="NON_TEKNIS") {
		delete obj.dataMain.column[2];
		delete obj.data.nilai;
	}
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		$.LoadingOverlay("hide");
        		//$('#modal-form').modal('hide');
        		//display();
				disabled.attr('disabled','disabled');
        		onModalActionSuccess(response);
        },
        error : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        		$('#modal-form-msg').show();
				disabled.attr('disabled','disabled');
        },
    });
}

function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();
}


function reset(){
	cleanMessage('msg');
	selected_id = '';
	$('#entry-form').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

function doRemove(id){
	$("#progressBar1").show();
	if(selected_action == 'delete' ){
		doAction(selected_id, selected_action, 1);
	}
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modal-form").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=nama]').val(value.nama_aspek);
	$('[name=sumber]').val(value.sumber_aspek);

//	$('#error_msg').hide();
//	$('#modal-form').modal('show');
//	reset();
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	tbody.text('');
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="&join=kelompok_jabatan,bidang_teknis,metode_pengembangan_jenis";
	if(params=="") qry+="&filter=id_kelompok_jabatan.eq("+$('#nav-tab .active').data('id')+"),id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	else qry+="&filter="+params+",id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	
	if($('[name=filter_id_master_jabatan]').val()!="") qry+=",id_master_jabatan.eq("+$('[name=filter_id_master_jabatan]').val()+")";
	else qry+=",id_master_jabatan.eq("+0+")";
	
	
	if(selected_mode=="NON_TEKNIS") qry+=",other.eq('"+selected_mode+"')";
	else qry+=",other.is(null)";
	ajaxGET(path + '/metode_pengembangan_kelompok_nilai?page='+page+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += renderRow(value, num);
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
	//render reference data
	getSelectJenisMetode(response.data);
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		query="";
		ajaxGET(path + '/metode_pengembangan_kelompok_nilai/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=jabatan';
		query="";
		ajaxPOST(path + '/metode_pengembangan_kelompok_nilai/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	//display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	$('[name=filter_id_master_jabatan]').trigger('change');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';

	//showAlertMessage(response.message, 1500);
	toastAlertMessage(response.message);
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=id_kelompok_jabatan]').val(value.id_kelompok_jabatan);
	$('[name=id_bidang_teknis]').val(value.id_bidang_teknis);
	//new
	/*$.each($('[name=id_metode_pengembangan_jenis] option'), function(key, element){
		if($('[name=filter_id_master_jabatan]').val()==value.id_metode_pengembangan_jenis){
			$('[name=id_metode_pengembangan_jenis]').append(new Option(value.type+":"+value.nama, value.id));
		}
	});*/
	$('[name=id_metode_pengembangan_jenis]').val(value.id_metode_pengembangan_jenis);
	$('[name=operator]').val(value.operator);
	$('[name=nilai]').val(value.nilai);
	$('[name=id_master_jabatan]').val(value.id_master_jabatan);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';
	row += '<td class="">'+value.metode_pengembangan_jenis.type+'</td>';
	row += '<td class="">'+value.metode_pengembangan_jenis.nama+'</td>';
	if(value.other=="NON_TEKNIS"){
		row += '<td class="">'+value.operator+'</td>';
	}else{
		row += '<td class="">'+value.operator+""+value.nilai+'</td>';
	}

	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}