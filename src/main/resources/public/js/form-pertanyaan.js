var path = ctx + '/restql';
var url = ctx + "/pertanyaan";

var param = "";
var selected_id="";
var selected_id_unit_kompetensi=$.urlParam('id')==null?"":$.urlParam('id');
var selected_jenis=$.urlParam('jenis')==null?"":$.urlParam('jenis');
var selected_bidang_teknis=$.urlParam('bidang_teknis')==null?"":$.urlParam('bidang_teknis');
var selected_action='';

var appendIds = [];
function init() {
	
	if(selected_jenis=="teknis"){
		getSelectKelompokJabatan();
	}else{
		display();
		getSelectUnitKompetensi();
	}
	$('#btn-add').click(function(e){
		$('[name=jumlah_jawaban]').attr("min", "");
		selected_id = '';
		selected_action = 'add';
		clearForm();
		$('[name=id_kelompok_jabatan]').val($('#nav-tab .active').data('id'));
		getDefaultDurasi();
		$('[name=id_bidang_teknis]').val($('[name=filter_id_bidang_teknis]').val());
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#search-form').submit(function(e){
		if(selected_id_unit_kompetensi=="") display(1, $('#search-form').serialize());
		else window.location.href="?id="+$('[name=filter_id_unit_kompetensi]').val();
		e.preventDefault();
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});

	$(".button-minus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepDown();
	});
	$(".button-plus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepUp();
	});
	// Listen for click on toggle checkbox
	$('[name=cek_all_pertanyaan]').click(function(event) { 
		var arr=[];
	    if(this.checked) {
	        // Iterate each checkbox
	        $(':checkbox').each(function() {
	            this.checked = true;   
	        });
	    } else {
	        $(':checkbox').each(function() {
	            this.checked = false;                       
	        });
	    }
	    $("input:checkbox[name=cek_id_pertanyaan]:checked").each(function(){
            arr.push(this.value);
	    });
        $('[name=id]').val(arr);
	});

	$('#entry-form-update').submit(function(e){
		var obj = new FormData(document.querySelector('#entry-form-update'));
	    ajaxPOST(ctx + '/pertanyaan/update-durasi',obj,'onModalActionSuccess','onSaveError');
		e.preventDefault();
	});
	$('#entry-form-delete').submit(function(e){
		var obj = new FormData(document.querySelector('#entry-form-delete'));

		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	    ajaxPOST(ctx + '/pertanyaan/delete-soal',obj,'onModalActionSuccess','onSaveError');
		e.preventDefault();
	});
	$('#modal-form-update').on('shown.bs.modal', function () {
	  selected_action="update-durasi";
	  $('[name=durasi]').val("");
	  if( $("input:checkbox[name=cek_id_pertanyaan]:checked").length==0) {
		  alert("Pertanyaan/Soal Belum dipilih.");
		  $('#modal-form-update').modal('hide');
	  }
	  $('[name=id]').attr('disabled',false);
	})
	
	$('#modal-form-delete').on('shown.bs.modal', function () {
	  selected_action="delete-soal";
	  if( $("input:checkbox[name=cek_id_pertanyaan]:checked").length==0) {
		  alert("Pertanyaan/Soal Belum dipilih.");
		  $('#modal-form-delete').modal('hide');
	  }
	  $('[name=id]').attr('disabled',false);
	})
	
	$('[name=filter_id_bidang_teknis]').on('change', function(){
		display();
		$('[name=id_bidang_teknis]').val(this.value);
	});
	$('[name=id_bidang_teknis]').on('change', function(){
		$('[name=filter_id_bidang_teknis]').val(this.value).trigger('change');
	});
	$(function () {
	  $('[data-toggle="popover"]').popover()
	})
}
function setValueCek(){
	$('[name=cek_id_pertanyaan]').click(function(event) {
		var arr=[]; 
	    $("input:checkbox[name=cek_id_pertanyaan]:checked").each(function(){
            arr.push(this.value);
	    });
        $('[name=id]').val(arr);
	});
	//init multi check
	var $chkboxes = $('[name=cek_id_pertanyaan]');
    var lastChecked = null;

    $chkboxes.click(function(e) {
        if (!lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
        }

        lastChecked = this;
    });
}
function getDefaultDurasi(){
	$('[name=id_kelompok_jabatan]').empty();
	var page=1,params="filter=code.eq('DURASI_SOAL')",qry="";
	ajaxGET(path + '/config?page='+1+'&'+params+'&'+qry,'onGetDefaultDurasi','onGetSelectError');
}
function onGetDefaultDurasi(response){
	var durasi=response.data[0].value;
	$('[name=durasi]').val(durasi);
}
function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	//$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		var params='onclick="getSelectBidangTeknis('+value.id+');"';
		$('#nav-tab').append('<a data-id="'+value.id+'" '+params+' class="nav-item nav-link '+(key==0?"active":"")+'" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">'+value.kode+'</a>');
		
	});
	getSelectBidangTeknis($('#nav-tab .active').data('id'));
	getSelectUnitKompetensi();
}

function getSelectBidangTeknis(id){
	$('[name=id_bidang_teknis]').empty();
	$('[name=id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	$('[name=filter_id_bidang_teknis]').empty();
	//
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="";
	ajaxGET(path + '/bidang_teknis?'+params,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_bidang_teknis]').append(new Option(value.nama, value.id));
		$('[name=filter_id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	if(selected_bidang_teknis!="") $('[name=filter_id_bidang_teknis]').val(selected_bidang_teknis);
	display();
	getSelectUnitKompetensi();
}

var id_main="";
var id_filter="";
function getSelectUnitKompetensi(){
	var url_path="";
	id_filter="filter_id_unit_kompetensi";
	id_main="id_unit_kompetensi";
	if(selected_jenis!=""){
		if(selected_jenis=="teknis") {
			url_path="unit_kompetensi_teknis";
		}
		
		if(selected_jenis=="manajerial") {
			url_path="unit_kompetensi_manajerial";
		}
		if(selected_jenis=="sosiokultural") {
			url_path="unit_kompetensi_sosiokultural";
		}
	}
	$('[name='+id_filter+']').empty();
	$('[name='+id_filter+']').append(new Option('Pilih/Ketik Untuk Mencari Unit Kompetensi', ''));

	$('[name='+id_main+']').empty();
	$('[name='+id_main+']').append(new Option('Pilih/Ketik Untuk Mencari Unit Kompetensi', ''));
	
	var page=1,params="",qry="";
	if(selected_jenis=="teknis"){
		params="filter=id_kelompok_jabatan.eq('"+$('#nav-tab .active').data('id')+"')";
	}
	ajaxGET(path + '/'+url_path+'?'+params+"&limit=10000000",'onGetSelectUnitKompetensi','onGetSelectError');
}
function onGetSelectUnitKompetensi(response){
	$.each(response.data, function(key, value) {
		$('[name='+id_filter+']').append(new Option(value.kode+" - "+value.judul, value.id));
		$('[name='+id_main+']').append(new Option(value.kode+" - "+value.judul, value.id));
	});
	if(selected_id_unit_kompetensi!=""){
		$('[name='+id_filter+']').val(selected_id_unit_kompetensi);
		$('[name='+id_main+']').val(selected_id_unit_kompetensi);
		$('[name='+id_main+']').trigger('change');
	}
}
function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function save(){
	var obj = new FormData(document.querySelector('#entry-form'));
	obj.append('jenis', selected_jenis);
    ajaxPOST(ctx + '/pertanyaan/save',obj,'onModalActionSuccess','onSaveError');
}
/*function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	//display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}else if(selected_action == 'delete-soal'){
//		var params='id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'")';
		alert("try call");
		getSelectBidangTeknis($('#nav-tab .active').data('id'));
	}
	
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	//selected_id='';
	$('[name=id]').attr('disabled',true);

	showAlertMessage(response.message, 1500);
}*/
function onSaveError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').removeClass('d-none');
	$('#modal-form-msg').show();
}

function clearForm(){
	$("#file-name").text("tidak ada lampiran.");
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();
	$('#form-pilihan-jawaban').html('');
	$('[name=id]').attr('disabled',true);

	$('[name=id_unit_kompetensi]').val(selected_id_unit_kompetensi);
	$('[name=id_unit_kompetensi]').trigger('change');
}


function reset(){
	cleanMessage('msg');
	//selected_id = '';
	$('[name=id]').attr('disabled',true);
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#entry-form').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

function doRemove(id){
	$("#progressBar1").show();
	if(selected_action == 'delete' ){
		doAction(selected_id, selected_action, 1);
	}
}
//batas

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params != 'page' || page ==1){ //params == '' || params != $( "#search-form" ).serialize() || page ==1
		tbody.text('');
	}
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	if(selected_id_unit_kompetensi!="") params='filter_id_unit_kompetensi='+selected_id_unit_kompetensi;
	else {
		if(params!="") params='filter_id_unit_kompetensi='+$('[name=filter_id_unit_kompetensi]').val();
	}
	if(selected_jenis!="") params+='&filter_jenis='+selected_jenis;
	if(selected_jenis=="teknis") params+='&filter_id_bidang_teknis='+$('[name=filter_id_bidang_teknis]').val();
	
	ajaxGET(ctx + '/pertanyaan/list?page='+page+'&'+params+"",'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += renderRow(value, num);
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display(\''+response.next_page_number+'\',\'page\')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	setValueCek();
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select;//+"&fetch=pertanyaan_jawaban";//.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi) //akun_bank.limit(1)
//		ajaxPOST(url+'/'+id+'/'+selected_action,{},'onPrepareModalActionSuccess','onActionError');
		//var query=select+"&fetch=pertanyaan_jawaban";//.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi) //akun_bank.limit(1)
		ajaxGET(url + '/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=jabatan';
		query="";
		ajaxPOST(path + '/pertanyaan/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	//display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}else if(selected_action == 'delete-soal'){
//		var params='id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'")';
		getSelectBidangTeknis($('#nav-tab .active').data('id'));
	}
	
	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	$('#modal-form-update').modal('hide');
	$('#modal-form-delete').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function fillFormValue(value){
	console.log(value); 
	clearForm();
	$('[name=id]').attr('disabled',false);
	$('[name=id]').val(value.id);
	$('[name=id_unit_kompetensi]').val(value.id_unit_kompetensi).trigger('change');
	$('[name=id_bidang_teknis]').val(value.id_bidang_teknis);
	$('[name=kode]').val(value.kode);
	$('[name=bobot]').val(value.bobot);
	$('[name=isi]').val(value.isi);
	$('[name=kunci_jawaban]').val(value.kunci_jawaban);
	$('[name=kunci_jawaban_work]').val(value.kunci_jawaban);
	$('[name=tipe_soal]').val(value.tipe_soal);
	$('[name=durasi]').val(value.durasi);
	$("#file-name").text(value.file);
	var urlfile=ctx+"/"+"files/"+value.id+"?filename="+value.file+"";//&preview=
	$("#file-name").attr("href", urlfile);
	//

	$('[name=jumlah_jawaban]').val(value.listPertanyaanJawaban.length);
	$('[name=jumlah_jawaban]').attr("min", value.listPertanyaanJawaban.length);
	
	var i=1;
	var img="";
	$.each(value.listPertanyaanJawaban, function(key){
		img+='<img src='+ctx+"/"+"files/"+this.id+"?filename="+this.file+""+' class="img-responsive>';
		var attr='';
		var btn_view="";
		if(this.file!=null){
			attr='data-toggle="popover" data-content="<img class=\''+"img-fluid"+'\' src='+ctx+"/"+"files/"+this.id+"?filename="+this.file+">";
			btn_view='<button '+attr+'" class="btn btn-light border" type="button"><i class="fa fa-eye"></i></button>';
		}
		temp = '<div>';
		temp = '<div class="input-group mt-3">'+
		//
		//<a tabindex="0" class="btn btn-lg btn-danger" role="button" 
		//data-toggle="popover" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?"
		//>Dismissible popover</a>
		//title="Dismissible popover" data-content="And heres some amazing content. Its very engaging. Right?" 
		'    <div class="input-group-prepend img-responsive">'+
		'        <span class="input-group-text">'+i+'</span>'+
		'    </div>'+
		'    <div class="input-group-prepend">';
		//
		if(selected_jenis=="teknis"){
			temp += '<div class="input-group-text">';
				if(value.kunci_jawaban==this.order){// && this.bobot==1
					temp += '<input checked name="kunci_jawaban" type="radio" placeholder="Tentukan Nilai Bobot" value="'+this.bobot+'">';
				}else{
					temp += '<input name="kunci_jawaban" onclick="setKunciJawaban('+i+')" type="radio" placeholder="Tentukan Nilai Bobot" value="'+this.bobot+'">';
				}
			temp += '</div>';
		}else{
			temp += '<input style="width: 80px;"  class="form-control" name="bobot_jawaban" type="number" placeholder="Bobot" value="'+this.bobot+'">';
		}
		temp += '</div>'+
		'    <input type="text" class="form-control d-none" name="id_jawaban" value="'+this.id+'">'+
		'    <input type="text" class="form-control" name="isi_jawaban" placeholder="Pilihan Jawaban ke '+(i)+'" autocomplete="off" value="'+this.isi+'">'+
		'    <div class="input-group-prepend">'+
		btn_view+
		'	 <input type="hidden" name="filename_jawaban" value="'+this.file+'">'+
		'    </div>'+
		'</div>'+
		'	 <input type="file" accept="image/jpeg,image/gif,image/png" onchange="uploadFileMove(this)" name="file_jawaban">'+
		'</div>';
		//
		$('#form-pilihan-jawaban').append(temp);
		i++;
	});
	$('#btn-create-form').data('obj', value.pertanyaan_jawaban);
	
	/*
	var temp="";
	for(var i=1; i <= $('[name=jumlah_jawaban]').val(); i++){
		temp += '<div class="input-group mb-3">'+
		'    <div class="input-group-prepend">'+
		'        <span class="input-group-text">'+i+'</span>'+
		'    </div>'+
		'    <div class="input-group-prepend">';
		if(selected_jenis=="teknis"){
			temp += '<div class="input-group-text">';
				temp += '<input name="kunci_jawaban" type="radio" placeholder="Tentukan Nilai Bobot" value="'+i+'">';
			temp += '</div>';
		}else{
			temp += '<input class="form-control" name="bobot_jawaban" type="number" placeholder="Tentukan Nilai Bobot">';
		}
		temp += '</div>'+
		'    <input type="text" class="form-control d-none" name="id_jawaban">'+
		'    <input type="text" class="form-control" name="isi_jawaban" placeholder="Pilihan Jawaban ke '+(i)+'" autocomplete="off">'+
		''+
		'</div>';
	}
	$('#form-pilihan-jawaban').html(temp);
	if($('#btn-create-form').data('obj').length>0){
		$.each($('#btn-create-form').data('obj'), function(key){
			$('[name=bobot_jawaban]')[key].value=this.bobot;
			$('[name=id_jawaban]')[key].value=this.id;
			$('[name=isi_jawaban]')[key].value=this.isi;
		});
	}
	*/

	$(function () {
	  $('[data-toggle="popover"]').popover({
		  placement:'left',
		  title : 'Preview',
		  template : '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
		  content : 'cmon',
		  html : true
	  })
	})
}

function setKunciJawaban(id){
	$("[name=kunci_jawaban_work]").val(id);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	//var num = $('#row-'+value.id+' td:second-child').text();
	var num = $('#row-'+value.id+' td')[1].textContent;
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td><input type="checkbox" name="cek_id_pertanyaan" value="'+value.id+'"></td>';
	row += '<td>'+(num)+'</td>';
	row += '<td class="">'+value.kode+'</td>';
	//row += '<td class="">'+value.isiAndFilename+'</td>';
	var newisi = $.trim(value.isiAndFilename).replace(/\n/g, '<br/>');
	row += '<td class="">'+newisi+'</td>';
	row += '<td class="">'+value.bobot+'</td>';
	if(selected_jenis=="teknis"){
	row += '<td class="">'+value.kunci_jawaban+'</td>';
	row += '<td class="">'+value.tipe_soal+'</td>';
	}
	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function uploadFile(e){
	console.log(e.parentElement.children[1]);
	$(e.parentElement.children[1]).trigger('click');
}

function uploadFileMove(e){
	console.log($(e)[0].files[0].name);
	var nama=$(e)[0].files[0].name;
	$(e.parentElement.children[2]).val(nama);
}
function createFormJawaban(){
	var temp="";
	for(var i=1; i <= $('[name=jumlah_jawaban]').val(); i++){
		/*temp += '<div class="input-group" style="margin-bottom:10px">'+
		'						  <input class="form-control hidden" name="order_jawaban" type="text" value="'+i+'">'+
		'						  <span class="input-group-addon" id="basic-addon1">'+i+'</span>'+
		'						  <input class="form-control hidden" name="id_jawaban" type="text" value="">'+
		'						  <span class="input-group-addon hidden" id="basic-addon1">-</span>'+
		'						  <input name="isi_jawaban" type="text" class="form-control" placeholder="Pilihan jawaban ke '+(i)+'" autocomplete="off" value="">'+
		'						</div>';*/
		temp += '<div>';
		temp += '<div class="input-group mt-3">';

		if(selected_jenis!="teknis"){
			temp += ' <div class="input-group-prepend">'+
		'        <span class="input-group-text">'+i+'</span>'+
		'    </div>';
		}
		temp +='<div class="input-group-prepend">';
		if(selected_jenis=="teknis"){
			temp += '<div class="input-group-text"> '+i+'. ';
				temp += '<input name="kunci_jawaban" type="radio" placeholder="Tentukan Nilai Bobot" value="'+i+'">';
			temp += '</div>';
		}else{
			temp += '<input style="width: 80px;" class="form-control" name="bobot_jawaban" type="number" placeholder="Bobot">';
		}
		temp += '</div>'+
		'    <input type="text" class="form-control d-none" name="id_jawaban">'+
		'    <input type="text" class="form-control" name="isi_jawaban" placeholder="Pilihan Jawaban ke '+(i)+'" autocomplete="off">'+
//		'    <div class="input-group-prepend">'+
//		'    <button onclick="uploadFile(this)" class="btn btn-light border" type="button"><i class="fa fa-eye"></i></button>'+
//		'    </div>'+
		'</div>'+
		'	 <input type="file" accept="image/jpeg,image/gif,image/png" name="file_jawaban">'+
		'</div>';
	}
	$('#form-pilihan-jawaban').html(temp);
	
	if($('#btn-create-form').data('obj').length>0){
		$.each($('#btn-create-form').data('obj'), function(key){
			$('[name=bobot_jawaban]')[key].value=this.bobot;
			$('[name=id_jawaban]')[key].value=this.id;
			$('[name=isi_jawaban]')[key].value=this.isi;
		});
	}
	
}