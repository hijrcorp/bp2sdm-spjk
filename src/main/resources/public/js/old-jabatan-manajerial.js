var path = ctx + '/restql';
var path_view = ctx + '/restql/view';

var param = "";
var selected_id='';
var selected_action='';

var appendIds = [];
function init() {
	getSelectJabatan();
	getSelectKelompokManajerial();
	display();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
}

function getSelectKelompokManajerial(){
	$('[name=id_kelompok_manajerial]').empty();
	$('[name=id_kelompok_manajerial]').append(new Option('Pilih Kelompok Manajerial', ''));
	ajaxGET(path + '/kelompok_manajerial','onGetSelectKelompokManajerial','onGetSelectError');
}
function onGetSelectKelompokManajerial(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_manajerial]').append(new Option(value.nama, value.id));
	});
}

function getSelectJabatan(){
	$('[name=id_view_jabatan]').empty();
	$('[name=id_view_jabatan]').append(new Option('Pilih Jabatan', ''));
	ajaxGET(path+ '/view_jabatan','onGetSelectJabatan','onGetSelectError');
}
function onGetSelectJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_view_jabatan]').append(new Option(value.nama, value.id));
	});
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function save(){
	//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/jabatan_manajerial/save?append=false&join=kelompok_manajerial,view_jabatan';
	
	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray())
		};
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		//$.LoadingOverlay("hide");
        		//$('#modal-form').modal('hide');
        		//display();
        	onModalActionSuccess(response);
        },
        error : function (response) {
        		//$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        },
    });
}

function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();
}


function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#entry-form').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

function doRemove(id){
	$("#progressBar1").show();
	if(selected_action == 'delete' ){
		doAction(selected_id, selected_action, 1);
	}
}
//batas
function edit(id, row){
	reset();
	$("#progressBar").show();
//	$("#modal-form").modal('show');
	selected_id = id;
	$('#form-label').text('Edit');
	ajaxGET(url + id,'onEditSuccess','onEditError');
}

function onEditSuccess(response){
	console.log(response);
	var value = response.data;
	$('[name=nama]').val(value.nama_aspek);
	$('[name=sumber]').val(value.sumber_aspek);

//	$('#error_msg').hide();
//	$('#modal-form').modal('show');
//	reset();
//	$("#progressBar").show();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	if($('#txtsearch').val() != '' && $('#column').data('id') != '' || page ==1){
		tbody.text('');
	}
	
	if($('#column').data('id') != ''){
		//params+= $('#column').data('id')+'.slike \''+$('#txtsearch').val()+'\';';
	}
	
//	if($('[name=filter_nama]').val() == '' && $('[name=filter_nomor]').val() == '' 
//		&& $('[name=filter_id_jabatan]').val() == '' && $('[name=filter_alamat_rumah]').val() == '' 
//			&& $('[name=filter_id_propinsi]').val() == ''){
//		params='';
//	}
	// "%'+$('[name=filter]').val()+'%"
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=kelompok_manajerial,view_jabatan";
	ajaxGET(path + '/jabatan_manajerial?page='+page+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += renderRow(value, num);
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&join=kelompok_manajerial,view_jabatan";
		ajaxGET(path + '/jabatan_manajerial/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=jabatan';
		query="";
		ajaxPOST(path + '/jabatan_manajerial/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	//display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=id_view_jabatan]').val(value.id_view_jabatan);
	$('[name=id_kelompok_manajerial]').val(value.id_kelompok_manajerial);
	$('[name=nilai_minimum]').val(value.nilai_minimum);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';
	row += '<td class="">'+value.view_jabatan.nama+'</td>';
	row += '<td class="">'+value.kelompok_manajerial.nama+'</td>';
	row += '<td class="">'+value.nilai_minimum+'</td>';

	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}