var path = ctx + '/restql';
var filter_id="";
var selected_action="";
function init(){
	filter_id=($.urlParam('filter_id') == null ? '' : $.urlParam('filter_id'));
	if(filter_id!=""){
		display(1, $('#search-form').serialize()+"&filter_id="+filter_id);
	}
	onGetListDetilSuccess();
	getSelectKelompokJabatan();
	getListTahun();
	//renderChart();
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-reset').click(function(e){
		clearForm();
		$("#search-form").trigger('reset');
	});
	
	$("#entry-form").submit(function() {
		save();
		return false;
	});
	

	$('#search-form').submit(function(e){
		display(1, $('#search-form').serialize());
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('.single-date-picker').on('change', function(){
         var dateFirst = new Date($('[name=tanggal_mulai]').val());//"11/25/2017"
         var dateSecond = new Date($('[name=tanggal_selesai]').val());//"11/28/2017"
         // time difference
         var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());
         // days difference
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
         $('#jumlah_cuti').text(diffDays+" Hari");
         $('[name=jumlah_cuti]').val(diffDays+" Hari"); 
	});
	
	$('#btn-reset-pertanyaan').click(function(e){
		var obj = new FormData(document.querySelector('#entry-form'));
		obj.append("id_account", $('#list-user').find('.active.show').data('idAccount'));
		obj.append("id_sesi", $('#list-user').find('.active.show').data('idSesi'));
	    ajaxPOST(ctx + '/sesi/reset-hasil-ujian',obj,'onResetActionSuccess','onSaveError');
	});
	
	$('#btn-delete-pertanyaan').click(function(e){
		var id = $('#list-user').find('.active.show').data('idResponden');
		doAction(id,'delete');
		//selected_action="deleted";
	    //ajaxPOST(ctx + '/sesi/user-responden/'+$('#list-user').find('.active.show').data('idResponden')+"/delete",{},'onResetActionSuccess','onSaveError');
	});
}

function renderChart(){
//	var myBarChart = new Chart(ctx, {
//	    type: 'horizontalBar',
//	    data: data,
//	    options: options
//	});
	var ctx = document.getElementById('myChart0');
	var myChart = new Chart(ctx, {
	    type: 'horizontalBar',
	    data: {
	        labels: ['A', 'B', 'C', 'D'],
	        datasets: [{
	            label: 'Jumlah Jawaban',
	            data: [12, 19, 3, 5, 2, 3],
	            backgroundColor: [
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(75, 192, 192, 0.2)'
	            ],
	            borderColor: [
	                'rgba(54, 162, 235, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(75, 192, 192, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        }
	    }
	});
}

function getSelectKelompokJabatan(){
	$('[name=filter_kelompok_jabatan_id]').empty();
	$('[name=filter_kelompok_jabatan_id]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_kelompok_jabatan_id]').append(new Option(value.kode, value.id));
	});
	$('[name=filter_kelompok_jabatan_id]').on('change', function(){
		getSelectBidangTeknis(this.value);
	});
}

function getSelectBidangTeknis(id){
	$('[name=filter_bidang_teknis_id]').empty();
	$('[name=filter_bidang_teknis_id]').append(new Option('Pilih Bidang Teknis', ''));
	//
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="";
	ajaxGET(path + '/bidang_teknis?'+params,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_bidang_teknis_id]').append(new Option(value.nama, value.id));
	});
}

function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=filter_tahun]").append(new Option(i, i));
	}
}

function display(page=1, params='', mode='filter'){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == ''){ // || params != $( "#search-form" ).serialize() || page ==1
		//tbody.text('');
	}
	
	//tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//console.log(path + '/list?page='+page+'&'+params);
	if(mode=="filter" && filter_id==""){
		mode="onGetListParamSuccess";
	}else if(mode=="data" || filter_id!=""){
		mode="onGetListDetilSuccess";
	}
	var qry="join=pegawai,akun_bank";
	qry="";
	ajaxGET(ctx + '/sesi/list-hasil-ujian?page='+page+'&'+params+'&'+qry,mode,'onGetListDetilError');
}

function onGetListParamSuccess(response){
	$('#modal-form').modal('show');
	var list="";
	$.each(response.data,function(key,value){
		var func="display(1, '&filter_id="+value.id+"', 'data');";
		list+= '<a href="#" onclick="'+func+'" class="list-group-item list-group-item-action">'+value.kode+' <span class="badge badge-primary badge-pill float-right">'+value.account.length+'</span></a>';
	});
	$("#list-modal").html(list);
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#modal-form').modal('hide');
	var alpha = ["a","b","c","d"];
	var list="";
	var row = "";
	for(var k=0; k <=5; k++){
	//$.each(response.data[0].account,function(key,value){
		list += '<a data-toggle="list" data-id-responden="'+"value.userResponden.id"+'" data-id-account="'+"value.id"+'" data-id-sesi="'+"response.data[0].id"+'"  href="#list-pertanyaan-'+k+'" class="list-group-item list-group-item-action">'+"value.username"+'<span class="float-right">'+"value.userResponden.status"+'</span></a>';
		// show active
		row +=' <div class="tab-pane fade" id="list-pertanyaan-'+k+'">';

		//for(var l=0; l <=5; l++){
			row += '<ul class="nav nav-tabs" id="myTab" role="tablist">'+
			'  <li class="nav-item" role="presentation">'+
			'    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Inti</a>'+
			'  </li>'+
			'  <li class="nav-item" role="presentation">'+
			'    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Pilihan</a>'+
			'  </li>'+
			'  <li class="nav-item" role="presentation">'+
			'    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Manajerial</a>'+
			'  </li>'+
			'  <li class="nav-item" role="presentation">'+
			'    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Sosiokultural</a>'+
			'  </li>'+
			'</ul>'+
			'<div class="tab-content d-none" id="myTabContent">'+
			'  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">...</div>'+
			'  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>'+
			'  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>'+
			'</div>';
		//}
		
		for(var i=0; i <= 3; i++){
		//$.each(value.userPertanyaan,function(key, valuePertanyaan){
			row += '<div class="row">'+
			'    <div class="col-md-12">'+
			'        <div class="data-row card mb-3" id="row-'+"key"+'">'+
			'            <div class="card-header">'+
			'                <strong>'+(i+1)+'</strong>.'+"this.isi"+' <button onclick="showRemarks(\'null\')" type="button" class="btn m-0 p-0"><i class="fas fa-question-circle text-info"></i></button>'+
			'                <strong class="d-none">(undefined)</strong>'+
			'            </div>'+
			'            <ul class="list-group list-group-flush">';
			for(var j=0; j <=3; j++){
			//$.each(this.userPertanyaanJawaban,function(key){
			row +='          <li class="list-group-item list-group-item-action">'+
			'                    <div class="">';
			
			/*if(this.is_checked==1 && valuePertanyaan.kunci_jawaban==this.order)
				row +='<span class="fas fa-check-circle text-success"></span>';
			else if(this.is_checked==1 && value.kunci_jawaban!=this.order)
				row +='<span class="fas fa-times-circle text-danger"></span>';
			else*/
				row +='<span class="far fa-circle"></span>';
			row +='                      <label class="form-check-label">'+
			'                            <strong>'+alpha[j]+'</strong>. '+"this.isi"+
			'                        </label>'+
			'                    </div>'+
			'                </li>';
			//});
			}
			row +='     </ul>'+
			'        </div>'+
			'    </div>'+
			'</div>';
		//});
		}
		row +='</div>';
	//});
	}
	list == "" ? $("#list-user").html('<div align="center">Data Tidak Ada.</div>') : $("#list-user").html(list);
	row == "" ? $("#list-pertanyaan").html('<div align="center">Data Tidak Ada.</div>') : $("#list-pertanyaan").html(row);

}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&fetch=penggajian_detil.select(id,id_penggajian,jenis,komponen,jumlah,keterangan)";//akun_bank.limit(1)
		query=""
		ajaxGET(path + '/user_responden/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		var cascade="";
		//if(selected_action=="delete")  cascade="&cascade=penggajian_detil";
		var query="";
		ajaxPOST(ctx + '/sesi/user-responden/'+id+'/'+selected_action+'?'+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('.modal table').find('tbody').html('');
	$('[name=filter_id_pegawai]').val('').trigger('change');
	$('[name=filter_id_akun_bank]').val('').trigger('change');
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=kode]').val(value.kode);
	$('[name=tahun]').val(value.tahun);
	$('[name=periode_awal]').val(value.periode_awal);
	$('[name=periode_akhir]').val(value.periode_akhir);
	$('[name=keterangan]').val(value.keterangan);
	$('[name=kelompok_jabatan_id]').val(value.kelompok_jabatan_id);
	getSelectBidangTeknis(value.kelompok_jabatan_id);
	setTimeout(() =>
	$('[name=bidang_teknis_id]').val(value.bidang_teknis_id), 3000
	);
	
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	if(selected_action=="") display();
	
	// Update data ke existing tabel
	/*if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}*/
	if(selected_action=="delete"){
		$('#list-pertanyaan').find('.active.show').remove();
	}
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}
function onResetActionSuccess(response){
	console.log("SAVING");
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	
	showAlertMessage(response.message, 1500);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


