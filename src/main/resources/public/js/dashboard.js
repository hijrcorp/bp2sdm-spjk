var path = ctx + '/dashboard';

var mode_debug=$.urlParam('debug');

function init(){
	renderDashboard();
}
//
function display(page=1, params=''){
	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params != 'page' || page ==1){ //params == '' || params != $( "#search-form" ).serialize() || page ==1
		tbody.text('');
	}
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	if(selected_id_unit_kompetensi!="") params='filter_id_unit_kompetensi='+selected_id_unit_kompetensi;
	else {
		if(params!="") params='filter_id_unit_kompetensi='+$('[name=filter_id_unit_kompetensi]').val();
	}
	if(selected_jenis!="") params+='&filter_jenis='+selected_jenis;
	params+='&filter_id_bidang_teknis='+$('[name=filter_id_bidang_teknis]').val();
	ajaxGET(ctx + '/pertanyaan/list?page='+page+'&'+params+"",'onGetListDetilSuccess','onGetListDetilError');
}
//
function renderDashboard(){
	var params="";
	if(mode_debug!=null) params+="filter_debug=";
	ajaxGET(path + '/main?'+params+"",'onRenderDashboardSuccess','onerror');
}

function onRenderDashboardSuccess(response){
	console.log(response);
	var jumlahData=[];
	var labelData=[];
	var htmlBoxJml="";
	$.each(response.data.main, function(key){
		labelData.push(this.kodeKelompokJabatan);
		jumlahData.push(this.jumlahTotalUnitKompetensi);

		htmlBoxJml += '<div class="col col-xxl-3 mb-3 pr-md-2">'+
		'    <div class="card border-primary h-md-100">'+
		'        <div class="card-header bg-transparent border-bottom-0 pb-0">'+
		'            <h6 class="mb-0 mt-2 d-flex align-items-center">'+
		'                '+this.kodeKelompokJabatan+
		'            </h6>'+
		'        </div>'+
		'        <div class="card-body align-items-end">'+
		'            <div class="row flex-end-center d-flex align-items-center">'+
		'                <div class="col-auto pr-2">'+
		'                    <div class="fs--1 font-weight-semi-bold">'+this.persentaseTerisiUnitKompetensi+' %</div>'+
		'                </div>'+
		'                <div class="col pr-card pl-2">'+
		'                    <div class="progress mr-2" style="height: 8px;">'+
		'                        <div data-toggle="tooltip" title="'+this.persentaseTerisiUnitKompetensi+'" class="progress-bar bg-primary rounded-capsule" role="progressbar" style="width: '+this.persentaseTerisiUnitKompetensi+'%;" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>'+
		'                    </div>'+
		'                </div>'+
		'            </div>'+
		'        </div>'+
		'    </div>'+
		'</div>';
	});
	$('#container-dashboard').html(htmlBoxJml);
	
	//render bidang
	var htmlBidang="";
	$.each(response.data.bidang, function(key){
		htmlBidang += '<div class="mb-1 small">'+this.namaBidangTeknis+' <span class="float-right">'+this.persentaseTerisiUnitKompetensi+'%</span></div>'+
		'<div class="progress mb-3 rounded-soft" style="height: 10px;">'+
		'    <div data-toggle="tooltip" title="'+this.persentaseTerisiUnitKompetensi+'" class="progress-bar bg-primary border-right border-white border-2x" role="progressbar" style="width: '+this.persentaseTerisiUnitKompetensi+'%;" aria-valuenow="9.38" aria-valuemin="0" aria-valuemax="100"></div>'+
		'</div>';
	});
	$('#container-bidang').html(htmlBidang);
	
	//renderchart
	if(document.getElementById('myChart')!=null){
		var ctx = document.getElementById('myChart').getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: labelData,//['PEH', 'POLHUT', 'PLH', 'PEDAL', 'PK'],
		        datasets: [{
		            label: '',
		            data: jumlahData,//[12, 19, 3, 5, 2], //backgroundColor: 'rgba(54, 162, 235, 0.2)',
		             backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(255, 159, 64, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255, 99, 132, 1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(255, 159, 64, 1)'
		            ], 
		            borderWidth: 1,
		            steppedLine: false
		        }]
		    },
		    options: {
		        scales: {
		            xAxes: [{
		                barPercentage: 1,
		                categoryPercentage: 0.5 / 10 * 5,
		            }],
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});
	}
	renderDisplayMonitoring(response);
}

function renderDisplayMonitoring(response){
	var row="";
	$.each(response.data.monitoring, function(key){
		//first card
		$('#selesai-responden-count').text(this.respondenSelesai);
		$('#proses-responden-count').text(this.respondenProses);
		$('#no-proses-responden-count').text(this.respondenNoProses);
		$('#total-responden-count').text(this.totalResponden);
		//second card
		$('#jabatan-invalid-count').text(this.jabatanInvalid);
		$('#bidang-teknis-invalid-count').text(this.bidangTeknisInvalid);
		$('#duplicate-responden-count').text(this.respondenDuplicate);
		$('#duplicate-menjawab-count').text(this.menjawabDuplicate);
		
		row += '<tr class="btn-reveal-trigger" style="text-align:center!important">'+
		'    <td class="align-middle" style="width: 28px;">'+
		'        <div class="form-check mb-0 d-flex align-items-center"><input class="form-check-input" type="checkbox" id="checkbox-0" data-bulk-select-row="data-bulk-select-row" /></div>'+
		'    </td>'+
		'    <th class="align-middle white-space-nowrap name"><a href="../pages/customer-details.html">'+this.totalResponden+'</a></th>'+
		'    <td class="align-middle white-space-nowrap email">'+this.respondenSelesai+'</td>'+
		'    <td class="align-middle white-space-nowrap product">'+this.respondenProses+'</td>'+
//		'    <td class="align-middle text-center fs-0 white-space-nowrap payment">'+
//		'        <span class="badge badge rounded-pill badge-soft-success">Success <span class="ml-1 fas fa-check" data-fa-transform="shrink-2"></span> </span>'+
//		'    </td>'+
		'    <td class="align-middle text-right amount">'+this.respondenNoProses+'</td>'+
		'    <td class="align-middle text-right amount">'+this.respondenDuplicate+'</td>'+
		'    <td class="align-middle text-right amount">'+this.bidangTeknisInvalid+'</td>'+
		'    <td class="align-middle text-right amount">'+this.jabatanInvalid+'</td>'+
		'</tr>';
	});
	$('#table-monitoring-body').append(row);
}

function onerror(response){
	alert("errr");
}
