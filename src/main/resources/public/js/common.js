var selected_menu = '';

$(document).ready(function(){
	selected_menu = ($.urlParam('menu') == null ? '' : $.urlParam('menu'));
	
	$.ajaxSetup({
		headers : {
			'Authorization' : 'Bearer '+ getCookieValue(tokenCookieName)
		}
	});
	
	$('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('DD-MM-YYYY'));
		$(this).val(picker.endDate.format('DD-MM-YYYY'));
		picker.autoUpdateInput = true;
		$(this).change();
		
	});
	$('.single-date-picker').on('cancel.daterangepicker', function(ev, picker) {
	    $(this).val('');
	});
	
	
	$('.select2').select2();
    $('.select2').select2({
    		width: '100%'
    });
	
	$('.sidebar .nav-item' ).each(function() {
		var a = this.firstElementChild;
		if(selected_menu != ''){
			if(a.innerText.trim()==selected_menu.trim()){
				this.classList.add("active");
			}
		}else{
			if((window.location.href.indexOf(a.getAttribute('href'))> 0) == true){
				this.classList.add("active");
			}
		}
		
		
	});
	
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
	
	
	
	moment.locale('id', {
	    relativeTime : {
	        future: "in %s",
	        past:   "%s yg lalu",
	        s  : '1 detik',
	        ss : '%d detik',
	        m:  "1 menit",
	        mm: "%d menit",
	        h:  "1 jam",
	        hh: "%d jam",
	        d:  "1 hari",
	        dd: "%d hari",
	        M:  "1 bulan",
	        MM: "%d bulan",
	        y:  "1 tahun",
	        yy: "%d tahun"
	    }
	});

	
	$('.single-date-picker').daterangepicker({
	    singleDatePicker: true,
		autoUpdateInput: false,
	    locale: {
	            format: 'DD-MM-YYYY',
	            cancelLabel: 'Clear'
	    }
	});
	//$('.calendar').calendar();
	init();
});


function startLoading(btn){
	$('#'+btn).text('Loading..');
	$('#'+btn).prop('disabled', true);
}

function stopLoading(btn, btnlabel){
	$('#'+btn).text(btnlabel);
	$('#'+btn).prop('disabled', false);
}

function showAlertMessage(msg, timeout = 0){
	$('#modal-alert-msg').html(msg);
	$('#modal-alert').modal('show');
	if(timeout >0 ){
		setTimeout(function(){ $('#modal-alert').modal('hide'); }, timeout);
	}
}

function toastAlertMessage(msg){
	Toast.fire({
	  icon: 'success',
	  title: msg
	})
}

function onActionError(response, msg){
	console.log(response);
	$.LoadingOverlay("hide");
	$('.modal').modal('hide');
	if(response.responseJSON!=undefined){
		msg=response.responseJSON.message;
	}
	$('#modal-error-msg').html(msg);
	$('#modal-error').modal('show');
}

function getURLParam(param) {
	var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
	}
	return getUrlParameter(param);
}

function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

function url_base64_decode(str) {
	var output = str.replace('-', '+').replace('_', '/');
	switch (output.length % 4) {
		case 0:
			break;
		case 2:
			output += '==';
			break;
		case 3:
			output += '=';
			break;
		default:
			throw 'Illegal base64url string!';
	}
	var result = window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	return result;
}


function ajaxPOST(url,obj,fnsuccess, fnerror, param){
	$.ajax({
	    url : url,
	    method: "POST",
	    crossDomain: true,
	    contentType: false,
	    processData: false,
	    data : obj,
	    cache: false,
	    success : function (response, param) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response, param);
	    		}
	    },
	    error : function (response) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	});
}
function ajaxPOSTKU(url,obj,fnsuccess, fnerror){
	$.ajax({
	    url : url,
	    //method: "GET",
	    crossDomain: true,
	    contentType: false,
	    processData: false,
	    data : obj,
	    cache: false,
	    success : function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error : function (response) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	});
}
function ajaxGET(url, fnsuccess, fnerror, param){
    $.ajax({
        url: url,
        method: "GET",
        success: function (response) {
            if (fnsuccess instanceof Function) {
                fnsuccess(response, param);
            } else {
                var fn = window[fnsuccess];
                if(typeof fn === 'function') {
                    fn(response, param);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (fnerror instanceof Function) {
                fnerror(jqXHR);
            } else {
                var fn = window[fnerror];
                if(typeof fn === 'function') {
                    fn(jqXHR);
                }
            }
        }
        
    }); 
}
function ajaxGETDATA(url, fnsuccess, fnerror){
	$.ajax({
	    url: url,
	    method: "GET",
        xhrFields: {
            responseType: 'blob'
        },
	    success: function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error: function (response) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    }
		
	});	
}

function changeLang(lang){
	console.log(lang.split('_')[0]);
	moment.locale(lang.split('_')[0]);
	location.href = '?lang='+lang;
}


///** custom by Hadi **/

function getValue(value, replace = '') {
	return (value == null || value == 'null' ? replace : value) 
}

//function getDateForList(datetime) {
//	let result = moment(datetime).format('DD-MM-YYYY').toUpperCase();
//	return (result == 'INVALID DATE' ? '' : result);
//}
function getDateForList(datetime, type='date') {
	console.log(type);
    var result = moment(datetime).format('DD-MM-YYYY').toUpperCase();
    if(type=='datetime') result = moment(datetime).format('DD-MM-YYYY HH:mm').toUpperCase();
    if(result == 'INVALID DATE') return '';
    var month = result.split('-')[1];
    var month_name = '';
    if(month=='01') month_name = 'Jan';
    else if(month=='02') month_name = 'Feb';
    else if(month=='03') month_name = 'Mar';
    else if(month=='04') month_name = 'Apr';
    else if(month=='05') month_name = 'Mei';
    else if(month=='06') month_name = 'Jun';
    else if(month=='07') month_name = 'Jul';
    else if(month=='08') month_name = 'Agu';
    else if(month=='09') month_name = 'Sep';
    else if(month=='10') month_name = 'Okt';
    else if(month=='11') month_name = 'Nov';
    else month_name = 'Des';
    return result.split('-')[0] + ' ' + month_name.toUpperCase() + ' ' + result.split('-')[2];
}

function getDateForInput(datetime) {
	return moment(datetime).format('DD-MM-YYYY');
}


$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var contains = function(needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if(!findNaN && typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function(needle) {
            var i = -1, index = -1;

            for(i = 0; i < this.length; i++) {
                var item = this[i];

                if((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
};

function objectifyForm(formArray) {//serialize data function

	  var returnArray = {};
	  for (var i = 0; i < formArray.length; i++){
		  returnArray[formArray[i]['name']] = formArray[i]['value'];
	  }
	  return returnArray;
	}