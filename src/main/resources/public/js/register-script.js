
$( document ).ready(function() {

    /*
        Fullscreen background
    */
    $.backstretch(ctx + "/images/backgrounds/1.jpg");
    
    $("form").submit(function(e){
    		if($('[name=source]').length ){
    			$('[name=username]').val($('[name=username]').val()+'::'+$('[name=source]').val());
    		}
    		if($('[name=password]').length ){
    			if($('[name=password]').val() == ''){
    				alert('Password tidak boleh kosong, harap diisi.');
    				$('#btnLogin').val('Update');
    				e.preventDefault();
    				return false;
    			}
    		}
			if($('#password').val() != $('#password2').val() ){
				alert('Konfirmasi Password tidak sama, harap dicek kembali.');
				$('#btnLogin').val('Update');
				e.preventDefault();
				return false;
    		}
    		if($('[name=email]').length ){
    			if($('[name=email]').val() != '') {
    				if(!validateEmail($('[name=email]').val())){
        				alert('Format email tidak benar, harap diperbaiki.');
        				$('#btnLogin').val('Update');
        				e.preventDefault();
        				return false;
        			}
    			}
    			
    		}
    		
    		$('#btnLogin').text("Sedang proses..");
    });
    
    $('[name=source]').on('change', function(e){
    		$("[name=password]").focus();
    });
    	
    if($('[name=password]').length ){
    		$("[name=password]").focus();
    }else{
    		$("[name=username]").focus();
    }
    

    $("#show_password").on('click', function(event) {
        event.preventDefault();
        if($('#password').attr("type") == "text"){
            $('#password').attr('type', 'password');
            $('#show_password i').addClass( "fa-eye-slash" );
            $('#show_password i').removeClass( "fa-eye" );
        }else if($('#password').attr("type") == "password"){
            $('#password').attr('type', 'text');
            $('#show_password i').removeClass( "fa-eye-slash" );
            $('#show_password i').addClass( "fa-eye" );
        }
    });
    $("#show_password2").on('click', function(event) {
        event.preventDefault();
        if($('#password2').attr("type") == "text"){
            $('#password2').attr('type', 'password');
            $('#show_password i').addClass( "fa-eye-slash" );
            $('#show_password i').removeClass( "fa-eye" );
        }else if($('#password2').attr("type") == "password"){
            $('#password2').attr('type', 'text');
            $('#show_password i').removeClass( "fa-eye-slash" );
            $('#show_password i').addClass( "fa-eye" );
        }
    });
     
});


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
