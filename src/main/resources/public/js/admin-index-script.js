var url_responden = ctx + "/responden/";
var url_sesi = ctx + "/sesi/";
var url_dashboard = ctx + "/dashboard/";

var selected_status='';

var param = "";
var selected_id='';
function init() {
	selected_status = ($.urlParam('status') == null ? '' : $.urlParam('status'));
	
	if(selected_status != "") getSuratTugasList();
	renderChart();
}

function to_persen(nilai){
	return((nilai)*100)+'%';
}

function renderChart(){
	var ctx = document.getElementById('myChart').getContext('2d');
	var obj_chart = [];
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = tahun; i <= now; i++) {
		$("[name=tahun]").append(new Option(i));
		obj_chart.push({"tahun" : i, "responden" : 0, "progress" : 0, "selesai" : 0});
	}
	
	$.getJSON(url_dashboard+'list_tahun', function(response_dashboard) {
		console.log("response_dashboard", response_dashboard);
		console.log(obj_chart);
		if (response_dashboard.code == 200) {
			var arr_tahun=[];
			var arr_tahun_data=[];
			var arr_tahun_data_progress=[];
			var arr_tahun_data_selesai=[];
			var arr_tahun_data_persen=[];
			var count_responden=0;
			$.each(obj_chart, function(key_chart, val_chart){
				$.each(response_dashboard.data, function(key, value_dashboard){
					if(value_dashboard.tahun_sesi == val_chart.tahun){
						obj_chart[key_chart].responden=value_dashboard.jumlah_survey_total
						obj_chart[key_chart].progress=value_dashboard.jumlah_survey_progress
						obj_chart[key_chart].selesai=value_dashboard.jumlah_survey_selesai
						count_responden+=parseInt(value_dashboard.jumlah_survey_total);
					}
				});
				arr_tahun_data.push(obj_chart[key_chart].responden);
				arr_tahun_data_persen.push(to_persen(obj_chart[key_chart].responden));
				arr_tahun_data_progress.push(obj_chart[key_chart].progress);
				arr_tahun_data_selesai.push(obj_chart[key_chart].selesai);
				arr_tahun.push(val_chart.tahun);
			});

			console.log("arr_tahun_data ",arr_tahun_data);
			console.log("arr_tahun_data_persen ",arr_tahun_data_persen);
			console.log("arr_tahun ",arr_tahun);
			console.log("count_responden ", count_responden);
			$('#total_responden').text(count_responden);
			
//			var ctx = document.getElementById("myPieChart");
//			var myPieChart = new Chart(ctx, {
//			  type: 'pie',
//			  data: {
//			    labels: ["Laki laki:<?php echo $jumlah_pria; ?>","Perempuan:<?php echo $jumlah_perempuan; ?>"],
//			    datasets: [{
//			    label: '',
//			      data: [<?php echo $tampilkan['jumlah_pria']; ?>, <?php echo $tampilkan['jumlah_perempuan']; ?>],
//			      backgroundColor: ['#007bff', '#dc3545'],
//			    }],
//			  },
//			});
			
			var chart = new Chart(ctx, {
			    // The type of chart we want to create
			    type: 'bar',
			    // The data for our dataset
			    data: {
			        labels: arr_tahun,//['2017', '2018', '2019', 'Ya'],
			        datasets: [{
			            label: 'Selesai',
			            backgroundColor: 'rgba(217,83,79,0.75)',
			            borderColor: 'rgb(255, 99, 132)',
			            id: "y-axis-0",
			            data: arr_tahun_data_selesai//[3, 27, 84, 1]
			        },{
			            label: 'Progress',
			            backgroundColor: 'rgba(92,184,92,0.75)',
			            borderColor: 'rgb(255, 99, 132)',
			            id: "y-axis-0",
			            data: arr_tahun_data_progress//[3, 27, 84, 1]
			        }]
			    },
			    // Configuration options go here
			    options: {
			        legend: {
			            display: false,
			            labels: {
			                fontColor: 'rgb(255, 99, 132)'
			            }
			        },
			        scales: {
			            xAxes: [{
			              stacked: true
			            }]
			        }
			    }
			});
		}
	});
}
function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=tahun]").append(new Option(i));
	}
}


function save(){
	var obj = new FormData(document.querySelector('#form-surat-tugas'));
	if($(".list-group .list-group-item.active").data('responden') != undefined) obj.append('id', $(".list-group .list-group-item.active").data('responden'));
	
	if($(".list-group .list-group-item.active").length > 0)
	obj.append('surat_tugas_id', $(".list-group .list-group-item.active").data('id'));
    console.log(obj);
    ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
//	$("#progressBar").hide();
//	$("#modalPengguna").modal('hide');
//	refresh();
//	reset();
	window.location.href=ctx+"/page/survey?id="+response.data.id_responden+"&kegiatan_pengawasan_id="+$('[name=jenis_audit_id]').val();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	console.log(response);
	addAlert('msg', "alert-danger", "kesalahan dari server");
	if(response.responseJSON.code=="409")
	alert(response.responseJSON.message)
	else alert("Surat tugas belum dipilih.");
}

//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

