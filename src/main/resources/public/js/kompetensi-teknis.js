var path = ctx + '/restql';

var param = "";
var selected_id='';
var selected_action='';

var appendIds = [];
function init() {
	getSelectKelompokJabatan();
	
	$('#btn-add,#btn-adds').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
		$('[name=id_kelompok_jabatan]').val($('#nav-tab .active').data('id'));
		getSelectJabatan($('#nav-tab .active').data('id'));
		getSelectUnitKompetensiTeknis($('#nav-tab .active').data('id'));
		getSelectKelompokTeknis($('#nav-tab .active').data('id'));

		$('[name=id_bidang_teknis]').val($('[name=filter_id_bidang_teknis]').val());
		$('#id_bidang_teknis').val($('[name=filter_id_bidang_teknis] option:selected').text());
		$('[name=id_unit_kompetensi_teknis]').val("").trigger('change');
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('[name=id_jenjang_jabatan]').on('change', function(){
		console.log("sebelum");
		console.log(this.value);
		var newValue = this.value.split("/");
		console.log("sesudah");
		console.log(newValue);
		$('[name=id_tingkat_jenjang]').val(newValue[1]);
	});
	
	$('[name=jenis_kompetensi]').on('click', function(){
		if(this.value=="INTI"){
			$('[name=id_kelompok_teknis]').attr('disabled',true);
			$('[name=id_kelompok_teknis]').val('').trigger('change');
		}else if(this.value=="PILIHAN"){
			$('[name=id_kelompok_teknis]').attr('disabled',false);
		}
	});
	
	$('[name=id_kelompok_jabatan]').on('change', function(){
		getSelectJabatan(this.value);
	});

	
	$('[name=filter_id_bidang_teknis]').on('change', function(){
		display(1,'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+this.value+'")');//,id_master_jabatan.eq("'+$('[name=filter_id_master_jabatan]').val()+'")
		$('#btn-add').attr('disabled',false);
	});
	
	$('[name=filter_id_master_jabatan]').on('change', function(){
		display(1,'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+$('[name=filter_id_bidang_teknis]').val()+'"),id_master_jabatan.eq("'+this.value+'")');
		$('#btn-add').attr('disabled',false);
	});
	
	$('#tbl-data').on('click', '.data-row', function(event) {
		$(this).addClass('table-primary').siblings().removeClass('table-primary');
		 
	});
	$('#tbl-data').on('click', '.data-head th', function(event) {
		if($(this).hasClass("table-primary")){
			$(this).removeClass('table-primary');
		}else{
			$(this).addClass('table-primary');
		}
	});
}

function getSelectBidangTeknis(id){
	$('[name=id_bidang_teknis]').empty();
	$('[name=id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	$('[name=filter_id_bidang_teknis]').empty();
	//
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="";
	ajaxGET(path + '/bidang_teknis?'+params,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_bidang_teknis]').append(new Option(value.nama, value.id));
		$('[name=filter_id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	display();
}

function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	//$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		var params='onclick="getSelectBidangTeknis('+value.id+')"';
		$('#nav-tab').append('<a data-id="'+value.id+'" '+params+' class="nav-item nav-link '+(key==0?"active":"")+'" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">'+value.kode+'</a>');
		
	});
	getSelectBidangTeknis($('#nav-tab .active').data('id'));
	display();
}

function getSelectJabatan(id_kelompok_jabatan){
	$('[name=id_master_jabatan]').empty();
	$('[name=filter_id_master_jabatan]').empty();
	$('[name=id_master_jabatan]').append(new Option('Pilih Jabatan', ''));
	var page=1,params="filter=id_kelompok.eq('"+id_kelompok_jabatan+"')",qry="";
	ajaxGET(path + '/master_jabatan?page='+page+'&'+params+'&'+qry,'onGetSelectJabatan','onGetSelectError');
}
function onGetSelectJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_master_jabatan]').append(new Option(value.nama, value.id));
		$('[name=filter_id_master_jabatan]').append(new Option(value.nama, value.id));
	});
	$('[name=id_master_jabatan]').val($('#table-data tbody,tr.data-row.table-primary').data('id')).trigger('change');
}

function getSelectKelompokTeknis(id_kelompok_jabatan){
	$('[name=id_kelompok_teknis]').empty();
	$('[name=id_kelompok_teknis]').append(new Option('Pilih Kelompok Teknis', ''));
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id_kelompok_jabatan+"'),id_bidang_teknis.eq('"+$('[name=filter_id_bidang_teknis]').val()+"')",qry="";
	ajaxGET(path + '/kelompok_teknis?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokTeknis','onGetSelectError');
}
function onGetSelectKelompokTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_teknis]').append(new Option(value.nama, value.id));
	});
	if($('#table-data thead,th.table-primary').data('id')!="INTI" && $('#table-data thead,th.table-primary').data('id')!=undefined){
		$('[name=id_kelompok_teknis]').val($('#table-data thead,th.table-primary').data('id')).trigger('change');
		$('#jenis_kompetensi2').click();
	}else if($('#table-data thead,th.table-primary').data('id')!=undefined) {
		$('#jenis_kompetensi1').click();
	}
		
}

function getSelectUnitKompetensiTeknis(id_kelompok_jabatan){
	$('#id_unit_kompetensi_teknis').empty();
//	$('[name=id_unit_kompetensi_teknis]').append(new Option('Pilih Unit Kompetensi Teknis', ''));
	var page=1,
	params="filter=id_kelompok_jabatan.eq('"+id_kelompok_jabatan+"')",
	qry="limit=100000000";
	ajaxGET(path + '/unit_kompetensi_teknis?page='+page+'	&'+params+'&'+qry,'onGetSelectUnitKompetensiTeknis','onGetSelectError');
}
function onGetSelectUnitKompetensiTeknis(response){
	$.each(response.data, function(key, value) {
		$('#id_unit_kompetensi_teknis').append(new Option(value.kode, value.id));
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		//param = $('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		//console.log(param);
		display(1, 'filter=');
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

//function save(){
//	//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
//	
//	var url = path+'/jabatan_kompetensi_teknis/save?append=false&join=kelompok_jabatan,master_jabatan,bidang_teknis,kelompok_teknis,unit_kompetensi_teknis';
//	
//	window.obj = {
//			data : objectifyForm($('#entry-form').serializeArray())
//	};
//	//if($('#id_unit_kompetensi_teknis').val().length>){
//	    obj.data.id_unit_kompetensi_teknis = $('#id_unit_kompetensi_teknis').val();
//	//}
////	console.log(obj.)
//	if($('[name=id]').val() == "") delete obj.data.id;
//	console.log(obj);
//	
//	$.ajax({
//        url : url,
//        type : "POST",
//        traditional : true,
//        contentType : "application/json",
//        dataType : "json",
//        data : JSON.stringify(obj),
//        success : function (response) {
//        		//$.LoadingOverlay("hide");
//        		//$('#modal-form').modal('hide');
//        		//display();
//        	onModalActionSuccess(response);
//        },
//        error : function (response) {
//        		//$.LoadingOverlay("hide");
//        		$('#modal-form-msg').text(response.responseJSON.message);
//        		$('#modal-form-msg').removeClass('d-none');
//        		$('#modal-form-msg').show();
//        },
//    });
//}
function save(){
	var obj = new FormData(document.querySelector('#entry-form'));
    ajaxPOST(ctx + '/jabatan-kompetensi-teknis/save',obj,'onModalActionSuccess','onSaveError');
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();

	$('[name=id_master_jabatan]').val('').trigger('change');
	$('[name=id_kelompok_teknis]').val('').trigger('change');
}


function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#entry-form').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

//batas
function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	tbody.text('');
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=kelompok_jabatan,master_jabatan,bidang_teknis,kelompok_teknis,unit_kompetensi_teknis&limit=10000000";
	if(params=="") qry+="&filter=id_kelompok_jabatan.eq("+$('#nav-tab .active').data('id')+"),id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	else qry+="&filter="+params;//+",id_bidang_teknis.eq("+$('[name=filter_id_bidang_teknis]').val()+")";
	ajaxGET(path + '/jabatan_kompetensi_teknis?page='+page+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	var tbody = $("#tbl-data").find('tbody');
	var qry="";
	var page=1;
	var params="";
	if($('[name=filter_id_bidang_teknis]').val()!=""){
		params+='filter=id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+$('[name=filter_id_bidang_teknis]').val()+'")';
	
	var responseDataJabatanKompetensiTeknis = response.data;
	$.ajax({
        url : path + '/kelompok_teknis?page='+page+'&'+params+'&'+qry,
        type : "GET",
        traditional : true,
        contentType : "application/json",
        success : function (response) {
        	var responseDataKelompokTeknis = response.data;
        	//render head
        	var thead = $("#head");
        	var rowHead = '<tr class="data-head">'+
        	'        <th rowspan="3" width="3%">No</th>'+
        	'        <th  rowspan="3" width="13%">Jabatan</th>'+
        	'        <th class="text-center " colspan="2" data-id="INTI">Kompetensi Inti</th>'+
        	'        <th class="text-center" colspan="'+(responseDataKelompokTeknis.length*2)+'">Kompetensi Pilihan</th>'+
        	'    </tr>'+
        	'    <tr class="data-head">'+
        	'        <th rowspan="2" width="3%">No</th>'+
        	'        <th rowspan="2" width="10%">Kode Unit</th>';
        	
        	$.each(responseDataKelompokTeknis,function(key,value){
        		rowHead += '<th class="text-center data-head" colspan="2" data-id="'+value.id+'">'+value.nama+'</th>';
        	});
        	
        	rowHead +='</tr>'+
        	'    <tr>';
        	$.each(responseDataKelompokTeknis,function(key,value){
        		rowHead +='<th class="" width="3%">No</th>';
        		rowHead +='<th class="" width="10%">Kode Unit</th>';
        	});
        	rowHead +='</tr>';
        		
        	thead.html(rowHead);
        	//end head
        	
        	var html=[];
        	$.each(responseDataJabatanKompetensiTeknis,function(key,value){
        		var name = value.master_jabatan.nama;
        		if(html.indexOf(name) == -1) html.push(name);
        		else html.push("");
        	});

        	$('#loading-row').remove();
        	$('#no-data-row').remove();
        	var row = "";
        	var num = $('.data-row').length+1;num = 1;
        	$.each(responseDataJabatanKompetensiTeknis,function(key,value){
				var id_jabatan_kompetensi = value.id;
        		if(html[key]){
        			row += '<tr data-id="'+value.id_master_jabatan+'" class="data-row" id="row-'+value.id+'">';
        			row += '<td>'+(num)+'</td>';
        			row += '<td class="">'+html[key]+'</td>';
        				row += '<td>';
        				var id = value.id_master_jabatan;
        				var no=1;
        				$.each(responseDataJabatanKompetensiTeknis,function(key,value){
        					if(id==value.id_master_jabatan && value.jenis_kompetensi=="INTI"){
        						row += '<div>'+(no)+'</div>';
        						no++;
        					}
        				});
        				row += '</td>';
        				row += '<td>';
        				var id = value.id_master_jabatan;
        				var no=1;
        				$.each(responseDataJabatanKompetensiTeknis,function(key,value){
        					if(id==value.id_master_jabatan && value.jenis_kompetensi=="INTI"){
        						row += '<div class="d-flex" >'+value.unit_kompetensi_teknis.kode+''+""+''+' '+' <a href="#" onclick="doAction(\''+value.id+'\',\'delete\')"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><a href="javscript:void(0)" data-id="'+value.id+'" data-id-kompetensi-teknis="'+value.unit_kompetensi_teknis.id+'" onclick="almghty(this)"> <span class="fas fa-fw fa-pencil-alt" style="color:blue;"></span></a>'+'</div>';
        						no++;
        					}
        				});
        				row += '</td>';
        				
        				$.each(responseDataKelompokTeknis,function(key,valKelTek){
        					
        					row += '<td>';
        					var no=1;
        					$.each(responseDataJabatanKompetensiTeknis,function(key,value){
        						if(id==value.id_master_jabatan && value.jenis_kompetensi=="PILIHAN" & valKelTek.id== value.id_kelompok_teknis){
        							row += '<div>'+(no)+'</div>';
        							no++;
        						}
        					});
        					row += '</td>';
        					
        					row += '<td>';
        					var no=1;
        					$.each(responseDataJabatanKompetensiTeknis,function(key,value){
        						if(id==value.id_master_jabatan && value.jenis_kompetensi=="PILIHAN" & valKelTek.id== value.id_kelompok_teknis){
        							row += '<div class="d-flex" >'+value.unit_kompetensi_teknis.kode+''+""+''+' '+' <a href="#" onclick="doAction(\''+value.id+'\',\'delete\')"> <span class="fas fa-fw fa-trash-alt" style="color:red;"></span></a><a href="javscript:void(0)" data-id="'+value.id+'" data-id-kompetensi-teknis="'+value.unit_kompetensi_teknis.id+'" onclick="almghty(this)"> <span class="fas fa-fw fa-pencil-alt" style="color:blue;"></span></a>'+'</div>';
            						no++;
        						}
        					});
        					row += '</td>';
        				});
               			
        			row += '</tr>';
            		num++;
        		}
        	});
        	
        	//tbody.html(row);
        	if(response.next_more){
        		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
        		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
        		row += '</div></td></tr>';
        	}
        	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="'+4+(responseDataKelompokTeknis.length*2)+'"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
        	
        },
        error : function (response) {
        	alert("wrong");
//        		$.LoadingOverlay("hide");
//        		$('#modal-form-msg').text(response.responseJSON.message);
//        		$('#modal-form-msg').removeClass('d-none');
        },
    });
	}
}
function almghty(e){
	console.log(e.dataset.idKompetensiTeknis);
	console.log(e.dataset.id);
	//window.location.href="form-pertanyaan?id="+e.dataset.id;
	window.open("form-pertanyaan?id="+e.dataset.idKompetensiTeknis+"&jenis=teknis&bidang_teknis="+$('[name=filter_id_bidang_teknis]').val(), '_blank');
	console.log(e);
}
function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		query="";
		ajaxGET(path + '/jabatan_kompetensi_teknis/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=jabatan';
		query="";
		ajaxPOST(path + '/jabatan_kompetensi_teknis/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display(1,'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'"),id_bidang_teknis.eq("'+$('[name=filter_id_bidang_teknis]').val()+'"),id_master_jabatan.eq("'+response.data.id_master_jabatan+'")');
	$('[name=filter_id_master_jabatan]').val(response.data.id_master_jabatan);
	// Update data ke existing tabel
	/*if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}*/
	
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=id_kelompok_jabatan]').val(value.id_kelompok_jabatan);
	$('[name=id_master_jabatan]').val(value.id_master_jabatan).trigger('change');
	$('[name=id_bidang_teknis]').val(value.id_bidang_teknis);
	$('[name=id_kelompok_teknis]').val(value.id_kelompok_teknis).trigger('change');
	$('[name=id_unit_kompetensi_teknis]').val(value.id_unit_kompetensi_teknis);
	$('[name=jenis_kompetensi]').val(value.jenis_kompetensi);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

var html = [];
function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';//value.kelompok_jabatan.kode
	row += '<td class="">'+value.master_jabatan.nama+'</td>';
	var name = value.master_jabatan.nama;
	if(html.indexOf(name) == -1) html.push(name);
	else html.push("");
	row += '<td class="">'+value.bidang_teknis.nama+'</td>';
	row += '<td class="">'+value.kelompok_teknis.nama+'</td>';
	row += '<td class="">'+value.unit_kompetensi_teknis.kode+'</td>';
	row += '<td class="">'+value.jenis_kompetensi+'</td>';


	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}