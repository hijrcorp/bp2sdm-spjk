var path = ctx + '/restql';
var id_temp_bidang_teknis=0;

function init(){

	getSelectKelompokJabatan();
	getListTahun();
	display();

	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-reset').click(function(e){
		clearForm();
		$("#search-form").trigger('reset');
	});
	
	$("#entry-form").submit(function() {
		save();
		return false;
	});
	

	$('#search-form').submit(function(e){
		var orig = $('#search-form').serialize();
		var withoutEmpties = orig.replace(/[^&]+=\.?(?:&|$)/g, '')
		display(1, withoutEmpties);
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('.single-date-picker').on('change', function(){
         var dateFirst = new Date($('[name=tanggal_mulai]').val());//"11/25/2017"
         var dateSecond = new Date($('[name=tanggal_selesai]').val());//"11/28/2017"
         // time difference
         var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());
         // days difference
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
         $('#jumlah_cuti').text(diffDays+" Hari");
         $('[name=jumlah_cuti]').val(diffDays+" Hari"); 
	});
}

function cekInputPeriode(e){
	console.log($(e).is(':checked'));	
	if(!$(e).is(':checked')){
		$('[name=periode_awal]').attr('disabled', true);
		$('[name=periode_akhir]').attr('disabled', true);
	}else{
		$('[name=periode_awal]').attr('disabled', false);
		$('[name=periode_akhir]').attr('disabled', false);
	}
}

function getSelectKelompokJabatan(){
	$('[name=kelompok_jabatan_id]').empty();
	$('[name=kelompok_jabatan_id]').append(new Option('Pilih Kelompok Jabatan', ''));
	$('[name=filter_kelompok_jabatan_id]').empty();
	$('[name=filter_kelompok_jabatan_id]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=kelompok_jabatan_id]').append(new Option(value.kode, value.id));
		$('[name=filter_kelompok_jabatan_id]').append(new Option(value.kode, value.id));
	});
	$('[name=kelompok_jabatan_id]').on('change', function(){
		getSelectBidangTeknis(this.value);
	});
	$('[name=filter_kelompok_jabatan_id]').on('change', function(){
		getSelectBidangTeknis(this.value);
	});
}

function getSelectBidangTeknis(id_kelompok_jabatan, id_bidang_teknis=0){
	$('[name=bidang_teknis_id]').empty();
	$('[name=bidang_teknis_id]').append(new Option('Pilih Bidang Teknis', ''));
	$('[name=filter_bidang_teknis_id]').empty();
	$('[name=filter_bidang_teknis_id]').append(new Option('Pilih Bidang Teknis', ''));
	//
	id_temp_bidang_teknis=id_bidang_teknis;
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id_kelompok_jabatan+"')",qry="";
	ajaxGET(path + '/bidang_teknis?'+params,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=bidang_teknis_id]').append(new Option(value.nama, value.id));
		$('[name=filter_bidang_teknis_id]').append(new Option(value.nama, value.id));
	});
	if(id_temp_bidang_teknis>0){
		$('[name=bidang_teknis_id]').val(id_temp_bidang_teknis);
	}
}

function getListTahun() {
	var d = new Date();
	var now = d.getFullYear()+1;
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=tahun]").append(new Option(i, i));
		$("[name=filter_tahun]").append(new Option(i, i));
	}
	$("[name=tahun]").val(d.getFullYear());
	$("[name=filter_tahun]").val(d.getFullYear());
	/*var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=tahun]").append(new Option(i, i));
		$("[name=filter_tahun]").append(new Option(i, i));
	}*/
}
//function save(){
//	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
//	
//	var url = path+'/sesi/save';
//	if($('[name=id]').val() != "") url = path+'/sesi/save?append=false';
//	window.obj = {
//			data : objectifyForm($('#entry-form').serializeArray())
//		};
//	if($('[name=id]').val() == "") delete obj.data.id;
//	console.log(obj);
//	$.ajax({
//        url : url,
//        type : "POST",
//        traditional : true,
//        contentType : "application/json",
//        dataType : "json",
//        data : JSON.stringify(obj),
//        success : function (response) {
//        		$.LoadingOverlay("hide");
//        		$('#modal-form').modal('hide');
//        		display();
//        },
//        error : function (response) {
//        		$.LoadingOverlay("hide");
//        		$('#modal-form-msg').text(response.responseJSON.message);
//        		$('#modal-form-msg').removeClass('d-none');
//        },
//    });
//    
//}
function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	var obj = new FormData(document.querySelector('#entry-form'));
    //if(selected_id != '') obj.append('id', selected_id);
	//obj.append("mode", mode);
    ajaxPOST(ctx + '/sesi/save',obj,'onModalActionSuccess','onSaveError');
}
function onSaveError(response){
	console.log(response);
	$.LoadingOverlay("hide");
	$('#modal-form-msg').html(response.responseJSON.message);
	$('#modal-form-msg').removeClass('d-none');
	$('#modal-form-msg').show();
}

function getSelectJenisCuti(){
	$('[name=id_jenis]').empty();
	$('[name=id_jenis]').append(new Option('Pilih Jenis Cuti', ''));
	//
	ajaxGET(path + '/cuti_pengaturan','onGetSelectJenisCuti','onGetSelectError');
}
function onGetSelectJenisCuti(response){
	$.each(response.data, function(key, value) {
		$('[name=id_jenis]').append(new Option(value.nama, value.id));
	});
}

function display(page=1, params=''){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//tbody.html('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	//console.log(path + '/list?page='+page+'&'+params);
	var qry="join=pegawai,akun_bank";
	qry="";
	ajaxGET(ctx + '/sesi/list?page='+page+'&'+params+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		var span="";
		row += '<tr class="data-row" id="row-'+value.id+'">';
			row += '<td>'+(num)+'</td>';
			row += '<td> '+'<a title="klik untuk meninjau dashboard" target="_blank" href="../dashboard/laporan?kode-sesi='+value.kode+'" >'+value.kode+'<a/>'+' </td>';
			row += '<td> '+value.tahun +' </td>';
			if(value.sumber=="SCHEDULED_NOW") span="badge badge-success";
			if(value.sumber=="DONE") span="badge badge-primary";
			if(value.sumber=="IN_COMMING") span="badge badge-warning";
			row += '<td class="">'+value.periode_awal_serialize+' <strong>s.d</strong> '+value.periode_akhir_serialize+' ('+value.jumlah_hari+' Hari)<br/><span class="'+span+'">'+value.sumber+'</span></td>';

			row += '<td class="text-small ">'+value.keterangan+'</td>';

			row += '<td class="">';
				row +='<a onclick="generateToDashboard(\''+value.kode+'\')" href="javascript:void(0)" >Generate<a/>';//'+value.account.length+"/"+value.account.length+' //hasil-ujian?filter_id='+value.id+'
			row += '</td>';
			row += '<td class="text-small ">'+getDateForList((value.timestamp_modified==null?value.timestamp_added:value.timestamp_modified), 'datetime')+'</td>';
			row += '<td class="text-center">';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
			row += '</td>';
		row += '</tr>';
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		//var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		var query=select+"&fetch=penggajian_detil.select(id,id_penggajian,jenis,komponen,jumlah,keterangan)";//akun_bank.limit(1)
		query=""
		ajaxGET(path + '/sesi/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		var cascade="";
		//if(selected_action=="delete")  cascade="&cascade=penggajian_detil";
		var query="";
		ajaxPOST(path + '/sesi/'+id+'/'+selected_action+'?'+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('.modal table').find('tbody').html('');
	$('[name=filter_id_pegawai]').val('').trigger('change');
	$('[name=filter_id_akun_bank]').val('').trigger('change');
	$('#customSwitch1').prop('checked', false); 
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=kode]').val(value.kode);
	$('[name=tahun]').val(value.tahun);
	$('[name=periode_awal]').val(getDateForInput(value.periode_awal));
	$('[name=periode_akhir]').val(getDateForInput(value.periode_akhir));
	$('[name=periode_awal]').prop('disabled', true);
	$('[name=periode_akhir]').prop('disabled', true);
	$('[name=keterangan]').val(value.keterangan);
	$('[name=kelompok_jabatan_id]').val(value.kelompok_jabatan_id);
	getSelectBidangTeknis(value.kelompok_jabatan_id, value.bidang_teknis_id);
	/*setTimeout(() =>
	$('[name=bidang_teknis_id]').val(value.bidang_teknis_id), 3000
	);*/
	
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display();
	
	// Update data ke existing tabel
	/*if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}*/
	
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}
function onError(response){
	console.log(response);
	if(response.responseJSON.code==400 || response.status==400){
		Swal.fire({
		  icon: 'error',
		  //title: 'Oops...',
		  html: '<p class="text-left">'+response.responseJSON.message+'</p>'
		})
	}else{
		Swal.fire({
		  icon: 'error',
		  //title: 'Oops...',
		  html: '<p class="text-left">Waktu load terlalu lama atau mohon coba beberapa saat lagi, dan pastikan internet anda dalam keadaan stabil.</p>'+
			  '<div class="text-left"><strong>Terima Kasih</strong></div>',
		})
	}
}
function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  //return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
  return seconds;
}

let timerInterval
let isDone=true;
function generateToDashboard(idsesi){
	/*Swal.fire({
	  title: 'Copy data sedang di proses, mohon tunggu.',
	  allowEscapeKey: false,
	  allowOutsideClick: false,
	  timerProgressBar: true,
	  showCancelButton: false,
	  showConfirmButton: false,
	  html:
	    'Estimasi selesai dalam waktu <strong></strong> detik.',
	  timer: 60000,
	  didOpen: () => {
	    const content = Swal.getHtmlContainer()
	    const $ = content.querySelector.bind(content)
	 
	    //const increase = $('#increase')
	 
	    Swal.showLoading()
	 
	    //function toggleButtons () {
	     // stop.disabled = !Swal.isTimerRunning()
	     // resume.disabled = Swal.isTimerRunning()
	    //}
	 
	    //stop.addEventListener('click', () => {
	      //Swal.stopTimer()
	     // toggleButtons()
	    //})
	 
	    //increase.addEventListener('click', () => {
	      //Swal.increaseTimer(5000)
	    //})
		if(Swal.getHtmlContainer().querySelector('strong')
		        .textContent!=null){
		    timerInterval = setInterval(() => {
		      Swal.getHtmlContainer().querySelector('strong')
		        .textContent = (Swal.getTimerLeft() / 1000)
		          .toFixed(0)
		    }, 100)
		}
		ajaxGET(ctx + '/sesi/copy-hasil-survey?id_sesi='+idsesi,'onGetCopySuccess','onError');
	  },
	  willClose: () => {
		clearInterval(timerInterval)
	  }
	}).then((result) => {
	  if (result.dismiss === Swal.DismissReason.timer) {
	    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	  }
	})*/
	const { value: ipAddress } = Swal.fire({
	  title: 'Mohon Untuk Konfirmasi, Sebelum Generate',
	  input: 'text',
	  allowEscapeKey: false,
	  allowOutsideClick: false,
	  inputLabel: 'Ketik Kode Berikut : "KONFIRMASI_GENERATE"',
	  //inputValue: inputValue,
	  showCancelButton: true,
	  inputValidator: (value) => {
	    if (value=="") {
	    	return 'You need to write something!'
	    }else if(value!="KONFIRMASI_GENERATE"){
			return 'Kode yang Anda Masukan Tidak Tepat.'
		}else{
			Swal.fire({
			  title: 'Copy data sedang diproses, mohon tunggu.',
			  allowEscapeKey: false,
			  allowOutsideClick: false,
			  html: 
				'Estimasi selesai dalam waktu : <b></b> detik.'+
				'<br/><br/><p class="text-small small text-muted text-mutted">*Perkiraan waktu bisa lebih lama dari waktu yang tertera, tergantung performa koneksi Anda, atau kondisi server yang tidak dalam kondisi ramai.</p>'
				,
			  timer: 60000,
			  timerProgressBar: true,
			  showCancelButton: false,
			  showConfirmButton: false,
			  willOpen: () => {
			    Swal.showLoading()
			    timerInterval = setInterval(() => {
			      const content = Swal.getContent()
			      if (content) {
			        const b = content.querySelector('b')
			        if (b) {
			          b.textContent = millisToMinutesAndSeconds(Swal.getTimerLeft());//Swal.getTimerLeft().toString().substring(0,2);
						if(Swal.getTimerLeft().toString().substring(0,2) < 1){
							Swal.stopTimer();
						}
			        }
			      }
			    }, 100)
			    ajaxGET(ctx + '/sesi/copy-hasil-survey?id_sesi='+idsesi,'onGetCopySuccess','onError');
			  },
			  willClose: () => {
			    clearInterval(timerInterval)
			  }
			}).then((result) => {
			  if (result.dismiss === Swal.DismissReason.timer) {
			    $.LoadingOverlay("show", { textResizeFactor:0.3, text: "Mohon tunggu, hingga message ini hilang, dan anda melihat pesan berhasil.", image : ctx + "/images/loading-spinner.gif" });
			  }
			})
		}
	  }
	})
}

function onGetCopySuccess(response){
	isDone=false;
	$.LoadingOverlay("hide");
    Swal.fire({
	  title: 'Berhasil',
	  html: response.message,
	  icon: 'success',
	  allowEscapeKey: false,
	  allowOutsideClick: false,
	})
    clearInterval(timerInterval)
	var value=response.data[0];
	var dated=getDateForList((value.timestamp_modified==null?value.timestamp_added:value.timestamp_modified), 'datetime');
	
	$('#row-'+value.id).find('td')[6].innerHTML=dated.toString();
}
