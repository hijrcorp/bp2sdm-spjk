var path = ctx + '/restql';
var url = ctx + '/unit-auditi';
var urlReferensi = ctx + '/ref'

function init(){

	getSelectEselon1();
	getSelectPropinsi();
	getSelectStatusAuditi();
	getSimpegOrganisasiList();
	display();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-reset').click(function(e){
		clearForm();
		$("#search-form").trigger('reset');
	});
	
	$("#entry-form").submit(function() {
		save();
		return false;
	});
	
	$('#search-form').submit(function(e){
		display(1);
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('.single-date-picker').on('change', function(){
         var dateFirst = new Date($('[name=tanggal_mulai]').val());//"11/25/2017"
         var dateSecond = new Date($('[name=tanggal_selesai]').val());//"11/28/2017"
         // time difference
         var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());
         // days difference
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
         $('#jumlah_cuti').text(diffDays+" Hari");
         $('[name=jumlah_cuti]').val(diffDays+" Hari"); 
	});
	
	$('[name=id_propinsi]').on('change', function(){
		if($(this).find(':selected').data('kode')!=undefined) getSelectDistrik($(this).find(':selected').data('kode'));
	});
	$('[name=filter_id_propinsi]').on('change', function(){
		getSelectDistrik($(this).find(':selected').data('kode'));
	});
}

function importExcel(upload){
	$('#modal-form-table-msg').hide();
	if(upload!=undefined){
		var obj = new FormData();
		obj.append('file_excel', $('[name=file_excel]')[0].files[0]);
		if(upload==2) obj.append('mode', 'save');
		
		if($('[name=file_excel]')[0].files[0].size > 8871848){
			alert("file excel terlalu besar, minimal 8mb");
			return;
		}
		
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	    ajaxPOST(ctx + '/unit-auditi/preview-import',obj,'onImportExcelSuccess','onImportExcelError');
	}else{
		$('#modal-form-table').modal('show');
	}
}

function onImportExcelError(response){
	console.log(response);
	$.LoadingOverlay("hide");
	var new_msg = response.responseJSON.message.replace("[","* ").replace("]","");
	$('#modal-form-table-msg').html(new_msg.replaceAll(",","<br/>*"));
	$('#modal-form-table-msg').show();
	var table = $('#tbl-responden-modal-table').find('tbody');
	table.html("");
}

function onImportExcelSuccess(response){
	console.log(response);
	$.LoadingOverlay("hide");
	if(response.data=="save"){
		$('#modal-form-table').modal('hide');
		showAlertMessage(response.message, 1500);
		$('[name=file_excel]').val('').trigger('change');
		var table = $('#tbl-responden-modal-table').find('tbody');
		table.html("");
		display();
	}else{
		var table = $('#tbl-responden-modal-table').find('tbody');
		var row="";
		$.each(response.data, function(key){
			row+="<tr>";
				row+="<td>"+(key+1)+"</td>";
				row+="<td>"+"<strong>Kode</strong>:"+this.kode_unit_auditi+"<br/>"+this.nama_unit_auditi+"</td>";
				row+="<td>"+this.nama_propinsi+"</td>";
				row+="<td>"+this.nama_distrik+"</td>";
				row+="<td>"+this.nama_eselon1+"</td>";
				row+="<td>"+this.id_status_auditi_unit_auditi+"</td>";
				row+="<td>"+this.kode_simpeg_unit_auditi+"</td>";
				//row+="<td>"+this.unitAuditi.nama_unit_auditi+"</td>";
			row+="</tr>";
		});
		table.html(row);
		
		$('#btn-form-table').removeAttr('disabled');
	}
}


function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/unit_auditi/save';
	if($('[name=id]').val() != "") url = path+'/unit_auditi/save?append=false';
	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray()),
			dataMain : {
				"column" : [
					$('[name=nama]').val(),
					$('[name=id_eselon1]').val(), $('[name=id_propinsi]').val(), $('[name=id_status_auditi]').val()
				]
			}
		};
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form').modal('hide');
        		display();
        },
        error : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        		$('#modal-form-msg').show();
        },
    });
}

/*function save(){
	var obj = new FormData(document.querySelector('#entry-form'));
	
	if(obj.get('id') == "") obj.delete("id");
	
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    ajaxPOST(ctx + '/unit-auditi/save',obj,'onModalActionSuccess','onSaveError');
}*/

function onModalActionSuccess(response){
	$.LoadingOverlay("hide");
	if(response.data=="save"){
		showAlertMessage(response.message, 1500);
		$('[name=file_excel]').val('').trigger('change');
		var deff='<i class="fas fa-fw fa-save"></i> SUBMIT';
		$('#btn-submit').html(deff);
		$('#btn-submit').attr('disabled',false);
	}else{
		onGetListDetilSuccess(response);
	}
}

function onSaveError(response){
	$.LoadingOverlay("hide");
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').removeClass('d-none');
	$('#modal-form-msg').show();
}

function getSelectEselon1(){
	$('[name=filter_id_eselon1]').empty();
	$('[name=filter_id_eselon1]').append(new Option('Semua Eselon1', ''));
	//
	$('[name=id_eselon1]').empty();
	$('[name=id_eselon1]').append(new Option('Pilih Eselon1', ''));
	
	ajaxGET(path + '/eselon1?filter=kode_simpeg.not(null)','onGetSelectEselon1','onGetSelectError');
}
function onGetSelectEselon1(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_eselon1]').append(new Option(value.nama, value.id));
		$('[name=id_eselon1]').append(new Option(value.nama, value.id));
	});
}

function getSelectDistrik(kode){
	$('[name=filter_id_distrik]').empty();
	$('[name=filter_id_distrik]').append(new Option('Semua Kota/Kabupaten', ''));
	
	$('[name=id_distrik]').empty();
	$('[name=id_distrik]').append(new Option('Pilih Kota/Kabupaten', ''));
	//?filter=kode_simpeg.not(null)
	ajaxGET(path + '/distrik?filter=kode_propinsi.eq('+kode+')','onGetSelectDistrik','onGetSelectError');
}
function onGetSelectDistrik(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_distrik]').append(new Option(value.nama, value.id));
		$('[name=id_distrik]').append(new Option(value.nama, value.id));
	});
	if($('[name=id_distrik]').data('id')!="") $('[name=id_distrik]').val($('[name=id_distrik]').data('id'))
}


function getSelectPropinsi(){
	$('[name=filter_id_propinsi]').empty();
	$('[name=filter_id_propinsi]').append(new Option('Semua Propinsi', ''));
	//
	$('[name=id_propinsi]').empty();
	$('[name=id_propinsi]').append(new Option('Pilih Propinsi', ''));
	
	ajaxGET(path + '/propinsi','onGetSelectPropinsi','onGetSelectError');
}
function onGetSelectPropinsi(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_propinsi]').append("<option data-kode='"+value.kode+"' value='"+value.id+"'>"+value.nama+"</option>");
		$('[name=id_propinsi]').append("<option data-kode='"+value.kode+"' value='"+value.id+"'>"+value.nama+"</option>");
	});
}

function getSelectStatusAuditi(){
	$('[name=filter_id_status_auditi]').empty();
	$('[name=filter_id_status_auditi]').append(new Option('Semua Status Auditi', ''));
	//
	$('[name=id_status_auditi]').empty();
	$('[name=id_status_auditi]').append(new Option('Pilih Status Auditi', ''));
	
	ajaxGET(path + '/status_auditi','onGetSelectStatusAuditi','onGetSelectError');
}
function onGetSelectStatusAuditi(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_status_auditi]').append(new Option(value.nama, value.id));
		$('[name=id_status_auditi]').append(new Option(value.nama, value.id));
	});
}
//

/*function getStatusAuditiList(){
	$.getJSON(url+'statusAuditi/'+"?token="+token, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("#statusAuditi").append(new Option(value.namaStatusAuditi, value.id));
			});
		}else{
			alert("Connection error");
		}
	});
}*/
function getKelompokUPTList(){
	$.getJSON(urlReferensi+'kelompokUPT/', function(response) {
		if (response.status == 200) {
			$.each(response.data, function(key, value) {
				$("#kelompokUPT").append(new Option(value.namaKelompokUPT, value.id));
			});
		}
	});
}

function getSimpegOrganisasiList(){
	$.getJSON(path+'/simpeg_view_organisasi?limit=10000', function(response) {
		if (response.code == 200) {
			listSimpeg = response.data;
			$("[name=kode_simpeg").append(new Option('000000000000000: Tidak memiliki kode organisasi SIMPEG', '000000000000000'));
			$.each(response.data, function(key, value) {
				$("[name=kode_simpeg").append(new Option(value.kode + ': ' + value.nama, value.kode));
			});
		}
	});
}
function getSelectStatusAuditi(){
	$('[name=filter_id_status_auditi]').empty();
	$('[name=filter_id_status_auditi]').append(new Option('Semua Status Auditi', ''));
	//
	$('[name=id_status_auditi]').empty();
	$('[name=id_status_auditi]').append(new Option('Pilih Status Auditi', ''));
	
	ajaxGET(path + '/status_auditi','onGetSelectStatusAuditi','onGetSelectError');
}
function onGetSelectStatusAuditi(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_id_status_auditi]').append(new Option(value.nama, value.id));
		$('[name=id_status_auditi]').append(new Option(value.nama, value.id));
	});
}
//
function display(page=1, params=''){
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(page ==1){ //params == '' || params != $( "#search-form" ).serialize() || 
		tbody.text('');
	}
	var clause=[];
	if($('[name=filter_id_eselon1]').val() != '') clause.push('id_eselon1.eq(\''+$('[name=filter_id_eselon1]').val()+'\')');
	if($('[name=filter_id_propinsi]').val() != '') clause.push('id_propinsi.eq(\''+$('[name=filter_id_propinsi]').val()+'\')');
	if($('[name=filter_id_distrik]').val() != '' && $('[name=filter_id_distrik]').val() != undefined) clause.push('id_distrik.eq(\''+$('[name=filter_id_distrik]').val()+'\')');
	if($('[name=filter_kode]').val() != '') clause.push('kode.like(\''+$('[name=filter_kode]').val()+'\')');
	if($('[name=filter_nama]').val() != '') clause.push('nama.like(\''+$('[name=filter_nama]').val()+'\')');
	if($('[name=filter_id_status_auditi]').val() != '') clause.push('id_status_auditi.eq(\''+$('[name=filter_id_status_auditi]').val()+'\')');
	
	$.each(clause, function(key, value){
		if(key==0) clause[key]=this.replace(",","");
	});
	
	console.log(clause.toString());
	
	if(clause.length > 0) params="filter="+clause.toString();
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=eselon1,propinsi,distrik,status_auditi";
	ajaxGET(path + '/unit_auditi?page='+page+'&'+params+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');

}

function onGetListDetilSuccess(response){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += '<tr class="data-row" id="row-'+value.id+'">';
			row += '<td>'+(num)+'</td>';
			row += '<td> '+value.kode +' </td>';
			row += '<td> '+value.nama +' </td>';
			row += '<td> <strong>Propinsi:</strong>'+value.propinsi.nama+'<br/><strong>Kab:</strong>'+(value.distrik.nama!=undefined?value.distrik.nama:"-")+' </td>';
			row += '<td> '+value.eselon1.nama +' </td>';
			row += '<td> '+value.status_auditi.nama +' </td>';
			//row += '<td> '+(value.kode_simpeg!=undefined?value.kode_simpeg:"-") +' </td>';
			row += '<td class="text-center">';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
			row += '</td>';
		row += '</tr>';
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		//var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		var query=select+"&fetch=penggajian_detil.select(id,id_penggajian,jenis,komponen,jumlah,keterangan)";//akun_bank.limit(1)
		query=""
		ajaxGET(path + '/unit_auditi/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		startLoading('btn-modal-confirm-yes');
		//if(selected_action=="delete")  cascade="&cascade=penggajian_detil";
		var query="";
		ajaxPOST(url +"/"+id+'/'+selected_action+'?action='+selected_action,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'update-true' || selected_action == 'update-false'){
		if(selected_action=="update-true")
			updateStatus(0);
		else if(selected_action=="update-false")
			updateStatus(1);
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('.modal table').find('tbody').html('');
	$('[name=id_propinsi]').val('').trigger('change');
	$('[name=id_eselon1]').val('').trigger('change');
	$('[name=kode_simpeg]').val('').trigger('change');
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=kode]').val(value.kode);
	$('[name=nama]').val(value.nama);
	$('[name=id_propinsi]').val(value.id_propinsi).trigger('change');
	$('[name=id_eselon1]').val(value.id_eselon1).trigger('change');
	$('[name=id_status_auditi]').val(value.id_status_auditi).trigger('change');
	
	//$('[name=id_distrik]').val(value.id_distrik).trigger('change');
	$('[name=id_distrik]').data('id',value.id_distrik);
	
	$('[name=kode_simpeg]').val(value.kode_simpeg).trigger('change');
	
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display();
	
	// Update data ke existing tabel
	/*if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}*/
	
	$.LoadingOverlay("hide");
	stopLoading('btn-save', 'Simpan');
	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


