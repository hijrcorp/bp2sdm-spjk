var ws; 

function document_ready(){
	if ("WebSocket" in window) {
		connect();
	}else{
		alert("WebSocket NOT supported by your Browser!");
	}
}

function connect(){
	//ws = new WebSocket(document.URL.replace('http', 'ws') + "websocket");
	ws = new ReconnectingWebSocket("ws://"+location.host + "/websocket", null, {debug: true, reconnectInterval: 3000});
	
	
	//console.log(ws);
	
	ws.onopen = function(e){ websocket_onopen(e); };
	ws.onmessage = function(e){ websocket_onmessage(e); };
	ws.onclose = function(e){ websocket_onclose(e); }; 
	ws.onerror = function(e){ websocket_onerror(e); }; 
}

function websocket_onclose(e){	
	if($('#state').text().indexOf('proses update') < 0) {
		$('#state').html('Gagal terhubung ke server, <a href="#" onclick="ws.send(\'#check\')">coba lagi</a>');
		$('#imgstate').attr("src","img/hijr-dis.png");
	}
}

function websocket_onerror(e){
	
}

function websocket_onmessage(e){	
	
	
	if(e.data.indexOf('#reload') >= 0) {
		refresh();
	}else{
		if($('#state').text().indexOf('proses update') < 0) {
			if(e.data.indexOf('#connected') >= 0 && $('#imgstate').attr("src").indexOf('hijr.png') < 0) {
				//alert(e.data);
				if(e.data.indexOf('#update') >= 0){
					$('#state').html('Ada versi terbaru, <a href="#" onclick="sendMessage(\'#update\')">update sekarang</a>');
				}else{
					$('#state').html('Terhubung ke server');	
				}
				
				$('#imgstate').attr("src","img/hijr.png");
			}
			if(e.data.indexOf('#disconnected') >= 0) {
				$('#state').html('Gagal terhubung ke server, <a href="#" onclick="sendMessage(\'#check\')">coba lagi</a>');
				$('#imgstate').attr("src","img/hijr-dis.png");
			}
		}
	}
	
}

function refresh(){
	$("#txtSearch").val("#done");
	$("#frmSearch").submit();
}

function websocket_onopen(e){
	sendMessage('#check');
}


function sendMessage(msg){
	if(msg.indexOf('#update') >= 0) {
		$('#state').text('Sedang proses update, silahkan tunggu...');
		
	}
	if(msg.indexOf('#check') >= 0) {
		$('#state').text('Coba hubungi server..');
		
	}
	if(msg.indexOf('#exit') >= 0) {
		$('#state').text('Keluar aplikasi . . . ');
		
	}
	ws.send(msg);
}