var path = ctx + '/restql';
var url = ctx + "/pertanyaan";

var param = "";
var selected_id=($.urlParam('id')==null?"":$.urlParam('id'));
var selected_id_unit_kompetensi='';
var selected_action='';

var appendIds = [];
function init() {
	getSelectBidangTeknis();
	$('#btn-submit').click(function(e){
		if($('[name=file_excel]')[0].files.length==0){
			return alert("File Excel Belum dipilih.");
		}
		save('save');
	});
	
	$('#entry-form-pertanyaan').submit(function(e){
		save();
		e.preventDefault();
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});

	$(".button-minus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepDown();
	});
	$(".button-plus").click(function(){
		document.getElementsByName("jumlah_jawaban")[0].stepUp();
	});
	$('[name=file_excel]').change(function(){
		save();
	});
	$('[name=start_read_row]').change(function(){
		if(this.value=='') this.value=3;
		if(this.value<3) this.value=3;
		if(this.value!='') $('[name=file_excel]').attr('disabled',false);
	});
	//$('[name=file_excel]').attr('disabled',true);
}
function display(){
	console.log("none");
}

function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	//$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		var params='onclick="getSelectBidangTeknis('+value.id+');"';
		$('#nav-tab').append('<a data-id="'+value.id+'" '+params+' class="nav-item nav-link '+(key==0?"active":"")+'" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">'+value.kode+'</a>');
		
	});
	getSelectBidangTeknis($('#nav-tab .active').data('id'));
}

function getSelectBidangTeknis(id){
	$('[name=id_bidang_teknis]').empty();
	$('[name=id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	$('[name=filter_id_bidang_teknis]').empty();
	//
//	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="";
	var page=1,params="",qry="";
	ajaxGET(path + '/bidang_teknis?'+params,'onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_bidang_teknis]').append(new Option(value.nama, value.id));
		$('[name=filter_id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	//display();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function save(mode="preview"){
	var obj = new FormData(document.querySelector('#entry-form-pertanyaan'));
	obj.append("mode", mode);

	if($('[name=file_excel]')[0].files[0].size > 8871848){
		alert("file excel terlalu besar, minimal 8mb");
		return;
	}
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    ajaxPOST(ctx + '/pertanyaan/preview-import',obj,'onModalActionSuccess','onSaveError');
}
function onModalActionSuccess(response){
	$.LoadingOverlay("hide");
	if(response.data=="save"){
		showAlertMessage(response.message, 1500);
		$('[name=file_excel]').val('').trigger('change');
		var deff='<i class="fas fa-fw fa-save"></i> SUBMIT';
		$('#btn-submit').html(deff);
		$('#btn-submit').attr('disabled',false);
	}else{
		onGetListDetilSuccess(response);
	}
}
function onSaveError(response){
	console.log(response);
	onActionError(response, "err");
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();
	$('#form-pilihan-jawaban').html('');
	$('[name=id]').attr('disabled',true);
}


function reset(){
	cleanMessage('msg');
	//selected_id = '';
	$('[name=id]').attr('disabled',true);
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#entry-form').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

function doRemove(id){
	$("#progressBar1").show();
	if(selected_action == 'delete' ){
		doAction(selected_id, selected_action, 1);
	}
}
//batas

function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}
function onGetListDetilSuccess(response, mode){
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	var tabbody = $("#list-kode-tab");
	var navTab = $('#nav-tabContent');
	
	var row = "";
	var rowright = "";
	//render tab
	var mytab="";
	var kunci_jawaban=["A","B","C","D","E"];
	$.each(response.data,function(key,value){
		if(value.judul=="DUPLICATED"){
			rowright += '<a class="list-group-item list-group-item-action" data-toggle="list" href="#list-tab-'+key+'">'+value.kode+'<i class="fas fa-exclamation-circle float-right text-warning"></i></a>';
		}else{
			rowright += '<a class="list-group-item list-group-item-action" data-toggle="list" href="#list-tab-'+key+'">'+value.kode+'<i class="fas fa-check-circle float-right text-success"></i></a>';
		}
		mytab +='<div class="tab-pane fade" id="list-tab-'+key+'">'+
				'<div id="table-card-body">';
		$.each(value.listPertanyaan, function(idx, val){
		mytab +='<div class="data-row card mb-3" id="row-'+val.id+'">'+
		'        <div class="card-body">'+
		'            <strong class="d-nones">'+(idx+1)+'.</strong>'+val.isi+
		'            <button onclick="showRemarks(\'\')" type="button" class="btn m-0 p-0 d-none"><i class="fas fa-question-circle text-info"></i></button><strong class="d-none">(undefined)</strong>'+
		'        </div>'+
		'        <div class="card-footer">';
			$.each(val.listPertanyaanJawaban, function(idx2, val2){
				if(val.kunci_jawaban==val2.order){
					mytab +='<label class="form-check-label bg-success" for="radio-1596552375875344-1596552375886749"> <strong>'+kunci_jawaban[val2.order-1]+'</strong>. '+val2.isi+' </label><br/>';
				}else{
					mytab +='<label class="form-check-label" for="radio-1596552375875344-1596552375886749"> <strong>'+kunci_jawaban[val2.order-1]+'</strong>. '+val2.isi+' </label><br/>';
				}
			});
		mytab +='</div>'+
		'    </div>';
		});
		mytab += '</div>';
		mytab += '</div>';
			
	});
	navTab.html(mytab);
	tabbody.html(rowright);
}

//function onGetListDetilSuccessOLD(response, mode){
//	//console.log(response.data);
//	$('#loading-row').remove();
//	$('#no-data-row').remove();
//	var tbody = $("#tbl-data").find('tbody');
//	var tabbody = $("#list-kode-tab");
//	var navTab = $('#nav-tabContent');
//	
//	var row = "";
//	var rowright = "";
//	//render tab
//	var mytab="";
//	var kunci_jawaban=["A","B","C","D"];
//	$.each(response.data,function(key,value){
//		rowright += '<a class="list-group-item list-group-item-action" data-toggle="list" href="#list-tab-'+key+'">'+value.kode+'</a>'
//		mytab += '<div class="tab-pane fade" id="list-tab-'+key+'">'+
//		'    <div class="table-responsive">'+
//		'        <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">'+
//		'            <thead class="thead-dark">'+
//		'                <tr>'+
//		'                    <th width="1%">No</th>'+
//		'                    <th >Kode</th>'+
//		'                    <th width="60%">Pertanyaan</th>'+
//		'                    <th width="10%">Kunci Jawaban</th>'+
//		'                    <th width="10%">Tipe Soal</th>'+
//		'                </tr>'+
//		'            </thead>';
//		mytab +='<tbody>';
//		$.each(value.listPertanyaan, function(idx, val){
//			mytab += '<tr class="data-row" id="row-'+val.id+'">';
//				mytab += '<td>'+(idx+1)+'</td>';
//				mytab += '<td class="">'+val.kode+'</td>';
//				mytab += '<td class="">'+val.isi+'</td>';
//				mytab += '<td class="text-center">'+kunci_jawaban[val.kunci_jawaban-1]+'</td>';
//				mytab += '<td class="">'+val.tipe_soal+'</td>';
//			mytab += '</tr>';
//		});
//		mytab +='</tbody>'+
//		'        </table>'+
//		'    </div>'+
//		'</div>';
//			
//	});
//	navTab.html(mytab);
//	tabbody.html(rowright);
//}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select;//+"&fetch=pertanyaan_jawaban";//.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi) //akun_bank.limit(1)
//		ajaxPOST(url+'/'+id+'/'+selected_action,{},'onPrepareModalActionSuccess','onActionError');
		//var query=select+"&fetch=pertanyaan_jawaban";//.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi) //akun_bank.limit(1)
		ajaxGET(url + '/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=jabatan';
		query="";
		ajaxPOST(path + '/pertanyaan/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

/*function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	//display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}*/

function fillFormValue(value){
	clearForm();
	$('[name=id]').attr('disabled',false);
	$('[name=id]').val(value.id);
	$('[name=kode]').val(value.kode);
	$('[name=bobot]').val(value.bobot);
	$('[name=isi]').val(value.isi);
	$('[name=kunci_jawaban]').val(value.kunci_jawaban);
	$('[name=tipe_soal]').val(value.tipe_soal);
	//

	$('[name=jumlah_jawaban]').val(value.listPertanyaanJawaban.length);
	$('[name=jumlah_jawaban]').attr("min", value.listPertanyaanJawaban.length);
	
	var i=1;
	$.each(value.listPertanyaanJawaban, function(key){
		temp = '<div class="input-group mb-3">'+
		'    <div class="input-group-prepend">'+
		'        <span class="input-group-text">'+i+'</span>'+
		'    </div>'+
		'    <div class="input-group-prepend">'+
		'        <input class="form-control" name="bobot_jawaban" type="number" value="'+this.bobot+'" placeholder="Tentukan Nilai Bobot">'+
		'    </div>'+
		'    <input type="text" class="form-control d-none" name="id_jawaban" value="'+this.id+'">'+
		'    <input type="text" class="form-control" name="isi_jawaban" placeholder="Pilihan Jawaban ke '+(i)+'" autocomplete="off" value="'+this.isi+'">'+
		''+
		'</div>';
		$('#form-pilihan-jawaban').append(temp);
		/*$('#form-pilihan-jawaban').append('<div class="input-group" style="margin-bottom:10px">'+
		'						  <input class="form-control hidden" name="order_jawaban" type="text" value="'+this.order_pernyataan_jawaban+'" readonly>'+
		'						  <span class="input-group-addon " id="basic-addon'+this.order_pernyataan_jawaban+'">'+this.order_pernyataan_jawaban+'</span>'+
		'						<input class="form-control hidden" name="id_jawaban" type="text" value="'+this.id_pernyataan_jawaban+'" readonly>'+
		'						  <span class="input-group-addon hidden" id="basic-addon'+this.order_pernyataan_jawaban+'">-</span>'+
		'						  <input name="isi_jawaban" type="text" class="form-control" placeholder="Pilihan jawaban ke '+(this.order_pernyataan_jawaban)+'" autocomplete="off" value="'+this.isi_pernyataan_jawaban+'">'+
		'						</div>');*/
		i++;
	});
	$('#btn-create-form').data('obj', value.pertanyaan_jawaban);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}


function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';
	row += '<td class="">'+value.kode+'</td>';
	row += '<td class="">'+value.isi+'</td>';
	row += '<td class="">'+value.listPertanyaanJawaban.length+'</td>';
	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function createFormJawaban(){
	var temp="";
	for(var i=1; i <= $('[name=jumlah_jawaban]').val(); i++){
		/*temp += '<div class="input-group" style="margin-bottom:10px">'+
		'						  <input class="form-control hidden" name="order_jawaban" type="text" value="'+i+'">'+
		'						  <span class="input-group-addon" id="basic-addon1">'+i+'</span>'+
		'						  <input class="form-control hidden" name="id_jawaban" type="text" value="">'+
		'						  <span class="input-group-addon hidden" id="basic-addon1">-</span>'+
		'						  <input name="isi_jawaban" type="text" class="form-control" placeholder="Pilihan jawaban ke '+(i)+'" autocomplete="off" value="">'+
		'						</div>';*/
		temp += '<div class="input-group mb-3">'+
		'    <div class="input-group-prepend">'+
		'        <span class="input-group-text">'+i+'</span>'+
		'    </div>'+
		'    <div class="input-group-prepend">'+
		'        <input class="form-control" name="bobot_jawaban" type="number" placeholder="Tentukan Nilai Bobot">'+
		'    </div>'+
		'    <input type="text" class="form-control d-none" name="id_jawaban">'+
		'    <input type="text" class="form-control" name="isi_jawaban" placeholder="Pilihan Jawaban ke '+(i)+'" autocomplete="off">'+
		''+
		'</div>';
	}
	$('#form-pilihan-jawaban').html(temp);
	
	if($('#btn-create-form').data('obj').length>0){
		$.each($('#btn-create-form').data('obj'), function(key){
			$('[name=bobot_jawaban]')[key].value=this.bobot;
			$('[name=id_jawaban]')[key].value=this.id;
			$('[name=isi_jawaban]')[key].value=this.isi;
		});
	}
	
}