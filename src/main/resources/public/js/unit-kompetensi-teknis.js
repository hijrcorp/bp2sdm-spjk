var path = ctx + '/restql';

var param = "";
var selected_id='';
var selected_action='';

var appendIds = [];
function init() {
	
	display();
	getSelectKelompokJabatan();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
		$('[name=id_kelompok_jabatan]').val($('#nav-tab .active').data('id'));
		
		getSelectFungsiUtama($('#nav-tab .active').data('id'));
		$('[name=id_fungsi_utama]').val('').trigger('change');
		//getSelectBidangTeknis($('#nav-tab .active').data('id'));
		//getSelectKelompokTeknis($('#nav-tab .active').data('id'));
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});

	$('#search-form').submit(function(e){
		var params='kode.slike \''+$('[name=filter_keyword]').val()+'\','+'id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'")';
		if($('[name=filter_keyword]')=="") params="";
		display(1, params);
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
	
	$('[name=id_jenjang_jabatan]').on('change', function(){
		console.log("sebelum");
		console.log(this.value);
		var newValue = this.value.split("/");
		console.log("sesudah");
		console.log(newValue);
		$('[name=id_tingkat_jenjang]').val(newValue[1]);
	});
}

var sel_id;
function getSelectBidangTeknis(id, sel_id){
	sel_id=sel_id;
	$('[name=id_bidang_teknis]').empty();
	$('[name=id_bidang_teknis]').append(new Option('Pilih Bidang Teknis', ''));
	//
	ajaxGET(path + '/bidang_teknis?filter=id_kelompok_jabatan.eq("'+id+'")','onGetSelectBidangTeknis','onGetSelectError');
}
function onGetSelectBidangTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_bidang_teknis]').append(new Option(value.nama, value.id));
	});
	if(sel_id!=undefined) $('[name=id_bidang_teknis]').val(sel_id);
}

function getSelectKelompokJabatan(){
	$('[name=id_kelompok_jabatan]').empty();
	$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(path + '/kelompok_jabatan?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokJabatan','onGetSelectError');
}
function onGetSelectKelompokJabatan(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_jabatan]').append(new Option(value.kode, value.id));
		var params='onclick="display(1,\''+'id_kelompok_jabatan.eq'+"("+ value.id +")"+'\');getSelectFungsiUtama(\''+ value.id +'\');"';
		$('#nav-tab').append('<a data-id="'+value.id+'" '+params+' class="nav-item nav-link '+(key==0?"active":"")+'" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">'+value.kode+'</a>');
		
	});
}

function getSelectFungsiUtama(id){
	$('[name=id_fungsi_utama]').empty();
	$('[name=id_fungsi_utama]').append(new Option('Pilih Fungsi Utama', ''));
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="order=kode";
	ajaxGET(path + '/fungsi_utama?page='+page+'	&'+params+'&'+qry,'onGetSelectFungsiUtama','onGetSelectError');
}
function onGetSelectFungsiUtama(response){
	$.each(response.data, function(key, value) {
		$('[name=id_fungsi_utama]').append(new Option(value.kode+":"+value.judul, value.id));
	});
}

var sel_id;
function getSelectKelompokTeknis(id, sel_id){
	sel_id=sel_id;
	$('[name=id_kelompok_teknis]').empty();
	$('[name=id_kelompok_teknis]').append(new Option('Pilih Kelompok Teknis', ''));
	var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')",qry="";
	ajaxGET(path + '/kelompok_teknis?page='+page+'	&'+params+'&'+qry,'onGetSelectKelompokTeknis','onGetSelectError');
}
function onGetSelectKelompokTeknis(response){
	$.each(response.data, function(key, value) {
		$('[name=id_kelompok_teknis]').append(new Option(value.nama, value.id));
	});
	if(sel_id!=undefined) $('[name=id_kelompok_teknis]').val(sel_id);
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		//param = $('#column').data('id')+"="+$('#txtsearch').val();//+'&nama='+jenis;
		//console.log(param);
		display(1, 'filter=');
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
		param = "";
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modal-form").modal('show');
}

function save(){
	//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = path+'/unit_kompetensi_teknis/save?append=false&join=kelompok_jabatan,fungsi_utama';
	
	$('[name=id_kelompok_jabatan]').prop('disabled',false);
	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray()),
			dataMain : {
				"column" : [
					$('[name=kode]').val(),
					$('[name=id_fungsi_utama]').val()
				]
			}
	};
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		//$.LoadingOverlay("hide");
        		//$('#modal-form').modal('hide');
        		//display();
        	onModalActionSuccess(response);
	    	$('[name=id_kelompok_jabatan]').prop('disabled',true);
        },
        error : function (response) {
        		//$.LoadingOverlay("hide");
	    		$('#modal-form-msg').text(response.responseJSON.message);
	    		$('#modal-form-msg').removeClass('d-none');
	    		$('#modal-form-msg').show();
	    		$('[name=id_kelompok_jabatan]').prop('disabled',true);
        },
    });
}

function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	addAlert('msg', "alert-danger", "kesalahan dari server");
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$("#progressBar").hide();
}


function reset(){
	cleanMessage('msg');
	selected_id = '';
//	$('#id').val('0');
//	$('#kelompokUPT').val(0);
//	$('#uraianTUSI').val('');
	$('#entry-form').trigger('reset');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display();
}

//batas
function remove(id, confirm = 0){
	$("#progressBar1").show();
	$("#btnRemove").data("id",id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(url +id,'getRemoveDetailSuccess','getRemoveDetailError');
	else ajaxPOST(url + id+'/delete',{},'onRemoveSuccess','onRemoveError');
}

function getRemoveDetailSuccess(response) {
	var value = response.data;
	console.log(value);
	$('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data ini?</p>')
	$('#modal-remove').modal('show');
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function onRemoveSuccess(response){
//	$('#row-' + selected_id).remove();
//	if($('.data-row').length == 0) display(1, '', 0);
	refresh();
	$('#modal-remove').modal('hide');
}
//batas
function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */

function display(page=1, params=''){
	console.log(params);
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	// disable kondisi ini kalo pake metode refresh semua isi table
	//if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
	if(page ==1){
		tbody.text('');
	}
	
	if($('#column').data('id') != ''){
		//params+= $('#column').data('id')+'.slike \''+$('#txtsearch').val()+'\';';
	}
	
//	if($('[name=filter_nama]').val() == '' && $('[name=filter_nomor]').val() == '' 
//		&& $('[name=filter_id_jabatan]').val() == '' && $('[name=filter_alamat_rumah]').val() == '' 
//			&& $('[name=filter_id_propinsi]').val() == ''){
//		params='';
//	}
	// "%'+$('[name=filter]').val()+'%"
	if(params=='') params+='id_kelompok_jabatan.eq(1)';
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	var qry="join=kelompok_jabatan,fungsi_utama";
	ajaxGET(path + '/unit_kompetensi_teknis?page='+page+'&filter='+params+'&'+qry,'onGetListDetilSuccess','onGetListDetilError');
}

function onGetListDetilSuccess(response){
	var kelompokjb=$('#nav-tab .active').data('id');
	//console.log(response.data);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		row += renderRow(value, num);
		num++;
	});
	
	//tbody.html(row);
	if(response.next_more){
		//
		var params='id_kelompok_jabatan.eq('+kelompokjb+')';
		if($('[name=filter_keyword]')=="") params="";
		//display(1, params);
		//
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display(\''+response.next_page_number+'\',\''+params+'\')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		//$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var select = "select=id,nomor,nama,jenis_kelamin,tanggal_penerimaan";
		select="";
		var query=select+"&join=propinsi,jabatan&fetch=anggota_keluarga.select(id,nomor_induk,nama,tanggal_lahir,status,status_verifikasi),akun_bank.select(id,nama_bank,nomor_rekening)";//akun_bank.limit(1)
		query="";
		ajaxGET(path + '/unit_kompetensi_teknis/'+id+'?'+query+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		//startLoading('btn-modal-confirm-yes');
		var cascade="";
		if(selected_action=="delete")  cascade="&cascade=akun_bank,anggota_keluarga";
		cascade="";
		var query='?join=jabatan';
		query="";
		ajaxPOST(path + '/unit_kompetensi_teknis/'+id+'/'+selected_action+query+cascade,{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
		$("#btnRemove").data("id",selected_id);
		$("#progressBar1").hide();
	}
}

function onModalActionSuccess(response){
	console.log(response);
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}

	// Kalo mau refresh semua data yang tampil di table

	var params='id_kelompok_jabatan.eq("'+$('#nav-tab .active').data('id')+'")';
	display(1, params);
//	$.LoadingOverlay("hide");
//	stopLoading('btn-save', 'Simpan');
//	stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	
	//$('[name=id_bidang_teknis]').val(value.id_bidang_teknis);
	//$('[name=id_kelompok_teknis]').val(value.id_kelompok_teknis);
	$('[name=kode]').val(value.kode);
	$('[name=judul]').val(value.judul);
	$('[name=jenis_kompetensi]').val(value.jenis_kompetensi);
	$('[name=id_kelompok_jabatan]').val(value.id_kelompok_jabatan);
	$('[name=id_fungsi_utama]').val(value.id_fungsi_utama).trigger('change');
	getSelectBidangTeknis(value.id_kelompok_jabatan, value.id_bidang_teknis);
	getSelectKelompokTeknis(value.id_kelompok_jabatan, value.id_kelompok_teknis);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function almghty(e){
	console.log(e.dataset.idKompetensiTeknis);
	console.log(e.dataset.id);
	//window.location.href="form-pertanyaan?id="+e.dataset.id;
	window.open("form-pertanyaan?id="+e.dataset.idKompetensiTeknis, '_blank');
	console.log(e);
}

function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';
	/*row += '<td class="">'+value.kelompok_jabatan.kode+'</td>';
	row += '<td class="">'+value.bidang_teknis.nama+'</td>';
	row += '<td class="">'+value.kelompok_teknis.nama+'</td>';*/
	row += '<td class="">'+'<a href="form-pertanyaan?id='+value.id+'&jenis=teknis">'+value.kode+'</a></td>';
	row += '<td class="">'+value.judul+'</td>';
	row += '<td class="">'+value.fungsi_utama.kode+":"+value.fungsi_utama.judul+'</td>';


	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}