# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.11-MariaDB)
# Database: bp2sdmdb
# Generation Time: 2020-08-27 00:47:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table spjk_progress_peserta_ujian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spjk_progress_peserta_ujian`;

CREATE TABLE `spjk_progress_peserta_ujian` (
  `id_progress_peserta_ujian` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kelompok_jabatan_progress_peserta_ujian` int(11) NOT NULL,
  `jumlah_peserta_berjalan_progress_peserta_ujian` int(10) NOT NULL DEFAULT 0,
  `total_peserta_progress_peserta_ujian` int(10) NOT NULL DEFAULT 0,
  `kode_sesi_progress_peserta_ujian` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_progress_peserta_ujian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `spjk_progress_peserta_ujian` WRITE;
/*!40000 ALTER TABLE `spjk_progress_peserta_ujian` DISABLE KEYS */;

INSERT INTO `spjk_progress_peserta_ujian` (`id_progress_peserta_ujian`, `id_kelompok_jabatan_progress_peserta_ujian`, `jumlah_peserta_berjalan_progress_peserta_ujian`, `total_peserta_progress_peserta_ujian`, `kode_sesi_progress_peserta_ujian`)
VALUES
	(1,1,93,300,'TES_2020'),
	(2,2,99,200,'TES_2020'),
	(3,3,99,300,'TES_2020'),
	(4,4,99,300,'TES_2020'),
	(5,5,99,300,'TES_2020');

/*!40000 ALTER TABLE `spjk_progress_peserta_ujian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table spjk_rata_rata_kompetensi_individu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spjk_rata_rata_kompetensi_individu`;

CREATE TABLE `spjk_rata_rata_kompetensi_individu` (
  `id_rata_rata_kompetensi_individu` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user_responden_rata_rata_kompetensi_individu` varchar(100) DEFAULT NULL,
  `nip_rata_rata_kompetensi_individu` varchar(100) DEFAULT NULL,
  `nama_rata_rata_kompetensi_individu` varchar(100) DEFAULT NULL,
  `golongan_rata_rata_kompetensi_individu` varchar(3) DEFAULT NULL COMMENT 'I,II,III',
  `id_eselon1_rata_rata_kompetensi_individu` int(11) DEFAULT NULL,
  `id_unit_auditi_rata_rata_kompetensi_individu` int(11) DEFAULT NULL,
  `id_kelompok_jabatan_rata_rata_kompetensi_individu` int(11) DEFAULT NULL,
  `id_jenjang_jabatan_rata_rata_kompetensi_individu` int(11) DEFAULT NULL,
  `nilai_kompetensi_manajerial_rata_rata_kompetensi_individu` int(10) DEFAULT NULL,
  `nilai_kompetensi_sosiokultural_rata_rata_kompetensi_individu` int(10) DEFAULT NULL,
  `nilai_kompetensi_teknis_rata_rata_kompetensi_individu` int(10) DEFAULT NULL,
  `kode_sesi_rata_rata_kompetensi_individu` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_rata_rata_kompetensi_individu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `spjk_rata_rata_kompetensi_individu` WRITE;
/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_individu` DISABLE KEYS */;

INSERT INTO `spjk_rata_rata_kompetensi_individu` (`id_rata_rata_kompetensi_individu`, `id_user_responden_rata_rata_kompetensi_individu`, `nip_rata_rata_kompetensi_individu`, `nama_rata_rata_kompetensi_individu`, `golongan_rata_rata_kompetensi_individu`, `id_eselon1_rata_rata_kompetensi_individu`, `id_unit_auditi_rata_rata_kompetensi_individu`, `id_kelompok_jabatan_rata_rata_kompetensi_individu`, `id_jenjang_jabatan_rata_rata_kompetensi_individu`, `nilai_kompetensi_manajerial_rata_rata_kompetensi_individu`, `nilai_kompetensi_sosiokultural_rata_rata_kompetensi_individu`, `nilai_kompetensi_teknis_rata_rata_kompetensi_individu`, `kode_sesi_rata_rata_kompetensi_individu`)
VALUES
	(1,'1596874349819684','196601221989021001','Engkos Kosasih, S.Hut.','IV',12,5,3,1,3,3,80,'TES_2020'),
	(2,'1596874661937616','196306071994031003','Suwanto','III',7,1,4,2,4,4,80,'TES_2020'),
	(3,'1597207826843111','196601221989021001','Engkos Kosasih, S.Hut.','IV',12,5,5,3,2,3,80,'TES_2020');

/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_individu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table spjk_rata_rata_kompetensi_individu_copy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spjk_rata_rata_kompetensi_individu_copy`;

CREATE TABLE `spjk_rata_rata_kompetensi_individu_copy` (
  `id_rata_rata_kompetensi_individu` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user_responden_rata_rata_kompetensi_individu` int(11) DEFAULT NULL,
  `nama_rata_rata_kompetensi_individu` int(11) DEFAULT NULL COMMENT 'X',
  `nip_rata_rata_kompetensi_individu` int(11) DEFAULT NULL COMMENT 'X',
  `id_kelompok_jabatan_rata_rata_kompetensi_individu` int(11) DEFAULT NULL,
  `id_jenjang_jabatan_rata_rata_kompetensi_individu` int(11) DEFAULT NULL,
  `nilai_standard_rata_rata_kompetensi_individu` int(10) DEFAULT NULL,
  `nilai_pemetaan_rata_rata_kompetensi_individu` int(10) DEFAULT NULL,
  `kode_sesi_rata_rata_kompetensi_individu` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_rata_rata_kompetensi_individu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table spjk_rata_rata_kompetensi_manajerial
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spjk_rata_rata_kompetensi_manajerial`;

CREATE TABLE `spjk_rata_rata_kompetensi_manajerial` (
  `id_rata_rata_kompetensi_manajerial` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kelompok_jabatan_rata_rata_kompetensi_manajerial` int(11) NOT NULL,
  `id_tingkat_jabatan_rata_rata_kompetensi_manajerial` int(11) NOT NULL,
  `id_jenjang_jabatan_rata_rata_kompetensi_manajerial` int(11) NOT NULL,
  `id_kelompok_manajerial_rata_rata_kompetensi_manajerial` int(11) NOT NULL,
  `nilai_standard_rata_rata_kompetensi_manajerial` int(10) NOT NULL,
  `nilai_pemetaan_rata_rata_kompetensi_manajerial` int(10) NOT NULL,
  `kode_sesi_rata_rata_kompetensi_manajerial` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_rata_rata_kompetensi_manajerial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `spjk_rata_rata_kompetensi_manajerial` WRITE;
/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_manajerial` DISABLE KEYS */;

INSERT INTO `spjk_rata_rata_kompetensi_manajerial` (`id_rata_rata_kompetensi_manajerial`, `id_kelompok_jabatan_rata_rata_kompetensi_manajerial`, `id_tingkat_jabatan_rata_rata_kompetensi_manajerial`, `id_jenjang_jabatan_rata_rata_kompetensi_manajerial`, `id_kelompok_manajerial_rata_rata_kompetensi_manajerial`, `nilai_standard_rata_rata_kompetensi_manajerial`, `nilai_pemetaan_rata_rata_kompetensi_manajerial`, `kode_sesi_rata_rata_kompetensi_manajerial`)
VALUES
	(1,1,1,4,1,3,2,'TES_2020'),
	(2,1,2,10,2,4,4,'TES_2020'),
	(3,2,2,10,10,4,4,'TES_2020'),
	(4,4,1,4,10,3,4,'TES_2020'),
	(5,5,2,10,10,4,5,'TES_2020'),
	(6,3,1,4,2,2,1,'TES_2020'),
	(7,3,2,10,2,3,3,'TES_2020'),
	(8,3,2,10,2,3,3,'TES_2020');

/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_manajerial` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table spjk_rata_rata_kompetensi_sosiokultural
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spjk_rata_rata_kompetensi_sosiokultural`;

CREATE TABLE `spjk_rata_rata_kompetensi_sosiokultural` (
  `id_rata_rata_kompetensi_sosiokultural` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kelompok_jabatan_rata_rata_kompetensi_sosiokultural` int(11) NOT NULL,
  `id_tingkat_jabatan_rata_rata_kompetensi_sosiokultural` int(11) NOT NULL,
  `id_jenjang_jabatan_rata_rata_kompetensi_sosiokultural` int(11) NOT NULL,
  `id_kelompok_sosiokultural_rata_rata_kompetensi_sosiokultural` int(11) NOT NULL,
  `nilai_rata_rata_kompetensi_sosiokultural` int(10) NOT NULL,
  `kode_sesi_rata_rata_kompetensi_sosiokultural` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_rata_rata_kompetensi_sosiokultural`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `spjk_rata_rata_kompetensi_sosiokultural` WRITE;
/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_sosiokultural` DISABLE KEYS */;

INSERT INTO `spjk_rata_rata_kompetensi_sosiokultural` (`id_rata_rata_kompetensi_sosiokultural`, `id_kelompok_jabatan_rata_rata_kompetensi_sosiokultural`, `id_tingkat_jabatan_rata_rata_kompetensi_sosiokultural`, `id_jenjang_jabatan_rata_rata_kompetensi_sosiokultural`, `id_kelompok_sosiokultural_rata_rata_kompetensi_sosiokultural`, `nilai_rata_rata_kompetensi_sosiokultural`, `kode_sesi_rata_rata_kompetensi_sosiokultural`)
VALUES
	(1,1,1,4,1,2,'TES_2020'),
	(2,1,2,10,1,4,'TES_2020'),
	(3,2,2,10,1,4,'TES_2020'),
	(4,4,1,4,1,4,'TES_2020'),
	(5,5,2,10,1,5,'TES_2020'),
	(6,3,1,4,1,1,'TES_2020'),
	(7,3,2,10,1,3,'TES_2020');

/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_sosiokultural` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table spjk_rata_rata_kompetensi_teknis
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spjk_rata_rata_kompetensi_teknis`;

CREATE TABLE `spjk_rata_rata_kompetensi_teknis` (
  `id_rata_rata_kompetensi_teknis` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kelompok_jabatan_rata_rata_kompetensi_teknis` int(11) NOT NULL,
  `id_tingkat_jabatan_rata_rata_kompetensi_teknis` int(11) NOT NULL,
  `id_jenjang_jabatan_rata_rata_kompetensi_teknis` int(11) NOT NULL,
  `id_kelompok_manajerial_rata_rata_kompetensi_teknis` int(11) NOT NULL,
  `nilai_rata_rata_kompetensi_teknis` int(10) NOT NULL,
  `kode_sesi_rata_rata_kompetensi_teknis` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_rata_rata_kompetensi_teknis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `spjk_rata_rata_kompetensi_teknis` WRITE;
/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_teknis` DISABLE KEYS */;

INSERT INTO `spjk_rata_rata_kompetensi_teknis` (`id_rata_rata_kompetensi_teknis`, `id_kelompok_jabatan_rata_rata_kompetensi_teknis`, `id_tingkat_jabatan_rata_rata_kompetensi_teknis`, `id_jenjang_jabatan_rata_rata_kompetensi_teknis`, `id_kelompok_manajerial_rata_rata_kompetensi_teknis`, `nilai_rata_rata_kompetensi_teknis`, `kode_sesi_rata_rata_kompetensi_teknis`)
VALUES
	(1,1,1,4,1,50,'TES_2020'),
	(2,1,2,10,1,53,'TES_2020'),
	(3,2,2,10,1,75,'TES_2020'),
	(4,4,1,4,1,75,'TES_2020'),
	(5,5,2,10,1,80,'TES_2020'),
	(6,3,1,4,1,50,'TES_2020'),
	(7,3,2,10,1,30,'TES_2020');

/*!40000 ALTER TABLE `spjk_rata_rata_kompetensi_teknis` ENABLE KEYS */;
UNLOCK TABLES;

CREATE ALGORITHM=UNDEFINED DEFINER=`bp2sdm`@`localhost` SQL SECURITY DEFINER VIEW `spjk_account`
AS SELECT
   `hijr_account`.`id_account` AS `id_account`,
   `hijr_account`.`username_account` AS `username_account`,
   `hijr_account`.`first_name_account` AS `first_name_account`,
   `hijr_account`.`last_name_account` AS `last_name_account`
FROM `hijr_account` where `hijr_account`.`username_account` like '1%';

CREATE ALGORITHM=UNDEFINED DEFINER=`bp2sdm`@`localhost` SQL SECURITY DEFINER VIEW `spjk_unit_auditi`
AS SELECT
   `tbl_unit_auditi`.`unit_auditi_id` AS `id_unit_auditi`,
   `tbl_unit_auditi`.`nama_unit_auditi` AS `nama_unit_auditi`,
   `tbl_unit_auditi`.`propinsi` AS `id_propinsi_unit_auditi`,
   `tbl_unit_auditi`.`eselon1` AS `id_eselon1_unit_auditi`
FROM `tbl_unit_auditi`;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
