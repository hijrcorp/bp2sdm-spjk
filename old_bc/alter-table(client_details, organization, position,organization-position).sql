/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.1.31-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

insert into `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`) values('survey-web','lhk_base,lhk_survey,lhk_lha,lhk_lapgas','secret','read,write','authorization_code,password,refresh_token','','ROLE_USER','86400','30000','{}','read,write');


insert into `tbl_organization` (`id`, `code`, `name`, `leader_name`, `leader_code`, `leader_title`, `location`) values('1508628921431','SURVEY','Survey',NULL,NULL,NULL,NULL);


insert into `tbl_position` (`position_id`, `name_position`, `description`, `date_added`, `user_added`, `date_modified`, `user_modified`, `date_deleted`, `user_deleted`) values('100','Responden','',NULL,NULL,NULL,NULL,NULL,NULL);




insert into `tbl_organization_position` (`organization_id`, `position_id`) values('1508628921431','1');
insert into `tbl_organization_position` (`organization_id`, `position_id`) values('1508628921431','2');
insert into `tbl_organization_position` (`organization_id`, `position_id`) values('1508628921431','100');


1499851133265