/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.1.31-MariaDB : Database - spjkdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`spjkdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `spjkdb`;

/*Table structure for table `spjk_bidang_teknis` */

DROP TABLE IF EXISTS `spjk_bidang_teknis`;

CREATE TABLE `spjk_bidang_teknis` (
  `id_bidang_teknis` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_bidang_teknis` varchar(500) DEFAULT NULL,
  `id_kelompok_jabatan_bidang_teknis` int(11) DEFAULT NULL,
  `id_source_bidang_teknis` varchar(50) DEFAULT NULL,
  `id_account_added_bidang_teknis` varchar(50) DEFAULT NULL,
  `timestamp_added_bidang_teknis` datetime DEFAULT NULL,
  `id_account_modified_bidang_teknis` varchar(50) DEFAULT NULL,
  `timestamp_modified_bidang_teknis` datetime DEFAULT NULL,
  PRIMARY KEY (`id_bidang_teknis`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_bidang_teknis` */

insert  into `spjk_bidang_teknis`(`id_bidang_teknis`,`nama_bidang_teknis`,`id_kelompok_jabatan_bidang_teknis`,`id_source_bidang_teknis`,`id_account_added_bidang_teknis`,`timestamp_added_bidang_teknis`,`id_account_modified_bidang_teknis`,`timestamp_modified_bidang_teknis`) values 
(1,'Bidang Perencanaan Hutan',1,'admin','admin',NULL,NULL,NULL),
(2,'Bidang Pemanfaatan Hasil Hutan',1,'admin','admin',NULL,NULL,NULL),
(3,'Bidang Rehabilitasi Hutan dan Pengelolaan DAS',1,'admin','admin',NULL,NULL,NULL),
(4,'Bidang Konservasi Sumberdaya Hutan',1,'admin','admin',NULL,NULL,NULL),
(5,'Bidang Teknis Polisi Kehutanan',2,'admin','admin',NULL,NULL,NULL),
(6,'Bidang Teknis Pengawas Lingkungan Hidup',3,'admin','admin',NULL,NULL,NULL);

/*Table structure for table `spjk_jabatan` */

DROP TABLE IF EXISTS `spjk_jabatan`;

CREATE TABLE `spjk_jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelompok_jabatan_jabatan` int(11) NOT NULL,
  `id_jenjang_jabatan_jabatan` int(11) NOT NULL,
  `id_tingkat_jenjang_jabatan` int(11) NOT NULL,
  `id_source_jabatan` varchar(50) DEFAULT NULL,
  `id_account_added_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_added_jabatan` datetime DEFAULT NULL,
  `id_account_modified_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_modified_jabatan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`),
  KEY `id_jabatan` (`id_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_jabatan` */

insert  into `spjk_jabatan`(`id_jabatan`,`id_kelompok_jabatan_jabatan`,`id_jenjang_jabatan_jabatan`,`id_tingkat_jenjang_jabatan`,`id_source_jabatan`,`id_account_added_jabatan`,`timestamp_added_jabatan`,`id_account_modified_jabatan`,`timestamp_modified_jabatan`) values 
(1,2,1,1,'admin','admin','2020-03-20 00:07:24',NULL,NULL),
(3,2,2,1,'admin','admin','2020-03-20 00:14:36',NULL,NULL),
(4,1,9,2,'admin','admin','2020-03-20 00:14:44','admin','2020-03-20 23:44:30'),
(7,2,9,2,'admin','admin','2020-04-07 16:34:33',NULL,NULL);

/*Table structure for table `spjk_jabatan_manajerial` */

DROP TABLE IF EXISTS `spjk_jabatan_manajerial`;

CREATE TABLE `spjk_jabatan_manajerial` (
  `id_jabatan_manajerial` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_view_jabatan_jabatan_manajerial` int(11) DEFAULT NULL,
  `id_kelompok_manajerial_jabatan_manajerial` int(11) DEFAULT NULL,
  `nilai_minimum_jabatan_manajerial` int(11) DEFAULT NULL,
  `id_source_jabatan_manajerial` varchar(50) DEFAULT NULL,
  `id_account_added_jabatan_manajerial` varchar(50) DEFAULT NULL,
  `timestamp_added_jabatan_manajerial` datetime DEFAULT NULL,
  `id_account_modified_jabatan_manajerial` varchar(50) DEFAULT NULL,
  `timestamp_modified_jabatan_manajerial` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jabatan_manajerial`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_jabatan_manajerial` */

insert  into `spjk_jabatan_manajerial`(`id_jabatan_manajerial`,`id_view_jabatan_jabatan_manajerial`,`id_kelompok_manajerial_jabatan_manajerial`,`nilai_minimum_jabatan_manajerial`,`id_source_jabatan_manajerial`,`id_account_added_jabatan_manajerial`,`timestamp_added_jabatan_manajerial`,`id_account_modified_jabatan_manajerial`,`timestamp_modified_jabatan_manajerial`) values 
(1,1,1,1,'admin','admin','2020-04-07 15:51:56',NULL,NULL),
(2,4,1,3,'admin','admin','2020-04-07 17:02:45','admin','2020-04-07 17:04:21');

/*Table structure for table `spjk_jenjang_jabatan` */

DROP TABLE IF EXISTS `spjk_jenjang_jabatan`;

CREATE TABLE `spjk_jenjang_jabatan` (
  `id_jenjang_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_tingkat_jenjang_jenjang_jabatan` int(11) NOT NULL,
  `nama_jenjang_jabatan` varchar(150) DEFAULT NULL,
  `id_source_jenjang_jabatan` varchar(50) DEFAULT NULL,
  `id_account_added_jenjang_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_added_jenjang_jabatan` datetime DEFAULT NULL,
  `id_account_modified_jenjang_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_modified_jenjang_jabatan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jenjang_jabatan`),
  KEY `id_jenjang_jabatan` (`id_jenjang_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_jenjang_jabatan` */

insert  into `spjk_jenjang_jabatan`(`id_jenjang_jabatan`,`id_tingkat_jenjang_jenjang_jabatan`,`nama_jenjang_jabatan`,`id_source_jenjang_jabatan`,`id_account_added_jenjang_jabatan`,`timestamp_added_jenjang_jabatan`,`id_account_modified_jenjang_jabatan`,`timestamp_modified_jenjang_jabatan`) values 
(1,1,'Madya','admin','admin','2020-03-19 21:52:11',NULL,NULL),
(2,1,'Utama','admin','admin','2020-03-19 21:54:38',NULL,NULL),
(3,2,'Pemula','admin','admin','2020-03-19 23:28:30',NULL,NULL),
(7,2,'Mahir','admin','admin','2020-03-19 23:46:34','admin','2020-03-19 23:54:27'),
(9,2,'Terampil','admin','admin','2020-03-19 23:54:13',NULL,NULL);

/*Table structure for table `spjk_kelompok_jabatan` */

DROP TABLE IF EXISTS `spjk_kelompok_jabatan`;

CREATE TABLE `spjk_kelompok_jabatan` (
  `id_kelompok_jabatan` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode_kelompok_jabatan` varchar(20) DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(200) DEFAULT '',
  `id_source_kelompok_jabatan` varchar(50) DEFAULT NULL,
  `id_account_added_kelompok_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_added_kelompok_jabatan` datetime DEFAULT NULL,
  `id_account_modified_kelompok_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_modified_kelompok_jabatan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_kelompok_jabatan` */

insert  into `spjk_kelompok_jabatan`(`id_kelompok_jabatan`,`kode_kelompok_jabatan`,`nama_kelompok_jabatan`,`id_source_kelompok_jabatan`,`id_account_added_kelompok_jabatan`,`timestamp_added_kelompok_jabatan`,`id_account_modified_kelompok_jabatan`,`timestamp_modified_kelompok_jabatan`) values 
(1,'PEH','Pengendali Ekosistem Hutan','admin','admin',NULL,NULL,NULL),
(2,'POLHUT','Polisi Kehutanan','admin','admin',NULL,NULL,NULL),
(3,'PLH','Pengawas Lingkungan Hidup','admin','admin',NULL,NULL,NULL),
(5,'AMN','Amaaan','admin','admin','2020-04-07 11:27:43','admin','2020-04-07 17:47:47');

/*Table structure for table `spjk_kelompok_manajerial` */

DROP TABLE IF EXISTS `spjk_kelompok_manajerial`;

CREATE TABLE `spjk_kelompok_manajerial` (
  `id_kelompok_manajerial` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kelompok_manajerial` varchar(500) DEFAULT NULL,
  `id_source_kelompok_manajerial` varchar(50) DEFAULT NULL,
  `id_account_added_kelompok_manajerial` varchar(50) DEFAULT NULL,
  `timestamp_added_kelompok_manajerial` datetime DEFAULT NULL,
  `id_account_modified_kelompok_manajerial` varchar(50) DEFAULT NULL,
  `timestamp_modified_kelompok_manajerial` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_manajerial`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_kelompok_manajerial` */

insert  into `spjk_kelompok_manajerial`(`id_kelompok_manajerial`,`nama_kelompok_manajerial`,`id_source_kelompok_manajerial`,`id_account_added_kelompok_manajerial`,`timestamp_added_kelompok_manajerial`,`id_account_modified_kelompok_manajerial`,`timestamp_modified_kelompok_manajerial`) values 
(1,'Integritas','admin','admin',NULL,NULL,NULL),
(3,'Perencanaan yang terorganisasi','admin','admin',NULL,NULL,NULL),
(4,'Kepemimpinan','admin','admin',NULL,NULL,NULL),
(5,'Kemampuan mempengaruhi orang lain','admin','admin',NULL,NULL,NULL),
(6,'Kemampuan berkomunikasi','admin','admin',NULL,NULL,NULL),
(7,'Kerjasama','admin','admin',NULL,NULL,NULL),
(8,'Membangun relasi','admin','admin',NULL,NULL,NULL),
(9,'Tanggap terhadap budaya','admin','admin',NULL,NULL,NULL),
(10,'Interaksi sosial','admin','admin',NULL,NULL,NULL);

/*Table structure for table `spjk_kelompok_sosiokultural` */

DROP TABLE IF EXISTS `spjk_kelompok_sosiokultural`;

CREATE TABLE `spjk_kelompok_sosiokultural` (
  `id_kelompok_sosiokultural` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kelompok_sosiokultural` varchar(200) DEFAULT NULL,
  `id_source_kelompok_sosiokultural` varchar(50) DEFAULT NULL,
  `id_account_added_kelompok_sosiokultural` varchar(50) DEFAULT NULL,
  `timestamp_added_kelompok_sosiokultural` datetime DEFAULT NULL,
  `id_account_modified_kelompok_sosiokultural` varchar(50) DEFAULT NULL,
  `timestamp_modified_kelompok_sosiokultural` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_sosiokultural`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_kelompok_sosiokultural` */

insert  into `spjk_kelompok_sosiokultural`(`id_kelompok_sosiokultural`,`nama_kelompok_sosiokultural`,`id_source_kelompok_sosiokultural`,`id_account_added_kelompok_sosiokultural`,`timestamp_added_kelompok_sosiokultural`,`id_account_modified_kelompok_sosiokultural`,`timestamp_modified_kelompok_sosiokultural`) values 
(1,'Perekat Bangsa','admin','admin',NULL,NULL,NULL);

/*Table structure for table `spjk_kelompok_teknis` */

DROP TABLE IF EXISTS `spjk_kelompok_teknis`;

CREATE TABLE `spjk_kelompok_teknis` (
  `id_kelompok_teknis` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kelompok_teknis` varchar(200) DEFAULT NULL,
  `id_kelompok_jabatan_kelompok_teknis` int(11) DEFAULT NULL,
  `id_bidang_teknis_kelompok_teknis` int(11) DEFAULT NULL,
  `id_source_kelompok_teknis` varchar(50) DEFAULT NULL,
  `id_account_added_kelompok_teknis` varchar(50) DEFAULT NULL,
  `timestamp_added_kelompok_teknis` datetime DEFAULT NULL,
  `id_account_modified_kelompok_teknis` varchar(50) DEFAULT NULL,
  `timestamp_modified_kelompok_teknis` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_teknis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_kelompok_teknis` */

insert  into `spjk_kelompok_teknis`(`id_kelompok_teknis`,`nama_kelompok_teknis`,`id_kelompok_jabatan_kelompok_teknis`,`id_bidang_teknis_kelompok_teknis`,`id_source_kelompok_teknis`,`id_account_added_kelompok_teknis`,`timestamp_added_kelompok_teknis`,`id_account_modified_kelompok_teknis`,`timestamp_modified_kelompok_teknis`) values 
(1,'Persuteraan Alam',1,1,'admin','admin','2020-04-07 18:09:46','admin','2020-04-07 18:35:03'),
(3,'Rehabilitasi Hutan dan lahan',1,3,'admin','admin','2020-04-07 18:35:53',NULL,NULL);

/*Table structure for table `spjk_tingkat_jenjang` */

DROP TABLE IF EXISTS `spjk_tingkat_jenjang`;

CREATE TABLE `spjk_tingkat_jenjang` (
  `id_tingkat_jenjang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tingkat_jenjang` varchar(150) DEFAULT NULL,
  `id_source_tingkat_jenjang` varchar(50) DEFAULT NULL,
  `id_account_added_tingkat_jenjang` varchar(50) DEFAULT NULL,
  `timestamp_added_tingkat_jenjang` datetime DEFAULT NULL,
  `id_account_modified_tingkat_jenjang` varchar(50) DEFAULT NULL,
  `timestamp_modified_tingkat_jenjang` datetime DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_jenjang`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_tingkat_jenjang` */

insert  into `spjk_tingkat_jenjang`(`id_tingkat_jenjang`,`nama_tingkat_jenjang`,`id_source_tingkat_jenjang`,`id_account_added_tingkat_jenjang`,`timestamp_added_tingkat_jenjang`,`id_account_modified_tingkat_jenjang`,`timestamp_modified_tingkat_jenjang`) values 
(1,'Keahlian','admin','admin','2020-03-19 21:03:14',NULL,NULL),
(2,'Keterampilan','admin','admin','2020-03-19 21:03:18',NULL,NULL),
(10,'Almyghty','admin','admin','2020-04-06 23:12:52',NULL,NULL);

/*Table structure for table `spjk_unit_kompetensi_manajerial` */

DROP TABLE IF EXISTS `spjk_unit_kompetensi_manajerial`;

CREATE TABLE `spjk_unit_kompetensi_manajerial` (
  `id_unit_kompetensi_manajerial` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kelompok_manajerial_unit_kompetensi_manajerial` int(11) DEFAULT NULL,
  `kode_unit_kompetensi_manajerial` varchar(50) DEFAULT NULL,
  `judul_unit_kompetensi_manajerial` varchar(100) DEFAULT NULL,
  `id_source_unit_kompetensi_manajerial` varchar(50) DEFAULT NULL,
  `id_account_added_unit_kompetensi_manajerial` varchar(50) DEFAULT NULL,
  `timestamp_added_unit_kompetensi_manajerial` datetime DEFAULT NULL,
  `id_account_modified_unit_kompetensi_manajerial` varchar(50) DEFAULT NULL,
  `timestamp_modified_unit_kompetensi_manajerial` datetime DEFAULT NULL,
  PRIMARY KEY (`id_unit_kompetensi_manajerial`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_unit_kompetensi_manajerial` */

insert  into `spjk_unit_kompetensi_manajerial`(`id_unit_kompetensi_manajerial`,`id_kelompok_manajerial_unit_kompetensi_manajerial`,`kode_unit_kompetensi_manajerial`,`judul_unit_kompetensi_manajerial`,`id_source_unit_kompetensi_manajerial`,`id_account_added_unit_kompetensi_manajerial`,`timestamp_added_unit_kompetensi_manajerial`,`id_account_modified_unit_kompetensi_manajerial`,`timestamp_modified_unit_kompetensi_manajerial`) values 
(1,1,'LH.PDL.001.01','Mengidentifikasi Permasalahan / Kondisi Lingkungan\r\n','admin','admin',NULL,NULL,NULL),
(2,3,'001','lorem dolor','admin','admin','2020-04-07 14:25:41','admin','2020-04-07 14:35:06');

/*Table structure for table `spjk_unit_kompetensi_sosiokultural` */

DROP TABLE IF EXISTS `spjk_unit_kompetensi_sosiokultural`;

CREATE TABLE `spjk_unit_kompetensi_sosiokultural` (
  `id_unit_kompetensi_sosiokultural` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kelompok_sosiokultural_unit_kompetensi_sosiokultural` int(11) DEFAULT NULL,
  `kode_unit_kompetensi_sosiokultural` varchar(50) DEFAULT NULL,
  `judul_unit_kompetensi_sosiokultural` varchar(100) DEFAULT NULL,
  `id_source_unit_kompetensi_sosiokultural` varchar(50) DEFAULT NULL,
  `id_account_added_unit_kompetensi_sosiokultural` varchar(50) DEFAULT NULL,
  `timestamp_added_unit_kompetensi_sosiokultural` datetime DEFAULT NULL,
  `id_account_modified_unit_kompetensi_sosiokultural` varchar(50) DEFAULT NULL,
  `timestamp_modified_unit_kompetensi_sosiokultural` datetime DEFAULT NULL,
  PRIMARY KEY (`id_unit_kompetensi_sosiokultural`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_unit_kompetensi_sosiokultural` */

/*Table structure for table `spjk_unit_kompetensi_teknis` */

DROP TABLE IF EXISTS `spjk_unit_kompetensi_teknis`;

CREATE TABLE `spjk_unit_kompetensi_teknis` (
  `id_unit_kompetensi_teknis` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kelompok_jabatan_unit_kompetensi_teknis` int(11) DEFAULT NULL,
  `id_bidang_teknis_unit_kompetensi_teknis` int(11) DEFAULT NULL,
  `id_kelompok_teknis_unit_kompetensi_teknis` int(11) DEFAULT NULL,
  `kode_unit_kompetensi_teknis` varchar(50) DEFAULT NULL,
  `judul_unit_kompetensi_teknis` varchar(100) DEFAULT NULL,
  `jenis_kompetensi_unit_kompetensi_teknis` varchar(50) DEFAULT NULL,
  `id_source_unit_kompetensi_teknis` varchar(50) DEFAULT NULL,
  `id_account_added_unit_kompetensi_teknis` varchar(50) DEFAULT NULL,
  `timestamp_added_unit_kompetensi_teknis` datetime DEFAULT NULL,
  `id_account_modified_unit_kompetensi_teknis` varchar(50) DEFAULT NULL,
  `timestamp_modified_unit_kompetensi_teknis` datetime DEFAULT NULL,
  PRIMARY KEY (`id_unit_kompetensi_teknis`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `spjk_unit_kompetensi_teknis` */

insert  into `spjk_unit_kompetensi_teknis`(`id_unit_kompetensi_teknis`,`id_kelompok_jabatan_unit_kompetensi_teknis`,`id_bidang_teknis_unit_kompetensi_teknis`,`id_kelompok_teknis_unit_kompetensi_teknis`,`kode_unit_kompetensi_teknis`,`judul_unit_kompetensi_teknis`,`jenis_kompetensi_unit_kompetensi_teknis`,`id_source_unit_kompetensi_teknis`,`id_account_added_unit_kompetensi_teknis`,`timestamp_added_unit_kompetensi_teknis`,`id_account_modified_unit_kompetensi_teknis`,`timestamp_modified_unit_kompetensi_teknis`) values 
(1,1,1,1,'001','lorem dolor','PILIHAN','admin','admin','2020-04-07 19:05:50',NULL,NULL);

/*Table structure for table `spjk_view_jabatan` */

DROP TABLE IF EXISTS `spjk_view_jabatan`;

/*!50001 DROP VIEW IF EXISTS `spjk_view_jabatan` */;
/*!50001 DROP TABLE IF EXISTS `spjk_view_jabatan` */;

/*!50001 CREATE TABLE  `spjk_view_jabatan`(
 `id_view_jabatan` int(11) ,
 `id_kelompok_jabatan_view_jabatan` int(11) ,
 `id_jenjang_jabatan_view_jabatan` int(11) ,
 `id_tingkat_jenjang_view_jabatan` int(11) ,
 `id_source_view_jabatan` varchar(50) ,
 `id_account_added_view_jabatan` varchar(50) ,
 `timestamp_added_view_jabatan` datetime ,
 `id_account_modified_view_jabatan` varchar(50) ,
 `timestamp_modified_view_jabatan` datetime ,
 `nama_kelompok_view_jabatan` varchar(200) ,
 `nama_jenjang_view_jabatan` varchar(150) ,
 `nama_tingkat_view_jenjang` varchar(150) ,
 `nama_view_jabatan` varchar(322) 
)*/;

/*View structure for view spjk_view_jabatan */

/*!50001 DROP TABLE IF EXISTS `spjk_view_jabatan` */;
/*!50001 DROP VIEW IF EXISTS `spjk_view_jabatan` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `spjk_view_jabatan` AS (select `a`.`id_jabatan` AS `id_view_jabatan`,`a`.`id_kelompok_jabatan_jabatan` AS `id_kelompok_jabatan_view_jabatan`,`a`.`id_jenjang_jabatan_jabatan` AS `id_jenjang_jabatan_view_jabatan`,`a`.`id_tingkat_jenjang_jabatan` AS `id_tingkat_jenjang_view_jabatan`,`a`.`id_source_jabatan` AS `id_source_view_jabatan`,`a`.`id_account_added_jabatan` AS `id_account_added_view_jabatan`,`a`.`timestamp_added_jabatan` AS `timestamp_added_view_jabatan`,`a`.`id_account_modified_jabatan` AS `id_account_modified_view_jabatan`,`a`.`timestamp_modified_jabatan` AS `timestamp_modified_view_jabatan`,`b`.`nama_kelompok_jabatan` AS `nama_kelompok_view_jabatan`,`c`.`nama_jenjang_jabatan` AS `nama_jenjang_view_jabatan`,`d`.`nama_tingkat_jenjang` AS `nama_tingkat_view_jenjang`,concat(`b`.`kode_kelompok_jabatan`,' ',`d`.`nama_tingkat_jenjang`,' ',`c`.`nama_jenjang_jabatan`) AS `nama_view_jabatan` from (((`spjk_jabatan` `a` left join `spjk_kelompok_jabatan` `b` on((`b`.`id_kelompok_jabatan` = `a`.`id_kelompok_jabatan_jabatan`))) left join `spjk_jenjang_jabatan` `c` on((`c`.`id_jenjang_jabatan` = `a`.`id_jenjang_jabatan_jabatan`))) left join `spjk_tingkat_jenjang` `d` on((`d`.`id_tingkat_jenjang` = `a`.`id_tingkat_jenjang_jabatan`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
